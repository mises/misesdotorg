﻿using System;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.SilverlightMediaFramework.Player;

namespace Mises.Silverlight.VideoPlayer {
   
    public partial class MainPage : UserControl {
        private string _path;
        private int _mediaId;
        public MainPage(string path) {
            this._path = path;

            InitializeComponent();
            this.Loaded += Page_Loaded;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            if (HtmlPage.Document.QueryString != null && HtmlPage.Document.QueryString.ContainsKey("MediaId"))
            {
                this._mediaId = int.Parse(HtmlPage.Document.QueryString["MediaId"]);
                getSourceFromMediaId();
            }

            if (HtmlPage.Document.QueryString != null && HtmlPage.Document.QueryString.ContainsKey("path") )
            {
                this._path = HtmlPage.Document.QueryString["path"];
                setPlayerToUrl();
            }
            else {
                if(!String.IsNullOrEmpty(this._path)) {
                    setPlayerToUrl();
                }
                else {
                    HtmlPage.Window.Alert("There was no Media specified. Please specify a media source either by Querystring or InitParams in the object tag.");
                }
            }
        }

        private void getSourceFromMediaId()
        {
            
        }

        private void setPlayerToUrl()
        {
            //this.videoplayer.MediaElement.SmoothStreamingSource = new Uri(this._path);
            this.videoplayer.MediaElement.Source = new Uri(this._path);
           // this.videoplayer.MediaElement.CanSeek = false;
           // this.videoplayer.MediaElement.BufferingTime = TimeSpan.FromSeconds(5);
            this.videoplayer.MediaElement.MediaFailed += (MediaElement_MediaFailed);
            this.videoplayer.MediaElement.MediaOpened += (MediaElement_MediaOpened);
        }

        void MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            //this.videoplayer.MediaElement.StartSeekToLive();
            //this.videoplayer.MediaElement.Source = new Uri(this._path);
            //this.videoplayer.MediaElement.Play();
        }

        void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            this.BusyWindow.IsBusy = false;
            this.videoplayer.MediaElement.Play();
        }
 
       
   
    }
}
