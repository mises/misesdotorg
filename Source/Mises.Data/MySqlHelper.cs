﻿#region

using System.Data;
using MySql.Data.MySqlClient;

#endregion

namespace Mises.Data
{
    public class MySqlHelper
    {
        private readonly string connString;

        public MySqlHelper(string connectionString)
        {
            connString = connectionString;
        }

        public DataSet ExecuteQuery(string commandText)
        {
            var ds = new DataSet();
            using (var connection = new MySqlConnection(connString))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = commandText;
                using (var data = new MySqlDataAdapter {SelectCommand = command})
                {
                    data.Fill(ds);
                    connection.Close();
                }
            }
            return ds;
        }


        public int ExecuteNonQuery(string commandText)
        {
            int result;
            using (var connection = new MySqlConnection(connString))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = commandText;
                result = command.ExecuteNonQuery();
            }
            return result;
        }
    }
}