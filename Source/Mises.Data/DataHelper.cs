#region

using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Web.Configuration;
using MvcMiniProfiler;
using MvcMiniProfiler.Data;

#endregion

namespace Mises.Data
{
    ///<summary>
    ///  Base Business Class to Inherit
    ///</summary>
    ///<remarks>
    ///</remarks>
    [Serializable]
    [DataContract]
    public abstract class DataHelper
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["Public"].ConnectionString; }
        }

        public static ProfiledDbConnection DbConnection
        {
            get
            {
                var conn = DataHelper.ConnectionString;
                return new MvcMiniProfiler.Data.ProfiledDbConnection(new SqlConnection(conn), MiniProfiler.Current);
            }
        }

        public static string StoreConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["AbleCommerce"].ConnectionString; }
        }

        public static string BlogConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MisesBlog"].ConnectionString; }
        }

        public static string DocumentFolder
        {
            get { return ConfigurationManager.AppSettings["DocumentFolderRoot"]; }
        }

        public static string StoreFolder
        {
            get { return ConfigurationManager.AppSettings["StoreFolderRoot"]; }
        }

        public static string WebsiteFolder
        {
            get { return ConfigurationManager.AppSettings["WebsiteFolderRoot"]; }
        }


        #region Entity Framework Profiling

        private static SqlConnection GetConnection()
        {
            var connStr = ConfigurationManager.ConnectionStrings["MisesEntities"].ConnectionString;
            var entityConnStr = new EntityConnectionStringBuilder(connStr);
            return new SqlConnection(entityConnStr.ProviderConnectionString);
        }

        public static MisesModel EntityDataModel
        {
            get
            {
                //var sqlConn = new SqlConnection(ConnectionString);
                //var conn = new MvcMiniProfiler.Data.EFProfiledDbConnection(sqlConn, MiniProfiler.Current);
                //return conn.CreateObjectContext<MisesModel>();

                return new MisesModel(ConfigurationManager.ConnectionStrings["MisesEntities"].ConnectionString);
               
            }
        }

        public static AbleCommerceEntities StoreDataModel
        {
            get
            {
                var sqlConn = new SqlConnection(StoreConnectionString);
                var conn = new MvcMiniProfiler.Data.EFProfiledDbConnection(sqlConn, MiniProfiler.Current);
                return conn.CreateObjectContext<Mises.Data.AbleCommerceEntities>();


            }
        }

        public static T GetProfiledContext<T>() where T : System.Data.Objects.ObjectContext
        {
            var conn = new ProfiledDbConnection(GetStoreConnection<T>(), MiniProfiler.Current);
            return ObjectContextUtils.CreateObjectContext<T>(conn);
        }

        public static DbConnection GetStoreConnection<T>() where T : System.Data.Objects.ObjectContext
        {
            return GetStoreConnection("name=" + typeof(T).Name);
        }

        public static DbConnection GetStoreConnection(string entityConnectionString)
        {
            // Build the initial connection string.
            var builder = new EntityConnectionStringBuilder(entityConnectionString);

            // If the initial connection string refers to an entry in the configuration, load that as the builder.
            object configName;
            if (builder.TryGetValue("name", out configName))
            {
                var configEntry = WebConfigurationManager.ConnectionStrings[configName.ToString()];
                builder = new EntityConnectionStringBuilder(configEntry.ConnectionString);
            }

            // Find the proper factory for the underlying connection.
            var factory = DbProviderFactories.GetFactory(builder.Provider);

            // Build the new connection.
            DbConnection tempConnection = null;
            try
            {
                tempConnection = factory.CreateConnection();
                tempConnection.ConnectionString = builder.ProviderConnectionString;

                var connection = tempConnection;
                tempConnection = null;
                return connection;
            }
            finally
            {
                // If creating of the connection failed, dispose the connection.
                if (tempConnection != null)
                {
                    tempConnection.Dispose();
                }
            }
        }

        #endregion




        public static MisesDBDataContext MisesDataContext
        {
            get
            {
                var context = new MisesDBDataContext(new MvcMiniProfiler.Data.ProfiledDbConnection(new SqlConnection(DataHelper.ConnectionString), MiniProfiler.Current));
                return context;
            }
        }



        ///<summary>
        ///  Executes query and returns a single object as the result.
        ///</summary>
        ///<param name = "commandText">The command text.</param>
        ///<param name = "commandParameters">
        ///  The command parameters.
        ///</param>
        ///<returns></returns>
        public static object ExecuteScalar(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, commandText, commandParameters);
        }


        ///<summary>
        ///  Executes the query and returns a dataset.
        ///</summary>
        ///<param name = "commandText">The command text.</param>
        ///<param name = "commandParameters">
        ///  The command parameters.
        ///</param>
        ///<returns></returns>
        public static DataSet ExecuteDataset(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, commandText, commandParameters);
        }

        public static DataSet ExecuteDatasetSP(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, commandText,
                                            commandParameters);
        }

        ///<summary>
        ///  Executes the a query and returns the number of rows changed
        ///</summary>
        ///<param name = "commandText">The command text.</param>
        ///<param name = "commandParameters">
        ///  The command parameters.
        ///</param>
        ///<returns></returns>
        public static int ExecuteNonQuery(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, commandText, commandParameters);
        }

        public static void SetTransactionIsolationLevelReadUncommited(MisesModel context)
        {
            context.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
        }
    }
}