#region

using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.VisualBasic;

#endregion

namespace Mises.Data
{
    ///<summary>
    ///  Contains DataFormatting
    ///</summary>
    ///<remarks>
    ///</remarks>
    [Serializable]
    [DataContract]
    public class DataFormat
    {
        #region Data Conversions

        public static bool CheckBool(object value)
        {
            return !Convert.IsDBNull(value) && value.ToString() != "" && Convert.ToBoolean(value);
        }

        //public static int CellIntValue(DataRow row, string value)
        //{
        //    return Convert.IsDBNull(row[value]) ? 0 : Convert.ToInt32(row[value]);
        //}

        //public static string ObjToString(object value)
        //{
        //    return value == null || Convert.IsDBNull(value) ? "" : value.ToString();
        //}

        //public static decimal ObjToDecimal(object value)
        //{
        //    try
        //    {
        //        return value == null || Convert.IsDBNull(value) || value.ToString() == "" ? 0 : Convert.ToDecimal(value);
        //    }
        //    catch
        //    {
        //        return 0M;
        //    }
        //}

        //public static bool ObjToBoolean(object value)
        //{
        //    try
        //    {
        //        return (value != null && !Convert.IsDBNull(value)) && value.ToString() != "" && Convert.ToBoolean(value);
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public static int ObjToInteger(object value)
        //{
        //    try
        //    {
        //        return value == null || Convert.IsDBNull(value) || value.ToString() == "" ? 0 : Convert.ToInt32(value);
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}


        //public static double ObjToDouble(object value)
        //{
        //    return ObjToDouble(value, false);
        //}

        //public static double ObjToDouble(object value, bool round)
        //{
        //    try
        //    {
        //        if (value == null || Convert.IsDBNull(value) || value.ToString() == String.Empty)
        //        {
        //            return (double)0M;
        //        }
        //        if (round == false)
        //        {
        //            return
        //                Convert.ToDouble(Strings.FormatNumber(Convert.ToDouble(value), 2, TriState.UseDefault,
        //                                                      TriState.True, TriState.True));
        //        }
        //        return
        //            (double)
        //            Convert.ToDecimal(Strings.FormatNumber(Convert.ToDouble(value), 2, TriState.UseDefault,
        //                                                   TriState.True, TriState.True));
        //    }
        //    catch
        //    {
        //        return (double)0M;
        //    }
        //}

        public static DataTable RowsToTable(DataRow[] rows, DataTable dtTemplate, string newTableName)
        {
            DataTable dt = dtTemplate.Clone();
            dt.TableName = newTableName;
            if (dt.Columns.Count > 0)
            {
                dt.PrimaryKey = new[] { dt.Columns[0] };
            }
            if (rows == null || rows.Length == 0)
            {
                return dt;
            }

            rows.ToList().ForEach(dt.ImportRow);
            return dt;
        }

        #endregion

        #region String Formatting

        public static string GetPhoneFormat(string phone)
        {
            if (String.IsNullOrWhiteSpace(phone))
            {
                return "";
            }
            string p;
            if (phone.Length >= 10)
            {
                p = phone.Substring(0, 3) + "-" + phone.Substring(3, 3) + "-" +
                    phone.Substring(6, phone.Length - 6);
            }
            else if (phone.Length < 10 & phone.Length > 3)
            {
                p = phone.Substring(0, 3) + "-" + phone.Substring(3, phone.Length - 3);
            }
            else
            {
                p = phone;
            }
            return p;
        }

        #endregion

        #region String Parsing

        public const string siteRoot = "http://mises.freecapitalists.org//";
        public const string assets = "http://store.mises.org/Assets";
        private const string libraryURL = "http://library.mises.org";
        private const string mediaURL = "http://media.mises.org";


        public static string GetAbsoluteURL(string URL)
        {
            if (String.IsNullOrWhiteSpace(URL)) return String.Empty;

            //TODO: FIX to relative URL
            URL = URL.Replace("\\", "/");
            if (URL.StartsWith("~/Assets"))
            {
                URL = URL.Replace("~/Assets", assets);
            }
            else if (URL.StartsWith("/") & !URL.StartsWith("//"))
            {
                URL = siteRoot + URL;
            }
            else if (!URL.StartsWith("http") && !URL.StartsWith("//"))
            {
                URL = siteRoot + URL;
            }

            return URL.Replace(" ", "%20");
        }

        public static string GetFileNameFromURL(string URL)
        {
            int LastIndex = URL.LastIndexOf("/");
            URL = URL.Substring(LastIndex + 1, (URL.Length - LastIndex - 1));
            return URL;
        }

        public static bool GetPathFromURL(string webRoot, string URL, out string fileNameAndPath)
        {
            fileNameAndPath = null;

            //URL = URL.Replace("%20", " ");
            URL = HttpUtility.HtmlDecode(URL);
            
            if (URL.StartsWith(libraryURL))
            {
                fileNameAndPath = DataHelper.DocumentFolder + URL.Replace(libraryURL, "");
            }
            else
            {

                URL = URL.Replace(mediaURL, "/multimedia").Replace(siteRoot, String.Empty);
                
                if (URL.StartsWith("http://") && !URL.StartsWith(siteRoot))
                {
                    return false;
                }

            }

            if (String.IsNullOrEmpty(fileNameAndPath))
                fileNameAndPath = webRoot + URL;

            return File.Exists(fileNameAndPath.Replace(siteRoot, String.Empty).Replace("/", "\\"));
        }

        public static int CountLinks(string content)
        {
            const string pattern =
                "(http|ftp|https):\\/\\/[\\w]+(.[\\w]+)([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])";
            return Regex.Matches(content, pattern, RegexOptions.IgnoreCase).Count;
        }

        public static string StripHTML(string content)
        {
            return !String.IsNullOrWhiteSpace(content)
                       ? HttpUtility.HtmlEncode((Regex.Replace(content, "<(.|\\n)*?>", String.Empty).Replace("&nbsp;", " ").Replace("&#39;", "'")))
                       : content;
        }

        public static string EncodeForJavaScript(string text)
        {
            return text.Replace("\\", "\\\\").Replace("\n", "\\n").Replace("\r", "\\r").Replace("'", "\\'");
        }

        public static bool IsGUID(string expression)
        {
            if (expression != null)
            {
                var guidRegEx =
                    new Regex(
                        "^(\\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\\}{0,1})$");

                return guidRegEx.IsMatch(expression);
            }
            return false;
        }

        public static bool IsValidEmail(string email)
        {
            return !String.IsNullOrWhiteSpace(email) &&
                   Regex.IsMatch(email, "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
        }

        #endregion

        /// <summary>
        ///   Validates a credit card number using the standard Luhn/mod10
        ///   validation algorithm.
        /// </summary>
        /// <param name = "cardNumber">Card number, with or without
        ///   punctuation</param>
        /// <returns>True if card number appears valid, false if not
        /// </returns>
        public static bool IsCreditCardValid(string cardNumber)
        {
            const string allowed = "0123456789";
            int i;

            var cleanNumber = new StringBuilder();
            for (i = 0; i < cardNumber.Length; i++)
            {
                if (allowed.IndexOf(cardNumber.Substring(i, 1)) >= 0)
                    cleanNumber.Append(cardNumber.Substring(i, 1));
            }
            if (cleanNumber.Length < 13 || cleanNumber.Length > 16)
                return false;

            for (i = cleanNumber.Length + 1; i <= 16; i++)
                cleanNumber.Insert(0, "0");

            int total = 0;
            string number = cleanNumber.ToString();

            for (i = 1; i <= 16; i++)
            {
                int multiplier = 1 + (i % 2);
                int digit = Int32.Parse(number.Substring(i - 1, 1));
                int sum = digit * multiplier;
                if (sum > 9)
                    sum -= 9;
                total += sum;
            }
            return (total % 10 == 0);
        }

        public static string GetDateString(DateTime startDate, DateTime endDate)
        {
            if (endDate == DateTime.MinValue)
                return startDate.ToLongDateString();

            if (startDate.Date == endDate.Date)
                return String.Format("{0} {1}",
                                     CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month),
                                     startDate.Day);

            if (startDate.Month == endDate.Month)
                return String.Format("{0} {1}-{2} {3}",
                                     CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month),
                                     startDate.Day, endDate.Day, endDate.Year);

            else
                return String.Format("{0} {1} - {2} {3} {4}",
                                     CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month),
                                     startDate.Day,
                                     CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(endDate.Month), endDate.Day,
                                     endDate.Year);
        }

        public static string GenerateSlug(object phrase)
        {
            return ConvertTextToSlug(phrase.ToString());
            //return GenerateSlug((string)phrase, 140);
        }

        public static string GenerateSlug(string phrase)
        {
            return ConvertTextToSlug(phrase);
            //   return GenerateSlug(phrase, 140);
        }

        /// <summary>
        ///   Creates a "slug" from text that can be used as part of a valid URL.
        /// 
        ///   Invalid characters are converted to hyphens. Punctuation that is
        ///   perfect valid in a URL is also converted to hyphens to keep the
        ///   result mostly text. Steps are taken to prevent leading, trailing,
        ///   and consecutive hyphens.
        /// </summary>
        /// <param name = "s">String to convert to a slug</param>
        /// <returns></returns>
        public static string ConvertTextToSlug(string s)
        {
            if (s == null) return String.Empty;
            s = s.Replace("'", "").Replace(@"\", "").Replace("\"", String.Empty);
            s = FixForeignEntities(s);
            s = StripHTML(s);

            var sb = new StringBuilder();
            bool wasHyphen = true;
            foreach (char c in s)
            {
                if (Char.IsLetterOrDigit(c))
                {
                    sb.Append(c);
                    wasHyphen = false;
                }
                else if (Char.IsWhiteSpace(c) && !wasHyphen)
                {
                    sb.Append('-');
                    wasHyphen = true;
                }
            }
            // Avoid trailing hyphens
            if (wasHyphen && sb.Length > 0)
                sb.Length--;
            return sb.ToString();
        }

        public static string FixForeignEntities(string sanitized)
        {
            sanitized = sanitized.Replace("�", "a");
            sanitized = sanitized.Replace("�", "a");
            sanitized = sanitized.Replace("�", "e");
            sanitized = sanitized.Replace("�", "o");
            sanitized = sanitized.Replace("�", "u");

            return sanitized;

        }

        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        #region Path Sanitization

        public const int maxFileLength = 254; // maximum path length is 260 chars
        private const int maxDirectoryLength = 254;

        // these are legal but undesirable
        const string invalidPathChars = "���'!@#$%^&*[]{}=;|/*+~`<>?\"@+\"";

        public static string SanitizeFileName(string fileName)
        {
            if (fileName.Length > maxFileLength)
            {
                fileName = fileName.Substring(0, maxFileLength);
            }

            fileName = FixForeignEntities(fileName);

            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = String.Format(@"[{0}]+", invalidChars);
            fileName = Regex.Replace(fileName, invalidReStr, "_");

            invalidPathChars.ToList().ForEach(c => fileName = fileName.Replace(c.ToString(), String.Empty));

            return fileName.Trim('_').Trim();
        }

        public static string SanitizePath(string filePath)
        {
            filePath = StripHTML(filePath);

            if (filePath.Length > maxDirectoryLength)
            {
                string ext = Path.GetExtension(filePath);
                filePath = filePath.Substring(0, maxDirectoryLength - 4) + ext;
            }

            filePath = FixForeignEntities(filePath);

            string invalidChars = Regex.Escape(new string(Path.GetInvalidPathChars()));
            string invalidReStr = String.Format(@"[{0}]+", invalidChars);
            filePath = Regex.Replace(filePath, invalidReStr, "_");
            invalidPathChars.ToList().ForEach(c => filePath = filePath.Replace(c.ToString(), String.Empty));
            return filePath.Trim('_').Trim();
        }

        /// <summary>
        /// Domain-specific sanitization
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SanitizePathElement(string value)
        {
            if (value.EndsWith(", An"))
            {
                value = value.Substring(0, value.Length - 4);
            }
            else if (value.EndsWith(", The"))
            {
                value = value.Substring(0, value.Length - 5);
            }

            if (value.EndsWith("-ed"))
            {
                value = value.Substring(0, value.Length - 3);
            }

            value = value.Replace("(ed.)", "").Trim().Replace(":", String.Empty).Replace(".", "").Replace("  ", " ").Trim();

            return value;
        }

        #endregion
    }
}