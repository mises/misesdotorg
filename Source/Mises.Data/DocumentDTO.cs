#region

using System;
using System.Data.Linq;
using System.Runtime.Serialization;

#endregion

namespace Mises.Data
{
    [DataContract]
    public class DocumentDTO
    {
        [DataMember]
        public int DocumentId { get; set; }

        [DataMember]
        public Guid GUID { get; set; }

        [DataMember]
        public bool Display { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Source { get; set; }

      //  [DataMember]
      //  public bool? FullText { get; set; }

        [DataMember]
        public int Author1 { get; set; }

        [DataMember]
        public int? Author2 { get; set; }

        [DataMember]
        public int? CategoryId { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime EditDate { get; set; }

        [DataMember]
        public bool? Featured { get; set; }

        [DataMember]
        public string PrivateComment { get; set; }

        [DataMember]
        public int? Length { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string MetaKeyWords { get; set; }

        [DataMember]
        public int? OldMediaTypeId { get; set; }

        [DataMember]
        public string OldUrl { get; set; }

        [DataMember]
        public string PublicationInformation { get; set; }

        [DataMember]
        public Binary ImageBinary { get; set; }

        [DataMember]
        public string Author1Name { get; set; }

        [DataMember]
        public string Author2Name { get; set; }

        [DataMember]
        public int MediaTypeId { get; set; }

        [DataMember]
        public string URL { get; set; }

        [DataMember]
        public int MediaId { get; set; }
    }
}