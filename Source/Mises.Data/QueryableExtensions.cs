﻿using System;
using System.Collections.Generic;
using System.Data.Objects;

using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Mises.Data
{
    /// <summary>
    /// Include for IQueryable
    /// </summary>
    /// <remarks>
    /// The include code is basically copied from Entity Framework 4.1
    /// </remarks>
    public static class QueryableExtensions
    {
        public static IQueryable<T> NoTracking<T>(this IQueryable<T> source)
        {
            var objectQuery = source as ObjectQuery;
            if (objectQuery != null)
                objectQuery.MergeOption= System.Data.Objects.MergeOption.NoTracking;

            return source;
        }

        private static bool TryParsePath(Expression expression, out string path)
        {
            path = null;
            expression = expression.RemoveConvert();
            var memberExpression = expression as MemberExpression;
            var methodCallExpression = expression as MethodCallExpression;
            if (memberExpression != null)
            {
                string parentPath;
                string name = memberExpression.Member.Name;
                if (!TryParsePath(memberExpression.Expression, out parentPath))
                {
                    return false;
                }
                path = (parentPath == null) ? name : (parentPath + "." + name);
            }
            else if (methodCallExpression != null)
            {
                if ((methodCallExpression.Method.Name == "Select") && (methodCallExpression.Arguments.Count == 2))
                {
                    string parentPath;
                    if (!TryParsePath(methodCallExpression.Arguments[0], out parentPath))
                    {
                        return false;
                    }
                    if (parentPath != null)
                    {
                        var lambdaExpression = methodCallExpression.Arguments[1] as LambdaExpression;
                        if (lambdaExpression != null)
                        {
                            string subPath;
                            if (!TryParsePath(lambdaExpression.Body, out subPath))
                            {
                                return false;
                            }
                            if (subPath != null)
                            {
                                path = parentPath + "." + subPath;
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        private static Expression RemoveConvert(this Expression expression)
        {
            while ((expression != null) && ((expression.NodeType == ExpressionType.Convert) || (expression.NodeType == ExpressionType.ConvertChecked)))
            {
                expression = ((UnaryExpression)expression).Operand.RemoveConvert();
            }
            return expression;
        }

       // #endregion Include
    }
}
