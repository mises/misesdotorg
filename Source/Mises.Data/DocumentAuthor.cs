using System;

namespace Mises.Data
{
    public partial class DocumentAuthor
    {
        public override String ToString()
        {
            return (AuthorFirst + " " + AuthorMiddle + " " + AuthorLast).Trim();
        }
    }
}