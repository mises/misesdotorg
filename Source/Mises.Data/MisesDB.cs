﻿#region

using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

#endregion

namespace Mises.Data
{
    internal class MisesDB
    {
    }

    public partial class MisesDBDataContext
    {
        [Function(Name = "dbo.GlobalGetStatistics")]
        [ResultType(typeof (GlobalGetStatisticsResult))]
        [ResultType(typeof (RevisionGetRecentResult))]
        public IMultipleResults GlobalGetStatisticsMultipleRS(
            [Parameter(Name = "ShowPrivateData", DbType = "Bit")] bool? showPrivateData)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo) (MethodBase.GetCurrentMethod())),
                                                      showPrivateData);
            return ((IMultipleResults) (result.ReturnValue));
        }

        

        //[global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.DocumentDetails")]
        //public IMultipleResults DocumentDetailsMultipleRS([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "DocumentId", DbType = "Int")] System.Nullable<int> documentId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "MediaId", DbType = "Int")] System.Nullable<int> mediaId)
        //{
        //    IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), documentId, mediaId);
        //    return (IMultipleResults) ((result.ReturnValue));
        //}
    }

    public partial class MediaDocument
    {

        private int _DocumentId;

        private System.Guid _GUID;

        private bool _Display;

        private System.Nullable<bool> _Featured;

        private string _Title;

        private int _Author1;

        private System.Nullable<int> _Author2;

        private string _PubInfo;

        private string _Description;

        private string _metaKeywords;

        private System.Data.Linq.Binary _metaImage;

        private System.Nullable<bool> _FullText;

        private string _Source;

        private string _GuideContent;

        private System.Nullable<int> _Length;

        private System.Nullable<int> _CategoryId;

        private System.DateTime _CreateDate;

        private System.DateTime _EditDate;

        private string _Author;

        private string _CoAuthor;

        private string _URL;

        public MediaDocument()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_DocumentId", DbType = "Int NOT NULL")]
        public int DocumentId
        {
            get
            {
                return this._DocumentId;
            }
            set
            {
                if ((this._DocumentId != value))
                {
                    this._DocumentId = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_GUID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid GUID
        {
            get
            {
                return this._GUID;
            }
            set
            {
                if ((this._GUID != value))
                {
                    this._GUID = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Display", DbType = "Bit NOT NULL")]
        public bool Display
        {
            get
            {
                return this._Display;
            }
            set
            {
                if ((this._Display != value))
                {
                    this._Display = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Featured", DbType = "Bit")]
        public System.Nullable<bool> Featured
        {
            get
            {
                return this._Featured;
            }
            set
            {
                if ((this._Featured != value))
                {
                    this._Featured = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Title", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string Title
        {
            get
            {
                return this._Title;
            }
            set
            {
                if ((this._Title != value))
                {
                    this._Title = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Author1", DbType = "Int NOT NULL")]
        public int Author1
        {
            get
            {
                return this._Author1;
            }
            set
            {
                if ((this._Author1 != value))
                {
                    this._Author1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Author2", DbType = "Int")]
        public System.Nullable<int> Author2
        {
            get
            {
                return this._Author2;
            }
            set
            {
                if ((this._Author2 != value))
                {
                    this._Author2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_PubInfo", DbType = "VarChar(MAX)")]
        public string PubInfo
        {
            get
            {
                return this._PubInfo;
            }
            set
            {
                if ((this._PubInfo != value))
                {
                    this._PubInfo = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Description", DbType = "VarChar(MAX)")]
        public string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                if ((this._Description != value))
                {
                    this._Description = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_metaKeywords", DbType = "VarChar(350)")]
        public string metaKeywords
        {
            get
            {
                return this._metaKeywords;
            }
            set
            {
                if ((this._metaKeywords != value))
                {
                    this._metaKeywords = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_metaImage", DbType = "Image", CanBeNull = true)]
        public System.Data.Linq.Binary metaImage
        {
            get
            {
                return this._metaImage;
            }
            set
            {
                if ((this._metaImage != value))
                {
                    this._metaImage = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_FullText", DbType = "Bit")]
        public System.Nullable<bool> FullText
        {
            get
            {
                return this._FullText;
            }
            set
            {
                if ((this._FullText != value))
                {
                    this._FullText = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Source", DbType = "VarChar(100)")]
        public string Source
        {
            get
            {
                return this._Source;
            }
            set
            {
                if ((this._Source != value))
                {
                    this._Source = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_GuideContent", DbType = "VarChar(MAX)")]
        public string GuideContent
        {
            get
            {
                return this._GuideContent;
            }
            set
            {
                if ((this._GuideContent != value))
                {
                    this._GuideContent = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Length", DbType = "Int")]
        public System.Nullable<int> Length
        {
            get
            {
                return this._Length;
            }
            set
            {
                if ((this._Length != value))
                {
                    this._Length = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_CategoryId", DbType = "Int")]
        public System.Nullable<int> CategoryId
        {
            get
            {
                return this._CategoryId;
            }
            set
            {
                if ((this._CategoryId != value))
                {
                    this._CategoryId = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_CreateDate", DbType = "DateTime NOT NULL")]
        public System.DateTime CreateDate
        {
            get
            {
                return this._CreateDate;
            }
            set
            {
                if ((this._CreateDate != value))
                {
                    this._CreateDate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_EditDate", DbType = "DateTime NOT NULL")]
        public System.DateTime EditDate
        {
            get
            {
                return this._EditDate;
            }
            set
            {
                if ((this._EditDate != value))
                {
                    this._EditDate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Author", DbType = "VarChar(252)")]
        public string Author
        {
            get
            {
                return this._Author;
            }
            set
            {
                if ((this._Author != value))
                {
                    this._Author = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_CoAuthor", DbType = "VarChar(252)")]
        public string CoAuthor
        {
            get
            {
                return this._CoAuthor;
            }
            set
            {
                if ((this._CoAuthor != value))
                {
                    this._CoAuthor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_URL", DbType = "VarChar(MAX)")]
        public string URL
        {
            get
            {
                return this._URL;
            }
            set
            {
                if ((this._URL != value))
                {
                    this._URL = value;
                }
            }
        }
    }


    public partial class MediaFileDBResult
	{
		
		private int _MediaId;
		
		private int _DocumentId;
		
		private int _MediaTypeId;
		
		private string _URL;
		
		private long _fileSize;
		
		private decimal _duration;
		
		private System.DateTime _CreateDate;
		
		private System.Nullable<bool> _IsMedia;
		
		private System.Nullable<int> _MediaTypeId1;
		
		private string _Description;
		
		private string _MediaIconPath;

		private byte _VolumeOrdinal;

		private string _VolumeComment;

        public MediaFileDBResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MediaId", DbType="Int NOT NULL")]
		public int MediaId
		{
			get
			{
				return this._MediaId;
			}
			set
			{
				if ((this._MediaId != value))
				{
					this._MediaId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DocumentId", DbType="Int NOT NULL")]
		public int DocumentId
		{
			get
			{
				return this._DocumentId;
			}
			set
			{
				if ((this._DocumentId != value))
				{
					this._DocumentId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MediaTypeId", DbType="Int NOT NULL")]
		public int MediaTypeId
		{
			get
			{
				return this._MediaTypeId;
			}
			set
			{
				if ((this._MediaTypeId != value))
				{
					this._MediaTypeId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_URL", DbType="VarChar(500) NOT NULL", CanBeNull=false)]
		public string URL
		{
			get
			{
				return this._URL;
			}
			set
			{
				if ((this._URL != value))
				{
					this._URL = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fileSize", DbType="BigInt NOT NULL")]
		public long fileSize
		{
			get
			{
				return this._fileSize;
			}
			set
			{
				if ((this._fileSize != value))
				{
					this._fileSize = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_duration", DbType="Decimal(18,0) NOT NULL")]
		public decimal duration
		{
			get
			{
				return this._duration;
			}
			set
			{
				if ((this._duration != value))
				{
					this._duration = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreateDate", DbType="DateTime NOT NULL")]
		public System.DateTime CreateDate
		{
			get
			{
				return this._CreateDate;
			}
			set
			{
				if ((this._CreateDate != value))
				{
					this._CreateDate = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IsMedia", DbType="Bit")]
		public System.Nullable<bool> IsMedia
		{
			get
			{
				return this._IsMedia;
			}
			set
			{
				if ((this._IsMedia != value))
				{
					this._IsMedia = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MediaTypeId1", DbType="Int")]
		public System.Nullable<int> MediaTypeId1
		{
			get
			{
				return this._MediaTypeId1;
			}
			set
			{
				if ((this._MediaTypeId1 != value))
				{
					this._MediaTypeId1 = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Description", DbType="VarChar(MAX)")]
		public string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				if ((this._Description != value))
				{
					this._Description = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MediaIconPath", DbType="VarChar(255)")]
		public string MediaIconPath
		{
			get
			{
				return this._MediaIconPath;
			}
			set
			{
				if ((this._MediaIconPath != value))
				{
					this._MediaIconPath = value;
				}
			}
		}

		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_VolumeOrdinal", DbType = "tinyint")]
		public byte VolumeOrdinal
		{
			get
			{
				return _VolumeOrdinal;
			}
			set
			{
				if ((_VolumeOrdinal != value))
				{
					this._VolumeOrdinal = value;
				}
			}
		}

		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_VolumeComment", DbType = "varchar(50)")]
		public string VolumeComment
		{
			get
			{
				return _VolumeComment;
			}
			set
			{
				if ((_VolumeComment != value))
				{
					_VolumeComment = value;
				}
			}
		}
	}


    
}