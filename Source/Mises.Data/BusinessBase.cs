#region

using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Web.Configuration;
using MvcMiniProfiler.Data;

#endregion

namespace Mises.Data
{
    ///<summary>
    ///  Base Business Class to Inherit
    ///</summary>
    ///<remarks>
    ///</remarks>
    [Serializable]
    [DataContract]
    public abstract class BusinessBase : DataFormat
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["Public"].ConnectionString; }
        }

        public static MisesModel EnityDataModel
        {
            get
            {
                var connectionStringsSection =
                    WebConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;

                DbConnection conn = ProfiledDbConnection.Get(
                    new EntityConnection(connectionStringsSection.ConnectionStrings["MisesEntities"].ConnectionString));

                return new MisesModel((EntityConnection) conn);
            }
        }

        ///<summary>
        ///  Executes query and returns a single object as the result.
        ///</summary>
        ///<param name = "commandText">The command text.</param>
        ///<param name = "commandParameters">
        ///  The command parameters.
        ///</param>
        ///<returns></returns>
        public static object ExecuteScalar(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, commandText, commandParameters);
        }


        ///<summary>
        ///  Executes the query and returns a dataset.
        ///</summary>
        ///<param name = "commandText">The command text.</param>
        ///<param name = "commandParameters">
        ///  The command parameters.
        ///</param>
        ///<returns></returns>
        public static DataSet ExecuteDataset(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, commandText, commandParameters);
        }

        public static DataSet ExecuteDatasetSP(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, commandText,
                                            commandParameters);
        }

        ///<summary>
        ///  Executes the a query and returns the number of rows changed
        ///</summary>
        ///<param name = "commandText">The command text.</param>
        ///<param name = "commandParameters">
        ///  The command parameters.
        ///</param>
        ///<returns></returns>
        public static int ExecuteNonQuery(string commandText, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, commandText, commandParameters);
        }
    }
}