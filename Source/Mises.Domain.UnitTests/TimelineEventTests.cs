﻿#region

using System.Collections.Generic;
using Mises.Domain.Timeline;
using NUnit.Framework;

#endregion

namespace Mises.Domain.UnitTests
{
    [TestFixture]
    public class TimelineEventTests
    {
        [Test]
        public void ShouldNotAddAttributesForEmptyStringParametersExceptStart()
        {
            //arrange
            var testList = new List<TimeLineEvent>
                               {
                                   new TimeLineEvent
                                       {
                                           Born = "",
                                           Died = "",
                                           Image = "",
                                           Text = "",
                                           Title = ""
                                       }
                               };

            //act
            var actualResult = TimeLineEvent.GetXML(testList);
            var expectedResult = "<data><event start=\"\" isDuration=\"false\"></event></data>";

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void ShouldProduceXmlRepresentationofTimeLineEventList()
        {
            //arrange
            var testList = new List<TimeLineEvent>
                               {
                                   new TimeLineEvent
                                       {
                                           Born = "November",
                                           Died = "September",
                                           Image = "noImage.jpg",
                                           Text = "Test Text",
                                           Title = "Test Title"
                                       }
                               };

            //act
            var actualResult = TimeLineEvent.GetXML(testList);
            var expectedResult =
                "<data><event start=\"November\" end=\"September\" isDuration=\"true\" title=\"Test Title\" image=\"noImage.jpg\">Test Text</event></data>";

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void ShouldSetDurationToFalseWhenDiedDateIsNullOrEmpty()
        {
            //arrange
            var testList = new List<TimeLineEvent>
                               {
                                   new TimeLineEvent {Died = null},
                                   new TimeLineEvent {Died = ""}
                               };

            //act
            var actualResult = TimeLineEvent.GetXML(testList);
            var expectedResult =
                "<data><event start=\"\" isDuration=\"false\"></event><event start=\"\" isDuration=\"false\"></event></data>";

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}