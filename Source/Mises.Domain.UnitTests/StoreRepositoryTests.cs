﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.Domain.Contracts;
using Moq;

namespace Mises.Domain.UnitTests
{
    [TestClass]
   public class StoreRepositoryTests
    {

        [TestMethod]
        public void GetInvalidBook_Returns_Null()
        {
            //var mockRepository = new Mock<IStoreRepository>();
            //mockRepository.Setup(m => m.GetBookByProductId()).Returns(new Document() { DocumentId = 66, Title = "Test" });

            var book = StoreRepository.GetBookByProductId(10395);
            Assert.IsNull(book);

        }

    }
}
