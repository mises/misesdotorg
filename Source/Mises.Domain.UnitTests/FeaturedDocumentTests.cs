﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mises.Data;
using Mises.Domain.Media;
using NUnit.Framework;

namespace Mises.Domain.UnitTests
{

    [TestFixture]
    public class DataTests
    {

        [Test]
        public void ShouldDoSomethingOtherThan404()
        {
            FeaturedDocument d = new FeaturedDocument();
            List<DocumentDTO> documents = d.FeaturedDocuments;
            Assert.IsTrue(documents.Count > 0);
        }
        [Test]
        public void ShouldGetAuthorsName()
        {
            FeaturedDocument d = new FeaturedDocument();
            List<DocumentDTO> documents = d.FeaturedDocuments;
            Assert.IsTrue(documents[0].Author1Name.Length > 0);
        }
        [Test]
        public void ShouldGetMediaTypeId()
        {
            FeaturedDocument d = new FeaturedDocument();
            List<DocumentDTO> documents = d.FeaturedDocuments;
            Assert.IsTrue(documents[0].MediaTypeId > 0);
        }
        [Test]
        public void ShouldGetUrl()
        {
            FeaturedDocument d = new FeaturedDocument();
            List<DocumentDTO> documents = d.FeaturedDocuments;
            Assert.IsTrue(documents[0].URL.Length > 0);
        }
    }
}
