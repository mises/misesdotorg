﻿#region

using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Web;
using Mises.Data;


#endregion

namespace Mises.Domain.Utility
{
    public class ExceptionLogging
    {
        public static bool LoggingException;
        protected HttpRequestBase Request { get; set; }
        public HttpContext context;
        public Exception ex;

        public static void LogException(HttpContextBase httpContext, Exception exception)
        {
            HttpApplication app = (HttpApplication)httpContext.GetService(typeof(HttpApplication));
            LogException(app.Context, exception);
        }

        /// <summary>
        ///   Logs a web exception
        /// </summary>
        /// <param name = "httpContext">The HTTP context.</param>
        /// <param name = "exception">The exception.</param>
        public static void LogException(HttpContext httpContext, Exception exception)
        {
            try
            {
                if (httpContext.Request.Url.Host == "alpha.mises.org" || httpContext.Request.Url.Host == "beta.mises.org") return;

                if (exception.StackTrace != null)
                    if (exception.StackTrace.Contains("System.Web.Compilation.BuildManager.GetVPathBuildResultInternal")
                        ||
                        exception.StackTrace.Contains("AuthorHeaderImage")
                        ||
                        httpContext.Request.Url.ToString().Contains("cgi-bin")
                        ||
                        exception.StackTrace.Contains("HttpException")
                        ||
                        exception.Message.Contains("does not implement IController")
                        ||
                        exception.StackTrace.Contains("ThreadAbortException")
                        ||
                        exception.StackTrace.Contains("FromBase64_Decode")
                        ||
                        exception.Message.Contains("Exception has been thrown by the target of an invocation")
                        ||
                        exception.Message.Contains("Server cannot set status after HTTP headers have been sent")
                        ||
                        exception.Message.Contains("Specified file does not exist:")
                        ||
                        exception.ToString().Contains("The length of the URL for this request exceeds the configured maxUrlLength value.")
                        ||
                        exception.ToString().Contains("The input is not a valid Base-64 string")
                        ||
                        exception.StackTrace.Contains(
                            "A network-related or instance-specific error occurred while establishing a connection to SQL Server")
                        )
                        return;

                if (exception.InnerException != null)
                    exception = exception.InnerException;

                var logger = new ExceptionLogging { ex = exception, context = httpContext };
                logger.LogException();


                //SendExceptionEmail(exception, httpContext.Request);
            }
            catch
            {
            }
        }

        /// <summary>
        ///   Logs a console exception
        /// </summary>
        /// <param name = "assembly">The assembly.</param>
        /// <param name = "exception">The exception.</param>
        public static void LogException(Assembly assembly, Exception exception)
        {
            try
            {
                string stringToHash = Process.GetCurrentProcess().MachineName + assembly.FullName + exception.Message;
                string hashCode = stringToHash.GetHashCode().ToString();

                var db =
                    new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));


                db.Exceptions_Log(hashCode,
                                  Process.GetCurrentProcess().MachineName,
                                  exception.Message,
                                  exception.ToString(),
                                  assembly.CodeBase,
                                  string.Empty,
                                  string.Empty,
                                  string.Empty,
                                  assembly.Location);
            }
            catch (Exception)
            {
            }
        }

        public static void LogHttpError(HttpContextBase httpContext, HttpRequestBase request)
        {
            HttpApplication app = (HttpApplication)httpContext.GetService(typeof(HttpApplication));

            var logger = new ExceptionLogging { context = app.Context, Request = request };

            logger.LogException();
        }

        public static void SendExceptionEmail(Exception exception, HttpRequest Request)
        {


            string errorMessage = "Error in " + Request.Url + Environment.NewLine +
                                  Environment.NewLine;

            if (exception.InnerException != null)
            {
                exception = exception.InnerException;
                errorMessage += "Inner exception:";
            }
            else
            {
                errorMessage += "Outer exception:";
            }

            if (exception.Message == "" || !(Request.Url.ToString().Contains("mises.org")) ||
                exception.Message.StartsWith("The file") ||
                exception.Message.StartsWith("Invalid length for a Base-64 char array") ||
                exception.Message.StartsWith("Exception of type 'System.Web.HttpUnhandledException' was thrown") ||
                exception.Message.StartsWith("File does not exist") ||
                exception.Message.StartsWith("This is an invalid script resource request") ||
                exception.Message.Contains("webresource") ||
                Request.Url.ToString().Contains("CuteEditor") ||
                exception.Message.Contains("Content/telerik.") ||
                exception.Message.Contains("was not found on controller 'Mises.Web.Controllers") ||
                exception.Message.Contains("Exception has been thrown by the target of an invocation") ||
                exception.Message.StartsWith("A potentially dangerous Request") || exception.Message.StartsWith("Path") ||
                exception.Message.StartsWith("The specified path, file name, or both are too long") ||
                exception.Message.StartsWith("Overflow") || exception.Message.StartsWith("Padding is invalid") ||
                exception.Message.StartsWith("Invalid postback or callback argument") ||
                exception.Message.StartsWith("Invalid viewstate") ||
                exception.Message.StartsWith("Illegal characters in path") || exception.Message.Contains("1.0.61025.0") ||
                exception.Message.Contains("Input string was not in a correct format") ||
                exception.Message.Contains("There was no channel actively listening") ||
                exception.Message.Contains("Cannot use a leading .. "))
            {
                return;
            }

            errorMessage += exception.Message + Environment.NewLine + Environment.NewLine;
            errorMessage += exception.StackTrace + Environment.NewLine + Environment.NewLine;
            errorMessage += exception.Source + Environment.NewLine;

            EmailHelper.SendMail("errors@freecapitalists.org", "heroic@gmail.com",
                                 "Mises.org Error :" + Request.Url, errorMessage);
        }

        #region Private Methods

        private void LogException()
        {
            if (LoggingException) return; // prevent error flooding
            var db = DataHelper.MisesDataContext;
            try
            {
                LoggingException = true;

                if (Request != null)
                {
                    string url = Request.RawUrl.Replace("/Error/Http404/?aspxerrorpath=", "");

                    db.Exceptions_Log(GetExceptionHashCode(),
                  context.Server.MachineName,
                  url,
                  "404",
                  context.Request.UserAgent,
                  context.Request.GetOriginalHostAddress(),
                  context.Request.UrlReferrer != null
                      ? context.Request.UrlReferrer.ToString()
                      : string.Empty,
                  context.Request.HttpMethod,
                  context.Request.RawUrl);
                }
                else
                {
                    db.Exceptions_Log(GetExceptionHashCode(),
                                      context.Server.MachineName,
                                      ex.Message,
                                      ex.ToString(),
                                      context.Request.UserAgent,
                                      context.Request.GetOriginalHostAddress(),
                                      context.Request.UrlReferrer != null
                                          ? context.Request.UrlReferrer.ToString()
                                          : string.Empty,
                                      context.Request.HttpMethod,
                                      context.Request.RawUrl);

                }


                Thread.Sleep(1000);
            }
            catch (Exception)
            {
                Thread.Sleep(5000);

                db.Exceptions_Log(GetExceptionHashCode(),
                                  context.Server.MachineName,
                                  ex.Message,
                                  ex.ToString(),
                                  context.Request.UserAgent,
                                  context.Request.GetOriginalHostAddress(),
                                  context.Request.UrlReferrer != null
                                      ? context.Request.UrlReferrer.ToString()
                                      : string.Empty,
                                  context.Request.HttpMethod,
                                  context.Request.RawUrl);
            }
            finally
            {
                LoggingException = false;
            }
        }


        private string GetExceptionHashCode()
        {
            string stringToHash = context.Server.MachineName + context.Request.RawUrl;
            if (ex != null)
            {
                stringToHash += ex.Message;
            }
            return stringToHash.GetHashCode().ToString();
        }

        #endregion Private Methods
    }
}