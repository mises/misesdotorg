#region

using System;
using Tidy;

#endregion

namespace Mises.Domain.Utility
{
    public class HTMLtidy
    {
        private static void UpdateStatus(int status)
        {
            Console.Write("" + '\t' + "done. (");
            Console.Write(status);
            Console.WriteLine(").");

            //  status = CInt(status & status & vbCrLf)
        }

        //[STAThread]
        public static string TidyHTML(string inputHTML)
        {
            int status = 0;
            string outputHTML = "";

            Console.Write("Creating document object ...");
            var tdoc = new Document();
            Console.WriteLine("" + '\t' + "done.");

            // http://hpux.cs.utah.edu/hppd/cgi-bin/wwwtar?/hpux/Networking/WWW/tidy-20080220/tidy-20080220-hppa-11.11.depot.gz+tidy/tidy-RUN/usr/local/include/tidyenum.h+text
            tdoc.SetOptValue(TidyOptionId.TidyIndentSpaces, Convert.ToString(4));
            tdoc.SetOptValue(TidyOptionId.TidyIndentContent, "auto");
            tdoc.SetOptValue(TidyOptionId.TidyMark, "no");
            tdoc.SetOptValue(TidyOptionId.TidyBodyOnly, "yes");
            //tdoc.SetOptValue(TidyOptionId.TidyMakeClean, "yes")
            tdoc.SetOptValue(TidyOptionId.TidyLogicalEmphasis, "yes");
            tdoc.SetOptValue(TidyOptionId.TidyXhtmlOut, "yes");
            tdoc.SetOptValue(TidyOptionId.TidyWord2000, "yes");
            tdoc.SetOptValue(TidyOptionId.TidyDropPropAttrs, "yes");
            tdoc.SetOptValue(TidyOptionId.TidyDropFontTags, "yes");
            tdoc.SetOptValue(TidyOptionId.TidyEncloseBodyText, "yes");
            tdoc.SetOptValue(TidyOptionId.TidyWrapLen, "120");

            if (status >= 0)
            {
                Console.Write("parse file...");
                status = tdoc.ParseString(inputHTML);
                //UpdateStatus(status)
            }

            if (status >= 0)
            {
                Console.Write("cleaning file...");
                status = tdoc.CleanAndRepair();
                //UpdateStatus(status)
            }

            if (status >= 0)
            {
                Console.Write("running diagnostics...");
                status = tdoc.RunDiagnostics();
                //UpdateStatus(status)
            }

            if (status > 1)
            {
                tdoc.SetOptBool(TidyOptionId.TidyForceOutput, 1);
            }

            if (status >= 0)
            {
                Console.Write("saving file...");
                outputHTML = tdoc.SaveString();
                //UpdateStatus(status)
            }

            return outputHTML;
        }
    }
}