#region

using Joel.Net;

#endregion

namespace Mises.Domain.Utility
{
    public class SpamCheck
    {
        public string Author;
        public string CommentText;
        public string Email;
        public string IP;
        public bool IsSpam;
        public string URL;
        public string UserAgent;

        public SpamCheck(string author, string commentText, string email, string ip)
        {
            Author = author;
            CommentText = commentText;
            Email = email;
            IP = ip;
        }

        public bool CheckComment()
        {
            //TODO: FIX to relative URL ?
            const string URL = "https://mises.org";
            const string apiKey = "902c41f16c2e";
            var api = new Akismet(apiKey, URL, "Test/1.0");
            var comment = new AkismetComment
                              {
                                  Blog = URL,
                                  CommentAuthor = Author,
                                  CommentAuthorEmail = Email,
                                  CommentAuthorUrl = URL,
                                  CommentContent = CommentText,
                                  CommentType = "comment",
                                  UserAgent = UserAgent,
                                  UserIp = IP
                              };

            IsSpam = api.CommentCheck(comment);


            return IsSpam;
        }
    }
}