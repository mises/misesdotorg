#region

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using Mises.Data;

#endregion

namespace Mises.Domain.Utility
{
    public static class Imaging
    {
        /// <summary>
        ///   Creates the proportional image thumbnail.
        /// </summary>
        /// <param name = "ImageURL">The image URL.</param>
        /// <param name = "thumbURL">The thumb URL.</param>
        /// <param name = "width">The width.</param>
        /// <param name = "height">The height.</param>
        /// <param name = "context">The context.</param>
        public static void CreateProportionalImageThumbnail(string ImageURL, string thumbURL, int width, int height,
                                                            HttpContext context)
        {
            CreateProportionalImageThumbnail(ImageURL, thumbURL, width, height, context, true);
        }

        public static void CreateProportionalImageThumbnail(string ImageURL, string thumbURL, int width, int height,
                                                            HttpContext context, bool ConstrainProportions)
        {
            string thumbPath = context.Server.MapPath(thumbURL);
            CreateProportionalImageThumbnail(ImageURL, thumbPath, width, height, ConstrainProportions);
        }

        public static void CreateProportionalImageThumbnail(string ImageURL, string thumbPath, int width, int height,
                                                            bool ConstrainProportions)
        {
            Bitmap originalImage = GetImageFromURL(ImageURL);
            Image thumbnailImage = null;

            if (ConstrainProportions)
            {
                if (width == 0)
                {
                    width = originalImage.Width;
                }
                if (height == 0)
                {
                    height = originalImage.Height;
                }
                thumbnailImage = resizeImage(originalImage, new Size(width, height));
            }
            else
            {
                Bitmap myBitMap = GetImageFromURL(ImageURL);
                thumbnailImage = CreateReducedImage(myBitMap, new Size(width, height));

                //Image.GetThumbnailImageAbort myCallBack = null;
                //thumbnailImage = myBitMap.GetThumbnailImage(width, height, myCallBack, IntPtr.Zero);
            }

            try
            {
                if (!File.Exists(thumbPath))
                {
                    //  File.Delete(thumbPath);

                    var myEncoderParameters = new EncoderParameters(1);

                    Encoder myEncoder = Encoder.Quality;
                    var myEncoderParameter = new EncoderParameter(myEncoder, 65L);

                    myEncoderParameters.Param[0] = myEncoderParameter;

                    ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                    thumbnailImage.Save(thumbPath, jpgEncoder, myEncoderParameters);
                }
            }
            catch
            {
                // Error updating thumbnail, so give up
            }
        }

        public static void CreateProportionalImageThumbnail(Bitmap originalImage, string thumbPath)
        {
            Image thumbnailImage = null;

            {
                const int width = 170;
                const int height = 130;

                thumbnailImage = resizeImage(originalImage, new Size(width, height));
            }


            try
            {
                if (!File.Exists(thumbPath))
                {
                    //  File.Delete(thumbPath);

                    var myEncoderParameters = new EncoderParameters(1);

                    Encoder myEncoder = Encoder.Quality;
                    var myEncoderParameter = new EncoderParameter(myEncoder, 65L);

                    myEncoderParameters.Param[0] = myEncoderParameter;

                    ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                    thumbnailImage.Save(thumbPath, jpgEncoder, myEncoderParameters);
                }
            }
            catch
            {
                // Error updating thumbnail, so give up
            }
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            return codecs.FirstOrDefault(codec => codec.FormatID == format.Guid);
        }

        /// <summary>
        ///   Resizes the image.
        /// </summary>
        /// <param name = "imgToResize">The img to resize.</param>
        /// <param name = "size">The size.</param>
        /// <returns></returns>
        public static Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0F;
            float nPercentW = 0F;
            float nPercentH = 0F;

            nPercentW = (Convert.ToSingle(size.Width) / Convert.ToSingle(sourceWidth));
            nPercentH = (Convert.ToSingle(size.Height) / Convert.ToSingle(sourceHeight));

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
            }
            else
            {
                nPercent = nPercentW;
            }

            int destWidth = Convert.ToInt32((sourceWidth * nPercent));
            int destHeight = Convert.ToInt32((sourceHeight * nPercent));

            try
            {
                return (ImageResize(imgToResize, destHeight, destWidth));
            }
            catch
            {
                var b = new Bitmap(destWidth, destHeight);
                Graphics g = Graphics.FromImage(b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                g.Dispose();
                return b;
            }
        }


        /// <summary>
        ///   Resize image with GDI+  so that image is nice and clear with required size.
        /// </summary>
        /// <param name = "image">Original image</param>
        /// <param name = "height">Output image height</param>
        /// <param name = "width">Output image width</param>
        /// <returns>New image with the specified size</returns>
        /// <remarks>
        /// </remarks>
        /// <history>
        ///   [Mital.Kakaiya] 27/05/2005 Created
        /// </history>
        public static Image ImageResize(Image image, Int32 height, Int32 width)
        {
            var bitmap = new Bitmap(width, height, image.PixelFormat);
            if (bitmap.PixelFormat == PixelFormat.Format1bppIndexed ||
                bitmap.PixelFormat == PixelFormat.Format4bppIndexed ||
                bitmap.PixelFormat == PixelFormat.Format8bppIndexed || bitmap.PixelFormat == PixelFormat.Undefined ||
                bitmap.PixelFormat == PixelFormat.DontCare || bitmap.PixelFormat == PixelFormat.Format16bppArgb1555 ||
                bitmap.PixelFormat == PixelFormat.Format16bppGrayScale)
            {
                //More Info http://msdn.microsoft.com/library/default.asp?_
                //url=/library/en-us/cpref/html/frlrfSystemDrawingGraphicsClassFromImageTopic.asp
                throw new NotSupportedException("Pixel format of the image is not supported.");
            }
            Graphics graphicsImage = Graphics.FromImage(bitmap);
            graphicsImage.SmoothingMode = SmoothingMode.HighQuality;
            graphicsImage.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphicsImage.DrawImage(image, 0, 0, bitmap.Width, bitmap.Height);
            graphicsImage.Dispose();
            return bitmap;
        }

        //'''To call this function
        //Dim image As Image = ImageResize(bigImage, smallHeight, smallWidth)


        /// <summary>
        ///   Gets the image from URL.
        /// </summary>
        /// <param name = "url">The URL.</param>
        /// <returns></returns>
        public static Bitmap GetImageFromURL(string url)
        {
            url = DataFormat.GetAbsoluteURL(url);

            Bitmap image;
            var client = new WebClient();
            using (Stream stream = client.OpenRead(url))
            {
                image = new Bitmap(stream);
                stream.Close();
            }
            return image;
        }


        //Dim thumbnail As Bitmap = MisesWeb.Imaging.GetImageFromURL(ImageURL)
        //Dim info() As System.Drawing.Imaging.ImageCodecInfo = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders
        //Dim params As System.Drawing.Imaging.EncoderParameters = New System.Drawing.Imaging.EncoderParameters(2)
        //params.Param(0) = New EncoderParameter(Encoder.Quality, 100L)
        //params.Param(1) = New EncoderParameter(Encoder.Compression, 76)
        //thumbnail.Save(thumbPath, info(1), params)
        /// <summary>
        /// Saves the image from URL.
        /// </summary>
        /// <param name="imageUrl">The image URL.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="fileName">Name of the file.</param>
        public static void SaveImageFromUrl(string imageUrl, string filePath, string fileName = "")
        {
            if (fileName == "")
            {
                int s = imageUrl.LastIndexOf("/") + 1;
                if (s < 0) s = 0;
                fileName = imageUrl.Substring(s);
            }

            if (System.IO.File.Exists(DataHelper.WebsiteFolder  + @"images\archived\" + fileName))
            {
                System.IO.File.Copy(DataHelper.WebsiteFolder  + @"images\archived\" + fileName, DataHelper.WebsiteFolder  + @"images\MisesDaily\" + fileName);
                return;
            }

            if (System.IO.File.Exists(DataHelper.WebsiteFolder  + @"MisesDaily\" + fileName))
            {
                return;
            }



            Bitmap myBitMap = GetImageFromURL(imageUrl);

            if (imageUrl.EndsWith(".jpg"))
            {
                myBitMap.Save(filePath, ImageFormat.Jpeg);    
            }
            else if (imageUrl.EndsWith(".png") || imageUrl.EndsWith(".gif"))
            {
                myBitMap.Save(filePath, ImageFormat.Png);
            }
            else
            {
                myBitMap.Save(filePath, ImageFormat.Jpeg);    
            }
            



            //var myEncoderParameters = new EncoderParameters(1);

            //Encoder myEncoder = Encoder.Quality;
            //var myEncoderParameter = new EncoderParameter(myEncoder, 65L);

            //myEncoderParameters.Param[0] = myEncoderParameter;

            //ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            //thumbnailImage.Save(thumbPath, jpgEncoder, myEncoderParameters);
        }

        //public static void CreateProportionalImageThumbnail(Stream fileContent, int articleId)
        //{
        //    throw new NotImplementedException();
        //}


        [DllImport("Kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern bool CreateHardLink(
            string lpfileName,
            string lpExistingfileName,
            IntPtr lpSecurityAttributes
            );


        public static Stream ToStream(this Image image, ImageFormat formaw)
        {
            var stream = new MemoryStream();
            image.Save(stream, formaw);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// High quality resize
        /// </summary>
        /// <param name="imgOrig">The img orig.</param>
        /// <param name="newSize">The new size.</param>
        /// <returns></returns>
        public static Image CreateReducedImage(Image imgOrig, Size newSize)
        {
            var newBm = new Bitmap(newSize.Width, newSize.Height);
            using (var newGrapics = Graphics.FromImage(newBm))
            {
                newGrapics.CompositingQuality = CompositingQuality.HighSpeed;
                newGrapics.SmoothingMode = SmoothingMode.HighSpeed;
                newGrapics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                newGrapics.DrawImage(imgOrig, new Rectangle(0, 0, newSize.Width, newSize.Height));
            }

            return newBm;
        }
    }
}