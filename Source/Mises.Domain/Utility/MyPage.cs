#region

using System;
using System.Web;
using System.Web.UI;

#endregion

namespace Mises.Domain.Utility
{
    public class MyPage : Page
    {
        public MyPage()
        {
            PreInit += Page_PreInit;
            Load += Page_Load;
        }

        private void Page_PreInit(Object sender, EventArgs e)
        {
            string host = Context.Request.Url.Host;

            if (host == "mobile.mises.org" || host == "m.mises.org")
            {
                MasterPageFile = "~/MasterPages/Mobile.master";
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            // CompressionHelper.GZipEncodePage();
        }

        public static bool IsMobileUserAgent()
        {
            string userAgent = HttpContext.Current.Request.UserAgent;
            return userAgent != null &&
                   (userAgent.Contains("Mobile") || userAgent.Contains("Android") || userAgent.Contains("iPhone") ||
                    userAgent.Contains("iPad"));
        }

        public static string ConvertToMobileUrl(string fileURL)
        {
            if (IsMobileUserAgent())
            {
                if (fileURL.EndsWith(".epub"))
                {
                    return fileURL.Replace("http://", "epub://");
                }
            }
            return fileURL;
        }
    }
}