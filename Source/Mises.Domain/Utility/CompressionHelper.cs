#region

using System.IO.Compression;
using System.Web;

#endregion

namespace Mises.Domain.Utility
{
    public class CompressionHelper
    {
        /// <summary>
        ///   Determines if GZip is supported
        /// </summary>
        /// <returns></returns>
        public static bool IsGZipSupported()
        {
            if (HttpContext.Current.Request.Url.Host == "localhost")
            {
                return false;
            }

            if (HttpContext.Current.Response.Filter.ToString() == "System.IO.Compression.GZipStream")
            {
                return false;
            }

            if (HttpContext.Current.Response.Headers["Accept-Encoding"] != null)
            {
                return false;
            }

            string AcceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];
            if (! (string.IsNullOrEmpty(AcceptEncoding)) &&
                (AcceptEncoding.Contains("gzip") || AcceptEncoding.Contains("deflate")))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///   Sets up the current page or handler to use GZip through a Response.Filter
        ///   IMPORTANT:
        ///   You have to call this method before any output is generated!
        /// </summary>
        public static void GZipEncodePage()
        {
            if (!IsGZipSupported()) return;

            HttpResponse Response = HttpContext.Current.Response;

            string AcceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];
            if (AcceptEncoding.Contains("gzip"))
            {
                Response.Filter = new GZipStream(Response.Filter, CompressionMode.Compress);
                Response.AppendHeader("Content-Encoding", "gzip");
            }
            else
            {
                Response.Filter = new DeflateStream(Response.Filter, CompressionMode.Compress);
                Response.AppendHeader("Content-Encoding", "deflate");
            }
        }
    }
}