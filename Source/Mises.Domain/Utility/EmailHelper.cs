﻿#region

using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using Mises.Data;

#endregion

namespace Mises.Domain.Utility
{
    public class EmailHelper
    {
        /// <summary>
        ///   Sends the email.
        /// </summary>
        /// <param name = "From">From.</param>
        /// <param name = "sendTo">The send to.</param>
        /// <param name = "Subject">The subject.</param>
        /// <param name = "Body">The body.</param>
        /// <param name = "IsBodyHtml">if set to <c>true</c> [is body HTML].</param>
        /// <param name = "HighPriority">if set to <c>true</c> [high priority].</param>
        /// <param name = "AttachmentFile">The attachment file.</param>
        /// <param name = "CC">The CC.</param>
        /// <param name = "BCC">The BCC.</param>
        /// <returns></returns>
        /// David
        /// 12/25/2005
        public static bool SendMail(string From, string sendTo, string Subject, string Body, bool IsBodyHtml,
                                    bool HighPriority, string AttachmentFile, string CC, string BCC)
        {
            return SendMail(From, sendTo, Subject, Body, IsBodyHtml, HighPriority, AttachmentFile, CC, BCC, "");
        }

        public static bool SendMail(string From, string sendTo, string Subject, string Body, bool IsBodyHtml,
                                    bool HighPriority, string AttachmentFile, string CC)
        {
            return SendMail(From, sendTo, Subject, Body, IsBodyHtml, HighPriority, AttachmentFile, CC, "", "");
        }

        public static bool SendMail(string From, string sendTo, string Subject, string Body, bool IsBodyHtml,
                                    bool HighPriority, string AttachmentFile)
        {
            return SendMail(From, sendTo, Subject, Body, IsBodyHtml, HighPriority, AttachmentFile, "", "", "");
        }

        public static bool SendMail(string From, string sendTo, string Subject, string Body, bool IsBodyHtml,
                                    bool HighPriority)
        {
            return SendMail(From, sendTo, Subject, Body, IsBodyHtml, HighPriority, "", "", "", "");
        }

        public static bool SendMail(string From, string sendTo, string Subject, string Body, bool IsBodyHtml)
        {
            return SendMail(From, sendTo, Subject, Body, IsBodyHtml, false, "", "", "", "");
        }

        public static bool SendMail(string From, string sendTo, string Subject, string Body)
        {
            return SendMail(From, sendTo, Subject, Body, false, false, "", "", "", "");
        }

        public static bool SendMail(string From, string sendTo, string Subject, string Body, bool IsBodyHtml,
                                    bool HighPriority, string AttachmentFile, string CC, string BCC, string SMTPServer)
        {
            try
            {
                var myMessage = new MailMessage();
                if (HighPriority)
                {
                    myMessage.Headers.Add("X-Priority", "1 (Highest)");
                }

                if (!String.IsNullOrEmpty(sendTo))
                    myMessage.To.Add(new MailAddress(sendTo));
                if (!String.IsNullOrEmpty(CC))
                {
                    myMessage.CC.Add(new MailAddress(CC));
                }
                if (!String.IsNullOrEmpty(CC))
                {
                    myMessage.Bcc.Add(new MailAddress(BCC));
                }

                if (string.IsNullOrEmpty(Subject))
                    Subject = "[No Subject]";

                Subject = DataFormat.StripHTML(Subject).Trim().Replace('\r', ' ').Replace('\n', ' ').Replace("&#39;","'")
;

                MailAddress fromAddress;
                const string displayName = "Ludwig von Mises Institute";

                if (From == "mail@freecapitalists.org" || From == "contact@freecapitalists.org")
                    fromAddress = new MailAddress(From, displayName);
                else
                {
                    fromAddress = new MailAddress(From);
                }

                myMessage.From = fromAddress;

                myMessage.Subject = Subject;
                myMessage.Body = Body;
                myMessage.IsBodyHtml = IsBodyHtml;
                if (!String.IsNullOrEmpty(AttachmentFile) && File.Exists(AttachmentFile))
                {
                    myMessage.Attachments.Add(new Attachment(AttachmentFile));
                }
                var client = new SmtpClient();
                                 //{
                                 //    Host = "127.0.0.1",
                                 //    DeliveryMethod = SmtpDeliveryMethod.Network,
                                 //    Credentials = new NetworkCredential("mail", "mdg69yo7", "mises.org"),
                                 //    UseDefaultCredentials = false,
                                 //};

                try
                {
                    client.Send(myMessage);
                }
                catch (SmtpException ex)
                {
                    ExceptionLogging.LogException(System.Reflection.Assembly.GetExecutingAssembly(), ex);
                    //client.Host = "174.132.65.90";
                    //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //client.Credentials = new NetworkCredential("mail@freecapitalists.org", "mdg69yo7", "mises.org");
                    //client.Send(myMessage);
                }

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        ///   Sends the BCC mail.
        /// </summary>
        /// <param name = "From">From.</param>
        /// <param name = "MailTo">The mail to.</param>
        /// <param name = "subject">The subject.</param>
        /// <param name = "MessageText">The message text.</param>
        /// <param name = "sendHtml">if set to <c>true</c> [send HTML].</param>
        /// <returns></returns>
        /// David
        /// 1/4/2006
        public static bool SendBccMail(string From, string MailTo, string subject, string MessageText, bool sendHtml)
        {
            return SendBccMail(From, MailTo, subject, MessageText, sendHtml, false);
        }

        public static bool SendBccMail(string From, string MailTo, string subject, string MessageText)
        {
            return SendBccMail(From, MailTo, subject, MessageText, false, false);
        }

        public static bool SendBccMail(string from, string MailTo, string subject, string MessageText, bool sendHtml,
                                       bool HighPriority)
        {
            try
            {
                MailAddress fromAddress;
                const string displayName = "Ludwig von Mises Institute";

                if (from == "mail@freecapitalists.org" || from == "contact@freecapitalists.org")
                    fromAddress = new MailAddress(from, displayName);
                else
                {
                    fromAddress = new MailAddress(from);
                }

                var email = new MailMessage { From = fromAddress, Subject = subject };
                if (HighPriority)
                {
                    email.Headers.Add("X-Priority", "1 (Highest)");
                }
                email.Body = MessageText;
                email.Bcc.Add(MailTo);
                email.IsBodyHtml = sendHtml;
                var client = new SmtpClient
                                 {
                                     Host = "127.0.0.1",
                                     DeliveryMethod = SmtpDeliveryMethod.Network,
                                     Credentials = new NetworkCredential("mail@freecapitalists.org", "mdg69yo7", "mises.org"),
                                     UseDefaultCredentials = false
                                 };

                client.Send(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}