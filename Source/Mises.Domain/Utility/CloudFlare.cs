﻿#region

using System.Web;

#endregion

namespace Mises.Domain.Utility
{
    /// <summary>
    /// </summary>
    public static class CloudFlare
    {
        /// <summary>
        ///   Gets the original host address when using CloudFlare.
        /// </summary>
        /// <param name = "request">The request.</param>
        /// <returns></returns>
        public static string GetOriginalHostAddress(this HttpRequest request)
        {
            return request.Headers["CF-Connecting-IP"] ?? request.UserHostAddress;
        }

        public static string GetOriginalHostAddress(HttpRequestBase request)
        {
            return request.Headers["CF-Connecting-IP"] ?? request.UserHostAddress;
        }
    }
}