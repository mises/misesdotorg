#region

using System.Data;
using System.Data.SqlClient;
using Mises.Data;

#endregion

namespace Mises.Domain.Utility
{
    public class RedirectedURL
    {
        public static string GetRedirectedURL(string requestedURL)
        {
            return
                SqlHelper.ExecuteScalar(DataHelper.ConnectionString,
                                        CommandType.StoredProcedure, "dbo.RedirectedURLGet",
                                        new SqlParameter("@RequestedURL", requestedURL)) as string;
        }
    }
}