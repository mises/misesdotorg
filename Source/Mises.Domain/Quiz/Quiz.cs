#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Mises.Data;
using Mises.Domain.Utility;

#endregion

namespace Mises.Domain.Quiz
{
    ///<summary>
    ///  Summary description for Quiz
    ///</summary>
    public class Quiz
    {
        private const string QuizIdParameter = "@QuizId";
        private const string ScoreDisplayFormat = "{0} {1}/100";
        private const string QuizIdMustBeGreaterThanZero = "QuizID must be > 0";
        private const string QuizNotFound = "Quiz not found for this QuizID";

        public string[] ABCD = new[] { "A. ", "B. ", "C. ", "D. " };
        public List<int> SelectedAnswers;

        #region Regionalized Text

        public string QuizPostLabel { get; set; }

        public string CorrectAnswerText { get; set; }
        public string IncorrectAnswerText { get; set; }

        public string ErrorText { get; set; }
        public string EmailPrompt { get; set; }

        public string ThankYouText { get; set; }
        public string YourScoreLabel { get; set; }
        public string YourAnswerLabel { get; set; }

        public string SubmitQuizLabel { get; set; }

        public string ScoreText
        {
            get { return string.Format(ScoreDisplayFormat, YourScoreLabel, Score); }
        }

        #endregion Regionalized Text

        public int QuizID { get; set; }
        public int TimesTaken { get; set; }
        public string QuizTitle { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }

        public IEnumerable<Question> Questions { get; set; }
        public IEnumerable<Answer> Answers { get; set; }

        public int Score { get; set; }

        public void GetQuiz()
        {
            if (QuizID == 0)
                throw new ArgumentOutOfRangeException(QuizIdMustBeGreaterThanZero);

            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                  "dbo.QuizGetDetails",
                                                  new SqlParameter(QuizIdParameter, QuizID));

            if (ds.Tables[0].Rows.Count == 0)
                throw new ArgumentOutOfRangeException(QuizNotFound);

            DataRow quizRow = ds.Tables[0].Rows[0];
            Language = quizRow[QuizFields.Language] as string;

            BuildQuiz(quizRow);

            Questions = new List<Question>();
            Answers = new List<Answer>();
            SelectedAnswers = new List<int>();


            DataTable questionTable = ds.Tables[1];
            Questions = BuildQuestions(questionTable);


            DataTable answerTable = ds.Tables[2];
            Answers = BuildAnswers(answerTable);
        }

        private IEnumerable<Answer> BuildAnswers(DataTable answerTable)
        {
            return answerTable.Rows.Cast<DataRow>().Select(
                row => new Answer
                           {
                               AnswerId =
                                   Convert.ToInt32(
                                       row[AnswerFields.Id].ToString()),
                               QuestionId =
                                   Convert.ToInt32(
                                       row[AnswerFields.QuestionId].ToString()),
                               AnswerValue =
                                   Convert.ToInt32(
                                       row[AnswerFields.Value].ToString()),
                               AnswerText = row[AnswerFields.Text].ToString(),
                               AnswerExplanation =
                                   row[AnswerFields.Explanation].ToString()
                           });
        }

        private static IEnumerable<Question> BuildQuestions(DataTable questionTable)
        {

            return questionTable.Rows.Cast<DataRow>().Select(
                row => new Question
                           {
                               QuestionId = row.Field<int>(QuestionFields.Id),
                               QuestionText = row.Field<string>(QuestionFields.QuestionText),
                               CorrectAnswer = row.Field<byte?>(QuestionFields.CorrectAnswer),
                               SortOrder = row.Field<int?>(QuestionFields.SortOrder)
                           });
        }

        private void BuildQuiz(DataRow quizRow)
        {
            TimesTaken = (int)quizRow[QuizFields.NumberOfTimesTaken];
            QuizTitle = quizRow[QuizFields.Title] as string;
            Language = quizRow[QuizFields.Language] as string;
            Description = quizRow[QuizFields.Description] as string;
            QuizPostLabel = quizRow[QuizFields.PostInfo] as string;

            CorrectAnswerText = quizRow[QuizFields.CorrectAnswer] as string;
            IncorrectAnswerText = quizRow[QuizFields.LabelBestAnswer] as string;

            ErrorText = quizRow[QuizFields.LabelError] as string;
            EmailPrompt = quizRow[QuizFields.LabelEnterEmail] as string;

            ThankYouText = quizRow[QuizFields.LabelThankYou] as string;
            YourScoreLabel = quizRow[QuizFields.LabelYourScore] as string;
            YourAnswerLabel = quizRow[QuizFields.LabelYourAnswer] as string;
            SubmitQuizLabel = quizRow[QuizFields.LabelSubmitQuiz] as string;
        }

        ///<summary>
        ///  Sends the result email.
        ///</summary>
        ///<param name = "p">The p.</param>
        public void SendResultEmail(string p)
        {
            if (!DataFormat.IsValidEmail(p))
                return;

            var body = new StringBuilder();
            body.AppendLine(ScoreText);
            body.AppendLine(string.Empty);
            body.AppendLine(QuizTitle);
            body.AppendLine(string.Empty);
            body.AppendLine(ThankYouText);


            int count = 1;

            foreach (Question question in Questions)
            {
                body.AppendLine(string.Format("{0}. {1}", count, question.QuestionText));
                count += 1;
                body.AppendLine(string.Empty);

                int letterAnswer = 0;
                int selectedLetterAnswer = 0;
                int selectedAnswerId = 0;
                foreach (Answer answer in (Answers.Where(a => a.QuestionId == question.QuestionId)))
                {
                    body.AppendLine(ABCD[letterAnswer] + answer.AnswerText);

                    if (SelectedAnswers.Contains(answer.AnswerId))
                    {
                        selectedLetterAnswer = letterAnswer;
                        selectedAnswerId = answer.AnswerId;
                    }
                    letterAnswer += 1;
                }
                body.AppendLine(string.Empty);
                body.AppendLine(YourAnswerLabel + ABCD[selectedLetterAnswer]);
                body.AppendLine(
                    (from Answer a in Answers where a.AnswerId == selectedAnswerId select a).First().AnswerExplanation);
                body.AppendLine(string.Empty);
            }

            string subject = QuizTitle + " Results";
            EmailHelper.SendMail(ConfigurationManager.AppSettings["ContactEmail"], p, subject, body.ToString(), false);
        }


        public void IncrementNumTaken()
        {
            SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                      "dbo.QuizIncrementCount",
                                      new SqlParameter(QuizIdParameter, QuizID));
        }

        #region Nested type: AnswerFields

        private static class AnswerFields
        {
            public const string Id = "AnswerId";
            public const string QuestionId = "QuestionId";
            public const string Value = "AnswerValue";
            public const string Text = "Answer";
            public const string Explanation = "AnswerExplanation";
        }

        #endregion

        #region Nested type: QuestionFields

        private static class QuestionFields
        {
            public const string Id = "QuestionId";
            public const string QuestionText = "Question";
            public const string CorrectAnswer = "CorrectAnswer";
            public const string SortOrder = "SortOrder";
        }

        #endregion

        #region Nested type: QuizFields

        private static class QuizFields
        {
            public const string NumberOfTimesTaken = "NumTaken";
            public const string Title = "QuizTitle";
            public const string Language = "Language";
            public const string Description = "QuizDescription";
            public const string PostInfo = "QuizPostInfo";
            public const string CorrectAnswer = "CorrectAnswerText";
            public const string LabelBestAnswer = "LabelBestAnswer";
            public const string LabelError = "LabelError";
            public const string LabelEnterEmail = "LabelEnterEmail";
            public const string LabelThankYou = "LabelThankYou";
            public const string LabelYourScore = "LabelYourScore";
            public const string LabelYourAnswer = "LabelYourAnswer";
            public const string LabelSubmitQuiz = "LabelSubmitQuiz";
        }

        #endregion
    }

    public struct Question
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public byte? CorrectAnswer { get; set; }
        public int? SortOrder { get; set; }
    }

    public struct Answer
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerValue { get; set; }
        public string AnswerText { get; set; }
        public string AnswerExplanation { get; set; }
    }
}