#region

using System.Collections.Generic;
using System.Xml;

#endregion

//
//
//<event
//
//start="May 28 2006 09:00:00 GMT"
//
//end="Jun 15 2006 09:00:00 GMT"
//
//isDuration="true"
//
//title="Writing Timeline documentation"
//
//image="http://simile.mit.edu/images/csail-logo.png"
//
//>
//
//A few days to write some documentation for &lt;a href="http://simile.mit.edu/timeline/" mce_href="http://simile.mit.edu/timeline/"&gt;Timeline&lt;/a&gt;.
//
//</event>
//
//

namespace Mises.Domain.Timeline
{
    /// <summary>
    ///   Wrapper for the simile timeline control data invented by MIT
    /// </summary>
    /// ///
    /// <created>04/23/2007</created>
    /// <by>Jeff Jensen</by>
    /// [Serializable()]
    public class TimeLineEvent
    {
        public string Born;
        public string Died;

        /// <summary>
        ///   Gets a value indicating whether this instance is duration.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is duration; otherwise, <c>false</c>.
        /// </value>
        public bool IsDuration { get; private set; }

        public string Title { get; set; }


        /// <summary>
        ///   Gets or sets the image (optional)
        /// </summary>
        /// <value>The image.</value>
        public string Image { get; set; }


        /// <summary>
        ///   Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        /// <created>04/23/2007</created>
        /// <by>Jeff Jensen</by>
        public string Text { get; set; }


        /// <summary>
        ///   Gets the XML.
        /// </summary>
        /// <param name = "tlEvents">The timeline events.</param>
        /// <returns></returns>
        /// <created>04/23/2007</created>
        /// <by>Jeff Jensen</by>
        public static string GetXML(List<TimeLineEvent> tlEvents)
        {
            var xDoc = new XmlDocument();

            //create root data element
            XmlElement position = xDoc.CreateElement("data");

            //add the root to the document
            xDoc.AppendChild(position);

            //iterate through events and add them to the timeline
            foreach (TimeLineEvent tlEvent in tlEvents)
            {
                //create a new timeline element
                XmlElement iNode = xDoc.CreateElement("", "event", "");

                //add the start and end date in the correct format
                iNode.SetAttribute("start", tlEvent.Born);

                if (! string.IsNullOrEmpty(tlEvent.Died))
                {
                    iNode.SetAttribute("end", tlEvent.Died);
                    iNode.SetAttribute("isDuration", "true");
                }
                else
                {
                    iNode.SetAttribute("isDuration", "false");
                }

                if (!string.IsNullOrEmpty(tlEvent.Title))
                {
                    iNode.SetAttribute("title", tlEvent.Title);
                }

                if (!string.IsNullOrEmpty(tlEvent.Image))
                {
                    iNode.SetAttribute("image", tlEvent.Image);
                }

                //Add the text to the node
                XmlNode xNode = xDoc.CreateTextNode(tlEvent.Text);

                iNode.AppendChild(xNode);

                //add the node to the XML document
                position.AppendChild(iNode);
            }
            return xDoc.InnerXml;
        }
    }
}