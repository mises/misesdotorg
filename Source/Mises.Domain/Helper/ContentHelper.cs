﻿#region

using System.Linq;
using Mises.Data;

#endregion

namespace System.Web.Mvc
{
    public static class ContentHelper
    {
        [OutputCache(Duration = 180, VaryByParam = "*")] // I dont think this does anything
        public static MvcHtmlString PageContent(this HtmlHelper helper, string contentName)
        {
            return new MvcHtmlString(GetContentForPage(contentName));
        }

        public static string GetContentForPage(string contentName)
        {
            var model = Mises.Data.DataHelper.EntityDataModel;
            var page = model.PageContent.SingleOrDefault(p => p.Name == contentName);
            return page != null ? page.Body : contentName;
        }
    }
}