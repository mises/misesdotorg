﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Mises.Data;
using Mises.Domain.Contracts;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core.Objects;

#endregion

namespace Mises.Domain
{
    public class MediaRepository : IMediaRepository
    {
        public MediaRepository() : this(DataHelper.ConnectionString) //TODO for now default to this connection string
        {
        }

        public MediaRepository(String connectionString)
        {
            ConnectionString = connectionString;
        }

        public String ConnectionString { get; private set; }

        #region IMediaRepository Members

        /// <summary>
        /// </summary>
        /// <param name = "id"></param>
        /// <returns></returns>
        public Document GetDocumentFromFileId(Int32 id)
        {
            Document result;

            using (var context = DataHelper.EntityDataModel)
            {
                DataHelper.SetTransactionIsolationLevelReadUncommited(context);

                var query =
                    (from d in context.Documents
                     where (d.DocumentFiles.Any(m => m.FileId == id) && d.Display)
                     select new
                                {
                                    d,
                                    DocumentFiles =
                         (from m in d.DocumentFiles
                          where m.Display
                          select m),
                                    d.MediaCategory,
                                    d.DocumentAuthor,
                                    d.DocumentAuthor2
                                });

                result = query.AsEnumerable().Select(d => d.d).FirstOrDefault();
            }

            return result;
        }

      
            

        public Document GetDocument(Int32 id)
        {
            Document result = null;

            using (var context = DataHelper.EntityDataModel)
            {
                var query =
                    (from d in context.Documents.Include("DocumentFiles").Include("DocumentAuthor").Include("DocumentAuthor2")
                     where ((d.DocumentId == id) && d.Display)
                     select new
                                {
                                    d,
                                    DocumentFiles =
                         (from m in d.DocumentFiles
                          where m.Display
                          select m),
                                    d.MediaCategory,
                                    d.DocumentAuthor,
                                    d.DocumentAuthor2
                                });

                result = query.AsEnumerable().Select(d => d.d).FirstOrDefault();

              //  result = compiledDocQuery.Invoke(context, id).SingleOrDefault();
            }

            return result;
        }

		public PaginableResult<Document> GetDocuments(
			string searchString
			, int categoryId
			, int subjectId
			, int authorId
			, int[] mediaTypeIds
			, Int32 pageIndex, Int32 pageSize)
		{
			const double titleWeight = 10;
			List<int> findDocsResults;
			int? count = 0;
			using (var db = DataHelper.MisesDataContext)
			{
				const string idListDelimiter = ";";
				string mediaTypesString = mediaTypeIds == null ? "" : string.Join(idListDelimiter, mediaTypeIds);
				findDocsResults = db.FindDocuments(
					searchString
					, pageSize
					, pageIndex
					, categoryId
					, subjectId
					, authorId
					, mediaTypesString
					, idListDelimiter
					, titleWeight
					, ref count).Select(r => r.DocumentId).ToList(); ;

			}

			PaginableResult<Document> result;

            using (var context = DataHelper.EntityDataModel)
            {
                context.DocumentAuthors.MergeOption= System.Data.Objects.MergeOption.NoTracking;
                context.MediaCategory.MergeOption= System.Data.Objects.MergeOption.NoTracking;
                context.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;

				result = new PaginableResult<Document>
				{
					PageIndex = pageIndex,
					PageSize = pageSize,
					TotalItems = count.HasValue ? count.Value : 0,
					Items = ((ObjectQuery<Document>)context.Documents
						.Where(d => findDocsResults.Any(r => r == d.DocumentId)))
                        .Include("DocumentFiles")
						.Include("DocumentAuthor")
						.Include("MediaCategory")
						.Include("DocumentAuthor2")
						.ToList()
				};

				// explicitly loading formats collection, see considerations above
				foreach (var d in result.Items.Where(d => !d.DocumentFiles.IsLoaded))
				{
				    d.DocumentFiles.Load();
				}
			}

			return result;
		}

        // cached compiled query

    //    static readonly Func<MisesModel, Decimal, IQueryable<Document>> compiledDocQuery =
    //CompiledQuery.Compile<MisesModel, Decimal, IQueryable<Document>>(
    //        (ctx, total) => from docs in ctx.Documents
    //                        .Where(d => d.Display && d.DocumentFiles.Any(m => m.Display)) select docs);

		/// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <param name = "pageIndex"></param>
        /// <param name = "pageSize"></param>
        /// <returns></returns>
        public PaginableResult<Document> GetDocuments(IDocumentFilter filter, Int32 pageIndex, Int32 pageSize)
        {
            using (var context = DataHelper.EntityDataModel)
            {
                context.DocumentFiles.MergeOption= System.Data.Objects.MergeOption.NoTracking;
                context.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;

                var result = new PaginableResult<Document>
                                 {
                                     PageIndex = pageIndex,
                                     PageSize = pageSize
                                 };

                var query = context.Documents.Where(d => d.Display && d.DocumentFiles.Any(m => m.Display));

                /* Filter specific... */

                if (!(filter is DocumentsWithVideoFilter) && !(filter is DocumentsWithAudioFilter) &&
                    !(filter is DocumentsWithLiteratureFilter))
                {
                    query = query.Where(d => d.DocumentFiles.Any(m =>
                                                                         (m.MediaTypeId == (Int32) MediaTypes.Wmv)
                                                                         || (m.MediaTypeId == (Int32) MediaTypes.Mp4)
                                                                         ||
                                                                         (m.MediaTypeId == (Int32) MediaTypes.Youtube)
                                                                         || (m.MediaTypeId == (Int32) MediaTypes.Stream)
                                                                         || (m.MediaTypeId == (Int32) MediaTypes.Mp3)
                                                                         || (m.MediaTypeId == (Int32) MediaTypes.Mp4a)));
                }

                if (filter is DocumentAuthorIdListFilter)
                    //NOTE: this must go above DocumentIdListFilter (inheritance means it'll get swallowed if not)
                {
                    DocumentAuthorIdListFilter f = filter as DocumentAuthorIdListFilter;
                    query =
                        query.Where(
                            d => f.IdList.Any(i => i == d.DocumentAuthor.AuthorId || i == d.DocumentAuthor2.AuthorId));
                }
                else if (filter is DocumentIdListFilter)
                {
                    DocumentIdListFilter f = filter as DocumentIdListFilter;
                    query = query.Where(d => f.IdList.Any(i => i == d.DocumentId));
                }
                else if (filter is DocumentMediaIdListFilter)
                {
                    DocumentMediaIdListFilter f = filter as DocumentMediaIdListFilter;
                    query = query.Where(d => d.DocumentFiles.Any(m => f.IdList.Any(i => i == m.FileId)));
                }
                else if (filter is DocumentsWithVideoFilter)
                {
                    query =
                        query.Where(
                            d =>
                            d.DocumentFiles.Any(
                                m =>
                                (m.MediaTypeId == (Int32) MediaTypes.Wmv) || (m.MediaTypeId == (Int32) MediaTypes.Mp4) ||
                                (m.MediaTypeId == (Int32) MediaTypes.Youtube) ||
                                (m.MediaTypeId == (Int32) MediaTypes.Stream)));
                }
                else if (filter is DocumentsWithAudioFilter)
                {
                    query =
                        query.Where(
                            d =>
                            d.DocumentFiles.Any(
                                m =>
                                (m.MediaTypeId == (Int32) MediaTypes.Mp3) || (m.MediaTypeId == (Int32) MediaTypes.Mp4a)));
                }
                else if (filter is DocumentsWithLiteratureFilter)
                {
                    query =
                        query.Where(
                            d =>
                            d.DocumentFiles.Any(
                                m =>
                                (m.MediaTypeId == (Int32) MediaTypes.Document) ||
                                (m.MediaTypeId == (Int32) MediaTypes.Ebook) ||
                                (m.MediaTypeId == (Int32) MediaTypes.Pdf)));
                }
                else if (filter is DocumentsByAuthorFilter)
                {
                    DocumentsByAuthorFilter f = filter as DocumentsByAuthorFilter;
                    query = query.Where(d => d.DocumentAuthor.AuthorId == f.Id || d.DocumentAuthor2.AuthorId == f.Id);
                }
                else if (filter is DocumentsByCategoryFilter)
                {
                    DocumentsByCategoryFilter f = filter as DocumentsByCategoryFilter;
                    query = query.Where(d => d.MediaCategory.CategoryId == f.Id);
                }
                else if (filter is DocumentsBySubjectFilter)
                {
                    DocumentsBySubjectFilter f = filter as DocumentsBySubjectFilter;

                    query = from d in query
                            join l in context.DocumentSubjectLink on d.DocumentId equals l.DocumentId
                            join s in context.DocumentSubjects on l.SubjectID equals s.SubjectId
                            where s.SubjectId == f.Id && s.Visible.HasValue && s.Visible.Value
                            select d;
                }
                else if (filter is DocumentMediaSearchFilter)
                {
                    DocumentMediaSearchFilter f = filter as DocumentMediaSearchFilter;
                    query =
                        query.Where(
                            d =>
                            d.Title.Contains(f.SearchTerm) || d.DocumentAuthor.AuthorLast.Contains(f.SearchTerm) ||
                            (d.DocumentAuthor.AuthorFirst + " " + d.DocumentAuthor.AuthorLast).Contains(f.SearchTerm));

                    //var results = dtos.Where(d => d.Title.Contains(term) || d.Authors.Any(a=> a.LastName.Contains(term) || (a.FirstName + " " + a.LastName).Contains(term)));
                }

                if (filter != null && filter.MaxResults != null)
                    query = query.OrderByDescending(d => d.CreateDate).Take(filter.MaxResults.Value);

                result.TotalItems = query.Count();
                result.Counts.Video =
                    query.Count(
                        d =>
                        d.DocumentFiles.Any(
                            m =>
                            (m.MediaTypeId == (Int32) MediaTypes.Wmv) || (m.MediaTypeId == (Int32) MediaTypes.Mp4) ||
                            (m.MediaTypeId == (Int32) MediaTypes.Youtube) ||
                            (m.MediaTypeId == (Int32) MediaTypes.Stream)));
                result.Counts.Audio =
                    query.Count(
                        d =>
                        d.DocumentFiles.Any(
                            m => (m.MediaTypeId == (Int32) MediaTypes.Mp3) || (m.MediaTypeId == (Int32) MediaTypes.Mp4a)));
                //result.Counts.Literature = query.Count(d => d.DocumentFiles.Any(m => (m.MediaTypeId == (Int32)MediaTypes.Pdf) || (m.MediaTypeId == (Int32)MediaTypes.Document) || (m.MediaTypeId == (Int32)MediaTypes.Ebook)));

				// issue: https://groups.google.com/forum/#!topic/misesdev/CP4ZB216Gsc
				// eager load includes have to go as late as possible so that the shape of the query does not change afterwards
				// see http://blogs.msdn.com/b/alexj/archive/2009/06/02/tip-22-how-to-make-include-really-include.aspx
				result.Items = ((ObjectQuery<Document>)query
					.OrderByDescending(d => d.CreateDate).Skip(pageIndex * pageSize).Take(pageSize))
					.Include("DocumentAuthor")
					// eagerly loading collection may lead to multiple copies of document rows being fetched (cartesian product), one of the document's columns,
					// metaImage is very big and causes excessive network traffic
					// ideally metaImage should not be loaded here at all, but it seems impossible in EF (probably would need to have it as separate entity)
					// the collection is however required and will be loaded in the loop explicitly afterwards
					//.Include("DocumentFiles")
					.Include("MediaCategory")
					.Include("DocumentAuthor2")
					.ToList();

				// explicitly loading formats collection, see considerations above
				foreach (var d in result.Items.Where(d => !d.DocumentFiles.IsLoaded))
				{
				    d.DocumentFiles.Load();
				}

                return result;
            }
        }

        public PaginableResult<MediaCategory> GetCategories(ICategoryFilter filter, Int32 pageIndex, Int32 pageSize)
        {
            var result = new PaginableResult<MediaCategory> {PageIndex = pageIndex, PageSize = pageSize};

            using (var context = DataHelper.EntityDataModel)
            {
                IQueryable<MediaCategory> q1 = from c in context.MediaCategory orderby c.SortOrder select c;

                // Filter...
                if (filter is TopLevelCategoryFilter)
                {
                    TopLevelCategoryFilter f = filter as TopLevelCategoryFilter;
                    q1 = q1.Where(c => c.ParentCategory == 0);
                }
                else if (filter is CategoryAncestorsFilter)
                {
                    CategoryAncestorsFilter f = filter as CategoryAncestorsFilter;
                    q1 = q1.Where(c => c.CategoryId == f.Id);

                    var p2 = getAncestors(context, q1.FirstOrDefault());
                    result.Items = f.IncludeTarget ? p2.Reverse().ToList() : p2.Reverse().Skip(1).ToList();
                    result.TotalItems = p2.Count();
                    return result;
                }
                else if (filter is CategoryChildrenFilter)
                {
                    CategoryChildrenFilter f = filter as CategoryChildrenFilter;
                    q1 = q1.Where(c => c.ParentCategory == f.Id);
                }
                else if (filter is CategoryNameFilter)
                {
                    CategoryNameFilter si = filter as CategoryNameFilter;
                    q1 =
                        q1.Where(c => c.Category != null && c.Category.Substring(0, si.Term.Length).ToLower() == si.Term);
                    //or c.Category.ToLower().Contains(si.Term)?
                }

                var q2 = (from c in q1
                          orderby /*c.SortOrder, */ c.SortOrder
                          select c);

                var paged = q2.Skip(pageIndex*pageSize).Take(pageSize);

                result.Items = paged.ToList();
                result.TotalItems = q2.Count();
            }

            return result;
        }

        public PaginableResult<DocumentSubjects> GetSubjects(ISubjectFilter filter, Int32 pageIndex, Int32 pageSize)
        {
            var result = new PaginableResult<DocumentSubjects> {PageIndex = pageIndex, PageSize = pageSize};

            using (var context = DataHelper.EntityDataModel)
            {
                var q1 = from s in context.DocumentSubjects where s.Visible.HasValue && s.Visible.Value select s;

                // Filter...
                if (filter is SubjectNameFilter)
                {
                    SubjectNameFilter si = filter as SubjectNameFilter;
                    q1 = q1.Where(c => c.Subject != null && c.Subject.Substring(0, si.Term.Length).ToLower() == si.Term);
                    //or c.Subject.ToLower().Contains(si.Term)?
                }

                var q2 = (from c in q1
                          orderby /*c.SortOrder, */ c.Subject
                          select c);

                var paged = q2.Skip(pageIndex*pageSize).Take(pageSize);

                result.Items = paged.ToList();
                result.TotalItems = q2.Count();
            }

            return result;
        }

        /// <summary>
        /// </summary>
        /// <returns>Currently only returns Documents containing YouTube clips</returns>
        public Document GetRandomVideo()
        {
            Document result = null;

            using (var context = DataHelper.EntityDataModel)
            {
                DataHelper.SetTransactionIsolationLevelReadUncommited(context);

                //TODO merge these into 1 call (either sp returns a complete entity or linq it)...
                var video = context.MediaGetRandomVideo().First();
                if (video != null)
                {
                    result = GetDocument(video.DocumentId);
                }
            }

            return result;
        }

        public MediaCategory GetCategory(Int32 id)
        {
            MediaCategory result = null;

            using (var context = DataHelper.EntityDataModel)
            {
                result = (from c in context.MediaCategory where c.CategoryId == id select c).FirstOrDefault();
            }

            return result;
        }

        public DocumentSubjects GetSubject(Int32 id)
        {
            DocumentSubjects result = null;

            using (var context = DataHelper.EntityDataModel)
            {
                result = (from c in context.DocumentSubjects where c.SubjectId == id select c).FirstOrDefault();
            }

            return result;
        }

        #endregion

        private IEnumerable<MediaCategory> getAncestors(MisesModel context, MediaCategory start)
        {
            MediaCategory cur = start;
            while (cur != null)
            {
                yield return cur;

                var q1 = from c in context.MediaCategory where (c.CategoryId == cur.ParentCategory) select c;
                cur = q1.FirstOrDefault();
            }
        }
    }
}