#region

using System;
using System.Data;
using System.Data.SqlClient;
using Mises.Data;

#endregion

namespace Mises.Domain.Periodicals
{
    public class PeriodicalStory
    {
        #region Properties

        public string Author2First;
        public string Author2Last;
        public string Author3First;
        public string Author3Last;
        public string AuthorFirst;
        public string AuthorLast;
        public string Content;
        public DateTime Created;
        public string EditedBy;
        public Guid GUID;
        public int Id;


        public int IssueSeason;
        public int IssueYear;
        public int PeriodicalId;
        public string Title;
// 1-4

        public string Season
        {
            get
            {
                switch (IssueSeason)
                {
                    case 1:
                        return "Spring";
                    case 2:
                        return "Summer";
                    case 3:
                        return "Fall";
                    case 4:
                        return "Winter";
                    default:
                        return "Unknown";
                }
            }
        }

        #endregion

        public static PeriodicalStory GetPeriodicalDetails(int periodicalId, int reviewId)
        {
            var story = new PeriodicalStory();

            switch (periodicalId)
            {
                case 2:
                    using (
                        SafeDataReader reader = SqlHelper.ExecuteReader(DataHelper.ConnectionString,
                                                                        CommandType.StoredProcedure,
                                                                        "dbo.MisesReviewGetReview",
                                                                        new SqlParameter("@control", reviewId)))
                    {
                        while (reader.Read())
                        {
                            // SELECT title, authorFirst, authorLast, authorFirst2, authorlast2,authorFirst3, authorlast3, issue_Year, body, CreateDate, GUID FROM misesreview WHERE (control = @control)
                            story.PeriodicalId = periodicalId;

                            story.AuthorFirst = reader.GetString("authorFirst");
                            story.AuthorLast = reader.GetString("authorLast");

                            story.Author2First = reader.GetString("authorFirst2");
                            story.Author2Last = reader.GetString("authorLast2");

                            story.Author3First = reader.GetString("authorFirst3");
                            story.Author3Last = reader.GetString("authorLast3");

                            story.Content = reader.GetString("body");

                            story.Title = reader.GetString("title");

                            story.Created = reader.GetDateTime("CreateDate");

                            story.IssueYear = reader.GetInt32("issue_Year");
                            story.IssueSeason = reader.GetInt32("issue_Season");

                            story.GUID = new Guid(reader["GUID"].ToString());
                        }
                    }
                    break;

                case 4:

                    var db = new MisesModel(DataHelper.ConnectionString);

                    //db.QJAEdb.Single(p => p.control = 1);


                    // var article = db.QJAEdb.Select(p => p.control == ArticleId);
                    // Get Specified Periodical


                    break;
            }

            return story;
        }

        public int UpdatePeriodical()
        {
            if (Id == 0) // Insert
            {
                return
                    Convert.ToInt32(SqlHelper.ExecuteScalar(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                            "PeriodicalAddStory", new SqlParameter("@PeriodicalId", 2),
                                                            new SqlParameter("@EditedBy", EditedBy),
                                                            new SqlParameter("@authorfirst", AuthorFirst),
                                                            new SqlParameter("@authorlast", AuthorLast),
                                                            new SqlParameter("@authorfirst2", Author2First),
                                                            new SqlParameter("@authorlast2", Author2Last),
                                                            new SqlParameter("@authorfirst3", Author3First),
                                                            new SqlParameter("@authorlast3", Author3Last),
                                                            new SqlParameter("@title", Title),
                                                            new SqlParameter("@body", Content),
                                                            new SqlParameter("@issue_season", IssueSeason),
                                                            new SqlParameter("@issue_year", IssueYear)));
            }
            else // Update
            {
                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                          "PeriodicalUpdateStory", new SqlParameter("@PeriodicalId", 2),
                                          new SqlParameter("@StoryId", Id), new SqlParameter("@EditedBy", EditedBy),
                                          new SqlParameter("@authorfirst", AuthorFirst),
                                          new SqlParameter("@authorlast", AuthorLast),
                                          new SqlParameter("@authorfirst2", Author2First),
                                          new SqlParameter("@authorlast2", Author2Last),
                                          new SqlParameter("@authorfirst3", Author3First),
                                          new SqlParameter("@authorlast3", Author3Last),
                                          new SqlParameter("@title", Title), new SqlParameter("@body", Content),
                                          new SqlParameter("@issue_season", IssueSeason),
                                          new SqlParameter("@issue_year", IssueYear));
                return Id;
            }
        }

        public static void Delete(Guid StoryGUID)
        {
            throw new Exception("TODO");
        }
    }
}