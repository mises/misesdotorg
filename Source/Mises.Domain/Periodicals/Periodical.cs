#region

using System;
using System.Data;
using System.Data.SqlClient;
using Mises.Data;

#endregion

namespace Mises.Domain.Periodicals
{
    public class Periodical
    {
        public DateTime CreateDate;
        public string Description;
        public string Logo;
        public int PeriodicalId;
        public string PublicationFrequency;
        public string RedirectURL;

        public string ShortDescription;
        public string Title;
        public string metaDescription;
        public string metaKeywords;
        public int results;


        public Periodical(int Id)
        {
            using (
                SafeDataReader reader = SqlHelper.ExecuteReader(DataHelper.ConnectionString,
                                                                CommandType.StoredProcedure, Sprocs.GetPeriodicalDetails,
                                                                new SqlParameter(Parameters.PeriodicalId, Id)))
            {
                while (reader.Read())
                {
                    PeriodicalId = reader.GetInt32(PeriodicalField.Id);
                    Title = reader.GetString(PeriodicalField.Title);
                    Description = reader.GetString(PeriodicalField.Description);
                    Logo = reader.GetString(PeriodicalField.Logo);
                    ShortDescription = reader.GetString(PeriodicalField.ShortDescription);
                    RedirectURL = reader.GetString(PeriodicalField.RedirectUrl);
                    PublicationFrequency = reader.GetString(PeriodicalField.PublicationFrequency);
                    metaDescription = reader.GetString(PeriodicalField.MetaDescription);
                    metaKeywords = reader.GetString(PeriodicalField.MetaKeywords);
                    CreateDate = reader.GetDateTime(PeriodicalField.CreateDate);
                }
            }
        }


        public DataSet GetPeriodicalArchives()
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            Sprocs.GetPeriodicalArchives,
                                            new SqlParameter(Parameters.PeriodicalId, PeriodicalId));
        }

        public DataSet GetPeriodicalArchives(string volume)
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            Sprocs.GetPeriodicalArchives,
                                            new SqlParameter(Parameters.PeriodicalId, PeriodicalId),
                                            new SqlParameter(Parameters.Volume, volume));
        }

        public DataSet GetPeriodicalAuthors()
        {
            throw new NotImplementedException();
        }

        public DataSet Search(string title, string author, string subject)
        {
            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                  Sprocs.GetPeriodicalArchives,
                                                  new SqlParameter(Parameters.PeriodicalId, PeriodicalId),
                                                  new SqlParameter(Parameters.Title, title),
                                                  new SqlParameter(Parameters.Author, author),
                                                  new SqlParameter(Parameters.Subject, subject));
            results = ds.Tables[0].Rows.Count;
            return ds;
        }

        #region Nested type: Parameters

        private static class Parameters
        {
            public const string PeriodicalId = "@PeriodicalId";
            public const string Title = "@title";
            public const string Author = "@author";
            public const string Subject = "@subject";
            public const string Volume = "@volume";
        }

        #endregion

        #region Nested type: PeriodicalField

        private static class PeriodicalField
        {
            public const string Id = "PeriodicalId";
            public const string Title = "Title";
            public const string Description = "Description";
            public const string Logo = "Logo";
            public const string ShortDescription = "ShortDescription";
            public const string RedirectUrl = "RedirectURL";
            public const string PublicationFrequency = "PublicationFrequency";
            public const string MetaDescription = "metaDescription";
            public const string MetaKeywords = "metaKeywords";
            public const string CreateDate = "CreateDate";
        }

        #endregion

        #region Nested type: Sprocs

        private static class Sprocs
        {
            public const string GetPeriodicalDetails = "dbo.PeriodicalsGetDetails";
            public const string GetPeriodicalArchives = "dbo.PeriodicalsGetArchives";
        }

        #endregion
    }
}