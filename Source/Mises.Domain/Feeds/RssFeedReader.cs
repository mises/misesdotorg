﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Cache;
using System.ServiceModel.Syndication;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Xml;

#endregion

namespace Mises.Domain.Feeds
{
    /// <summary>
    /// </summary>
    public static class RssFeedReader
    {
        private static string _feedUrl;
        //private static HttpContext _context;
        private static int _maxItems;

        public static IEnumerable<SyndicationItem> GetFeedItems(string feedUrl, HttpContextBase context,
                                                                int maxItems = 10)
        {
            try
            {
                var feed = context.Cache[feedUrl];

                if (feed == null)
                {
                    feed = UpdateFeed(feedUrl, maxItems, 2000);
                    context.Cache.Add(feedUrl, feed, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero,
                                      CacheItemPriority.AboveNormal, null);
                }

                return (IEnumerable<SyndicationItem>)feed;
            }
            catch (Exception ex1)
            {
                try
                {
                    _feedUrl = feedUrl;
                    _maxItems = 10;
                    var thread = new Thread(UpdateFeedAsync);
                    thread.Start();

                    return new List<SyndicationItem>
                               {
                                   new SyndicationItem
                                       {
                                           Title = new TextSyndicationContent("Failed to get feed: " + ex1.Message),
                                           Summary = new TextSyndicationContent(ex1.ToString())
                                       }
                               };
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    return new List<SyndicationItem>
                               {
                                   new SyndicationItem
                                       {
                                           Title = new TextSyndicationContent("Failed to get feed async: " + ex.Message),
                                           Summary = new TextSyndicationContent(ex.ToString())
                                       }
                               };
                }
            }
        }

        private static void UpdateFeedAsync()
        {
            if (_feedUrl == null) return;

            var feed = UpdateFeed(_feedUrl, _maxItems, 60000);
        }

        private static IEnumerable<SyndicationItem> UpdateFeed(string feedUrl, int maxItems, int timeout)
        {
            try
            {
                if (feedUrl == null) return null;

                var client = new CGWebClient { Timeout = timeout, UseDefaultCredentials = true };

                using (XmlReader reader = new SyndicationFeedXmlReader(client.OpenRead(feedUrl)))
                {
                    SyndicationFeed feed = SyndicationFeed.Load(reader);
                    if (feed != null) return feed.Items.Take(maxItems);
                }
            }
            catch (XmlException)
            {
                return null;
            }
            catch
            {
                return null;
            }
            return null;
        }


        // todo
        //private static CacheFeed(HttpContext httpContext)
        //{
        //    _context.Cache.Add(_feedUrl, feed, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero,
        //                       CacheItemPriority.Normal, null);
        //}
    }
}