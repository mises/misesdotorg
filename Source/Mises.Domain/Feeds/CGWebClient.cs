﻿#region

using System;
using System.Net;

#endregion

namespace Mises.Domain.Feeds
{
    public class CGWebClient : WebClient
    {
        private CookieContainer cookieContainer;
        private int timeout;
        private string userAgent;

        public CGWebClient()
        {
            timeout = -1;
            userAgent = @"OdinPlus Crawler v1.0";
            cookieContainer = new CookieContainer();
        }

        public CookieContainer CookieContainer
        {
            get { return cookieContainer; }
            set { cookieContainer = value; }
        }

        public string UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }

        public int Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            if (request.GetType() == typeof (HttpWebRequest))
            {
                ((HttpWebRequest) request).CookieContainer = cookieContainer;
                ((HttpWebRequest) request).UserAgent = userAgent;
                (request).Timeout = timeout;
            }

            return request;
        }
    }
}