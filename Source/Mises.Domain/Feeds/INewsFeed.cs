#region

using System.ServiceModel;
using System.ServiceModel.Syndication;
using System.ServiceModel.Web;

#endregion

namespace Mises.Domain.Feeds
{
    [ServiceContract]
    [ServiceKnownType(typeof (Atom10FeedFormatter))]
    [ServiceKnownType(typeof (Rss20FeedFormatter))]
    public interface INewsFeed
    {
        [OperationContract]
        [WebGet(UriTemplate = "feed/{format}")]
        SyndicationFeedFormatter feed(string format);
    }
}