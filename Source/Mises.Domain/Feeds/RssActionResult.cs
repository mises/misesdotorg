﻿#region

using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;

#endregion

namespace Mises.Domain.Feeds
{
    /// <summary>
    ///   How to use: return new RssActionResult() { Feed = myFeedInstance };
    ///   http://www.developerzen.com/2009/01/11/aspnet-mvc-rss-feed-action-result/
    /// </summary>
    public class RssActionResult : ActionResult
    {
        public SyndicationFeed Feed { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/rss+xml";

            Rss20FeedFormatter rssFormatter = new Rss20FeedFormatter(Feed);
            using (XmlWriter writer = XmlWriter.Create(context.HttpContext.Response.Output))
            {
                rssFormatter.WriteTo(writer);
            }
        }
    }
}