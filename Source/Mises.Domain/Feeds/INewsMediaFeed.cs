#region

using System.ServiceModel;
using System.ServiceModel.Syndication;
using System.ServiceModel.Web;

#endregion

namespace Mises.Domain.Feeds
{
    [ServiceContract]
    [ServiceKnownType(typeof (Atom10FeedFormatter))]
    [ServiceKnownType(typeof (Rss20FeedFormatter))]
    public interface INewsMediaFeed
    {
        [OperationContract]
        [WebGet(UriTemplate = "feed/{format}/?authorId={AuthorId}&CategoryId={CategoryId}")]
        SyndicationFeedFormatter feed(string format, int authorId, int categoryId);

        //[WebGet(UriTemplate = "feed/{format}/?authorId={AuthorId}&CategoryId={CategoryId}&MediaTypeId={MediaTypeId}")]
        //SyndicationFeedFormatter feed(string format, int authorId, int categoryId, int mediaTypeId);
    }
}