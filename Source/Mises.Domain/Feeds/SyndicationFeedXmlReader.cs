﻿#region

using System.IO;
using System.Linq;
using System.Xml;

#endregion

namespace Mises.Domain.Feeds
{
    public class SyndicationFeedXmlReader : XmlTextReader
    {
        private readonly string[] Atom10DateTimeHints = {"updated", "published", "lastBuildDate"};
        private readonly string[] Rss20DateTimeHints = {"pubDate"};
        //private bool isAtomDateTime;
        //private bool isRss2DateTime;

        public SyndicationFeedXmlReader(Stream stream) : base(stream)
        {
        }

        public override bool IsStartElement(string localname, string ns)
        {
          //  isRss2DateTime = false;
           // isAtomDateTime = false;

           // if (Rss20DateTimeHints.Contains(localname)) isRss2DateTime = true;
           // if (Atom10DateTimeHints.Contains(localname)) isAtomDateTime = true;

            return base.IsStartElement(localname, ns);
        }

        //public override string ReadString()
        //{
        //    string dateVal = base.ReadString();

        //    try
        //    {
        //        if (isRss2DateTime)
        //        {
        //            MethodInfo objMethod = typeof(Rss20FeedFormatter).GetMethod("DateFromString", BindingFlags.NonPublic | BindingFlags.Static);
        //            Debug.Assert(objMethod != null);
        //            objMethod.Invoke(null, new object[] { dateVal, this });

        //        }
        //        if (isAtomDateTime)
        //        {
        //            MethodInfo objMethod = typeof(Atom10FeedFormatter).GetMethod("DateFromString", BindingFlags.NonPublic | BindingFlags.Instance);
        //            Debug.Assert(objMethod != null);
        //            objMethod.Invoke(new Atom10FeedFormatter(), new object[] { dateVal, this });
        //        }
        //    }
        //    catch (TargetInvocationException)
        //    {
        //        DateTimeFormatInfo dtfi = CultureInfo.CurrentCulture.DateTimeFormat;
        //        return DateTimeOffset.UtcNow.ToString(dtfi.RFC1123Pattern);
        //    }

        //    return dateVal;

        //}
    }
}