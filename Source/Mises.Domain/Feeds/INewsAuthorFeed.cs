#region

using System.ServiceModel;
using System.ServiceModel.Syndication;
using System.ServiceModel.Web;

#endregion

namespace Mises.Domain.Feeds
{
    [ServiceContract]
    [ServiceKnownType(typeof (Atom10FeedFormatter))]
    [ServiceKnownType(typeof (Rss20FeedFormatter))]
    public interface INewsAuthorFeed
    {
        [OperationContract]
        [WebGet(UriTemplate = "feed/{format}/?authorId={AuthorId}")]
        SyndicationFeedFormatter feed(string format, int authorId);

        [OperationContract]
        [WebGet(UriTemplate = "fulltext/{format}/?authorId={AuthorId}")]
        SyndicationFeedFormatter FullTextFeed(string format, int authorId);
    }
}