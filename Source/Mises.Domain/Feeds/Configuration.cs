﻿namespace Mises.Domain.Feeds
{
    public class Configuration
    {
        public const string Email = "webmaster@freecapitalists.org";
        public const string Organization = "Ludwig von Mises Institute";
        public const string Copyright = "Copyright 2010 Mises Institute";


        public const string itunesNs = "http://www.itunes.com/dtds/podcast-1.0.dtd";
        public const string itunesPrefix = "itunes";

        public const string itunesUNs = "http://www.itunesu.com/feed";
        public const string itunesUPrefix = "itunesu";


    }
}