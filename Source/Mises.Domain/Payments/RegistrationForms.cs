#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Mises.Data;

#endregion

namespace Mises.Domain.Payments
{
    public class Product
    {
        public string Description;
        public int FormId;
        public double Price;
        public int ProductId;
        public string ProductName;
        public decimal Quantity;
        public bool SelectQuantity;

        //        public Controls_ContactInfo ContactInfo;


        public int Save()
        {
            if (FormId == 0)
            {
                throw new Exception("FormId is 0!");
            }

            if (ProductId > 0)
            {
                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                          "UPDATE    RegistrationProducts SET SelectQuantity = @SelectQuantity, Description = @Description, Price = @Price, ProductName = @ProductName, FormId = @FormId WHERE (ProductId = @ProductId)",
                                          new SqlParameter("@ProductId", ProductId), new SqlParameter("@FormId", FormId),
                                          new SqlParameter("@ProductName", ProductName),
                                          new SqlParameter("@Price", Price),
                                          new SqlParameter("@Description", Description),
                                          new SqlParameter("@SelectQuantity", SelectQuantity));
                return ProductId;
            }
            else
            {
                return
                    Convert.ToInt32(SqlHelper.ExecuteScalar(DataHelper.ConnectionString, CommandType.Text,
                                                            "INSERT INTO RegistrationProducts  (SelectQuantity, Description, Price, ProductName, FormId) VALUES (@SelectQuantity,@Description,@Price,@ProductName,@FormId)",
                                                            new SqlParameter("@FormId", FormId),
                                                            new SqlParameter("@ProductName", ProductName),
                                                            new SqlParameter("@Price", Price),
                                                            new SqlParameter("@Description", Description),
                                                            new SqlParameter("@SelectQuantity", SelectQuantity)));
            }
        }
    }

    public class Question
    {
        public string Answer;
        public int FormId;
        public int QuestionId;
        //INSTANT C# WARNING: Member names cannot be the same as their enclosing type:
        public string QuestionText;
        public int QuestionType;
        public bool Required;

        public int Save()
        {
            if (FormId == 0)
            {
                throw new Exception("FormId is 0!");
            }
            if (QuestionId > 0)
            {
                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                          "UPDATE    RegistrationQuestions SET FormId = @FormId, Question = @Question, QuestionType = @QuestionType, Required=@Required WHERE     (QuestionId = @QuestionId)",
                                          new SqlParameter("@QuestionId", QuestionId),
                                          new SqlParameter("@FormId", FormId),
                                          new SqlParameter("@Question", QuestionText),
                                          new SqlParameter("@QuestionType", QuestionType),
                                          new SqlParameter("@Required", Required));
                return QuestionId;
            }
            return
                Convert.ToInt32(SqlHelper.ExecuteScalar(DataHelper.ConnectionString, CommandType.Text,
                                                        "INSERT INTO RegistrationQuestions (FormId, Question, QuestionType,Required) VALUES     (@FormId,@Question,@QuestionType,@Required)",
                                                        new SqlParameter("@FormId", FormId),
                                                        new SqlParameter("@Question", QuestionText),
                                                        new SqlParameter("@QuestionType", QuestionType),
                                                        new SqlParameter("@Required", Required)));
        }
    }

    ///<summary>
    ///  Handles saved forms
    ///</summary>
    ///<remarks>
    ///</remarks>
    public sealed class RegistrationForm : IDisposable
    {
        #region Properties

        private DataSet dsForm;

        public int FormId { get; set; }

        public string EmailAddress { get; set; }

        public string EmailConfirmation { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public DateTime Expiration { get; set; }

        public DateTime CreateDate { get; set; }

        public string Introduction { get; set; }

        public string FooterText { get; set; }

        public string ThankYouMessage { get; set; }

        public string Image { get; set; }

        public int PrimaryProductId { get; set; }

        public bool GetAddressInfo { get; set; }

        public bool ProcessPayment { get; set; }

        public bool TakeCreditCard { get; set; }

        public bool EnterAmountPrompt { get; set; }

        public Guid Identifier { get; private set; }

        public string FormType { get; set; }
        public bool ShowSidebar { get; set; }
        public bool RecurringPrompt { get; set; }

        public bool SendConfirmationAsHtml { get; set; }

        #endregion

        #region Public Subs

        /// David
        /// 12/25/2005
        public RegistrationForm()
        {
        }

        ///<summary>
        ///  Loads saved form from database.
        ///</summary>
        ///<param name = "FormId">The form id.</param>
        ///David
        ///12/25/2005
        public RegistrationForm(int FormId)
        {
            //var db = new MisesDBDataContext(BusinessBase.ConnectionString);
            //var dsForm =  db.RegistrationFormDetails(FormId);


            dsForm = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                              "dbo.RegistrationFormDetails", new SqlParameter("@FormId", FormId));
            if (dsForm.Tables[0].Rows.Count == 0)
            {
                throw new ArgumentOutOfRangeException("Form not found!");
            }

            DataRow tempWith1 = dsForm.Tables[0].Rows[0];
            this.FormId = Convert.ToInt32(tempWith1["FormId"]);
            Title = Convert.ToString(tempWith1["FormTitle"]);
            Author = Convert.ToString(tempWith1["FormAuthor"]);
            CreateDate = Convert.ToDateTime(tempWith1["CreateDate"]);
            Expiration = Convert.ToDateTime(tempWith1["ExpirationDate"]);
            TakeCreditCard = Convert.ToBoolean(tempWith1["TakeCreditCard"]);
            ProcessPayment = Convert.ToBoolean(tempWith1["ProcessPayment"]);
            GetAddressInfo = Convert.ToBoolean(tempWith1["GetAddressInfo"]);
            Introduction = Convert.ToString(tempWith1["Introduction"]);
            FooterText = Convert.ToString(tempWith1["FooterText"]);
            ThankYouMessage = Convert.ToString(tempWith1["ThankYouMessage"]);
            Image = Convert.ToString(tempWith1["Image"]);
            PrimaryProductId = Convert.ToInt32(tempWith1["PrimaryProductId"]);
            EmailAddress = Convert.ToString(tempWith1["EmailAddress"]);
            ShowSidebar = Convert.ToBoolean(tempWith1["ShowSidebar"]);
            FormType = Convert.ToString(tempWith1["FormType"]);
            EmailConfirmation = Convert.ToString(tempWith1["EmailConfirmation"]);
            EnterAmountPrompt = Convert.ToBoolean(tempWith1["EnterAmountPrompt"]);

            if (tempWith1["RecurringPrompt"] != DBNull.Value)
            {
                RecurringPrompt = Convert.ToBoolean(tempWith1["RecurringPrompt"]);
            }
            Identifier = new Guid(tempWith1["GUID"].ToString());

        }

        public int SaveForm()
        {


            if (FormId > 0) // Update
            {
                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                          "UPDATE [RegistrationForms] SET [FormTitle] = @FormTitle, [FormAuthor] = @FormAuthor,[ExpirationDate] = @ExpirationDate, [TakeCreditCard] = @TakeCreditCard, [ProcessPayment] = @ProcessPayment, [GetAddressInfo] = @GetAddressInfo,[ShowSidebar] = @ShowSidebar, [Introduction] = @Introduction,[FooterText] = @FooterText,  [ThankYouMessage] = @ThankYouMessage, [Image] = @Image, [PrimaryProductId] = @PrimaryProductId, [EmailAddress]=@EmailAddress, [EmailConfirmation]=@EmailConfirmation, EnterAmountPrompt=@EnterAmountPrompt, [RecurringPrompt]=@RecurringPrompt  WHERE [FormId] = @FormId",
                                          new SqlParameter("@FormId", FormId), new SqlParameter("@FormTitle", Title),
                                          new SqlParameter("@FormAuthor", Author),
                                          new SqlParameter("@ExpirationDate", Expiration),
                                          new SqlParameter("@TakeCreditCard", TakeCreditCard),
                                          new SqlParameter("@ProcessPayment", ProcessPayment),
                                          new SqlParameter("@GetAddressInfo", GetAddressInfo),
                                          new SqlParameter("@ShowSidebar", ShowSidebar),
                                          new SqlParameter("@Introduction", Introduction),
                                          new SqlParameter("@FooterText", FooterText),
                                          new SqlParameter("@Image", Image),
                                          new SqlParameter("@PrimaryProductId", PrimaryProductId),
                                          new SqlParameter("@ThankYouMessage", ThankYouMessage),
                                          new SqlParameter("@EmailAddress", EmailAddress),
                                          new SqlParameter("@EmailConfirmation", EmailConfirmation),
                                          new SqlParameter("@EnterAmountPrompt", EnterAmountPrompt),
                                          new SqlParameter("@RecurringPrompt", RecurringPrompt));
            }
            else // Add New
            {

                FormId =
                    Convert.ToInt32(SqlHelper.ExecuteScalar(DataHelper.ConnectionString, CommandType.Text,
                                                            "INSERT INTO [RegistrationForms] ([FormTitle],ShowSidebar, [FormAuthor], [CreateDate], [ExpirationDate], [TakeCreditCard], [ProcessPayment], [GetAddressInfo], [Introduction],[FooterText],  [ThankYouMessage], [Image], [PrimaryProductId], EmailAddress,EmailConfirmation, EnterAmountPrompt,RecurringPrompt) VALUES (@FormTitle, @ShowSidebar, @FormAuthor, getdate(), @ExpirationDate, @TakeCreditCard, @ProcessPayment, @GetAddressInfo, @Introduction,@FooterText, @ThankYouMessage, @Image, @PrimaryProductId, @EmailAddress,@EmailConfirmation,@EnterAmountPrompt,@RecurringPrompt);SELECT TOP 1 FormId FROM [RegistrationForms] ORDER BY FormId DESC",
                                                            new SqlParameter("@FormId", FormId),
                                                            new SqlParameter("@FormTitle", Title),
                                                            new SqlParameter("@FormAuthor", Author),
                                                            new SqlParameter("@ExpirationDate", Expiration),
                                                            new SqlParameter("@TakeCreditCard", TakeCreditCard),
                                                            new SqlParameter("@ProcessPayment", ProcessPayment),
                                                            new SqlParameter("@GetAddressInfo", GetAddressInfo),
                                                            new SqlParameter("@ShowSidebar", ShowSidebar),
                                                            new SqlParameter("@Introduction", Introduction),
                                                            new SqlParameter("@FooterText", FooterText),
                                                            new SqlParameter("@Image", Image),
                                                            new SqlParameter("@PrimaryProductId", PrimaryProductId),
                                                            new SqlParameter("@ThankYouMessage", ThankYouMessage),
                                                            new SqlParameter("@EmailAddress", EmailAddress),
                                                            new SqlParameter("@EmailConfirmation", EmailConfirmation),
                                                            new SqlParameter("@RecurringPrompt", RecurringPrompt),
                                                            new SqlParameter("@EnterAmountPrompt", EnterAmountPrompt)));
            }

            return FormId;
        }


        public void DeleteForm(int FormId)
        {
            SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                      "DELETE FROM [RegistrationForms] WHERE [FormId] = @FormId",
                                      new SqlParameter("@FormId", FormId));
        }

        #endregion

        private bool disposedValue;

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: free unmanaged resources when explicitly called
                    //Products = null;
                    //Questions = null;
                    dsForm = null;
                }

                // TODO: free shared unmanaged resources
            }
            disposedValue = true;
        }

        #region  IDisposable Support

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}