#region

using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Utility;
using Olvio.Ecommerce;
using Document = Mises.Domain.Documents.Document;

#endregion

namespace Mises.Domain.Payments
{
    public class Donations
    {
        #region Properties

        public bool Success;

        public Mises.Data.Donations donation = new Data.Donations();
        public string IPaddress { get; set; }
        //public bool TestMode { get; set; }
        public string DonationReceipt { get; set; }
        public string WebConfirmationMessage { get; set; }
        public string InMemoryof { get; set; }
        public string CardVerification { get; set; }

        #endregion

        #region Private vars

        private readonly string contactEmail = ConfigurationManager.AppSettings["ContactEmail"];
        private int _DonationID;
        private AuthorizeNet anet;


        public Donations(string url)
        {
            this.donation = new Data.Donations
                                {
                                    FormUrl = url,
                                    Comments = "",
                                    CardName = "",
                                    OtherDesignation = "",
                                    Designation = "",
                                    CreateTime = DateTime.Now
                                };


        }

        #endregion

        public void ProcessDonation()
        {
            CreateAuthorizeNetSubmission("Donation: " + this.donation.Designation);

            try
            {
                if (anet.SubmitTransaction()) // Display Result
                {
                    Success = true;

                    if (donation.Amount >= 50) donation.MisesMember = true;

                    // Confirmation Message to Show on Site
                    var confirmation = new StringBuilder(Document.GetPageContentByName("donateConfirmation"));
                    doStringReplacement(confirmation);

                    WebConfirmationMessage += confirmation.ToString();

                    CreateDonationReceipt();

                    try
                    {
                        _DonationID = SaveDonation();
                    }
                    catch (Exception ex)
                    {
                        WebConfirmationMessage +=
                            "<br />There was an error saving the donation record to the database: " + ex;

                        EmailHelper.SendMail(contactEmail, contactEmail,
                                             "Problem @ Donations Page: Saving Record to DB",
                                             WebConfirmationMessage + ex);
                    }

                    try
                    {
                        SendMailReceipt();
                    }
                    catch (Exception ex)
                    {
                        WebConfirmationMessage += "<br />There was an error sending the donation receipt: " + ex;

                        EmailHelper.SendMail(contactEmail, contactEmail,
                                             "Problem @ Donations Page: Sending Receipt", WebConfirmationMessage + ex);
                    }
                }
                else
                {
                    Success = false;
                    DonationReceipt =
                        "Sorry, there was a problem processing your donation.  You can use your back button to correct your information and try again.  If you are unable to correct this problem, please call us at 1-800-OF-MISES.";

                    try
                    {
                        EmailHelper.SendMail(contactEmail, contactEmail, "Problem @ Donations Page",
                                             string.Format("{0} {1} {2} {3} ", anet.CardExpDate, anet.ResponseRawData,
                                                           anet.ResponseCardCode, anet.ResponseCode));
                    }
                    catch
                    {
                    }
                }
            }
            catch (AuthorizeNetException ex)
            {
                DonationReceipt = "Sorry, there was an error processing your transaction:";
                WebConfirmationMessage = ex + Environment.NewLine + "<br />";

                EmailHelper.SendMail("Errors@freecapitalists.org", "heroic@gmail.com", "Exception In Donations Page",
                                     ex + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                     anet.ResponseRawData);
            }
            finally
            {
                WebConfirmationMessage += "<br /><h4>Process Results:</h4><br />";
                WebConfirmationMessage += "<em>Response Code: </em>" + anet.ResponseCodeText + "<br />";
                WebConfirmationMessage += "<em>Reason: </em>" + anet.ResponseReasonText + "<br />";
                WebConfirmationMessage += "<em>Transaction Id: </em>" + _DonationID + "<br />";
            }
        }

        private void CreateDonationReceipt()
        {
            var strReceipt = new StringBuilder(Document.GetPageContentByName("DonateEmail"));

            // Send the donor an email receipt
            strReceipt.AppendFormat("*********************{0}CONTACT INFORMATION{1}*********************{2} <br />",
                                    Environment.NewLine, Environment.NewLine, Environment.NewLine);
            strReceipt.AppendFormat("First Name    : {0}{1} <br />", donation.FirstName, Environment.NewLine);
            strReceipt.AppendFormat("Last Name     : {0}{1} <br />", donation.LastName, Environment.NewLine);
            strReceipt.AppendFormat("Address1       : {0}{1} <br />", donation.Address, Environment.NewLine);
            strReceipt.AppendFormat("Address2       : {0}{1} <br />", donation.Address2, Environment.NewLine);
            strReceipt.AppendFormat("City          : {0}{1} <br />", donation.City, Environment.NewLine);
            strReceipt.AppendFormat("State         : {0}{1} <br />", donation.State, Environment.NewLine);
            strReceipt.AppendFormat("Zip           : {0}{1} <br />", donation.Zip, Environment.NewLine);
            strReceipt.AppendFormat("Country       : {0}{1} <br />", donation.Country, Environment.NewLine);
            strReceipt.AppendFormat("Email         : {0}{1} <br />", donation.Email, Environment.NewLine);
            //strReceipt.AppendFormat("Mises Member? : {0}{1} <br />", MisesMemberStatus, Environment.NewLine);
            if (!string.IsNullOrEmpty(InMemoryof))
                donation.Comments += " In Memory Of:" + InMemoryof;
            strReceipt.AppendFormat("In Memory Of : {0}{1} <br />", InMemoryof, Environment.NewLine);

            //if (GiftIncome)
            //    strReceipt.AppendFormat(
            //        "Contact me about gifts that can provide retirement income for myself or another loved one. {0} <br />",
            //        Environment.NewLine);
            if (donation.GiftEstate)
                strReceipt.AppendFormat("Contact me about making a gift through my estate plan. {0} <br />",
                                        Environment.NewLine);

            if (donation.Recurring)
                strReceipt.AppendFormat("Make this donation recurring. {0} <br />",
                                        Environment.NewLine);

            if (donation.Anonymous == true)
                strReceipt.AppendFormat("I wish to be anonymous. {0} <br />",
                                        Environment.NewLine);


            strReceipt.AppendFormat("Comments : {0}{1} <br />{2}{3}", Environment.NewLine, donation.Comments,
                                    Environment.NewLine,
                                    Environment.NewLine);

            strReceipt.AppendFormat("*********************{0}DONATION INFORMATION{1}*********************{2} <br />",
                                    Environment.NewLine, Environment.NewLine, Environment.NewLine);
            strReceipt.AppendFormat("Total Donation Amount          : {0}{1} <br />",
                                    Strings.FormatCurrency(donation.Amount, -1, TriState.UseDefault,
                                                           TriState.UseDefault, TriState.UseDefault),
                                    Environment.NewLine);
            strReceipt.AppendFormat("Contribution Designation : {0}{1} <br />", donation.Designation, Environment.NewLine);
            strReceipt.AppendFormat("Contribution Designation Other: {0}{1} <br />{2}", donation.OtherDesignation,
                                    Environment.NewLine, Environment.NewLine);
            strReceipt.AppendFormat("Recurring Monthly Donation: {0}{1} <br />{2}", donation.Recurring, Environment.NewLine,
                                    Environment.NewLine);

            strReceipt.AppendFormat("*********************{0}PAYMENT INFORMATION{1}*********************{2} <br />",
                                    Environment.NewLine, Environment.NewLine, Environment.NewLine);
            strReceipt.AppendFormat("Name On Card : {0}{1} <br />", donation.CardName, Environment.NewLine);
            strReceipt.AppendFormat("Card Type    : {0}{1} <br />", donation.CardType, Environment.NewLine);
            strReceipt.AppendFormat("Card Number  : xxxxxxxxxxx{0}{1} <br />",
                                    donation.CardNumber.Substring(donation.CardNumber.Length - 4), Environment.NewLine);
            strReceipt.AppendFormat("Exp. Date    : {0}/{1}{2}", donation.CardExpMonth, donation.CardExpYear, Environment.NewLine);

            doStringReplacement(strReceipt);

            DonationReceipt = strReceipt.ToString();
        }

        private void doStringReplacement(StringBuilder strReceipt)
        {
            strReceipt.Replace("{first}", donation.FirstName).Replace("{last}", donation.LastName).Replace("{timestamp}",
                                                                                         string.Format("{0} {1}",
                                                                                                       DateTime.Now.
                                                                                                           ToLongDateString
                                                                                                           (),
                                                                                                       DateTime.Now.
                                                                                                           ToLongTimeString
                                                                                                           ()));
        }

        public string ProcessRegistration(string Name)
        {
            CreateAuthorizeNetSubmission(Name);

            try
            {
                Success = anet.SubmitTransaction();
            }
            catch (AuthorizeNetException ex)
            {
                WebConfirmationMessage = ex + Environment.NewLine + "<br />";
            }
            finally
            {
                if (Success == false)
                {
                }
                WebConfirmationMessage += "<br /><h4>Transaction Results:</h4><br />";
                WebConfirmationMessage += "<em>Response Code: </em>" + anet.ResponseCodeText + "<br />";
                WebConfirmationMessage += "<em>Response Reason Code: </em>" + anet.ResponseReasonCode + "<br />";
                WebConfirmationMessage += "<em>Reason: </em>" + anet.ResponseReasonText + "<br />";
                WebConfirmationMessage += "<em>Transaction Id: </em>" + _DonationID + "<br />";

                // Save to database 
                SaveDonation();
            }
            return "";
        }

        private void CreateAuthorizeNetSubmission(string Name)
        {
            if (string.IsNullOrEmpty(donation.Country))
            {
                donation.Country = "USA";
            }
            // Get Values
            anet = new AuthorizeNet(AuthorizeNet.GatewayTypes.AIM)
                       {
                           Login = ConfigurationManager.AppSettings["PaymentLogin"],
                           TransactionKey = ConfigurationManager.AppSettings["PaymentTransactionKey"],
                           TransactionId = donation.DonationID.ToString(),
                           CardNumber = donation.CardNumber,
                           CardExpDate = donation.CardExpMonth + "/" + donation.CardExpYear,
                           CardCode = !string.IsNullOrEmpty(CardVerification) ? CardVerification : "",
                           Amount = donation.Amount.ToString(),
                           CurrencyCode = "USD",
                           BillFirstName = donation.FirstName,
                           Description = Name,
                           BillLastName = " " + donation.LastName,
                           BillAddress = (donation.Address + " " + donation.Address2).Trim(),
                           BillCity = donation.City,
                           BillState = donation.State,
                           BillCountry = donation.Country + " ",
                           BillZip = donation.Zip,
                           CustomerEmail = donation.Email,
                           CustomerIP = IPaddress,
                           BillPhone = donation.Phone,
                           TestMode = donation.TestMode,
                           Type = AuthorizeNet.TransType.AUTH_CAPTURE,
                           RecurringCharge = donation.Recurring,
                       };

            anet.SendCustomerEmail = false;
        }



        public int SaveDonation()
        {
            if (donation.TestMode)
            {
                donation.Comments += "(TEST MODE)";
            }
            var model = new MisesModel();

            donation.CardNumber = string.Empty; // do not store Credit Card numbers in DB!

            donation.ResponseCode = anet.ResponseCode;
            donation.ResponseCodeText = anet.ResponseCodeText;
            donation.ResponseReasonCode = anet.ResponseReasonCode;
            donation.TransactionId = anet.TransactionId;
            donation.ResponseRawData = anet.ResponseRawData;

            model.AddToDonations(donation);

            model.SaveChanges();

            if (!string.IsNullOrEmpty(donation.CardNumber) && donation.CardNumber.Length > 4)
                donation.CardNumber = donation.CardNumber.Remove(0, donation.CardNumber.Length - 4);

            return (int)donation.DonationID;

        }


        public void SendMailReceipt()
        {
            if (string.IsNullOrEmpty(donation.Email) || donation.Email.Trim() == string.Empty)
                return;

            // Send Mises faculty an email notification that a donation has been placed

            string strNotice = "The following donation was placed at " + DateTime.Now + Environment.NewLine +
                               Environment.NewLine;

            strNotice += "Referrer: " + donation.Referrer + Environment.NewLine;
            strNotice += "Url: " + donation.FormUrl + Environment.NewLine;
            strNotice += "IP address: " + IPaddress + Environment.NewLine;

            strNotice += "A record of this donation is available at at: http://mises.freecapitalists.org//manager/donations.aspx " +
                        Environment.NewLine +
                        Environment.NewLine;
            strNotice += DonationReceipt;
            const string MailFrom = "contact@freecapitalists.org";
            string Subject = "Donation Received at " + donation.FormUrl;

            try
            {
                EmailHelper.SendMail(MailFrom, ConfigurationManager.AppSettings["DonationEmail"], Subject, strNotice, true);
                EmailHelper.SendMail(MailFrom, "contact@freecapitalists.org", Subject, DonationReceipt, true, true);
               // EmailHelper.SendMail(MailFrom, "briggs@mises.com", Subject, DonationReceipt, true, true);
                EmailHelper.SendMail(MailFrom, "ashley@mises.com", Subject, DonationReceipt, true, true);
                //EmailHelper.SendMail(MailFrom, "david@freecapitalists.org", Subject, DonationReceipt, true, true);
            }
            catch
            {
            }

            // Send email to recipient
            Subject = "Receipt for your contribution to Mises Institute";
            EmailHelper.SendMail(MailFrom, donation.Email, Subject, DonationReceipt, true, true);
        }


    }
}