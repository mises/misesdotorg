#region

using System.Configuration;
using System.Data.Linq;

#endregion

namespace Mises.Domain.Books
{
    public class Book
    {
        public Book()
        {
        }

        public Book(int control, string display, string title, string subject, string checkedOutBy, string authorFirst,
                    string authorLast, string authorFirst2, string authorLast2, string authorFirst3, string authorLast3,
                    string publisherInfo, string comments)
        {
            Control = control;
            Subject = subject;
            Display = display;
            Title = title;
            CheckedOutBy = checkedOutBy;
            AuthorFirst = authorFirst;
            AuthorLast = authorLast;
            AuthorFirst2 = authorFirst2;
            AuthorLast2 = authorLast2;
            AuthorFirst3 = authorFirst3;
            AuthorLast3 = authorLast3;
            PublisherInfo = publisherInfo;
            Comments = comments;
        }


        public int Control { get; set; }
        protected string Subject { get; set; }
        public string Display { get; set; }
        public string Title { get; set; }
        public string CheckedOutBy { get; set; }
        public string AuthorFirst { get; set; }
        public string AuthorLast { get; set; }
        public string AuthorFirst2 { get; set; }
        public string AuthorLast2 { get; set; }
        public string AuthorFirst3 { get; set; }
        public string AuthorLast3 { get; set; }
        public string PublisherInfo { get; set; }
        public string Comments { get; set; }

        public string AuthorName
        {
            get { return AuthorLast + ", " + AuthorFirst; }
        }
    }

    
    }