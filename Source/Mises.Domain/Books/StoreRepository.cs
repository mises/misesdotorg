using System;
using System.Linq;
using Mises.Data;

namespace Mises.Domain
{
    public class StoreProduct
    {
        public int ProductId { get; set; }

        public string Name { get; set; }

        public int AuthorId { get; set; }

        public string ThumbnailUrl { get; set; }

        public string ThumbnailAltText { get; set; }

        public string Summary { get; set; }

        public decimal Price { get; set; }

        public decimal MSRP { get; set; }

        public object ImageUrl { get; set; }
    }

    public interface IStoreRepository
    {
        }

    public class StoreRepository : IStoreRepository
    {
        public static StoreProduct GetBookByProductId(int productId = 0)
        {
            var store = DataHelper.StoreDataModel;
            ac_Products book;

            if (productId > 0)
            {
                book = store.ac_Products.Where(p=> p.DisablePurchase == false && p.VisibilityId == 0).SingleOrDefault(p => p.ProductId == productId);

                // if getting a random book
                if (productId == 0 && book == null) return GetBookByProductId(0);
            }
            else
            {
                var products = store.ac_Products.Select(p => p.ProductId).ToList();

                var rand = new Random();
                var id = rand.Next(products.Count() - 1);
                productId = products[id];
                return GetBookByProductId(productId);
            }

            if (book == null) return null;

            if (book.ThumbnailUrl.Length > 1)
            {
                book.ThumbnailUrl = "http://store.mises.org" +
                                    (book.ThumbnailUrl).Replace("~/", "");
            }

            if (book.ImageUrl.Length > 1)
            {
                book.ImageUrl = "http://store.mises.org" +
                                (book.ImageUrl).Replace("~/", "");
            }

            string summary = (book.Summary);
            if (summary != null && summary.Length > 150)
                book.Summary = summary.Substring(0, 150) + "..";


            var product = new StoreProduct()
                              {
                                  ProductId = book.ProductId,
                                  Name = book.ac_Manufacturers.Name,
                                  AuthorId = book.ac_Manufacturers.ManufacturerId,
                                  ThumbnailUrl = book.ThumbnailUrl,
                                  ThumbnailAltText = book.ThumbnailAltText,
                                  Summary = book.Summary,
                                  Price = book.Price,
                                  MSRP = book.MSRP,
                                  ImageUrl = book.ImageUrl
                              };




            return product;
        }
    }
}