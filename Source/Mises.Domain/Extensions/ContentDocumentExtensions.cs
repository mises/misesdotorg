﻿#region

using System;
using Mises.Data;
using Mises.Domain.Contracts;
using Mises.Domain.Documents;
using Mises.Domain.Media;

#endregion

namespace Mises.Domain.Extensions
{
    public static class ContentDocumentExtensions
    {
        public static DocumentFile GetBestVideo(this ContentDocument document)
        {
            Int32[] types = VideoTypes.AsArray();

            DocumentFile result = null;

            foreach (int t in types)
            {
                result = document.DocumentFileList.Find(m => (m.MediaTypeId == t));
                if (result != null)
                    break;
            }

            return result;
        }
    }
}