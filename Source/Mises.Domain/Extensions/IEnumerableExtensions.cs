﻿#region

using System.Linq;
using Mises.Domain.Contracts;

#endregion

namespace System.Collections.Generic
{
    public static class IEnumerableExtensions
    {
        public static VideoDTO GetBestFormat(this IEnumerable<VideoDTO> list)
        {
            String[] types = VideoTypes.AsStringArray();

            VideoDTO result = null;

            foreach (string t in types)
            {
                result = list.FirstOrDefault(v => (v.MimeType == t));
                if (result != null)
                    break;
            }

            return result;
        }

        public static AudioDTO GetBestFormat(this IEnumerable<AudioDTO> list)
        {
            AudioDTO result = null;

            String[] types = AudioTypes.AsStringArray();

            foreach (string t in types)
            {
                result = list.FirstOrDefault(v => (v.MimeType == t));
                if (result != null)
                    break;
            }

            return result;
        }

        public static LiteratureDTO GetBestFormat(this IEnumerable<LiteratureDTO> list)
        {
            LiteratureDTO result = null;
            String[] types = LiteratureTypes.AsStringArray();

            foreach (string t in types)
            {
                result = list.FirstOrDefault(v => (v.MimeType == t));
                if (result != null)
                    break;
            }

            return result;
        }
    }
}