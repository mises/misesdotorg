﻿#region

using Mises.Data;

#endregion

namespace System
{
    public static class StringExtensions
    {
        public static String ToSlug(this String src)
        {
            return DataFormat.GenerateSlug(src);
        }

        public static String StripHTML(this String src)
        {
            return DataFormat.StripHTML(src);
        }
    }
}