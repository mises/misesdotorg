#region

using System;
using System.Data;
using System.Web.Services;

#endregion

//TODO: FIX to relative URL
[WebService(Namespace = "http://mises.freecapitalists.org//articles/"), WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ArticlesService : WebService
{
    public void articles()
    {
    }

    //[WebMethod]
    //public DailyArticle GetTodaysArticleExcerpt()
    //{
    //    var daily = new Ticker();
    //    daily.GetTinyStory();
    //    var dailyExcerpt = new DailyArticle();
    //    dailyExcerpt.Title = daily.title;
    //    dailyExcerpt.Author = daily.author;
    //    dailyExcerpt.DatePublished = DateTime.Today;
    //    dailyExcerpt.Description = daily.description;
    //    //TODO: FIX to relative URL
    //    dailyExcerpt.URL = "http://mises.freecapitalists.org//daily/" + daily.recordId;
    //    daily = null;
    //    return dailyExcerpt;
    //}

    [WebMethod]
    public DailyArticle GetArticleDetails(int articleId)
    {
        var archives = new Mises.Domain.Articles.DailyArticle(articleId);
        var article = new DailyArticle();
        article.Title = archives.Title;
        article.Author = archives.AuthorName;
        article.DatePublished = archives.DatePosted;
        article.Description = archives.Description;
        //TODO: FIX to relative URL
        article.URL = "http://mises.freecapitalists.org//daily/" + archives.ArticleId;
        article.ArticleText = archives.Contents;
        archives = null;
        return article;
    }


    [WebMethod]
    public DataSet GetMonthlyArchives()
    {
        var articles = new Mises.Domain.Articles.DailyArticle();
        return articles.GetArchives();
    }

    [WebMethod]
    public DataSet GetAuthorArchives(Int32 AuthorId)
    {
        var articles = new Mises.Domain.Articles.DailyArticle();
        return articles.GetArchives(false);
    }

    #region Nested type: DailyArticle

    public class DailyArticle
    {
        public string ArticleText;
        public string Author;
        public DateTime DatePublished;
        public string Description;
        public string Title;
        public string URL;
    }

    #endregion
}