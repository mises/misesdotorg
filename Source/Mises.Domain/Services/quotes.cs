#region

using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using Mises.Data;


#endregion

namespace Mises.Domain.Services
{
    //TODO: FIX to relative URL ?
    [WebService(Namespace = "http://mises.freecapitalists.org//quotes/"), WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class quotes : WebService
    {
        [WebMethod]
        public string RandomMisesQuote()
        {
            return RandomQuote(true);
        }
        
        public static string GetRandomQuote(string format)
        {
            var db = DataHelper.MisesDataContext;
            QuoteGetRandomResult quote = db.QuoteGetRandom().SingleOrDefault();

            if (quote == null)
                return string.Empty;

            switch (format)
            {
                case "js":
                    return
                        string.Format(
                            "document.write(\"<div class='quote'><a target='_top' href='http://mises.freecapitalists.org//quotes.aspx?action=subject&amp;subject={0}'> Ludwig von Mises:</a> " +
                            "\\\"{1}\\\" - <a target='_top' href='/quotes.aspx?action=source&amp;Source={2}'>{3}</a></div>\");",
                            quote.Subject, quote.Quote, quote.Source, quote.Source);

                case "txt":
                    return string.Format("{0}~~{1}", quote.Quote, "Ludwig von Mises");

                default:
                    return
                        "<small><a target=\"_top\" href=\"http://mises.freecapitalists.org//quotes.aspx?action=subject&amp;subject=" +
                        quote.Subject + "\"> Ludwig von Mises:</a> " + "\"" + quote.Quote +
                        "\" - <a target=\"_top\" href=\"http://mises.freecapitalists.org//quotes.aspx?action=source&amp;Source=" +
                        quote.Source + "\">" + quote.Source + "</a></small>";
            }
        }


        [WebMethod]
        public string RandomQuote(bool TextOnly)
        {
            var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
            QuoteGetRandomResult quote = db.QuoteGetRandom().SingleOrDefault();

            if (quote == null) return string.Empty;

            if (TextOnly)
            {
                return "Ludwig von Mises: \"" + quote.Quote + "\" (" + quote.Source + ")";
                // dr.Item("Subject").ToString &
            }
            else
            {
                return
                    "<small><a target=\"_top\" href=\"/http://mises.freecapitalists.org//quotes.aspx?action=subject&subject=" +
                    quote.Subject + "\"> Ludwig von Mises:</a> " + "\"" + quote.Quote +
                    "\" - <a target=\"_top\" href=\"/http://mises.freecapitalists.org//quotes.aspx?action=source&Source=" +
                    quote.Source + "\">" + quote.Source + "</a></small>";
            }
        }

        [WebMethod]
        public static QuoteGetRandomResult RandomQuote()
        {
            var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
            QuoteGetRandomResult quote = db.QuoteGetRandom().SingleOrDefault();

            return quote;
        }

        [WebMethod]
        public DataTable AllQuotes()
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure, "dbo.spGetQuotes").
                    Tables[0];
            return dt;
        }

        [WebMethod]
        public DataTable QuotesByAuthor(int AuthorId)
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure, "dbo.spGetQuotes",
                                         new SqlParameter("@AuthorId", AuthorId)).Tables[0];
            return dt;
        }

        [WebMethod]
        public DataTable QuotesBySource(string Source)
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure, "dbo.spGetQuotes",
                                         new SqlParameter("@Source", Source)).Tables[0];
            return dt;
        }

        [WebMethod]
        public DataTable QuoteSearch(string SearchTerm)
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure, "dbo.spGetQuotes",
                                         new SqlParameter("@SearchQuery", SearchTerm)).Tables[0];
            return dt;
        }

        [WebMethod]
        public static DataTable GetSubjectList()
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                         "dbo.QuotesGetSubjectList").Tables[0];
            return dt;
        }

        [WebMethod]
        public static DataTable GetAuthorList()
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                         "dbo.DocumentAuthorsGetList", new SqlParameter("@ContentType", "Quotes")).
                    Tables[0];
            return dt;
        }


        [WebMethod]
        public DataTable GetSourceList()
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text,
                                         "SELECT DISTINCT SOurce FROM QUOTES ORDER BY SOURCE").Tables[0];
            return dt;
        }
    }
}