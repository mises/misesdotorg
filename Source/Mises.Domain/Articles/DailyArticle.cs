#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Books;

using Document = Mises.Domain.Documents.Document;

#endregion

namespace Mises.Domain.Articles
{
    [Serializable]
    [DataContract]
    public class DailyArticle : Document
    {
        private const string MisesDailyUrlFormat = "http://mises.freecapitalists.org//daily/{0}/{1}";
        private const string MissingArticleIdMessage = "Missing Article Id!";
        public const string GetDailyArticleById = "dbo.DailyArticleGet";
        private const string ArticleIdParameter = "@ArticleId";

        public DailyArticle()
        {
        }

        public DailyArticle(int ArticleId)
        {
            var db = new MisesDBDataContext((new SqlConnection(ConnectionString)));

            DailyArticleGetResult article = db.DailyArticleGet(ArticleId).SingleOrDefault();

            if (null == article)
            {
                Debug.WriteLine(MissingArticleIdMessage);
                //throw new Exception(MissingArticleIdMessage);
                return;
            }

            ArticleFromLinqResult(article);
            this.ArticleId = ArticleId;
        }

        public DailyArticle ArticleFromLinqResult(DailyArticleGetResult article)
        {
            Title = article.Title;
            HeadlineText = article.HeadlineText;

            Contents = article.ArticleText;
            AuthorName = article.AuthorName;
            CoAuthorName = article.CoAuthorName.Trim();
            AuthorId = article.AuthorId;
            if (article.CoAuthorId != null) CoAuthorId = (int)article.CoAuthorId;
            if (article.DisplayOrder != null) DisplayOrder = (int)article.DisplayOrder;
            ShowArticle = article.ShowArticle;
            Featured = article.Featured;
            Headline = article.Headline;
            DatePosted = article.DatePosted;
            Description = article.Description;
            PhotoURL = article.PhotoURL;
            if (article.PhotoHeight != null) PhotoHeight = (int)article.PhotoHeight;
            Identifier = article.GUID;
            //PreviousGUID = article.PreviousId;
            //NextGUID = article.NextId;
            PreviousId = article.PreviousId;
            NextId = article.NextId;
            EditBy = article.EditBy;
            EditDate = article.EditDate;
            return this;
        }


        public DailyArticle ArticleFromLinqResult(DailyArticlesGetFeedResult article)
        {
            ArticleId = article.ArticleId;
            Title = article.Title;
            //HeadlineText = article.HeadlineText;
            AuthorName = article.AuthorName;
            AuthorId = article.authorId;
            Headline = article.headline;
            DatePosted = article.dateposted;
            Description = article.description;
            PhotoURL = article.Photo;
            Identifier = article.GUID;
            return this;
        }

        public DailyArticle ArticleFromLinqResult(DailyArticlesGetFullTextFeedResult article)
        {
            ArticleId = article.ArticleId;
            Title = article.Title;
            AuthorName = article.AuthorName;
            AuthorId = article.authorId;
            Headline = article.headline;
            DatePosted = article.dateposted;
            Description = article.description;
            PhotoURL = article.Photo;
            Identifier = article.GUID;
            Contents = article.ArticleText;
            return this;
        }

        ///<summary>
        ///  Inserts the bio in contents.
        ///  http://jamesewelch.wordpress.com/2008/07/11/how-to-render-a-aspnet-user-control-within-a-web-service-and-return-the-generated-html/
        ///</summary>
        ///<returns></returns>
        public string ReplaceText()
        {
            if (Contents == null) return string.Empty;

            string contents = Contents;

            var products = new Dictionary<int, string>();
            MatchCollection matches = Regex.Matches(contents, @"\[product:.*\]");

            try
            {
                foreach (Match tag in matches)
                {

                    int productId = Convert.ToInt32(tag.Value.Replace("[product:", "").Replace("]", ""));

                    var book = StoreRepository.GetBookByProductId(productId);

                    if (book == null)
                    {
                        productId = 0;
                        book = StoreRepository.GetBookByProductId(productId);
                    }

                    if (book != null)
                    {
                        string bookHtml =
                            string.Format(
                                @"

<div class=""book-ad"" id=""{0}-ad"">
  <div class=""book-img"">
        <a href=""http://store.mises.org/Product.aspx?ProductId={0}"" title=""{3}""><img src=""http://mises.freecapitalists.org/{1}"" border=""0"" alt=""{3}""></a>
  </div>
  <div class=""book-price"">
        <p><a href=""http://store.mises.org/Product.aspx?ProductId={0}""><span class=""line-through"">{4}</span> {2}</a></p>
  </div>  
</div>

",
                                book.ProductId, book.ImageUrl, (book.Price).ToString("C"), book.Name,
                                (book.MSRP).ToString("C"));
                        products.Add(productId, bookHtml);
                    }
                }



                products.ToList().ForEach(
                    p => { contents = contents.Replace(string.Format("[product:{0}]", p.Key), p.Value); });

            }
            catch
            {
                contents = contents.Replace("[product:0]", "<!-- exception getting random product -->");
            }


            var courses = new Dictionary<int, string>();
            matches = Regex.Matches(contents, @"\[course:.*\]");

            foreach (Match tag in matches)
            {
                int courseId = Convert.ToInt32(tag.Value.Replace("[course:", "").Replace("]", ""));

                var ads = from widget in Mises.Data.DataHelper.EntityDataModel.AcademyCourses orderby Guid.NewGuid() select widget;

                AcademyCourse course = courseId > 0 ? ads.SingleOrDefault(c => c.WidgetId == courseId) : ads.First();

                if (course != null)
                {
                    string bookHtml =
                        string.Format(
                            @"<div class=""figure-left""><a href=""{0}""><img alt=""{2}"" src=""{1}""></a></div>",
                            course.ProductUrl, course.ImageUrl, course.Description
                            );

                    courses.Add(courseId, bookHtml);
                }
            }

            courses.ToList().ForEach(
                p => { contents = contents.Replace(string.Format("[course:{0}]", p.Key), p.Value); });


            contents = contents.Replace("[image]",
                                        "<img src=\"//s3.amazonaws.com/veksler-backup/DailyArticleBigImages/" + ArticleId +
                                        ".jpg\" alt=\"\">");
            contents = contents.Replace("[thumb]",
                                        "<img src=\"//s3.amazonaws.com/veksler-backup/DailyArticleImages/" + ArticleId +
                                        ".jpg\" alt=\"\">");


            contents = contents.Replace("[AuthorName]", AuthorName);
            contents = contents.Replace("[AuthorName2]", CoAuthorName);

            contents = contents.Replace("[AuthorArchive]",
                                        String.Format(
                                            @"<a class=""archives"" href=""http://mises.freecapitalists.org//daily/author/{0}/{1}"">article archives</a>",
                                            AuthorId, AuthorName.ToSlug()));

            contents = contents.Replace("[AuthorArchive2]",
                                        String.Format(
                                            @"<a class=""archives"" href=""http://mises.freecapitalists.org//daily/author/{0}/{1}"">article archives</a>",
                                            CoAuthorId, CoAuthorName.ToSlug()));

            contents = contents.Replace("[RSSfeed]",
                                        String.Format(
                                            @"<a class=""archives"" href=""http://mises.freecapitalists.org//Feeds/articles.ashx?AuthorId={0}"">RSS feed</a>",
                                            AuthorId));

            contents = contents.Replace("[RSSfeed2]",
                                        String.Format(
                                            @"<a class=""archives"" href=""http://mises.freecapitalists.org//Feeds/articles.ashx?AuthorId={0}"">RSS feed</a>",
                                            CoAuthorId));

            contents = contents.Replace("[comment]",
                                        String.Format(
                                            @"<a class=""comment"" href=""javascript:$('#tabs').tabs('select',1);window.scrollTo(0, 0);"">Comment on this article.</a>"));


            const string embedString = "[Embed:";
            if (contents.Contains(embedString))
            {
                int mediaIdStart = contents.IndexOf(embedString) + 7;
                int mediaIdEnd = contents.IndexOf("]", mediaIdStart);
                int mediaId = Convert.ToInt32(contents.Substring(mediaIdStart, mediaIdEnd - mediaIdStart));
                contents = contents.Replace(string.Format("[Embed:{0}]", mediaId),
                                            String.Format(
                                                @"<iframe src=""/Services/MediaEmbed.aspx?MediaId={0}"" scrolling=""no"" frameborder=""0"" style=""width:640px; height:485px""></iframe>",
                                                mediaId));
            }


            if (!contents.Contains("[bio]") && !contents.Contains("[bio2]") && !contents.Contains("[LatestArticle2]") &&
                !contents.Contains("[LatestArticle]") &&
                !contents.Contains("[AuthorPhoto]") && !contents.Contains("[AuthorPhoto2]"))
                return contents;

            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                                  "dbo.DocumentAuthorsGetDetail",
                                                  new SqlParameter(ArticleIdParameter, ArticleId));

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                contents = contents.Replace("[bio]", ds.Tables[0].Rows[0]["BioText"].ToString());
                contents = contents.Replace("[LatestArticle]",
                                            string.Format(@"<a href=""/daily/{0}"">latest article</a>",
                                                          ds.Tables[0].Rows[0]["LatestArticleId"])
                    );

                contents = contents.Replace("[AuthorPhoto]",
                                            string.Format(
                                                @"<img class=""AuthorPhoto"" src=""{0}"" alt=""Photo of {1}"" />",
                                                ds.Tables[0].Rows[0]["Photo"],
                                                AuthorName)
                    );
            }

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 1)
            {
                contents = contents.Replace("[bio2]", ds.Tables[0].Rows[1]["BioText"].ToString());
                contents = contents.Replace("[LatestArticle2]",
                                            string.Format(@"<a href=""/daily/{0}"">latest article</a>",
                                                          ds.Tables[0].Rows[1]["LatestArticleId"])
                    );

                contents = contents.Replace("[AuthorPhoto2]",
                                            string.Format(
                                                @"<img class=""AuthorPhoto"" src=""{0}"" alt=""Photo of {1}"" />",
                                                ds.Tables[0].Rows[1]["Photo"],
                                                AuthorName)
                    );
            }

            return contents;
        }

        public static IEnumerable<DailyArticle> GetTodaysArticles()
        {
            var db = new MisesDBDataContext(ConnectionString);
            var articles = db.DailyArticlesGetTodaysSummary();

            return articles.Select(dbarticle => new DailyArticle
                                                    {
                                                        ArticleId = dbarticle.ArticleId,
                                                        Title = dbarticle.Title,
                                                        AuthorName = dbarticle.Author,
                                                        CoAuthorName = dbarticle.CoAuthor,
                                                        AuthorId = dbarticle.AuthorId,
                                                        CoAuthorId =
                                                            (int)dbarticle.CoAuthorId,
                                                        DatePosted = dbarticle.DatePosted,
                                                        Description = dbarticle.Description,
                                                        PhotoURL = dbarticle.PhotoURL,
                                                        PhotoHeight =
                                                            dbarticle.PhotoHeight != null
                                                                ? (int)dbarticle.PhotoHeight
                                                                : 0
                                                    });
        }

        #region Article Archive Views

        public static int SearchByTitle(string Title)
        {
            var titleParam = new SqlParameter("@Title", SqlDbType.VarChar)
                                 {
                                     Size = 100,
                                     Value = Title.StripHTML()
                                 };
            return
                Convert.ToInt32(
                    Conversion.Val(SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                           "SELECT ArticleId FROM DailyArticles WHERE Title = @Title",
                                                           titleParam)));
        }


        //public static DataSet GetHomepage()
        //{
        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.spGetHomepage");
        //}


        public DataSet GetArticleAuthors()
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "spGetAuthorList");
            _results = ds.Tables[0].Rows.Count;
            return ds;
        }


        public DataSet GetArchives()
        {
            return GetArchives(false);
        }

        //INSTANT C# NOTE: C# does not support optional parameters. Overloaded method(s) are created above.
        //ORIGINAL LINE: Public Function GetArchives(Optional ByVal ShowAll As Boolean = false) As DataSet
        public DataSet GetArchives(bool ShowAll)
        {
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GetDailyArticlesArchive,
                                                  new SqlParameter("@ShowAll", true));
            _results = ds.Tables[0].Rows.Count;
            return ds;
        }


        ///<summary>
        ///  Searches the specified STR search.
        ///  Manager Only
        ///</summary>
        ///<param name = "strSearch">The STR search.</param>
        ///<returns></returns>
        public DataTable Search(string strSearch)
        {
            DataTable dt =
                SqlHelper.ExecuteDataset(ConnectionString, GetDailyArticlesArchive,
                                         new SqlParameter("@SearchQuery", strSearch), new SqlParameter("@ShowAll", true))
                    .Tables[0];
            _results = dt.Rows.Count;
            return dt;
        }


        public static IEnumerable<DailyArticlesGetFeedResult> GetRSSFeed()
        {
            return GetRSSFeed(0);
        }

        //INSTANT C# NOTE: C# does not support optional parameters. Overloaded method(s) are created above.
        //ORIGINAL LINE: Public Shared Function GetRSSFeed(Optional ByVal AuthorId As Integer = 0) As DataTable
        public static IEnumerable<DailyArticlesGetFeedResult> GetRSSFeed(int AuthorId)
        {
            var db = new MisesDBDataContext((new SqlConnection(ConnectionString)));
            return db.DailyArticlesGetFeed(AuthorId);
        }


        public static ISingleResult<DailyArticlesGetFullTextFeedResult> GetRSSFullTextFeed()
        {
            var db = new MisesDBDataContext((new SqlConnection(ConnectionString)));
            return db.DailyArticlesGetFullTextFeed();
        }

        #endregion

        #region Edit Article

        public int SaveArticle(string _EditedBy)
        {
            PhotoURL = PhotoURL.Replace("http://www.mises.org", "").Replace("http://mises.freecapitalists.org/", "");

            var parameters = new List<SqlParameter>
                                 {
                                     new SqlParameter("@Title", Title),
                                     new SqlParameter("@HeadlineText", HeadlineText),
                                     new SqlParameter("@Description", Description),
                                     new SqlParameter("@ArticleText", Contents),
                                     new SqlParameter("@DatePosted", DatePosted),
                                     new SqlParameter("@Featured", Featured),
                                     new SqlParameter("@Headline", Headline),
                                     new SqlParameter("@ShowArticle", ShowArticle),
                                     new SqlParameter("@AuthorId", AuthorId),
                                     new SqlParameter("@CoAuthorId", CoAuthorId),
                                     new SqlParameter("@PhotoURL", PhotoURL),
                                     new SqlParameter("@PhotoHeight", PhotoHeight),
                                     new SqlParameter("@EditBy", _EditedBy)
                                 };
            if (ArticleId > 0)
            {
                parameters.Add(new SqlParameter(ArticleIdParameter, ArticleId));
            }
            else
            {
                var param = new SqlParameter(ArticleIdParameter, 0) { Direction = ParameterDirection.Output };
                parameters.Add(param);
            }
            var commandParameters = new SqlParameter[parameters.Count + 1];
            int i = 0;
            foreach (SqlParameter param in parameters)
            {
                commandParameters[i] = param;
                i += 1;
            }
            if (ArticleId > 0)
            {
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.DailyArticleUpdate",
                                          commandParameters);
            }
            else
            {
                ArticleId = (int)SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure,
                                                          "dbo.DailyArticleAdd", commandParameters);
            }
            return ArticleId;
        }

        public static void DeleteArticle(int articleId)
        {
            if (articleId == 0)
            {
                throw new ArgumentOutOfRangeException("articleId");
            }
            SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure,
                                      "dbo.DailyArticlesDeleteByArticleId",
                                      new SqlParameter(ArticleIdParameter, articleId));
        }

        #endregion

        #region Properties

        private const string GetDailyArticlesArchive = "DailyArticlesGetArchive";
        private string EditBy;
        private DateTime? EditDate;
        private int _results;
        private string formattedHtml;


        public int results
        {
            get { return _results; }
        }

        public bool Featured { get; set; }

        public bool Headline { get; set; }

        public string HeadlineText { get; set; }

        [DataMember]
        public int ArticleId
        {
            get { return Id; }
            set { Id = value; }
        }

        [DataMember]
        public string AuthorName { get; set; }

        [DataMember]
        public string CoAuthorName { get; set; }

        [DataMember]
        public Int32 AuthorId { get; set; }

        [DataMember]
        public Int32? CoAuthorId { get; set; }

        public bool ShowArticle { get; set; }

        //[DataMember]
        //public string HTMLContent { get; set; }

        public string Homepage { get; set; }

        public int DisplayOrder { get; set; }

        [DataMember]
        public string PhotoURL { get; set; }

        [DataMember]
        public string ThumbnailURL { get; set; }

        public int PhotoHeight { get; set; }

        [DataMember]
        public DateTime DatePosted { get; set; }

        [DataMember]
        public new string URL
        {
            get { return string.Format(MisesDailyUrlFormat, ArticleId, Title.ToSlug()); }
            set { } // hack
        }

        public override string NextURL
        {
            get
            {
                if (!(string.IsNullOrEmpty(NextId.ToString())))
                {
                    return string.Format(MisesDailyUrlFormat, NextId, "");
                }
                return string.Empty;
            }
        }

        public override string PreviousURL
        {
            get
            {
                return (string.IsNullOrEmpty(PreviousId.ToString()))
                           ? string.Empty
                           : string.Format(MisesDailyUrlFormat, PreviousId, "");
            }
        }

        public string EditedBy
        {
            get
            {
                return (((EditBy + " - ") + ((DateTime)EditDate).ToShortDateString()) + " ") +
                       ((DateTime)EditDate).ToShortTimeString();
            }
        }

        protected int? PreviousId { get; set; }

        protected int? NextId { get; set; }

        public string AuthorArchivesUrl
        {
            get { return "/daily/author/" + AuthorId + "/" + AuthorName.ToSlug(); }
        }

        public string CoAuthorArchivesUrl
        {
            get { return "/daily/author/" + CoAuthorId + "/" + CoAuthorName.ToSlug(); }
        }

        public HtmlString ArticleHtml
        {
            get
            {
                if (formattedHtml == null)
                {
                    formattedHtml = ReplaceText();
                }

                if (IsPreview)
                {
                    formattedHtml = formattedHtml.Replace(@"//mises.freecapitalists.org/images", @"//direct.mises.org/images");
                }

                return new HtmlString(formattedHtml);
            }
        }

        public bool IsPreview { get; set; }

        #endregion
    }
}