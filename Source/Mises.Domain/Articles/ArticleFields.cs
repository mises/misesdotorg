﻿namespace Mises.Domain.Articles
{
    public static class ArticleFields
    {
        public const string DatePosted = "DatePosted";
        public const string Description = "Description";
        public const string ArticleId = "ArticleId";
        public const string ArticleText = "ArticleText";
        public const string AuthorId = "AuthorId";
        public const string Title = "Title";
        public const string AuthorName = "AuthorName";
        public const string Photo = "Photo";
        public const string Guid = "GUID";
        public const string Headline = "Headline";
        public const string Featured = "Featured";
    }
}