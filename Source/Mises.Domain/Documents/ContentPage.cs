#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Mises.Data;

#endregion

namespace Mises.Domain.Documents
{
    public class ContentPage : Document
    {
        public ContentPage(Guid Id) : base(Id)
        {
            Identifier = Id;
        }

        private MisesModel model;

        
        public int UpdateDocument()
        {
            model = DataHelper.EntityDataModel;
            var page = model.PageTableSet.SingleOrDefault(p => p.GUID == Identifier) ?? new PageTable();

            page.content = Contents;
            page.metaKeywords = Keywords;
            page.metaDescription = Description;
            page.pageTitle = Title;
            page.CreateDate = DateTime.Now;
            page.EditDate = DateTime.Now;
            page.Visible = true;
            

            if (page.PageId == 0)
            {
                model.PageTableSet.AddObject(page);
            }

            model.SaveChanges();

            Id = page.PageId;

            return page.PageId;   
        }

    
    }
}