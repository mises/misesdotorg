#region

using System.Data;
using Mises.Data;

#endregion

namespace Mises.Domain.Documents
{
    public class DocumentCatalog : DataHelper
    {
        public static DataSet GetCatalogHomepage()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.DocumentGetHomepage");
        }
    }
}