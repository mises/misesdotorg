#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using Mises.Data;
using Mises.Domain.Media;


#endregion

namespace Mises.Domain.Documents
{
    public class ContentDocument : Document
    {
        public ContentDocument()
        {
            Display = true;
        }

        public ContentDocument(int documentId)
        {
            DocumentFromDocumentFileId(documentId, 0);
        }

        public string StoreDescription { get; set; }

        public void DocumentFromFileId(int fileId)
        {
            DocumentFromDocumentFileId(0, fileId);
        }


        public void DocumentFromDocumentFileId(int documentId, int fileId)
        {
            Display = true;
            Id = documentId;
            dsDocument = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                                  Procedures.GetDocumentDetails,
                                                  new SqlParameter(Parameters.DocumentId, Id),
                                                  new SqlParameter(Parameters.FileId, fileId));

            if (dsDocument.Tables[0].Rows.Count == 0)
            {
                // document not found
                return;
            }

            var row = dsDocument.Tables[0].Rows[0];

            Id = Convert.ToInt32(row[Fields.DocumentId]);

            Display = Convert.ToBoolean(row[Fields.Display]);
            // Featured = Convert.ToBoolean(row[Fields.Featured);
            Title = row.Field<string>(Fields.Title);
            Identifier = new Guid(row[Fields.Guid].ToString());
            Author1 = row.Field<int>(Fields.Author1);
            Author2 = row.Field<int?>(Fields.Author2);
            AuthorName = row.Field<string>(Fields.AuthorName);
            CoAuthorName = row.Field<string>("CoAuthorName");
            PublicationInformation = row.Field<string>(Fields.PublicationInformation);
            CoverImage = row.Field<string>("CoverImage");


            ProductId = row.Field<int?>("ProductId");
            //StoreDescription = row.Field<string>("StoreDescription");

            ISBN = row.Field<string>("ISBN");


            Source = row.Field<string>(Fields.Source);
            CreateDate = row.Field<DateTime>(Fields.CreateDate);
            CategoryId = row.Field<int?>(Fields.CategoryId);


            ParentCategoryId = row.Field<int?>(Fields.ParentCategoryId);
            GrandParentCategoryId = row.Field<int?>(Fields.GrandParentCategoryId);

            Keywords += row.Field<string>(Fields.MetaKeywords);

            ////if (!string.IsNullOrEmpty(row.Field<string>("StoreKeywords")))
            ////{
            ////    Keywords = row.Field<string>("StoreKeywords") + "," + Keywords;
            ////}


            Description = row.Field<string>(Fields.Description);

            dsDocument.Tables[1].AsEnumerable().ToList().ForEach(r => SelectedSubjects.Add(Convert.ToInt32(r[0])));

            DocumentFileList = DataHelper.EntityDataModel.DocumentFiles.Include("DocumentMediaType").Where(d => d.DocumentId == Id).ToList();

            //if (dsDocument.Tables.Count > 2)
            //{
            //    var file = new DocumentFile()
            //                   {
            //                       FileId = row.Field<int>("FileId"),
            //                       DocumentId = row.Field<int>("DocumentId"),
            //                       MediaTypeId = row.Field<int>("MediaTypeId"),
            //                       URL = row.Field<string>("URL"),
            //                       fileSize = row.Field<long>("fileSize"),
            //                       duration = row.Field<decimal>("duration"),
            //                       CreateDate = row.Field<DateTime>("CreateDate"),
            //                       //Description = row.Field<string>("Description"),
            //                       //                  IsMedia = row["IsMedia"] != DBNull.Value && row.Field<bool>("IsMedia"),
            //                       VolumeOrdinal = row.Field<byte>("VolumeOrdinal"),
            //                       VolumeComment = row.Field<string>("VolumeComment"),
            //                       DocumentMediaType =  new DocumentMediaType() { MediaIconPath = row.Field<string>("MediaIconPath")}
            //    //                    MediaIconPath = row.Field<string>("MediaIconPath"),
            //                   };



            //    dsDocument.Tables[2].AsEnumerable().ToList().ForEach(r => DocumentFileList.Add(file));

            //}
        }


        public DataTable GetMediaCategoryList()
        {
            return
                SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, Procedures.GetAllMediaCategories)
                    .
                    Tables[0];
        }

        public DataTable GetParentMediaCategoryList()
        {
            return
                SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text,
                                         Procedures.GetMediaCategoryParents)
                    .Tables[0];
        }


        public static IEnumerable<DocumentGetFeedResult> GetRSSFeed(int AuthorId)
        {
            return GetRSSFeed(AuthorId, 0);
        }

        public static IEnumerable<DocumentGetFeedResult> GetRSSFeed()
        {
            return GetRSSFeed(0, 0);
        }

        public static IEnumerable<DocumentGetFeedResult> GetRSSFeed(int AuthorId, int SubjectId, int? MediaTypeId = null)
        {
            var db = new MisesDBDataContext((new SqlConnection(ConnectionString)));

            return db.DocumentGetFeed(AuthorId, SubjectId, MediaTypeId, 60, null);

            //DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, Procedures.GetDocumentFeed,
            //                                      new SqlParameter(Parameters.AuthorId, AuthorId),
            //                                      new SqlParameter(Parameters.SubjectId, SubjectId));
            ////_results = ds.Tables[0].Rows.Count
            //return ds.Tables[0];
        }

        public bool BrowserSupportsHtml5(string userAgent)
        {
            if (string.IsNullOrEmpty(userAgent)) return false;
            return userAgent.Contains("AppleWebKit") || userAgent.Contains("Opera");
        }

        #region Edit Documents

        ///<summary>
        ///  Add New Document.
        ///</summary>
        ///<returns></returns>
        public int SaveDocument()
        {
            Identifier = new Guid();

            int oldId = Id;

            Id =
                Convert.ToInt32(
                    (SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, Procedures.SaveDocument,
                                             new SqlParameter(Parameters.DocumentId, Id),
                                             new SqlParameter(Parameters.Title, Title),
                                             new SqlParameter(Parameters.Display, Display),
                                             new SqlParameter(Parameters.Author1, Author1),
                                             new SqlParameter(Parameters.Author2, Author2),
                                             new SqlParameter(Parameters.PublicationInformation, PublicationInformation),
                                             new SqlParameter(Parameters.Identifier, Identifier),
                                             new SqlParameter(Parameters.Description, Description),
                                             new SqlParameter(Parameters.Keywords, Keywords),
                                             new SqlParameter(Parameters.Source, Source),
                                             new SqlParameter(Parameters.Featured, Featured),
                                             new SqlParameter(Parameters.CategoryId, CategoryId),
                                             new SqlParameter("@ProductId", ProductId),
                                             new SqlParameter(Parameters.ParentCategoryId, ParentCategoryId),
                                             new SqlParameter(Parameters.GrandParentCategoryId, GrandParentCategoryId)
                        )));

            if (oldId > 0)
                Id = oldId; // safety check


            // update story copy
            if (ProductId > 0)
            {
                const string updateQuery = "update ac_Products set Description = @Description WHERE ProductId = @ProductId";

                SqlHelper.ExecuteNonQuery(DataHelper.StoreConnectionString, CommandType.Text, updateQuery,
                                          new SqlParameter("@ProductId", ProductId),
                                          new SqlParameter("@Description", Description));
            }


            // Insert Subjects
            SelectedSubjects.ForEach(p => SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure,
                                                                    Procedures.AddNewDocumentSubjectLink,
                                                                    new SqlParameter(Parameters.DocumentId, Id),
                                                                    new SqlParameter(Parameters.SubjectId, p)));
            return Id;
        }

        public static void DeleteDocument(int Id)
        {
            if (Id == 0)
            {
                throw new Exception("Missing Guide Id!");
            }
            SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, Procedures.DeleteDocument,
                                      new SqlParameter(Parameters.DocumentId, Id));
        }

        public static byte[] GetMetaImage(int Id)
        {
            byte[] imageArray = null;
            try
            {
                object result = SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure,
                                                        Procedures.GetDocumentImage,
                                                        new SqlParameter(Parameters.DocumentId, Id));
                if (!Convert.IsDBNull(result))
                    imageArray = (byte[])result;
            }
            catch (InvalidCastException)
            {
            }
            return imageArray;
        }

        #endregion

        #region Browse Documents

        public new static Int32 GetNextId(int Id)
        {
            try
            {
                return
                    Convert.ToInt32(
                        SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                Procedures.GetNextDocumentId,
                                                new SqlParameter(Parameters.DocumentId, Id)).ToString());
            }
            catch
            {
                return
                    Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                            Procedures.GetLastDocumentId));
            }
        }

        public new static Int32 GetPreviousId(int Id)
        {
            try
            {
                return
                    Convert.ToInt32(
                        SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                Procedures.GetPreviousDocument,
                                                new SqlParameter(Parameters.DocumentId, Id)).ToString());
            }
            catch
            {
                return
                    Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                            Procedures.GetFirstDocument));
            }
        }

        public static Int32 FindDocumentByName(string Name)
        {
            try
            {
                return
                    Convert.ToInt32(
                        SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                Procedures.GetDocumentWithNameLike,
                                                new SqlParameter(Parameters.Name, Name)).ToString());
            }
            catch
            {
                return 0;
            }
        }

        #endregion

        #region Nested type: Fields

        private static class Fields
        {
            public const string DocumentId = "DocumentId";
            public const string Display = "Display";
            public const string Featured = "Featured";
            public const string Title = "Title";
            public const string Guid = "GUID";
            public const string Author1 = "Author1";
            public const string Author2 = "Author2";
            public const string AuthorName = "AuthorName";
            public const string PublicationInformation = "PublicationInformation";
            public const string Source = "Source";
            public const string PrivateComment = "PrivateComment";
            public const string CreateDate = "CreateDate";
            public const string CategoryId = "CategoryId";
            public const string ParentCategoryId = "ParentCategoryId";
            public const string GrandParentCategoryId = "GrandParentCategoryId";
            public const string MetaKeywords = "Keywords";
            public const string Description = "Description";
        }

        #endregion

        #region Nested type: Parameters

        private static class Parameters
        {
            public const string Name = "@Name";
            public const string DocumentId = "@DocumentId";
            public const string FileId = "@FileId";
            public const string AuthorId = "AuthorId";
            public const string Display = "@Display";
            public const string Author1 = "@Author1";
            public const string Author2 = "@Author2";
            public const string PublicationInformation = "@PublicationInformation";
            public const string PrivateComment = "@PrivateComment";
            public const string Identifier = "@Identifier";
            public const string Description = "@Description";
            public const string Keywords = "@Keywords";
            public const string Source = "@Source";
            public const string Featured = "@Featured";
            public const string CategoryId = "@CategoryId";
            public const string ParentCategoryId = "@ParentCategoryId";
            public const string GrandParentCategoryId = "@GrandParentCategoryId";
            public const string SubjectId = "@SubjectId";
            public const string Title = "@Title";
        }

        #endregion

        #region Nested type: Procedures

        private static class Procedures
        {
            public const string GetDocumentDetails = "dbo.DocumentDetails";

            public const string GetAllMediaCategories = "dbo.MediaCategoryGetAll";
            public const string GetMediaCategoryParents = "dbo.MediaCategoryGetParents";
            public const string GetDocumentFeed = "DocumentGetFeed";

            public const string SaveDocument = "dbo.DocumentSave";
            public const string AddNewDocumentSubjectLink = "dbo.spAddNewDocumentSubjectLink";
            public const string DeleteDocument = "dbo.DocumentDelete";
            public const string GetDocumentImage = "dbo.DocumentsGetImage";

            public const string GetNextDocumentId =
                "select top 1 DocumentId from Documents WHERE DocumentId > @DocumentId";

            public const string GetLastDocumentId = "select top 1 DocumentId from Documents ORDER BY Id DESC";

            public const string GetPreviousDocument =
                "select top 1 DocumentId from Documents WHERE DocumentId < @DocumentId";

            public const string GetFirstDocument = "select top 1 DocumentId from Documents ORDER BY Id ";

            public const string GetDocumentWithNameLike =
                "select top 1 DocumentId from Documents where Title LIKE '%' + @Name + '%'";
        }

        #endregion

        #region Properties

        public List<DocumentFile> DocumentFileList = new List<DocumentFile>();
        public List<int> SelectedSubjects = new List<int>();
        private DataSet dsDocument;
        //Public Length As Integer


        // Media Information

        public Image MetaImage { get; set; }
        public string CoverImage { get; set; }

        public string PublicationInformation { get; set; }
        public string Source { get; set; }
        public int Author1 { get; set; }
        public int? Author2 { get; set; }
        public string AuthorName { get; set; }
        public string CoAuthorName { get; set; }
        public bool Display { get; set; }
        public bool Featured { get; set; }
        public int? CategoryId { get; set; }
        public int? ParentCategoryId { get; set; }
        public int? GrandParentCategoryId { get; set; }
        public int? ProductId { get; set; }

        public DataTable dtMediaFiles
        {
            get { return dsDocument.Tables.Count > 2 ? dsDocument.Tables[2] : new DataTable(); }
        }

        public string ISBN { get; set; }

        #endregion

        //Public Shared Function GetNewLiterature() As DataSet
        //    Return SqlHelper.ExecuteDataset(BusinessBase.ConnectionString, "DocumentGetNewLiterature")
        //End Function

        public static ContentDocument GetDocumentByFileId(int fileId)
        {
            var doc = new ContentDocument();
            doc.DocumentFromFileId(fileId);
            return doc;
        }
    }
}