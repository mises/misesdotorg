#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using Mises.Data;


#endregion

namespace Mises.Domain.Documents
{
    public enum DocumentType
    {
        Page,
        Content,
        DailyArticle,
        Product,
        Calendar,
        Media,
        PDF,
        Periodical
    }

    [Serializable]
    [DataContract]
    public class Document : DataHelper
    {
        private DocumentType mDocumentType;
        private Guid mIdentifier = Guid.NewGuid();
        //private string mTypeString;

        #region Properties

        public string Contents { get; set; }
        public DateTime CreateDate { get; set; }

        private string customUrl { get; set; }
        public int Id { get; set; }
        public string Keywords { get; set; }
        protected Guid? NextGUID { get; set; }
        protected Guid? PreviousGUID { get; set; }


        public string DocumentTypeString
        {
            get { return mDocumentType.ToString(); }
        }

        public DocumentType DocumentType
        {
            get { return mDocumentType; }
            set { mDocumentType = value; }
        }

        [DataMember]
        public string URL
        {
            get
            {
                if (! string.IsNullOrEmpty(customUrl))
                {
                    return customUrl;
                }
                if (Id > 0)
                {
                    switch (DocumentType)
                    {
                        case DocumentType.DailyArticle:
                            return string.Format("/daily/{0}", (Id));
                        case DocumentType.Product:
                            return string.Format("http://store.mises.org/Product.aspx?ProductId={0}", (Id));
                        case DocumentType.Calendar:
                            return string.Format("/event/{0}", (Id));
                        case DocumentType.PDF:
                            return ! string.IsNullOrEmpty(Contents) ? Contents : string.Format("/resources/{0}", Id);
                        case DocumentType.Media:
                        case DocumentType.Content:
                            return string.Format("/document/{0}", Id);

                            //Case MisesWeb.DocumentType.Page  
                            // Returns CustomURL
                        default:
                            return string.Format("/resources/{0}", Identifier);
                    }
                }
                else
                {
                    return string.Format("/resources/{0}", Identifier);
                }
            }
            set { customUrl = value; }
        }


        public string Title { get; set; }

        public Guid Identifier
        {
            get { return mIdentifier; }
            set { mIdentifier = value; }
        }

        public virtual string NextURL
        {
            get { return ! string.IsNullOrEmpty(NextGUID.ToString()) ? string.Format("/resources/{0}", NextGUID) : ""; }
        }

        public virtual string PreviousURL
        {
            get
            {
                return ! string.IsNullOrEmpty(PreviousGUID.ToString())
                           ? string.Format("/resources/{0}", PreviousGUID)
                           : "";
            }
        }

        [DataMember]
        public string Description { get; set; }

        public void SetDocumentType(string type)
        {
            DocumentType = GetDocumentType(type);
        }


        public static DocumentType GetDocumentType(string type)
        {
            switch (type)
            {
                case "Page":
                    return DocumentType.Page;
                case "Content":
                    return DocumentType.Content;
                case "DailyArticle":
                    return DocumentType.DailyArticle;

                case "Product":
                    return DocumentType.Product;

                case "Calendar":
                    return DocumentType.Calendar;

                case "Media":
                    return DocumentType.Media;

                case "PDF":
                    return DocumentType.PDF;

                case "Periodical":
                    return DocumentType.Periodical;

                default:
                    return DocumentType.Periodical;
            }
        }

        #endregion

        public Document()
        {
        }


        public Document(Guid Identifier)
        {
            var db = new MisesDBDataContext((new SqlConnection(ConnectionString)));

            var content = db.GetContentbyGUID(Identifier).FirstOrDefault();

            if (content == null) return;

            Id = content.Id;
            this.Identifier = Identifier;
            Title = content.Title;
            Description = content.Description;
            Contents = content.Contents;
            Keywords = content.Keywords;
            customUrl = content.URL;
            if (content.CreateDate != null) CreateDate = (DateTime) content.CreateDate;
            SetDocumentType(content.DocumentType);
            PreviousGUID = content.PreviousGUID;
            NextGUID = content.NextGUID;
        }


        public static int Delete(Guid Identifier)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "dbo.PageDelete",
                                             new SqlParameter("GUID", Identifier));
        }

        public static string GetPageContentByName(string ContentName)
        {
            return
                Convert.ToString(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure,
                                                         "dbo.PageContentGetDetail",
                                                         new SqlParameter("@ContentName", ContentName)));
        }

        #region Browsing

        public static Guid GetPageGUID(string Page, string Path)
        {
            object value =
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "dbo.PagesGetGUID",
                                                    new SqlParameter("@PageName", Page), new SqlParameter("@Path", Path));
            return value != null ? new Guid(value.ToString()) : Guid.Empty;
        }

        ///<summary>
        ///  Gets the page id from the GUID.
        ///</summary>
        ///<param name="Ident">The ident.</param>
        ///<returns></returns>
        public static int GetPageIdfromGUID(Guid Ident)
        {
            object value = (SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.PageGetIdByGUID", new SqlParameter("@GUID", Ident.ToString())));

            return value == null ? 0 : Convert.ToInt32(value.ToString());
        }

        ///<summary>
        ///  Gets the GUID from the Page Id.
        ///</summary>
        ///<param name="PageId">The page id.</param>
        ///<returns></returns>
        public static Guid GetGUID(int PageId)
        {
            object value =
                SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure,
                                                    "dbo.PageGetGUIDByPageId", new SqlParameter("@PageId", PageId));

            return value != null ? new Guid(value.ToString()) : Guid.Empty;
        }

        public static Int32 GetNextId(int PageId)
        {
            try
            {
                return
                    Convert.ToInt32(
                        SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                "select top 1 PageId from Page where PageId > @PageId",
                                                new SqlParameter("@PageId", PageId)).ToString());
            }
            catch
            {
                return
                    Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                            "select top 1 PageId from Page ORDER BY PageId DESC"));
            }
        }

        public static Int32 GetPreviousId(int PageId)
        {
            try
            {
                return
                    Convert.ToInt32(
                        SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                "select top 1 PageId from Page where PageId < @PageId ORDER BY PageId DESC",
                                                new SqlParameter("@PageId", PageId)).ToString());
            }
            catch
            {
                return
                    Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text,
                                                            "select top 1 PageId from Page ORDER BY PageId "));
            }
        }

        #endregion
    }
}