﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mises.Data;

namespace Mises.Domain.Documents
{
    public class DocumentRepository
    {
        protected MisesModel db { get; set; }

        public DocumentRepository(MisesModel db)
        {
            this.db = db;
        }


        public int AddFile(DocumentFile media)
        {
            if (media.DocumentId == 0)
            {
                throw new ArgumentNullException("DocumentId");
            }

            media.CreateDate = DateTime.Now;

            db.AddToDocumentFiles(media);
            db.SaveChanges();

            int fileId = media.FileId;

            return fileId;
        }

        public int UpdateFile(int fileId, int documentId, int mediaTypeId, byte volumeOrdinal, string volumeComment, string URL, long fileSize)
        {
            if (fileId == 0)
            {
                throw new ArgumentNullException("FileId");
            }

            var file = db.DocumentFiles.Single(f => f.FileId == fileId);

            file.DocumentId = documentId;
            file.MediaTypeId = mediaTypeId;
            file.VolumeOrdinal = volumeOrdinal;
            file.VolumeComment = volumeComment;
            file.URL = URL;
            file.fileSize = fileSize;

            db.SaveChanges();



            return fileId;
        }

        public void DeleteFile(int fileId)
        {
            var db = DataHelper.EntityDataModel;
            var file = db.DocumentFiles.Single(f => f.FileId == fileId);
            db.DocumentFiles.DeleteObject(file);
            db.SaveChanges();
        }

        public List<Data.Document> GetMediaFeed(int authorId, int mediaTypeId, int categoryId)
        {
            var query =
                db.Documents.Include("DocumentAuthor").Include("DocumentAuthor2").Include("DocumentFiles").Include(
                    "DocumentFiles.DocumentMediaType").Include("MediaCategory").NoTracking().Where(docs => docs.Display && docs.DocumentFiles.Any(df => df.DocumentMediaType.IsMedia && df.Display));

            if (categoryId == 210)
            {
                categoryId = 0;
            }

            if (authorId > 0)
            {
                query = query.Where(a => a.DocumentAuthor.AuthorId == authorId || a.DocumentAuthor2.AuthorId == authorId);
            }

            if (mediaTypeId > 0)
            {
                query = query.Where(a => a.DocumentFiles.Any(df => df.MediaTypeId == mediaTypeId));
            }

            if (categoryId > 0)
            {
                query = query.Where(a => a.MediaCategory.CategoryId == categoryId);
            }

            IQueryable<Data.Document> documents = query.OrderByDescending(d => d.CreateDate);

            if (categoryId == 0 && authorId == 0 && mediaTypeId == 0)
            {
                // restrict maximum number of results when no filter is provided to 50
                documents = documents.Take(50);
            }

            documents.ToList().ForEach(doc =>
                                  {
                                      if (string.IsNullOrWhiteSpace(doc.Description))
                                      { doc.Description = doc.PublicationInformation; }

                                  });

            return documents.ToList();
        }

        public List<Data.Document> GetLiteratureFeed(int authorId, int mediaTypeId, int subjectId, string subject)
        {
            var query =
                db.Documents.Include("DocumentAuthor").Include("DocumentAuthor2").Include("DocumentFiles").Include(
                    "DocumentFiles.DocumentMediaType").Include("MediaCategory").NoTracking().Where(docs => docs.Display && docs.DocumentFiles.Any(df => !df.DocumentMediaType.IsMedia && df.Display));

            if (authorId > 0)
            {
                query = query.Where(a => a.DocumentAuthor.AuthorId == authorId || a.DocumentAuthor2.AuthorId == authorId);
            }

            if (mediaTypeId > 0)
            {
                query = query.Where(a => a.DocumentFiles.Any(df => df.MediaTypeId == mediaTypeId));
            }

            if (subjectId > 0)
            {
                query = query.Where(a => a.DocumentSubjectLink.Any(s=> s.SubjectID == subjectId));
            }

            IQueryable<Data.Document> documents = query.OrderByDescending(d => d.CreateDate);

            if (subjectId == 0 && authorId == 0 && mediaTypeId == 0)
            {
                // restrict maximum number of results when no filter is provided to 50
                documents = documents.Take(50);
            }

            documents.ToList().ForEach(doc =>
            {
                if (string.IsNullOrWhiteSpace(doc.Description))
                { doc.Description = doc.PublicationInformation; }

            });

            return documents.ToList();
        }
    }
}
