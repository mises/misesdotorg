#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using Mises.Domain.Feeds;

#endregion

namespace Mises.Domain.Documents
{
    ///<summary>
    ///  TODO
    ///</summary>
    public class DocumentsFeed : INewsAuthorFeed
    {
        #region INewsAuthorFeed Members

        public SyndicationFeedFormatter feed(string format, int AuthorId)
        {
            //Setting up the feed formatter.
            // TODO: FIX to relative URL
            var feed = new SyndicationFeed(
                "Mises Institute Daily Articles",
                "Daily Articles from The Mises Institute on Austrian Economics and Libertarianism",
                new Uri("http://mises.freecapitalists.org//articles.aspx"));

            var items = new List<SyndicationItem>();

            feed.Items = items;

            feed.Authors.Add(new SyndicationPerson("webmaster@freecapitalists.org"));
            feed.Categories.Add(new SyndicationCategory("Articles"));
            feed.Categories.Add(new SyndicationCategory("Economics"));

            // TODO: FIX to relative URL
            feed.ImageUrl = new Uri("http://mises.freecapitalists.org//images/DailyArticles.gif");
            feed.Copyright = new TextSyndicationContent("Copyright 2002-2008 Mises Institute");


            if (AuthorId > 0 && feed.Items.Any())
            {
                feed.Title =
                    new TextSyndicationContent("Daily Article archives for " + feed.Items.First().Authors[0].Name);
            }


            // Processing and serving the feed according to the required format
            // i.e. either RSS or Atom.
            if (format == "rss")
                return new Rss20FeedFormatter(feed);
            if (format == "atom")
                return new Atom10FeedFormatter(feed);
            return null;
        }

        public SyndicationFeedFormatter FullTextFeed(string format, int authorId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

//Ends Namespace