#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Mises.Data;

#endregion

namespace Mises.Domain.Documents
{
    /// <summary>
    /// </summary>
    public class DocumentAuthors
    {
        ///<summary>
        ///  Gets the authors.
        ///</summary>
        ///<returns></returns>
        public static DataSet GetAuthors()
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            "dbo.DocumentAuthorsGetList");
        }

        public static DataSet GetAuthorsLastNameFirst()
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            "dbo.DocumentAuthorsGetList",
                                            new SqlParameter("ContentType", "LastNameFirst"));
        }

        ///<summary>
        ///  Add New Author
        ///</summary>
        ///<param name = "FirstName"></param>
        ///<param name = "MiddleName"></param>
        ///<param name = "LastName"></param>
        ///<remarks>
        ///</remarks>
        public static bool AddNewAuthor(string FirstName, string MiddleName, string LastName)
        {
            return SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                             "INSERT INTO DocumentAuthors (AuthorFirst, AuthorMiddle, AuthorLast) VALUES (@AuthorFirst, @AuthorMiddle, @AuthorLast)",
                                             new SqlParameter("@AuthorFirst", FirstName),
                                             new SqlParameter("@AuthorMiddle", MiddleName),
                                             new SqlParameter("@AuthorLast", LastName)) > 0;
        }

        public static int ReplaceAuthor(int OldAuthorId, int NewAuthorId)
        {
            if (OldAuthorId == 0 || NewAuthorId == 0)
            {
                throw new Exception("Missing AuthorId Field");
            }
            return SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["Public"].ConnectionString,
                                             CommandType.StoredProcedure, "dbo.ReplaceGuideAuthor",
                                             new SqlParameter("@OldAuthorId", OldAuthorId),
                                             new SqlParameter("@NewAuthorId", NewAuthorId));
        }

        public static DataSet GetAuthorBioByArticleId(int ArticleId)
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            "dbo.DocumentAuthorsGetDetail",
                                            new SqlParameter("@ArticleId", ArticleId));
        }

        public static DataSet GetAuthorBioByAuthorId(int AuthorId)
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            "dbo.DocumentAuthorsGetDetail",
                                            new SqlParameter("@AuthorId", AuthorId));
        }

        public static DocumentAuthor GetAuthorByAuthorId(int AuthorId)
        {
            return DataHelper.EntityDataModel.DocumentAuthors.Single(a => a.AuthorId == AuthorId);

        }
    }
}