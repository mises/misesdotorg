#region

using System;
using System.Configuration;
using System.Data.Odbc;

#endregion

namespace Mises.Domain.Old
{
    public class BlogComment
    {
        public string Comment;
        public string Email;
        public string Name;
        public int PostId;
        public string URL;

        public void SubmitComment(string IPaddress)
        {
            string SQL = null;
            SQL = "INSERT INTO mt_comment ";
            SQL += " (comment_blog_id, comment_entry_id, comment_ip, comment_author, comment_email, ";
            SQL += " comment_text, comment_url, comment_created_on, comment_visible) ";
            SQL += " VALUES (3," + PostId + ", '" + IPaddress + "','" + Name + " ','" + Email + "','" + Comment + "', '" +
                   URL + "','" + DateTime.Now + "', 1)";

            var sqlCmd = new OdbcCommand(SQL,
                                         new OdbcConnection(
                                             ConfigurationManager.AppSettings["MySqlDB_ConnectionString"]));

            sqlCmd.Connection.Open();
            sqlCmd.ExecuteNonQuery();
            sqlCmd.Connection.Close();

            //Dim SQL As String
            //SQL = "INSERT INTO mt_comment "
            //SQL &= " (comment_blog_id, comment_entry_id, comment_ip, comment_author, comment_email, "
            //SQL &= " comment_text, comment_url, comment_created_on,comment_visible) "
            //SQL &= " VALUES (3, @PostId, @IPaddress,@Name,@Email,@Comment,@URL,@theDate, 1)"


            //sqlCmd.Parameters.AddWithValue("@PostId", Odbc.OdbcType.Int).Value = PostId
            //sqlCmd.Parameters.AddWithValue("@URL", Odbc.OdbcType.VarChar).Value = URL
            //sqlCmd.Parameters.AddWithValue("@Name", Odbc.OdbcType.VarChar).Value = Name
            //sqlCmd.Parameters.AddWithValue("@Comment", Odbc.OdbcType.VarChar).Value = Comment
            //sqlCmd.Parameters.AddWithValue("@Email", Odbc.OdbcType.VarChar).Value = Email
            //sqlCmd.Parameters.AddWithValue("@IPaddress", Odbc.OdbcType.VarChar).Value = IPaddress
            //sqlCmd.Parameters.AddWithValue("@theDate", Odbc.OdbcType.DateTime).Value = Date.Now
        }
    }
}