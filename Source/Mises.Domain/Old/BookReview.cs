#region

using System;
using System.Data;
using System.Data.SqlClient;
using Mises.Data;
using Mises.Domain.Utility;

#endregion

namespace Mises.Domain.Old
{
    public class BookReview
    {
        #region Variables

        public int BookId;
        public string Location;
        public int Rating;
        public string Review;
        public int ReviewId;
        public string ScreenName;
        public string Title;
        public int results;

        #endregion

        public void AddReview(string IPaddress)
        {
            if (BookId == 0)
            {
                throw new Exception("Missing BookId");
            }

            SqlHelper.ExecuteScalar(DataHelper.ConnectionString, CommandType.StoredProcedure, "dbo.spAddBookReview",
                                    new SqlParameter("@BookId", BookId), new SqlParameter("@Rating", Rating),
                                    new SqlParameter("@Title", Title), new SqlParameter("@Review", Review),
                                    new SqlParameter("@ScreenName", ScreenName), new SqlParameter("@Location", Location));


            string message = "You have a new book review." + Environment.NewLine;
            message += "BookId: " + BookId + Environment.NewLine;
            message += "Location: " + Location + Environment.NewLine;
            message += "Rating: " + Rating + Environment.NewLine;
            message += "ScreenName: " + ScreenName + Environment.NewLine;
            message += "Title: " + Title + Environment.NewLine;
            message += "Review: " + Review + Environment.NewLine;
            message += "IP Address: " + IPaddress + Environment.NewLine + Environment.NewLine;
            // TODO: FIX to relative URL 
            message += "Book URL: " + "http://store.mises.org/product1.aspx?Product_ID=" + BookId + Environment.NewLine;
            message += "Moderate: " + "http://mises.freecapitalists.org//manager/Reviews/" + Environment.NewLine;
            EmailHelper.SendMail("books@freecapitalists.org", "tucker@freecapitalists.org", "New Book Review for " + Title, message);
        }

        public DataTable GetReviews(int BookId)
        {
            return
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                         "dbo.spGetBookReviews", new SqlParameter("@BookId", BookId)).Tables[0];
        }

        public DataTable GetReview(int ReviewId)
        {
            return
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text,
                                         "SELECT * FROM BookReviews WHERE ReviewId = @ReviewId",
                                         new SqlParameter("@ReviewId", ReviewId)).Tables[0];
        }


        public DataTable GetReviews()
        {
            return GetReviews("ReviewDate");
        }

//INSTANT C# NOTE: C# does not support optional parameters. Overloaded method(s) are created above.
//ORIGINAL LINE: Public Function GetReviews (Optional ByVal ORDER As String = "ReviewDate") As DataTable
        public DataTable GetReviews(string ORDER)
        {
            if (string.IsNullOrEmpty(ORDER))
            {
                ORDER = "ReviewDate";
            }
            return
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text,
                                         "SELECT * FROM BookReviews ORDER BY " + ORDER).Tables[0];
        }

        public DataTable Search(string strSearch)
        {
            string strSQL = "SELECT  * FROM BookReviews WHERE review LIKE '%" + strSearch + "%' OR title LIKE '%" +
                            strSearch + "%' OR screenname LIKE '%" + strSearch + "%'  ORDER BY ReviewId DESC";
            DataTable dt = null;
            dt = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text, strSQL).Tables[0];
            results = dt.Rows.Count;
            return dt;
        }


        public void DeleteReview(int ReviewId)
        {
            if (ReviewId == 0)
            {
                throw new Exception("Missing review ID to delete!");
            }
            SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                      "DELETE FROM BookReviews WHERE ReviewId = " + ReviewId);
        }
    }
}