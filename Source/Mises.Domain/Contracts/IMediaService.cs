﻿#region

using System;
using System.Collections.Generic;
using System.ServiceModel;

#endregion

namespace Mises.Domain.Contracts
{
    [ServiceContract]
    public interface IMediaService
    {
        ISearchProvider SearchProvider { get; set; }

        [OperationContract]
        PaginableResult<MisesDTO> Search(String term, Int32 pageIndex, Int32 pageSize);

        #region Articles

        [OperationContract]
        PaginableResult<ArticleSummaryDTO> GetArticleSummaries(IArticleFilter filter, Int32 pageIndex, Int32 pageSize);

        #endregion

        #region Categories

        [OperationContract]
        PaginableResult<CategorySummaryDTO> GetCategories(ICategoryFilter filter, Int32 pageIndex = 0,
                                                          Int32 pageSize = Int32.MaxValue);

        #endregion

        #region Subjects

        [OperationContract]
        SubjectDTO GetSubject(Int32 id, Int32 pageIndex, Int32 pageSize);

        [OperationContract]
        PaginableResult<SubjectSummaryDTO> GetSubjects(ISubjectFilter filter = null, Int32 pageIndex = 0,
                                                       Int32 pageSize = Int32.MaxValue);

        #endregion

        #region Documents

        [OperationContract]
        PaginableResult<DocumentDTO> GetDocuments(IDocumentFilter filter, Int32 pageIndex, Int32 pageSize);

		[OperationContract]
		PaginableResult<DocumentDTO> GetDocuments(
			string searchString
			, int categoryId
			, int subjectId
			, int authorId
			, int[] mediaTypeIds
			, Int32 pageIndex, Int32 pageSize);

        [OperationContract]
        DocumentDTO GetDocumentFromMediaId(Int32 id);

        [OperationContract]
        DocumentDTO GetDocument(Int32 id);

        [OperationContract]
        DocumentDTO GetRandomVideo();

        #endregion

        #region Authors

        [OperationContract]
        AuthorDTO GetAuthor(Int32 id, Int32 articlePageIndex, Int32 articlePageSize, Int32 documentPageIndex,
                            Int32 documentPageSize);

        [OperationContract]
        IEnumerable<AuthorSummaryDTO> GetAuthorSummaries();

        [OperationContract]
        PaginableResult<AuthorDTO> GetAuthors(IAuthorFilter filter = null, Int32 pageIndex = 0,
                                              Int32 pageSize = Int32.MaxValue);

        [OperationContract]
        IDictionary<Char, Int32> GetAuthorCountsBySurnameInitial();

        [OperationContract]
        MisesDTO GetLatestForAuthor(Int32 authorId);

        #endregion
    }
}