﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Mises.Domain.Contracts
{
    public interface IAuthorFilter
    {
    }

    public class AuthorSurnameFilter : IAuthorFilter
    {
        public AuthorSurnameFilter(String startsWith)
        {
            Term = startsWith.ToLower();
        }

        public String Term { get; private set; }
    }

    public class AuthorSurnameInitialFilter : IAuthorFilter //TODO could drop this and just use above
    {
        public AuthorSurnameInitialFilter(Char letter)
        {
            Letter = letter;
        }

        public Char Letter { get; private set; }
    }

    public class AuthorIdListFilter : IAuthorFilter
    {
        public AuthorIdListFilter(IEnumerable<Int32> idList)
        {
            IdList = idList;
        }

        public IEnumerable<Int32> IdList { get; set; }
    }
}