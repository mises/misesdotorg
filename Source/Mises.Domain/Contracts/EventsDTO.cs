﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mises.Domain.Contracts
{
	public class EventDTO
	{
		public Int32 EventId { get; set; }
		public DateTime StartDate { get; set; }
		public String Title { get; set; }
		public String Location { get; set; }
		public String IntroText { get; set; }
	}
}
