﻿namespace Mises.Domain.Contracts
{
    public interface IServiceLayer
    {
        IMediaService MediaService { get; }
    }
}