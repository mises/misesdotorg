﻿#region

using System;
using Mises.Data;

#endregion

namespace Mises.Domain.Contracts
{
    //TODO refactor the domain (POCO template EF etc) and remove dependency to Mises.Data here and elsewhere its bled...
    public interface IMediaRepository
    {
        Document GetDocumentFromFileId(Int32 id);
        Document GetDocument(Int32 id);
        PaginableResult<Document> GetDocuments(IDocumentFilter filter, Int32 pageIndex, Int32 pageSize);

		PaginableResult<Document> GetDocuments(
			string searchString
			, int categoryId
			, int subjectId
			, int authorId
			, int[] mediaTypeIds
			, Int32 pageIndex, Int32 pageSize);

        Document GetRandomVideo();

        MediaCategory GetCategory(Int32 id);
        PaginableResult<MediaCategory> GetCategories(ICategoryFilter filter, Int32 pageIndex, Int32 pageSize);

        DocumentSubjects GetSubject(Int32 id);
        PaginableResult<DocumentSubjects> GetSubjects(ISubjectFilter filter, Int32 pageIndex, Int32 pageSize);
    }
}