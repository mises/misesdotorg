﻿#region

using System;

#endregion

namespace Mises.Domain.Contracts
{
    public interface IArticleFilter
    {
    }

    public class ArticlesByAuthorFilter : IArticleFilter
    {
        public ArticlesByAuthorFilter(Int32 id)
        {
            Id = id;
        }

        public Int32 Id { get; private set; }
    }
}