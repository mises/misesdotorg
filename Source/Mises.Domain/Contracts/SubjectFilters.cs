﻿#region

using System;

#endregion

namespace Mises.Domain.Contracts
{
    public interface ISubjectFilter
    {
    }

    public class SubjectNameFilter : ISubjectFilter
    {
        public SubjectNameFilter(String term)
        {
            Term = term;
        }

        public String Term { get; set; }
    }
}