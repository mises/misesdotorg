﻿#region

using System;
using System.Collections.Generic;

using Mises.Data;
using System.Data.Entity.Core.Objects.DataClasses;

#endregion

namespace Mises.Domain.Contracts
{
    public interface IAuthorRepository
    {
        PaginableResult<DailyArticle> GetArticleSummaries(IArticleFilter filter, Int32 pageIndex, Int32 pageSize);

        dynamic GetLatestContribution(Int32 authorId);

        dynamic GetAuthor(Int32 id);
        IEnumerable<MediaGetAuthorsAlpha_Result> GetAuthorSummaries();
        PaginableResult<DocumentAuthor> GetAuthors(IAuthorFilter filter, Int32 pageIndex, Int32 pageSize);

        IDictionary<Char, Int32> GetCountsBySurnameInitial();
    }
}