﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Mises.Domain.Contracts
{
    public interface IDocumentFilter
    {
        Int32? MaxResults { get; set; }
    }

    public abstract class BaseDocumentFilter : IDocumentFilter
    {
        public BaseDocumentFilter(Int32? maxResults = null)
        {
            MaxResults = maxResults;
        }

        #region IDocumentFilter Members

        public Int32? MaxResults { get; set; }

        #endregion
    }

    public abstract class DocumentsByEntityFilter : BaseDocumentFilter
    {
        public DocumentsByEntityFilter(Int32 id)
        {
            Id = id;
        }

        public Int32 Id { get; private set; }
    }

    public class DocumentMediaSearchFilter : DocumentsByEntityFilter
    {
        public DocumentMediaSearchFilter(Int32 id) : base(id)
        {
        }

        public String SearchTerm { get; set; }
    }

    public class DocumentsBySubjectFilter : DocumentsByEntityFilter
    {
        public DocumentsBySubjectFilter(Int32 id) : base(id)
        {
        }
    }

    public class DocumentsByCategoryFilter : DocumentsByEntityFilter
    {
        public DocumentsByCategoryFilter(Int32 id) : base(id)
        {
        }
    }

    public class DocumentsByAuthorFilter : DocumentsByEntityFilter
    {
        public DocumentsByAuthorFilter(Int32 id) : base(id)
        {
        }
    }

    public class DocumentsWithMediaFilter : BaseDocumentFilter
    {
        public DocumentsWithMediaFilter(Int32? maxResults = null) : base(maxResults)
        {
        }
    }

    public class DocumentsWithVideoFilter : DocumentsWithMediaFilter
    {
    }

    public class DocumentsWithAudioFilter : DocumentsWithMediaFilter
    {
    }

    public class DocumentsWithLiteratureFilter : DocumentsWithMediaFilter
    {
    }

    public class DocumentMediaIdListFilter : BaseDocumentFilter
    {
        public DocumentMediaIdListFilter(IEnumerable<Int32> idList)
        {
            IdList = idList;
        }

        public IEnumerable<Int32> IdList { get; set; }
    }

    public class DocumentIdListFilter : BaseDocumentFilter
    {
        public DocumentIdListFilter(IEnumerable<Int32> idList)
        {
            IdList = idList;
        }

        public IEnumerable<Int32> IdList { get; set; }
    }

    public class DocumentAuthorIdListFilter : DocumentIdListFilter
    {
        public DocumentAuthorIdListFilter(IEnumerable<Int32> idList) : base(idList)
        {
        }
    }
}