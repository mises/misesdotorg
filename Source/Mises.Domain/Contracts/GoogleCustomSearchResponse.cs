﻿namespace Mises.Domain.Contracts.Google
{
    public class GoogleCustomSearchResponse
    {
        public string Kind { get; set; }
        public Url Url { get; set; }
        public Queries Queries { get; set; }
        public Context Context { get; set; }
        public Item[] Items { get; set; }
    }

    public class Context
    {
        public string Title { get; set; }
    }

    public class Item
    {
        public string Kind { get; set; }
        public string Title { get; set; }
        public string HtmlTitle { get; set; }
        public string Link { get; set; }
        public string DisplayLink { get; set; }
        public string Snippet { get; set; }
        public string HtmlSnippet { get; set; }
        public string CacheId { get; set; }
        public Pagemap Pagemap { get; set; }
    }

    public class Metatag
    {
        public string ApplicationName { get; set; }
        public string MsapplicationStarturl { get; set; }
        public string MsapplicationTooltip { get; set; }
        public string MsapplicationTask { get; set; }
        public string ContentLanguage { get; set; }
        public string Framework { get; set; }
        public string Author { get; set; }
        public string Moddate { get; set; }
        public string Creationdate { get; set; }
        public string Creator { get; set; }
        public string Producer { get; set; }
        public string Viewport { get; set; }
    }

    public class NextPage
    {
        public string Title { get; set; }
        public int TotalResults { get; set; }
        public string SearchTerms { get; set; }
        public int Count { get; set; }
        public int StartIndex { get; set; }
        public string InputEncoding { get; set; }
        public string OutputEncoding { get; set; }
        public string Safe { get; set; }
        public string Cx { get; set; }
    }

    public class Pagemap
    {
        public Metatag[] Metatags { get; set; }
    }

    public class Queries
    {
        public NextPage[] NextPage { get; set; }
        public Request[] Request { get; set; }
    }

    public class Request
    {
        public string Title { get; set; }
        public int TotalResults { get; set; }
        public string SearchTerms { get; set; }
        public int Count { get; set; }
        public int StartIndex { get; set; }
        public string InputEncoding { get; set; }
        public string OutputEncoding { get; set; }
        public string Safe { get; set; }
        public string Cx { get; set; }
    }

    public class Url
    {
        public string Type { get; set; }
        public string Template { get; set; }
    }
}