﻿#region

using System;

#endregion

namespace Mises.Domain.Contracts
{
    public class ContentDocumentDTO
    {
        public String Title { get; set; }
        public String Description { get; set; }
        public Int32 AuthorId { get; set; }
        public String AuthorName { get; set; }
        public Int32 CategoryId { get; set; }
        public String CategoryTitle { get; set; }
        public DateTime CreatedDate { get; set; }

        public Int32 FileId { get; set; }
        public String VideoType { get; set; }
        public String VideoUrl { get; set; }
        public String PosterUrl { get; set; }
    }
}