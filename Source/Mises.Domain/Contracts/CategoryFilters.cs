﻿#region

using System;

#endregion

namespace Mises.Domain.Contracts
{
    public interface ICategoryFilter
    {
    }

    public class TopLevelCategoryFilter : ICategoryFilter
    {
    }

    public class CategoryAncestorsFilter : ICategoryFilter
    {
        public CategoryAncestorsFilter(Int32 id, Boolean includeTarget = true)
        {
            Id = id;
            IncludeTarget = includeTarget;
        }

        public Int32 Id { get; set; }
        public Boolean IncludeTarget { get; set; }
    }

    public class CategoryChildrenFilter : ICategoryFilter
    {
        public CategoryChildrenFilter(Int32 id)
        {
            Id = id;
        }

        public Int32 Id { get; set; }
    }

    public class CategoryNameFilter : ICategoryFilter
    {
        public CategoryNameFilter(String term)
        {
            Term = term;
        }

        public String Term { get; set; }
    }
}