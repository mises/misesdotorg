﻿#region

using System;
using System.Linq;

#endregion

namespace Mises.Domain.Contracts
{
    public enum MediaPreferenceTypes
    {
        Undefined = 0,
        Video,
        Audio,
        Literature
    }

    // Stick these here for now (would like to see them as attributes of the enum below)...
    // maybe update mimetype in db and use instead?
    public static class VideoTypes
    {
        public const String Undefined = "undefined";
        public const String Webm = "video/webm; codecs='vp8, vorbis'";
        public const String Ogg = "video/ogg; codecs='theora, vorbis'";
        public const String Swf = "video/swf";
        public const String Flv = "video/flv"; //also support for F4v?
        public const String Mp4 = "video/mp4; codecs='h.264, aac'";
        public const String Wmv = "video/mpeg";
        public const String Youtube = "youtube";
        public const String Stream = "video/wma";

        public static Int32[] _intArray;
        public static String[] _stringArray;

        public static Int32[] AsArray()
        {
            // Encoding preferences...WebM, Ogg, Flash, then Mp4 and Wmv (where does youtube rate?)
            return _intArray ?? (_intArray = new[]
                                                 {
                                                     (Int32) MediaTypes.Mp4, (Int32) MediaTypes.Youtube,
                                                     (Int32) MediaTypes.Wmv,
                                                     (Int32) MediaTypes.Stream
                                                 });
        }

        public static Boolean InArray(Int32 mediaTypeId)
        {
            return AsArray().Any(t => t == mediaTypeId);
        }

        public static String[] AsStringArray()
        {
            // Encoding preferences...WebM, Ogg, Flash, then Mp4 and Wmv (where does youtube rate?)
            return _stringArray ?? (_stringArray = new[] {Mp4, Youtube, Wmv, Stream});
        }

        public static String ToMimeType(Int32 mediaTypeId)
        {
            switch (mediaTypeId)
            {
                case (Int32) MediaTypes.Mp4:
                    return Mp4;
                case (Int32) MediaTypes.Wmv:
                    return Wmv;
                case (Int32) MediaTypes.Youtube:
                    return Youtube;
                case (Int32) MediaTypes.Stream:
                    return Stream;
                default:
                    return Undefined;
            }
        }
    }

    public static class AudioTypes
    {
        public const String Undefined = "undefined";
        public const String Mp3 = "audio/mp3";
        public const String Mp4a = "audio/mpeg";

        public static Int32[] _intArray;

        public static String[] _stringArray;

        public static Int32[] AsArray()
        {
            return _intArray ?? (_intArray = new[] {(Int32) MediaTypes.Mp3, (Int32) MediaTypes.Mp4a});
        }

        public static String[] AsStringArray()
        {
            return _stringArray ?? (_stringArray = new[] {Mp3, Mp4a});
        }

        public static String ToMimeType(Int32 mediaTypeId)
        {
            switch (mediaTypeId)
            {
                case (Int32) MediaTypes.Mp3:
                    return Mp3;
                case (Int32) MediaTypes.Mp4a:
                    return Mp4a;
                default:
                    return Undefined;
            }
        }
    }

    public static class LiteratureTypes
    {
        public const String Undefined = "undefined";
        public const String Pdf = "applicaion/pdf";
        public const String Document = "applicaion/document";
        public const String Ebook = "applicaion/epub+zip";
        public const String Mobi = "application/x-mobipocket-ebook";

        public static Int32[] _intArray;

        public static String[] _stringArray;

        public static Int32[] AsArray()
        {
            return _intArray ??
                   (_intArray = new[] { (Int32)MediaTypes.Pdf, (Int32)MediaTypes.Document, (Int32)MediaTypes.Ebook, (Int32)MediaTypes.AmazonMobi, (Int32)MediaTypes.ZipArchive });
        }

        public static String[] AsStringArray()
        {
            return _stringArray ?? (_stringArray = new[] {Pdf, Document, Ebook, Mobi});
        }


        public static String ToMimeType(Int32 mediaTypeId)
        {
            switch (mediaTypeId)
            {
                case (Int32) MediaTypes.Pdf:
                    return Pdf;
                case (Int32) MediaTypes.Document:
                    return Document;
                case (Int32) MediaTypes.Ebook:
                    return Ebook;
                case (Int32)MediaTypes.AmazonMobi:
                    return Mobi;
                default:
                    return Undefined;
            }
        }
    }

    //1	Audio (.mp3, .wav, etc.)
    //2	Video (.wmv)
    //3	Adobe Acrobat (.pdf)
    //4	Document (.doc, .rtf, etc.)
    //5	Web Page (.html,.aspx)
    //6	Offline Files (books, articles)
    //7	Mises Store
    //8	Audio Book (.mp4a)
    //9	ebook (.epub)
    //12	Streaming Video
    //14	MPEG-4 Video
    public enum MediaTypes
    {
        Mp3 = 1,
        Wmv = 2,
        Pdf = 3,
        Document = 4,
        Mp4a = 8, // AudioBook
        Ebook = 9,
        Stream = 12,
        Mp4 = 14,
        Youtube = 16,
        ZipArchive = 18,
        AmazonMobi = 19
    }
}