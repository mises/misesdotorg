﻿#region

using System;

#endregion

namespace Mises.Domain.Contracts
{
    public enum MisesType
    {
        Undefined = 0,
        Authors,
        Media
    }

    public class SearchProviderResult
    {
        public MisesType Type { get; set; }
        public Int32 Id { get; set; }
        public String Link { get; set; }
        public String Title { get; set; }
        public String TitleHtml { get; set; }
        public String SnippetHtml { get; set; }
    }

    public interface ISearchProvider
    {
        PaginableResult<SearchProviderResult> Search(String term, Int32 pageIndex, Int32 pageSize);
    }
}