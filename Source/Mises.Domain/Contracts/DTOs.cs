﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Mises.Domain.Contracts
{
    /*public interface IMisesDTO
	{
		Int32 Id { get; set; }
		DateTime CreatedDate { get; set; }
	}*/

    public class MisesDTO // : IMisesDTO
    {
        public Int32 Id { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class UnknownLinkDTO : MisesDTO
    {
        public String Link { get; set; }
        public String Title { get; set; }
        public String TitleHtml { get; set; }
        public String SnippetHtml { get; set; }
    }

    public class SubjectSummaryDTO : MisesDTO
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String ImageUrl { get; set; }
    }

    public class SubjectDTO : SubjectSummaryDTO
    {
        public PaginableResult<DocumentDTO> Documents { get; set; }
    }

    public class CategorySummaryDTO : MisesDTO
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String ImageUrl { get; set; }
    }

    public class CategoryDTO : CategorySummaryDTO
    {
        public PaginableResult<CategorySummaryDTO> Ancestors { get; set; }
        public PaginableResult<CategorySummaryDTO> Children { get; set; }
        public PaginableResult<DocumentDTO> Documents { get; set; }
    }

    public class AuthorSummaryDTO : MisesDTO
    {
        public String FullName
        {
            get
            {
                return String.Format("{0}{1} {2}", FirstName,
                                     String.IsNullOrWhiteSpace(MiddleName)
                                         ? String.Empty
                                         : String.Concat(" ", MiddleName), LastName);
            }
        }

        public String FirstName { get; set; }
        public String MiddleName { get; set; }
        public String LastName { get; set; }
        public String BioInfoHtml { get; set; }
        public String PhotoUrl { get; set; }
    }

    public class AuthorDTO : AuthorSummaryDTO
    {
        public PaginableResult<ArticleSummaryDTO> Articles { get; set; }
        public PaginableResult<DocumentDTO> Documents { get; set; }

        // Added by DavidV
        public PaginableResult<DocumentDTO> Publications { get; set; }
    }

    public abstract class MediaDTO : MisesDTO
    {
        public String MimeType { get; set; }
        public String Url { get; set; }
        public String PosterUrl { get; set; }
		public byte VolumeOrdinal { get; set; }
		public String VolumeComment { get; set; }
	}

    public class VideoDTO : MediaDTO
    {
    }

    public class AudioDTO : MediaDTO
    {
    }

    public class LiteratureDTO : MediaDTO
    {
    }

    public class DocumentDTO : MisesDTO
    {
        public String Title { get; set; }
        public Guid Identifier { get; set; }
        public String PublishInfoHtml { get; set; }
        public String DescriptionHtml { get; set; }

        public CategorySummaryDTO Category { get; set; }
        public IList<AuthorSummaryDTO> Authors { get; set; }
        public IList<MediaDTO> Media { get; set; }
    }

    public class ArticleSummaryDTO : MisesDTO
    {
        private String _thumbnailUrl;
        public String Title { get; set; }
        public String DescriptionHtml { get; set; }

        //TODO whats in the db doesn't seem to be used so for now...

        public String ThumbnailUrl
        {
            get { return String.Format("http://s3.amazonaws.com/veksler-backup/DailyArticleBigImages/{0}.jpg", Id); }
            set { _thumbnailUrl = value; }
        }

        public DateTime PostedDate { get; set; }

        public IList<AuthorSummaryDTO> Authors { get; set; }
    }
}