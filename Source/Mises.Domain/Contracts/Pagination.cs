﻿#region

using System;
using System.Collections.Generic;
using System.Dynamic;

#endregion

namespace Mises.Domain.Contracts
{
    public class Pagination
    {
        private dynamic _counts = new ExpandoObject();

        public dynamic Counts
        {
            get { return _counts; }
            set { _counts = value; }
        }

        public String RouteSegmentName { get; set; }
        public Int32 TotalItems { get; set; }
        public Int32 PageIndex { get; set; }
        public Int32 PageSize { get; set; }
        public Int32 ListSize { get; set; }

        public Int32 TotalPages
        {
            get { return (Int32) Math.Ceiling(Decimal.Divide(TotalItems, PageSize)); }
        }

        public Boolean HasPrevious
        {
            get { return PageIndex > ListSize - 1; }
        }

        public Boolean HasNext
        {
            get
            {
                if (ListSize == 0)
                    return false;

                var result = (TotalPages%ListSize);

                if (result == 0)
                    result = ListSize;

                result = TotalPages - result;

                return PageIndex < result;
            }
        }

        public Int32 StartIndex
        {
            get { return (ListSize > 0) ? PageIndex - (PageIndex%ListSize) : 0; }
        }

        public Int32 EndIndex
        {
            get
            {
                var result = StartIndex + ListSize;

                if (result > TotalPages)
                    result = TotalPages;

                return result;
            }
        }

        public Int32 LastStartIndex
        {
            get
            {
                if (ListSize == 0)
                    return 0;

                var result = (TotalPages%ListSize);

                if (result == 0)
                    result = ListSize;

                result = TotalPages - result;

                return result;
            }
        }
    }

    public class PaginableResult<T> : Pagination
    {
        public IList<T> Items { get; set; }
    }
}