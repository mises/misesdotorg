﻿#region

using System;
using System.Text;
using System.Web;

#endregion

namespace Mises.Domain.Mobile
{
    public class Analytics
    {
        // Copyright 2009 Google Inc. All Rights Reserved.
        private const string GaAccount = "MO-12345-5";
        private const string GaPixel = "/Services/GoogleAnalytics.ashx";

        private const string ImageSrc = @"<img src=""{0}"" />";

        public static string GetAnalyticsImageTag(HttpContextBase context)
        {
            return string.Format(ImageSrc, GoogleAnalyticsGetImageUrl(context));
        }

        public static string GoogleAnalyticsGetImageUrl(HttpContextBase context)
        {
            var url = new StringBuilder();
            url.Append(GaPixel + "?");
            url.Append("utmac=").Append(GaAccount);
            var RandomClass = new Random();
            url.Append("&utmn=").Append(RandomClass.Next(0x7fffffff));
            string referer = "-";
            if (context.Request.UrlReferrer != null
                && "" != context.Request.UrlReferrer.ToString())
            {
                referer = context.Request.UrlReferrer.ToString();
            }
            url.Append("&utmr=").Append(HttpUtility.UrlEncode(referer));
            if (context.Request.Url != null)
            {
                url.Append("&utmp=").Append(HttpUtility.UrlEncode(context.Request.Url.PathAndQuery));
            }
            url.Append("&guid=ON");
            return url.ToString().Replace("&", "&amp;");
        }
    }
}