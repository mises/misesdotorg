﻿#region

using System;
using System.Collections.Specialized;
using System.Web;
using Mises.Data;

#endregion

namespace Mises.Domain
{
    public class AdminHelper
    {
        public static string GetEditURL(string Id, string currentURL, NameValueCollection QueryString)
        {
            string URL;
            //string currentURL = request.Url.ToString();

            if (DataFormat.IsGUID(Id))
            {
                URL = string.Format("http://mises.freecapitalists.org//manager/page.aspx?Id={0}", Id);
            }
            else
            {
                if (currentURL.Contains("/daily/") || currentURL.Contains("/preview/")  || currentURL.Contains("article.aspx") ||
                    currentURL.Contains("article2.aspx"))
                {
                    URL = string.Format("http://mises.freecapitalists.org//manager/article.aspx?Id={0}", Id);
                }
                else if (currentURL.Contains("mediaPlayer"))
                {
                    URL = string.Format("http://mises.freecapitalists.org//manager/document.aspx?MediaId={0}&format=media",
                                        QueryString["MediaId"]);
                }
                else if (currentURL.Contains("document/"))
                {
                    URL = "http://mises.freecapitalists.org//manager/document.aspx?format=literature&Id=" + Id;
                 //   URL = string.Format("/Admin/Document/Edit/{0}",Id);
                }
                else if (currentURL.Contains("/page/") || currentURL.Contains("ClassicASP"))
                {
                    URL = string.Format("/manager/page.aspx?PageId={0}", Id);
                }
                    
                else if (currentURL.Contains("media"))
                {
                    URL = string.Format("http://mises.freecapitalists.org//manager/document.aspx?Id={0}&format=media", Id);
                }
                else if (currentURL.Contains("/events") || currentURL.Contains("/event"))
                {
                    URL = "http://mises.freecapitalists.org//manager/events.aspx?Id=" + Id;
                }
                else if (currentURL.Contains("/form.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/formEditor.aspx?formId=" + Id;
                }
                else if (currentURL.Contains("/file.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/content.aspx";
                }
                else if (currentURL.Contains("/periodical.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/periodicals/JournalArchives.aspx?Id=" + Id;
                }
                else if (currentURL.Contains("/periodical.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/periodicals.aspx?Id=" + Id;
                }
                else if (currentURL.Contains("/Literature") || currentURL.Contains("/literature.aspx") || currentURL.Contains("/media.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/content.aspx";
                }
                else if (currentURL.Contains("misesreview"))
                {
                    URL = "http://mises.freecapitalists.org//manager/periodicals/misesreview.aspx?Id=" + QueryString["control"];
                }
                else if (currentURL.Contains("quotes.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/quotes.aspx";
                }
                else if (currentURL.Contains("fellow.aspx"))
                {
                    URL = "http://mises.freecapitalists.org//manager/fellows.aspx";
                }
                else
                {
                    URL = "http://mises.freecapitalists.org//manager/document.aspx?format=literature&Id=" + Id;
                }
            }

            return URL;
        }

        public static bool IsAdmin(HttpCookieCollection cookies)
        {
            try
            {
                return cookies != null && cookies["IsAdminLoggedIn"] != null && cookies["IsAdminLoggedIn"].Value == "1";
            }
            catch
            {
                return false;
            }
            

            
        }
    }
}