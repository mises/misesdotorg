namespace Mises.Domain.Tags
{
    public class TagItem
    {
        private readonly string _URL;
        private readonly string _name;

        private readonly int _weight;

        public TagItem(string name, int weight, string URL)
        {
            _name = name;
            _weight = weight;
            _URL = URL;
        }

        public string Name
        {
            get { return _name; }
        }

        public int Weight
        {
            get { return _weight; }
        }

        public string URL
        {
            get { return _URL; }
        }
    }
}