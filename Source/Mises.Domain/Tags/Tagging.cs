#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Mises.Data;
using Document = Mises.Domain.Documents.Document;

#endregion

namespace Mises.Domain.Tags
{
    public class Tag
    {
        public DateTime EditDate;
        public Guid TagAuthority;
        public int TagCount;

        public string TagId { get; set; }

        public string TagText { get; set; }
    }

    public class Tagging : DataHelper
    {
        #region Instance Properties

        public IEnumerable<TagItem> TagItemList = new List<TagItem>();

        #endregion

        #region Add Tags

        ///<summary>
        ///  Tags the document.
        ///</summary>
        ///<param name = "tags">The tags.</param>
        ///<param name = "Identifier">The identifier.</param>
        public static void TagDocument(string tags, Guid Identifier)
        {
            TagDocument(tags, Identifier, "");
        }


        public static void TagDocument(string tags, Guid Identifier, string user)
        {
            TagDocument(ParseTags(tags), Identifier, user);
        }

        private static void TagDocument(IEnumerable<string> tags, Guid Identifier, string user)
        {
            var db = new MisesDBDataContext(ConnectionString);

            tags.AsParallel().ForAll(tag =>
                                         {
                                             try
                                             {
                                                 db.TagAddNew(tag, Identifier, user);
                                             }
                                             catch (Exception ex)
                                             {
                                                 if (ex.Message.Contains("Violation of UNIQUE KEY constraint"))
                                                 {
                                                     return; // Duplicate key
                                                 }
                                                 throw;
                                             }
                                         });
        }

        #endregion

        #region Get Tags

        public IEnumerable<TagItem> GetDocumentAuthors()
        {
            var db = new MisesDBDataContext(ConnectionString);

            IEnumerable<TagItem> tags = db.DocumentTopAuthors().Select(a => new TagItem(a.AuthorLast, (int) a.Count,
                                                                                        string.Format(
                                                                                            "/Literature/Author/{0}",
                                                                                            a.AuthorId)));
            return tags;
        }

        public static IEnumerable<Tag> GetDocumentTags(Guid Identifier)
        {
            if (Identifier == Guid.Empty)
            {
                return new List<Tag>();
            }

            var db = new MisesDBDataContext(ConnectionString);
            var documentTags = db.TagGetDocumentTags(Identifier);

            return
                documentTags.Select(
                    tag => new Tag {TagId = tag.TagId.ToString(), TagText = tag.Tag, TagCount = (int) tag.Count});
        }


        public IEnumerable<Document> GetUserTags(string User)
        {
            var db = new MisesDBDataContext(ConnectionString);
            var tagsDB = db.TagGetUserTaggedDocuments(User, null);

            return tagsDB.Select(document => new Document
                                                 {
                                                     Id = document.Id,
                                                     Identifier = document.GUID,
                                                     Title = document.Title,
                                                     URL = document.URL,
                                                     DocumentType = Document.GetDocumentType(document.Type)
                                                 });
        }

        public IEnumerable<Document> GetUserTags(string User, string User2)
        {
            var db = new MisesDBDataContext(ConnectionString);

            var docs = db.TagGetUserTaggedDocuments(User, User2);
            var tags = db.TagGetUserTags(User, User2);

            var documents = docs.Select(tag => new Document
                                                   {
                                                       Id = tag.Id,
                                                       Identifier = (tag.GUID),
                                                       Title = Convert.ToString(tag.Title),
                                                       URL = Convert.ToString(tag.URL),
                                                       DocumentType = Document.GetDocumentType(tag.Type)
                                                   });


            TagItemList = tags.Select(tag => new TagItem(tag.Tag, (int) tag.Count, string.Format("/tag/{0}", tag.Tag)));

            return documents;
        }

        public static List<TagItem> GetDocumentTagCloud(Guid Identifier)
        {
            var db = new MisesDBDataContext(ConnectionString);
            ISingleResult<TagGetDocumentTagsResult> tagsDB = db.TagGetDocumentTags(Identifier);

            return
                tagsDB.Select(
                    tag => new TagItem(tag.Tag, (int)tag.Count, string.Format("/tag/{0}", tag.Tag.StripHTML()))).ToList();
        }

        public static List<Document> GetDocumentsWithTag(string tag)
        {
            var db = new MisesDBDataContext(ConnectionString);
            var tagsDB = db.TagGetDocumentsWithTag(tag);

            return new List<Document>(tagsDB.Select(document => new Document
                                                                    {
                                                                        Id = document.Id,
                                                                        Identifier = (Guid) document.GUID,
                                                                        Title = document.Title,
                                                                        URL = document.URL,
                                                                        DocumentType =
                                                                            Document.GetDocumentType(document.Type)
                                                                    }));
        }


        public static IEnumerable<Document> GetDocumentsWithTags(string tags)
        {
            //var documents = new List<Document>();

            var db = new MisesDBDataContext(ConnectionString);
            var tagsDB = db.TagGetDocumentsWithTag(tags);


            return tagsDB.Select(document => new Document
                                                 {
                                                     Id = document.Id,
                                                     Identifier = (Guid) document.GUID,
                                                     Title = document.Title,
                                                     URL = document.URL,
                                                     DocumentType = Document.GetDocumentType(document.Type)
                                                 });
        }

        public static IEnumerable<TagItem> GetRelatedTagCloud(string Tag)
        {
            var db = new MisesDBDataContext(ConnectionString);
            ISingleResult<TagGetRelatedTagsResult> tagsDB = db.TagGetRelatedTags(Tag);

            return
                tagsDB.Select(
                    tag => new TagItem(tag.Tag, (int)tag.Count, string.Format("/tag/{0}", tag.Tag.StripHTML())));
        }

        ///<summary>
        ///  Gets the related documents.
        ///</summary>
        ///<param name = "ArticleId">The article id.</param>
        ///<returns>List of documents</returns>
        public static IEnumerable<Document> GetRelatedDocuments(int ArticleId)
        {
            IEnumerable<Document> documents = new List<Document>();

            var db = new MisesDBDataContext(ConnectionString);
            ISingleResult<TagGetRelatedDocumentsResult> tagsDB = db.TagGetRelatedDocuments(null, ArticleId);

            documents = tagsDB.Select(document => new Document
                                                      {
                                                          Id = document.Id,
                                                          Identifier = (Guid) document.GUID,
                                                          Title = document.Title,
                                                          URL = document.URL,
                                                          DocumentType = Document.GetDocumentType(document.Type)
                                                      });
            return documents;
        }

        public static IEnumerable<Document> GetRelatedDocuments(Guid DocumentId)
        {
            IEnumerable<Document> documents = new List<Document>();

            var db = new MisesDBDataContext(ConnectionString);
            ISingleResult<TagGetRelatedDocumentsResult> tagsDB = db.TagGetRelatedDocuments(DocumentId, null);

            documents = tagsDB.Select(document => new Document
                                                      {
                                                          Id = document.Id,
                                                          Identifier = (Guid) document.GUID,
                                                          Title = document.Title,
                                                          URL = document.URL,
                                                          DocumentType = Document.GetDocumentType(document.Type)
                                                      });

            return documents;
        }

        public static IEnumerable<TagItem> GetPopularTagCloud()
        {
            try
            {
                var db = new MisesDBDataContext(ConnectionString);

                ISingleResult<TagGetTopTagsResult> tagsDB = db.TagGetTopTags(60);

                return
                    tagsDB.Select(
                        tag => new TagItem(tag.tag, (int)tag.Count, string.Format("/tag/{0}", tag.tag.StripHTML())));
            }
            catch (Exception)
            {
                // todo
            }
            return new List<TagItem>();
        }


        //Public Shared Function GetDocumentsWithTags(ByVal tags As List(Of String)) As List(Of Document)
        //    Dim list As New List(Of String)
        //    list.Add(tag)
        //    Return GetDocumentsWithTags(list)
        //End Function


        public static DataSet GetAnonymousTaggers()
        {
            //var db = new MisesDBDataContext(ConnectionString);
            //return db.TagGetAnonymousTaggers()
            return ExecuteDataset("TagGetAnonymousTaggers");
        }

        #endregion

        #region Edit Tags

        public static int DeleteTag(string tag)
        {
            var db = new MisesDBDataContext(ConnectionString);
            return db.TagDelete(tag, null, null);
        }

        public static int DeleteDocumentTag(string tag, Guid Identifier)
        {
            var db = new MisesDBDataContext(ConnectionString);
            return db.TagDelete(tag, null, Identifier);
        }


        public static int DeleteAllDocumentTags(Guid Identifier)
        {
            var db = new MisesDBDataContext(ConnectionString);
            return db.TagDelete(null, null, Identifier);
        }

        ///<summary>
        ///  Deletes the user tag for the given document GUID
        ///</summary>
        ///<param name = "user">The user.</param>
        ///<param name = "Identifier">The identifier.</param>
        ///<param name = "tag">The tag.</param>
        public static void DeleteUserDocumentTag(string user, Guid Identifier, string tag)
        {
            var db = new MisesDBDataContext(ConnectionString);
            db.TagDelete(tag, user, Identifier);
        }

        public static int DeleteUserTags(string user)
        {
            var db = new MisesDBDataContext(ConnectionString);
            return db.TagDelete(null, user, null);
        }

        public static int UpdateTag(string oldTag, string newTag)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text,
                                             "UPDATE Tag SET Tag = @newTag WHERE Tag = @oldTag",
                                             new SqlParameter("oldTag", oldTag), new SqlParameter("newTag", newTag));
        }

        #endregion

        #region Format Tags

        private static IEnumerable<string> ParseTags(string tags)
        {
            string[] tagArray = (tags.StripHTML()).Split(Convert.ToChar(","));
            return tagArray.Select(tag => NormalizeTag(tag.Trim()));
        }

        ///<summary>
        ///  Normalizes the tag.
        ///</summary>
        ///<param name = "tag">The tag.</param>
        ///<returns></returns>
        private static string NormalizeTag(string tag)
        {
            const string normalized_valid_chars = "a-zA-Z0-9";
            const bool normalize_tags = false;
            if (Convert.ToBoolean(normalize_tags))
            {
                string normalized_tag = new Regex("/[^" + normalized_valid_chars + "]/").Replace(tag, "");
                return normalized_tag.ToLower();
            }
            return tag;
        }

        #endregion

        public static void GetGoogleKeywords(Guid DocumentIdentifier, HttpContext Context)
        {
            try
            {
                if (Context.Request.UrlReferrer != null)
                {
                    string referrer = Context.Request.UrlReferrer.ToString();
                    if (referrer.Contains("q="))
                    {
                        // Get Keywords
                        int startIndex = referrer.IndexOf("q=") + 2;
                        if (startIndex > -1)
                        {
                            int endindex = referrer.IndexOf("&", startIndex);
                            if (endindex == -1)
                            {
                                endindex = referrer.Length;
                            }
                            if (endindex == -1)
                            {
                                return;
                            }
                            referrer = referrer.Substring(startIndex, endindex - startIndex);
                            TagDocumentFromSearchKeyword(referrer, DocumentIdentifier);
                        }
                    }
                }
            }
            catch (UriFormatException)
            {
            }
        }

        private static void TagDocumentFromSearchKeyword(string keyword, Guid DocumentIdentifier)
        {
            // Is this an OK keyword?

            keyword =
                keyword.Replace("+", " ").Replace("define:", "").Replace("site:mises.org", "").Replace(
                    "site:mises.org", "").Replace("www.mises.org", "").Replace("mises.org", "").Replace("%25", "%").
                    Replace("what is ", "").Replace("&quot;", "\"").Replace("%2522", "\"").Replace("label:", "");

            // Questions
            if (keyword.Length < 5 || keyword.Contains(" is ") || keyword.Contains(" are ") || keyword.Contains(" has ") ||
                keyword.Contains(" have ") || keyword.StartsWith("have ") || keyword.StartsWith("what ") ||
                keyword.Contains("has ") || keyword.Contains("how to ") || keyword.Contains("was a ") ||
                keyword.Contains(" about ") || keyword.StartsWith("why ") || keyword.Contains("%252B") ||
                keyword.Contains("cache:") || keyword.Contains("%") || keyword.Contains(DateTime.Now.Year.ToString()) ||
                keyword.Contains(Convert.ToString(DateTime.Now.Year + 1)) || keyword.Contains("/") ||
                keyword.Contains(".gif") || keyword.Contains("sex ") || keyword.Contains("porn ") ||
                keyword.Contains("nude"))
            {
                return;
            }

            // To many quotes, or too long quote
            if (keyword.Contains("\"") &&
                (keyword.Split(Convert.ToChar("\"")).Length > 2 | keyword.Split(Convert.ToChar(" ")).Length > 4))
            {
                return;
            }

            keyword = keyword.Replace("\"", ""); // Strip Quotes

            string[] keywords = keyword.Split(Convert.ToChar(","));
            foreach (string t in keywords)
            {
                if (t.Split(Convert.ToChar(" ")).Length > 5)
                {
                    return;
                }
                TagDocument(t, DocumentIdentifier, "SearchEngineBot");
            }
        }
    }
}