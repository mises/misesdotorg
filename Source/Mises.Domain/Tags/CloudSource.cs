#region

using System.Collections.Generic;
using System.Data;
using Mises.Data;

#endregion

namespace Mises.Domain.Tags
{
    ///<summary>
    ///  ItemsSource supplies the items
    ///</summary>
    public class CloudSource
    {
        public List<TagItem> GetDocumentAuthors()
        {
            var tags = new List<TagItem>();
            using (
                SafeDataReader reader = SqlHelper.ExecuteReader(DataHelper.ConnectionString,
                                                                CommandType.StoredProcedure, "dbo.DocumentTopAuthors"))
            {
                while (reader.Read())
                {
                    tags.Add(new TagItem(reader["AuthorLast"].ToString(), (int) reader["Count"],
                                         string.Format("/Literature/Author/{0}", reader["AuthorId"])));
                }
            }

            return tags;
        }


        public List<TagItem> GetMediaAuthors()
        {
            var tags = new List<TagItem>();
            using (
                SafeDataReader reader = SqlHelper.ExecuteReader(DataHelper.ConnectionString,
                                                                CommandType.StoredProcedure, "dbo.MediaTopAuthors"))
            {
                while (reader.Read())
                {
                    tags.Add(new TagItem(reader["AuthorLast"].ToString(), (int) reader["Count"],
                                         string.Format("/media/authors/{0}/{1}", reader["AuthorId"],
                                                       DataFormat.GenerateSlug(reader["AuthorLast"]))));
                }
            }

            return tags;
        }

        public List<TagItem> GetDailyAuthors()
        {
            var tags = new List<TagItem>();
            using (
                SafeDataReader reader = SqlHelper.ExecuteReader(DataHelper.ConnectionString,
                                                                CommandType.StoredProcedure,
                                                                "dbo.DailyArticlesTopAuthors"))
            {
                while (reader.Read())
                {
                    tags.Add(new TagItem(reader["AuthorLast"].ToString(), (int) reader["Count"],
                                         string.Format("/articles.aspx?AuthorId={0}", reader["AuthorId"])));
                }
            }

            return tags;
        }

        public List<TagItem> GetDocumentSubjects()
        {
            var tags = new List<TagItem>();
            using (
                SafeDataReader reader = SqlHelper.ExecuteReader(DataHelper.ConnectionString,
                                                                CommandType.StoredProcedure, "dbo.SubjectsTopSubjects"))
            {
                while (reader.Read())
                {
                    tags.Add(new TagItem(reader["ShortSubject"].ToString(), (int) reader["Count"],
                                         string.Format("/Literature/Subject/{0}", reader["SubjectId"])));
                }
            }

            return tags;
        }
    }
}