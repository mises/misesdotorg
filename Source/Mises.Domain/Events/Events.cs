#region

using System;
using System.Data;
using System.Linq;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using Mises.Data;

#endregion

namespace Mises.Domain.Events
{
    public class Events : DataHelper
    {
        public static DataSet GetUpcomingEvents()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                            "dbo.CalendarGetUpcomingEvents");
        }

        //public static DataSet GetEvents()
        //{
        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.CalendarGetEventsList");
        //}

        public static CalendarGetEventDetailResult GetEventDetails(int eventId)
        {
            var db = new MisesDBDataContext(ConnectionString);

            return db.CalendarGetEventDetail(eventId).First();
        }

        public static string GetICalEvent(CalendarGetEventDetailResult calendarEvent)
        {
            // Create a new iCalendar
            var iCal = new iCalendar();

            // Create the event, and add it to the iCalendar
            CalendarEventToICalEvent(iCal, calendarEvent);
            var serializer = new iCalendarSerializer(iCal);

            return serializer.SerializeToString(iCal);
        }

        public static void CalendarEventToICalEvent(iCalendar iCal, CalendarGetEventDetailResult calendarEvent)
        {
            var evt = iCal.Create<Event>();
            // Set information about the event
            evt.Start = new iCalDateTime(calendarEvent.StartDate);
            evt.End = evt.Start.AddHours(18); // This also sets the duration            
            //evt.Name = DataFormat.StripHTML(calendarEvent.Title);
            evt.End = new iCalDateTime(calendarEvent.EndDate);
            evt.Location = calendarEvent.Location;
            evt.Description = calendarEvent.IntroText.StripHTML();
            evt.Summary = calendarEvent.Title.StripHTML();
            evt.Location = calendarEvent.Location;
            evt.Url = new Uri(string.Format("http://mises.freecapitalists.org//events/{0}", calendarEvent.EventId));
            evt.IsAllDay = true;
        }
    }
}