#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mises.Data;
using Mises.MetaParser.Tagging;
using Mises.MetaParser.TextParsing;
using TagLib;
using File = TagLib.File;

#endregion

namespace Mises.MetaParser.Media
{
    public class MediaFile
    {
        public Int32 DocumentId;
        public Int32 FileId;
        public string URL;
        public string AbsolutePathToFile;

        public string webRoot = ConfigurationManager.AppSettings["TargetFolder"];
        public string Description { get; set; }
        public string Keywords { get; set; }

        ///<summary>
        ///  Gets the properties of the media file.
        ///</summary>
        public bool? UpdateProperties()
        {
            if (!String.IsNullOrWhiteSpace(AbsolutePathToFile))
            {
                
            }
            else
            {
                if (!DataFormat.GetPathFromURL(webRoot, URL, out AbsolutePathToFile)) return false;    
            }

            var info = new FileInfo(AbsolutePathToFile);
            var tagInfo = new MediaTag { fileSize = info.Length.ToString() };
            try
            {
                var generic = File.Create(AbsolutePathToFile);
                tagInfo.tag = generic.Tag;
                tagInfo.duration = generic.Properties.Duration.TotalSeconds.ToString();
                tagInfo.GetValuesFromTag();
            }
            catch (CorruptFileException exception)
            {
                ParserUtility.WriteError(exception, "CORRUPT FILE: " + URL);
                return false;

            }
            catch (Exception exception)
            {
                ParserUtility.WriteError(exception, "Parse error: " + URL);
                return false;
            }


            if (!string.IsNullOrEmpty(Description))
                tagInfo.Description = Description;
            if (!string.IsNullOrEmpty(Keywords))
                tagInfo.Keywords = Keywords;

            if (tagInfo.Keywords.Length > 350)
                tagInfo.Keywords = tagInfo.Keywords.Substring(0, 349);

            UpdateMediaTable(tagInfo);
            return true;
        }

        private void UpdateMediaTable(MediaTag tagInfo)
        {
            if (null != tagInfo.picData) // has thumbnails
            {
                ParserUtility.WriteLog("Updating with thumbnail: " + URL);

                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure, "MediaAddMetadata",
                                          new SqlParameter("DocumentId", DocumentId),
                                          new SqlParameter("@FileId", FileId),
                                          new SqlParameter("@filesize", tagInfo.fileSize),
                                          new SqlParameter("@duration", tagInfo.duration),
                                          new SqlParameter("@Description",
                                                           DataFormat.StripHTML(tagInfo.Description)),
                                          new SqlParameter("Keywords", tagInfo.Keywords),
                                          new SqlParameter("@CoverImage", tagInfo.picData));
            }
            else
            {
                ParserUtility.WriteLog("Updating: " + URL);
                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure, "MediaAddMetadata",
                                          new SqlParameter("DocumentId", DocumentId),
                                          new SqlParameter("@FileId", FileId),
                                          new SqlParameter("@filesize", tagInfo.fileSize),
                                          new SqlParameter("@duration", tagInfo.duration),
                                          new SqlParameter("@Description",
                                                           DataFormat.StripHTML(tagInfo.Description)),
                                          new SqlParameter("Keywords", tagInfo.Keywords));
            }
        }
    }
}