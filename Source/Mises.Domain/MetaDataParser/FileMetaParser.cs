﻿#region

using System;
using System.Configuration;
using System.IO;
using Mises.Data;
using Mises.MetaParser.TextParsing;

#endregion

namespace Mises.MetaParser
{
    public class FileMetaParser
    {
        public bool GetFileMetadata(DocumentFileTB file)
        {
            string fileName;
            if (!DataFormat.GetPathFromURL(ConfigurationManager.AppSettings["TargetFolder"], file.URL, out fileName))
            {
                return false;
            }

            var info = new FileInfo(fileName);

            // Update Length
            if (file.fileSize == 0)
                file.fileSize = Convert.ToInt64(info.Length);
            file.CreateDate = info.CreationTime;
            return true;
        }

        internal bool GetExternalFileMetadata(DocumentFileTB file)
        {
            Stream stream = ParserUtility.GetFileStreamFromURL(file.URL);
            file.fileSize = stream.Length;
            return true;
        }
    }
}