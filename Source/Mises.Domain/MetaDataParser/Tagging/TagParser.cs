#region

using System;
using System.Data;
using System.Data.SqlClient;
using Mises.Data;

#endregion

namespace Mises.MetaParser.Tagging
{
    public class TagParser
    {
        private const string IdentifierParameter = "@Identifier";
        private const string TagParameter = "@Tag";
        private const string GuidKey = "GUID";
        private const string AddNewTagSproc = "TagAddNew";
        private const string AuthorKey = "Author";
        private const string SearchKeywordsKey = "SearchKeywords";
        private const string UserParameter = "@User";
        private const string User = "MisesBot";
        // TODO: Database Name is hardcoded here
        private const string sql =
            "USE AbleCommerce; SELECT GUID,[SearchKeywords], [ac_Manufacturers].[Name] AS Author FROM ac_Products JOIN [ac_Manufacturers] ON [ac_Products].[ManufacturerId] = [ac_Manufacturers].[ManufacturerId] WHERE [SearchKeywords] IS NOT NULL";

        public static int UpdateTags()
        {
            Int32 FileCount = 0;


            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text, sql);

            //ds.Tables[0].AsEnumerable().ForEach(row =>
            //                                        {

            //                                        });

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string[] Tags = row[SearchKeywordsKey].ToString().Split(Convert.ToChar(","));


                foreach (string t in Tags)
                {
                    SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure, AddNewTagSproc,
                                              new SqlParameter(UserParameter, User),
                                              new SqlParameter(IdentifierParameter, row[GuidKey]),
                                              new SqlParameter(TagParameter, t));
                    FileCount++;
                }

                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure, AddNewTagSproc,
                                          new SqlParameter(UserParameter, User),
                                          new SqlParameter(IdentifierParameter, row[GuidKey]),
                                          new SqlParameter(TagParameter, row[AuthorKey]));
                FileCount++;
            }

            return FileCount;
        }
    }
}