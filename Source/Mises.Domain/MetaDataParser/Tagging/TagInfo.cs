#region

using System;
using TagLib;

#endregion

namespace Mises.MetaParser.Tagging
{
    public class MediaTag
    {
        public string album = string.Empty;

        public string artists = string.Empty;
        public string comment = string.Empty;
        public string duration = string.Empty;
        public string fileSize = string.Empty;
        public string genre = string.Empty;
        public string Description = string.Empty;
        public string Keywords = string.Empty;

        public Byte[] picData;

        //  private IPicture picture;
        public Tag tag;
        public string title = string.Empty;
        public string year = string.Empty;

        public void TagInfo(Tag NewTag)
        {
            tag = NewTag;
        }

        public void GetValuesFromTag()
        {
            year = tag.Year.ToString();
            title = tag.Title;
            comment = tag.Comment;
            album = tag.Album;
            artists = tag.JoinedAlbumArtists;
            genre = tag.JoinedGenres;

            if (tag.Pictures.Length > 0)
            {
                //  picture = tag.Pictures[0];
                // Image img;

                picData = tag.Pictures[0].Data.Data;
                //MemoryStream myStream = new MemoryStream(picData);
                //img = System.Drawing.Image.FromStream(myStream);
                //img.Save(@"C:\img.jpg");
            }

            if (!string.IsNullOrEmpty(tag.Album) && !string.IsNullOrEmpty(tag.Title))
            {
                Description = tag.Album.Trim() + ": " + tag.Title.Trim();
            }
            else
            {
                Description = tag.Album + tag.Title;
            }

            if (tag.Year > 0)
            {
                Keywords += Environment.NewLine + tag.Year;
            }
            Description = " " + tag.Comment;

            Description = Description.Trim();


            //Description = String.Format("{0} {1} {2}", title, artists, album).Trim();
            Keywords =
                String.Format("{0},{1},{2},{3},{4},{5}", title, artists, year, genre, album, comment).Replace(",,", ",")
                    .Replace(",,", ",").Replace(",0,", string.Empty);
        }
    }
}