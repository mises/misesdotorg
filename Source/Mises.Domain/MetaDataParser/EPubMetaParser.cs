﻿#region

using System;
using System.Configuration;
using System.IO;
using Mises.Data;
using Mises.MetaParser.TextParsing;

#endregion

namespace Mises.MetaParser
{
    public class EPubMetaParser
    {
        public string webRoot = ConfigurationManager.AppSettings["TargetFolder"];

        public bool GetFileMetadata(DocumentFileTB file)
        {
            string fileName;
            if (!DataFormat.GetPathFromURL(webRoot, file.URL, out fileName))
            {
                if (!DataFormat.GetPathFromURL(webRoot, @"\books" + file.URL, out fileName))
                {
                    return false;
                }
            }

            var info = new FileInfo(fileName);

            // Update Length
            if (file.fileSize == 0)
                file.fileSize = Convert.ToInt64(info.Length);
            file.CreateDate = info.CreationTime;

            //var db = new MisesDBDataContext(BusinessBase.ConnectionString);
            //var epub = from book in db.DocumentFileTBs where book.MediaTypeId == 1
            //db.SubmitChanges();

            return true;
        }
    }
}