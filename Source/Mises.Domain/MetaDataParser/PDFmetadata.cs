#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mises.Data;
using Mises.MetaParser.TextParsing;
using iTextSharp.text.pdf;

#endregion

namespace Mises.MetaParser.Media
{
    public class PDFmetadata
    {
        private const string AddMetadataToMediaSproc = "MediaAddMetadata";

        private string Author;
        private string Creator; // Not used
        private string Description;
        public Int32 DocumentId;
        private string Keywords;
        public Int32 FileId;
        private string Producer;
        private string Subject, Title;
        public string URL;
        private string fileSize;
        public string webRoot = ConfigurationManager.AppSettings["TargetFolder"];


        public bool GetFileMetadata()
        {
            string fileName;
            if (!DataFormat.GetPathFromURL(webRoot, URL, out fileName))
            {
                if (!DataFormat.GetPathFromURL(webRoot, @"\PDF" + URL, out fileName))
                {
                    return false;
                }
            }

            var info = new FileInfo(fileName);
            fileSize = info.Length.ToString();

            var Reader = new PdfReader(fileName);
            var info1 = Reader.Info;

            foreach (var entry in info1)
            {
                switch (entry.Key)
                {
                    case Keys.Title:
                        Title = info1[Keys.Title];
                        break;
                    case Keys.Author:
                        Author = info1[Keys.Author];
                        break;
                    case Keys.Subject:
                        Subject = info1[Keys.Subject];
                        break;
                    case Keys.Creator:
                        Creator = info1[Keys.Creator];
                        break;
                    case Keys.Producer:
                        Producer = info1[Keys.Producer];
                        break;
                    case Keys.Keywords:
                        Keywords = info1[Keys.Keywords];
                        break;
                }
            }
            Description = ParserUtility.PadWord(Author) + ParserUtility.PadWord(Title) + ParserUtility.PadWord(Producer) +
                          ParserUtility.PadWord(Subject);
            Keywords = ParserUtility.PadWord(Author, ",") + ParserUtility.PadWord(Title, ",") + ParserUtility.PadWord(Producer, ",") +
                       ParserUtility.PadWord(Subject, ",");

            // Update DB
            SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                      AddMetadataToMediaSproc,
                                      new SqlParameter(Parameters.DocumentId, DocumentId),
                                      new SqlParameter(Parameters.FileId, FileId),
                                      new SqlParameter(Parameters.FileSize, fileSize),
                                      new SqlParameter(Parameters.Description, Description),
                                      new SqlParameter(Parameters.Keywords, Keywords));

            return true;
        }

        #region Nested type: Keys

        private static class Keys
        {
            public const string Title = "Title";
            public const string Author = "Author";
            public const string Subject = "Subject";
            public const string Creator = "Creator";
            public const string Producer = "Producer";
            public const string Keywords = "Keywords";
        }

        #endregion

        #region Nested type: Parameters

        private static class Parameters
        {
            public const string DocumentId = "DocumentId";
            public const string FileId = "@FileId";
            public const string FileSize = "@filesize";
            public const string Description = "@Description";
            public const string Keywords = "Keywords";
        }

        #endregion
    }
}