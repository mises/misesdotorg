#region

using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Mises.Data;

#endregion

namespace Mises.Domain.Membership
{
    public class Favorites
    {
        public static DataSet GetUserFavorites(string UserName, string IPadress)
        {
            return SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                            "dbo.FavoritesGetUserFavorites", new SqlParameter("UserId", UserName),
                                            new SqlParameter("IPaddress", IPadress));
        }


        public static void SaveFavorite(string URL, string Title, string UserId, string IPaddress)
        {
            SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["Public"].ConnectionString,
                                      CommandType.StoredProcedure, "dbo.FavoriteAdd", new SqlParameter("URL", URL),
                                      new SqlParameter("Title", Title), new SqlParameter("UserId", UserId),
                                      new SqlParameter("IPaddress", IPaddress));
        }

        public static void DeleteFavorite(int Id)
        {
            SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["Public"].ConnectionString,
                                      CommandType.Text, "DELETE FROM Favorites WHERE FavoriteId=@Id",
                                      new SqlParameter("Id", Id));
        }
    }
}