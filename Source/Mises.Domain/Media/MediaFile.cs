//#region

//using System;
//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using Mises.Data;
//using Mises.Domain.Documents;

//#endregion

//namespace Mises.Domain.Media
//{
//    public static class MediaFile 
//    {
//        //public MediaFile()
//        //{
//        //}


//        //public MediaFile(DataRow row)
//        //{
//        //    FileId =  row.Field<int>("FileId");
//        //    DocumentId = row.Field<int>("DocumentId");
//        //    MediaTypeId = row.Field<int>("MediaTypeId");
//        //    URL = row.Field<string>("URL");
//        //    FileSize = row.Field<Int64?>("fileSize");
//        //    Duration = row.Field<decimal>("duration");
//        //    CreateDate = row.Field<DateTime>("CreateDate");
//        //    if (row.Table.Columns.Contains("Description")) Description = row.Field<string>("Description");
//        //    MediaIconPath = row.Field<string>("MediaIconPath");
//        //    IsMedia = row["IsMedia"] != DBNull.Value && row.Field<bool>("IsMedia");
//        //    VolumeOrdinal = row.Field<byte>("VolumeOrdinal");
//        //    VolumeComment = row.Field<string>("VolumeComment");
//        //}

//        #region Edit Media Sources

//        //public int AddNewSource()
//        //{
//        //    return SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString,
//        //                                     CommandType.Text,
//        //                                     "INSERT INTO DocumentFiles (DocumentId, MediaTypeId, URL,VolumeOrdinal,VolumeComment) VALUES (@DocumentId, @MediaTypeId, @URL, @VolumeOrdinal,@VolumeComment)",
//        //                                     new SqlParameter("@DocumentId", DocumentId),
//        //                                     new SqlParameter("@MediaTypeId", MediaTypeId),
//        //                                     new SqlParameter("@VolumeOrdinal", VolumeOrdinal),
//        //                                     new SqlParameter("@VolumeComment", VolumeComment),
//        //                                     new SqlParameter("@URL", URL));
//        //}

//        //public int UpdateSource()
//        //{
//        //    return SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString,
//        //                                     CommandType.Text,
//        //                                     "UPDATE DocumentFiles SET DocumentId=@DocumentId, MediaTypeId=@MediaTypeId, URL=@URL WHERE FileId=@FileId",
//        //                                     new SqlParameter("@FileId", FileId),
//        //                                     new SqlParameter("@DocumentId", DocumentId),
//        //                                     new SqlParameter("@MediaTypeId", MediaTypeId),
//        //                                     new SqlParameter("@URL", URL));
//        //}

//        //public static void DeleteSource(int FileId)
//        //{
//        //    SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["Public"].ConnectionString,
//        //                              CommandType.Text, "DELETE FROM DocumentFiles WHERE FileId = @FileId",
//        //                              new SqlParameter("@FileId", FileId));
//        //}

//        #endregion

//        //public bool IsMedia { get; set; }
//        //public DateTime CreateDate { get; set; }
//        //public int DocumentId { get; set; }
//        //public decimal Duration { get; set; }
//        //public Int64? FileSize { get; set; }
//        //public int FileId { get; set; }
//        //public int MediaTypeId { get; set; }
//        //public string URL { get; set; }
//        //public string Description { get; set; }
//        //public string MediaIconPath { get; set; }
//        //public byte VolumeOrdinal { get; set; }
//        //public string VolumeComment { get; set; }

//        public static ContentDocument GetDocumentByFileId(int fileId)
//        {
//            var doc = new ContentDocument();
//            doc.DocumentFromFileId(fileId);
//            return doc;
//        }

//        public static string GetYouTubeEmbedCode(string url)
//        {
//            if (string.IsNullOrEmpty(url)) return string.Empty;
//            url = url.Replace("&hd=1", "").Replace("&amp;hd=1","");
//            if (url.IndexOf("v=") > 1)
//                url = url.Substring(url.IndexOf("v=") + 2);

//            const string code = @"<iframe title=""YouTube video player"" width=""480"" height=""390"" src=""http://www.youtube.com/embed/{0}"" frameborder=""0"" allowfullscreen></iframe>";


//            return string.Format(code, url);
//        }
//    }
//}