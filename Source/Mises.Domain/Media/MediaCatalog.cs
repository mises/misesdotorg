#region

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Mises.Data;

#endregion

namespace Mises.Domain.Media
{
    public class MediaCatalog : DataHelper
    {
        #region Public Properties

        private int _results;
        private DataSet dsMediaFiles;

        public int results
        {
            get { return _results; }
        }


        public DataTable dtCategoryList
        {
            get { return dsMediaFiles.Tables[0]; }
        }

        public DataTable dtMediafile
        {
            get { return dsMediaFiles.Tables[1]; }
        }

        public DataTable dtCategoryInfo
        {
            get { return dsMediaFiles.Tables[2]; }
        }

        public static DataSet GetContributorList()
        {
            return ExecuteDataset("MediaGetContributorList");
        }

        #endregion

        public void GetCategoryContents()
        {
            GetCategoryContents(0);
        }

//INSTANT C# NOTE: C# does not support optional parameters. Overloaded method(s) are created above.
//ORIGINAL LINE: Public Sub GetCategoryContents(Optional ByVal CategoryId As Integer = 0)
        public void GetCategoryContents(int CategoryId)
        {
            dsMediaFiles = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                                    "dbo.MediaGetCategoryContent",
                                                    new SqlParameter("@CategoryId", CategoryId));
            _results = dsMediaFiles.Tables[0].Rows.Count;
        }

        public void GetSubjectMatches(int SubjectId)
        {
            dsMediaFiles = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                                    "dbo.MediaGetCategoryContent",
                                                    new SqlParameter("@SubjectId", SubjectId),
                                                    new SqlParameter("@CategoryId", -1));
            _results = dsMediaFiles.Tables[0].Rows.Count;
        }

        public void GetContributorFiles(int AuthorId)
        {
            dsMediaFiles = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                                    "dbo.MediaGetAuthorContents",
                                                    new SqlParameter("@AuthorId", AuthorId));
            _results = dsMediaFiles.Tables[0].Rows.Count;
        }

        public static DataSet GetMediaAuthors()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                            "dbo.MediaGetAuthors");
            //_results = dsMediaFiles.Tables[0].Rows.Count;
        }

        public static DataSet GetMediaAuthorsAlpha()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                            "dbo.MediaGetAuthorsAlpha");
            //_results = dsMediaFiles.Tables[0].Rows.Count;
        }


        public void MediaSearch(string SearchTerm)
        {
            dsMediaFiles = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.MediaSearch",
                                                    new SqlParameter("@SearchClause", SearchTerm));
            _results = dsMediaFiles.Tables[1].Rows.Count + dsMediaFiles.Tables[0].Rows.Count;
        }


        //<Obsolete()> Public ReadOnly Property GetCategories() As DataTable
        //    Get
        //        Dim dt As DataTable = SqlHelper.ExecuteDataset(MisesWeb.BusinessBase.ConnectionString, CommandType.StoredProcedure, "dbo.MediaGetRootCategories").Tables[0]
        //        _results = dsMediaFiles.Tables[0].Rows.Count
        //        Return dt
        //    End Get
        //End Property


        ///<summary>
        ///  Gets the Media RSS feed.
        ///</summary>
        ///<param name = "AuthorId">The author id.</param>
        ///<returns></returns>
        public static DataSet GetRSSFeed(int AuthorId)
        {
            return GetRSSFeed(AuthorId, 0, 0);
        }

        public static DataSet GetRSSFeed()
        {
            return GetRSSFeed(0, 0, 0);
        }

        public static DataSet GetRSSFeed(int AuthorId, int CategoryId, int MediaTypeId)
        {
            return GetRSSFeed(AuthorId, CategoryId, MediaTypeId, 0);
        }

        private static DataSet GetRSSFeed(int AuthorId, int CategoryId, int MediaTypeId, int MaxResults)
        {
            return SqlHelper.ExecuteDataset(ConnectionString, "dbo.MediaGetFeed",
                                            new SqlParameter("@AuthorId", AuthorId),
                                            new SqlParameter("@CategoryId", CategoryId),
                                            new SqlParameter("@MediaTypeId", MediaTypeId),
                                            new SqlParameter("@MaxResults", MaxResults));

            //var dsMedia =

//            var media = (from DataRow row in dsMedia.Tables[0].Rows select new MediaFile(row)).ToList();

//            DocumentId
//Title
//GUID
//Description
//URL
//MediaId
//MIMEtypeLength
//Author
//Author1
//CreateDate
//fileSize
//duration
//metaDescription
//            media[0].

//            return media;
        }

        public static DataSet GetNewMedia()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.MediaGetLatest");
        }

        public static List<MediaGetFeaturedResult> GetFeaturedMedia()
        {
            var db = DataHelper.MisesDataContext;
            var featured = db.MediaGetFeatured().ToList();

            return featured;
        }

        public static DataSet GetMisesLive()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "dbo.MediaGetMisesLive");
        }

        public static DataSet GetNewAudio()
        {
            return GetRSSFeed(0, 0, 1, 6);
        }


        public static DataSet GetNewVideo()
        {
            return GetRSSFeed(0, 0, 2, 6);
        }


        public object GetMediaByType(int MediaTypeId)
        {
            dsMediaFiles = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure,
                                                    "dbo.MediaGetMediaByType",
                                                    new SqlParameter("@MediaTypeId", MediaTypeId));
            _results = dsMediaFiles.Tables[0].Rows.Count;
            return dsMediaFiles;
        }

        public static string GetYouTubeEmbedCode(string url)
        {
            if (string.IsNullOrEmpty(url)) return string.Empty;
            url = url.Replace("&hd=1", "").Replace("&amp;hd=1", "");
            if (url.IndexOf("v=") > 1)
                url = url.Substring(url.IndexOf("v=") + 2);

            const string code = @"<iframe title=""YouTube video player"" width=""480"" height=""390"" src=""http://www.youtube.com/embed/{0}"" frameborder=""0"" allowfullscreen></iframe>";


            return string.Format(code, url);
        }
    }
}