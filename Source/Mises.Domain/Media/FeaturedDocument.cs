﻿#region

using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Mises.Data;

#endregion

namespace Mises.Domain.Media
{
    public class FeaturedDocument
    {
        public IEnumerable<DocumentDTO> FeaturedDocuments
        {
            get
            {
                using (
                    var context =
                        new MisesDBDataContext(ConfigurationManager.ConnectionStrings["Public"].ConnectionString))
                {
                    return
                        context.DocumentTBs
                            .Join(context.DocumentAuthorTBs, d => d.Author1, da => da.AuthorId,
                                  (d, da) => new {D = d, DA = da})
                            .Join(context.DocumentFileTBs, a => a.D.DocumentId, ma => ma.DocumentId,
                                  (a, ma) => new DocumentDTO
                                                 {
                                                     DocumentId = a.D.DocumentId,
                                                     GUID = a.D.GUID,
                                                     Display = a.D.Display,
                                                     Author1 = a.D.Author1,
                                                     Author2 = a.D.Author2,
                                                     CategoryId = a.D.CategoryId,
                                                     CreateDate = a.D.CreateDate,
                                                     EditDate = a.D.EditDate,
                                                    // Featured = a.D.Featured,
                                                     //Length = a.D.DocumentFiles.,
                                                     Description = a.D.Description,
                                                     MetaKeyWords = a.D.Keywords,
                                                     PublicationInformation = a.D.PublicationInformation,
                                                     Source = a.D.Source,
                                                     Title = a.D.Title,
                                                     ImageBinary = a.D.CoverImage,
                                                     Author1Name =
                                                         string.Format("{0} {1} {2}", a.DA.AuthorFirst,
                                                                       a.DA.AuthorMiddle, a.DA.AuthorLast),
                                                     MediaTypeId = ma.MediaTypeId,
                                                     URL = ma.URL,
                                                     MediaId = ma.FileId
                                                 });
                }
            }
        }
    }
}