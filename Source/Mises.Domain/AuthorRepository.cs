﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using Mises.Data;
using Mises.Domain.Contracts;
using System.Data.Entity.Core.Objects;

#endregion

namespace Mises.Domain
{
    public class AuthorRepository : IAuthorRepository
    {
        public AuthorRepository()
            : this(DataHelper.ConnectionString) //TODO for now default to this connection string
        {
        }

        public AuthorRepository(String connectionString)
        {
            ConnectionString = connectionString;
        }

        public String ConnectionString { get; private set; }

        #region IAuthorRepository Members

        /// <summary>
        /// </summary>
        /// <param name = "authorId"></param>
        /// <returns></returns>
        public dynamic GetLatestContribution(Int32 authorId)
        {
            using (var context = DataHelper.EntityDataModel)
            {
                context.DocumentAuthors.MergeOption =System.Data.Objects.MergeOption.NoTracking;
                context.Documents.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                
                //TODO merge these into 1 statement...

                var documentQuery = (from d in context.Documents
                                     where (d.Display
                                            &&
                                            (d.DocumentAuthor.AuthorId == authorId ||
                                             d.DocumentAuthor2.AuthorId == authorId)
                                            && d.DocumentFiles.Any(m => m.Display))
                                     orderby d.CreateDate descending
                                     select new
                                                {
                                                    d,
                                                    DocumentFiles =
                                         (from m in d.DocumentFiles
                                          where m.Display
                                          select m),
                                                    d.MediaCategory,
                                                    d.DocumentAuthor,
                                                    d.DocumentAuthor2
                                                });

                var doc = documentQuery.AsEnumerable().Select(d => d.d).FirstOrDefault();

                var articleQuery = (from a in context.DailyArticles
                                    where (a.ShowArticle && (a.AuthorId == authorId || a.CoAuthorId == authorId))
                                    orderby a.DatePosted descending , a.Title
                                    select a);

                var article = (articleQuery as ObjectQuery<DailyArticle>)
                    .Include("DocumentAuthor")
                    .Include("DocumentAuthor1")
                    .Take(1).SingleOrDefault();

                if ((article == null) || ((doc != null) && (doc.CreateDate >= article.DatePosted)))
                    return doc;
                else
                    return article;
            }
        }

        public PaginableResult<DailyArticle> GetArticleSummaries(IArticleFilter filter, Int32 pageIndex, Int32 pageSize)
        {
            var result = new PaginableResult<DailyArticle> {PageIndex = pageIndex, PageSize = pageSize};

            using (var context = DataHelper.EntityDataModel)
            {
                context.DailyArticles.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                IQueryable query = null;

                // If/when more filters required then this'll need changing (linq expressions the way forward) but for now...

                if (filter == null)
                {
                    query = (from a in context.DailyArticles
                             where (a.ShowArticle)
                             orderby a.DatePosted descending , a.Title
                             select a);
                }
                else if (filter is ArticlesByAuthorFilter)
                {
                    ArticlesByAuthorFilter f = filter as ArticlesByAuthorFilter;

                    query = (from a in context.DailyArticles
                             where (a.ShowArticle && (a.AuthorId == f.Id || a.CoAuthorId == f.Id))
                             orderby a.DatePosted descending , a.Title
                             select a);
                }

                var paged = (query as ObjectQuery<DailyArticle>)
                    .Include("DocumentAuthor")
                    .Include("DocumentAuthor1")
                    .Skip(pageIndex*pageSize)
                    .Take(pageSize);

                result.Items = paged.ToList();
                result.TotalItems = (query as ObjectQuery<DailyArticle>).Count();
            }

            return result;
        }

        /// <summary>
        ///   Returns the author entity.
        ///   NOTE: Only returns current articles, documents, media, etc (i.e. 'display' == true).
        /// </summary>
        /// <param name = "id"></param>
        /// <returns></returns>
        public dynamic GetAuthor(Int32 id
            /*, Int32 documentPageIndex, Int32 documentPageSize, Int32 articlePageIndex, Int32 articlePageSize*/)
        {
            DocumentAuthor result = null;

            using (var context = DataHelper.EntityDataModel)
            {
                //TODO paginate each child entity here (currently done in service)?

                var query =
                    from author in context.DocumentAuthors
                    where (author.AuthorId == id)
                    select new
                               {
                                   author,
                                   DailyArticles =
                        (from article in author.DailyArticles
                         where article.ShowArticle
                         orderby article.DatePosted descending
                         select article),
                                   /*don't .Union here */
                                   DailyArticles1 =
                        (from article2 in author.DailyArticles1
                         where article2.ShowArticle
                         orderby article2.DatePosted descending
                         select article2),
                                   Documents =
                        (from document in author.Documents
                         where document.Display
                         orderby document.CreateDate descending
                         select new
                                    {
                                        document,
                                        document.DocumentAuthor,
                                        document.DocumentAuthor2,
                                        DocumentFiles =
                             from media in document.DocumentFiles
                             where media.Display
                             select media
                                    }),
                                   /*don't .Union here */
                                   Documents1 =
                        (from document in author.Documents1
                         where document.Display
                         orderby document.CreateDate descending
                         select new
                                    {
                                        document,
                                        document.DocumentAuthor,
                                        document.DocumentAuthor2,
                                        DocumentFiles =
                             from media in document.DocumentFiles
                             where media.Display
                             select media
                                    })
                               };

                result = query.AsEnumerable().Select(a => a.author).FirstOrDefault();
            }

            return result;
        }

        public IEnumerable<MediaGetAuthorsAlpha_Result> GetAuthorSummaries()
        {
            using (var context = DataHelper.EntityDataModel)
            {
                var result = context.MediaGetAuthorsAlpha().Distinct();

                return result.ToList();
            }
        }

        public PaginableResult<DocumentAuthor> GetAuthors(IAuthorFilter filter, Int32 pageIndex, Int32 pageSize)
        {
            var result = new PaginableResult<DocumentAuthor>();
            result.PageIndex = pageIndex;
            result.PageSize = pageSize;

            using (var context = DataHelper.EntityDataModel)
            {
                var q1 = from a in context.DocumentAuthors select a;

                if (filter == null)
                {
                    q1 = q1.Where(a => a.AuthorLast.Length > 0);
                }
                else if (filter is AuthorSurnameFilter)
                {
                    AuthorSurnameFilter si = filter as AuthorSurnameFilter;
                    q1 =
                        q1.Where(
                            a =>
                            a.AuthorLast != null && a.AuthorLast.Trim().Length > 0 &&
                            a.AuthorLast.Substring(0, si.Term.Length).ToLower() == si.Term);
                    //or a.AuthorLast.ToLower().Contains(si.Term)
                }
                else if (filter is AuthorSurnameInitialFilter)
                {
                    AuthorSurnameInitialFilter si = filter as AuthorSurnameInitialFilter;
                    var letter = si.Letter.ToString();
                    q1 =
                        q1.Where(
                            a =>
                            (a.AuthorLast != null && a.AuthorLast.Trim().Length > 0 &&
                             a.AuthorLast.Substring(0, 1).ToLower() == letter));
                }
                else if (filter is AuthorIdListFilter)
                {
                    AuthorIdListFilter idfilter = filter as AuthorIdListFilter;
                    q1 = q1.Where(a => idfilter.IdList.Any(i => i == a.AuthorId));
                }

                var q2 =
                    from a in q1
                    orderby a.AuthorLast , a.AuthorFirst , a.AuthorMiddle
                    select new
                               {
                                   a,
                                   DailyArticles =
                        (from article in a.DailyArticles
                         where article.ShowArticle
                         orderby article.DatePosted descending
                         select article),
                                   DailyArticles1 =
                        (from article2 in a.DailyArticles1
                         where article2.ShowArticle
                         orderby article2.DatePosted descending
                         select article2),
                                   Documents =
                        (from document in a.Documents
                         where document.Display
                         orderby document.CreateDate descending
                         select new
                                    {
                                        document,
                                        document.DocumentAuthor,
                                        document.DocumentAuthor2,
                                        DocumentFiles =
                             from media in document.DocumentFiles
                             where media.Display
                             select media
                                    }),
                                   Documents1 =
                        (from document in a.Documents1
                         where document.Display
                         orderby document.CreateDate descending
                         select new
                                    {
                                        document,
                                        document.DocumentAuthor,
                                        document.DocumentAuthor2,
                                        DocumentFiles =
                             from media in document.DocumentFiles
                             where media.Display
                             select media
                                    })
                               };

                var q3 = q2.AsEnumerable()
                    .Select(a => a.a)
                    .Where(
                        a =>
                        a.Documents.Any() || a.Documents1.Any() || a.DailyArticles.Any() ||
                        a.DailyArticles1.Any());

                var paged = q3.Skip(pageIndex*pageSize).Take(pageSize);
                result.Items = paged.ToList();
                result.TotalItems = q3.Count();
            }

            return result;
        }

        public IDictionary<Char, Int32> GetCountsBySurnameInitial()
        {
            IDictionary<Char, Int32> result = null;

            using (var context = DataHelper.EntityDataModel)
            {
                var query = from a in context.DocumentAuthors
                            where (a.AuthorLast != null && a.AuthorLast.Trim().Length > 0)
                                  &&
                                  (a.Documents.Any(d => d.Display) || a.Documents1.Any(d => d.Display) ||
                                   a.DailyArticles.Any(r => r.ShowArticle) || a.DailyArticles1.Any(r => r.ShowArticle))
                            let si = a.AuthorLast.Substring(0, 1).ToLower()
                            group a by si
                            into g
                            orderby g.Key
                            select new {Letter = g.Key, Count = g.Count()};

                result = query.ToDictionary(k => k.Letter[0], v => v.Count);
            }

            return result;
        }

        #endregion

        public List<DocumentAuthorsGetDetailResult> GetAuthorInfo(int AuthorId, int ArticleId)
        {
            var db = DataHelper.MisesDataContext;
            List<DocumentAuthorsGetDetailResult> authors = db.DocumentAuthorsGetDetail(ArticleId, AuthorId).ToList();
            return authors;
        }
    }
}