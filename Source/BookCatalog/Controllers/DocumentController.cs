﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookCatalog.Models;


namespace BookCatalog.Controllers
{
    public class DocumentController : Controller
    {

        #region Detail

        public ActionResult View(int id)
        {
            var db = new LibraryModel();
            var doc = db.Documents.Include("DocumentFiles").Include("DocumentAuthor").Include("DocumentAuthor1").SingleOrDefault(d => d.DocumentId == id);

            //var storeInfo = db.Documents.First(d => d.DocumentId == id);

            var vm = new LiteratureViewModel()
            {
                Description = doc.Description,
                PublicationInformation = doc.PublicationInformation,
                DocumentId = doc.DocumentId,
                Title = doc.Title,
                EditDate = doc.EditDate,
                Thumbnail = "http://mises.org/" + doc.CoverImageURL,
                Formats =new List<DocumentFile>(doc.DocumentFiles)
                //Formats = new List<FileModel>() { new FileModel() { MediaTypeId = (int)doc.MediaTypeId, URL = doc.URL, MediaImage = NavigationModels.GetMediaTypeFromId((int)doc.MediaTypeId).MediaIconPath, FileType = doc.MIMEtype } }
            };

            if (doc.DocumentAuthor != null)
            {
                vm.AuthorId = doc.DocumentAuthor.AuthorId;
                vm.Author = doc.DocumentAuthor.ToString();
            }
            else if (doc.DocumentAuthor1 != null)
            {
                vm.AuthorId = doc.DocumentAuthor1.AuthorId;
                vm.Author = doc.DocumentAuthor1.ToString();
            }

            //doc.DocumentFiles.ToList().ForEach(f => vm.Formats.Add(new FileModel()
            //{
            //    URL = f.URL,
            //    FileType = NavigationModels.GetMediaTypeFromId(f.MediaTypeId).MediaType,
            //    MediaImage = NavigationModels.GetMediaTypeFromId(f.MediaTypeId).MediaIconPath,
            //    FileSize = f.fileSize,
            //    Duration = f.duration
            //}));


            return View(vm);

            //return GetDocumentList(documentId);
        }

        #endregion


    }
}
