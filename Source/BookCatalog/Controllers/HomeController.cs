﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BookCatalog.Models;
using LinqKit;

namespace BookCatalog.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Literature/

        private int[] bookDocTypes = { 4, 6, 9, 3 };

        #region Filter Lists

        public ActionResult Authors(int id = 0, int page = 0)
        {
            var mises = new LibraryModel();

            var authorList = (from author in mises.DocumentAuthors
                              select new AuthorViewModel
                                         {
                                             AuthorFirst = author.AuthorFirst,
                                             AuthorMiddle = author.AuthorMiddle,
                                             AuthorLast = author.AuthorLast,
                                             AuthorId = author.AuthorId,
                                             BioText = author.BioText,
                                             Born = author.Born,
                                             Died = author.Died,
                                             Books = author.Documents.Count,
                                             Photo = author.Photo

                                         }).ToList();

            return View(authorList);
            //return View(mises.DocumentAuthors.ToList());
        }

        #endregion

        #region Browse

        public const int PageSize = 50;

        [OutputCache(Duration = 3600,VaryByParam = "page")]
        public ActionResult Index(int page = 0)
        {
            return GetDocumentList(page);
        }

        [OutputCache(Duration = 3600, VaryByParam = "page")]
        public ActionResult Author(int id, int page = 0)
        {
            return GetDocumentList(page, null, id);
        }

        [OutputCache(Duration = 3600, VaryByParam = "page")]
        public ActionResult Subject(int id, int page = 0)
        {
            return GetDocumentList(page, null, 0, id);
        }

        [OutputCache(Duration = 3600, VaryByParam = "page")]
        public ActionResult Source(string id, int page = 0)
        {
            return GetDocumentList(page, id.ToLower() == "books" ? "books" : null, 0, 0, id);
        }

        [OutputCache(Duration = 3600, VaryByParam = "page")]
        public ActionResult Format(int id, int page = 0)
        {
            return GetDocumentList(page, null, 0, 0, null, id);
        }

        public ActionResult ClientSearch(string text)
        {
            var mises = new LibraryModel();
            var docs = mises.Documents.Where(d => d.DocumentFiles.Any(file => bookDocTypes.Contains(file.MediaTypeId))).Where(b => b.Title.Contains(text) || b.DocumentAuthor.AuthorLast.Contains(text) || b.Description.Contains(text) || b.Keywords.Contains(text))
                .Select(f => new { Title = f.DocumentAuthor.AuthorLast + ": " + f.Title, f.DocumentId });

            return new JsonResult
                       {
                           JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                           Data = new SelectList(docs.ToList(), "DocumentId", "Title")
                       };

        }

        public ActionResult Search(string q, int page = 0)
        {
            string term = q.Substring(q.IndexOf(":", System.StringComparison.Ordinal) + 2);
            return GetDocumentList(page, term, 0, 0, null, 0);
        }

        #endregion Browse



        public ActionResult Test()
        {
            var mises = new LibraryModel();

            var query2 = (from docs in mises.Documents where docs.DocumentId == 3088 orderby docs.DocumentId descending select docs).ToList();
            query2.ForEach(v => Response.Write(string.Format("<h2>{0}</h2>", v.DocumentId.ToString())));


            //var query = mises.Documents.Where(d => d.DocumentId == 3088).OrderBy(d => d.EditDate).ToList();
            return Content(query2.Skip(1).First().DocumentAuthor.AuthorLast.ToSlug());

        }
        #region Data Access


        private ActionResult GetDocumentList(int page, string filter = null, int AuthorId = 0, int SubjectId = 0, string source = null, int MediaTypeId = 0)
        {
            ViewBag.PageSize = PageSize;
            ViewBag.CurrentPage = page;
            ViewBag.Total = 0;

            var mises = new LibraryModel();


            var query = mises.Documents.Where(docs => docs.DocumentFiles.Any(file => bookDocTypes.Contains(file.MediaTypeId))).Where(v => v.Display);

            //if (MediaTypeId == 0 && filter == null)
            //{
            //    query = query.Where(d => bookDocTypes.Contains(d.DocumentFiles.URL.Value));
            //}

            #region Filters

            if (AuthorId > 0)
            {
                query = query.Where(a => a.Author1 == AuthorId || a.Author2 == AuthorId);

                // get author info

                var author = mises.DocumentAuthors.SingleOrDefault(d => d.AuthorId == AuthorId);
                ViewBag.Author = new AuthorViewModel
                                     {
                                         AuthorFirst = author.AuthorFirst,
                                         AuthorMiddle = author.AuthorMiddle,
                                         AuthorLast = author.AuthorLast,
                                         AuthorId = author.AuthorId,
                                         BioText = author.BioText,
                                         Born = author.Born,
                                         Died = author.Died,
                                         Books = author.Documents.Count,
                                         Photo = author.Photo
                                     };
            }

            if (SubjectId > 0)
            {
                //query = query.Where(a => a.Author1 == AuthorId || a.Author2 == AuthorId);
                var documentIds = mises.DocumentSubjectLinks.Where(d => d.SubjectID == SubjectId).Select(d => d.DocumentId);
                query = query.Where(d => documentIds.Contains(d.DocumentId));
            }

            if (MediaTypeId > 0)
            {
                query = query.Where(a => a.DocumentFiles.Any(m => m.MediaTypeId == MediaTypeId));
            }

            if (!String.IsNullOrWhiteSpace(source) && source.ToLower() != "books")
            {
                query = query.Where(a => a.Source == source);
            }

            if (filter != null && filter.Equals("books", StringComparison.InvariantCultureIgnoreCase))
            {
                query = query.Where(a => a.DocumentFiles.Any(m => m.MediaTypeId == 9)); // 9 = ebook
            }
            else if (filter != null)
            {
                query = query.Where(b => b.Title.Contains(filter) || b.Description.Contains(filter) || b.Keywords.Contains(filter));
            }

            #endregion Filters
            ViewBag.Total = query.Count();

            query = query.OrderByDescending(d => d.EditDate).Skip(PageSize * page).Take(PageSize);

            var books = new List<LiteratureViewModel>();

            query.ForEach(doc => books.Add(new LiteratureViewModel()
                                                              {
                                                                  Author = doc.DocumentAuthor.ToString(),
                                                                  AuthorId = doc.Author1,
                                                                  Description = doc.Description,
                                                                  DocumentId = doc.DocumentId,
                                                                  Thumbnail = doc.CoverImageURL,
                                                                  Title = doc.Title,
                                                                  EditDate = doc.EditDate,
                                                                  Formats = doc.DocumentFiles.ToList()
                                                              }));

            books.ForEach(book => book.Formats.ForEach(f =>
                                                           {
                                                               // hack for testing - remove
                                                               f.URL = f.URL.Replace("http://mises.org",
                                                                                     "http://library.freecapitalists.org/");
                                                           }));

            //docs.ForEach(doc =>
            //{
            //    if (!books.ContainsKey(doc.DocumentId))
            //    {
            //        // add new record
            //        books.Add(doc.DocumentId, new LiteratureViewModel()
            //        {
            //            Author = doc.Author1.ToString(),
            //            AuthorId = doc.Author1,
            //            Description = doc.Description,
            //            DocumentId = doc.DocumentId,
            //            Thumbnail = doc.CoverImage,
            //            Title = doc.Title,
            //            EditDate = doc.EditDate,
            //            Formats = new List<FileModel>() { new FileModel() { MediaTypeId = (int)doc.MediaTypeId, URL = doc.URL, MediaImage = NavigationModels.GetMediaTypeFromId((int)doc.MediaTypeId).MediaIconPath, FileType = doc.MIMEtype } }

            //        });

            //    }
            //    else
            //    {
            //        // add new filetype
            //        var book = books[doc.DocumentId];
            //        book.Formats.Add(new FileModel() { MediaTypeId = (int)doc.MediaTypeId, URL = doc.URL, MediaImage = NavigationModels.GetMediaTypeFromId((int)doc.MediaTypeId).MediaIconPath, FileType = doc.MIMEtype });

            //    }
            //});


            return View("Index", books);
        }



        #endregion

        public ActionResult About()
        {
            return View();
        }
    }
}
