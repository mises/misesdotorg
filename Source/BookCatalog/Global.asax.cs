﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BookCatalog
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        private const string _ContraintId = @"\d+|\<\#\=.+?\#\>";

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "detail2", // Route name
               "{controller}/{id}/{slug}", // URL with parameters,
               new { controller = "Document", action = "View", id = _ContraintId, slug = UrlParameter.Optional },
                new { id = @"\d+" /*, slug = @"\[-a-zA-Z]+"*/}
               );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );



            routes.MapRoute(
             "PathWithName", // Route name
             "{controller}/{action}/{id}/{name}", // URL with parameters
             new { controller = "Home", action = "Index", id = UrlParameter.Optional, name = UrlParameter.Optional } // Parameter defaults
         );



            //routes.MapRoute(
            //    "author", // Route name
            //    "Home/Author/{id}/{name}", // URL with parameters
            //    new { controller = "Home", action = "Author", id = UrlParameter.Optional } // Parameter defaults
            //);

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}