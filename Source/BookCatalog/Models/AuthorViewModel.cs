using System;

namespace BookCatalog.Models
{
    public class AuthorViewModel
    {
        private string _photo;
        public string Born { get; set; }
        public string Died { get; set; }
        public int Books { get; set; }
        public string BioText { get; set; }
        public string AuthorFirst { get; set; }

        public string AuthorMiddle { get; set; }

        public string AuthorLast { get; set; }

        public int AuthorId { get; set; }

        public string Photo
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_photo))
                {
                    return "/Content/Images/image-missing.png";

                }
                else if (_photo.StartsWith("http"))
                {
                    return _photo;
                }
                else
                {
                    return "http://mises.org/" + _photo;
                }
            }
            set
            {
                _photo = value;
            }
        }

        public override String ToString()
        {
            return this.AuthorFirst + " " + this.AuthorMiddle + " " + this.AuthorLast;
        }
    }
}