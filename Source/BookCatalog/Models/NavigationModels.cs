﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Web.Mvc;
using System.Linq;

namespace BookCatalog.Models
{
    public static class NavigationModels
    {
        public static IEnumerable<KeyValuePair<int, string>> Authors(int max = 50)
        {
            var model = new LibraryModel();
            model.Documents.MergeOption = MergeOption.NoTracking;
            var anonAuthors = from doc in model.Documents
                              group doc by doc.DocumentAuthor
                                  into names
                                  select new
                                             {
                                                 AuthorId = names.Key,
                                                 Count = names.Count(),
                                                 names.FirstOrDefault().DocumentAuthor.AuthorFirst,
                                                 names.FirstOrDefault().DocumentAuthor.AuthorMiddle,
                                                 names.FirstOrDefault().DocumentAuthor.AuthorLast
                                             };

            var authors = anonAuthors.OrderByDescending(a=> a.Count).Take(max).ToDictionary(d => d.AuthorId.AuthorId, d => (d.AuthorFirst + ' ' + d.AuthorMiddle + ' ' + d.AuthorLast).ToString()).Take(max);
            //model.Documents.GroupBy(d=> d.Author1).Select(d => new { d.Author1, d.Author }).ToDictionary(d => d.Author1, d => d.Author).Take(100);

            return authors;
        }

        public static IEnumerable<KeyValuePair<int, string>> Categories()
        {
            var model = new LibraryModel();
            model.Documents.MergeOption = MergeOption.NoTracking;
            var authors = model.DocumentSubjects.Where(v => v.Visible == true).Select(d => new { d.SubjectId, d.Subject }).Distinct().ToDictionary(d => d.SubjectId, d => d.Subject).Take(100);

            return authors;
        }

        public static IEnumerable<string> Sources()
        {
            var model = new LibraryModel();
            model.Documents.MergeOption = MergeOption.NoTracking;
            var sources = model.Documents.Where(d => d.Source.Length > 0).Select(d => d.Source).Distinct().Take(50).ToList();
            sources.Insert(0, "Books");
            return sources;
        }

        public static SelectList SourcesList()
        {
            return new SelectList(Sources(), "Key", "Key");


        }

        public static IEnumerable<KeyValuePair<int, string>> Formats()
        {
            var model = new LibraryModel();
            model.DocumentMediaTypes.MergeOption = MergeOption.NoTracking;
            var formats = model.DocumentMediaTypes.Where(v => v.IsMedia == false).Select(d => new { d.MediaTypeID, d.MediaType }).Distinct().ToDictionary(d => d.MediaTypeID, d => d.MediaType);

            return formats;
        }

        private static List<DocumentMediaType> mediaTypes;

        public static List<DocumentMediaType> MediaTypes
        {
            get
            {
                if (mediaTypes != null) return mediaTypes;

                var model = new LibraryModel();
                model.DocumentMediaTypes.MergeOption = MergeOption.NoTracking;
                var formats = model.DocumentMediaTypes;
                mediaTypes = formats.ToList();
                return mediaTypes;
            }
        }

        public static DocumentMediaType GetMediaTypeFromId(int mediaTypeId)
        {
            return MediaTypes.SingleOrDefault(i => i.MediaTypeID == mediaTypeId);
        }
    }
}