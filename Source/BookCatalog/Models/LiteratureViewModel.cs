﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace BookCatalog.Models
{
    public class LiteratureViewModel
    {
        public LiteratureViewModel()
        {
            Formats = new List<DocumentFile>();
        }

        public int DocumentId { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string URL { get { return string.Format("~/document/{0}/{1}", DocumentId, Title.ToSlug()); }
        }

      //  public string PurchaseURL { get; set; }

        public string Thumbnail { get; set; }

        public int AuthorId { get; set; }

        public string Description { get; set; }

       // public string MediaImage { get; set; }

        public DateTime EditDate { get; set; }

        public List<DocumentFile> Formats { get; set; }

        public string PublicationInformation { get; set; }
    }

    //public class FileModel
    //{
    //    public string URL { get; set; }

    //    public int MediaTypeId { get; set; }

    //    public string MediaImage { get; set; }

    //    public string FileType { get; set; }

    //    public long FileSize { get; set; }

    //    public decimal Duration { get; set; }
    //}

}