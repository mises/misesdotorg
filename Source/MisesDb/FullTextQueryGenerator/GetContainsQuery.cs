﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using FullTextQueryGenerator;
using System.Security.Permissions;

// see http://www.blackbeltcoder.com/Articles/data/easy-full-text-search-queries

[HostProtection(MayLeakOnAbort=false)]
public partial class UserDefinedFunctions
{
	private static readonly EasyFts _easyFts;

	static UserDefinedFunctions()
	{
		_easyFts = new EasyFts();

		_easyFts.StopWords.Add("a");
		_easyFts.StopWords.Add("an");
		_easyFts.StopWords.Add("and");
		_easyFts.StopWords.Add("are");
		_easyFts.StopWords.Add("as");
		_easyFts.StopWords.Add("at");
		_easyFts.StopWords.Add("be");
		_easyFts.StopWords.Add("by");
		_easyFts.StopWords.Add("for");
		_easyFts.StopWords.Add("from");
		_easyFts.StopWords.Add("has");
		_easyFts.StopWords.Add("he");
		_easyFts.StopWords.Add("in");
		_easyFts.StopWords.Add("is");
		_easyFts.StopWords.Add("it");
		_easyFts.StopWords.Add("its");
		_easyFts.StopWords.Add("of");
		_easyFts.StopWords.Add("on");
		_easyFts.StopWords.Add("that");
		_easyFts.StopWords.Add("the");
		_easyFts.StopWords.Add("to");
		_easyFts.StopWords.Add("was");
		_easyFts.StopWords.Add("were");
		_easyFts.StopWords.Add("will");
		_easyFts.StopWords.Add("with");
	}

	[Microsoft.SqlServer.Server.SqlFunction]
	public static SqlString GetContainsQuery(SqlString webQuery)
	{
		if (webQuery.IsNull)
		{
			return SqlString.Null;
		}
		else
		{
			return new SqlString(_easyFts.ToFtsQuery(webQuery.Value));
		}
	}
};

