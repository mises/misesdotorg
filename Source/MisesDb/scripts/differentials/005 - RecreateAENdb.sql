USE [Mises]
GO

/****** Object:  Table [dbo].[AENdb]    Script Date: 05/24/2011 20:41:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AENdb1](
	[control] [int] NOT NULL IDENTITY,
	[display] [varchar](3) NULL,
	[volume] [int] NULL,
	[number] [varchar](2) NULL,
	[articleNum] [varchar](2) NULL,
	[title] [varchar](255) NULL,
	[authorFirst1] [varchar](50) NULL,
	[authorLast1] [varchar](50) NULL,
	[authorFirst2] [varchar](50) NULL,
	[authorLast2] [varchar](50) NULL,
	[link] [varchar](50) NULL,
	[fileType] [varchar](4) NULL,
 CONSTRAINT [PK_aenDB1] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [Mises].[dbo].[AENdb1]
           ([display]
           ,[volume]
           ,[number]
           ,[articleNum]
           ,[title]
           ,[authorFirst1]
           ,[authorLast1]
           ,[authorFirst2]
           ,[authorLast2]
           ,[link]
           ,[fileType])
SELECT [display]
      ,[volume]
      ,[number]
      ,[articleNum]
      ,[title]
      ,[authorFirst1]
      ,[authorLast1]
      ,[authorFirst2]
      ,[authorLast2]
      ,[link]
      ,[fileType]
  FROM [Mises].[dbo].[AENdb]
GO

GO

/****** Object:  Table [dbo].[AENdb]    Script Date: 05/24/2011 20:48:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AENdb]') AND type in (N'U'))
DROP TABLE [dbo].[AENdb]
GO


Exec sp_rename 'AENdb1', 'AENdb'

GO

