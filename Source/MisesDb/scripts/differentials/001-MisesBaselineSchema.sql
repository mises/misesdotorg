USE @MisesDatabase@
GO

/****** Object:  Table [dbo].[RegistrationProducts]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrationProducts](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[FormId] [int] NOT NULL,
	[ProductName] [varchar](50) NULL,
	[Price] [smallmoney] NULL,
	[Description] [varchar](max) NULL,
	[SelectQuantity] [bit] NULL,
	[ProductOrder] [int] NULL,
 CONSTRAINT [PK_RegistrationProducts] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuizAnswer]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuizAnswer](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionID] [int] NOT NULL,
	[Answer] [varchar](max) NOT NULL,
	[AnswerValue] [int] NOT NULL,
	[AnswerExplanation] [varchar](max) NOT NULL,
 CONSTRAINT [PK_QuizAnswer] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[misesreview]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[misesreview](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[authorfirst] [varchar](50) NOT NULL,
	[authorlast] [varchar](50) NOT NULL,
	[authorfirst2] [varchar](50) NULL,
	[authorlast2] [varchar](50) NULL,
	[authorfirst3] [varchar](50) NULL,
	[authorlast3] [varchar](50) NULL,
	[title] [varchar](255) NOT NULL,
	[body] [varchar](max) NOT NULL,
	[issue] [varchar](11) NULL,
	[issue_season] [int] NULL,
	[issue_year] [int] NULL,
	[CreateDate] [smalldatetime] NULL,
	[EditedBy] [varchar](max) NULL,
 CONSTRAINT [PK_misesreview] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ManagerUsers]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManagerUsers](
	[username] [varchar](20) NOT NULL,
	[password] [varchar](20) NOT NULL,
	[email] [varchar](255) NULL,
	[perms] [varchar](50) NULL,
	[active] [bit] NULL,
	[createDate] [smalldatetime] NULL,
 CONSTRAINT [PK_ManagerUsers] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Faculty]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faculty](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[school] [varchar](100) NULL,
	[email] [varchar](100) NULL,
	[web] [varchar](100) NULL,
	[groups] [varchar](50) NOT NULL,
	[orders] [int] NULL,
	[jobtitle] [varchar](100) NULL,
	[display] [bit] NOT NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuizQuestion]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuizQuestion](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[QuizID] [int] NOT NULL,
	[Question] [varchar](max) NOT NULL,
	[SortOrder] [int] NULL,
	[CorrectAnswer] [tinyint] NULL,
 CONSTRAINT [PK_QuizQuestion] PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Page]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Page](
	[PageId] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[Visible] [bit] NOT NULL,
	[pageTitle] [varchar](255) NOT NULL,
	[pageName] [varchar](255) NULL,
	[folder] [varchar](255) NULL,
	[content] [varchar](max) NOT NULL,
	[metaKeywords] [varchar](255) NULL,
	[metaDescription] [varchar](255) NULL,
	[CreateDate] [datetime] NOT NULL,
	[EditDate] [datetime] NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[PageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oldDocumentCategory]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oldDocumentCategory](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](50) NULL,
 CONSTRAINT [PK_StudyGuideCategories] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[QuizGetList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[QuizGetList]
AS 
    BEGIN	
        SET NOCOUNT ON ;
        SELECT  *
        FROM    dbo.quizType
        WHERE   Display = 1
    END
GO
/****** Object:  Table [dbo].[Donations]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Donations](
	[DonationID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Address] [nvarchar](1000) NULL,
	[City] [nvarchar](100) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[MisesMember] [bit] NULL,
	[Amount] [money] NULL,
	[OtherAmount] [money] NULL,
	[Designation] [varchar](500) NULL,
	[OtherDesignation] [varchar](500) NULL,
	[Recurring] [bit] NULL,
	[RecurringInterval] [int] NULL,
	[GiftEstate] [bit] NULL,
	[GiftIncome] [bit] NULL,
	[CardName] [varchar](100) NULL,
	[CardType] [varchar](50) NULL,
	[CardNumber] [varchar](50) NULL,
	[CardExpMonth] [int] NULL,
	[CardExpYear] [int] NULL,
	[Comments] [varchar](max) NULL,
	[ApprovalCode] [int] NULL,
	[CreateTime] [smalldatetime] NULL,
	[CreateUserID] [int] NULL,
	[ModifyTime] [smalldatetime] NULL,
	[ModifyUserID] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_tblDonation] PRIMARY KEY CLUSTERED 
(
	[DonationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[QuizIncrementCount]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[QuizIncrementCount] @QuizId INT
AS 
    BEGIN	
        SET NOCOUNT ON ;	
        UPDATE  dbo.quizType
        SET     NumTaken = NumTaken + 1
        WHERE   QuizID = @QuizId	
    END
GO
/****** Object:  Table [dbo].[People]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[People](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](300) NULL,
	[MiddleName] [varchar](300) NULL,
	[LastName] [varchar](300) NULL,
	[School] [varchar](300) NULL,
	[Status] [varchar](300) NULL,
	[City] [varchar](300) NULL,
	[State] [varchar](300) NULL,
	[Country] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
	[WebSite] [varchar](300) NULL,
	[BlogURL] [varchar](300) NULL,
	[Specialization] [varchar](4000) NULL,
	[FavoriteBooks] [varchar](4000) NULL,
	[FavoriteThinkers] [varchar](4000) NULL,
	[InstantMessage] [varchar](4000) NULL,
	[PhotoURL] [varchar](4000) NULL,
	[Education] [varchar](4000) NULL,
	[ForumId] [int] NULL,
	[SlashdotNick] [varchar](50) NULL,
	[DiggNick] [varchar](50) NULL,
	[AdditionalInfo] [varchar](8000) NULL,
	[CreateDate] [smalldatetime] NULL,
	[LastEditBy] [varchar](50) NULL,
 CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ticker]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ticker](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[display] [varchar](3) NOT NULL,
	[dateola] [smalldatetime] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[url] [varchar](150) NOT NULL,
 CONSTRAINT [PK_ticker] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[spSaveHomepageItem]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSaveHomepageItem]
    @ItemId INT = 0 ,
    @Display BIT ,
    @Title VARCHAR(255) ,
    @Author1 INT ,
    @Author2 INT ,
    @PubInfo VARCHAR(8000) ,
    @URL VARCHAR(255) ,
    @MediaTypeId INT ,
    @FullText BIT ,
    @Source VARCHAR(35) ,
    @GuideContent TEXT ,
    @Length INT ,
    @CategoryId INT ,
    @ItemOrder INT = 0
AS 
    IF @ItemId > 0 -- Addding New Item
        BEGIN

            UPDATE  HomepageItems
            SET     Display = @Display ,
                    Title = @Title ,
                    Author1 = @Author1 ,
                    Author2 = @Author2 ,
                    PubInfo = @PubInfo ,
                    URL = @URL ,
                    MediaTypeId = @MediaTypeId ,
                    FullText = @FullText ,
                    Source = @Source ,
                    GuideContent = @GuideContent ,
                    Length = @Length ,
                    CategoryId = @CategoryId ,
                    ItemOrder = @ItemOrder
            WHERE   ItemId = @ItemId

--DELETE FROM DocumentSubjectLink WHERE ItemId = @ItemId

        END

    ELSE 
        BEGIN

            INSERT  INTO HomepageItems
                    ( Display ,
                      Title ,
                      Author1 ,
                      Author2 ,
                      PubInfo ,
                      URL ,
                      MediaTypeId ,
                      FullText ,
                      Source ,
                      GuideContent ,
                      Length ,
                      CategoryId ,
                      ItemOrder ,
                      CreateDate
                    )
            VALUES  ( @Display ,
                      @Title ,
                      @Author1 ,
                      @Author2 ,
                      @PubInfo ,
                      @URL ,
                      @MediaTypeId ,
                      @FullText ,
                      @Source ,
                      @GuideContent ,
                      @Length ,
                      @CategoryId ,
                      @ItemOrder ,
                      GETDATE()
                    )

            SELECT  SCOPE_IDENTITY()

        END
GO
/****** Object:  Table [dbo].[DocumentMediaType]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentMediaType](
	[MediaTypeID] [int] IDENTITY(1,1) NOT NULL,
	[MediaType] [varchar](255) NOT NULL,
	[MediaIconPath] [varchar](255) NOT NULL,
	[Extensions] [varchar](500) NULL,
	[UploadPath] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
	[CreateTime] [smalldatetime] NULL,
	[MIMEtype] [varchar](50) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_tblMediaType] PRIMARY KEY CLUSTERED 
(
	[MediaTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[freemarket]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[freemarket](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NULL,
	[GUID] [uniqueidentifier] NULL,
	[authorfirst] [varchar](50) NULL,
	[authorlast] [varchar](50) NULL,
	[authorfirst2] [varchar](50) NULL,
	[authorlast2] [varchar](50) NULL,
	[subject1] [varchar](50) NULL,
	[subject2] [varchar](50) NULL,
	[subject3] [varchar](50) NULL,
	[body] [text] NULL,
	[articledate] [datetime] NULL,
	[PDFurl] [varchar](250) NULL,
 CONSTRAINT [PK_freemarket] PRIMARY KEY NONCLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmr]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmr](
	[filename] [varchar](30) NULL,
	[hyperlinktext] [varchar](50) NOT NULL,
	[control] [int] NOT NULL,
	[date] [datetime] NULL,
	[listorder] [varchar](20) NULL,
 CONSTRAINT [PK_tmr] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BookReviews]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookReviews](
	[ReviewId] [int] IDENTITY(1,1) NOT NULL,
	[BookId] [int] NOT NULL,
	[Rating] [tinyint] NULL,
	[Title] [varchar](255) NULL,
	[Review] [varchar](max) NULL,
	[ScreenName] [varchar](255) NULL,
	[Location] [varchar](255) NULL,
	[ReviewDate] [smalldatetime] NULL,
 CONSTRAINT [PK_BookReviews] PRIMARY KEY CLUSTERED 
(
	[ReviewId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegistrationForms]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrationForms](
	[FormId] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[FormTitle] [varchar](500) NOT NULL,
	[FormAuthor] [varchar](128) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[TakeCreditCard] [bit] NOT NULL,
	[ProcessPayment] [bit] NOT NULL,
	[GetAddressInfo] [bit] NOT NULL,
	[Introduction] [varchar](max) NOT NULL,
	[FooterText] [varchar](max) NULL,
	[ThankYouMessage] [varchar](8000) NOT NULL,
	[EmailConfirmation] [varchar](max) NULL,
	[EmailAddress] [varchar](128) NULL,
	[Image] [varchar](255) NOT NULL,
	[PrimaryProductId] [int] NOT NULL,
	[EnterAmountPrompt] [bit] NULL,
 CONSTRAINT [PK_RegistrationForms] PRIMARY KEY CLUSTERED 
(
	[FormId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[CountriesGetList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CountriesGetList]
AS 
    BEGIN
	
        SET NOCOUNT ON ;
	
        SELECT  Name ,
                CountryCode ,
                1 AS Sort
        FROM    AbleCommerce..ac_countries WITH ( NOLOCK )
        WHERE   CountryCode = 'US'
        UNION ALL
        SELECT  Name ,
                CountryCode ,
                2 AS Sort
        FROM    AbleCommerce..ac_countries WITH ( NOLOCK )
        ORDER BY SORT

	
    END
GO
/****** Object:  Table [dbo].[fellows]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fellows](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[bio] [text] NOT NULL,
	[image] [varchar](100) NOT NULL,
	[display] [varchar](3) NOT NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_fellows] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[donsp_aDonationBill]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[donsp_aDonationBill]
    @BillDate DATETIME = NULL
AS 
    BEGIN
    --
    -- Author: Howard Hart
    -- Date..: 5/8/2004
    --
    -- Creates new billing records based on a 30 day cycle.  Can override the date for testing, otherwise
    -- the current date is used
    --
    -- Revision History
    -- -----------------
    --
        DECLARE @Day VARCHAR(2) ,
            @Month VARCHAR(2) ,
            @Year VARCHAR(4) ,
            @Debug VARCHAR(255)
           
        IF @BillDate IS NULL 
            BEGIN
                SELECT  @BillDate = GETDATE()
            END
    --
    -- Get the 'current' date and the target date
    --
        SELECT  @Day = CONVERT(VARCHAR(2), DATEPART(day, @BillDate)) ,
                @Month = CONVERT(VARCHAR(2), DATEPART(month, @BillDate)) ,
                @Year = CONVERT(VARCHAR(4), DATEPART(year, @BillDate))
        IF @Month = 1 
            BEGIN
                SELECT  @Month = 12
            END
        ELSE 
            BEGIN
                SELECT  @Month = @Month - 1
            END
        SELECT  @Debug = 'Month: ' + CONVERT(VARCHAR(2), @Month) + ' Day: '
                + CONVERT(VARCHAR(2), @Day) + ' Year: '
                + CONVERT(VARCHAR(4), @Year)
        PRINT @Debug
    -- Create the new records
        INSERT  tblDonation
                ( FirstName ,
                  LastName ,
                  Address ,
                  City ,
                  State ,
                  Zip ,
                  Country ,
                  Email ,
                  MisesMember ,
                  Amount ,
                  OtherAmount ,
                  Designation ,
                  OtherDesignation ,
                  Recurring ,
                  LongTermGiving ,
                  CardName ,
                  CardType ,
                  CardNumber ,
                  CardExpMonth ,
                  CardExpYear ,
                  Comments ,
                  ApprovalCode ,
                  CreateTime ,
                  CreateUserID ,
                  ModifyTime ,
                  ModifyUserID ,
                  IsDeleted
                )
                SELECT  FirstName ,
                        LastName ,
                        Address ,
                        City ,
                        State ,
                        Zip ,
                        Country ,
                        Email ,
                        MisesMember ,
                        Amount ,
                        OtherAmount ,
                        Designation ,
                        OtherDesignation ,
                        Recurring ,
                        LongTermGiving ,
                        CardName ,
                        CardType ,
                        CardNumber ,
                        CardExpMonth ,
                        CardExpYear ,
                        Comments ,
                        ApprovalCode ,
                        @BillDate ,
                        CreateUserID ,
                        @BillDate ,
                        ModifyUserID ,
                        IsDeleted
                FROM    tblDonation d
                WHERE   Recurring = 1
                        AND DATEPART(month, CreateTime) = DATEPART(month,
                                                              DATEADD(month,
                                                              ( -1
                                                              * RecurringInterval ),
                                                              @BillDate))
                        AND DATEPART(day, CreateTime) = @Day
                        AND DATEPART(year, CreateTime) = @Year
                        AND IsDeleted = 0
                        AND NOT EXISTS ( SELECT 1
                                         FROM   tblDonation
                                         WHERE  CardNumber = d.CardNumber
                                                AND DATEPART(month, CreateTime) = DATEPART(month,
                                                              DATEADD(month,
                                                              RecurringInterval,
                                                              @BillDate))
                                                AND DATEPART(day, CreateTime) = @Day
                                                AND DATEPART(year, CreateTime) = @Year )
    END
GO
/****** Object:  Table [dbo].[WardLibrary]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WardLibrary](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[display] [varchar](3) NULL,
	[title] [varchar](255) NOT NULL,
	[checked_out_by] [varchar](255) NULL,
	[authorfirst] [varchar](50) NULL,
	[authorlast] [varchar](50) NULL,
	[authorfirst2] [varchar](50) NULL,
	[authorlast2] [varchar](50) NULL,
	[authorfirst3] [varchar](50) NULL,
	[authorlast3] [varchar](50) NULL,
	[publisher_info] [varchar](500) NULL,
	[comments] [text] NULL,
	[donatedBy] [varchar](100) NULL,
	[donatedDate] [smalldatetime] NULL,
	[coauthors] [varchar](500) NULL,
	[subject] [varchar](255) NULL,
	[CreateDate] [smalldatetime] NULL,
 CONSTRAINT [PK_wardlibrary] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[CommunityGetFeed]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CommunityGetFeed]
AS 
    BEGIN
        SET NOCOUNT ON ;

-- Forums
        SELECT TOP 20
--T.*,
                T.ThreadId ,
                T.PostDate ,
                T.PostAuthor ,
                T.UserId ,
                T.ThreadDate ,
                ( SELECT TOP 1
                            subject
                  FROM      MisesCommunity..cs_Posts
                  WHERE     cs_Posts.ThreadID = T.ThreadID
                ) AS 'Subject'
        FROM    MisesCommunity..cs_Threads T WITH ( NOLOCK )
--INNER JOIN MisesCommunity..cs_Posts P ON p.ThreadID = T.ThreadID 
        WHERE   ( SELECT TOP 1
                            ApplicationPostType
                  FROM      MisesCommunity..cs_Posts WITH ( NOLOCK )
                  WHERE     cs_Posts.ThreadID = T.ThreadID
                ) <> 1
                AND T.IsApproved = 1
                AND T.sectionid NOT IN ( 1, 2,9, 81, 82, 83, 84, 86, 87, 88, 89,
                                         90, 91, 92, 93, 94, 310 )
        ORDER BY T.ThreadDate DESC
  
  
-- Blogs
        SELECT TOP 20
                * ,
                PostID ,
                PostAuthor AS author ,
                Subject AS title ,
                UserId ,
                PostDate AS PubDate ,
                PostName ,
                '' AS link ,
                UserTime ,
                ApplicationKey ,
                PostStatus
        FROM    MisesCommunity..cs_Posts WITH ( NOLOCK )
                JOIN MisesCommunity..cs_Sections WITH ( NOLOCK ) ON cs_Sections.SectionID = cs_Posts.SectionID
        WHERE   cs_Posts.[ApplicationPostType] = 1
                AND IsApproved = 1
                AND cs_Sections.SectionID NOT IN ( 86, 81 )
                AND ApplicationType = 1
                AND IsIndexed = 1
        ORDER BY cs_Posts.PostDate DESC   
   
    END
GO
/****** Object:  StoredProcedure [dbo].[_SearchAndReplace]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[_SearchAndReplace]
    (
      @SearchStr NVARCHAR(100) ,
      @ReplaceStr NVARCHAR(100)
    )
AS 
    BEGIN

	-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
	-- Purpose: To search all columns of all tables for a given search string and replace it with another string
	-- Written by: Narayana Vyas Kondreddi
	-- Site: http://vyaskn.tripod.com
	-- Tested on: SQL Server 7.0 and SQL Server 2000
	-- Date modified: 2nd November 2002 13:50 GMT

        SET NOCOUNT ON

        DECLARE @TableName NVARCHAR(256) ,
            @ColumnName NVARCHAR(128) ,
            @SearchStr2 NVARCHAR(110) ,
            @SQL NVARCHAR(4000) ,
            @RCTR INT
        SET @TableName = ''
        SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%', '''')
        SET @RCTR = 0

        WHILE @TableName IS NOT NULL 
            BEGIN
                SET @ColumnName = ''
                SET @TableName = ( SELECT   MIN(QUOTENAME(TABLE_SCHEMA) + '.'
                                                + QUOTENAME(TABLE_NAME))
                                   FROM     INFORMATION_SCHEMA.TABLES
                                   WHERE    TABLE_TYPE = 'BASE TABLE'
                                            AND QUOTENAME(TABLE_SCHEMA) + '.'
                                            + QUOTENAME(TABLE_NAME) > @TableName
                                            AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA)
                                                              + '.'
                                                              + QUOTENAME(TABLE_NAME)),
                                                              'IsMSShipped') = 0
                                 )

                WHILE ( @TableName IS NOT NULL )
                    AND ( @ColumnName IS NOT NULL ) 
                    BEGIN
                        SET @ColumnName = ( SELECT  MIN(QUOTENAME(COLUMN_NAME))
                                            FROM    INFORMATION_SCHEMA.COLUMNS
                                            WHERE   TABLE_SCHEMA = PARSENAME(@TableName,
                                                              2)
                                                    AND TABLE_NAME = PARSENAME(@TableName,
                                                              1)
                                                    AND DATA_TYPE IN ( 'char',
                                                              'varchar',
                                                              'nchar',
                                                              'nvarchar' )
                                                    AND QUOTENAME(COLUMN_NAME) > @ColumnName
                                          )
	
                        IF @ColumnName IS NOT NULL 
                            BEGIN
                                SET @SQL = 'UPDATE ' + @TableName + ' SET '
                                    + @ColumnName + ' =  REPLACE('
                                    + @ColumnName + ', '
                                    + QUOTENAME(@SearchStr, '''') + ', '
                                    + QUOTENAME(@ReplaceStr, '''')
                                    + ') WHERE ' + @ColumnName + ' LIKE '
                                    + @SearchStr2
                                EXEC ( @SQL )
                                SET @RCTR = @RCTR + @@ROWCOUNT
                            END
                    END	
            END

        SELECT  'Replaced ' + CAST(@RCTR AS VARCHAR) + ' occurence(s)' AS 'Outcome'
    END
GO
/****** Object:  StoredProcedure [dbo].[_SearchAllTables]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[_SearchAllTables]
    (
      @SearchStr NVARCHAR(100)
    )
AS 
    BEGIN


--To search all columns of all tables in Pubs database for the keyword "Computer"
--EXEC SearchAllTables 'Computer'
--GO
--
--Here is the complete stored procedure code:


	-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
	-- Purpose: To search all columns of all tables for a given search string
	-- Written by: Narayana Vyas Kondreddi
	-- Site: http://vyaskn.tripod.com
	-- Tested on: SQL Server 7.0 and SQL Server 2000
	-- Date modified: 28th July 2002 22:50 GMT


        CREATE TABLE #Results
            (
              ColumnName NVARCHAR(370) ,
              ColumnValue NVARCHAR(3630)
            )

        SET NOCOUNT ON

        DECLARE @TableName NVARCHAR(256) ,
            @ColumnName NVARCHAR(128) ,
            @SearchStr2 NVARCHAR(110)
        SET @TableName = ''
        SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%', '''')

        WHILE @TableName IS NOT NULL 
            BEGIN
                SET @ColumnName = ''
                SET @TableName = ( SELECT   MIN(QUOTENAME(TABLE_SCHEMA) + '.'
                                                + QUOTENAME(TABLE_NAME))
                                   FROM     INFORMATION_SCHEMA.TABLES
                                   WHERE    TABLE_TYPE = 'BASE TABLE'
                                            AND QUOTENAME(TABLE_SCHEMA) + '.'
                                            + QUOTENAME(TABLE_NAME) > @TableName
                                            AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA)
                                                              + '.'
                                                              + QUOTENAME(TABLE_NAME)),
                                                              'IsMSShipped') = 0
                                 )

                WHILE ( @TableName IS NOT NULL )
                    AND ( @ColumnName IS NOT NULL ) 
                    BEGIN
                        SET @ColumnName = ( SELECT  MIN(QUOTENAME(COLUMN_NAME))
                                            FROM    INFORMATION_SCHEMA.COLUMNS
                                            WHERE   TABLE_SCHEMA = PARSENAME(@TableName,
                                                              2)
                                                    AND TABLE_NAME = PARSENAME(@TableName,
                                                              1)
                                                    AND DATA_TYPE IN ( 'char',
                                                              'varchar',
                                                              'nchar',
                                                              'nvarchar' )
                                                    AND QUOTENAME(COLUMN_NAME) > @ColumnName
                                          )
	
                        IF @ColumnName IS NOT NULL 
                            BEGIN
                                INSERT  INTO #Results
                                        EXEC
                                            ( 'SELECT ''' + @TableName + '.'
                                              + @ColumnName + ''', LEFT('
                                              + @ColumnName + ', 3630) 
					FROM ' + @TableName + ' (NOLOCK) ' + ' WHERE '
                                              + @ColumnName + ' LIKE '
                                              + @SearchStr2
                                            )
                            END
                    END	
            END

        SELECT  ColumnName ,
                ColumnValue
        FROM    #Results
    END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetArchive]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyArticlesGetArchive]
    @ShowAll BIT = 0 ,
    @AuthorId INT = 0 ,
    @SearchQuery VARCHAR(100) = '' ,
    @Debug BIT = 0
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    DECLARE @SQLSTATEMENT NVARCHAR(MAX)
    SET @SQLSTATEMENT = '
    SELECT  DailyArticles.ArticleId,
                DailyArticles.DisplayOrder,
                DailyArticles.title,
                da.authorLast,
                da.AuthorId,
                DailyArticles.CoAuthorId,
                DailyArticles.ShowArticle,
                DATEPART(month, DailyArticles.dateposted) AS MonthNum,
                DATENAME(month, DailyArticles.dateposted) AS Month,
                DailyArticles.DatePosted,
                DailyArticles.ShowArticle,
                da.authorFirst + '' '' + da.authorMiddle + '' '' + da.authorLast AS authorName,
                da2.authorFirst + '' '' + da2.authorMiddle + '' '' + da2.authorLast AS CoAuthorName,
                da.Photo,
                da2.Photo As CoPhoto
        FROM    dbo.DailyArticles
                JOIN dbo.DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId  
                LEFT JOIN dbo.DocumentAuthors da2 ON da2.AuthorId = DailyArticles.CoAuthorId      
        WHERE  1=1' 


    IF @ShowAll != 1 
        SET @SQLSTATEMENT = @SQLSTATEMENT
            + ' AND ( DailyArticles.ShowArticle = 1 ) '

    IF @AuthorId > 0 
        SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND ( DailyArticles.AuthorId = '
            + CAST(@AuthorId AS VARCHAR(7))
            + ' OR DailyArticles.CoAuthorId = '
            + CAST(@AuthorId AS VARCHAR(7)) + ')'
        
    IF LEN(@SearchQuery) > 2 
        SET @SQLSTATEMENT = @SQLSTATEMENT + ' AND 
(
FREETEXT(ArticleText,@SearchQuery)
OR
description LIKE @SearchQuery
OR
title LIKE @SearchQuery
)'

--'AND ((title LIKE ' + CHAR(39) + '%'  + @SearchQuery      + '%' + CHAR(39) + ' )' +
--'OR (DESCRIPTION LIKE ' + CHAR(39) + '%'  + @SearchQuery      + '%' + CHAR(39) + ' )' +
--'OR (ArticleText LIKE ' + CHAR(39) + '%'  + @SearchQuery      + '%' + CHAR(39) + ' ))'

    DECLARE @PARAMS NVARCHAR(MAX)
    SET @PARAMS = N'@SearchQuery VARCHAR(100) '
      
    SET @SQLSTATEMENT = @SQLSTATEMENT + ' ORDER BY 

DailyArticles.DatePosted DESC,
CASE WHEN DailyArticles.Featured = 1 THEN Featured END,    
CASE WHEN DailyArticles.Headline = 1 THEN Headline END

'        

    IF @debug = 0 
--        EXEC ( @SQLSTATEMENT )
        EXEC sp_executesql @SQLSTATEMENT, @PARAMS, @SearchQuery

    ELSE 
        SELECT  @sqlstatement

--SELECT da.AuthorId, da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'authorName'
--FROM dbo.DocumentAuthors da WHERE da.AuthorId = @AuthorId
GO
/****** Object:  Table [dbo].[WardLibraryBackup]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WardLibraryBackup](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[display] [varchar](3) NULL,
	[title] [varchar](255) NOT NULL,
	[checked_out_by] [varchar](255) NULL,
	[authorfirst] [varchar](50) NULL,
	[authorlast] [varchar](50) NULL,
	[authorfirst2] [varchar](50) NULL,
	[authorlast2] [varchar](50) NULL,
	[authorfirst3] [varchar](50) NULL,
	[authorlast3] [varchar](50) NULL,
	[publisher_info] [varchar](500) NULL,
	[comments] [text] NULL,
	[donatedBy] [varchar](100) NULL,
	[donatedDate] [smalldatetime] NULL,
	[coauthors] [varchar](500) NULL,
	[subject] [varchar](255) NULL,
	[CreateDate] [smalldatetime] NULL,
 CONSTRAINT [PK_wardlibraryBackup] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[_Table_Sizes]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_Table_Sizes] @sortbyrows BIT = 0
AS 
    SET NOCOUNT ON

--http://www.sqlteam.com/forums/topic.asp?TOPIC_ID=53843

--EXEC sp_space --show stats sorted by reserved space size
--EXEC sp_space 1 --show stats sorted by row count

    SELECT  CAST(OBJECT_NAME(id) AS VARCHAR(50)) AS name ,
            SUM(CASE WHEN indid < 2 THEN rows
                END) AS rows ,
            SUM(reserved) * 8 AS reserved ,
            SUM(dpages) * 8 AS data ,
            SUM(used - dpages) * 8 AS index_size ,
            SUM(reserved - used) * 8 AS unused
    FROM    sysindexes WITH ( NOLOCK )
    WHERE   indid IN ( 0, 1, 255 )
            AND id > 100
    GROUP BY id
            WITH ROLLUP
    ORDER BY CASE WHEN @sortbyrows = 1 THEN SUM(CASE WHEN indid < 2 THEN rows
                                                END)
                  ELSE SUM(reserved) * 8
             END DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetLinks]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DocumentGetLinks]
    @SubjectId INT = 0 ,
    @AuthorId INT = 0 ,
    @MediaType INT = 0 ,
    @Source VARCHAR(500) = NULL ,
    @SearchQuery VARCHAR(500) = NULL ,
    @MediaCategory VARCHAR(10) = '' ,
    @debug BIT = 0
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF
    DECLARE @SQLSTATEMENT NVARCHAR(MAX)
    SET @SQLSTATEMENT = ' 

SELECT  Documents.DocumentId                     ,        

Documents.GUID                           ,        

Title                                    ,        

PubInfo As Description                   ,        

Source                                   ,        

FullText                                 ,        

DocumentAuthors.AuthorLast                               ,        

DocumentAuthors.AuthorFirst + '' '' + DocumentAuthors.AuthorMiddle + '' '' + DocumentAuthors.AuthorLast As Author,
DocumentAuthors2.AuthorFirst + '' '' + DocumentAuthors2.AuthorMiddle + '' '' + DocumentAuthors2.AuthorLast As CoAuthor,                                            

Author1                                  ,        

Author2                                  ,        

--MediaType                                ,        

media.MediaTypeId                        ,        

MediaIconPath =        

CASE MediaIconPath + ''''                

WHEN ''''                

THEN ''/images/Icons/html.png''                

ELSE MediaIconPath        

END                                       ,        

Documents.CreateDate                       As DatePosted,        

--CAST(Documents.CreateDate As varchar(MAX)) As CreateDate        ,        

media.URL

FROM    Documents

LEFT JOIN MediaAlternateFormat media

ON      media.DocumentId = Documents.DocumentId

LEFT JOIN DocumentMediaType

ON      DocumentMediaType.MediaTypeId = media.MediaTypeId

LEFT JOIN DocumentAuthors

ON      DocumentAuthors.AuthorId = Documents.Author1

LEFT JOIN DocumentAuthors DocumentAuthors2

ON      DocumentAuthors2.AuthorId = Documents.Author2

LEFT JOIN DocumentSubjectLink

ON      DocumentSubjectLink.DocumentId = Documents.DocumentId

WHERE   Display                        = 1    

AND media.MediaTypeId IN (3,4,5,6,7,9)  -- not in 1,2,8 

'
    IF @SubjectId > 0
--SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND Documents.DocumentId IN (select DocumentId from DocumentSubjectLink WHERE SubjectId = '
        SET @SQLSTATEMENT = @SQLSTATEMENT
            + 'AND EXISTS (select * from DocumentSubjectLink WHERE DocumentSubjectLink.DocumentId = documents.DocumentId AND  SubjectId = '
            + CAST(@SubjectId AS VARCHAR(7)) + ')'
    ELSE 
        IF @AuthorId > 0 
            SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND (Author1 = '
                + CAST(@AuthorId AS VARCHAR(7)) + ' OR Author2 = '
                + CAST(@AuthorId AS VARCHAR(7)) + ')'
        ELSE 
            IF @MediaType > 0 
                SET @SQLSTATEMENT = @SQLSTATEMENT
                    + 'AND DocumentMediaType.MediaTypeId  = '
                    + CAST(@MediaType AS VARCHAR(7))
            ELSE 
                IF LEN(@SOURCE) > 0 
                    SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND Source = ('''
                        + CAST(@Source AS VARCHAR(MAX)) + ''')'
                ELSE 
                    IF LEN(@SearchQuery) > 0 
                        SET @SQLSTATEMENT = @SQLSTATEMENT
                            + 'AND (Title LIKE  ' + CHAR(39) + '%'
                            + @SearchQuery + '%' + CHAR(39) + ' 

OR DocumentAuthors.AuthorFirst LIKE ' + CHAR(39) + '%' + @SearchQuery + '%' + CHAR(39) + ' 

OR DocumentAuthors.AuthorLast LIKE ' + CHAR(39) + '%' + @SearchQuery + '%' + CHAR(39) + ' 



OR metaDescription LIKE ' + CHAR(39) + '%' + @SearchQuery + '%' + CHAR(39)
                            + ' 

OR metaKeywords LIKE ' + CHAR(39) + '%' + @SearchQuery + '%' + CHAR(39) + ' 



OR PubInfo LIKE ' + CHAR(39) + '%' + @SearchQuery + '%' + CHAR(39) + ')'
    IF @MediaCategory = 'Media' 
        SET @SQLSTATEMENT = @SQLSTATEMENT
            + ' AND DocumentMediaType.MediaTypeId IN (1,2,8) '
    ELSE 
        IF @MediaCategory = 'Text' 
            SET @SQLSTATEMENT = @SQLSTATEMENT
                + ' AND DocumentMediaType.MediaTypeId IN (3,4,5,6,7,9,10) '  
-- *********************************************************************************************************
-- Get Journal Entries
    SET @SQLSTATEMENT = @SQLSTATEMENT + ' UNION



SELECT  0 As DocumentId                                           ,        

GUID                                                      ,        

Title                                                     ,        

Volume          AS Description,        

Journal                                     AS Source     ,        

0                                           As FullText   ,        

AuthorLast                                                ,        

AuthorName AS Author                                      ,        
'''' As CoAuthor											  ,

AuthorId          AS Author1                                     ,        

0          AS Author2                                     ,        

--MediaType                                                 ,        

DocumentMediaType.MediaTypeId                             ,        

MediaIconPath =        

CASE MediaIconPath + ''''                

WHEN ''''                

THEN ''/images/Icons/html.png''                

ELSE MediaIconPath        

END        ,                

DatePosted,        

URL

FROM    [PeriodicalsView]

INNER JOIN DocumentMediaType

ON      DocumentMediaType.MediaTypeId = PeriodicalsView.MediaTypeId

WHERE   1                             =1 

'
    IF @SubjectId > 0 
        SET @SQLSTATEMENT = @SQLSTATEMENT
            + 'AND EXISTS (select * from DocumentSubjectLink WHERE 
PeriodicalsView.GUID = DocumentSubjectLink.GUID AND 
SubjectId = ' + CAST(@SubjectId AS VARCHAR(7)) + ')'
    ELSE 
        IF @AuthorId > 0 
            SET @SQLSTATEMENT = @SQLSTATEMENT
                + 'AND (PeriodicalsView.AuthorId = '
                + CAST(@AuthorId AS VARCHAR(7)) + ')'
        ELSE 
            IF @MediaType > 0 
                SET @SQLSTATEMENT = @SQLSTATEMENT
                    + 'AND DocumentMediaType.MediaTypeId  = '
                    + CAST(@MediaType AS VARCHAR(7))
            ELSE 
                IF LEN(@SOURCE) > 0 
                    SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND Journal = ('''
                        + CAST(@Source AS VARCHAR(MAX)) + ''')'
                ELSE 
                    IF LEN(@SearchQuery) > 0 
                        SET @SQLSTATEMENT = @SQLSTATEMENT
                            + 'AND (Title LIKE  ' + CHAR(39) + '%'
                            + @SearchQuery + '%' + CHAR(39) + ' 

OR AuthorName LIKE ' + CHAR(39) + '%' + @SearchQuery + '%' + CHAR(39) + ')'
-- ***************** ORDERING
    SET @SQLSTATEMENT = @SQLSTATEMENT + ' ORDER BY  '

    IF @SubjectId > 0
        OR @AuthorId > 0
        OR LEN(@Source) > 0 
        SET @SQLSTATEMENT = @SQLSTATEMENT + ' Documents.Title, '

    IF @MediaCategory = 'title' 
        SET @SQLSTATEMENT = @SQLSTATEMENT + ' Documents.Title, '
    SET @SQLSTATEMENT = @SQLSTATEMENT + ' DatePosted DESC '
-- *******************************************************************
-- Get Other Tables
    IF @SubjectId > 0 
        BEGIN
            SET @SQLSTATEMENT = @SQLSTATEMENT
                + ';SELECT Subject,Photo FROM DocumentSubjects WHERE SubjectId = '
                + CAST(@SubjectId AS VARCHAR(7))
        END
    IF @AuthorId > 0 
        SET @SQLSTATEMENT = @SQLSTATEMENT
            + ';SELECT AuthorFirst + ''  '' + AuthorLast As Author, Photo FROM DocumentAuthors WHERE DocumentAuthors.AuthorId = '
            + CAST(@AuthorId AS VARCHAR(7))
--SELECT @SQLSTATEMENT
    IF @debug = 0 
        EXEC sp_executesql @SQLSTATEMENT
    ELSE 
        SELECT  @sqlstatement
GO
/****** Object:  Table [dbo].[Favorites]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favorites](
	[FavoriteId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](100) NULL,
	[URL] [varchar](500) NULL,
	[Title] [varchar](500) NULL,
	[IPaddress] [varchar](50) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Favorites] PRIMARY KEY CLUSTERED 
(
	[FavoriteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[spUpdateHomepageItemOrder]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateHomepageItemOrder]
    (
      @Itemid INT ,
      @DisplayOrder INT
    )
AS 
    UPDATE  HomepageItems
    SET     Itemorder = @DisplayOrder
    WHERE   ItemId = @ItemId
GO
/****** Object:  StoredProcedure [dbo].[zzzspGetHomepageItemDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzzspGetHomepageItemDetails] @Itemid INT
AS 
    SELECT  Itemid ,
            Display ,
            Title ,
            Author1 ,
            Author2 ,
            PubInfo ,
            URL ,
            MediaTypeId ,
            FullText ,
            Source ,
            GuideContent ,
            CreateDate ,
            CategoryId ,
            Length ,
            ItemOrder
    FROM    HomepageItems
    WHERE   Itemid = @Itemid

--SELECT DocumentSubjectLink.SubjectId FROM DocumentSubjectLink 
--JOIN DocumentsSubject ON DocumentsSubject.SubjectId = DocumentSubjectLink.SubjectId
--WHERE Itemid = @Itemid
GO
/****** Object:  Table [dbo].[Periodicals]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Periodicals](
	[PeriodicalId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Logo] [varchar](255) NOT NULL,
	[ShortDescription] [varchar](255) NULL,
	[RedirectURL] [varchar](255) NULL,
	[PublicationFrequency] [varchar](50) NULL,
	[metaDescription] [varchar](255) NULL,
	[metaKeywords] [varchar](255) NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Periodicals] PRIMARY KEY CLUSTERED 
(
	[PeriodicalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RedirectedURL]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RedirectedURL](
	[RedirectId] [int] IDENTITY(1,1) NOT NULL,
	[RequestedURL] [varchar](150) NOT NULL,
	[RedirectURL] [varchar](500) NOT NULL,
	[Priority] [int] NOT NULL,
	[ExactMatchOnly] [bit] NOT NULL,
 CONSTRAINT [PK_RedirectedURL] PRIMARY KEY CLUSTERED 
(
	[RedirectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [unique_RequestedURL] UNIQUE NONCLUSTERED 
(
	[RequestedURL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNet_SqlCacheTablesForChangeNotification]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification](
	[tableName] [nvarchar](450) NOT NULL,
	[notificationCreated] [datetime] NOT NULL,
	[changeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[tableName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[_MediaCategoryUpdateImages]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_MediaCategoryUpdateImages]
AS 
    BEGIN
        UPDATE  dbo.MediaCategory
        SET     CategoryImage = 'http://mises.org/Controls/Media/DocumentImage.ashx?Id='
                + CAST(dbo.Documents.DocumentId AS VARCHAR(MAX))
        FROM    dbo.MediaCategory
                JOIN dbo.Documents ON dbo.Documents.CategoryId = dbo.MediaCategory.CategoryId
        WHERE   CategoryImage IS NULL
                AND metaImage IS NOT NULL
    END
GO
/****** Object:  Table [dbo].[DailyArticles]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyArticles](
	[ArticleId] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[AuthorId] [int] NOT NULL,
	[CoAuthorId] [int] NULL,
	[Description] [varchar](1500) NULL,
	[DisplayOrder] [tinyint] NULL,
	[ArticleText] [varchar](max) NOT NULL,
	[OLDArticleType] [varchar](2) NULL,
	[PhotoURL] [varchar](255) NULL,
	[PhotoHeight] [int] NULL,
	[Featured] [bit] NOT NULL,
	[Headline] [bit] NOT NULL,
	[ShowArticle] [bit] NOT NULL,
	[DatePosted] [datetime] NOT NULL,
	[OLDauthorFirst] [varchar](75) NULL,
	[OLDauthorLast] [varchar](75) NULL,
	[EditBy] [varchar](75) NULL,
	[EditDate] [smalldatetime] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_dailyarticles] PRIMARY KEY CLUSTERED 
(
	[ArticleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[MediaCategoryGetParents]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavjdV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaCategoryGetParents]
AS 
    BEGIN
	
        SET NOCOUNT ON ;

        SELECT  CategoryId ,
                Category ,
                Description ,
                CreateDate
        FROM    dbo.MediaCategory
        WHERE   CategoryId IN ( SELECT  ParentCategory
                                FROM    dbo.MediaCategory )
                OR ParentCategory = 0
                ORDER BY Category
    END
GO
/****** Object:  Table [dbo].[MediaCategory]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MediaCategory](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[ParentCategory] [int] NOT NULL,
	[Category] [varchar](255) NOT NULL,
	[Description] [varchar](8000) NULL,
	[CategoryImage] [varchar](100) NULL,
	[iTunesCategoryCode] [varchar](50) NULL,
	[CreateDate] [smalldatetime] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_MediaCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MediaAlternateFormat]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MediaAlternateFormat](
	[MediaId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentId] [int] NOT NULL,
	[MediaTypeId] [int] NOT NULL,
	[URL] [varchar](500) NOT NULL,
	[fileSize] [bigint] NOT NULL,
	[duration] [decimal](18, 0) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AlternateMediaFormat] PRIMARY KEY CLUSTERED 
(
	[MediaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentAuthors]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentAuthors](
	[AuthorId] [int] IDENTITY(1,1) NOT NULL,
	[AuthorFirst] [varchar](100) NULL,
	[AuthorMiddle] [varchar](50) NULL,
	[AuthorLast] [varchar](100) NOT NULL,
	[Photo] [varchar](250) NULL,
	[Born] [varchar](12) NULL,
	[Died] [varchar](12) NULL,
	[BioText] [varchar](max) NULL,
	[BioGUID] [uniqueidentifier] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_StudyGuideAuthors] PRIMARY KEY CLUSTERED 
(
	[AuthorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuizType]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuizType](
	[QuizID] [int] NOT NULL,
	[QuizType] [varchar](50) NOT NULL,
	[Language] [varchar](50) NOT NULL,
	[NumOfAnswers] [int] NULL,
	[QuizTitle] [varchar](255) NOT NULL,
	[QuizDescription] [varchar](max) NOT NULL,
	[QuizPostInfo] [varchar](max) NULL,
	[Display] [bit] NOT NULL,
	[RecipientEmail] [varchar](255) NULL,
	[NumTaken] [int] NOT NULL,
	[CorrectAnswerText] [varchar](max) NOT NULL,
	[LabelSubmitQuiz] [varchar](max) NULL,
	[LabelError] [varchar](max) NOT NULL,
	[LabelEnterEmail] [varchar](max) NOT NULL,
	[LabelYourAnswer] [varchar](max) NOT NULL,
	[LabelBestAnswer] [varchar](max) NOT NULL,
	[LabelYourScore] [varchar](max) NOT NULL,
	[LabelThankYou] [varchar](max) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_quizType] PRIMARY KEY CLUSTERED 
(
	[QuizID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aenDB]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aenDB](
	[control] [int] NOT NULL,
	[display] [varchar](3) NULL,
	[volume] [int] NULL,
	[number] [varchar](2) NULL,
	[articleNum] [varchar](2) NULL,
	[title] [varchar](255) NULL,
	[authorFirst1] [varchar](50) NULL,
	[authorLast1] [varchar](50) NULL,
	[authorFirst2] [varchar](50) NULL,
	[authorLast2] [varchar](50) NULL,
	[link] [varchar](50) NULL,
	[fileType] [varchar](4) NULL,
 CONSTRAINT [PK_aenDB] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Calendar]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calendar](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[Title] [varchar](200) NOT NULL,
	[EventDate] [varchar](150) NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[IntroText] [varchar](4000) NULL,
	[EventImage] [varchar](250) NULL,
	[Description] [varchar](max) NOT NULL,
	[Location] [varchar](150) NOT NULL,
	[Display] [bit] NOT NULL,
	[CreateDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_calendar2] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Biography]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Biography](
	[AuthorId] [int] IDENTITY(1,1) NOT NULL,
	[AuthorFirst] [varchar](100) NULL,
	[AuthorMiddle] [varchar](50) NULL,
	[AuthorLast] [varchar](100) NOT NULL,
	[Photo] [varchar](250) NULL,
	[Born] [varchar](12) NULL,
	[Died] [varchar](12) NULL,
	[BioText] [varchar](max) NULL,
	[BioGUID] [uniqueidentifier] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_Biography] PRIMARY KEY CLUSTERED 
(
	[AuthorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegistrationQuestions]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrationQuestions](
	[QuestionId] [int] IDENTITY(1,1) NOT NULL,
	[FormId] [int] NOT NULL,
	[Question] [varchar](500) NOT NULL,
	[QuestionType] [tinyint] NOT NULL,
	[QuestionOrder] [int] NULL,
	[Required] [bit] NULL,
 CONSTRAINT [PK_RegistrationQuestions] PRIMARY KEY CLUSTERED 
(
	[QuestionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[qjaeDB]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[qjaeDB](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[display] [varchar](3) NULL,
	[volume] [int] NULL,
	[number] [int] NULL,
	[articleNum] [int] NULL,
	[title] [varchar](255) NULL,
	[authorFirst1] [varchar](50) NULL,
	[authorLast1] [varchar](50) NULL,
	[authorFirst2] [varchar](50) NULL,
	[authorLast2] [varchar](50) NULL,
	[link] [varchar](50) NULL,
 CONSTRAINT [PK_qjaeDB] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[content]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[content](
	[ContentID] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Body] [nvarchar](50) NULL,
	[LastModified] [nvarchar](50) NULL,
	[SortOrder] [nvarchar](50) NULL,
	[Hidden] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageContent]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageContent](
	[ContentID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](256) NOT NULL,
	[Title] [nvarchar](256) NOT NULL,
	[Body] [varchar](max) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[SortOrder] [int] NULL,
	[Hidden] [bit] NOT NULL,
 CONSTRAINT [PK_cs_Content] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[raeDB]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[raeDB](
	[control] [int] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[display] [varchar](3) NULL,
	[volume] [int] NULL,
	[number] [varchar](2) NULL,
	[articleNum] [varchar](2) NULL,
	[title] [varchar](255) NULL,
	[authorFirst1] [varchar](50) NULL,
	[authorLast1] [varchar](50) NULL,
	[authorFirst2] [varchar](50) NULL,
	[authorLast2] [varchar](50) NULL,
	[link] [varchar](50) NULL,
 CONSTRAINT [PK_raeDB] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quotes]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quotes](
	[QuoteId] [int] IDENTITY(1,1) NOT NULL,
	[AuthorId] [int] NULL,
	[Author] [varchar](250) NULL,
	[Quote] [varchar](8000) NULL,
	[Source] [varchar](250) NULL,
	[Page] [varchar](250) NULL,
	[Subject] [varchar](250) NULL,
 CONSTRAINT [PK_Quotes] PRIMARY KEY CLUSTERED 
(
	[QuoteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pages]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pages](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[Visible] [bit] NOT NULL,
	[display] [varchar](50) NULL,
	[pageName] [varchar](255) NULL,
	[pageTitle] [varchar](255) NULL,
	[folder] [varchar](50) NULL,
	[content] [varchar](max) NULL,
	[backupdata] [varchar](max) NULL,
	[metaKeywords] [varchar](max) NULL,
	[metaDescription] [varchar](max) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_pages] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[jls]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[jls](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NULL,
	[display] [varchar](3) NULL,
	[volume] [int] NULL,
	[number] [int] NULL,
	[articleNum] [int] NULL,
	[title] [varchar](255) NULL,
	[authorFirst1] [varchar](50) NULL,
	[authorLast1] [varchar](50) NULL,
	[authorFirst2] [varchar](50) NULL,
	[authorLast2] [varchar](50) NULL,
	[link] [varchar](50) NULL,
 CONSTRAINT [PK_jls] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[Tag] [varchar](75) NOT NULL,
	[TagAuthority] [uniqueidentifier] NULL,
	[LockAuthority] [bit] NOT NULL,
	[EditDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Tag] UNIQUE NONCLUSTERED 
(
	[Tag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[MediaCategoryGetAll]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavjdV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaCategoryGetAll]
AS 
    BEGIN
	
        SET NOCOUNT ON ;

        SELECT  CategoryId ,
                Category ,
                Description ,
                CreateDate
        FROM    MediaCategory
        ORDER BY SortOrder,CreateDate DESC ,
                Category
    END
GO
/****** Object:  Table [dbo].[scholar]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[scholar](
	[control] [int] IDENTITY(1,1) NOT NULL,
	[author_name] [varchar](100) NULL,
	[paper_name] [varchar](255) NULL,
	[whenit] [varchar](75) NULL,
	[CreateDate] [datetime] NOT NULL,
	[file_name] [varchar](50) NULL,
	[display] [varchar](5) NULL,
	[notation] [varchar](100) NULL,
	[listorder] [varchar](20) NULL,
 CONSTRAINT [PK_scholar] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentSubjects]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentSubjects](
	[SubjectId] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [varchar](255) NOT NULL,
	[ShortSubject] [varchar](255) NULL,
	[Photo] [varchar](255) NULL,
	[CategoryId] [int] NULL,
	[SortOrder] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_StudyGuideSubj] PRIMARY KEY CLUSTERED 
(
	[SubjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Documents](
	[DocumentId] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[Display] [bit] NOT NULL,
	[Featured] [bit] NULL,
	[Title] [varchar](255) NOT NULL,
	[Author1] [int] NOT NULL,
	[Author2] [int] NULL,
	[PubInfo] [varchar](8000) NULL,
	[metaDescription] [varchar](max) NULL,
	[metaKeywords] [varchar](350) NULL,
	[metaImage] [image] NULL,
	[oldURL] [varchar](255) NULL,
	[oldMediaTypeId] [int] NULL,
	[FullText] [bit] NULL,
	[Source] [varchar](100) NULL,
	[GuideContent] [varchar](max) NULL,
	[Length] [int] NULL,
	[CategoryId] [int] NULL,
	[CreateDate] [datetime] NOT NULL,
	[EditDate] [datetime] NOT NULL,
 CONSTRAINT [PK_StudyGuide] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentSubjectLink]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentSubjectLink](
	[SubjectID] [int] NOT NULL,
	[DocumentId] [int] NOT NULL,
	[GUID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_StudyGuideSubjLink] PRIMARY KEY CLUSTERED 
(
	[SubjectID] ASC,
	[DocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TagMap]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TagMap](
	[TagId] [int] NOT NULL,
	[ObjectId] [uniqueidentifier] NOT NULL,
	[TaggedBy] [varchar](75) NOT NULL,
	[TaggedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TagMap_1] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[ObjectId] ASC,
	[TaggedBy] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[RegistrationFormDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date:  12/2005
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RegistrationFormDetails] @FormId INT
AS 
    BEGIN
        SET NOCOUNT ON

        SELECT  *
        FROM    RegistrationForms
        WHERE   FormId = @FormId
        SELECT  *
        FROM    RegistrationProducts
        WHERE   FormId = @FormId
        ORDER BY ProductOrder
        SELECT  *
        FROM    RegistrationQuestions
        WHERE   FormId = @FormId
        ORDER BY QuestionOrder

    END
GO
/****** Object:  StoredProcedure [dbo].[QuizGetDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[QuizGetDetails] @QuizId INT
AS 
    BEGIN	
        SET NOCOUNT ON ;
        SELECT  *
        FROM    dbo.QuizType
        WHERE   QuizID = @QuizId
        SELECT  *
        FROM    dbo.QuizQuestion
        WHERE   QuizID = @QuizId
        ORDER BY SortOrder 
        SELECT  dbo.QuizAnswer.*
        FROM    dbo.QuizAnswer
                JOIN dbo.QuizQuestion ON dbo.QuizAnswer.QuestionID = dbo.QuizQuestion.QuestionID
        WHERE   dbo.QuizQuestion.QuizID = @QuizId
    END
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalsGetArchives]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PeriodicalsGetArchives]
    @PeriodicalId INT ,
    @title VARCHAR(50) = 'RandomString' ,
    @author VARCHAR(50) = 'RandomString' ,
    @subject VARCHAR(50) = 'RandomString' ,
    @volume VARCHAR(50) = 'RandomString'
AS 
    BEGIN
	
        IF @title = 'RandomString'
            AND @author = 'RandomString'
            AND @subject = 'RandomString'
            AND @volume = 'RandomString' 
            BEGIN

                IF @PeriodicalId = 1 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            freemarket.title ,
                            freemarket.AuthorFirst + ' '
                            + freemarket.AuthorLast AS AuthorName ,
                            freemarket.AuthorLast ,
                            freemarket.articledate AS DatePosted ,
                            DATENAME(month, freemarket.articledate) AS Month ,
                            '/freemarket_detail.aspx?control='
                            + CAST(freemarket.control AS VARCHAR(10)) AS URL ,
                            freemarket.[control] AS Id
                    FROM    dbo.freemarket
                    ORDER BY freemarket.articledate DESC


-- Season, Volume
                IF @PeriodicalId = 2 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            misesreview.AuthorFirst + ' '
                            + misesreview.AuthorLast AS AuthorName ,
                            misesreview.AuthorLast ,
                            misesreview.title ,
                            CASE misesreview.issue_season
                              WHEN 1 THEN 'Spring'
                              WHEN 2 THEN 'Summer'
                              WHEN 3 THEN 'Fall'
                              WHEN 4 THEN 'Winter'
                            END + ' '
                            + CAST(misesreview.issue_year AS VARCHAR(10)) AS Volume ,
                            misesreview.issue_year AS Season ,
                            '/misesreview_detail.aspx?control='
                            + CAST(misesreview.control AS VARCHAR(10)) AS URL ,
                            misesreview.[control] AS Id
                    FROM    dbo.misesreview
                    ORDER BY misesreview.CreateDate DESC ,
                            PeriodicalId DESC

                IF @PeriodicalId = 3 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            jls.number ,
                            jls.display ,
                            jls.title ,
                            jls.authorFirst1 + ' ' + jls.authorLast1 AS AuthorName ,
                            jls.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(jls.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(jls.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(jls.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/jls/' + CAST(jls.Volume AS VARCHAR(10))
                            + '_' + CAST(jls.number AS VARCHAR(10)) + '/'
                            + CAST(jls.Volume AS VARCHAR(10)) + '_'
                            + CAST(jls.number AS VARCHAR(10)) + '_'
                            + CAST(jls.articleNum AS VARCHAR(10)) + '.pdf' AS URL
                    FROM    dbo.jls
                    ORDER BY jls.Volume DESC ,
                            number DESC ,
                            jls.ArticleNum

                IF @PeriodicalId = 4 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            qjaeDB.number ,
                            qjaeDB.display ,
                            qjaeDB.title ,
                            qjaeDB.authorFirst1 + ' ' + qjaeDB.authorLast1 AS AuthorName ,
                            qjaeDB.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(qjaeDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(qjaeDB.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(qjaeDB.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/qjae/pdf/qjae'
                            + CAST(qjaeDB.Volume AS VARCHAR(10)) + '_'
                            + CAST(qjaeDB.number AS VARCHAR(10)) + '_'
                            + CAST(qjaeDB.articleNum AS VARCHAR(10)) + '.pdf' AS URL
                    FROM    dbo.qjaeDB
                    ORDER BY [qjaeDB].Volume DESC ,
                            [qjaeDB].number DESC ,
                            ArticleNum

                IF @PeriodicalId = 5 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            raeDB.number ,
                            raeDB.display ,
                            raeDB.title ,
                            raeDB.authorFirst1 + ' ' + raeDB.authorLast1 AS AuthorName ,
                            raeDB.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(raeDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(raeDB.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(raeDB.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/rae/pdf/RAE'
                            + CAST(raeDB.Volume AS VARCHAR(10)) + '_'
                            + CAST(raeDB.number AS VARCHAR(10)) + '_'
                            + CAST(raeDB.articleNum AS VARCHAR(10)) + '.pdf' AS URL
                    FROM    dbo.raeDB
                    ORDER BY Volume DESC ,
                            number DESC ,
                            ArticleNum

                IF @PeriodicalId = 6 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            aenDB.number ,
                            aenDB.display ,
                            aenDB.title ,
                            aenDB.authorFirst1 + ' ' + aenDB.authorLast1 AS AuthorName ,
                            aenDB.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(aenDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(aenDB.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(aenDB.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/aen/aen'
                            + CAST(aenDB.Volume AS VARCHAR(10)) + '_'
                            + CAST(aenDB.number AS VARCHAR(10)) + '_'
                            + CAST(aenDB.articleNum AS VARCHAR(10))
                            + aenDB.[fileType] AS URL
                    FROM    dbo.aenDB
                    ORDER BY aenDB.Volume DESC ,
                            number DESC ,
                            ArticleNum

                IF @PeriodicalId = 7 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            [paper_name] + '(' + notation + ')' AS 'Title' ,
                            [author_name] AS AuthorName ,
                            [author_name] AS AuthorLast ,
                            [whenit] AS 'DatePosted' ,
                            '' AS 'Month' ,
                            '/journals/scholar/' + [file_name] AS [url]
                    FROM    scholar
                    ORDER BY control DESC
                    
                IF @PeriodicalId = 8 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            title AS 'Title' ,
                            AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName ,
                            [AuthorLast] AS AuthorLast ,
                            donatedDate AS 'DatePosted' ,
                            '' AS 'Season' ,
                            '' AS 'Volume' ,
                            'book.aspx?Id=' + CAST(CONTROL AS VARCHAR(10)) AS [url]
                    FROM    WardLibrary
                    WHERE   display = 'yes'
                    ORDER BY control DESC
                    
                   	


            END
        ELSE -- DO SEARCH
            BEGIN
          
                IF @title = '' 
                    SET @title = 'RandomString'
                IF @author = '' 
                    SET @author = 'RandomString'
                IF @subject = '' 
                    SET @subject = 'RandomString'
                IF @volume = '' 
                    SET @volume = 'RandomString'
            

                IF @PeriodicalId = 1 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            freemarket.title ,
                            freemarket.AuthorFirst + ' '
                            + freemarket.AuthorLast AS AuthorName ,
                            freemarket.AuthorLast ,
                            freemarket.articledate AS DatePosted ,
                            DATENAME(month, freemarket.articledate) AS Month ,
                            '/freemarket_detail.aspx?control='
                            + CAST(freemarket.control AS VARCHAR(10)) AS URL
                    FROM    dbo.freemarket
                    WHERE   freemarket.title LIKE '%' + @title + '%'
                            OR freemarket.body LIKE '%' + @title + '%'
                            OR freemarket.body LIKE '%' + @subject + '%'
                            OR freemarket.AuthorLast LIKE '%' + @author + '%'
                    ORDER BY freemarket.articledate DESC


-- Season, Volume
                IF @PeriodicalId = 2 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            misesreview.AuthorFirst + ' '
                            + misesreview.AuthorLast AS AuthorName ,
                            misesreview.AuthorLast ,
                            misesreview.title ,
                            CASE misesreview.issue_season
                              WHEN 1 THEN 'Spring'
                              WHEN 2 THEN 'Summer'
                              WHEN 3 THEN 'Fall'
                              WHEN 4 THEN 'Winter'
                            END + ' '
                            + CAST(misesreview.issue_year AS VARCHAR(10)) AS Volume ,
                            misesreview.issue_year AS Season ,
                            '/misesreview_detail.aspx?control='
                            + CAST(misesreview.control AS VARCHAR(10)) AS URL
                    FROM    dbo.misesreview
                    WHERE   misesreview.title LIKE '%' + @title + '%'
                            OR misesreview.body LIKE '%' + @title + '%'
                            OR misesreview.AuthorLast LIKE '%' + @author + '%'
                            OR misesreview.[authorfirst] LIKE '%' + @author
                            + '%'
                            OR misesreview.[authorfirst] + ' '
                            + +misesreview.[authorlast] LIKE '%' + @author
                            + '%'
                            OR misesreview.body LIKE '%' + @title + '%'
                            OR CASE misesreview.issue_season
                                 WHEN 1 THEN 'Spring'
                                 WHEN 2 THEN 'Summer'
                                 WHEN 3 THEN 'Fall'
                                 WHEN 4 THEN 'Winter'
                               END + ' '
                            + CAST(misesreview.issue_year AS VARCHAR(10)) LIKE '%'
                            + @volume + '%'
                    ORDER BY misesreview.CreateDate DESC ,
                            PeriodicalId DESC

                IF @PeriodicalId = 3 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            jls.number ,
                            jls.display ,
                            jls.title ,
                            jls.authorFirst1 + ' ' + jls.authorLast1 AS AuthorName ,
                            jls.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(jls.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(jls.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(jls.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/jls/' + CAST(jls.Volume AS VARCHAR(10))
                            + '_' + CAST(jls.number AS VARCHAR(10)) + '/'
                            + CAST(jls.Volume AS VARCHAR(10)) + '_'
                            + CAST(jls.number AS VARCHAR(10)) + '_'
                            + CAST(jls.articleNum AS VARCHAR(10)) + '.pdf' AS URL
                    FROM    dbo.jls
                    WHERE   jls.title LIKE '%' + @title + '%'
                            OR jls.AuthorLast1 LIKE '%' + @author + '%'
                            OR jls.AuthorLast2 LIKE '%' + @author + '%'
                            OR jls.authorFirst1 LIKE '%' + @author + '%'
                            OR jls.authorFirst2 LIKE '%' + @author + '%'
                            OR 'Vol. ' + CAST(jls.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(jls.[number] AS VARCHAR(10)) LIKE '%'
                            + @volume + '%'
                    ORDER BY jls.Volume DESC ,
                            number DESC ,
                            jls.ArticleNum

                IF @PeriodicalId = 4 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            qjaeDB.number ,
                            qjaeDB.display ,
                            qjaeDB.title ,
                            qjaeDB.authorFirst1 + ' ' + qjaeDB.authorLast1 AS AuthorName ,
                            qjaeDB.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(qjaeDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(qjaeDB.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(qjaeDB.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/qjae/pdf/qjae'
                            + CAST(qjaeDB.Volume AS VARCHAR(10)) + '_'
                            + CAST(qjaeDB.number AS VARCHAR(10)) + '_'
                            + CAST(qjaeDB.articleNum AS VARCHAR(10)) + '.pdf' AS URL
                    FROM    dbo.qjaeDB
                    WHERE   qjaeDB.title LIKE '%' + @title + '%'
                            OR qjaeDB.AuthorLast1 LIKE '%' + @author + '%'
                            OR qjaeDB.AuthorLast2 LIKE '%' + @author + '%'
                            OR qjaeDB.authorFirst1 LIKE '%' + @author + '%'
                            OR qjaeDB.authorFirst2 LIKE '%' + @author + '%'
                            OR 'Vol. ' + CAST(qjaeDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(qjaeDB.[number] AS VARCHAR(10)) LIKE '%'
                            + @volume + '%'
                    ORDER BY Volume DESC ,
                            number DESC ,
                            ArticleNum

                IF @PeriodicalId = 5 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            raeDB.number ,
                            raeDB.display ,
                            raeDB.title ,
                            raeDB.authorFirst1 + ' ' + raeDB.authorLast1 AS AuthorName ,
                            raeDB.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(raeDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(raeDB.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(raeDB.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/rae/pdf/RAE'
                            + CAST(raeDB.Volume AS VARCHAR(10)) + '_'
                            + CAST(raeDB.number AS VARCHAR(10)) + '_'
                            + CAST(raeDB.articleNum AS VARCHAR(10)) + '.pdf' AS URL
                    FROM    dbo.raeDB
                    WHERE   raeDB.title LIKE '%' + @title + '%'
                            OR raeDB.AuthorLast1 LIKE '%' + @author + '%'
                            OR raeDB.AuthorLast2 LIKE '%' + @author + '%'
                            OR raeDB.authorFirst1 LIKE '%' + @author + '%'
                            OR raeDB.authorFirst2 LIKE '%' + @author + '%'
                            OR 'Vol. ' + CAST(raeDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(raeDB.[number] AS VARCHAR(10)) LIKE '%'
                            + @volume + '%'
                    ORDER BY Volume DESC ,
                            number DESC ,
                            ArticleNum

                IF @PeriodicalId = 6 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            aenDB.number ,
                            aenDB.display ,
                            aenDB.title ,
                            aenDB.authorFirst1 + ' ' + aenDB.authorLast1 AS AuthorName ,
                            aenDB.authorLast1 AS AuthorLast ,
                            'Vol. ' + CAST(aenDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(aenDB.[number] AS VARCHAR(10)) AS Volume ,
                            'Volume ' + CAST(aenDB.Volume AS VARCHAR(10)) AS Season ,
                            '/journals/aen/aen'
                            + CAST(aenDB.Volume AS VARCHAR(10)) + '_'
                            + CAST(aenDB.number AS VARCHAR(10)) + '_'
                            + CAST(aenDB.articleNum AS VARCHAR(10))
                            + aenDB.[fileType] AS URL
                    FROM    dbo.aenDB
                    WHERE   aenDB.title LIKE '%' + @title + '%'
                            OR aenDB.AuthorLast1 LIKE '%' + @author + '%'
                            OR aenDB.AuthorLast2 LIKE '%' + @author + '%'
                            OR aenDB.authorFirst1 LIKE '%' + @author + '%'
                            OR aenDB.authorFirst2 LIKE '%' + @author + '%'
                            OR 'Vol. ' + CAST(aenDB.Volume AS VARCHAR(10))
                            + ' Num. ' + CAST(aenDB.[number] AS VARCHAR(10)) LIKE '%'
                            + @volume + '%'
                    ORDER BY Volume DESC ,
                            number DESC ,
                            ArticleNum

                IF @PeriodicalId = 7 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            [paper_name] + '(' + notation + ')' AS 'Title' ,
                            [author_name] AS AuthorName ,
                            [author_name] AS AuthorLast ,
                            [whenit] AS 'DatePosted' ,
                            '' AS 'Month' ,
                            '/journals/scholar/' + [file_name] AS [url]
                    FROM    scholar
                    WHERE   [paper_name] LIKE '%' + @title + '%'
                            OR [author_name] LIKE '%' + @author + '%'
                            OR [notation] LIKE '%' + @title + '%'
                    ORDER BY control DESC

                IF @PeriodicalId = 8 
                    SELECT  @PeriodicalId AS PeriodicalId ,
                            title AS 'Title' ,
                            AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName ,
                            [AuthorLast] AS AuthorLast ,
                            donatedDate AS 'DatePosted' ,
                            '' AS 'Season' ,
                            '' AS 'Volume' ,
                            'book.aspx?Id=' + CAST(CONTROL AS VARCHAR(10)) AS [url]
                    FROM    WardLibrary
                    WHERE   display = 'yes'
                            AND ( WardLibrary.comments LIKE '%' + @title + '%'
                                  OR WardLibrary.title LIKE '%' + @title + '%'
                                  OR WardLibrary.authorlast LIKE '%' + @author
                                  + '%'
                                  OR WardLibrary.authorfirst2 LIKE '%'
                                  + @author + '%'
                                  OR WardLibrary.authorfirst3 LIKE '%'
                                  + @author + '%'
                                  OR WardLibrary.subject LIKE '%' + @subject
                                  + '%'
                                  OR AuthorFirst + ' ' + AuthorLast LIKE '%'
                                  + @author + '%'
                                )
                    ORDER BY control DESC


            END

    END

--PeriodicalsGetArchives 3

--select * from aen

--[PeriodicalsGetArchives] 6,'Buchanan'
GO
/****** Object:  StoredProcedure [dbo].[SearchSite]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 1/30/2006
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SearchSite] 
	-- Add the parameters for the stored procedure here
    @terms VARCHAR(100)
AS 
    BEGIN

        SET CONCAT_NULL_YIELDS_NULL OFF

        SELECT  'Daily Articles' 'SearchType' ,
                '/daily/{0}' ,
                ArticleId [IDfield] ,
                Title + ' by ' + da.AuthorFirst + ' ' + da.AuthorMiddle + ' '
                + da.AuthorLast AS [Title]
        FROM    dailyarticles WITH ( NOLOCK )
                JOIN DocumentAuthors da ON da.AuthorId = dailyarticles.AuthorId
        WHERE   ShowArticle = 1
                AND ( ( Title LIKE '%' + @terms + '%' )
                      OR ( Description LIKE '%' + @terms + '%' )
                      OR FREETEXT ( ArticleText, @terms )
--                      OR ( ArticleText LIKE '%' + @terms + '%' )
                      
                    )
        ORDER BY title

        SELECT  'Literature Document' 'SearchType' ,
                '/resources/{0}' ,
                DocumentId [IDfield] ,
                Title [Title]
        FROM    Documents WITH ( NOLOCK )
        WHERE   display = 1
                AND ( ( title LIKE '%' + @terms + '%' )
                      OR ( PubInfo LIKE '%' + @terms + '%' )
                      OR FREETEXT ( [Documents].[GuideContent], @terms )
                    )
        ORDER BY title

        SELECT  'Mises Store Products' 'SearchType' ,
                '/store/Product.aspx?ProductId={0}' ,
                ProductId [IDfield] ,
                [Name] [Title]
        FROM    [AbleCommerce]..ac_Products WITH ( NOLOCK )
        WHERE   ( ( name LIKE '%' + @terms + '%' )
                  OR ( description LIKE '%' + @terms + '%' )
                )
        ORDER BY [name]


        SELECT  'Mises Institute Events' 'SearchType' ,
                '/events/{0}' ,
                EventId [IDfield] ,
                Title [Title]
        FROM    calendar WITH ( NOLOCK )
        WHERE   display = 1
                AND ( ( Description LIKE '%' + @terms + '%' )
                      OR ( Title LIKE '%' + @terms + '%' )
                      OR ( Location LIKE '%' + @terms + '%' )
                    )
        ORDER BY Title

        SELECT  'Free Market articles' 'SearchType' ,
                '/freemarket_detail.aspx?control={0}' ,
                control [IDfield] ,
                title [Title]
        FROM    freemarket WITH ( NOLOCK )
        WHERE   ( ( title LIKE '%' + @terms + '%' )
                  OR ( authorfirst LIKE '%' + @terms + '%' )
                  OR ( authorlast LIKE '%' + @terms + '%' )
                  OR ( authorfirst2 LIKE '%' + @terms + '%' )
                  OR ( authorlast2 LIKE '%' + @terms + '%' )
                  OR ( subject1 LIKE '%' + @terms + '%' )
                  OR ( subject2 LIKE '%' + @terms + '%' )
                  OR ( subject3 LIKE '%' + @terms + '%' )
                  OR ( body LIKE '%' + @terms + '%' )
                  OR ( articledate LIKE '%' + @terms + '%' )
                )
        ORDER BY title

        SELECT  'Mises Review articles' 'SearchType' ,
                '/misesreview_detail.aspx?control={0}' ,
                control [IDfield] ,
                title [Title]
        FROM    misesreview WITH ( NOLOCK )
        WHERE   ( ( title LIKE '%' + @terms + '%' )
                  OR ( authorfirst LIKE '%' + @terms + '%' )
                  OR ( authorlast LIKE '%' + @terms + '%' )
                  OR ( authorfirst2 LIKE '%' + @terms + '%' )
                  OR ( authorlast2 LIKE '%' + @terms + '%' )
                  OR ( authorfirst3 LIKE '%' + @terms + '%' )
                  OR ( authorlast3 LIKE '%' + @terms + '%' )
                  OR ( body LIKE '%' + @terms + '%' )
                )
        ORDER BY title


        SELECT  'Journal of Libertarian Studies articles' 'SearchType' ,
                '{0}' ,
                jls.[link] [IDfield] ,
                title [Title]
        FROM    jls WITH ( NOLOCK )
        WHERE   ( title LIKE '%' + @terms + '%' )
                OR ( authorFirst1 LIKE '%' + @terms + '%' )
                OR ( authorLast1 LIKE '%' + @terms + '%' )
                OR ( authorFirst2 LIKE '%' + @terms + '%' )
                OR ( authorLast2 LIKE '%' + @terms + '%' )
        ORDER BY title

        SELECT  'Quarterly Journal of Austrian Economics articles' 'SearchType' ,
                '/journals/qjae/qjae' + CAST(volume AS VARCHAR(MAX)) + '_'
                + CAST(number AS VARCHAR(MAX)) + '_'
                + CAST(ArticleNum AS VARCHAR(MAX)) + '.pdf' ,
                '' [IDfield] ,
                title [Title]
        FROM    qjaeDB WITH ( NOLOCK )
        WHERE   ( title LIKE '%' + @terms + '%' )
                OR ( authorFirst1 LIKE '%' + @terms + '%' )
                OR ( authorLast1 LIKE '%' + @terms + '%' )
                OR ( authorFirst2 LIKE '%' + @terms + '%' )
                OR ( authorLast2 LIKE '%' + @terms + '%' )
        ORDER BY title


        SELECT  'Austrian Economics Newsletter articles' 'SearchType' ,
                '/journals/aen/aen' + CAST(volume AS VARCHAR(MAX)) + '_'
                + CAST(number AS VARCHAR(MAX)) + '_'
                + CAST(ArticleNum AS VARCHAR(MAX)) + fileType ,
                '' [IDfield] ,
                title [Title]
        FROM    aenDB WITH ( NOLOCK )
        WHERE   ( title LIKE '%' + @terms + '%' )
                OR ( authorFirst1 LIKE '%' + @terms + '%' )
                OR ( authorLast1 LIKE '%' + @terms + '%' )
                OR ( authorFirst2 LIKE '%' + @terms + '%' )
                OR ( authorLast2 LIKE '%' + @terms + '%' )
        ORDER BY title

        SELECT  'Review of Austrian Economics articles' 'SearchType' ,
                '/journals/rae/pdf/rae' + CAST(volume AS VARCHAR(MAX)) + '_'
                + CAST(number AS VARCHAR(MAX)) + '_'
                + CAST(ArticleNum AS VARCHAR(MAX)) + '.pdf' ,
                '' [IDfield] ,
                title [Title]
        FROM    raeDB WITH ( NOLOCK )
        WHERE   ( title LIKE '%' + @terms + '%' )
                OR ( authorFirst1 LIKE '%' + @terms + '%' )
                OR ( authorLast1 LIKE '%' + @terms + '%' )
                OR ( authorFirst2 LIKE '%' + @terms + '%' )
                OR ( authorLast2 LIKE '%' + @terms + '%' )
        ORDER BY title

        SELECT  'Other Pages' 'SearchType' ,
                '' ,
                '/' + folder + '/' + pagename [IDfield] ,
                pagetitle [Title]
        FROM    page WITH ( NOLOCK )
        WHERE   ( pageName LIKE '%' + @terms + '%' )
                OR ( pageTitle LIKE '%' + @terms + '%' )
--                OR ( content LIKE '%' + @terms + '%' )
                OR FREETEXT ( [content], @terms )
                OR ( metaKeywords LIKE '%' + @terms + '%' )
                OR ( metaDescription LIKE '%' + @terms + '%' )
        ORDER BY pageName

        SELECT  'Ward Library books ' 'SearchType' ,
                '/book.aspx?Id={0}' ,
                control [IDfield] ,
                Title [Title]
        FROM    wardlibrary WITH ( NOLOCK )
        WHERE   ( ( Title LIKE '%' + @terms + '%' )
                  OR ( publisher_info LIKE '%' + @terms + '%' )
                  OR ( comments LIKE '%' + @terms + '%' )
                )
        ORDER BY title

        SELECT  'Faculty Biographies' 'SearchType' ,
                --'/fellow.aspx?Id={0}',
                web ,
                '' [IDfield] ,
                --control [IDfield],
                [name] [Title]
        FROM    faculty WITH ( NOLOCK )
        WHERE   ( display = 1 )
                AND ( ( name LIKE '%' + @terms + '%' )
                      OR ( school LIKE '%' + @terms + '%' )
                      OR ( email LIKE '%' + @terms + '%' )
                      OR ( jobtitle LIKE '%' + @terms + '%' )
                    )
        ORDER BY name


--        SELECT  'Misc Pages' 'SearchType',
--                '',
--                'http://www.mises.org' + folder + '/' + pagename [IDfield],
--                pagetitle [Title]
--        FROM    pages WITH (NOLOCK)
--        WHERE   ( pageName LIKE '%' + @terms + '%' )
--                OR ( pageTitle LIKE '%' + @terms + '%' )
--                OR ( content LIKE '%' + @terms + '%' )
--                OR ( metaKeywords LIKE '%' + @terms + '%' )
--                OR ( metaDescription LIKE '%' + @terms + '%' )
--        ORDER BY pageName

        

    END
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalUpdateStory]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PeriodicalUpdateStory]
    @PeriodicalId INT ,
    @StoryId INT ,
    @EditedBy VARCHAR(50) ,
    @authorfirst VARCHAR(50) ,
    @authorlast VARCHAR(50) ,
    @authorfirst2 VARCHAR(50) ,
    @authorlast2 VARCHAR(50) ,
    @authorfirst3 VARCHAR(50) ,
    @authorlast3 VARCHAR(50) ,
    @title VARCHAR(255) ,
    @body VARCHAR(MAX) ,
    @issue_season INT ,
    @issue_year INT
AS 
    BEGIN


        UPDATE  misesreview
        SET     authorfirst = @authorfirst ,
                authorlast = @authorlast ,
                authorfirst2 = @authorfirst2 ,
                authorlast2 = @authorlast2 ,
                authorfirst3 = @authorfirst3 ,
                authorlast3 = @authorlast3 ,
                title = @title ,
                body = @body ,
                issue_season = @issue_season ,
                issue_year = @issue_year
        WHERE   dbo.misesreview.[control] = @StoryId                      



    END
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalAddStory]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PeriodicalAddStory]
    @PeriodicalId INT ,
    @EditedBy VARCHAR(50) ,
    @authorfirst VARCHAR(50) ,
    @authorlast VARCHAR(50) ,
    @authorfirst2 VARCHAR(50) ,
    @authorlast2 VARCHAR(50) ,
    @authorfirst3 VARCHAR(50) ,
    @authorlast3 VARCHAR(50) ,
    @title VARCHAR(255) ,
    @body VARCHAR(MAX) ,
    @issue_season INT ,
    @issue_year INT
AS 
    BEGIN


        INSERT  INTO misesreview
                ( authorfirst ,
                  authorlast ,
                  authorfirst2 ,
                  authorlast2 ,
                  authorfirst3 ,
                  authorlast3 ,
                  title ,
                  body ,
                  issue_season ,
                  issue_year
                )
        VALUES  ( @authorfirst ,
                  @authorlast ,
                  @authorfirst2 ,
                  @authorlast2 ,
                  @authorfirst3 ,
                  @authorlast3 ,
                  @title ,
                  @body ,
                  @issue_season ,
                  @issue_year
                )


        SELECT  SCOPE_IDENTITY()

    END
GO
/****** Object:  StoredProcedure [dbo].[MisesReviewGetReview]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MisesReviewGetReview] @control INT
AS 
    BEGIN
	
        SET NOCOUNT ON ;

        SELECT  title ,
                authorFirst ,
                authorLast ,
                authorFirst2 ,
                authorlast2 ,
                authorFirst3 ,
                authorlast3 ,
                issue_Year ,
                issue_Season ,
                body ,
                CreateDate ,
                GUID
        FROM    misesreview
        WHERE   ( control = @control )
    END
GO
/****** Object:  View [dbo].[PeriodicalsView]    Script Date: 02/17/2010 04:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                    ORDER BY control desc*/
CREATE VIEW [dbo].[PeriodicalsView]
AS
SELECT     dbo.Periodicals.Title AS Journal, dbo.freemarket.GUID AS GUID, dbo.DocumentAuthors.AuthorId, dbo.freemarket.title, 
                      dbo.freemarket.authorfirst + ' ' + dbo.freemarket.authorlast AS AuthorName, dbo.freemarket.authorlast, CAST(DATENAME(month, 
                      dbo.freemarket.articledate) AS VARCHAR(MAX)) + ' ' + CAST(DATENAME(year, dbo.freemarket.articledate) AS VARCHAR(MAX)) AS Volume, 
                      dbo.freemarket.articledate AS DatePosted, '/freemarket_detail.aspx?control=' + CAST(dbo.freemarket.control AS VARCHAR(MAX)) AS URL, 3 As MediaTypeId
FROM         dbo.freemarket INNER JOIN
                      dbo.Periodicals ON dbo.Periodicals.PeriodicalId = 1 LEFT OUTER JOIN
                      dbo.DocumentAuthors ON dbo.DocumentAuthors.AuthorLast = dbo.freemarket.authorlast AND 
                      dbo.DocumentAuthors.AuthorFirst = dbo.freemarket.authorfirst
UNION
SELECT     Periodicals_6.Title AS Journal, dbo.misesreview.GUID AS GUID, DocumentAuthors_6.AuthorId, dbo.misesreview.title, 
                      dbo.misesreview.authorfirst + ' ' + dbo.misesreview.authorlast AS AuthorName, dbo.misesreview.authorlast, 
                      CASE misesreview.issue_season WHEN 1 THEN 'Spring' WHEN 2 THEN 'Summer' WHEN 3 THEN 'Fall' WHEN 4 THEN 'Winter' END + ' ' + CAST(dbo.misesreview.issue_year
                       AS VARCHAR(MAX)) AS Volume, dbo.misesreview.CreateDate AS DatePosted, 
                      '/misesreview_detail.aspx?control=' + CAST(dbo.misesreview.control AS VARCHAR(MAX)) AS URL, 5 As MediaTypeId
FROM         dbo.misesreview INNER JOIN
                      dbo.Periodicals AS Periodicals_6 ON Periodicals_6.PeriodicalId = 2 LEFT OUTER JOIN
                      dbo.DocumentAuthors AS DocumentAuthors_6 ON DocumentAuthors_6.AuthorLast = dbo.misesreview.authorlast AND 
                      dbo.misesreview.authorfirst LIKE '%' + DocumentAuthors_6.AuthorFirst + '%'
UNION
SELECT     Periodicals_5.Title AS Journal, dbo.jls.GUID, DocumentAuthors_5.AuthorId, dbo.jls.title, dbo.jls.authorFirst1 + ' ' + dbo.jls.authorLast1 AS AuthorName, 
                      dbo.jls.authorLast1 AS AuthorLast, 'Vol. ' + CAST(dbo.jls.volume AS VARCHAR(MAX)) + ' Num. ' + CAST(dbo.jls.number AS VARCHAR(MAX)) AS Volume, 
                      DATEADD(year, 1976 + dbo.jls.volume - 1900, 0) AS DatePosted, '/journals/jls/' + CAST(dbo.jls.volume AS VARCHAR(MAX)) 
                      + '_' + CAST(dbo.jls.number AS VARCHAR(MAX)) + '/' + CAST(dbo.jls.volume AS VARCHAR(MAX)) + '_' + CAST(dbo.jls.number AS VARCHAR(MAX)) 
                      + '_' + CAST(dbo.jls.articleNum AS VARCHAR(MAX)) + '.pdf' AS URL, 3 As MediaTypeId
FROM         dbo.jls INNER JOIN
                      dbo.Periodicals AS Periodicals_5 ON Periodicals_5.PeriodicalId = 3 LEFT OUTER JOIN
                      dbo.DocumentAuthors AS DocumentAuthors_5 ON DocumentAuthors_5.AuthorLast = dbo.jls.authorLast1 AND 
                      dbo.jls.authorFirst1 LIKE '%' + DocumentAuthors_5.AuthorFirst + '%'
UNION
SELECT     Periodicals_4.Title AS Journal, dbo.qjaeDB.GUID, DocumentAuthors_4.AuthorId, dbo.qjaeDB.title, 
                      dbo.qjaeDB.authorFirst1 + ' ' + dbo.qjaeDB.authorLast1 AS AuthorName, dbo.qjaeDB.authorLast1 AS AuthorLast, 
                      'Vol. ' + CAST(dbo.qjaeDB.volume AS VARCHAR(MAX)) + ' Num. ' + CAST(dbo.qjaeDB.number AS VARCHAR(MAX)) AS Volume, DATEADD(year, 
                      1996 + dbo.qjaeDB.volume - 1900, 0) AS DatePosted, '/journals/qjae/pdf/qjae' + CAST(dbo.qjaeDB.volume AS VARCHAR(MAX)) 
                      + '_' + CAST(dbo.qjaeDB.number AS VARCHAR(MAX)) + '_' + CAST(dbo.qjaeDB.articleNum AS VARCHAR(MAX)) + '.pdf' AS URL, 3 As MediaTypeId
FROM         dbo.qjaeDB INNER JOIN
                      dbo.Periodicals AS Periodicals_4 ON Periodicals_4.PeriodicalId = 4 LEFT OUTER JOIN
                      dbo.DocumentAuthors AS DocumentAuthors_4 ON DocumentAuthors_4.AuthorLast = dbo.qjaeDB.authorLast1 AND 
                      dbo.qjaeDB.authorFirst1 LIKE '%' + DocumentAuthors_4.AuthorFirst + '%'
UNION
SELECT     Periodicals_3.Title AS Journal, dbo.raeDB.GUID, DocumentAuthors_3.AuthorId, dbo.raeDB.title, 
                      dbo.raeDB.authorFirst1 + ' ' + dbo.raeDB.authorLast1 AS AuthorName, dbo.raeDB.authorLast1 AS AuthorLast, 
                      'Vol. ' + CAST(dbo.raeDB.volume AS VARCHAR(MAX)) + ' Num. ' + CAST(dbo.raeDB.number AS VARCHAR(MAX)) AS Volume, DATEADD(year, 
                      1986 + dbo.raeDB.volume - 1900, 0) AS DatePosted, '/journals/rae/pdf/RAE' + CAST(dbo.raeDB.volume AS VARCHAR(MAX)) 
                      + '_' + CAST(dbo.raeDB.number AS VARCHAR(MAX)) + '_' + CAST(dbo.raeDB.articleNum AS VARCHAR(MAX)) + '.pdf' AS URL, 3 As MediaTypeId
FROM         dbo.raeDB INNER JOIN
                      dbo.Periodicals AS Periodicals_3 ON Periodicals_3.PeriodicalId = 5 LEFT OUTER JOIN
                      dbo.DocumentAuthors AS DocumentAuthors_3 ON DocumentAuthors_3.AuthorLast = dbo.raeDB.authorLast1 AND 
                      dbo.raeDB.authorFirst1 LIKE '%' + DocumentAuthors_3.AuthorFirst + '%'
UNION
SELECT     Periodicals_2.Title AS Journal, NULL AS GUID, DocumentAuthors_2.AuthorId, dbo.aenDB.title, 
                      dbo.aenDB.authorFirst1 + ' ' + dbo.aenDB.authorLast1 AS AuthorName, dbo.aenDB.authorLast1 AS AuthorLast, 
                      'Vol. ' + CAST(dbo.aenDB.volume AS VARCHAR(MAX)) + ' Num. ' + CAST(dbo.aenDB.number AS VARCHAR(MAX)) AS Volume, DATEADD(year, 
                      1976 + dbo.aenDB.volume - 1900, 0) AS DatePosted, '/journals/aen/aen' + CAST(dbo.aenDB.volume AS VARCHAR(MAX)) 
                      + '_' + CAST(dbo.aenDB.number AS VARCHAR(MAX)) + '_' + CAST(dbo.aenDB.articleNum AS VARCHAR(MAX)) + dbo.aenDB.fileType AS URL, 3 As MediaTypeId
FROM         dbo.aenDB INNER JOIN
                      dbo.Periodicals AS Periodicals_2 ON Periodicals_2.PeriodicalId = 6 LEFT OUTER JOIN
                      dbo.DocumentAuthors AS DocumentAuthors_2 ON DocumentAuthors_2.AuthorLast = dbo.aenDB.authorLast1 AND 
                      dbo.aenDB.authorFirst1 LIKE '%' + DocumentAuthors_2.AuthorFirst + '%'
UNION
SELECT     Periodicals_1.Title AS Journal, NULL AS GUID, DocumentAuthors_1.AuthorId, dbo.scholar.paper_name + '(' + dbo.scholar.notation + ')' AS 'Title', 
                      dbo.scholar.author_name AS AuthorName, dbo.scholar.author_name AS AuthorLast, dbo.scholar.whenit AS Volume, 
                      dbo.scholar.CreateDate AS 'DatePosted', '/journals/scholar/' + dbo.scholar.file_name AS URL, 3 As MediaTypeId
FROM         dbo.scholar INNER JOIN
                      dbo.Periodicals AS Periodicals_1 ON Periodicals_1.PeriodicalId = 7 LEFT OUTER JOIN
                      dbo.DocumentAuthors AS DocumentAuthors_1 ON 
                      DocumentAuthors_1.AuthorFirst + ' ' + DocumentAuthors_1.AuthorMiddle + ' ' + DocumentAuthors_1.AuthorLast = dbo.scholar.author_name OR 
                      DocumentAuthors_1.AuthorFirst + ' ' + DocumentAuthors_1.AuthorLast = dbo.scholar.author_name
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[26] 4[32] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[84] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 5
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'PeriodicalsView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'PeriodicalsView'
GO
/****** Object:  StoredProcedure [dbo].[changePassword]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[changePassword] 
	-- Add the parameters for the stored procedure here
    @Username VARCHAR(50) ,
    @Password VARCHAR(50)
AS 
    BEGIN
        SET NOCOUNT ON ;

        UPDATE  ManagerUsers
        SET     password = @Password
        WHERE   username = @Username
    END
GO
/****** Object:  StoredProcedure [dbo].[spVerifyLogin]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spVerifyLogin]
    @Username VARCHAR(50) ,
    @Password VARCHAR(50)
AS 
    IF ( SELECT perms
         FROM   ManagerUsers
         WHERE  username = @Username
                AND password = @Password
                AND active = 1
       ) = 'Admin' 
        SELECT  2
        FROM    ManagerUsers
        WHERE   username = @Username
                AND password = @Password
                AND active = 1
    ELSE 
        SELECT  COUNT(*)
        FROM    ManagerUsers
        WHERE   username = @Username
                AND password = @Password
                AND active = 1
GO
/****** Object:  StoredProcedure [dbo].[PagesGetGUID]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PagesGetGUID]
    @PageName VARCHAR(255) ,
    @Path VARCHAR(255)
AS 
    BEGIN

        DECLARE @GUID UNIQUEIDENTIFIER

        SET @GUID = ( SELECT    Page.GUID
                      FROM      dbo.Page
                      WHERE     ( Page.pageName = @PageName )
                                AND ( Page.folder LIKE @path )
                    )

        SELECT  @GUID

    END
GO
/****** Object:  StoredProcedure [dbo].[PageAddNew]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PageAddNew]
    @Title VARCHAR(500) ,
    @Name VARCHAR(500) ,
    @Folder VARCHAR(255) ,
    @Content VARCHAR(MAX) ,
    @Keywords VARCHAR(500) ,
    @Description VARCHAR(MAX)
AS 
    BEGIN

        IF ( SELECT COUNT(*)
             FROM   page
             WHERE  folder = @Folder
                    AND pageName = @Name
           ) > 0 
            UPDATE  Page
            SET     pageTitle = @Title ,
                    pageName = @Name ,
                    folder = @Folder ,
                    [content] = @Content ,
                    metaKeywords = @Keywords ,
                    metaDescription = @Description ,
                    EditDate = GETDATE()
            WHERE   folder = @Folder
                    AND pageName = @Name
        ELSE 
            INSERT  INTO Page
                    ( pageTitle ,
                      pageName ,
                      folder ,
                      [content] ,
                      metaKeywords ,
                      metaDescription
                    )
            VALUES  ( @Title ,
                      @Name ,
                      @Folder ,
                      @Content ,
                      @Keywords ,
                      @Description
                    )


    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetUserTags]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetUserTags]
    @User VARCHAR(100) ,
    @User2 VARCHAR(100) = ''
AS 
    BEGIN

		-- Replace IP address with singed in user Id
		--IF @User2 <> ''
		--UPDATE TagMap SET TaggedBy = @User WHERE TaggedBy =@User2


		-- User Tagged Documents

        SELECT TOP 1000
                Documents.GUID ,
                Documents.[Title] ,
                CASE WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] IN ( 1, 2 )
                          ) > 0 THEN 'Media'
                     WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] = 3
                          ) > 0 THEN 'PDF'
                     ELSE 'Content'
                END AS 'Type' ,
                [Documents].[DocumentId] AS Id ,
                '' AS URL ,
                Tag.[Tag]
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN Documents WITH ( NOLOCK ) ON Documents.[GUID] = [TagMap].[ObjectId]
        WHERE   [Documents].[Display] = 1
                AND [TagMap].TaggedBy = @User
        UNION ALL
        SELECT TOP 1000
                Page.[GUID] ,
                Page.[pageTitle] AS Title ,
                'Content' AS 'Type' ,
                [Page].[PageId] AS Id ,
                '/' + page.[folder] + page.[pageName] AS URL ,
                Tag.[Tag]
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN Tag ON TagMap.TagId = Tag.TagId
                INNER JOIN [Page] ON [Page].[GUID] = [TagMap].[ObjectId]
        WHERE   [TagMap].TaggedBy = @User
        UNION ALL
        SELECT TOP 1000
                DailyArticles.GUID ,
                DailyArticles.[Title] ,
                'DailyArticle' AS TYPE ,
                [DailyArticles].[ArticleId] AS Id ,
                '' AS URL ,
                Tag.[Tag]
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN DailyArticles ON DailyArticles.[GUID] = [TagMap].[ObjectId]
        WHERE   [TagMap].TaggedBy = @User
        UNION ALL
        SELECT  [GUID] ,
                NAME ,
                'Product' AS 'Type' ,
                products.ProductId AS Id ,
                '' AS URL ,
                Tag.[Tag]
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN AbleCommerce..ac_Products products ON TagMap.ObjectId = products.GUID
        WHERE   [TagMap].TaggedBy = @User
        ORDER BY Id DESC
        
		-- User Tag Map
	
        SELECT  Tag.Tag ,
                COUNT(Tag.TagId) AS 'Count' ,
                Tag.TagId
        FROM    Tag
                INNER JOIN TagMap ON Tag.TagId = TagMap.TagId
        WHERE   ( TagMap.TaggedBy = @User )
                OR ( TagMap.TaggedBy = @User2 )
        GROUP BY Tag.TagId ,
                Tag.Tag

    END
GO
/****** Object:  StoredProcedure [dbo].[PageUpdate]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PageUpdate]
    @GUID UNIQUEIDENTIFIER ,
    @Title VARCHAR(255) ,
    @Keywords VARCHAR(500) ,
    @Description VARCHAR(500) ,
    @Content VARCHAR(MAX)
--@Name varchar(500),
--@Folder varchar(255),
AS 
    BEGIN


        UPDATE  Page
        SET     pageTitle = @Title ,
                [content] = @Content ,
                metaKeywords = @Keywords ,
                metaDescription = @Description ,
                EditDate = GETDATE()
        WHERE   GUID = @GUID

    END
GO
/****** Object:  StoredProcedure [dbo].[PageGetGUIDByPageId]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PageGetGUIDByPageId] @PageId INT
AS 
    BEGIN
	
        SET NOCOUNT ON ;
        SELECT  GUID
        FROM    Page
        WHERE   PageId = @PageId
    END
GO
/****** Object:  StoredProcedure [dbo].[PageGetIdByGUID]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PageGetIdByGUID]
    @GUID UNIQUEIDENTIFIER
AS 
    BEGIN
	
        SET NOCOUNT ON ;
        SELECT  PageId
        FROM    Page
        WHERE   [GUID] = @GUID
    END
GO
/****** Object:  StoredProcedure [dbo].[_SearchDocumentText]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_SearchDocumentText] @find VARCHAR(500)
AS 
    BEGIN

        SELECT  COUNT(*) AS 'Articles'
        FROM    DailyArticles
        WHERE   ArticleText LIKE '%' + @find + '%'

-- Study Guide

        SELECT  COUNT(*) AS 'Documents'
        FROM    Documents
        WHERE   GuideContent LIKE '%' + @find + '%'

-- Pages

        SELECT  COUNT(*) AS 'Pages'
        FROM    Page
        WHERE   [Content] LIKE '%' + @find + '%'

    END
GO
/****** Object:  StoredProcedure [dbo].[PageDelete]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PageDelete]
    @GUID UNIQUEIDENTIFIER
AS 
    BEGIN


        DELETE  FROM PAGE
        WHERE   GUID = @GUID

        DELETE  FROM [DailyArticles]
        WHERE   [GUID] = @GUID





    END
GO
/****** Object:  StoredProcedure [dbo].[GetContentbyGUID]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetContentbyGUID]
    @GUID UNIQUEIDENTIFIER
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    BEGIN

        IF EXISTS ( SELECT  *
                    FROM    page
                    WHERE   GUID = @GUID ) 
            BEGIN
                DECLARE @PageId INT
                SET @PageId = ( SELECT  PageId
                                FROM    Page
                                WHERE   GUID = @GUID
                              )

                SELECT  'Page' AS DocumentType ,
                        PageId AS Id ,
                        pageTitle AS Title ,
                        [content] AS Contents ,
                        page.[folder] + '/' + page.[pageName] AS URL ,
                        EditDate AS CreateDate ,
                        metaDescription AS Description ,
                        metaKeywords AS Keywords ,
						-- Get Next/Prev links
                        ( SELECT TOP 1
                                    GUID
                          FROM      Page
                          WHERE     PageId > @PageId
                                    AND Visible = 1
                        ) AS 'NextGUID' ,
                        ( SELECT TOP 1
                                    GUID
                          FROM      Page
                          WHERE     PageId < @PageId
                                    AND Visible = 1
                          ORDER BY  PageId DESC
                        ) AS 'PreviousGUID'
                FROM    Page
                WHERE   [GUID] = @GUID
            END

        ELSE 
            IF EXISTS ( SELECT  *
                        FROM    Documents
                        WHERE   GUID = @GUID ) 
                BEGIN

                    DECLARE @DocumentId INT
                    SET @DocumentId = ( SELECT  DocumentId
                                        FROM    Documents
                                        WHERE   GUID = @GUID
                                      )


                    DECLARE @Keywords VARCHAR(200)
                    SET @Keywords = ''
                    SELECT  @Keywords = @Keywords
                            + CASE WHEN LEN(@Keywords) > 0 THEN ','
                                   ELSE ''
                              END + Subject
                    FROM    DocumentSubjects ds
                            JOIN DocumentSubjectLink dsl ON dsl.SubjectId = ds.SubjectId
                    WHERE   dsl.DocumentId = @DocumentId


                    DECLARE @Contents VARCHAR(MAX) 
                    SET @Contents = ( SELECT    GuideContent
                                      FROM      Documents
                                      WHERE     DocumentId = @DocumentId
                                    )
					
                    IF @Contents = ''
                        OR @Contents IS NULL 
                        SET @Contents = ( SELECT TOP 1
                                                    URL
                                          FROM      MediaAlternateFormat
                                          WHERE     MediaAlternateFormat.DocumentId = @DocumentId
                                        )
				


                    SELECT  CASE WHEN LEN(GuideContent) = 0
                                      AND ( SELECT  COUNT(*)
                                            FROM    MediaAlternateFormat
                                            WHERE   DocumentId = @DocumentId
                                                    AND [MediaTypeId] IN ( 1,
                                                              2 )
                                          ) > 0 THEN 'Media'
                                 WHEN LEN(GuideContent) = 0
                                      AND ( ( SELECT    COUNT(*)
                                              FROM      MediaAlternateFormat
                                              WHERE     DocumentId = @DocumentId
                                                        AND [MediaTypeId] = 3
                                            ) > 0 ) THEN 'PDF'
                                 ELSE 'Content'
                            END AS 'DocumentType' ,
                            DocumentId AS Id ,
                            Title ,
                            PubInfo AS Description ,
                            @Contents AS Contents ,
                            '' AS URL ,
                            EditDate AS CreateDate ,
                            @Keywords AS Keywords ,
						-- Get Next/Prev links
                            ( SELECT TOP 1
                                        GUID
                              FROM      Documents
                              WHERE     DocumentId > @DocumentId
                                        AND Documents.Display = 1
                            ) AS 'NextGUID' ,
                            ( SELECT TOP 1
                                        GUID
                              FROM      Documents
                              WHERE     DocumentId < @DocumentId
                                        AND Documents.Display = 1
                              ORDER BY  DocumentId DESC
                            ) AS 'PreviousGUID'
                    FROM    Documents
                    WHERE   GUID = @GUID

                END
            ELSE 
                IF EXISTS ( SELECT  *
                            FROM    Calendar
                            WHERE   GUID = @GUID ) 
                    SELECT  'Calendar' AS DocumentType ,
                            EventId AS Id ,
                            Title ,
                            IntroText AS Description ,
                            Description AS Contents ,
                            '' AS URL ,
                            CreateDate ,
                            EventDate + ' ' + Location AS Keywords
                    FROM    Calendar
                    WHERE   ( GUID = @GUID )

                ELSE 
                    IF EXISTS ( SELECT  *
                                FROM    DailyArticles
                                WHERE   GUID = @GUID ) 
                        BEGIN

                            DECLARE @DatePosted DATETIME
                            SET @DatePosted = ( SELECT  DatePosted
                                                FROM    DailyArticles
                                                WHERE   GUID = @GUID
                                              )

                            SELECT  'DailyArticle' AS DocumentType ,
                                    ArticleId AS Id ,
                                    Title ,
                                    Description ,
                                    ArticleText AS Contents ,
                                    '' AS URL ,
                                    EditDate AS CreateDate ,
                                    da.AuthorFirst + ' ' + da.AuthorMiddle
                                    + ' ' + da.AuthorLast AS Author ,
                                    da.AuthorFirst + ' ' + da.AuthorMiddle
                                    + ' ' + da.AuthorLast + ','
                                    + da.AuthorFirst + ' ' + da.AuthorMiddle
                                    + ' ' + da.AuthorLast AS Keywords ,       

						-- Get Next/Prev links
                                    ( SELECT TOP 1
                                                daNext.GUID
                                      FROM      DailyArticles daNext
                                      WHERE     daNext.DatePosted > @DatePosted
                                                AND daNext.ShowArticle = 1
                                      ORDER BY  daNext.DatePosted
                                    ) AS 'NextId' ,
                                    ( SELECT TOP 1
                                                daprevious.GUID
                                      FROM      DailyArticles daprevious
                                      WHERE     daprevious.DatePosted < @DatePosted
                                                AND daprevious.ShowArticle = 1
                                      ORDER BY  daprevious.DatePosted DESC
                                    ) AS 'PreviousId'
                            FROM    DailyArticles
                                    LEFT OUTER JOIN dbo.DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
                                                              OR da.AuthorId = DailyArticles.CoAuthorId
                            WHERE   GUID = @GUID
                        END
                    ELSE 
                        IF EXISTS ( SELECT  *
                                    FROM    RegistrationForms
                                    WHERE   GUID = @GUID ) 
                            SELECT  'Form' AS DocumentType ,
                                    FormId AS Id ,
                                    FormTitle AS Title ,
                                    FormTitle AS Description ,
                                    Introduction AS Contents ,
                                    '' AS URL ,
                                    CreateDate ,
                                    '' AS Keywords
                            FROM    RegistrationForms
                            WHERE   GUID = @GUID

                        ELSE 
                            IF EXISTS ( SELECT  *
                                        FROM    pages
                                        WHERE   GUID = @GUID ) 
                                SELECT  'Page' AS DocumentType ,
                                        control AS Id ,
                                        pageTitle AS Title ,
                                        [content] AS Contents ,
                                        '' AS URL ,
                                        CreateDate ,
                                        metaDescription AS Description ,
                                        metaKeywords AS Keywords
                                FROM    Pages
                                WHERE   [GUID] = @GUID

        IF EXISTS ( SELECT  *
                    FROM    AbleCommerce..ac_Products
                    WHERE   GUID = @GUID ) 
            BEGIN

                DECLARE @ProductId INT
                SET @ProductId = ( SELECT   ProductId
                                   FROM     AbleCommerce..ac_Products
                                   WHERE    GUID = @GUID
                                 )
                SELECT  'Product' AS DocumentType ,
                        ProductId AS Id ,
                                      -- todo  AbleCommerce..ac_Manufacturers.Name + ' : ' +
                        NAME AS Title ,
                        '<img src="/store/' + ThumbnailUrl + '" alt="' + NAME
                        + '" align="left" />'
                        + '<span class="CommonMessageTitle"><a href="/store/Product1.aspx?ProductId='
                        + CAST(ProductId AS VARCHAR(10)) + '">
Buy Now!</a>
</span><br /><br />' + Description AS Contents ,
                        '' AS URL ,
                        CreatedDate AS 'CreateDate' ,
                        [NAME] + ' ' + Summary AS Description ,
                                       -- AbleCommerce..ac_Manufacturers.Name --+ ',' + (SELECT  TOP 1 FieldValue FROM MisesShop..CUSTOMFIELDS WHERE TableName = 'PRODUCTS' AND ForeignKey_ID = ProductId
                        '' AS Keywords ,
-- Get Next/Prev links
                        ( SELECT TOP 1
                                    GUID
                          FROM      AbleCommerce..ac_Products
                                    JOIN AbleCommerce..ac_Manufacturers ON AbleCommerce..ac_Manufacturers.ManufacturerId = AbleCommerce..ac_Products.ManufacturerId
                          WHERE     ProductId > @ProductId
                                    AND AbleCommerce..ac_Products.DisablePurchase = 0
                        ) AS 'NextGUID' ,
                        ( SELECT TOP 1
                                    GUID
                          FROM      AbleCommerce..ac_Products
                          WHERE     ProductId < @ProductId
                                    AND AbleCommerce..ac_Products.DisablePurchase = 0
                          ORDER BY  ProductId DESC
                        ) AS 'PreviousGUID'
                FROM    AbleCommerce..ac_Products
                WHERE   [GUID] = @GUID
            END

    END
GO
/****** Object:  StoredProcedure [dbo].[donsp_sDonationList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[donsp_sDonationList]
    @WhereClause VARCHAR(255) ,
    @SortOrder VARCHAR(255) = NULL
AS 
    BEGIN
        DECLARE @OldCardNumber VARCHAR(255) ,
            @CardNumber VARCHAR(255) ,
            @DonationID INT
        SELECT  * ,
                CONVERT(BIT, 0) AS IsOriginal ,
                0 AS IsProcessed
        INTO    #tmpResult
        FROM    Donations
        WHERE   IsDeleted = 0
        ORDER BY CardNumber ,
                CreateTime ASC
        SELECT  @OldCardNumber = 'CRAP'
        WHILE EXISTS ( SELECT   1
                       FROM     #tmpResult
                       WHERE    IsProcessed = 0 ) 
            BEGIN
                SET ROWCOUNT 1
                SELECT  @CardNumber = CardNumber ,
                        @DonationID = DonationID
                FROM    #tmpResult
                WHERE   IsProcessed = 0
                ORDER BY CardNumber ,
                        CreateTime ASC
                UPDATE  #tmpResult
                SET     IsProcessed = 1
                WHERE   IsProcessed = 0
                SET ROWCOUNT 0
                IF @CardNumber <> @OldCardNumber 
                    BEGIN
                        UPDATE  #tmpResult
                        SET     IsOriginal = 1
                        WHERE   DonationID = @DonationID
                    END
                SELECT  @OldCardNumber = @CardNumber
            END
        IF @SortOrder IS NULL 
            BEGIN
                EXECUTE ( 'select * from #tmpResult where ' + @WhereClause )
            END
        ELSE 
            BEGIN
                EXECUTE
                ( 'select * from #tmpResult where ' + @WhereClause
                + ' order by ' + @SortOrder )
            END
    END
GO
/****** Object:  StoredProcedure [dbo].[spSearchPeople]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSearchPeople]
    @SearchTerm VARCHAR(200) = NULL
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF

    IF @SearchTerm IS NULL 
        SELECT  Personid ,
                FirstName + ' ' + MiddleName + ' ' + LastName AS 'Name' ,
                School ,
                Status ,
                Email ,
                WebSite ,
                BlogURL ,
                SUBSTRING(WebSite, 8, 13) + '..' AS WebSiteTrim ,
                SUBSTRING(BlogURL, 8, 13) + '..' AS BlogURLTrim
        FROM    PEOPLE
        ORDER BY LastName
    ELSE 
        SELECT  Personid ,
                FirstName + ' ' + MiddleName + ' ' + LastName AS 'Name' ,
                School ,
                Status ,
                Email ,
                WebSite ,
                BlogURL ,
                SUBSTRING(WebSite, 8, 13) + '..' AS WebSiteTrim ,
                SUBSTRING(BlogURL, 8, 13) + '..' AS BlogURLTrim
        FROM    PEOPLE
        WHERE   FirstName LIKE '%' + @SearchTerm + '%'
                OR LastName LIKE '%' + @SearchTerm + '%'
                OR School LIKE '%' + @SearchTerm + '%'
                OR Status LIKE '%' + @SearchTerm + '%'
                OR WebSite LIKE '%' + @SearchTerm + '%'
                OR Specialization LIKE '%' + @SearchTerm + '%'
                OR FavoriteBooks LIKE '%' + @SearchTerm + '%'
                OR FavoriteThinkers LIKE '%' + @SearchTerm + '%'
                OR LastName LIKE '%' + @SearchTerm + '%'
                OR Education LIKE '%' + @SearchTerm + '%'
                OR AdditionalInfo LIKE '%' + @SearchTerm + '%'
                OR BlogURL LIKE '%' + @SearchTerm + '%'
                OR SlashdotNick LIKE '%' + @SearchTerm + '%'
                OR DiggNick LIKE '%' + @SearchTerm + '%'
        ORDER BY LastName
GO
/****** Object:  StoredProcedure [dbo].[spGetNewsTicker]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetNewsTicker]
AS 
    SET NOCOUNT ON 

    SELECT  url ,
            title
    FROM    ticker
    WHERE   ( display = 'Yes' )
    ORDER BY control DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaSearch]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaSearch]
    @SearchClause VARCHAR(100)
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  MediaCategory.CategoryId ,
            MediaCategory.Category ,
            MediaCategory.Description ,
            MediaCategory.CategoryImage ,
            ISNULL(( SELECT TOP 1
                            Documents.CreateDate
                     FROM   dbo.Documents
                     WHERE  Documents.CategoryId = MediaCategory.CategoryId
                     ORDER BY Documents.CreateDate DESC
                   ), '1/1/2050') AS EditDate
    FROM    dbo.MediaCategory
    WHERE   MediaCategory.CategoryId = -1
    ORDER BY EditDate DESC

    SELECT  Documents.DocumentId ,
            Documents.GUID ,
            Documents.Display ,
            Documents.metaDescription AS 'Description' ,
            Documents.Title ,
            Documents.Source ,
            Documents.CreateDate ,
            [Documents].[EditDate] ,
            media.URL ,
            media.MediaId ,
            DocumentMediaType.MediaType ,
            DocumentMediaType.MediaIconPath ,
            mc.AuthorId ,
            AuthorFirst + '  ' + AuthorMiddle + '  ' + AuthorLast AS Author ,
            mc.[AuthorLast] AS LastName ,
            CASE WHEN Documents.metaImage IS NULL THEN 0
                 WHEN Documents.metaImage IS NOT NULL THEN 1
            END AS HasImage
    FROM    Documents
            JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
            LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
            LEFT JOIN DocumentAuthors mc ON Documents.Author1 = mc.AuthorId
    WHERE   media.MediaTypeId IN ( 1, 2 )
            AND ( Documents.Title LIKE '%' + @SearchClause + '%'
                  OR Documents.PubInfo LIKE '%' + @SearchClause + '%'
                  OR Documents.metaDescription LIKE '%' + @SearchClause + '%'
                  OR Documents.metaKeywords LIKE '%' + @SearchClause + '%'
                  OR mc.AuthorFirst LIKE @SearchClause
                  OR mc.AuthorLast LIKE @SearchClause
                )
    ORDER BY Documents.CreateDate DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetAuthorContents]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetAuthorContents] @AuthorId INT
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  MediaCategory.CategoryId ,
            MediaCategory.Category ,
            MediaCategory.Description ,
            MediaCategory.CategoryImage ,
            ISNULL(( SELECT TOP 1
                            Documents.CreateDate
                     FROM   dbo.Documents
                     WHERE  Documents.CategoryId = MediaCategory.CategoryId
                     ORDER BY Documents.CreateDate DESC
                   ), '1/1/2050') AS EditDate
    FROM    dbo.MediaCategory
    WHERE   MediaCategory.CategoryId = -1    


    SELECT  Documents.DocumentId ,
            Documents.Title ,
            Documents.CategoryId ,
            Documents.[EditDate] ,
            Documents.CreateDate ,
            Documents.metaDescription AS 'Description' ,
            media.DocumentId ,
            media.MediaTypeId ,
            media.URL ,
            media.MediaId ,
            media.CreateDate ,
            DocumentAuthors.AuthorId ,
            DocumentAuthors.[AuthorLast] AS LastName ,
            DocumentMediaType.MediaType ,
            DocumentMediaType.MediaIconPath ,
            DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle
            + '  ' + DocumentAuthors.AuthorLast AS Author ,
            dbo.DocumentAuthors.Photo,
            CASE WHEN Documents.metaImage IS NULL THEN 0
                 WHEN Documents.metaImage IS NOT NULL THEN 1
            END AS HasImage
    FROM    dbo.Documents
            LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
            LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
            LEFT JOIN dbo.DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
    WHERE   DocumentMediaType.MediaTypeId IN ( 1, 2 )
            AND ( Documents.Author1 = @AuthorId
                  OR Documents.Author2 = @AuthorId
                )
            AND Documents.Display = 1
    ORDER BY Documents.Title
GO
/****** Object:  StoredProcedure [dbo].[oldMediaGetDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[oldMediaGetDetails] @DocumentId INT
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 
    SELECT  Documents.DocumentId ,
            Documents.GUID ,
            Documents.Display ,
            Documents.Title ,
            Documents.Author1 ,
            Documents.Author2 ,
            Documents.PubInfo ,
            Documents.FullText ,
            Documents.Source ,
            Documents.Length ,
            Documents.CategoryId ,
            Documents.CreateDate ,
            Documents.EditDate ,
            DocumentAuthors.AuthorId ,
            DocumentAuthors.AuthorFirst ,
            DocumentAuthors.AuthorMiddle ,
            DocumentAuthors.AuthorLast ,
            DocumentAuthors.Photo ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            Documents.metaDescription ,
            Documents.metaKeywords ,
            Documents.metaImage
    FROM    Documents
            LEFT OUTER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
    WHERE   ( Documents.DocumentId = @DocumentId )
    ORDER BY DocumentAuthors.AuthorLast



    SELECT  MediaAlternateFormat.MediaId ,
            MediaAlternateFormat.DocumentId ,
            MediaAlternateFormat.MediaTypeId ,
            MediaAlternateFormat.URL ,
            MediaAlternateFormat.CreateDate ,
            DocumentMediaType.MediaTypeID AS Expr1 ,
            DocumentMediaType.MediaType ,
            DocumentMediaType.MediaIconPath ,
            DocumentMediaType.Extensions ,
            DocumentMediaType.UploadPath ,
            DocumentMediaType.Description ,
            DocumentMediaType.CreateTime ,
            DocumentMediaType.MIMEtype ,
            DocumentMediaType.IsDeleted ,
            MediaAlternateFormat.fileSize ,
            MediaAlternateFormat.duration
    FROM    MediaAlternateFormat
            LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = MediaAlternateFormat.MediaTypeId
    WHERE   ( MediaAlternateFormat.DocumentId = @DocumentId )
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetNewLiterature]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentGetNewLiterature]
AS 
    BEGIN

        SET CONCAT_NULL_YIELDS_NULL OFF 
	
        SELECT TOP 12
                Documents.DocumentId ,
                Documents.Title ,
                Documents.PubInfo AS Description ,
                URL = CASE media.URL + ''
                        WHEN ''
                        THEN '/resources/'
                             + CAST(Documents.DocumentId AS VARCHAR(MAX))
                        ELSE media.URL
                      END ,
                DocumentAuthors.AuthorFirst + ' '
                + DocumentAuthors.AuthorMiddle + ' '
                + DocumentAuthors.AuthorLast AS Author ,
                Documents.CreateDate AS CreateDate ,
                MediaIconPath = CASE DocumentMediaType.MediaIconPath + ''
                                  WHEN '' THEN '/images/Icons/html.png'
                                  ELSE DocumentMediaType.MediaIconPath
                                END
        FROM    dbo.Documents WITH ( NOLOCK )
                LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                LEFT JOIN dbo.DocumentMediaType ON media.MediaTypeId = DocumentMediaType.MediaTypeID
        WHERE   Documents.Display = 1
                AND media.URL <> ''
                AND media.MediaTypeId NOT IN ( 1, 2 )
        ORDER BY documents.CreateDate DESC
	
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentAdminLinks]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DocumentAdminLinks]
    @SubjectId INT = 0 ,
    @AuthorId INT = 0 ,
    @MediaType INT = 0 ,
    @Source VARCHAR(30) = NULL ,
    @SearchQuery VARCHAR(50) = NULL
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    IF @SubjectId > 0 
        BEGIN
            SELECT DISTINCT
                    Documents.DocumentId ,
                    Title ,
                    PubInfo ,
                    URL ,
                    Source ,
                    FullText ,
                    AuthorFirst + '  ' + AuthorLast AS Author ,
                    Author1 ,
                    Author2 ,
                    MediaType ,
                    media.MediaTypeId ,
                    MediaType ,
                    MediaIconPath ,
                    Documents.CreateDate
            FROM    Documents
                    LEFT JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                    LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                    LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Documents.Author1
                    LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = Documents.DocumentId
            WHERE   SubjectId = @SubjectId
            ORDER BY CreateDate DESC ,
                    Documents.DocumentId DESC
--Documents.Documents.DocumentId DESC

            SELECT  Subject
            FROM    DocumentSubjects
            WHERE   SubjectId = @SubjectId

            SELECT TOP 10
                    Subject ,
                    SubjectID
            FROM    DocumentSubjects
            WHERE   CategoryId = ( SELECT   CategoryId
                                   FROM     DocumentSubjects
                                   WHERE    SubjectId = @SubjectId
                                 )

        END
    ELSE 
        IF @AuthorId > 0 
            BEGIN
                SELECT DISTINCT
                        Documents.DocumentId ,
                        Title ,
                        PubInfo ,
                        media.URL ,
                        Source ,
                        FullText ,
                        AuthorFirst + '  ' + AuthorLast AS Author ,
                        Author1 ,
                        Author2 ,
                        MediaType ,
                        media.MediaTypeId ,
                        MediaType ,
                        MediaIconPath ,
                        media.CreateDate
                FROM    Documents
                        LEFT JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                        LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                        LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Documents.Author1
                        LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = Documents.DocumentId
                WHERE   ( Author1 = @AuthorId
                          OR Author2 = @AuthorId
                        )
                ORDER BY CreateDate DESC ,
                        Documents.DocumentId DESC

                SELECT  AuthorFirst + '  ' + AuthorLast AS Author
                FROM    DocumentAuthors
                WHERE   DocumentAuthors.AuthorId = @AuthorId

            END
        ELSE 
            IF @MediaType > 0 
                SELECT DISTINCT
                        Documents.DocumentId ,
                        Title ,
                        PubInfo ,
                        URL ,
                        Source ,
                        FullText ,
                        AuthorFirst + '  ' + AuthorLast AS Author ,
                        Author1 ,
                        Author2 ,
                        MediaType ,
                        media.MediaTypeId ,
                        MediaType ,
                        MediaIconPath ,
                        media.CreateDate
                FROM    Documents
                        LEFT JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                        LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                        LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Documents.Author1
                        LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = Documents.DocumentId
                WHERE   media.MediaTypeId = @MediaType
                ORDER BY CreateDate DESC ,
                        Documents.DocumentId DESC

            ELSE 
                IF LEN(@SOURCE) > 0 
                    SELECT DISTINCT
                            Documents.DocumentId ,
                            Title ,
                            PubInfo ,
                            URL ,
                            Source ,
                            FullText ,
                            AuthorFirst + '  ' + AuthorLast AS Author ,
                            Author1 ,
                            Author2 ,
                            MediaType ,
                            media.MediaTypeId ,
                            MediaType ,
                            MediaIconPath ,
                            media.CreateDate
                    FROM    Documents
                            LEFT JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                            LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                            LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Documents.Author1
                            LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = Documents.DocumentId
                    WHERE   Source = @SOURCE
                    ORDER BY CreateDate DESC ,
                            Documents.DocumentId DESC


                ELSE 
                    IF LEN(@SearchQuery) > 0 
                        SELECT DISTINCT
                                Documents.DocumentId ,
                                Title ,
                                PubInfo ,
                                URL ,
                                Source ,
                                FullText ,
                                AuthorFirst + '  ' + AuthorLast AS Author ,
                                Author1 ,
                                Author2 ,
                                MediaType ,
                                media.MediaTypeId ,
                                MediaType ,
                                MediaIconPath ,
                                Documents.CreateDate
                        FROM    Documents
                                LEFT JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                                LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                                LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Documents.Author1
                                LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = Documents.DocumentId
                        WHERE   Title LIKE '%' + @SearchQuery + '%'
                                OR AuthorFirst LIKE '%' + @SearchQuery + '%'
                                OR AuthorLast LIKE '%' + @SearchQuery + '%'
                                OR PubInfo LIKE '%' + @SearchQuery + '%'
                        ORDER BY CreateDate DESC ,
                                Documents.DocumentId DESC

                    ELSE 
                        SELECT DISTINCT
                                Documents.DocumentId ,
                                Title ,
                                PubInfo ,
                                URL ,
                                Source ,
                                FullText ,
                                AuthorFirst + '  ' + AuthorLast AS Author ,
                                Author1 ,
                                Author2 ,
                                MediaType ,
                                media.MediaTypeId ,
                                MediaType ,
                                MediaIconPath ,
                                Documents.CreateDate
                        FROM    Documents
                                LEFT JOIN MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                                LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                                LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Documents.Author1
                                LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = Documents.DocumentId
                        ORDER BY CreateDate DESC ,
                                Documents.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetFeed]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetFeed]
    @AuthorId INT = 0 ,
    @CategoryId INT = 0 ,
    @MediaTypeId INT = 0 ,
    @MaxResults INT = 0
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 
    
    IF @MaxResults > 0 
        SET ROWCOUNT @MaxResults ;
    
    
    DECLARE @MediaTypes TABLE ( TypeId INT )

    IF @MediaTypeId > 0 
        INSERT  INTO @MediaTypes
        VALUES  ( @MediaTypeId )
    ELSE 
        BEGIN 
    
            INSERT  INTO @MediaTypes
                    ( TypeId )
            VALUES  ( 1 ) -- Audio 
    
            INSERT  INTO @MediaTypes
                    ( TypeId )
            VALUES  ( 2 ) -- Video
        
            INSERT  INTO @MediaTypes
                    ( TypeId )
            VALUES  ( 8 ) -- AudioBook
        END


    IF @AuthorId > 0 
        SELECT  Documents.DocumentId ,
                Documents.Title ,
                Documents.GUID ,
                Documents.PubInfo AS Description ,
                media.URL ,
                media.MediaId ,
                DocumentMediaType.MIMEtype ,
                Documents.Length ,
                authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
                Documents.Author1 ,
                Documents.CreateDate ,
                media.fileSize ,
                media.duration ,
                metaDescription ,
                dbo.MediaCategory.iTunesCategoryCode ,
                dbo.MediaCategory.Category
        FROM    Documents
                INNER JOIN MediaAlternateFormat AS media ON media.DocumentId = Documents.DocumentId
                INNER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                INNER JOIN dbo.MediaCategory ON dbo.MediaCategory.CategoryId = dbo.Documents.CategoryId
                LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = media.MediaTypeId
        WHERE   media.MediaTypeId IN ( SELECT   TypeId
                                       FROM     @MediaTypes )
                AND Documents.Display = 1
                AND ( Documents.Author1 = @AuthorId
                      OR Documents.Author2 = @AuthorId
                    )
        ORDER BY Documents.CreateDate    
    IF @CategoryId > 0 
        BEGIN
	

            SELECT  Documents.DocumentId ,
                    Documents.Title ,
                    Documents.GUID ,
                    Documents.PubInfo AS Description ,
                    media.URL ,
                    media.MediaId ,
                    DocumentMediaType.MIMEtype ,
                    Documents.Length ,
                    authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
                    Documents.Author1 ,
                    Documents.CreateDate ,
                    media.fileSize ,
                    media.duration ,
                    metaDescription ,
                    dbo.MediaCategory.iTunesCategoryCode ,
                    dbo.MediaCategory.Category
            FROM    Documents
                    INNER JOIN MediaAlternateFormat AS media ON media.DocumentId = Documents.DocumentId
                    INNER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                    INNER JOIN [MediaCategory] ON [Documents].[CategoryId] = [MediaCategory].[CategoryId]
                    LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = media.MediaTypeId
            WHERE   media.MediaTypeId IN ( SELECT   TypeId
                                           FROM     @MediaTypes )
                    AND Documents.Display = 1
                    AND ( [Documents].[CategoryId] = @CategoryId
                          OR Documents.CategoryId IN (
                          SELECT    m2.[CategoryId]
                          FROM      [MediaCategory]
                                    INNER JOIN [MediaCategory] m2 ON m2.[ParentCategory] = @CategoryId )
                        )
            ORDER BY Documents.CreateDate    

            SELECT  Category ,
                    [Description] ,
                    CategoryImage
            FROM    [MediaCategory]
            WHERE   [CategoryId] = @CategoryId

        END
    ELSE 
        SELECT TOP ( 60 )
                Documents.DocumentId ,
                Documents.Title ,
                Documents.GUID ,
                Documents.PubInfo AS Description ,
                media.URL ,
                media.MediaId ,
                DocumentMediaType.MIMEtype ,
                Documents.Length ,
                authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
                Documents.Author1 ,
                Documents.CreateDate ,
                media.fileSize ,
                media.duration ,
                metaDescription ,
                dbo.MediaCategory.iTunesCategoryCode ,
                dbo.MediaCategory.Category
        FROM    Documents
                INNER JOIN MediaAlternateFormat AS media ON media.DocumentId = Documents.DocumentId
                INNER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                INNER JOIN [MediaCategory] ON [Documents].[CategoryId] = [MediaCategory].[CategoryId]
                LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = media.MediaTypeId
        WHERE   ( media.MediaTypeId IN ( SELECT TypeId
                                         FROM   @MediaTypes ) )
                AND ( Documents.Display = 1 )
        ORDER BY Documents.CreateDate DESC ,
                Documents.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentMediaTypeGetList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentMediaTypeGetList]
AS 
    BEGIN
	
        SET NOCOUNT ON ;

        SELECT  MediaType ,
                MediaTypeId ,
                MediaIconPath
        FROM    DocumentMediaType
        WHERE   MediaTypeId NOT IN ( 1, 2, 8 )
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DocumentDetails]
    @DocumentId INT = 0 ,
    @MediaId INT = 0
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    DECLARE @ParentCategoryId INT

    SET @ParentCategoryId = ( SELECT TOP 1
                                        ParentCategory
                              FROM      MediaCategory dc
                              WHERE     dc.CategoryId = ( SELECT
                                                              CategoryId
                                                          FROM
                                                              dbo.Documents
                                                          WHERE
                                                              DocumentId = @DocumentId
                                                        )
                            )


    IF @MediaId > 0 
        SET @DocumentId = ( SELECT  DocumentId
                            FROM    MediaAlternateFormat
                            WHERE   MediaId = @MediaId
                          )

    SELECT  DocumentId ,
            GUID ,
            Display ,
            Featured ,
            Title ,
            Author1 ,
            Author2 ,
            PubInfo ,
            FullText ,
            Source ,
            GuideContent ,
            Documents.CreateDate ,
            CategoryId ,
            DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle
            + '  ' + DocumentAuthors.AuthorLast AS AuthorName ,
            @ParentCategoryId AS ParentCategoryId ,
            ( SELECT TOP 1
                        ParentCategory
              FROM      MediaCategory dc
              WHERE     dc.CategoryId = @ParentCategoryId
            ) AS GrandParentCategoryId ,
            [Length] ,
            metaDescription ,
            metaKeywords ,
            metaImage
    FROM    Documents
            LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
    WHERE   ( DocumentId = @DocumentId )

    SELECT  DocumentSubjectLink.SubjectId
    FROM    DocumentSubjectLink
            JOIN DocumentSubjects ON DocumentSubjects.SubjectId = DocumentSubjectLink.SubjectId
    WHERE   DocumentId = @DocumentId

    SELECT  MediaAlternateFormat.MediaId ,
            MediaAlternateFormat.DocumentId ,
            MediaAlternateFormat.MediaTypeId ,
            MediaAlternateFormat.URL ,
            MediaAlternateFormat.CreateDate ,
            DocumentMediaType.MediaTypeID AS Expr1 ,
            DocumentMediaType.MediaType ,
            DocumentMediaType.MediaIconPath ,
            DocumentMediaType.UploadPath ,
            DocumentMediaType.Description ,
            DocumentMediaType.CreateTime ,
            DocumentMediaType.MIMEtype ,
            DocumentMediaType.IsDeleted ,
            DocumentMediaType.Extensions ,
            MediaAlternateFormat.fileSize ,
            MediaAlternateFormat.duration
    FROM    MediaAlternateFormat
            LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = MediaAlternateFormat.MediaTypeId
    WHERE   ( MediaAlternateFormat.DocumentId = @DocumentId )
GO
/****** Object:  StoredProcedure [dbo].[MediaGetFeatured]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaGetFeatured]
AS 
    BEGIN
	
        SET NOCOUNT ON ;
        SET CONCAT_NULL_YIELDS_NULL OFF ;
    
    -- Get Headline Article

        SELECT  TOP 1 dbo.Documents.DocumentId ,
                dbo.Documents.GUID ,
                dbo.Documents.Display ,
                dbo.Documents.Featured ,
                dbo.Documents.Title ,
                dbo.Documents.Author1 ,
                dbo.Documents.Author2 ,
                dbo.Documents.PubInfo ,
                dbo.Documents.metaDescription AS 'Description' ,
                dbo.Documents.metaKeywords ,
                dbo.Documents.metaImage ,
                dbo.Documents.oldURL ,
                dbo.Documents.oldMediaTypeId ,
                dbo.Documents.FullText ,
                dbo.Documents.Source ,
                dbo.Documents.GuideContent ,
                dbo.Documents.Length ,
                dbo.Documents.CategoryId ,
                dbo.Documents.CreateDate ,
                dbo.Documents.EditDate,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
                da2.authorFirst + ' ' + da2.authorMiddle + ' ' + da2.authorLast AS CoAuthor , 
                '/MediaPlayer.aspx?Id=' + CAST(DocumentId AS VARCHAR(MAX)) AS 'URL'
                
                
        FROM    dbo.Documents
        JOIN dbo.DocumentAuthors da ON da.AuthorId = Documents.Author1
                LEFT JOIN dbo.DocumentAuthors da2 ON da2.AuthorId = Documents.Author2
        WHERE   Featured = 1
        ORDER BY dbo.Documents.DocumentId DESC
        
        SELECT  dbo.MediaAlternateFormat.MediaId ,
                dbo.MediaAlternateFormat.DocumentId ,
                dbo.MediaAlternateFormat.MediaTypeId ,
                dbo.MediaAlternateFormat.URL ,
                dbo.MediaAlternateFormat.fileSize ,
                dbo.MediaAlternateFormat.duration ,
                dbo.MediaAlternateFormat.CreateDate, 
                dmt.MediaTypeId,                
                dmt.Description,
                dmt.MediaIconPath
        FROM    dbo.MediaAlternateFormat
                INNER JOIN dbo.Documents ON dbo.MediaAlternateFormat.DocumentId = dbo.Documents.DocumentId
                LEFT OUTER JOIN DocumentMediaType dmt ON dmt.MediaTypeID = MediaAlternateFormat.MediaTypeId
                
                
        WHERE   Featured = 1
    
    
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetFeed]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DocumentGetFeed]
    @AuthorId INT = 0 ,
    @SubjectId INT = 0
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    IF @AuthorId > 0 
        SELECT TOP ( 500 )
                Documents.DocumentId ,
                Documents.Title ,
                Documents.GUID ,
                Documents.PubInfo AS Description ,
                media.URL ,
                DocumentMediaType.MIMEtype ,
                Documents.Length ,
                authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
                Documents.CreateDate ,
                media.fileSize ,
                media.duration ,
                metaDescription
        FROM    Documents
                LEFT OUTER JOIN MediaAlternateFormat AS media ON media.DocumentId = Documents.DocumentId
                INNER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = media.MediaTypeId
        WHERE   ( Documents.Display = 1 )
                AND ( Documents.Author1 = @AuthorId
                      OR Documents.Author2 = @AuthorId
                    )
        ORDER BY Documents.CreateDate DESC ,
                Documents.DocumentId DESC
    IF @SubjectId > 0 
        SELECT TOP ( 500 )
                Documents.DocumentId ,
                Documents.Title ,
                Documents.GUID ,
                Documents.PubInfo AS Description ,
                media.URL ,
                DocumentMediaType.MIMEtype ,
                Documents.Length ,
                authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
                Documents.CreateDate ,
                media.fileSize ,
                media.duration ,
                metaDescription
        FROM    Documents
                LEFT OUTER JOIN MediaAlternateFormat AS media ON media.DocumentId = Documents.DocumentId
                INNER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                INNER JOIN [DocumentSubjectLink] ON [DocumentSubjectLink].[DocumentId] = Documents.[DocumentId]
                LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = media.MediaTypeId
        WHERE   ( Documents.Display = 1 )
                AND [DocumentSubjectLink].[SubjectID] = @SubjectId
        ORDER BY Documents.CreateDate DESC ,
                Documents.DocumentId DESC

    ELSE 
        SELECT TOP ( 60 )
                Documents.DocumentId ,
                Documents.Title ,
                Documents.GUID ,
                Documents.PubInfo AS Description ,
                media.URL ,
                DocumentMediaType.MIMEtype ,
                Documents.Length ,
                authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
                Documents.CreateDate ,
                media.fileSize ,
                media.duration ,
                metaDescription
        FROM    Documents
                LEFT OUTER JOIN MediaAlternateFormat AS media ON media.DocumentId = Documents.DocumentId
                INNER JOIN DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                LEFT OUTER JOIN DocumentMediaType ON DocumentMediaType.MediaTypeID = media.MediaTypeId
        WHERE   ( Documents.Display = 1 )
                AND media.MediaTypeId NOT IN ( 1, 2, 8 )
        ORDER BY Documents.CreateDate DESC ,
                Documents.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[spAddBookReview]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddBookReview]
    (
      @BookId INT ,
      @Rating TINYINT ,
      @Title VARCHAR(255) ,
      @Review VARCHAR(8000) ,
      @ScreenName VARCHAR(255) ,
      @Location VARCHAR(255)
    )
AS 
    INSERT  INTO BookReviews
            ( BookId ,
              Rating ,
              Title ,
              Review ,
              ScreenName ,
              Location ,
              ReviewDate
            )
    VALUES  ( @BookId ,
              @Rating ,
              @Title ,
              @Review ,
              @ScreenName ,
              @Location ,
              GETDATE()
            )


    SELECT  @@Identity
GO
/****** Object:  StoredProcedure [dbo].[spGetBookReviews]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetBookReviews] @BookId INT
AS 
    SELECT  BookReviews.ReviewId ,
            BookReviews.BookId ,
            BookReviews.Rating ,
            BookReviews.Title ,
            BookReviews.Review ,
            BookReviews.ScreenName ,
            BookReviews.Location ,
            BookReviews.ReviewDate
    FROM    BookReviews
    WHERE   BookId = @BookId
    ORDER BY ReviewId
GO
/****** Object:  StoredProcedure [dbo].[FellowsGetBio]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FellowsGetBio] @control INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

        SELECT  name ,
                bio
        FROM    fellows
        WHERE   ( control = @control )
    END
GO
/****** Object:  StoredProcedure [dbo].[WardLibraryGetBook]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[WardLibraryGetBook] @BookId INT = 0
AS 
    BEGIN
        SELECT  * ,
                AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName
        FROM    wardlibrary
        WHERE   CONTROL = @BookId
    END
GO
/****** Object:  StoredProcedure [dbo].[WardLibraryGetList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[WardLibraryGetList]
    @authorFirst VARCHAR(MAX) ,
    @authorLast VARCHAR(MAX) ,
    @title VARCHAR(MAX) ,
    @subject VARCHAR(MAX)
AS 
    BEGIN
        SELECT  control ,
                display ,
                title ,
                checked_out_by ,
                authorfirst ,
                authorlast ,
                authorfirst2 ,
                authorlast2 ,
                authorfirst3 ,
                authorlast3 ,
                coauthors ,
                subject
        FROM    wardlibrary
	
	
	
    END
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalsGetDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PeriodicalsGetDetails] 
	-- Add the parameters for the stored procedure here
    @PeriodicalId INT = 0
AS 
    BEGIN
        SELECT  PeriodicalId ,
                Title ,
                Description ,
                Logo ,
                ShortDescription ,
                RedirectURL ,
                PublicationFrequency ,
                metaDescription ,
                metaKeywords ,
                CreateDate
        FROM    Periodicals
        WHERE   ( PeriodicalId = @PeriodicalId )
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetSourceJournalList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentGetSourceJournalList]
AS 
    BEGIN
	
        SET NOCOUNT ON ;

        SELECT DISTINCT
                source
        FROM    Documents
        WHERE   [Display] = 1
                AND source <> ''
                AND PATINDEX('%http%', source) = 0
        UNION ALL
        SELECT DISTINCT
                Title
        FROM    Periodicals
        ORDER BY [source]
    END
GO
/****** Object:  StoredProcedure [dbo].[FavoritesGetUserFavorites]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FavoritesGetUserFavorites] 
	-- Add the parameters for the stored procedure here
    @UserId VARCHAR(100) ,
    @IPaddress VARCHAR(50)
AS 
    BEGIN
	
        IF @UserId <> ''
            AND @UserId <> 'NONE' 
            UPDATE  Favorites
            SET     UserId = @UserId
            WHERE   IPaddress = @IPaddress


        SELECT  FavoriteId ,
                UserId ,
                URL ,
                Title ,
                IPaddress ,
                CreateDate
        FROM    Favorites
        WHERE   UserId = @UserId
                OR IPaddress = @IPaddress
        ORDER BY [CreateDate] DESC

    END
GO
/****** Object:  StoredProcedure [dbo].[FavoriteAdd]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FavoriteAdd]
    @UserId VARCHAR(MAX) ,
    @URL VARCHAR(MAX) ,
    @Title VARCHAR(MAX) ,
    @IPaddress VARCHAR(50)
AS 
    BEGIN

        INSERT  INTO Favorites
                ( URL, Title, UserId, IPaddress )
        VALUES  ( @URL, @Title, @UserId, @Ipaddress )	

    END
GO
/****** Object:  StoredProcedure [dbo].[MediaTopFullSubjects]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaTopFullSubjects]
AS 
    BEGIN
        SELECT  TOP 15 DocumentSubjectLink.SubjectID ,
                [Subject],
                COUNT(1) AS 'Count'
        FROM    DocumentSubjectLink
                INNER JOIN MediaAlternateFormat ON DocumentSubjectLink.DocumentId = MediaAlternateFormat.DocumentId
                JOIN DocumentSubjects ON DocumentSubjectLink.SubjectID = DocumentSubjects.SubjectId
        WHERE   DocumentSubjects.Visible = 1
        GROUP BY DocumentSubjectLink.SubjectID ,
                Subject
        ORDER BY 'Count' DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentSubjectsGetList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentSubjectsGetList] @CategoryId INT = 0
AS 
    BEGIN
        IF @CategoryId > 0 
            SELECT  Subject ,
                    SubjectId
            FROM    DocumentSubjects
            WHERE   Visible = 1
                    AND [CategoryId] = @CategoryId
            ORDER BY SortOrder
        ELSE 
            SELECT  Subject ,
                    SubjectId
            FROM    DocumentSubjects
            WHERE   Visible = 1
            ORDER BY SortOrder
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetLibertarianStudiesSubjects]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentGetLibertarianStudiesSubjects]
AS 
    BEGIN
        SELECT  SubjectId ,
                ShortSubject
        FROM    DocumentSubjects
        WHERE   CategoryID = 4
                AND Visible = 1
        ORDER BY sortorder
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetAustrianEconomicsSubjects]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentGetAustrianEconomicsSubjects]
AS 
    BEGIN
        SELECT  SubjectId ,
                ShortSubject
        FROM    DocumentSubjects
        WHERE   CategoryID = 1
                AND Visible = 1
        ORDER BY sortorder
    END
GO
/****** Object:  StoredProcedure [dbo].[MediaTopSubjects]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaTopSubjects]
AS 
    BEGIN
        SELECT  DocumentSubjectLink.SubjectID ,
                ShortSubject ,
                COUNT(1) AS 'Count'
        FROM    DocumentSubjectLink
                INNER JOIN MediaAlternateFormat ON DocumentSubjectLink.DocumentId = MediaAlternateFormat.DocumentId
                JOIN DocumentSubjects ON DocumentSubjectLink.SubjectID = DocumentSubjects.SubjectId
        WHERE   DocumentSubjects.Visible = 1
        GROUP BY DocumentSubjectLink.SubjectID ,
                ShortSubject
        ORDER BY 'Count' DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetSubjects]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentGetSubjects] 
	-- Add the parameters for the stored procedure here
    @DocumentId INT = 0
--	,@GUID uniqueidentifier = null
AS 
    BEGIN
        SET NOCOUNT ON ;
	
        SELECT  DocumentSubjects.SubjectId ,
                DocumentSubjects.ShortSubject
        FROM    DocumentSubjects WITH ( NOLOCK )
                JOIN [DocumentSubjectLink] ON [DocumentSubjects].[SubjectId] = [DocumentSubjectLink].[SubjectID]
        WHERE   DocumentSubjectLink.[DocumentId] = @DocumentId --OR [DocumentSubjectLink].[GUID] = @GUID
	
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetKeywords]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentGetKeywords] 
	-- Add the parameters for the stored procedure here
    @DocumentId INT
AS 
    BEGIN
        DECLARE @Keywords VARCHAR(MAX)


        SET @Keywords = ''
        SELECT  @Keywords = @Keywords + CASE WHEN LEN(@Keywords) > 0 THEN ','
                                             ELSE ''
                                        END + [ShortSubject]
        FROM    DocumentSubjects ds
                JOIN DocumentSubjectLink dsl ON dsl.SubjectId = ds.SubjectId
        WHERE   dsl.DocumentId = @DocumentId
        SELECT  @Keywords AS Keywords

    END
GO
/****** Object:  StoredProcedure [dbo].[SubjectsTopSubjects]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SubjectsTopSubjects]
AS 
    BEGIN
        SELECT  DocumentSubjectLink.SubjectID ,
                ShortSubject ,
                COUNT(1) AS 'Count'
        FROM    DocumentSubjectLink
                JOIN DocumentSubjects ON DocumentSubjectLink.SubjectID = DocumentSubjects.SubjectId
        WHERE   DocumentSubjects.Visible = 1
        GROUP BY DocumentSubjectLink.SubjectID ,
                ShortSubject
        ORDER BY 'Count' DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[RedirectedURLGet]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RedirectedURLGet]
    @RequestedURL VARCHAR(150)
AS 
    BEGIN

        DECLARE @URL VARCHAR(150)

--IF LEN(@RequestedURL) < 5 RETURN

        SET @URL = ( SELECT TOP 1
                            [RedirectURL]
                     FROM   [RedirectedURL]
                     WHERE  ExactMatchOnly = 1
                            AND [RequestedURL] = @RequestedURL
                     ORDER BY Priority
                   )

        IF @URL IS NULL 
            BEGIN 
                SET @URL = ( SELECT TOP 1
                                    [RedirectURL]
                             FROM   [RedirectedURL]
                             WHERE  ExactMatchOnly = 0
                                    AND @RequestedURL LIKE '%' + RequestedURL
                                    + '%'
                             ORDER BY Priority
                           )
            END

        SELECT  @URL

    END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure]
    @tableName NVARCHAR(450)
AS 
    BEGIN 
        UPDATE  dbo.AspNet_SqlCacheTablesForChangeNotification WITH ( ROWLOCK )
        SET     changeId = changeId + 1
        WHERE   tableName = @tableName
    END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheRegisterTableStoredProcedure]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCacheRegisterTableStoredProcedure]
    @tableName NVARCHAR(450)
AS 
    BEGIN

        DECLARE @triggerName AS NVARCHAR(3000) 
        DECLARE @fullTriggerName AS NVARCHAR(3000)
        DECLARE @canonTableName NVARCHAR(3000) 
        DECLARE @quotedTableName NVARCHAR(3000) 

         /* Create the trigger name */ 
        SET @triggerName = REPLACE(@tableName, '[', '__o__') 
        SET @triggerName = REPLACE(@triggerName, ']', '__c__') 
        SET @triggerName = @triggerName
            + '_AspNet_SqlCacheNotification_Trigger' 
        SET @fullTriggerName = 'dbo.[' + @triggerName + ']' 

         /* Create the cannonicalized table name for trigger creation */ 
         /* Do not touch it if the name contains other delimiters */ 
        IF ( CHARINDEX('.', @tableName) <> 0
             OR CHARINDEX('[', @tableName) <> 0
             OR CHARINDEX(']', @tableName) <> 0
           ) 
            SET @canonTableName = @tableName 
        ELSE 
            SET @canonTableName = '[' + @tableName + ']' 

         /* First make sure the table exists */ 
        IF ( SELECT OBJECT_ID(@tableName, 'U')
           ) IS NULL 
            BEGIN 
                RAISERROR ('00000001', 16, 1) 
                RETURN 
            END 

        BEGIN TRAN
         /* Insert the value into the notification table */ 
        IF NOT EXISTS ( SELECT  tableName
                        FROM    dbo.AspNet_SqlCacheTablesForChangeNotification
                                WITH ( NOLOCK )
                        WHERE   tableName = @tableName ) 
            IF NOT EXISTS ( SELECT  tableName
                            FROM    dbo.AspNet_SqlCacheTablesForChangeNotification
                                    WITH ( TABLOCKX )
                            WHERE   tableName = @tableName ) 
                INSERT  dbo.AspNet_SqlCacheTablesForChangeNotification
                VALUES  ( @tableName, GETDATE(), 0 )

         /* Create the trigger */ 
        SET @quotedTableName = QUOTENAME(@tableName, '''') 
        IF NOT EXISTS ( SELECT  name
                        FROM    sysobjects WITH ( NOLOCK )
                        WHERE   name = @triggerName
                                AND type = 'TR' ) 
            IF NOT EXISTS ( SELECT  name
                            FROM    sysobjects WITH ( TABLOCKX )
                            WHERE   name = @triggerName
                                    AND type = 'TR' ) 
                EXEC('CREATE TRIGGER ' + @fullTriggerName + ' ON ' + @canonTableName +'
                FOR INSERT, UPDATE, DELETE AS BEGIN
                SET NOCOUNT ON
                EXEC dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure N' + @quotedTableName + '
                END
                ')
        COMMIT TRAN
    END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure]
AS 
    SELECT  tableName
    FROM    dbo.AspNet_SqlCacheTablesForChangeNotification
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheUnRegisterTableStoredProcedure]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCacheUnRegisterTableStoredProcedure]
    @tableName NVARCHAR(450)
AS 
    BEGIN

        BEGIN TRAN
        DECLARE @triggerName AS NVARCHAR(3000) 
        DECLARE @fullTriggerName AS NVARCHAR(3000)
        SET @triggerName = REPLACE(@tableName, '[', '__o__') 
        SET @triggerName = REPLACE(@triggerName, ']', '__c__') 
        SET @triggerName = @triggerName
            + '_AspNet_SqlCacheNotification_Trigger' 
        SET @fullTriggerName = 'dbo.[' + @triggerName + ']' 

         /* Remove the table-row from the notification table */ 
        IF EXISTS ( SELECT  name
                    FROM    sysobjects WITH ( NOLOCK )
                    WHERE   name = 'AspNet_SqlCacheTablesForChangeNotification'
                            AND type = 'U' ) 
            IF EXISTS ( SELECT  name
                        FROM    sysobjects WITH ( TABLOCKX )
                        WHERE   name = 'AspNet_SqlCacheTablesForChangeNotification'
                                AND type = 'U' ) 
                DELETE  FROM dbo.AspNet_SqlCacheTablesForChangeNotification
                WHERE   tableName = @tableName 

         /* Remove the trigger */ 
        IF EXISTS ( SELECT  name
                    FROM    sysobjects WITH ( NOLOCK )
                    WHERE   name = @triggerName
                            AND type = 'TR' ) 
            IF EXISTS ( SELECT  name
                        FROM    sysobjects WITH ( TABLOCKX )
                        WHERE   name = @triggerName
                                AND type = 'TR' ) 
                EXEC('DROP TRIGGER ' + @fullTriggerName) 

        COMMIT TRAN
    END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCachePollingStoredProcedure]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCachePollingStoredProcedure]
AS 
    SELECT  tableName ,
            changeId
    FROM    dbo.AspNet_SqlCacheTablesForChangeNotification
    RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[ReplaceDocumentText]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ReplaceDocumentText]
    @find VARCHAR(500) ,
    @replace VARCHAR(500) ,
    @patfind VARCHAR(500) = ''
AS 
    BEGIN

        SELECT  @patfind = '%' + @find + '%'

        UPDATE  DailyArticles
        SET     ArticleText = STUFF(ArticleText,
                                    PATINDEX(@patfind, ArticleText),
                                    DATALENGTH(@find), @replace)
        WHERE   ArticleText LIKE @patfind

---- Study Guide
--
--        UPDATE  Documents
--        SET     GuideContent = STUFF(GuideContent,
--                                     PATINDEX(@patfind, GuideContent),
--                                     DATALENGTH(@find), @replace)
--        WHERE   GuideContent LIKE @patfind
--
-- --Pages
--
--        UPDATE  Page
--        SET     [Content] = STUFF([Content], PATINDEX(@patfind, [Content]),
--                                  DATALENGTH(@find), @replace)
--        WHERE   [Content] LIKE @patfind --AND Page.[folder] LIKE '%humanaction%'
--
---- Media URL
--		UPDATE  MediaAlternateFormat
--        SET     [URL] = STUFF([URL], PATINDEX(@patfind, [URL]),
--                                  DATALENGTH(@find), @replace)
--        WHERE   [URL] LIKE @patfind
--        
---- Product Descriptions
--		UPDATE  [AbleCommerce]..[ac_Products]
--        SET     [AbleCommerce]..[ac_Products].[Description] = STUFF([AbleCommerce]..[ac_Products].[Description], PATINDEX(@patfind, [AbleCommerce]..[ac_Products].[Description]),
--                                  DATALENGTH(@find), @replace)
--        WHERE   [AbleCommerce]..[ac_Products].[Description] LIKE @patfind        
--
    END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleUpdate]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyArticleUpdate]
    (
      @ArticleId INT ,
      @Title VARCHAR(255) ,
      @Description VARCHAR(1500) ,
      @ArticleText VARCHAR(MAX) ,
      @ShowArticle BIT ,
      @Featured BIT ,
      @Headline BIT ,
      @AuthorId INT = 0 ,
      @CoAuthorId INT = 0 ,
      --@DisplayOrder INT = 0,
      @PhotoHeight INT = 0 ,
      @PhotoURL VARCHAR(255) = NULL ,
      @DatePosted SMALLDATETIME = GETDATE ,
      @EditBy VARCHAR(75) = NULL
    )
AS 
    UPDATE  DailyArticles
    SET     Title = @Title ,
            [Description] = @Description ,
            ArticleText = @ArticleText ,
            DatePosted = @Dateposted ,
            ShowArticle = @ShowArticle ,
            Featured = @Featured ,
            Headline = @Headline ,
            CoAuthorId = @CoAuthorId ,
            AuthorId = @AuthorId ,
--            DisplayOrder = @DisplayOrder,
            PhotoURL = @PhotoURL ,
            PhotoHeight = @PhotoHeight ,
            EditDate = GETDATE() ,
            EditBy = @EditBy
    WHERE   ( ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesDeleteByArticleId]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DailyArticlesDeleteByArticleId] @ArticleId INT
AS 
    BEGIN
        DELETE  FROM DailyArticles
        WHERE   ArticleId = @ArticleId
    END
GO
/****** Object:  StoredProcedure [dbo].[spGetAuthorList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetAuthorList]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT DISTINCT
            DailyArticles.AuthorId ,
            da.authorLast ,
            0 AS monthnum ,
            '' AS month ,
            da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS AuthorName ,
            ( SELECT TOP 1
                        title
              FROM      DailyArticles a
              WHERE     a.AuthorId = DailyArticles.AuthorId
              ORDER BY  dateposted DESC
            ) AS title ,
            ( SELECT TOP 1
                        ArticleId
              FROM      DailyArticles a
              WHERE     a.AuthorId = DailyArticles.AuthorId
              ORDER BY  dateposted DESC
            ) AS ArticleId ,
            ( SELECT TOP 1
                        dateposted
              FROM      DailyArticles a
              WHERE     a.AuthorId = DailyArticles.AuthorId
              ORDER BY  dateposted DESC
            ) AS dateposted
    FROM    DailyArticles
            JOIN DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
    WHERE   ( ShowArticle = 1 )
    ORDER BY da.AuthorLast
GO
/****** Object:  StoredProcedure [dbo].[spAddNewArticle]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddNewArticle]
    (
      @Title VARCHAR(255) ,
      @Description TEXT ,
--@AuthorFirst varchar(50)='',
--@AuthorLast varchar(50)='',
      @ArticleText TEXT ,
      @ShowArticle BIT ,
      @AuthorId INT = 0 ,
      @CoAuthorId INT = 0 ,
      @DisplayOrder INT = 10 ,
      @PhotoHeight INT = 0 ,
      @PhotoURL VARCHAR(255) = NULL ,
      @EditBy VARCHAR(75) = NULL ,
      @DatePosted SMALLDATETIME = GETDATE
    )
AS 
    INSERT  INTO DailyArticles
            ( Title ,
              Description ,
              ArticleText ,
              ShowArticle ,
              DisplayOrder ,
              AuthorId ,
              CoAuthorId ,
              PhotoURL ,
              DatePosted ,
              PhotoHeight ,
              EditBy
            )
    VALUES  ( @title ,
              @description ,
              @ArticleText ,
              @ShowArticle ,
              @DisplayOrder ,
              @AuthorId ,
              @CoAuthorId ,
              @PhotoURL ,
              @DatePosted ,
              @PhotoHeight ,
              @EditBy
            )
    SELECT  @@Identity
GO
/****** Object:  StoredProcedure [dbo].[spUpdateArticle]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateArticle]
    (
      @ArticleId INT ,
      @Title VARCHAR(255) ,
      @Description VARCHAR(1500) ,
      @ArticleText VARCHAR(MAX) ,
      @ShowArticle BIT ,
      @AuthorId INT = 0 ,
      @CoAuthorId INT = 0 ,
      @DisplayOrder INT = 0 ,
      @PhotoHeight INT = 0 ,
      @PhotoURL VARCHAR(255) = NULL ,
      @DatePosted SMALLDATETIME = GETDATE ,
      @EditBy VARCHAR(75) = NULL
    )
AS 
    UPDATE  DailyArticles
    SET     Title = @Title ,
            [Description] = @Description ,
            ArticleText = @ArticleText ,
            DatePosted = @Dateposted ,
            ShowArticle = @ShowArticle ,
            CoAuthorId = @CoAuthorId ,
            AuthorId = @AuthorId ,
            DisplayOrder = @DisplayOrder ,
            PhotoURL = @PhotoURL ,
            PhotoHeight = @PhotoHeight ,
            EditDate = GETDATE() ,
            EditBy = @EditBy
    WHERE   ( ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleUpdateArticleOrder]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DailyArticleUpdateArticleOrder]
    (
      @ArticleId INT ,
      @DisplayOrder INT
    )
AS 
    UPDATE  DailyArticles
    SET     DisplayOrder = @DisplayOrder
    WHERE   ArticleId = @ArticleId
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesTopAuthors]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DocumentGetTopAuthors
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DailyArticlesTopAuthors]
AS 
    BEGIN
        SELECT TOP 60
                d.AuthorId ,
                da.AuthorLast ,
                COUNT(1) AS 'Count'
        FROM    [DailyArticles] d
                JOIN [DocumentAuthors] da ON d.[AuthorId] = da.[AuthorId]
                                             OR d.[CoAuthorId] = da.[AuthorId]
        GROUP BY d.AuthorId ,
                da.AuthorLast
        ORDER BY COUNT DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleAdd]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyArticleAdd]
    (
      @Title VARCHAR(255) ,
      @Description TEXT ,
--@AuthorFirst varchar(50)='',
--@AuthorLast varchar(50)='',
      @ArticleText TEXT ,
      @ShowArticle BIT ,
      @AuthorId INT = 0 ,
      @Featured BIT ,
      @Headline BIT ,
      @CoAuthorId INT = 0 ,
--@DisplayOrder int =10,
      @PhotoHeight INT = 0 ,
      @PhotoURL VARCHAR(255) = NULL ,
      @EditBy VARCHAR(75) = NULL ,
      @DatePosted SMALLDATETIME = GETDATE
    )
AS 
    INSERT  INTO DailyArticles
            ( Title ,
              Description ,
              ArticleText ,
              ShowArticle ,
              Featured ,
              Headline ,
              AuthorId ,
              CoAuthorId ,
              PhotoURL ,
              DatePosted ,
              PhotoHeight ,
              EditBy
            )
    VALUES  ( @title ,
              @description ,
              @ArticleText ,
              @ShowArticle ,
              @Featured ,
              @Headline ,
              @AuthorId ,
              @CoAuthorId ,
              @PhotoURL ,
              @DatePosted ,
              @PhotoHeight ,
              @EditBy
            )
    SELECT  @@Identity
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetTodaysSummary]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DailyArticlesGetTodaysSummary]
AS 
    BEGIN
	
        SET NOCOUNT ON ;
        SET CONCAT_NULL_YIELDS_NULL OFF 
    
    -- Get Headline Article

        SELECT  DailyArticles.ArticleId ,
                DailyArticles.ArticleId ,
                DailyArticles.Title ,
                DailyArticles.Description ,
                DailyArticles.AuthorId ,
                DailyArticles.DisplayOrder ,
                DailyArticles.Headline ,
                DailyArticles.Featured ,
                DailyArticles.CoAuthorId ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
                da2.authorFirst + ' ' + da2.authorMiddle + ' '
                + da2.authorLast AS CoAuthor ,
                DailyArticles.PhotoURL ,
                DailyArticles.DatePosted ,
                DailyArticles.PhotoHeight
        FROM    dbo.DailyArticles (NOLOCK)
                JOIN dbo.DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
                LEFT JOIN dbo.DocumentAuthors da2 ON da2.AuthorId = DailyArticles.CoAuthorId
        WHERE   ( DailyArticles.Featured = 1
                  OR Headline = 1
                )
                AND DailyArticles.ShowArticle = 1
        ORDER BY Headline DESC
    --CASE WHEN DailyArticles.Featured = 1 THEN Featured END,     
    --CASE WHEN DailyArticles.Headline = 1 THEN Headline END  
    
    
    END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetRecent]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DailyArticlesGetRecent]
AS 
    BEGIN

        SET NOCOUNT ON ;
        SET CONCAT_NULL_YIELDS_NULL OFF 

	-- Daily Articles
        SELECT TOP 7
                DailyArticles.ArticleId ,
                DailyArticles.Title ,
                da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS AuthorName ,
                DailyArticles.PhotoURL ,
                DailyArticles.AuthorId ,
                DailyArticles.PhotoHeight ,
                DatePosted
        FROM    DailyArticles WITH ( NOLOCK )
                INNER JOIN DocumentAuthors AS da ON da.AuthorId = DailyArticles.AuthorId
        WHERE   DailyArticles.ShowArticle = 1
                AND dbo.DailyArticles.Featured = 0
                AND dbo.DailyArticles.Headline = 0
        ORDER BY DailyArticles.DatePosted DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetFeed]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyArticlesGetFeed]
    @AuthorId INT = 0 ,
    @FullText BIT = 0
--@SortField varchar(50) = 'ORDER BY authorLast, dateposted DESC'
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    IF @AuthorId > 0 
        SELECT TOP 100
                ArticleId ,
                Title ,
                GUID ,
                Photo ,
                dateposted ,
                da.authorId ,
                description ,
                headline, featured,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS AuthorName
        FROM    DailyArticles
                JOIN DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
        WHERE   DailyArticles.ShowArticle = 1
                AND ( DailyArticles.AuthorId = @AuthorId
                      OR DailyArticles.CoAuthorId = @AuthorId
                    )
        ORDER BY DailyArticles.DatePosted DESC
        
    ELSE 
        IF @FullText = 1 
            SELECT TOP 5
                    ArticleId ,
                    Title ,
                    GUID ,
                    Photo ,
                    da.authorId ,
                    ArticleText ,
                    dateposted ,
                    description ,
                    headline, featured,
                    da.authorFirst + ' ' + da.authorMiddle + ' '
                    + da.authorLast AS AuthorName
            FROM    DailyArticles
                    JOIN DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
            WHERE   DailyArticles.ShowArticle = 1
                    AND ( DailyArticles.AuthorId = @AuthorId
                          OR DailyArticles.CoAuthorId = @AuthorId
                        )
            ORDER BY Headline DESC, DailyArticles.DatePosted DESC
    

        ELSE 
            SELECT TOP 30
                    ArticleId ,
                    Title ,
                    GUID ,
                    Photo ,
                    da.AuthorId ,
                    dateposted ,
                    description ,
                    headline, featured,
                    da.authorFirst + ' ' + da.authorMiddle + ' '
                    + da.authorLast AS AuthorName
            FROM    DailyArticles
                    JOIN DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
            WHERE   DailyArticles.ShowArticle = 1
            ORDER BY DailyArticles.DatePosted DESC
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetImageGallery]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyArticlesGetImageGallery]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  DatePosted ,
            title AS Title ,
            da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
            da.AuthorId ,
            'images/DailyArticleImages/' + CAST(ArticleId AS VARCHAR(MAX))
            + '.jpeg' AS ImageURL ,
            '/daily/' + CAST(ArticleId AS VARCHAR(MAX)) AS NavigateUrl ,
            DATENAME(month, DailyArticles.dateposted) AS Month
    FROM    dbo.DailyArticles
            JOIN dbo.DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId
    WHERE   DailyArticles.ShowArticle = 1
            --AND  LEN(PhotoURL) > 1
ORDER BY    Headline DESC ,
            [DatePosted] DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentAuthorsGetDetail]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentAuthorsGetDetail]
    @ArticleId INT = 0 ,
    @AuthorId INT = 0
AS 
    BEGIN

        SET CONCAT_NULL_YIELDS_NULL OFF 

        IF @ArticleId > 0 
            SELECT  DocumentAuthors.AuthorId ,
                    DocumentAuthors.AuthorFirst + '  '
                    + DocumentAuthors.AuthorMiddle + '  '
                    + DocumentAuthors.AuthorLast AS Author ,
                    Photo ,
                    BioText ,
                    ( SELECT TOP 1
                                ArticleId
                      FROM      DailyArticles
                      WHERE     AuthorId = DocumentAuthors.AuthorId
                      ORDER BY  ArticleId DESC
                    ) AS LatestArticleId
            FROM    DocumentAuthors
                    INNER JOIN DailyArticles ON DailyArticles.AuthorId = DocumentAuthors.AuthorId
                                                OR DailyArticles.CoAuthorId = DocumentAuthors.AuthorId
            WHERE   DailyArticles.ArticleId = @ArticleId

        ELSE 
            SELECT  AuthorId ,
                    DocumentAuthors.AuthorFirst + '  '
                    + DocumentAuthors.AuthorMiddle + '  '
                    + DocumentAuthors.AuthorLast AS Author ,
                    Photo ,
                    BioText
            FROM    DocumentAuthors
            WHERE   AuthorId = @AuthorId



    END
GO
/****** Object:  StoredProcedure [dbo].[ArticleGetDetails]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ArticleGetDetails] ( @ArticleId INT )
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  DailyArticles.ArticleId ,
            DailyArticles.GUID ,
            DailyArticles.Title ,
            DailyArticles.Description ,
            DailyArticles.ArticleText ,
            DailyArticles.ShowArticle ,
            DailyArticles.DatePosted ,
            da.AuthorFirst ,
            da.AuthorLast ,
            DailyArticles.AuthorId ,
            DailyArticles.CoAuthorId ,
            DailyArticles.DisplayOrder ,
            DailyArticles.CreatedDate ,
            da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS AuthorName ,
            ca.AuthorFirst + ' ' + ca.AuthorMiddle + ' ' + ca.AuthorLast AS CoAuthorName ,
            DailyArticles.PhotoURL ,
            DailyArticles.PhotoHeight ,
            [DailyArticles].[EditBy] ,
            [DailyArticles].[EditDate] ,
            ( SELECT TOP 1
                        daNext.ArticleId
              FROM      DailyArticles daNext
              WHERE     daNext.DatePosted > DailyArticles.DatePosted
                        AND daNext.ShowArticle = 1
              ORDER BY  daNext.DatePosted
            ) AS 'NextId' ,
            ( SELECT TOP 1
                        daprevious.ArticleId
              FROM      DailyArticles daprevious
              WHERE     daprevious.DatePosted < DailyArticles.DatePosted
                        AND daprevious.ShowArticle = 1
              ORDER BY  daprevious.DatePosted DESC
            ) AS 'PreviousId'
    FROM    dbo.DailyArticles
            LEFT OUTER JOIN dbo.DocumentAuthors AS da ON da.AuthorId = DailyArticles.AuthorId
            LEFT OUTER JOIN dbo.DocumentAuthors AS ca ON ca.AuthorId = DailyArticles.CoAuthorId
    WHERE   ( DailyArticles.ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[ReplaceGuideAuthor]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ReplaceGuideAuthor] 
	-- Add the parameters for the stored procedure here
    @OldAuthorId INT ,
    @NewAuthorId INT
AS 
    BEGIN

--select * from DocumentAuthors WHERE AuthorLast + AuthorFirst IN 
--(
--SELECT AuthorLast + AuthorFirst
--FROM DocumentAuthors
--GROUP BY AuthorLast + AuthorFirst
--HAVING ( COUNT(AuthorLast + AuthorFirst) > 1 )
--) oRDER By AuthorLast
	

	
        UPDATE  Documents
        SET     Author1 = @NewAuthorId
        WHERE   Author1 = @OldAuthorId

        UPDATE  Documents
        SET     Author2 = @NewAuthorId
        WHERE   Author2 = @OldAuthorId

        UPDATE  DailyArticles
        SET     AuthorId = @NewAuthorId
        WHERE   AuthorId = @OldAuthorId

        UPDATE  DailyArticles
        SET     CoAuthorId = @NewAuthorId
        WHERE   CoAuthorId = @OldAuthorId

        DELETE  FROM DocumentAuthors
        WHERE   ( AuthorId = @OldAuthorId )
                AND @OldAuthorId NOT IN ( SELECT    Author1
                                          FROM      Documents
                                          WHERE     Author1 = @OldAuthorId )

    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentAuthorsGetList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentAuthorsGetList]
    @ContentType VARCHAR(30) = NULL
AS 
    SET NOCOUNT ON ;
    SET CONCAT_NULL_YIELDS_NULL OFF 

    IF @ContentType = 'DailyArticles' 
        SELECT DISTINCT
                da.AuthorId ,
                AuthorLast ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
        FROM    DocumentAuthors da
                INNER JOIN DailyArticles ON DailyArticles.AuthorId = da.AuthorId
        WHERE   DailyArticles.ShowArticle = 1
        ORDER BY AuthorLast

    ELSE 
        IF @ContentType = 'Literature' 
            SELECT DISTINCT
                    da.AuthorId ,
                    AuthorLast ,
                    da.authorFirst + ' ' + da.authorMiddle + ' '
                    + da.authorLast AS 'AuthorName'
            FROM    DocumentAuthors da
                    INNER JOIN Documents ON da.AuthorId = Documents.Author1
            WHERE   Documents.Display = 1
            ORDER BY AuthorLast

        ELSE 
            IF @ContentType = 'Media' 
                SELECT DISTINCT
                        da.AuthorId ,
                        AuthorLast ,
                        da.authorFirst + ' ' + da.authorMiddle + ' '
                        + da.authorLast AS 'AuthorName'
                FROM    DocumentAuthors da
                        INNER JOIN Documents ON da.AuthorId = Documents.Author1
                        INNER JOIN MediaAlternateFormat ON MediaAlternateFormat.DocumentId = Documents.DocumentId
                WHERE   Documents.Display = 1
                ORDER BY AuthorLast

            ELSE 
                IF @ContentType = 'Quotes' 
                    SELECT DISTINCT
                            da.AuthorId ,
                            AuthorLast ,
                            da.authorFirst + ' ' + da.authorMiddle + ' '
                            + da.authorLast AS 'AuthorName'
                    FROM    DocumentAuthors da
                            INNER JOIN [Quotes] ON da.[AuthorId] = [Quotes].[AuthorId]
                    ORDER BY AuthorLast


                ELSE 
                    SELECT  da.AuthorId ,
                            AuthorLast ,
                            da.authorFirst + ' ' + da.authorMiddle + ' '
                            + da.authorLast AS 'AuthorName'
                    FROM    DocumentAuthors da
                    ORDER BY AuthorLast
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleGet]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyArticleGet] ( @ArticleId INT )
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  DailyArticles.ArticleId ,
            DailyArticles.GUID ,
            DailyArticles.Title ,
            DailyArticles.Description ,
            DailyArticles.ArticleText ,
            DailyArticles.Featured ,
            DailyArticles.Headline ,
            DailyArticles.ShowArticle ,
            DailyArticles.DatePosted ,
            da.AuthorFirst ,
            da.AuthorLast ,
            DailyArticles.AuthorId ,
            DailyArticles.CoAuthorId ,
            DailyArticles.DisplayOrder ,
            DailyArticles.CreatedDate ,
            da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS AuthorName ,
            ca.AuthorFirst + ' ' + ca.AuthorMiddle + ' ' + ca.AuthorLast AS CoAuthorName ,
            DailyArticles.PhotoURL ,
            DailyArticles.PhotoHeight ,
            [DailyArticles].[EditBy] ,
            [DailyArticles].[EditDate] ,
            ( SELECT TOP 1
                        daNext.ArticleId
              FROM      DailyArticles daNext
              WHERE     daNext.DatePosted > DailyArticles.DatePosted --AND daNext.ArticleId > DailyArticles.ArticleId
                        AND (daNext.ShowArticle = 1 OR daNext.Featured = 1 OR daNext.Headline = 1)
              ORDER BY  daNext.DatePosted, daNext.ArticleId
            ) AS 'NextId' ,
            ( SELECT TOP 1
                        daprevious.ArticleId
              FROM      DailyArticles daprevious
              WHERE     daprevious.DatePosted < DailyArticles.DatePosted -- AND daprevious.ArticleId < DailyArticles.ArticleId
                        AND (daprevious.ShowArticle = 1 OR daprevious.Featured = 1 OR daprevious.Headline = 1)
              ORDER BY  daprevious.DatePosted DESC, daprevious.ArticleId DESC
            ) AS 'PreviousId'
    FROM    dbo.DailyArticles
            LEFT OUTER JOIN dbo.DocumentAuthors AS da ON da.AuthorId = DailyArticles.AuthorId
            LEFT OUTER JOIN dbo.DocumentAuthors AS ca ON ca.AuthorId = DailyArticles.CoAuthorId
    WHERE   ( DailyArticles.ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[TagDelete]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagDelete]
    @tag VARCHAR(75) = NULL ,
    @user VARCHAR(100) = NULL ,
    @Identifier UNIQUEIDENTIFIER = NULL
AS 
    BEGIN

        IF @Identifier IS NOT NULL 
            BEGIN

                IF @tag IS NOT NULL 
                    DELETE  FROM TagMap
                    FROM    TagMap
                            INNER JOIN Tag ON TagMap.TagId = Tag.TagId
                    WHERE   ( Tag.Tag = @Tag )
                            AND [TagMap].[ObjectId] = @Identifier
                ELSE 
                    IF @user IS NOT NULL 
                        DELETE  FROM TagMap
                        FROM    TagMap
                                INNER JOIN Tag ON TagMap.TagId = Tag.TagId
                        WHERE   ( Tag.Tag = @Tag )
                                AND [TagMap].[ObjectId] = @Identifier
                                AND TagMap.TaggedBy = @user
                    ELSE 
                        DELETE  FROM TagMap
                        FROM    TagMap
                        WHERE   [TagMap].[ObjectId] = @Identifier

            END
        ELSE 
            IF @tag IS NOT NULL 
                DELETE  FROM TagMap
                FROM    TagMap
                        INNER JOIN Tag ON TagMap.TagId = Tag.TagId
                WHERE   ( Tag.Tag = @Tag )

            ELSE 
                DELETE  FROM TagMap
                WHERE   ( TaggedBy = @user )


    END
GO
/****** Object:  StoredProcedure [dbo].[TagAddNew]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagAddNew]
    @Tag VARCHAR(70) ,
    @Identifier UNIQUEIDENTIFIER ,
    @User VARCHAR(100)
AS 
    BEGIN
-- EXEC TagGetTopTags @TagCount = 2000
-- SELECT * FROM Tag JOIN TagMap ON Tag.TagId = TagMap.TagId WHERE TaggedBy <> 'MisesBot'
--delete FROM tagmap WHERE TaggedBy  = 'MisesBot'
--delete FROM [Tag] WHERE tagId NOT IN (SELECT tagid FROM tagmap)

        --IF @User = 'SearchEngineBot' 
        --    RETURN

        IF LEN(@Tag) < 2 
            RETURN

        SET @Tag = LTRIM(RTRIM(@Tag))
        SET @Tag = REPLACE(@Tag, 'Econmics', 'Economics')

        IF @Tag = 'mises' 
            SET @Tag = 'Ludwig von Mises'

        IF @Tag = 'Rothbard'
            OR @Tag = 'Murray N.  Rothbard'
            OR @Tag = 'Murray Rothbard' 
            SET @Tag = 'Murray N. Rothbard'



        IF @TAG LIKE '%http://%'
            OR @TAG LIKE '%@%'
            OR @Tag LIKE '%PDF%'
            OR @Tag LIKE '%Acrobat %'
            OR @Tag LIKE '%Palm Download%'
            OR @Tag = 'audio'
            OR @Tag LIKE '%Distiller%'
            OR @Tag = 'mises.org'
            OR @Tag = 'and th'
            OR @Tag = 'Speech'
            OR @Tag = 'Unknown'
            OR @Tag LIKE '%mises institute%'
            OR @Tag = 'PrecisionScan'
            OR @Tag LIKE '%.qxd%'
            OR @Tag LIKE '%phendimetrazine%' 
            RETURN

-- Check for cursewords
        IF EXISTS ( SELECT  *
                    FROM    MisesCommunity..cs_Censorship
                    WHERE   @Tag LIKE '%' + word + '%' ) 
            RETURN

-- Get Tag Id
        DECLARE @TagId INT

        SET @TagId = ( SELECT   TagId
                       FROM     Tag
                       WHERE    Tag = @Tag
                     )

-- Check for duplicates
        IF EXISTS ( SELECT  *
                    FROM    TagMap
                    WHERE   TagId = @TagId
                            AND ObjectId = @Identifier
                            AND TaggedBy = @User ) 
            RETURN


-- Add New Tag if Missing
        IF @TagId IS NULL 
            BEGIN
                INSERT  INTO Tag
                        ( Tag )
                VALUES  ( @Tag )

                SET @TagID = SCOPE_IDENTITY()
            END

-- Insert tag mapping
        INSERT  INTO TagMap
                ( TagId, ObjectId, TaggedBy )
        VALUES  ( @TagID, @Identifier, @User )

    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetRelatedTags]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	 Get Related Tags
-- =============================================
CREATE PROCEDURE [dbo].[TagGetRelatedTags] @Tag VARCHAR(70)
AS 
    BEGIN   
        DECLARE @TagId INT
        SET @TagId = ( SELECT TOP 1
                                TagId
                       FROM     Tag
                       WHERE    Tag = @Tag
                     )
                
        SELECT TOP 30
                Tag.Tag ,
                COUNT(TagMap.TagId) AS 'Count'
        FROM    TagMap
                INNER JOIN Tag ON TagMap.TagId = Tag.TagId
        WHERE   [TagMap].TagId <> @TagId
                AND ( TagMap.TagId IN (
                      SELECT DISTINCT
                                TagId
                      FROM      TagMap
                      WHERE     ( ObjectId IN ( SELECT  ObjectId
                                                FROM    TagMap
                                                WHERE   ( TagId = @TagId ) ) ) ) )
        GROUP BY TagMap.TagId ,
                Tag.Tag
        ORDER BY 'Count' DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetAnonymousTaggers]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetAnonymousTaggers]
AS 
    BEGIN
	
        SELECT TOP 3000
                TaggedBy ,
                Tag ,
                TagMap.TaggedDate
        FROM    TagMap
                JOIN Tag ON TagMap.TagId = Tag.TagId
        WHERE   TaggedBy NOT IN ( 'MisesBot', 'HeroicLife', 'jtucker' )
        ORDER BY TagMap.TaggedDate DESC
	
    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetTopTags]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetTopTags] @TagCount INT = 60
AS 
    BEGIN

-- SELECT * FROM tag JOIN [TagMap] ON [Tag].[TagId] = [TagMap].[TagId] WHERE [TaggedBy] <> 'MisesBot'

        SET ROWCOUNT @TagCount

        SELECT  [Tag].TagId ,
                tag ,
                COUNT(TagMap.TagId) AS Count
        FROM    [Tag] WITH ( NOLOCK )
                JOIN [TagMap] ON [Tag].[TagId] = [TagMap].[TagId]
        GROUP BY [Tag].[TagId] ,
                Tag.[Tag]
        ORDER BY COUNT(TagMap.TagId) DESC

    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetDocumentTags]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetDocumentTags]
    @Identifier UNIQUEIDENTIFIER
AS 
    BEGIN
	
        SELECT  Tag.Tag ,
                COUNT(Tag.TagId) AS 'Count' ,
                Tag.TagId
        FROM    Tag WITH ( NOLOCK )
                INNER JOIN TagMap WITH ( NOLOCK ) ON Tag.TagId = TagMap.TagId
        WHERE   ( TagMap.ObjectId = @Identifier )
                AND [TaggedBy] <> 'SearchEngineBot'
        GROUP BY Tag.TagId ,
                Tag.Tag

    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetStats]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetStats]
AS 
    BEGIN
	
        SELECT  ( SELECT    COUNT(*)
                  FROM      tag
                ) AS UniqueTags ,
                ( SELECT    COUNT(*)
                  FROM      TagMap
                  WHERE     TaggedBy = 'MisesBot'
                ) AS BotTags ,
                ( SELECT    COUNT(DISTINCT ObjectId)
                  FROM      TagMap
                ) AS TotalTaggedDocs ,
                ( SELECT    COUNT(DISTINCT ObjectId)
                  FROM      TagMap
                  WHERE     TaggedBy = 'MisesBot'
                ) AS BotTaggedDocs ,
                ( SELECT    COUNT(DISTINCT ObjectId)
                  FROM      TagMap
                  WHERE     TaggedBy <> 'MisesBot'
                ) AS UserTaggedDocs ,
                ( SELECT    COUNT(*)
                  FROM      TagMap
                  WHERE     TaggedDate > DATEADD(hour, -24, GETDATE())
                ) AS TodaysTagging ,
                ( SELECT    COUNT(DISTINCT TagId)
                  FROM      TagMap
                  WHERE     TaggedDate > DATEADD(hour, -24, GETDATE())
                ) AS TodaysTags

        SELECT TOP 10
                TaggedBy ,
                COUNT(TaggedBy) AS Count
        FROM    TagMap
        WHERE   TaggedDate > DATEADD(hour, -24, GETDATE())-- AND TaggedBy NOT LIKE '%.%'
GROUP BY        TaggedBy
        ORDER BY COUNT DESC

        SELECT TOP 10
                TaggedBy ,
                COUNT(TaggedBy) AS Count
        FROM    TagMap 
--WHERE TaggedBy NOT LIKE '%.%'
GROUP BY        TaggedBy
        ORDER BY COUNT DESC


        SELECT TOP 25
                Tag ,
                COUNT(Tag.TagId) AS 'Count'
        FROM    TagMap
                JOIN Tag ON Tag.TagId = TagMap.TagId
        WHERE   TaggedDate > DATEADD(hour, -24, GETDATE())
        GROUP BY Tag
        ORDER BY COUNT DESC

    END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetTopTagged]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaGetTopTagged]
	-- Add the parameters for the stored procedure here
    @p1 INT = 0, @p2 INT = 0
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
        SET CONCAT_NULL_YIELDS_NULL OFF;

        SELECT TOP 10
                ObjectId ,
                Documents.DocumentId ,
                Documents.Title ,
                Documents.Author1 ,
                Documents.CreateDate ,
                COUNT(1) AS 'Count' ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
        FROM    dbo.Documents
                INNER JOIN dbo.TagMap ON dbo.TagMap.ObjectId = dbo.Documents.GUID
                INNER JOIN dbo.MediaAlternateFormat ON dbo.Documents.DocumentId = dbo.MediaAlternateFormat.DocumentId
                INNER JOIN dbo.DocumentAuthors da ON da.AuthorId = dbo.Documents.Author1
        WHERE   Display = 1
                AND dbo.Documents.FullText = 0
                AND dbo.MediaAlternateFormat.MediaTypeId IN ( 1, 2, 8, 12, 14 )
                AND TaggedDate > ( GETDATE() - 30 )
        GROUP BY ObjectId ,
                Documents.DocumentId ,
                Documents.Title ,
                Documents.Author1 ,
                Documents.CreateDate ,
                da.authorFirst ,
                da.authorMiddle ,
                da.authorLast
        ORDER BY COUNT DESC


    END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetMisesLive]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetMisesLive]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  sg.DocumentId ,
            sg.Title ,
            sg.metaDescription AS Description ,
            media.URL ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            sg.Author1 AS AuthorId ,
            sg.CreateDate ,
            CAST(( sg.Length / 60 ) AS INT) AS Duration ,
            'TODO' AS Topics ,
            CASE WHEN sg.metaImage IS NULL THEN 0
                 WHEN sg.metaImage IS NOT NULL THEN 1
            END AS HasImage ,
            CASE WHEN sg.metaImage IS NULL THEN 'none'
                 WHEN sg.metaImage IS NOT NULL THEN 'inline'
            END AS Display
    FROM    dbo.Documents sg
            JOIN dbo.MediaAlternateFormat media ON media.DocumentId = sg.DocumentId
            JOIN dbo.DocumentAuthors ON sg.Author1 = DocumentAuthors.AuthorId
    WHERE   sg.Source = 'MisesLive'
    ORDER BY media.CreateDate DESC ,
            media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetLatestVideo]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetLatestVideo]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT TOP 10
            sg.DocumentId ,
            dbo.MediaCategory.Category + ' >> ' + sg.Title AS Title ,
            sg.PubInfo AS Description ,
            media.URL ,
            media.MediaId ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            sg.Author1 AS AuthorId ,
            sg.CreateDate ,
            CASE WHEN sg.metaImage IS NULL THEN 0
                 WHEN sg.metaImage IS NOT NULL THEN 1
            END AS HasImage ,
            CASE WHEN sg.metaImage IS NULL THEN 'none'
                 WHEN sg.metaImage IS NOT NULL THEN 'inline'
            END AS Display
    FROM    dbo.Documents sg
            JOIN dbo.MediaAlternateFormat media ON media.DocumentId = sg.DocumentId
            JOIN dbo.DocumentAuthors ON sg.Author1 = DocumentAuthors.AuthorId
            JOIN dbo.MediaCategory ON sg.CategoryId = dbo.MediaCategory.CategoryId
    WHERE   media.MediaTypeId = 2
    ORDER BY media.CreateDate DESC ,
            media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetLatestAudio]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetLatestAudio]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT TOP 10
            sg.DocumentId ,
            dbo.MediaCategory.Category + ' >> ' + sg.Title AS Title ,
            sg.PubInfo AS Description ,
            media.URL ,
            media.MediaId ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            sg.Author1 AS AuthorId ,
            sg.CreateDate ,
            CASE WHEN sg.metaImage IS NULL THEN 0
                 WHEN sg.metaImage IS NOT NULL THEN 1
            END AS HasImage ,
            CASE WHEN sg.metaImage IS NULL THEN 'none'
                 WHEN sg.metaImage IS NOT NULL THEN 'inline'
            END AS Display
    FROM    dbo.Documents sg
            JOIN dbo.MediaAlternateFormat media ON media.DocumentId = sg.DocumentId
            JOIN dbo.DocumentAuthors ON sg.Author1 = DocumentAuthors.AuthorId
            JOIN dbo.MediaCategory ON sg.CategoryId = dbo.MediaCategory.CategoryId
    WHERE   media.MediaTypeId = 1
    ORDER BY media.CreateDate DESC ,
            media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetAuthors]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DocumentGetTopAuthors
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaGetAuthors]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    BEGIN
        SELECT 
                d.Author1 AS AuthorId ,
                da.AuthorLast ,
                da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
                COUNT(1) AS 'Count'
        FROM    Documents d
                JOIN [DocumentAuthors] da ON d.[Author1] = da.[AuthorId]
                                             OR d.[Author2] = da.[AuthorId]
                JOIN MediaAlternateFormat ON MediaAlternateFormat.DocumentId = d.DocumentId
        WHERE   MediaAlternateFormat.MediaTypeId IN ( 1, 2, 8 )
        GROUP BY d.Author1 ,
                da.AuthorLast ,
                da.AuthorFirst ,
                da.AuthorMiddle
        ORDER BY COUNT DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[MediaAddMetadata]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaAddMetadata]
    @DocumentId INT ,
    @MediaId INT ,
    @filesize BIGINT ,
    @duration DECIMAL = 0 ,
    @metaKeywords VARCHAR(500) ,
    @metaDescription VARCHAR(500) ,
    @metaImage IMAGE = NULL
AS 
    BEGIN

        UPDATE  MediaAlternateFormat
        SET     fileSize = @fileSize ,
                duration = @duration
        WHERE   MediaId = @mediaId

        UPDATE  Documents
        SET     metaDescription = @metaDescription ,
                metaKeywords = @metaKeywords
        WHERE   ( DocumentId = @DocumentId )

        IF @metaImage IS NOT NULL 
            BEGIN

                UPDATE  Documents
                SET     metaImage = @metaImage
                WHERE   ( DocumentId = @DocumentId )	

                UPDATE  MediaCategory
                SET     CategoryImage = 'http://mises.org/Controls/Media/DocumentImage.ashx?Id='
                        + CAST(@DocumentID AS VARCHAR(10))
                WHERE   CategoryId IN ( SELECT  CategoryId
                                        FROM    Documents
                                        WHERE   DocumentId = @DocumentId )
                        AND ( MediaCategory.CategoryImage IS NULL
                              OR MediaCategory.CategoryImage = ''
                            )

            END

    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentTopAuthors]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentTopAuthors]
AS 
    BEGIN
        SET NOCOUNT ON ;

        SET CONCAT_NULL_YIELDS_NULL OFF 

        SELECT TOP 30
                AuthorId ,
                AuthorLast ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName' ,
                COUNT(1) AS 'Count'
        FROM    [Documents] d
                JOIN [DocumentAuthors] da ON d.[Author1] = da.[AuthorId]
                                             OR d.[Author2] = da.[AuthorId]
        GROUP BY [AuthorId] ,
                [AuthorLast] ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast
        ORDER BY COUNT DESC

    END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetContributorList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetContributorList]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT  DocumentAuthors.AuthorId ,
            DocumentAuthors.AuthorFirst AS FirstName ,
            DocumentAuthors.AuthorMiddle AS MiddleName ,
            DocumentAuthors.AuthorLast AS LastName ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author
    FROM    dbo.DocumentAuthors
    WHERE   DocumentAuthors.AuthorId IN (
            SELECT  Documents.Author1
            FROM    dbo.Documents
                    JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
            WHERE   media.MediaTypeId IN ( 1, 2 ) )
    ORDER BY DocumentAuthors.AuthorLast
GO
/****** Object:  StoredProcedure [dbo].[MediaTopAuthors]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DocumentGetTopAuthors
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaTopAuthors]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    BEGIN
        SELECT TOP 60
                d.Author1 AS AuthorId ,
                da.AuthorLast ,
                da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
                COUNT(1) AS 'Count'
        FROM    Documents d
                JOIN [DocumentAuthors] da ON d.[Author1] = da.[AuthorId]
                                             OR d.[Author2] = da.[AuthorId]
                JOIN MediaAlternateFormat ON MediaAlternateFormat.DocumentId = d.DocumentId
        WHERE   MediaAlternateFormat.MediaTypeId IN ( 1, 2, 8 )
        GROUP BY d.Author1 ,
                da.AuthorLast ,
                da.AuthorFirst ,
                da.AuthorMiddle
        ORDER BY COUNT DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetLatest]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetLatest]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SELECT TOP 5
            sg.DocumentId ,
            dbo.MediaCategory.Category + ' >> ' + sg.Title AS Title ,
            sg.PubInfo AS Description ,
            media.URL ,
            media.MediaId ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            sg.Author1 AS AuthorId ,
            sg.CreateDate ,
            CASE WHEN sg.metaImage IS NULL THEN 0
                 WHEN sg.metaImage IS NOT NULL THEN 1
            END AS HasImage ,
            CASE WHEN sg.metaImage IS NULL THEN 'none'
                 WHEN sg.metaImage IS NOT NULL THEN 'inline'
            END AS Display
    FROM    dbo.Documents sg
            JOIN dbo.MediaAlternateFormat media ON media.DocumentId = sg.DocumentId
            JOIN dbo.DocumentAuthors ON sg.Author1 = DocumentAuthors.AuthorId
            JOIN dbo.MediaCategory ON sg.CategoryId = dbo.MediaCategory.CategoryId
    WHERE   media.MediaTypeId IN ( 1, 2, 8 )
    ORDER BY media.CreateDate DESC ,
            media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentsGetImage]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentsGetImage] @DocumentId INT
AS 
    BEGIN
        SELECT  metaImage
        FROM    dbo.Documents WITH ( NOLOCK )
        WHERE   DocumentId = @DocumentId
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentsGetBiographies]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentsGetBiographies]
AS 
    BEGIN
        SELECT  Title ,
                Documents.DocumentId
        FROM    Documents
                INNER JOIN [DocumentSubjectLink] dsl ON dsl.[DocumentId] = documents.[DocumentId]
        WHERE   SubjectId = 135
    END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetSubCategories]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MediaGetSubCategories] @CategoryId INT = 0
AS 
    BEGIN
        SELECT  MediaCategory.CategoryId ,
                MediaCategory.Category ,
                MediaCategory.Description ,
                MediaCategory.CategoryImage ,
                ISNULL(( SELECT TOP 1
                                Documents.CreateDate
                         FROM   dbo.Documents
                         WHERE  Documents.CategoryId = MediaCategory.CategoryId
                         ORDER BY Documents.CreateDate DESC
                       ), '1/1/2050') AS EditDate
        FROM    dbo.MediaCategory
        WHERE   MediaCategory.ParentCategory = @CategoryId
        ORDER BY SortOrder,Category
    END
GO
/****** Object:  StoredProcedure [dbo].[DocumentSave]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DocumentSave]
    @DocumentId INT = 0 ,
    @Display BIT ,
    @Featured BIT ,
    @Title VARCHAR(255) ,
    @Author1 INT ,
    @Author2 INT ,
    @PubInfo VARCHAR(8000) ,
    @FullText BIT ,
    @Source VARCHAR(100) ,
    @GuideContent VARCHAR(MAX) ,
    @Length INT = 0 ,
    @CategoryId INT ,
    @ParentCategoryId INT ,
    @GrandParentCategoryId INT ,
    @Description VARCHAR(max) ,
    @Keywords VARCHAR(350) ,
    @Identifier UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000'
AS 
    IF @Identifier = '00000000-0000-0000-0000-000000000000' 
        SET @Identifier = NEWID()


    IF @DocumentId > 0 -- Addding New Item
        BEGIN

            UPDATE  Documents
            SET     Display = @Display ,
                    Title = @Title ,
                    Featured = @Featured ,
                    Author1 = @Author1 ,
                    Author2 = @Author2 ,
                    PubInfo = @PubInfo ,
                    [FullText] = @FullText ,
                    Source = @Source ,
                    GuideContent = @GuideContent ,
                    CategoryId = @CategoryId ,
                    metaDescription = @Description ,
                    metaKeywords = @Keywords ,
                    EditDate = GETDATE()
            WHERE   DocumentId = @DocumentId

            DELETE  FROM DocumentSubjectLink
            WHERE   DocumentId = @DocumentId

        END

    ELSE 
        BEGIN

            INSERT  INTO Documents
                    ( [GUID] ,
                      Display ,
                      Featured ,
                      Title ,
                      Author1 ,
                      Author2 ,
                      PubInfo ,
                      [FullText] ,
                      Source ,
                      GuideContent ,
                      CategoryId ,
                      metaDescription ,
                      metaKeywords
                    )
            VALUES  ( @Identifier ,
                      @Display ,
                      @Featured ,
                      @Title ,
                      @Author1 ,
                      @Author2 ,
                      @PubInfo ,
                      @FullText ,
                      @Source ,
                      @GuideContent ,
                      @CategoryId ,
                      @Description ,
                      @Keywords
                    )

            SET @DocumentId = ( SELECT  SCOPE_IDENTITY()
                              )

        END

    IF ( @ParentCategoryId > 0 )
        AND NOT ( ( @CategoryId = @ParentCategoryId )
                  OR ( @ParentCategoryId = @GrandParentCategoryId )
                  OR ( @CategoryId = @GrandParentCategoryId )
                  OR ( @CategoryId = 0 )
                ) 
        BEGIN
            UPDATE  dbo.MediaCategory
            SET     ParentCategory = @ParentCategoryId
            WHERE   CategoryId = @CategoryId ;	

            IF @GrandParentCategoryId > 0 
                BEGIN
                    UPDATE  dbo.MediaCategory
                    SET     ParentCategory = @GrandParentCategoryId
                    WHERE   CategoryId = @ParentCategoryId ;			
                END
        END

    SELECT  @DocumentId
GO
/****** Object:  StoredProcedure [dbo].[DocumentDelete]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DocumentDelete] @DocumentId INT
AS 
    DELETE  FROM MediaAlternateFormat
    WHERE   DocumentId = @DocumentId
    DELETE  FROM DocumentSubjectLink
    WHERE   DocumentId = @DocumentId
    DELETE  FROM Documents
    WHERE   DocumentId = @DocumentId
GO
/****** Object:  StoredProcedure [dbo].[_JLStoDocuments]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stephen W. Carson, JLStoDocuments>
-- Create date: <August 26, 2007>
-- Description:	<Copies info from the jls table into the Documents structure>
-- =============================================
CREATE PROCEDURE [dbo].[_JLStoDocuments] @last_id INT = 0
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

        INSERT  INTO Documents
                ( Display ,
                  oldMediaTypeId ,
                  Source ,
                  Title ,
                  Author1 ,
                  Author2 ,
                  PubInfo ,
                  oldURL
                )
                SELECT  0 ,
                        3 ,
                        'JLS' ,
                        '"' + j.title + '"' ,
                        a1.AuthorId ,
                        a2.AuthorId ,
                        '<em>Journal of Libertarian Studies</em>, '
                        + CAST(j.volume AS VARCHAR(10)) + '('
                        + CAST(j.number AS VARCHAR(10)) + ').' ,
                        'http://www.mises.org/journals/jls/'
                        + CAST(j.volume AS VARCHAR(10)) + '_'
                        + CAST(j.number AS VARCHAR(10)) + '/'
                        + CAST(j.volume AS VARCHAR(10)) + '_'
                        + CAST(j.number AS VARCHAR(10)) + '_'
                        + CAST(j.articleNum AS VARCHAR(10)) + '.pdf'
                FROM    jls j
                        LEFT OUTER JOIN DocumentAuthors a1 ON j.authorFirst1 = a1.AuthorFirst
                                                              AND j.authorLast1 = a1.AuthorLast
                        LEFT OUTER JOIN DocumentAuthors a2 ON j.authorFirst2 = a2.AuthorFirst
                                                              AND j.authorLast2 = a2.AuthorLast
                WHERE   j.volume = 20
                        AND j.number = 2
                        AND j.articleNum = 1
                ORDER BY j.volume ,
                        j.number ,
                        j.articleNum

        SET @last_id = SCOPE_IDENTITY()

        INSERT  INTO MediaAlternateFormat
                ( DocumentID ,
                  MediaTypeID ,
                  URL
                )
                SELECT  @last_id ,
                        3 ,
                        oldURL
                FROM    Documents
                WHERE   DocumentID = @last_id
    END
GO
/****** Object:  StoredProcedure [dbo].[spGetQuotes]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetQuotes]
    @Subject VARCHAR(50) = NULL ,
    @AuthorId INT = 0 ,
    @Source VARCHAR(30) = NULL ,
    @SearchQuery VARCHAR(50) = NULL
AS 
    IF LEN(@Subject) > 0 
        SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                [Quote] ,
                [Source] ,
                [Page] ,
                [Subject]
        FROM    [Quotes]
                JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
        WHERE   Subject LIKE '%' + @Subject + '%'
 

    ELSE 
        IF @AuthorId > 0 
            SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                    [Quote] ,
                    [Source] ,
                    [Page] ,
                    [Subject]
            FROM    [Quotes]
                    JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
            WHERE   Quotes.AuthorId = @AuthorId



        ELSE 
            IF LEN(@SOURCE) > 0 
                SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                        [Quote] ,
                        [Source] ,
                        [Page] ,
                        [Subject]
                FROM    [Quotes]
                        JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
                WHERE   Source LIKE '%' + @Source + '%'

            ELSE 
                IF LEN(@SearchQuery) > 0 
                    SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                            [Quote] ,
                            [Source] ,
                            [Page] ,
                            [Subject]
                    FROM    [Quotes]
                            JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
                    WHERE   Author LIKE '%' + @SearchQuery + '%'
                            OR Quote LIKE '%' + @SearchQuery + '%'

                ELSE 
                    SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                            [Quote] ,
                            [Source] ,
                            [Page] ,
                            [Subject]
                    FROM    [Quotes]
                            JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
GO
/****** Object:  StoredProcedure [dbo].[zzzspGetHomepageItems]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zzzspGetHomepageItems]
    @SubjectId INT = 0 ,
    @AuthorId INT = 0 ,
    @MediaType INT = 0 ,
    @Source VARCHAR(30) = NULL ,
    @SearchQuery VARCHAR(50) = NULL
AS 
    IF @SubjectId > 0 
        BEGIN
            SELECT  HomepageItems.Itemid ,
                    Title ,
                    PubInfo ,
                    URL ,
                    Source ,
                    FullText ,
                    AuthorFirst + '  ' + AuthorLast AS Author ,
                    Author1 ,
                    Author2 ,
                    ItemOrder ,
                    MediaType ,
                    HomepageItems.MediaTypeId ,
                    MediaType ,
                    MediaIconPath ,
                    [HomepageItems].[CreateDate]
            FROM    HomepageItems
                    LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = HomepageItems.MediaTypeId
                    LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = HomepageItems.Author1
                    LEFT JOIN DocumentSubjectLink ON DocumentSubjectLink.DocumentId = HomepageItems.Itemid
            WHERE   SubjectId = @SubjectId
            ORDER BY ItemOrder ,
                    CreateDate DESC ,
                    HomepageItems.Itemid DESC
--HomepageItems.HomepageItems.Itemid DESC

            SELECT  Subject
            FROM    StudyGuideSubject
            WHERE   SubjectId = @SubjectId

            SELECT TOP 10
                    Subject ,
                    SubjectID
            FROM    StudyGuideSubject
            WHERE   CategoryId = ( SELECT   CategoryId
                                   FROM     StudyGuideSubject
                                   WHERE    SubjectId = @SubjectId
                                 )

        END
    ELSE 
        IF @AuthorId > 0 
            BEGIN
                SELECT  HomepageItems.Itemid ,
                        Title ,
                        PubInfo ,
                        URL ,
                        Source ,
                        FullText ,
                        AuthorFirst + '  ' + AuthorLast AS Author ,
                        Author1 ,
                        Author2 ,
                        ItemOrder ,
                        MediaType ,
                        HomepageItems.MediaTypeId ,
                        MediaType ,
                        MediaIconPath ,
                        [HomepageItems].[CreateDate]
                FROM    HomepageItems
                        LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = HomepageItems.MediaTypeId
                        LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = HomepageItems.Author1
                WHERE   ( Author1 = @AuthorId
                          OR Author2 = @AuthorId
                        )
                ORDER BY ItemOrder ,
                        CreateDate DESC ,
                        HomepageItems.Itemid DESC

                SELECT  AuthorFirst + '  ' + AuthorLast AS Author
                FROM    DocumentAuthors
                WHERE   DocumentAuthors.AuthorId = @AuthorId

            END
        ELSE 
            IF @MediaType > 0 
                SELECT  HomepageItems.Itemid ,
                        Title ,
                        PubInfo ,
                        URL ,
                        Source ,
                        FullText ,
                        AuthorFirst + '  ' + AuthorLast AS Author ,
                        Author1 ,
                        Author2 ,
                        ItemOrder ,
                        MediaType ,
                        HomepageItems.MediaTypeId ,
                        MediaType ,
                        MediaIconPath ,
                        [HomepageItems].[CreateDate]
                FROM    HomepageItems
                        LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = HomepageItems.MediaTypeId
                        LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = HomepageItems.Author1
                WHERE   HomepageItems.MediaTypeId = @MediaType
                ORDER BY ItemOrder ,
                        CreateDate DESC ,
                        HomepageItems.Itemid DESC

            ELSE 
                IF LEN(@SOURCE) > 0 
                    SELECT  HomepageItems.Itemid ,
                            Title ,
                            PubInfo ,
                            URL ,
                            Source ,
                            FullText ,
                            AuthorFirst + '  ' + AuthorLast AS Author ,
                            Author1 ,
                            Author2 ,
                            ItemOrder ,
                            MediaType ,
                            HomepageItems.MediaTypeId ,
                            MediaType ,
                            MediaIconPath ,
                            [HomepageItems].[CreateDate]
                    FROM    HomepageItems
                            LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = HomepageItems.MediaTypeId
                            LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = HomepageItems.Author1
                    WHERE   Source = @SOURCE
                    ORDER BY ItemOrder ,
                            CreateDate DESC ,
                            HomepageItems.Itemid DESC


                ELSE 
                    IF LEN(@SearchQuery) > 0 
                        SELECT  HomepageItems.Itemid ,
                                Title ,
                                PubInfo ,
                                URL ,
                                Source ,
                                FullText ,
                                AuthorFirst + '  ' + AuthorLast AS Author ,
                                Author1 ,
                                Author2 ,
                                ItemOrder ,
                                MediaType ,
                                HomepageItems.MediaTypeId ,
                                MediaType ,
                                MediaIconPath ,
                                [HomepageItems].[CreateDate]
                        FROM    HomepageItems
                                LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = HomepageItems.MediaTypeId
                                LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = HomepageItems.Author1
                        WHERE   Title LIKE '%' + @SearchQuery + '%'
                                OR AuthorFirst LIKE '%' + @SearchQuery + '%'
                                OR AuthorLast LIKE '%' + @SearchQuery + '%'
                                OR PubInfo LIKE '%' + @SearchQuery + '%'
                        ORDER BY ItemOrder ,
                                CreateDate DESC ,
                                HomepageItems.Itemid DESC

                    ELSE 
                        SELECT  HomepageItems.Itemid ,
                                Title ,
                                PubInfo ,
                                URL ,
                                Source ,
                                FullText ,
                                AuthorFirst + '  ' + AuthorLast AS Author ,
                                Author1 ,
                                Author2 ,
                                ItemOrder ,
                                MediaType ,
                                HomepageItems.MediaTypeId ,
                                MediaType ,
                                MediaIconPath ,
                                [HomepageItems].[CreateDate]
                        FROM    HomepageItems
                                LEFT JOIN DocumentMediaType ON DocumentMediaType.MediaTypeId = HomepageItems.MediaTypeId
                                LEFT JOIN DocumentAuthors ON DocumentAuthors.AuthorId = HomepageItems.Author1
                        ORDER BY ItemOrder ,
                                CreateDate DESC ,
                                HomepageItems.Itemid DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentAuthorsAdd]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentAuthorsAdd] 
	-- Add the parameters for the stored procedure here
    @AuthorFirst VARCHAR(255) ,
    @AuthorMiddle VARCHAR(255) ,
    @AuthorLast VARCHAR(255)
AS 
    BEGIN
	        
        INSERT  INTO DocumentAuthors
                ( AuthorFirst ,
                  AuthorMiddle ,
                  AuthorLast
                )
        VALUES  ( @AuthorFirst ,
                  @AuthorMiddle ,
                  @AuthorLast
                )
    END
GO
/****** Object:  StoredProcedure [dbo].[spGetRandomQuote]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetRandomQuote]
AS -- Random Quote

    DECLARE @Quote INT
    DECLARE @rand_num INT
    DECLARE @num_recs INT

-- Get Random Number from number of records
    SELECT  @num_recs = COUNT(*)
    FROM    Quotes

-- Select a random number between 1 and the number
		-- of records in our table

		-- Rounding error corrected by Finn Gundersen; old version commented below
		-- set @rand_num = Round(((@num_recs - 1) * Rand() + 1), 0)
    SET @rand_num = ROUND(@num_recs * RAND() + 1, 0, 1)

		-- Create a local, static, read-only, scrollable cursor
		-- Needs scrollable to use fetch absolute, otherwise we
		-- would use forward-only
    DECLARE #mycursor CURSOR scroll static read_only
    FOR SELECT  QuoteId
    FROM    Quotes

    OPEN #mycursor
    FETCH ABSOLUTE @rand_num FROM #mycursor INTO @Quote
    CLOSE #mycursor
    DEALLOCATE #mycursor

		-- Select the columns from the table joined for the key
		-- selected from the cursor

    SELECT TOP 1
            Quote ,
            AuthorFirst + ' ' + AuthorLast ,
            Source ,
            Subject ,
            Quotes.AuthorId
    FROM    Quotes
            JOIN DocumentAuthors ON DocumentAuthors.AuthorId = Quotes.AuthorId
    WHERE   QuoteId = @Quote
GO
/****** Object:  StoredProcedure [dbo].[spGetAllQuotes]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllQuotes]
    @Subject VARCHAR(50) = NULL ,
    @AuthorId INT = 0 ,
    @Source VARCHAR(30) = NULL ,
    @SearchQuery VARCHAR(50) = NULL
AS 
    IF LEN(@Subject) > 0 
        SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                [Quote] ,
                [Source] ,
                [Page] ,
                [Subject]
        FROM    Quotes
                JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
        WHERE   Subject LIKE '%' + @Subject + '%'
 

    ELSE 
        IF @AuthorId > 0 
            SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                    [Quote] ,
                    [Source] ,
                    [Page] ,
                    [Subject]
            FROM    Quotes
                    JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
            WHERE   Quotes.AuthorId = @AuthorId



        ELSE 
            IF LEN(@SOURCE) > 0 
                SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                        [Quote] ,
                        [Source] ,
                        [Page] ,
                        [Subject]
                FROM    Quotes
                        JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
                WHERE   Source LIKE '%' + @Source + '%'

            ELSE 
                IF LEN(@SearchQuery) > 0 
                    SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                            [Quote] ,
                            [Source] ,
                            [Page] ,
                            [Subject]
                    FROM    Quotes
                            JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
                    WHERE   Author LIKE '%' + @SearchQuery + '%'
                            OR Quote LIKE '%' + @SearchQuery + '%'

                ELSE 
                    SELECT  AuthorFirst + ' ' + AuthorLast AS [Author] ,
                            [Quote] ,
                            [Source] ,
                            [Page] ,
                            [Subject]
                    FROM    Quotes
                            JOIN DocumentAuthors ON Quotes.AuthorId = DocumentAuthors.AuthorId
GO
/****** Object:  StoredProcedure [dbo].[spGetEventDetails]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetEventDetails] @EventId INT
AS 
    SELECT  Calendar.EventId ,
            Calendar.GUID ,
            Calendar.Title ,
            Calendar.EventDate ,
            Calendar.EndDate ,
            Calendar.IntroText ,
            Calendar.Description ,
            Calendar.Location ,
            Calendar.Display ,
            Calendar.CreateDate
    FROM    Calendar
    WHERE   EventId = @EventId
GO
/****** Object:  StoredProcedure [dbo].[CalendarGetUpcomingEvents]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CalendarGetUpcomingEvents] @limit INT = 0
AS 
    BEGIN

        IF @limit > 0 
            SET ROWCOUNT @limit	
	
        SELECT  EventId ,
                Title ,
                EventDate ,
                EndDate ,
                IntroText ,
                EventImage ,
                Location ,
                Display ,
                CreateDate ,
                GUID
        FROM    calendar WITH ( NOLOCK )
        WHERE   display = 1
                AND EndDate > GETDATE() - 1
        ORDER BY EndDate ASC
	 
	 
    END
GO
/****** Object:  StoredProcedure [dbo].[CalendarGetEventDetail]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CalendarGetEventDetail] @EventId INT
AS 
    BEGIN
        SELECT  EventId ,
                Title ,
                EventDate ,
                EndDate ,
                IntroText ,
                [Description] ,
                Location ,
                Display ,
                CreateDate
        FROM    calendar
        WHERE   EventId = @EventId
    END
GO
/****** Object:  StoredProcedure [dbo].[PageContentGetDetail]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PageContentGetDetail]
    @ContentName VARCHAR(150) = 0
AS 
    BEGIN	
        SET NOCOUNT ON ;

        SELECT  Body
        FROM    pagecontent
        WHERE   NAME = @ContentName
    END
GO
/****** Object:  StoredProcedure [dbo].[QuotesGetSubjectList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[QuotesGetSubjectList]
AS 
    BEGIN
	
        SET NOCOUNT ON ;

        SELECT DISTINCT
                Subject
        FROM    QUOTES
    END
GO
/****** Object:  StoredProcedure [dbo].[spAddNewDocumentSubjectLink]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddNewDocumentSubjectLink]
    @DocumentId INT ,
    @SubjectId INT
AS 
    INSERT  INTO DocumentSubjectLink
            ( SubjectId, DocumentId )
    VALUES  ( @SubjectId, @DocumentId )
GO
/****** Object:  StoredProcedure [dbo].[TagGetRelatedDocuments]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--TagGetDocumentsWithTags 'Bastiat'
--SELECT * FROM [SplitCVS] ('Bastiat')

-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetRelatedDocuments]
    @DocumentId UNIQUEIDENTIFIER = NULL ,
    @ArticleId INT = 0
AS 
    BEGIN


        IF @DocumentId IS NULL 
            SET @DocumentId = ( SELECT  [GUID]
                                FROM    [DailyArticles]
                                WHERE   [DailyArticles].[ArticleId] = @ArticleId
                              )

--CREATE TABLE #Tags(TagId int)
--INSERT INTO [#Tags] ([TagId])  (SELECT TagId FROM [TagMap] WHERE [TagMap].[ObjectId] = @DocumentId)	
   
        SELECT TOP 5
                Documents.GUID ,
                Documents.[Title] ,
                CASE WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] IN ( 1, 2 )
                          ) > 0 THEN 'Media'
                     WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] = 3
                          ) > 0 THEN 'PDF'
                     ELSE 'Content'
                END AS 'Type' ,
                [Documents].[DocumentId] AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN Documents WITH ( NOLOCK ) ON Documents.[GUID] = [TagMap].[ObjectId]
        WHERE   [Documents].[Display] = 1
                AND [Tag].[TagId] IN (
                SELECT  TagId
                FROM    [TagMap]
                WHERE   [TagMap].[ObjectId] = @DocumentId )
        UNION ALL
        SELECT TOP 4
                Page.[GUID] ,
                Page.[pageTitle] AS Title ,
                'Content' AS 'Type' ,
                [Page].[PageId] AS Id ,
                '/' + page.[folder] + page.[pageName] AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN Tag WITH ( NOLOCK ) ON TagMap.TagId = Tag.TagId
                INNER JOIN [Page] ON [Page].[GUID] = [TagMap].[ObjectId]
        WHERE   ( Tag.TagId IN ( SELECT TagId
                                 FROM   [TagMap]
                                 WHERE  [TagMap].[ObjectId] = @DocumentId ) )
        UNION ALL
        SELECT TOP 4
                DailyArticles.GUID ,
                DailyArticles.[Title] ,
                'DailyArticle' AS TYPE ,
                [DailyArticles].[ArticleId] AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN DailyArticles ON DailyArticles.[GUID] = [TagMap].[ObjectId]
        WHERE   TagMap.Objectid <> @DocumentId
                AND ( Tag.TagId IN ( SELECT TagId
                                     FROM   [TagMap]
                                     WHERE  [TagMap].[ObjectId] = @DocumentId ) )
        UNION ALL
        SELECT TOP 4
                [GUID] ,
                title ,
                PeriodicalsView.[Journal] AS 'Type' ,
                0 AS Id ,
                [PeriodicalsView].[URL] AS 'URL'
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN PeriodicalsView ON PeriodicalsView.GUID = TagMap.ObjectId
        WHERE   ( Tag.TagId IN ( SELECT TagId
                                 FROM   [TagMap]
                                 WHERE  [TagMap].[ObjectId] = @DocumentId ) )
        UNION ALL
        SELECT TOP 4
                [GUID] ,
                NAME ,
                'Product' AS 'Type' ,
                products.ProductId AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN AbleCommerce..ac_Products products ON TagMap.ObjectId = products.GUID
        WHERE   ( Tag.TagId IN ( SELECT TagId
                                 FROM   [TagMap]
                                 WHERE  [TagMap].[ObjectId] = @DocumentId ) )
        ORDER BY Id DESC
    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetDocumentsWithTag]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--TagGetDocumentsWithTags 'Bastiat'
--SELECT * FROM [SplitCVS] ('Bastiat')

-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetDocumentsWithTag] @Tag VARCHAR(75)
AS 
    BEGIN
        SELECT TOP 200
                Documents.GUID ,
                Documents.[Title] ,
                CASE WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] IN ( 1, 2 )
                          ) > 0 THEN 'Media'
                     WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf WITH ( NOLOCK )
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] = 3
                          ) > 0 THEN 'PDF'
                     ELSE 'Content'
                END AS 'Type' ,
                [Documents].[DocumentId] AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN Documents WITH ( NOLOCK ) ON Documents.[GUID] = [TagMap].[ObjectId]
        WHERE   Tag.Tag = @Tag
                AND [Documents].[Display] = 1
        UNION ALL
        SELECT TOP 200
                Page.[GUID] ,
                Page.[pageTitle] AS Title ,
                'Content' AS 'Type' ,
                [Page].[PageId] AS Id ,
                page.[folder] + '/' + page.[pageName] AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN Tag WITH ( NOLOCK ) ON TagMap.TagId = Tag.TagId
                INNER JOIN [Page] WITH ( NOLOCK ) ON [Page].[GUID] = [TagMap].[ObjectId]
        WHERE   Tag.Tag = @Tag
        UNION ALL
        SELECT  DISTINCT TOP 200
                DailyArticles.GUID ,
                DailyArticles.[Title] ,
                'DailyArticle' AS 'Type' ,
                [DailyArticles].[ArticleId] AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN DailyArticles WITH ( NOLOCK ) ON DailyArticles.[GUID] = [TagMap].[ObjectId]
        WHERE   Tag.Tag = @Tag
        UNION ALL
        SELECT DISTINCT TOP 200
                [GUID] ,
                title ,
                PeriodicalsView.[Journal] AS 'Type' ,
                0 AS Id ,
                [PeriodicalsView].[URL] AS 'URL'
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN PeriodicalsView WITH ( NOLOCK ) ON PeriodicalsView.GUID = TagMap.ObjectId
        WHERE   Tag.Tag = @Tag
        -- OR [Tag].[Tag] LIKE '%' + @Tags  + '%' 
        UNION ALL
        SELECT  [GUID] ,
                NAME ,
                'Product' AS 'Type' ,
                products.ProductId AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN AbleCommerce..[ac_Products] products WITH ( NOLOCK ) ON TagMap.ObjectId = products.GUID
        WHERE   Tag.Tag = @Tag


    END
GO
/****** Object:  StoredProcedure [dbo].[TagGetDocumentsWithTags]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--TagGetDocumentsWithTags 'Bastiat'
--SELECT * FROM [SplitCVS] ('Bastiat')

-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[TagGetDocumentsWithTags] @Tags VARCHAR(MAX)
AS 
    BEGIN
        SELECT TOP 500
                Documents.GUID ,
                Documents.[Title] ,
                CASE WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] IN ( 1, 2 )
                          ) > 0 THEN 'Media'
                     WHEN ( SELECT  COUNT(*)
                            FROM    MediaAlternateFormat mf
                            WHERE   mf.DocumentId = Documents.DocumentId
                                    AND [MediaTypeId] = 3
                          ) > 0 THEN 'PDF'
                     ELSE 'Content'
                END AS 'Type' ,
                [Documents].[DocumentId] AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN Documents WITH ( NOLOCK ) ON Documents.[GUID] = [TagMap].[ObjectId]
        WHERE   ( Tag.Tag IN ( SELECT   *
                               FROM     [SplitCVS](@Tags) ) )
                AND [Documents].[Display] = 1
        UNION ALL
        SELECT TOP 500
                Page.[GUID] ,
                Page.[pageTitle] AS Title ,
                'Content' AS 'Type' ,
                [Page].[PageId] AS Id ,
                page.[folder] + '/' + page.[pageName] AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN Tag ON TagMap.TagId = Tag.TagId
                INNER JOIN [Page] WITH ( NOLOCK ) ON [Page].[GUID] = [TagMap].[ObjectId]
        WHERE   ( Tag.Tag IN ( SELECT   *
                               FROM     [SplitCVS](@Tags) ) )
        UNION ALL
        SELECT  DISTINCT TOP 500
                DailyArticles.GUID ,
                DailyArticles.[Title] ,
                'DailyArticle' AS 'Type' ,
                [DailyArticles].[ArticleId] AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN DailyArticles WITH ( NOLOCK ) ON DailyArticles.[GUID] = [TagMap].[ObjectId]
        WHERE   ( Tag.Tag IN ( SELECT   *
                               FROM     [SplitCVS](@Tags) ) )
        UNION ALL
        SELECT DISTINCT TOP 500
                [GUID] ,
                title ,
                PeriodicalsView.[Journal] AS 'Type' ,
                0 AS Id ,
                [PeriodicalsView].[URL] AS 'URL'
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN PeriodicalsView WITH ( NOLOCK ) ON PeriodicalsView.GUID = TagMap.ObjectId
        WHERE   ( Tag.Tag IN ( SELECT   *
                               FROM     [SplitCVS](@Tags) ) )
        -- OR [Tag].[Tag] LIKE '%' + @Tags  + '%' 
        UNION ALL
        SELECT  [GUID] ,
                NAME ,
                'Product' AS 'Type' ,
                products.ProductId AS Id ,
                '' AS URL
        FROM    TagMap WITH ( NOLOCK )
                INNER JOIN [Tag] WITH ( NOLOCK ) ON [TagMap].[TagId] = [Tag].[TagId]
                INNER JOIN AbleCommerce..[ac_Products] products WITH ( NOLOCK ) ON TagMap.ObjectId = products.GUID
        WHERE   ( Tag.Tag IN ( SELECT   *
                               FROM     [SplitCVS](@Tags) ) )


    END
GO
/****** Object:  StoredProcedure [dbo].[zzzspGetHomepage]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zzzspGetHomepage]
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

    SET NOCOUNT ON
    
    EXEC DailyArticlesGetTodaysSummary

    EXEC DailyArticlesGetRecent

-- New Resources

-- HOmepage Items
--SELECT  Itemid, Title, PubInfo As Description, URL,
--AuthorFirst + ' ' + AuthorMiddle + ' ' + AuthorLast As Author, CreateDate, 
--MediaIconPath
--FROM HomepageItems  
--LEFT JOIN DocumentAuthors ON HomepageItems.Author1 = DocumentAuthors.AuthorId
--LEFT JOIN DocumentMediaType ON HomepageItems.MediaTypeId = DocumentMediaType.MediaTypeID
--
--
--WHERE Display= 1 AND ItemOrder > 0 
----ORDER BY ItemOrder, CreateDate DESC, ItemId DESC
--
--UNION ALL

-- Documents
    SELECT TOP 12
            Documents.DocumentId ,
            Documents.Title ,
            Documents.PubInfo AS Description ,
            URL = CASE media.URL + ''
                    WHEN ''
                    THEN '/resources/'
                         + CAST(Documents.DocumentId AS VARCHAR(MAX))
                    ELSE media.URL
                  END ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            Documents.CreateDate AS CreateDate ,
            MediaIconPath = CASE DocumentMediaType.MediaIconPath + ''
                              WHEN '' THEN '/images/Icons/html.png'
                              ELSE DocumentMediaType.MediaIconPath
                            END
    FROM    dbo.Documents WITH ( NOLOCK )
            LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
            LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
            LEFT JOIN dbo.DocumentMediaType ON media.MediaTypeId = DocumentMediaType.MediaTypeID
    WHERE   Documents.Display = 1
            AND media.URL <> ''
            AND media.MediaTypeId NOT IN ( 1, 2 )
    ORDER BY documents.CreateDate DESC



-- New Media
    SELECT TOP 10
            Documents.DocumentId ,
            Documents.Title ,
            Documents.PubInfo AS Description ,
            media.URL ,
            DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle
            + ' ' + DocumentAuthors.AuthorLast AS Author ,
            Documents.CreateDate ,
            DocumentMediaType.MediaIconPath
    FROM    dbo.Documents WITH ( NOLOCK )
            LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
            LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
            LEFT JOIN dbo.DocumentMediaType ON media.MediaTypeId = DocumentMediaType.MediaTypeID
    WHERE   Documents.Display = 1
            AND media.MediaTypeId IN ( 1, 2 )
    ORDER BY Documents.CreateDate DESC


-- Upcoming Evnets

    SELECT  calendar.EventId ,
            calendar.Title ,
            calendar.EventDate ,
            calendar.EndDate ,
            calendar.Location ,
            calendar.Display ,
            calendar.CreateDate
    FROM    dbo.calendar WITH ( NOLOCK )
    WHERE   calendar.display = 1
            AND calendar.EndDate > GETDATE() - 1
    ORDER BY calendar.EndDate ASC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetCategoryContent]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MediaGetCategoryContent]
    @CategoryId INT = 0 ,
    @SubjectId INT = 0
AS 
    SET CONCAT_NULL_YIELDS_NULL OFF 

--IF @CategoryId = -1 OR @CategoryId = 0

    EXEC dbo.MediaGetSubCategories @CategoryId
    
    IF @CategoryId >= 0 
        IF @CategoryId = 210  -- Get Recent Media
            SELECT  TOP 30 Documents.DocumentId ,
                    Documents.Title ,
                    Documents.CategoryId ,
                    Documents.CreateDate ,
                    Documents.metaDescription AS 'Description' ,
                    media.DocumentId ,
                    media.MediaTypeId ,
                    media.URL ,
                    media.MediaId ,
                    Documents.EditDate ,
                    media.CreateDate ,
                    DocumentAuthors.AuthorId ,
                    DocumentMediaType.MediaType ,
                    DocumentMediaType.MediaIconPath ,
                    MediaCategory.[Category] ,
                    DocumentAuthors.AuthorFirst + '  '
                    + DocumentAuthors.AuthorMiddle + '  '
                    + DocumentAuthors.AuthorLast AS Author ,
                    DocumentAuthors.[AuthorLast] AS LastName ,
                    CASE WHEN Documents.metaImage IS NULL THEN 0
                         WHEN Documents.metaImage IS NOT NULL THEN 1
                    END AS HasImage
            FROM    dbo.Documents
                    LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                    LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                    LEFT JOIN dbo.DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                    LEFT JOIN dbo.MediaCategory ON Documents.CategoryId = MediaCategory.CategoryId
            WHERE   DocumentMediaType.MediaTypeId IN ( 1, 2 )
                   -- AND Documents.CategoryId = @CategoryId
                    AND Documents.Display = 1
            ORDER BY Documents.CreateDate dESC
    
        ELSE 
            SELECT  Documents.DocumentId ,
                    Documents.Title ,
                    Documents.CategoryId ,
                    Documents.CreateDate ,
                    Documents.metaDescription AS 'Description' ,
                    media.DocumentId ,
                    media.MediaTypeId ,
                    media.URL ,
                    media.MediaId ,
                    Documents.EditDate ,
                    media.CreateDate ,
                    DocumentAuthors.AuthorId ,
                    DocumentMediaType.MediaType ,
                    DocumentMediaType.MediaIconPath ,
                    MediaCategory.[Category] ,
                    DocumentAuthors.AuthorFirst + '  '
                    + DocumentAuthors.AuthorMiddle + '  '
                    + DocumentAuthors.AuthorLast AS Author ,
                    DocumentAuthors.[AuthorLast] AS LastName ,
                    CASE WHEN Documents.metaImage IS NULL THEN 0
                         WHEN Documents.metaImage IS NOT NULL THEN 1
                    END AS HasImage
            FROM    dbo.Documents
                    LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                    LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                    LEFT JOIN dbo.DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                    LEFT JOIN dbo.MediaCategory ON Documents.CategoryId = MediaCategory.CategoryId
            WHERE   DocumentMediaType.MediaTypeId IN ( 1, 2 )
                    AND Documents.CategoryId = @CategoryId
                    AND Documents.Display = 1
            ORDER BY Documents.Title
        
    ELSE 
        IF @SubjectId > 0 
            SELECT  Documents.DocumentId ,
                    Documents.Title ,
                    Documents.CategoryId ,
                    Documents.CreateDate ,
                    Documents.EditDate ,
                    Documents.metaDescription AS 'Description' ,
                    media.DocumentId ,
                    media.MediaTypeId ,
                    media.URL ,
                    media.MediaId ,
                    media.CreateDate ,
                    DocumentAuthors.AuthorId ,
                    DocumentMediaType.MediaType ,
                    DocumentMediaType.MediaIconPath ,
--                MediaCategory.[Category],
                    DocumentAuthors.AuthorFirst + '  '
                    + DocumentAuthors.AuthorMiddle + '  '
                    + DocumentAuthors.AuthorLast AS Author ,
                    DocumentAuthors.[AuthorLast] AS LastName ,
                    CASE WHEN Documents.metaImage IS NULL THEN 0
                         WHEN Documents.metaImage IS NOT NULL THEN 1
                    END AS HasImage
            FROM    dbo.Documents
                    LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                    LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                    LEFT JOIN dbo.DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
--                LEFT JOIN dbo.MediaCategory ON Documents.CategoryId = MediaCategory.CategoryId                
            WHERE   DocumentMediaType.MediaTypeId IN ( 1, 2 )
                    AND Documents.Display = 1
                    AND Documents.DocumentId IN (
                    SELECT  DocumentId
                    FROM    DocumentSubjectLink
                    WHERE   DocumentSubjectLink.SubjectID = @SubjectId )
            ORDER BY Documents.Title

        ELSE  -- Get ALL media
            SELECT  Documents.DocumentId ,
                    Documents.Title ,
                    Documents.CategoryId ,
                    Documents.CreateDate ,
                    Documents.metaDescription AS 'Description' ,
                    media.DocumentId ,
                    media.MediaTypeId ,
                    media.URL ,
                    media.MediaId ,
                    media.CreateDate ,
                    Documents.EditDate ,
                    DocumentAuthors.AuthorId ,
                    DocumentMediaType.MediaType ,
                    DocumentMediaType.MediaIconPath ,
                    MediaCategory.[Category] ,
                    DocumentAuthors.AuthorFirst + '  '
                    + DocumentAuthors.AuthorMiddle + '  '
                    + DocumentAuthors.AuthorLast AS Author ,
                    DocumentAuthors.[AuthorLast] AS LastName ,
                    CASE WHEN Documents.metaImage IS NULL THEN 0
                         WHEN Documents.metaImage IS NOT NULL THEN 1
                    END AS HasImage
            FROM    dbo.Documents
                    LEFT JOIN dbo.MediaAlternateFormat media ON media.DocumentId = Documents.DocumentId
                    LEFT JOIN dbo.DocumentAuthors ON Documents.Author1 = DocumentAuthors.AuthorId
                    LEFT JOIN dbo.DocumentMediaType ON DocumentMediaType.MediaTypeId = media.MediaTypeId
                    LEFT JOIN dbo.MediaCategory ON Documents.CategoryId = MediaCategory.CategoryId
            WHERE   DocumentMediaType.MediaTypeId IN ( 1, 2 )
                    AND Documents.Display = 1
            ORDER BY Documents.Title
GO
/****** Object:  StoredProcedure [dbo].[_FixHTML]    Script Date: 02/17/2010 04:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_FixHTML] 
	
AS
BEGIN

UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, '<font[^>]*>','')
UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, '</font>','')
UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, ' class="MsoBodyText"','')

UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, '<p align="left">','<p>')
UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, '<p class="Mso[^>]*>','<p>')

UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, '<FONT[^>]*>','')
UPDATE DailyArticles SET ArticleText = dbo.regexReplace( ArticleText, '</FONT>','') 

EXEC _SearchAndReplace
	@SearchStr = '<br>',
	@ReplaceStr = '<br />'

EXEC _SearchAndReplace
	@SearchStr = '<s>',
	@ReplaceStr = '<span class="line-through">'

EXEC _SearchAndReplace
	@SearchStr = '</s>',
	@ReplaceStr = '</span>'	

EXEC _SearchAndReplace
	@SearchStr = '<center>',
	@ReplaceStr = '<div style="text-align: center">'

EXEC _SearchAndReplace
	@SearchStr = '</center>',
	@ReplaceStr = '</div>'	

EXEC [_SearchAndReplace]
	@SearchStr = 'nowrap="true"',
	@ReplaceStr = 'nowrap="nowrap"'	
	
EXEC [_SearchAndReplace]
	@SearchStr = '<hr color="#0d4d7d" noshade="noshade" />',
	@ReplaceStr = '<hr />'	

EXEC _SearchAndReplace
	@SearchStr = 'http://www.mises.org',
	@ReplaceStr = 'http://mises.org'	
	
EXEC _SearchAndReplace
	@SearchStr = 'https://www.mises.org',
	@ReplaceStr = 'https://mises.org'	

EXEC _SearchAndReplace
	@SearchStr = 'http://mises.org/MultiMedia/',
	@ReplaceStr = 'http://media.mises.org/'	
		

END
GO
/****** Object:  StoredProcedure [dbo].[CalendarGetEventsList]    Script Date: 02/17/2010 04:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CalendarGetEventsList]
AS 
    BEGIN
        EXEC dbo.CalendarGetUpcomingEvents	
	 
        SELECT TOP 30
                EventId ,
                Title ,
                EventDate ,
                EndDate ,
                IntroText ,
                EventImage ,
                Location ,
                Display ,
                CreateDate
        FROM    calendar WITH ( NOLOCK )
        WHERE   display = 1
                AND EndDate < GETDATE() - 1
        ORDER BY EndDate DESC
    END
GO
/****** Object:  Default [DF__AspNet_Sq__notif__40257DE4]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification] ADD  DEFAULT (getdate()) FOR [notificationCreated]
GO
/****** Object:  Default [DF__AspNet_Sq__chang__4119A21D]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification] ADD  DEFAULT (0) FOR [changeId]
GO
/****** Object:  Default [DF_Biography_AuthorMiddle]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Biography] ADD  CONSTRAINT [DF_Biography_AuthorMiddle]  DEFAULT ('') FOR [AuthorMiddle]
GO
/****** Object:  Default [DF_Biography_AuthorLast]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Biography] ADD  CONSTRAINT [DF_Biography_AuthorLast]  DEFAULT ('') FOR [AuthorLast]
GO
/****** Object:  Default [DF_Biography_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Biography] ADD  CONSTRAINT [DF_Biography_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_BookReviews_TimeCreated]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[BookReviews] ADD  CONSTRAINT [DF_BookReviews_TimeCreated]  DEFAULT (getdate()) FOR [ReviewDate]
GO
/****** Object:  Default [DF_Calendar_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Calendar] ADD  CONSTRAINT [DF_Calendar_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_calendar2_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Calendar] ADD  CONSTRAINT [DF_calendar2_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_dailyarticles_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_dailyarticles_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_DailyArticles_DisplayOrder]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_DailyArticles_DisplayOrder]  DEFAULT ((10)) FOR [DisplayOrder]
GO
/****** Object:  Default [DF_DailyArticles_Featured]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_DailyArticles_Featured]  DEFAULT ((0)) FOR [Featured]
GO
/****** Object:  Default [DF_DailyArticles_Headline]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_DailyArticles_Headline]  DEFAULT ((0)) FOR [Headline]
GO
/****** Object:  Default [DF_DailyArticles_ShowArticle]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_DailyArticles_ShowArticle]  DEFAULT ((0)) FOR [ShowArticle]
GO
/****** Object:  Default [DF_dailyarticles_DatePosted]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_dailyarticles_DatePosted]  DEFAULT (getdate()) FOR [DatePosted]
GO
/****** Object:  Default [DF_DailyArticles_EditDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_DailyArticles_EditDate]  DEFAULT (getdate()) FOR [EditDate]
GO
/****** Object:  Default [DF_dailyarticles_CreatedDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DailyArticles] ADD  CONSTRAINT [DF_dailyarticles_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_DocumentAuthors_AuthorMiddle]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentAuthors] ADD  CONSTRAINT [DF_DocumentAuthors_AuthorMiddle]  DEFAULT ('') FOR [AuthorMiddle]
GO
/****** Object:  Default [DF_DocumentAuthors_AuthorLast]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentAuthors] ADD  CONSTRAINT [DF_DocumentAuthors_AuthorLast]  DEFAULT ('') FOR [AuthorLast]
GO
/****** Object:  Default [DF_DocumentAuthors_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentAuthors] ADD  CONSTRAINT [DF_DocumentAuthors_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_tblMediaType_CreateTime]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentMediaType] ADD  CONSTRAINT [DF_tblMediaType_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_StudyGuide_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_StudyGuide_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_Documents_Featured]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_Documents_Featured]  DEFAULT ((0)) FOR [Featured]
GO
/****** Object:  Default [DF_StudyGuide_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_StudyGuide_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Documents_EditDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_Documents_EditDate]  DEFAULT (getdate()) FOR [EditDate]
GO
/****** Object:  Default [DF_DocumentSubjects_Visible]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentSubjects] ADD  CONSTRAINT [DF_DocumentSubjects_Visible]  DEFAULT ((1)) FOR [Visible]
GO
/****** Object:  Default [DF_tblDonation_RecurringInterval]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Donations] ADD  CONSTRAINT [DF_tblDonation_RecurringInterval]  DEFAULT ((0)) FOR [RecurringInterval]
GO
/****** Object:  Default [DF_tblDonation_CreateTime]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Donations] ADD  CONSTRAINT [DF_tblDonation_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_tblDonation_CreateUserID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Donations] ADD  CONSTRAINT [DF_tblDonation_CreateUserID]  DEFAULT ((0)) FOR [CreateUserID]
GO
/****** Object:  Default [DF_tblDonation_ModifyUserID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Donations] ADD  CONSTRAINT [DF_tblDonation_ModifyUserID]  DEFAULT ((0)) FOR [ModifyUserID]
GO
/****** Object:  Default [DF_tblDonation_IsDeleted]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Donations] ADD  CONSTRAINT [DF_tblDonation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_Faculty_display]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Faculty] ADD  CONSTRAINT [DF_Faculty_display]  DEFAULT ((1)) FOR [display]
GO
/****** Object:  Default [DF_Faculty_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Faculty] ADD  CONSTRAINT [DF_Faculty_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Favorites_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Favorites] ADD  CONSTRAINT [DF_Favorites_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_fellows_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[fellows] ADD  CONSTRAINT [DF_fellows_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_freemarket_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[freemarket] ADD  CONSTRAINT [DF_freemarket_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_jls_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[jls] ADD  CONSTRAINT [DF_jls_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_ManagerUsers_createDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[ManagerUsers] ADD  CONSTRAINT [DF_ManagerUsers_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
/****** Object:  Default [DF_MediaAlternateFormat_fileSize]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[MediaAlternateFormat] ADD  CONSTRAINT [DF_MediaAlternateFormat_fileSize]  DEFAULT ((0)) FOR [fileSize]
GO
/****** Object:  Default [DF_MediaAlternateFormat_duration]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[MediaAlternateFormat] ADD  CONSTRAINT [DF_MediaAlternateFormat_duration]  DEFAULT ((0)) FOR [duration]
GO
/****** Object:  Default [DF_AlternateMediaFormat_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[MediaAlternateFormat] ADD  CONSTRAINT [DF_AlternateMediaFormat_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_MediaCategory_ParentCategory]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[MediaCategory] ADD  CONSTRAINT [DF_MediaCategory_ParentCategory]  DEFAULT ((102)) FOR [ParentCategory]
GO
/****** Object:  Default [DF_MediaCategory_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[MediaCategory] ADD  CONSTRAINT [DF_MediaCategory_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_misesreview_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[misesreview] ADD  CONSTRAINT [DF_misesreview_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_misesreview_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[misesreview] ADD  CONSTRAINT [DF_misesreview_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Page_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_Page_Visible]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_Visible]  DEFAULT ((1)) FOR [Visible]
GO
/****** Object:  Default [DF_Page_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Page_EditDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Page] ADD  CONSTRAINT [DF_Page_EditDate]  DEFAULT (getdate()) FOR [EditDate]
GO
/****** Object:  Default [DF_cs_Content_LastModifed]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[PageContent] ADD  CONSTRAINT [DF_cs_Content_LastModifed]  DEFAULT (getdate()) FOR [LastModified]
GO
/****** Object:  Default [DF_cs_Content_SortOrder]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[PageContent] ADD  CONSTRAINT [DF_cs_Content_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
/****** Object:  Default [DF_cs_Content_Hidden]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[PageContent] ADD  CONSTRAINT [DF_cs_Content_Hidden]  DEFAULT ((1)) FOR [Hidden]
GO
/****** Object:  Default [DF_Pages_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_Pages_Visible]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_Visible]  DEFAULT ((1)) FOR [Visible]
GO
/****** Object:  Default [DF_Pages_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Pages] ADD  CONSTRAINT [DF_Pages_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_People_FirstName]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[People] ADD  CONSTRAINT [DF_People_FirstName]  DEFAULT ('(None)') FOR [FirstName]
GO
/****** Object:  Default [DF_People_MiddleName]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[People] ADD  CONSTRAINT [DF_People_MiddleName]  DEFAULT (' ') FOR [MiddleName]
GO
/****** Object:  Default [DF_People_LastName]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[People] ADD  CONSTRAINT [DF_People_LastName]  DEFAULT (' ') FOR [LastName]
GO
/****** Object:  Default [DF_People_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[People] ADD  CONSTRAINT [DF_People_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Periodicals_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Periodicals] ADD  CONSTRAINT [DF_Periodicals_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_qjaeDB_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[qjaeDB] ADD  CONSTRAINT [DF_qjaeDB_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_QuizQuestion_QuizID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[QuizQuestion] ADD  CONSTRAINT [DF_QuizQuestion_QuizID]  DEFAULT ((6)) FOR [QuizID]
GO
/****** Object:  Default [DF_QuizType_Display]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[QuizType] ADD  CONSTRAINT [DF_QuizType_Display]  DEFAULT ((1)) FOR [Display]
GO
/****** Object:  Default [DF_QuizType_DateCreated]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[QuizType] ADD  CONSTRAINT [DF_QuizType_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_raeDB_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[raeDB] ADD  CONSTRAINT [DF_raeDB_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_RedirectedURL_Priority]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RedirectedURL] ADD  CONSTRAINT [DF_RedirectedURL_Priority]  DEFAULT ((0)) FOR [Priority]
GO
/****** Object:  Default [DF_RedirectedURL_ExactMatchOnly]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RedirectedURL] ADD  CONSTRAINT [DF_RedirectedURL_ExactMatchOnly]  DEFAULT ((0)) FOR [ExactMatchOnly]
GO
/****** Object:  Default [DF_RegistrationForms_GUID]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RegistrationForms] ADD  CONSTRAINT [DF_RegistrationForms_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF_RegistrationForms_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RegistrationForms] ADD  CONSTRAINT [DF_RegistrationForms_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_RegistrationForms_TakeCreditCard]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RegistrationForms] ADD  CONSTRAINT [DF_RegistrationForms_TakeCreditCard]  DEFAULT ((0)) FOR [TakeCreditCard]
GO
/****** Object:  Default [DF_RegistrationForms_ProcessPayment]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RegistrationForms] ADD  CONSTRAINT [DF_RegistrationForms_ProcessPayment]  DEFAULT ((0)) FOR [ProcessPayment]
GO
/****** Object:  Default [DF_RegistrationForms_GetAddressInfo]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RegistrationForms] ADD  CONSTRAINT [DF_RegistrationForms_GetAddressInfo]  DEFAULT ((0)) FOR [GetAddressInfo]
GO
/****** Object:  Default [DF_RegistrationForms_PrimaryProductId]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[RegistrationForms] ADD  CONSTRAINT [DF_RegistrationForms_PrimaryProductId]  DEFAULT ((0)) FOR [PrimaryProductId]
GO
/****** Object:  Default [DF_scholar_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[scholar] ADD  CONSTRAINT [DF_scholar_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_Tag_LockAuthority]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Tag] ADD  CONSTRAINT [DF_Tag_LockAuthority]  DEFAULT ((0)) FOR [LockAuthority]
GO
/****** Object:  Default [DF_Tag_EditDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Tag] ADD  CONSTRAINT [DF_Tag_EditDate]  DEFAULT (getdate()) FOR [EditDate]
GO
/****** Object:  Default [DF_Table_1_TaggedOn]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[TagMap] ADD  CONSTRAINT [DF_Table_1_TaggedOn]  DEFAULT (getdate()) FOR [TaggedDate]
GO
/****** Object:  Default [DF_ticker_dateola]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[ticker] ADD  CONSTRAINT [DF_ticker_dateola]  DEFAULT (getdate()) FOR [dateola]
GO
/****** Object:  Default [DF_wardlibrary_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[WardLibrary] ADD  CONSTRAINT [DF_wardlibrary_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  Default [DF_wardlibraryBackup_CreateDate]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[WardLibraryBackup] ADD  CONSTRAINT [DF_wardlibraryBackup_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
/****** Object:  ForeignKey [FK_StudyGuide_StudyGuideAuthors]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_StudyGuideAuthors] FOREIGN KEY([Author1])
REFERENCES [dbo].[DocumentAuthors] ([AuthorId])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_StudyGuideAuthors]
GO
/****** Object:  ForeignKey [FK_StudyGuide_StudyGuideAuthors1]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_StudyGuideAuthors1] FOREIGN KEY([Author2])
REFERENCES [dbo].[DocumentAuthors] ([AuthorId])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_StudyGuideAuthors1]
GO
/****** Object:  ForeignKey [FK_StudyGuide_tblMediaType]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_tblMediaType] FOREIGN KEY([oldMediaTypeId])
REFERENCES [dbo].[DocumentMediaType] ([MediaTypeID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_tblMediaType]
GO
/****** Object:  ForeignKey [FK_StudyGuideSubjectLink_StudyGuide]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentSubjectLink]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuideSubjectLink_StudyGuide] FOREIGN KEY([DocumentId])
REFERENCES [dbo].[Documents] ([DocumentId])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[DocumentSubjectLink] NOCHECK CONSTRAINT [FK_StudyGuideSubjectLink_StudyGuide]
GO
/****** Object:  ForeignKey [FK_StudyGuideSubjectLink_StudyGuideSubject]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentSubjectLink]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuideSubjectLink_StudyGuideSubject] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[DocumentSubjects] ([SubjectId])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[DocumentSubjectLink] NOCHECK CONSTRAINT [FK_StudyGuideSubjectLink_StudyGuideSubject]
GO
/****** Object:  ForeignKey [FK_StudyGuideSubject_StudyGuideCategories]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[DocumentSubjects]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuideSubject_StudyGuideCategories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[oldDocumentCategory] ([CategoryId])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[DocumentSubjects] NOCHECK CONSTRAINT [FK_StudyGuideSubject_StudyGuideCategories]
GO
/****** Object:  ForeignKey [FK_TagMap_Tag]    Script Date: 02/17/2010 04:51:04 ******/
ALTER TABLE [dbo].[TagMap]  WITH CHECK ADD  CONSTRAINT [FK_TagMap_Tag] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
GO
ALTER TABLE [dbo].[TagMap] CHECK CONSTRAINT [FK_TagMap_Tag]
GO
