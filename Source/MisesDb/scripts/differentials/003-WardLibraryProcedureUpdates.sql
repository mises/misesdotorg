USE @MisesDatabase@
GO
/****** Object:  StoredProcedure [dbo].[WardLibraryGetBook]    Script Date: 11/19/2009 11:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
alter PROCEDURE [dbo].[WardLibraryGetBook] 	
(
	@Control int = 0
)
AS
BEGIN
	SELECT * 
	FROM wardlibrary WHERE CONTROL = @Control
END
