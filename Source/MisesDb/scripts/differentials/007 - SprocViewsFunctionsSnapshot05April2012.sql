USE [Mises]
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetLinks]    Script Date: 04/05/2012 12:12:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DocumentGetLinks]
       @SubjectId int = 0 ,
       @AuthorId int = 0 ,
       @MediaType int = 0 ,
       @Source varchar(500) = NULL ,
       @SearchQuery varchar(500) = NULL ,
       @MediaCategory varchar(10) = '' ,
       @debug bit = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF
DECLARE @SQLSTATEMENT nvarchar(max)
SET @SQLSTATEMENT =
' 

SELECT  Documents.DocumentId                     ,        
Documents.GUID                           ,        
Title                                    ,        
Documents.PublicationInformation + '' '' + SUBSTRING(Documents.Description,0,120) + ''...'' As Description                   ,        
Source                                   ,        
DocumentAuthors.AuthorLast                               ,        
DocumentAuthors.AuthorFirst + '' '' + DocumentAuthors.AuthorMiddle + '' '' + DocumentAuthors.AuthorLast As Author,
DocumentAuthors2.AuthorFirst + '' '' + DocumentAuthors2.AuthorMiddle + '' '' + DocumentAuthors2.AuthorLast As CoAuthor,                                            
Author1                                  ,        
Author2                                  ,             
media.MediaTypeId                        ,        
MediaIconPath =        
CASE MediaIconPath + ''''                
WHEN ''''                
THEN ''/images/Icons/html.png''                
ELSE MediaIconPath        
END                                       ,        
media.CreateDate                       As DatePosted,               
''/document/'' + CAST(Documents.DocumentId AS varchar(max)) AS ''URL''
FROM    Documents WITH(NOLOCK)
LEFT JOIN DocumentFiles media
ON      media.DocumentId = Documents.DocumentId
LEFT JOIN DocumentMediaType
ON      DocumentMediaType.MediaTypeId = media.MediaTypeId
LEFT JOIN DocumentAuthors
ON      DocumentAuthors.AuthorId = Documents.Author1
LEFT JOIN DocumentAuthors DocumentAuthors2
ON      DocumentAuthors2.AuthorId = Documents.Author2
LEFT JOIN DocumentSubjectLink
ON      DocumentSubjectLink.DocumentId = Documents.DocumentId
WHERE   Documents.Display                        = 1     AND media.Display = 1
AND DocumentMediaType.IsMedia != 1 AND DocumentMediaType.MediaTypeId != 7 
'
IF @SubjectId > 0
--SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND Documents.DocumentId IN (select DocumentId from DocumentSubjectLink WHERE SubjectId = '
SET @SQLSTATEMENT = @SQLSTATEMENT +
'AND EXISTS (select * from DocumentSubjectLink WHERE DocumentSubjectLink.DocumentId = documents.DocumentId AND  SubjectId = '
+ cast(@SubjectId AS VARCHAR(7)) + ')'
ELSE
   IF @AuthorId > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND (Author1 = ' + cast(@AuthorId AS VARCHAR(max))
+ ' OR Author2 = ' + cast(@AuthorId AS VARCHAR(max)) + ')'
   ELSE
      IF @MediaType > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND DocumentMediaType.MediaTypeId  = ' +
cast(@MediaType AS VARCHAR(7))
      ELSE
         IF LEN(@SOURCE) > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND Source = (''' + cast(@Source AS
VARCHAR(MAX)) + ''')'
         ELSE
            IF LEN(@SearchQuery) > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND (Title LIKE  ' + char(39) + '%' +
@SearchQuery + '%' + char(39) +
' 

OR DocumentAuthors.AuthorFirst LIKE ' + char(39) + '%' +
@SearchQuery + '%' + char(39) + ' 

OR DocumentAuthors.AuthorLast LIKE '
+ char(39) + '%' + @SearchQuery + '%' + char(39) +
' 



OR Documents.Description LIKE ' + char(39) + '%' + @SearchQuery + '%'
+ char(39) + ' 

OR Keywords LIKE ' + char(39) + '%' + @SearchQuery
+ '%' + char(39) + ' 



OR PublicationInformation LIKE ' + char(39) + '%' +
@SearchQuery + '%' + char(39) + ')'
IF @MediaCategory = 'Media'
SET @SQLSTATEMENT = @SQLSTATEMENT + ' AND DocumentMediaType.MediaTypeId IN (1,2,8) '
ELSE
   IF @MediaCategory = 'Text'
SET @SQLSTATEMENT = @SQLSTATEMENT +
' AND DocumentMediaType.IsMedia = 0 '
-- *********************************************************************************************************
-- Get Journal Entries
SET @SQLSTATEMENT = @SQLSTATEMENT +
' UNION



SELECT  0 As DocumentId                                           ,        

GUID                                                      ,        

Title                                                     ,        

Volume          AS Description,        

Journal                                     AS Source     ,        
        

AuthorLast                                                ,        

AuthorName AS Author                                      ,        
'''' As CoAuthor											  ,

AuthorId          AS Author1                                     ,        

0          AS Author2                                     ,        
      
DocumentMediaType.MediaTypeId                             ,        

MediaIconPath =        

CASE MediaIconPath + ''''                

WHEN ''''                

THEN ''/images/Icons/html.png''                

ELSE MediaIconPath        

END        ,                

DatePosted,        

URL

FROM    [PeriodicalsView]

INNER JOIN DocumentMediaType

ON      DocumentMediaType.MediaTypeId = PeriodicalsView.MediaTypeId

WHERE   1                             =1 

'
IF @SubjectId > 0
SET @SQLSTATEMENT = @SQLSTATEMENT +
'AND EXISTS (select * from DocumentSubjectLink WHERE 
PeriodicalsView.GUID = DocumentSubjectLink.GUID AND 
SubjectId = '
+ cast(@SubjectId AS VARCHAR(7)) + ')'
ELSE
   IF @AuthorId > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND (PeriodicalsView.AuthorId = ' + cast(
@AuthorId AS VARCHAR(7)) + ')'
   ELSE
      IF @MediaType > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND DocumentMediaType.MediaTypeId  = ' +
cast(@MediaType AS VARCHAR(7))
      ELSE
         IF LEN(@SOURCE) > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND Journal = (''' + cast(@Source AS
VARCHAR(MAX)) + ''')'
         ELSE
            IF LEN(@SearchQuery) > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND (Title LIKE  ' + char(39) + '%' +
@SearchQuery + '%' + char(39) + ' 

OR AuthorName LIKE ' + char(39) +
'%' + @SearchQuery + '%' + char(39) + ')'
-- ***************** ORDERING
SET @SQLSTATEMENT = @SQLSTATEMENT + ' ORDER BY  '

IF @SubjectId > 0 OR @AuthorId > 0 --OR LEN(@Source) > 0
SET @SQLSTATEMENT = @SQLSTATEMENT + ' Documents.Title, '

IF @MediaCategory = 'title'
SET @SQLSTATEMENT = @SQLSTATEMENT + ' Documents.Title, '

--IF LEN(@Source) > 0
--   SET @SQLSTATEMENT = @SQLSTATEMENT + ' Documents.-, '

SET @SQLSTATEMENT = @SQLSTATEMENT + ' DatePosted DESC '
-- *******************************************************************
-- Get Other Tables
IF @SubjectId > 0
   BEGIN
SET @SQLSTATEMENT = @SQLSTATEMENT +
';SELECT Subject,Photo FROM DocumentSubjects WHERE SubjectId = ' + cast(
@SubjectId AS VARCHAR(7))
   END
IF @AuthorId > 0
SET @SQLSTATEMENT = @SQLSTATEMENT +
';SELECT AuthorFirst + ''  '' + AuthorLast As Author, Photo FROM DocumentAuthors WHERE DocumentAuthors.AuthorId = '
+ cast(@AuthorId AS VARCHAR(7))
--SELECT @SQLSTATEMENT
IF @debug = 0
EXEC sp_executesql @SQLSTATEMENT
ELSE
SELECT @sqlstatement
GO
/****** Object:  StoredProcedure [dbo].[CommunityGetFeed]    Script Date: 04/05/2012 12:12:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[CommunityGetFeed]
AS
BEGIN
      SET NOCOUNT ON ;

-- Forums
      SELECT TOP 10
--T.*,
          T.ThreadId ,
          T.PostDate ,
          T.PostAuthor ,
          T.UserId ,
          T.ThreadDate ,
          ( SELECT TOP 1
                subject
            FROM
                MisesCommunity..cs_Posts
            WHERE
                cs_Posts.ThreadID = T.ThreadID ) AS 'Subject'
      FROM
          MisesCommunity..cs_Threads T WITH ( NOLOCK )
--INNER JOIN MisesCommunity..cs_Posts P ON p.ThreadID = T.ThreadID 
      WHERE
          ( SELECT TOP 1
                ApplicationPostType
            FROM
                MisesCommunity..cs_Posts WITH ( NOLOCK )
            WHERE
                cs_Posts.ThreadID = T.ThreadID ) <> 1 AND T.IsApproved = 1 AND T.
          sectionid NOT IN ( 1 , 2 , 9 , 81 , 82 , 83 , 84 , 86 , 87 , 88 , 89 , 90 , 91 ,
          92 , 93 , 94 , 310 )
      ORDER BY
          T.ThreadDate DESC
  
  
-- Blogs
      SELECT TOP 10
         --* ,
          PostID ,
          PostAuthor AS author ,
          Subject AS title ,
          UserId ,
          PostDate AS PubDate ,
          PostName ,
          '' AS link ,
          UserTime ,
          ApplicationKey ,
          PostStatus
      FROM
          MisesCommunity..cs_Posts WITH ( NOLOCK ) JOIN MisesCommunity..cs_Sections WITH
      ( NOLOCK )
      ON  cs_Sections.SectionID = cs_Posts.SectionID
      WHERE
          cs_Posts.[ApplicationPostType] = 1 AND IsApproved = 1 AND cs_Sections.SectionID
          NOT IN ( 86 , 81 ) AND ApplicationType = 1 AND IsIndexed = 1
      ORDER BY
          cs_Posts.PostDate DESC

END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetArchive]    Script Date: 04/05/2012 12:12:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticlesGetArchive]
       @ShowAll bit = 0 ,
       @AuthorId int = 0 ,
       @SearchQuery varchar(100) = '' ,
       @Debug bit = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF



DECLARE @SQLSTATEMENT nvarchar(max)
SET @SQLSTATEMENT =
'
    SELECT  DailyArticles.ArticleId,
                DailyArticles.DisplayOrder,
                DailyArticles.title,
                da.authorLast,
                da.AuthorId,
                DailyArticles.CoAuthorId,
                DailyArticles.ShowArticle,
                DATEPART(month, DailyArticles.dateposted) AS MonthNum,
                DATENAME(month, DailyArticles.dateposted) AS Month,
                DailyArticles.DatePosted,
                DailyArticles.ShowArticle,
                da.authorFirst + '' '' + da.authorMiddle + '' '' + da.authorLast AS authorName,
                da2.authorFirst + '' '' + da2.authorMiddle + '' '' + da2.authorLast AS CoAuthorName,
                da.Photo,
                da2.Photo As CoPhoto
        FROM    dbo.DailyArticles
                JOIN dbo.DocumentAuthors da ON da.AuthorId = DailyArticles.AuthorId  
                LEFT JOIN dbo.DocumentAuthors da2 ON da2.AuthorId = DailyArticles.CoAuthorId      
        WHERE  1=1'


IF @ShowAll != 1
   SET @SQLSTATEMENT = @SQLSTATEMENT + ' AND ( DailyArticles.ShowArticle = 1 ) '

IF @AuthorId > 0
   SET @SQLSTATEMENT = @SQLSTATEMENT + 'AND ( DailyArticles.AuthorId = ' + CAST(@AuthorId
   AS varchar(7)) + ' OR DailyArticles.CoAuthorId = ' + CAST(@AuthorId AS varchar(7)) +
   ')'

IF LEN(@SearchQuery) > 2
   SET @SQLSTATEMENT = @SQLSTATEMENT +
   ' AND 
(
FREETEXT(ArticleText,@SearchQuery)
OR
description LIKE @SearchQuery
OR
title LIKE @SearchQuery
)'

--'AND ((title LIKE ' + CHAR(39) + '%'  + @SearchQuery      + '%' + CHAR(39) + ' )' +
--'OR (DESCRIPTION LIKE ' + CHAR(39) + '%'  + @SearchQuery      + '%' + CHAR(39) + ' )' +
--'OR (ArticleText LIKE ' + CHAR(39) + '%'  + @SearchQuery      + '%' + CHAR(39) + ' ))'

DECLARE @PARAMS nvarchar(max)
SET @PARAMS = N'@SearchQuery VARCHAR(100) '

SET @SQLSTATEMENT = @SQLSTATEMENT +
' ORDER BY 

DailyArticles.DatePosted DESC,
CASE WHEN DailyArticles.Featured = 1 THEN Featured END,    
CASE WHEN DailyArticles.Headline = 1 THEN Headline END

'

IF @debug = 0 
--        EXEC ( @SQLSTATEMENT )
   EXEC sp_executesql @SQLSTATEMENT , @PARAMS , @SearchQuery
ELSE
   SELECT
       @sqlstatement

--SELECT da.AuthorId, da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'authorName'
--FROM dbo.DocumentAuthors da WHERE da.AuthorId = @AuthorId
GO
/****** Object:  StoredProcedure [dbo].[CountriesGetList]    Script Date: 04/05/2012 12:12:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[CountriesGetList]
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT
          Name ,
          CountryCode ,
          1 AS Sort
      FROM
          AbleCommerce..ac_countries WITH ( NOLOCK )
      WHERE
          CountryCode = 'US'
      UNION ALL
      SELECT
          Name ,
          CountryCode ,
          2 AS Sort
      FROM
          AbleCommerce..ac_countries WITH ( NOLOCK )
      ORDER BY
          SORT


END
GO
/****** Object:  StoredProcedure [dbo].[_Table_Sizes2]    Script Date: 04/05/2012 12:11:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE  [dbo].[_Table_Sizes2] @sortbyrows bit = 0
AS
SET NOCOUNT ON

-- http://www.databasejournal.com/features/mssql/article.php/3414111/Gathering-Space-Usage-Statistics.htm

BEGIN try  
DECLARE @table_name VARCHAR(500) ;  
DECLARE @schema_name VARCHAR(500) ;  
DECLARE @tab1 TABLE( 
        tablename VARCHAR (500) collate database_default 
,       schemaname VARCHAR(500) collate database_default 
);  
DECLARE  @temp_table TABLE (     
        tablename sysname 
,       row_count INT 
,       reserved VARCHAR(50) collate database_default 
,       data VARCHAR(50) collate database_default 
,       index_size VARCHAR(50) collate database_default 
,       unused VARCHAR(50) collate database_default  
);  

INSERT INTO @tab1  
SELECT t1.name 
,       t2.name  
FROM sys.tables t1  
INNER JOIN sys.schemas t2 ON ( t1.schema_id = t2.schema_id );    

DECLARE c1 CURSOR FOR  
SELECT t2.name + '.' + t1.name   
FROM sys.tables t1  
INNER JOIN sys.schemas t2 ON ( t1.schema_id = t2.schema_id );    

OPEN c1;  
FETCH NEXT FROM c1 INTO @table_name; 
WHILE @@FETCH_STATUS = 0  
BEGIN   
        SET @table_name = REPLACE(@table_name, '[','');  
        SET @table_name = REPLACE(@table_name, ']','');  

        -- make sure the object exists before calling sp_spacedused 
        IF EXISTS(SELECT OBJECT_ID FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(@table_name)) 
        BEGIN 
                INSERT INTO @temp_table EXEC sp_spaceused @table_name, false ; 
        END 
         
        FETCH NEXT FROM c1 INTO @table_name;  
END;  
CLOSE c1;  
DEALLOCATE c1;  
SELECT t1.* 
,       t2.schemaname  
FROM @temp_table t1  
INNER JOIN @tab1 t2 ON (t1.tablename = t2.tablename ) 
ORDER BY  row_count desc, schemaname,tablename; 
END try  
BEGIN catch  
SELECT -100 AS l1 
,       ERROR_NUMBER() AS tablename 
,       ERROR_SEVERITY() AS row_count 
,       ERROR_STATE() AS reserved 
,       ERROR_MESSAGE() AS data 
,       1 AS index_size, 1 AS unused, 1 AS schemaname  
END catch
GO
/****** Object:  StoredProcedure [dbo].[_Table_Sizes]    Script Date: 04/05/2012 12:11:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE  [dbo].[_Table_Sizes] @sortbyrows bit = 0
AS
SET NOCOUNT ON

--http://www.sqlteam.com/forums/topic.asp?TOPIC_ID=53843

--EXEC sp_space --show stats sorted by reserved space size
--EXEC sp_space 1 --show stats sorted by row count

SELECT
    CAST(OBJECT_NAME(id) AS varchar(50)) AS name ,
    SUM(CASE
             WHEN indid < 2 THEN rows
        END) AS rows ,
    SUM(reserved) * 8 AS reserved ,
    SUM(dpages) * 8 AS data ,
    SUM(used - dpages) * 8 AS index_size ,
    SUM(reserved - used) * 8 AS unused
FROM
    sysindexes WITH ( NOLOCK )
WHERE
    indid IN ( 0 , 1 , 255 ) AND id > 100
GROUP BY
    id
    WITH ROLLUP
ORDER BY
    CASE
         WHEN @sortbyrows = 1 THEN SUM(CASE
                                            WHEN indid < 2 THEN rows
                                       END)
         ELSE SUM(reserved) * 8
    END DESC
GO
/****** Object:  StoredProcedure [dbo].[_ServerStatus]    Script Date: 04/05/2012 12:11:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[_ServerStatus] 
	
AS
BEGIN

	EXEC sp_helpdb	
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[_SearchAndReplace]    Script Date: 04/05/2012 12:11:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE  [dbo].[_SearchAndReplace]
(
 @SearchStr varchar(100) ,
 @ReplaceStr varchar(100) )
AS
BEGIN

	-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
	-- Purpose: To search all columns of all tables for a given search string and replace it with another string
	-- Written by: Narayana Vyas Kondreddi
	-- Site: http://vyaskn.tripod.com
	-- Tested on: SQL Server 7.0 and SQL Server 2000
	-- Date modified: 2nd November 2002 13:50 GMT

      SET NOCOUNT ON

      DECLARE
              @TableName varchar(256) ,
              @ColumnName varchar(128) ,
              @SearchStr2 varchar(110) ,
              @SQL varchar(MAX) ,
              @RCTR int
      SET @TableName = ''
      SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%' , '''')
      SET @RCTR = 0
      WHILE @TableName IS NOT NULL
            BEGIN
                  SET @ColumnName = ''
                  SET @TableName = ( SELECT
                                         MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(
                                         TABLE_NAME))
                                     FROM
                                         INFORMATION_SCHEMA.TABLES
                                     WHERE
                                         TABLE_TYPE = 'BASE TABLE' AND QUOTENAME(
                                         TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) >
                                         @TableName AND OBJECTPROPERTY(OBJECT_ID(
                                         QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(
                                         TABLE_NAME)) , 'IsMSShipped') = 0 )
                  WHILE ( @TableName IS NOT NULL ) AND ( @ColumnName IS NOT NULL )
                        BEGIN
                              SET @ColumnName = ( SELECT
                                                      MIN(QUOTENAME(COLUMN_NAME))
                                                  FROM
                                                      INFORMATION_SCHEMA.COLUMNS
                                                  WHERE
                                                      TABLE_SCHEMA = PARSENAME(@TableName ,
                                                      2) AND TABLE_NAME = PARSENAME(
                                                      @TableName , 1) AND DATA_TYPE IN (
                                                      'char' , 'varchar' , 'nchar' ,
                                                      'nvarchar' ) AND QUOTENAME(
                                                      COLUMN_NAME) > @ColumnName )

                              IF @ColumnName IS NOT NULL
                                 BEGIN
                                       SET @SQL = 'UPDATE ' + @TableName + ' SET ' +
                                       @ColumnName + ' =  REPLACE(' + @ColumnName + ', '
                                       + QUOTENAME(@SearchStr , '''') + ', ' + QUOTENAME(
                                       @ReplaceStr , '''') + ') WHERE ' + @ColumnName +
                                       ' LIKE ' + @SearchStr2
                                       EXEC ( @SQL )
                                       SET @RCTR = @RCTR + @@ROWCOUNT
                                 END
                        END
            END

      SELECT
          'Replaced ' + CAST(@RCTR AS varchar) + ' occurence(s)' AS 'Outcome'
END
GO
/****** Object:  StoredProcedure [dbo].[_SearchAllTables]    Script Date: 04/05/2012 12:11:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE  [dbo].[_SearchAllTables]
(
 @SearchStr nvarchar(100) )
AS
BEGIN


--To search all columns of all tables in Pubs database for the keyword "Computer"
--EXEC SearchAllTables 'Computer'
--GO
--
--Here is the complete stored procedure code:


	-- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
	-- Purpose: To search all columns of all tables for a given search string
	-- Written by: Narayana Vyas Kondreddi
	-- Site: http://vyaskn.tripod.com
	-- Tested on: SQL Server 7.0 and SQL Server 2000
	-- Date modified: 28th July 2002 22:50 GMT


      CREATE  TABLE #Results
      (
        ColumnName nvarchar(370) ,
        ColumnValue nvarchar(3630) )

      SET NOCOUNT ON

      DECLARE
              @TableName nvarchar(256) ,
              @ColumnName nvarchar(128) ,
              @SearchStr2 nvarchar(110)
      SET @TableName = ''
      SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%' , '''')
      WHILE @TableName IS NOT NULL
            BEGIN
                  SET @ColumnName = ''
                  SET @TableName = ( SELECT
                                         MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(
                                         TABLE_NAME))
                                     FROM
                                         INFORMATION_SCHEMA.TABLES
                                     WHERE
                                         TABLE_TYPE = 'BASE TABLE' AND QUOTENAME(
                                         TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) >
                                         @TableName AND OBJECTPROPERTY(OBJECT_ID(
                                         QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(
                                         TABLE_NAME)) , 'IsMSShipped') = 0 )
                  WHILE ( @TableName IS NOT NULL ) AND ( @ColumnName IS NOT NULL )
                        BEGIN
                              SET @ColumnName = ( SELECT
                                                      MIN(QUOTENAME(COLUMN_NAME))
                                                  FROM
                                                      INFORMATION_SCHEMA.COLUMNS
                                                  WHERE
                                                      TABLE_SCHEMA = PARSENAME(@TableName ,
                                                      2) AND TABLE_NAME = PARSENAME(
                                                      @TableName , 1) AND DATA_TYPE IN (
                                                      'char' , 'varchar' , 'nchar' ,
                                                      'nvarchar' ) AND QUOTENAME(
                                                      COLUMN_NAME) > @ColumnName )

                              IF @ColumnName IS NOT NULL
                                 BEGIN
                                       INSERT INTO
                                           #Results
                                           EXEC ( 'SELECT '''+@TableName+'.'+@ColumnName+
                                           ''', LEFT('+@ColumnName+', 3630) 
					FROM '
                                           +@TableName+' (NOLOCK) '+' WHERE '+@ColumnName
                                           +' LIKE '+@SearchStr2 )
                                 END
                        END
            END

      SELECT
          ColumnName ,
          ColumnValue
      FROM
          #Results
END
GO
/****** Object:  StoredProcedure [dbo].[_DatabaseStatus]    Script Date: 04/05/2012 12:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[_DatabaseStatus] 
	
AS
BEGIN	
	SET NOCOUNT ON;


--All utility procs in the Mises DB start with an underscore.

--Here are the current ones:

--_ServerStatus: The size of each database

--_DatabaseStatus : Get a list of the longest-running queries

--_DeleteOldData: Delete old logging data in tables when you need to shrink the db

--_FixHTML: Does a bunch of search and replace commands to optimize the HTML in the database.  Ongoing project.  Time consuming!  Only run at night!

--_JLStoDocuments: Copies info from the jls table into the Documents structure

--_MediaCategoryUpdateImages: Update Media category images

--_SearchAndReplace: Be careful with this!  Global search and replace.

--_SearchDocumentText: use this before using searchandreplace

--_Table_Sizes2: Get the dize of each table

--_Table_Sizes: old version of this

    --exec sp_who2
    
    select 
    source_code,stats.total_elapsed_time/1000000 as seconds,last_execution_time 
from sys.dm_exec_query_stats as stats 
    cross apply(SELECT text as source_code FROM sys.dm_exec_sql_text(sql_handle))AS query_text 
order by total_elapsed_time desc
    
END
GO
/****** Object:  UserDefinedFunction [dbo].[SplitStringTable]    Script Date: 04/05/2012 12:13:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/***************************************************************************************************************************
Author:		Vasily Kabanov
Created:	23/07/2007
Parameters:	@vallist - the string containing a list of individual string values delimited by @delimiter
			@delimiter - the character sequence used to separate individual values in @vallist
Returns:	table (
				val nvarchar(100) not null - non-empty trimmed value
			)
Comment:	The utility function converts a delimited list of values into table format.
			Individual values are trimmed from both ends and empty values are removed from the result.
			Hence
				select * from dbo.SplitStringTable('one,two,three', ',')
			returns the same result as  
				select * from dbo.SplitStringTable(' one , two  ,,  , three ', ',')
Examples:	compare performance of

select		*
from		[SplitCVS](replicate(convert(varchar(max), 'inflation,'), 100000))

and

select		*
from		dbo.SplitStringTable(replicate(convert(varchar(max), 'inflation,'), 100000), ',')

***************************************************************************************************************************/
create function [dbo].[SplitStringTable] (@vallist nvarchar(max), @delimiter nvarchar(4))
returns @tbret table (val nvarchar(100) not null)
as
begin
	declare @i1 int, @i2 int;
	declare @length int;
	declare @delim_len int;
	declare @val nvarchar(100);
	set @delim_len = len(@delimiter);
	set @length = len(@vallist);
	-- current start
	set @i1 = 1;
	-- next start
	while @i1 <= @length
	begin
		set @i2 = charindex(@delimiter, @vallist, @i1);
		if @i2 = 0
		begin
			set @i2 = @length + 1;
		end
		if @i2 >= @i1
		begin
			-- insert found substring
			set @val = ltrim(rtrim(substring(@vallist,@i1,@i2-@i1)));
			if @val != N''
			begin
				insert @tbret values (@val);
			end
			set @i1 = @i2 + @delim_len;
		end
		else
		begin
			-- scroll to the end of the string, done
			set @i1 = @length + 1;
		end
	end
	return
end
GO
/****** Object:  UserDefinedFunction [dbo].[SplitCVS]    Script Date: 04/05/2012 12:13:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitCVS]
    (
      @OrderList VARCHAR(MAX)
    )
RETURNS @ParsedList TABLE ( OrderID VARCHAR(max) )
AS BEGIN
    DECLARE @OrderID VARCHAR(max),
        @Pos VARCHAR(max)
    SET @OrderList = LTRIM(RTRIM(@OrderList)) + ','
    SET @Pos = CHARINDEX(',', @OrderList, 1)
    IF REPLACE(@OrderList, ',', '') <> '' 
        BEGIN
            WHILE @Pos > 0
                BEGIN
                    SET @OrderID = LTRIM(RTRIM(LEFT(@OrderList, @Pos - 1)))
                    IF @OrderID <> '' 
                        BEGIN
                            INSERT  INTO @ParsedList ( OrderId )
                            VALUES  (
                                      CAST(@OrderID AS VARCHAR(max)) 
                                    ) --Use Appropriate conversion
                        END
                    SET @OrderList = RIGHT(@OrderList, LEN(@OrderList) - @Pos)
                    SET @Pos = CHARINDEX(',', @OrderList, 1)
                END
        END	
    RETURN
   END
GO
/****** Object:  StoredProcedure [dbo].[util_rethrow_exception]    Script Date: 04/05/2012 12:13:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}
/***********************************************************************************
Author:		Vasily
Created:	4/07/2007
Comment:	The utility procedure is intended to be used in CATCH blocks
			to re-throw the caught exception after handling it
			returns error number
Examples:	
			declare @retcode int
			set @retcode = 0
			begin try
				raiserror('test', 16, 1)
				print 'must not be printed'
			end try
			begin catch
				exec @retcode = dbo.util_rethrow_exception
			end catch
			print 'retcode = ' + convert(varchar(10), @retcode)
***********************************************************************************/
CREATE  PROCEDURE [dbo].[util_rethrow_exception]
as
set nocount on
	DECLARE 
			@ErrorMessage    NVARCHAR(4000),
			@ErrorNumber     INT,
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine       INT,
			@ErrorProcedure  NVARCHAR(200);
    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	if isnull(@ErrorNumber,0) = 0
		goto _exit

    -- Building the message string that will contain original
    -- error information.
    SELECT @ErrorMessage = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	_exit:
return @ErrorNumber
GO
/****** Object:  StoredProcedure [dbo].[StoreGetBestsellers]    Script Date: 04/05/2012 12:13:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[StoreGetBestsellers]
                @MaxItems INT  = 5
AS
  BEGIN
    SET ROWCOUNT  @MaxItems
    
    SELECT   ac_PRODUCTS.ProductId,
             ac_PRODUCTS.Name,
             --CAST(ac_PRODUCTS.Description1 AS VARCHAR(MAX)) + ' ' +
             CAST(ac_PRODUCTS.Description AS VARCHAR(500)) + '...' AS DESCRIPTION,
             ac_Manufacturers.Name AS Author,
             ac_Manufacturers.ManufacturerId AS ManufacturerId,
             ac_PRODUCTS.Sku                                                       AS GUID,
             'product.aspx?ProductId=' + CAST(ac_PRODUCTS.ProductId AS VARCHAR(MAX)) AS DisplayPage,
             ac_Products.CreatedDate,
             ac_Products.Price,
             ac_Products.ThumbnailUrl,
             SUM(ac_ORDERITEMS.Quantity)                                             AS TotalQuantity,
             SUM(ac_ORDERITEMS.Price * ac_ORDERITEMS.Quantity)                     AS TotalSales
    FROM     (AbleCommerce..ac_ORDERS
              INNER JOIN AbleCommerce..ac_ORDERITEMS
                ON ac_ORDERITEMS.OrderId = ac_ORDERS.OrderId)
             INNER JOIN AbleCommerce..ac_PRODUCTS
               ON ac_ORDERITEMS.ProductId = ac_PRODUCTS.ProductId
             INNER JOIN AbleCommerce..ac_Manufacturers
               ON ac_Manufacturers.ManufacturerId = ac_Products.ManufacturerId
    WHERE    ac_PRODUCTS.DisablePurchase = 0
             AND ac_PRODUCTS.VisibilityId = 0
             --AND ac_ORDERS.StoreId = 3
             AND OrderDate > GETDATE() - 15
             AND ac_ORDERS.OrderDate IS NOT NULL
             AND ac_ORDERITEMS.ProductId > 0
             AND ac_Products.Price > 1
             AND ac_Products.ProductId <> 157
    GROUP BY ac_PRODUCTS.ProductId,
             ac_PRODUCTS.Name,
             ac_PRODUCTS.DisplayPage,
             ac_PRODUCTS.Description,
             ac_PRODUCTS.Sku,
             ac_PRODUCTS.ThumbnailUrl,
             ac_PRODUCTS.Price,
             ac_Manufacturers.Name,
             ac_Products.CreatedDate,
             ac_Manufacturers.ManufacturerId
    ORDER BY SUM(ac_ORDERITEMS.Quantity) DESC
--             SUM(ac_ORDERITEMS.Price * ac_ORDERITEMS.Quantity) DESC
  END
GO
/****** Object:  StoredProcedure [dbo].[spUpdateHomepageItemOrder]    Script Date: 04/05/2012 12:13:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spUpdateHomepageItemOrder]
(
 @Itemid int ,
 @DisplayOrder int )
AS
UPDATE
    HomepageItems
SET
    Itemorder = @DisplayOrder
WHERE
    ItemId = @ItemId
GO
/****** Object:  UserDefinedFunction [dbo].[SplitUniqueIntTable]    Script Date: 04/05/2012 12:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/***************************************************************************************************************************
Author:			Vasily Kabanov
Created:		23/07/2007
Parameters:		@vallist - the delimited list of values to parse
				@delimiter - the character sequence used to separate individual values in the string
				@ignore_conversion_errors - if 0, then parsing fails if there's a non-numeric value in the list, otherwise
					all non-numeric values are silently ignored
Comment:		The utility function transforms a flat text string list of values into typed tabular form.
				Safe to use in the from clause (returned table contains primary key: quick lookups etc)
Examples:		
				select * from dbo.SplitUniqueIntTable('1,2,3,6,5', N',', 0)
***************************************************************************************************************************/
CREATE function [dbo].[SplitUniqueIntTable] (
	@vallist nvarchar(max), @delimiter nvarchar(4) = N';', @ignore_conversion_errors bit = 1)
returns @tbret table (val int not null primary key)
as
begin
	insert				@tbret
	select distinct		convert(int, val)
	from				dbo.SplitStringTable(@vallist, @delimiter)
	where				(@ignore_conversion_errors = 0 or 1 = isnumeric(val))
	return
end
GO
/****** Object:  StoredProcedure [dbo].[WardLibraryGetList]    Script Date: 04/05/2012 12:13:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[WardLibraryGetList]
       @authorFirst varchar(max) ,
       @authorLast varchar(max) ,
       @title varchar(max) ,
       @subject varchar(max)
AS
BEGIN
      SELECT
          control ,
          display ,
          title ,
          checked_out_by ,
          authorfirst ,
          authorlast ,
          authorfirst2 ,
          authorlast2 ,
          authorfirst3 ,
          authorlast3 ,
          coauthors ,
          subject
      FROM
          wardlibrary



END
GO
/****** Object:  StoredProcedure [dbo].[WardLibraryGetBook]    Script Date: 04/05/2012 12:13:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[WardLibraryGetBook] @BookId int = 0
AS
BEGIN
      SELECT
          * ,
          AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName
      FROM
          wardlibrary
      WHERE
          CONTROL = @BookId
END
GO
/****** Object:  StoredProcedure [dbo].[spAddBookReview]    Script Date: 04/05/2012 12:12:56 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spAddBookReview]
(
 @BookId int ,
 @Rating tinyint ,
 @Title varchar(255) ,
 @Review varchar(8000) ,
 @ScreenName varchar(255) ,
 @Location varchar(255) )
AS
INSERT INTO
    BookReviews
    (
      BookId ,
      Rating ,
      Title ,
      Review ,
      ScreenName ,
      Location ,
      ReviewDate )
VALUES
    (
      @BookId ,
      @Rating ,
      @Title ,
      @Review ,
      @ScreenName ,
      @Location ,
      GETDATE() )


SELECT
    SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[spGetAllQuotes]    Script Date: 04/05/2012 12:12:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spGetAllQuotes]
       @Subject varchar(50) = NULL ,
       @AuthorId int = 0 ,
       @Source varchar(30) = NULL ,
       @SearchQuery varchar(50) = NULL
AS
IF LEN(@Subject) > 0
   SELECT
       AuthorFirst + ' ' + AuthorLast AS [Author] ,
       [Quote] ,
       [Source] ,
       [Page] ,
       [Subject]
   FROM
       Quotes JOIN DocumentAuthors
   ON  Quotes.AuthorId = DocumentAuthors.AuthorId
   WHERE
       Subject LIKE '%' + @Subject + '%'
ELSE
   IF @AuthorId > 0
      SELECT
          AuthorFirst + ' ' + AuthorLast AS [Author] ,
          [Quote] ,
          [Source] ,
          [Page] ,
          [Subject]
      FROM
          Quotes JOIN DocumentAuthors
      ON  Quotes.AuthorId = DocumentAuthors.AuthorId
      WHERE
          Quotes.AuthorId = @AuthorId
   ELSE
      IF LEN(@SOURCE) > 0
         SELECT
             AuthorFirst + ' ' + AuthorLast AS [Author] ,
             [Quote] ,
             [Source] ,
             [Page] ,
             [Subject]
         FROM
             Quotes JOIN DocumentAuthors
         ON  Quotes.AuthorId = DocumentAuthors.AuthorId
         WHERE
             Source LIKE '%' + @Source + '%'
      ELSE
         IF LEN(@SearchQuery) > 0
            SELECT
                AuthorFirst + ' ' + AuthorLast AS [Author] ,
                [Quote] ,
                [Source] ,
                [Page] ,
                [Subject]
            FROM
                Quotes JOIN DocumentAuthors
            ON  Quotes.AuthorId = DocumentAuthors.AuthorId
            WHERE
                Author LIKE '%' + @SearchQuery + '%' OR Quote LIKE '%' + @SearchQuery +
                '%'
         ELSE
            SELECT
                AuthorFirst + ' ' + AuthorLast AS [Author] ,
                [Quote] ,
                [Source] ,
                [Page] ,
                [Subject]
            FROM
                Quotes JOIN DocumentAuthors
            ON  Quotes.AuthorId = DocumentAuthors.AuthorId
GO
/****** Object:  StoredProcedure [dbo].[spGetQuotes]    Script Date: 04/05/2012 12:13:00 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[spGetQuotes]
       @Subject varchar(50) = NULL ,
       @AuthorId int = 0 ,
       @Source varchar(30) = NULL ,
       @SearchQuery varchar(50) = NULL
AS
IF LEN(@Subject) > 0
   SELECT
       AuthorFirst + ' ' + AuthorLast AS [Author] ,
       [Quote] ,
       [Source] ,
       [Page] ,
       [Subject]
   FROM
       [Quotes] JOIN DocumentAuthors
   ON  Quotes.AuthorId = DocumentAuthors.AuthorId
   WHERE
       Subject LIKE '%' + @Subject + '%'
ELSE
   IF @AuthorId > 0
      SELECT
          AuthorFirst + ' ' + AuthorLast AS [Author] ,
          [Quote] ,
          [Source] ,
          [Page] ,
          [Subject]
      FROM
          [Quotes] JOIN DocumentAuthors
      ON  Quotes.AuthorId = DocumentAuthors.AuthorId
      WHERE
          Quotes.AuthorId = @AuthorId
   ELSE
      IF LEN(@SOURCE) > 0
         SELECT
             AuthorFirst + ' ' + AuthorLast AS [Author] ,
             [Quote] ,
             [Source] ,
             [Page] ,
             [Subject]
         FROM
             [Quotes] JOIN DocumentAuthors
         ON  Quotes.AuthorId = DocumentAuthors.AuthorId
         WHERE
             Source LIKE '%' + @Source + '%'
      ELSE
         IF LEN(@SearchQuery) > 0
            SELECT
                AuthorFirst + ' ' + AuthorLast AS [Author] ,
                [Quote] ,
                [Source] ,
                [Page] ,
                [Subject]
            FROM
                [Quotes] JOIN DocumentAuthors
            ON  Quotes.AuthorId = DocumentAuthors.AuthorId
            WHERE
                Author LIKE '%' + @SearchQuery + '%' OR Quote LIKE '%' + @SearchQuery +
                '%'
         ELSE
            SELECT
                AuthorFirst + ' ' + AuthorLast AS [Author] ,
                [Quote] ,
                [Source] ,
                [Page] ,
                [Subject]
            FROM
                [Quotes] JOIN DocumentAuthors
            ON  Quotes.AuthorId = DocumentAuthors.AuthorId
GO
/****** Object:  StoredProcedure [dbo].[spGetEventDetails]    Script Date: 04/05/2012 12:13:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spGetEventDetails] @EventId int
AS
SELECT
    Calendar.EventId ,
    Calendar.GUID ,
    Calendar.Title ,
    Calendar.EventDate ,
    Calendar.EndDate ,
    Calendar.IntroText ,
    Calendar.Description ,
    Calendar.Location ,
    Calendar.Display ,
    Calendar.CreateDate
FROM
    Calendar
WHERE
    EventId = @EventId
GO
/****** Object:  StoredProcedure [dbo].[spGetBookReviews]    Script Date: 04/05/2012 12:12:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spGetBookReviews] @BookId int
AS
SELECT
    BookReviews.ReviewId ,
    BookReviews.BookId ,
    BookReviews.Rating ,
    BookReviews.Title ,
    BookReviews.Review ,
    BookReviews.ScreenName ,
    BookReviews.Location ,
    BookReviews.ReviewDate
FROM
    BookReviews
WHERE
    BookId = @BookId
ORDER BY
    ReviewId
GO
/****** Object:  StoredProcedure [dbo].[RevisionGetListByGUID]    Script Date: 04/05/2012 12:12:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[RevisionGetListByGUID] @DocumentGUID uniqueidentifier
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT
          RevisionId ,
          DocumentGUID ,
          LEN(DocumentText) AS Size ,
          Editor ,
          EditDate
      FROM
          dbo.Revision
      WHERE
          DocumentGUID = @DocumentGUID
END
GO
/****** Object:  StoredProcedure [dbo].[RevisionGetByRevisionId]    Script Date: 04/05/2012 12:12:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[RevisionGetByRevisionId] @RevisionId int
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT
          RevisionId ,
          DocumentGUID ,
          DocumentText ,
          Editor ,
          EditDate
      FROM
          dbo.Revision
      WHERE
          RevisionId = @RevisionId
END
GO
/****** Object:  StoredProcedure [dbo].[RevisionCreate]    Script Date: 04/05/2012 12:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[RevisionCreate]
       @RevisionId int OUTPUT ,
       @DocumentGUID uniqueidentifier ,
       @DocumentText varchar(max) ,
       @Editor varchar(50)
AS
BEGIN

      IF EXISTS ( SELECT
                      DocumentText
                  FROM
                      dbo.Revision
                  WHERE
                      DocumentGUID = @DocumentGUID AND DocumentText = @DocumentText )
         RETURN

      INSERT INTO
          dbo.Revision
          (
            DocumentGUID ,
            DocumentText ,
            Editor ,
            EditDate )
      VALUES
          (
            @DocumentGUID ,
            @DocumentText ,
            @Editor ,
            GETDATE() )

      SET @RevisionId = SCOPE_IDENTITY()

END
GO
/****** Object:  UserDefinedFunction [dbo].[RegExReplace]    Script Date: 04/05/2012 12:13:23 ******/
CREATE FUNCTION [dbo].[RegExReplace](@expression [nvarchar](max), @pattern [nvarchar](max), @replace [nvarchar](max))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [regexReplace].[UserDefinedFunctions].[RegExReplace]
GO
EXEC sys.sp_addextendedproperty @name=N'AutoDeployed', @value=N'yes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'RegExReplace'
GO
EXEC sys.sp_addextendedproperty @name=N'SqlAssemblyFile', @value=N'RegExFunction.cs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'RegExReplace'
GO
EXEC sys.sp_addextendedproperty @name=N'SqlAssemblyFileLine', @value=10 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'RegExReplace'
GO
/****** Object:  View [dbo].[ProductsView]    Script Date: 04/05/2012 12:07:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ProductsView]
AS
SELECT     AbleCommerce.dbo.ac_Products.GUID, AbleCommerce.dbo.ac_Products.Name, AbleCommerce.dbo.ac_Products.Price, AbleCommerce.dbo.ac_Products.CostOfGoods, 
                      AbleCommerce.dbo.ac_Products.MSRP, AbleCommerce.dbo.ac_Products.Weight, AbleCommerce.dbo.ac_Products.Length, AbleCommerce.dbo.ac_Products.Width, 
                      AbleCommerce.dbo.ac_Products.Height, AbleCommerce.dbo.ac_Products.Sku, AbleCommerce.dbo.ac_Products.ModelNumber, 
                      AbleCommerce.dbo.ac_Products.ImageAltText, AbleCommerce.dbo.ac_Products.Summary, AbleCommerce.dbo.ac_Products.Description, 
                      AbleCommerce.dbo.ac_Products.ExtendedDescription, AbleCommerce.dbo.ac_Manufacturers.Name AS Author, AbleCommerce.dbo.ac_Products.ThumbnailUrl, 
                      AbleCommerce.dbo.ac_Products.ThumbnailAltText, AbleCommerce.dbo.ac_Products.ImageUrl, AbleCommerce.dbo.ac_Products.CreatedDate, 
                      AbleCommerce.dbo.ac_Products.LastModifiedDate, AbleCommerce.dbo.ac_Products.SearchKeywords, AbleCommerce.dbo.ac_Products.InStock, 
                      AbleCommerce.dbo.ac_Products.ProductId
FROM         AbleCommerce.dbo.ac_Products INNER JOIN
                      AbleCommerce.dbo.ac_Manufacturers ON AbleCommerce.dbo.ac_Products.ManufacturerId = AbleCommerce.dbo.ac_Manufacturers.ManufacturerId
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ac_Products (AbleCommerce.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ac_Manufacturers (AbleCommerce.dbo)"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 110
               Right = 430
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProductsView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProductsView'
GO
/****** Object:  StoredProcedure [dbo].[ProductGetRandom]    Script Date: 04/05/2012 12:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[ProductGetRandom]
@Id INT = 0
AS 

IF (@Id >0)
BEGIN
	
	SELECT ProductId,
               products.Name,
               [ac_Manufacturers].[Name]           AS Author,
               [ac_Manufacturers].[ManufacturerId] AS AuthorId,               
               [ThumbnailUrl],
               [ImageUrl],
               '/store/Product.aspx?ProductId=' + CAST (ProductId AS VARCHAR(MAX)) +  '&utm_source=Homepage&utm_medium=FeaturedProd&utm_term=Widget&utm_campaign=Featured_Widget' AS URL,
               [ThumbnailAltText],
               [Summary],
               Price,
               MSRP
  FROM    AbleCommerce.dbo.ac_Products products WITH ( NOLOCK )
            INNER JOIN AbleCommerce.dbo.[ac_Manufacturers] [ac_Manufacturers]
            WITH ( NOLOCK ) ON [ac_Manufacturers].[ManufacturerId] = products.[ManufacturerId]
  WHERE  ProductId = @Id AND products.DisablePurchase = 0
END 

    SELECT TOP 1
            ProductId ,
            products.Name ,
            [ac_Manufacturers].[Name] AS Author ,
            [ac_Manufacturers].[ManufacturerId] AS AuthorId ,
            [ThumbnailUrl],
             [ImageUrl],
            '/store/Product.aspx?ProductId=' + CAST (ProductId AS VARCHAR(MAX)) +  '&utm_source=Homepage&utm_medium=FeaturedProd&utm_term=Widget&utm_campaign=Featured_Widget' AS URL,
            [ThumbnailAltText] ,
            [Summary] ,
            Price,
            MSRP
    FROM    AbleCommerce.dbo.ac_Products products WITH ( NOLOCK )
            INNER JOIN AbleCommerce.dbo.[ac_Manufacturers] [ac_Manufacturers]
            WITH ( NOLOCK ) ON [ac_Manufacturers].[ManufacturerId] = products.[ManufacturerId]
            WHERE products.DisablePurchase = 0 AND InStock > 0
    ORDER BY NEWID()
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalUpdateStory]    Script Date: 04/05/2012 12:12:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PeriodicalUpdateStory]
       @PeriodicalId int ,
       @StoryId int ,
       @EditedBy varchar(50) ,
       @authorfirst varchar(50) ,
       @authorlast varchar(50) ,
       @authorfirst2 varchar(50) ,
       @authorlast2 varchar(50) ,
       @authorfirst3 varchar(50) ,
       @authorlast3 varchar(50) ,
       @title varchar(255) ,
       @body varchar(max) ,
       @issue_season int ,
       @issue_year int
AS
BEGIN


      UPDATE
          misesreview
      SET
          authorfirst = @authorfirst ,
          authorlast = @authorlast ,
          authorfirst2 = @authorfirst2 ,
          authorlast2 = @authorlast2 ,
          authorfirst3 = @authorfirst3 ,
          authorlast3 = @authorlast3 ,
          title = @title ,
          body = @body ,
          issue_season = @issue_season ,
          issue_year = @issue_year
      WHERE
          dbo.misesreview.[control] = @StoryId



END
GO
/****** Object:  View [dbo].[PeriodicalsView]    Script Date: 04/05/2012 12:07:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PeriodicalsView]
AS SELECT
       dbo.Periodicals.Title AS Journal , dbo.Periodicals.PeriodicalId AS PeriodicalId ,       
       dbo.freemarket.GUID AS GUID ,
       dbo.DocumentAuthors.AuthorId ,
       dbo.freemarket.title ,
       dbo.freemarket.authorfirst + ' ' + dbo.freemarket.authorlast AS AuthorName ,
       dbo.freemarket.authorlast ,
       CAST(DATENAME(month , dbo.freemarket.articledate) AS varchar(max)) + ' ' + CAST(
       DATENAME(year , dbo.freemarket.articledate) AS varchar(max)) AS Volume ,
       dbo.freemarket.articledate AS DatePosted ,
       '/freemarket_detail.aspx?control=' + CAST(dbo.freemarket.control AS varchar(max)) AS URL ,
       3 AS MediaTypeId
   FROM
       dbo.freemarket INNER JOIN dbo.Periodicals
   ON  dbo.Periodicals.PeriodicalId = 1 LEFT OUTER JOIN dbo.DocumentAuthors
   ON  dbo.DocumentAuthors.AuthorLast = dbo.freemarket.authorlast AND dbo.DocumentAuthors
       .AuthorFirst = dbo.freemarket.authorfirst
   UNION
   SELECT
       Periodicals_6.Title AS Journal ,  Periodicals_6.PeriodicalId AS PeriodicalId ,       
       dbo.misesreview.GUID AS GUID ,
       DocumentAuthors_6.AuthorId ,
       dbo.misesreview.title ,
       dbo.misesreview.authorfirst + ' ' + dbo.misesreview.authorlast AS AuthorName ,
       dbo.misesreview.authorlast ,
       CASE misesreview.issue_season
         WHEN 1 THEN 'Spring'
         WHEN 2 THEN 'Summer'
         WHEN 3 THEN 'Fall'
         WHEN 4 THEN 'Winter'
       END + ' ' + CAST(dbo.misesreview.issue_year AS varchar(max)) AS Volume ,
       dbo.misesreview.CreateDate AS DatePosted ,
       '/misesreview_detail.aspx?control=' + CAST(dbo.misesreview.control AS varchar(max)) AS URL ,
       5 AS MediaTypeId
   FROM
       dbo.misesreview INNER JOIN dbo.Periodicals AS Periodicals_6
   ON  Periodicals_6.PeriodicalId = 2 LEFT OUTER JOIN dbo.DocumentAuthors AS
   DocumentAuthors_6
   ON  DocumentAuthors_6.AuthorLast = dbo.misesreview.authorlast AND dbo.misesreview.
       authorfirst LIKE '%' + DocumentAuthors_6.AuthorFirst + '%'
   UNION
   SELECT
       Periodicals_5.Title AS Journal ,  Periodicals_5.PeriodicalId AS PeriodicalId ,       
       dbo.jls.GUID ,
       DocumentAuthors_5.AuthorId ,
       dbo.jls.title ,
       dbo.jls.authorFirst1 + ' ' + dbo.jls.authorLast1 AS AuthorName ,
       dbo.jls.authorLast1 AS AuthorLast ,
       'Vol. ' + CAST(dbo.jls.volume AS varchar(max)) + ' Num. ' + CAST(dbo.jls.number AS
       varchar(max)) AS Volume ,
       DATEADD(year , 1976 + dbo.jls.volume - 1900 , 0) AS DatePosted ,
       '/journals/jls/' + CAST(dbo.jls.volume AS varchar(max)) + '_' + CAST(dbo.jls.
       number AS varchar(max)) + '/' + CAST(dbo.jls.volume AS varchar(max)) + '_' + CAST(
       dbo.jls.number AS varchar(max)) + '_' + CAST(dbo.jls.articleNum AS varchar(max)) +
       '.pdf' AS URL ,
       3 AS MediaTypeId
   FROM
       dbo.jls INNER JOIN dbo.Periodicals AS Periodicals_5
   ON  Periodicals_5.PeriodicalId = 3 LEFT OUTER JOIN dbo.DocumentAuthors AS
   DocumentAuthors_5
   ON  DocumentAuthors_5.AuthorLast = dbo.jls.authorLast1 AND dbo.jls.authorFirst1 LIKE
       '%' + DocumentAuthors_5.AuthorFirst + '%'
   UNION
   SELECT
       Periodicals_4.Title AS Journal ,  Periodicals_4.PeriodicalId AS PeriodicalId ,       
       dbo.qjaeDB.GUID ,
       DocumentAuthors_4.AuthorId ,
       dbo.qjaeDB.title ,
       dbo.qjaeDB.authorFirst1 + ' ' + dbo.qjaeDB.authorLast1 AS AuthorName ,
       dbo.qjaeDB.authorLast1 AS AuthorLast ,
       'Vol. ' + CAST(dbo.qjaeDB.volume AS varchar(max)) + ' Num. ' + CAST(dbo.qjaeDB.
       number AS varchar(max)) AS Volume ,
       dbo.qjaeDB.CreateDate AS DatePosted,
      -- DATEADD(year , 1998 + dbo.qjaeDB.volume - 1900 , 0) AS DatePosted ,
       '/journals/qjae/pdf/qjae' + CAST(dbo.qjaeDB.volume AS varchar(max)) + '_' + CAST(
       dbo.qjaeDB.number AS varchar(max)) + '_' + CAST(dbo.qjaeDB.articleNum AS varchar(
       max)) + '.pdf' AS URL ,
       3 AS MediaTypeId
   FROM
       dbo.qjaeDB INNER JOIN dbo.Periodicals AS Periodicals_4
   ON  Periodicals_4.PeriodicalId = 4 LEFT OUTER JOIN dbo.DocumentAuthors AS
   DocumentAuthors_4
   ON  dbo.qjaeDB.Author1 = DocumentAuthors_4.AuthorId OR dbo.qjaeDB.Author2 =
       DocumentAuthors_4.AuthorId
   UNION
   SELECT
       Periodicals_3.Title AS Journal , Periodicals_3.PeriodicalId AS PeriodicalId ,       
       dbo.raeDB.GUID ,
       DocumentAuthors_3.AuthorId ,
       dbo.raeDB.title ,
       dbo.raeDB.authorFirst1 + ' ' + dbo.raeDB.authorLast1 AS AuthorName ,
       dbo.raeDB.authorLast1 AS AuthorLast ,
       'Vol. ' + CAST(dbo.raeDB.volume AS varchar(max)) + ' Num. ' + CAST(dbo.raeDB.
       number AS varchar(max)) AS Volume ,
       DATEADD(year , 1986 + dbo.raeDB.volume - 1900 , 0) AS DatePosted ,
       '/journals/rae/pdf/RAE' + CAST(dbo.raeDB.volume AS varchar(max)) + '_' + CAST(dbo.
       raeDB.number AS varchar(max)) + '_' + CAST(dbo.raeDB.articleNum AS varchar(max)) +
       '.pdf' AS URL ,
       3 AS MediaTypeId
   FROM
       dbo.raeDB INNER JOIN dbo.Periodicals AS Periodicals_3
   ON  Periodicals_3.PeriodicalId = 5 LEFT OUTER JOIN dbo.DocumentAuthors AS
   DocumentAuthors_3
   ON  DocumentAuthors_3.AuthorLast = dbo.raeDB.authorLast1 AND dbo.raeDB.authorFirst1
       LIKE '%' + DocumentAuthors_3.AuthorFirst + '%'
   UNION
   SELECT
       Periodicals_2.Title AS Journal ,  Periodicals_2.PeriodicalId AS PeriodicalId ,       
       NULL AS GUID ,
       DocumentAuthors_2.AuthorId ,
       dbo.aenDB.title ,
       dbo.aenDB.authorFirst1 + ' ' + dbo.aenDB.authorLast1 AS AuthorName ,
       dbo.aenDB.authorLast1 AS AuthorLast ,
       'Vol. ' + CAST(dbo.aenDB.volume AS varchar(max)) + ' Num. ' + CAST(dbo.aenDB.
       number AS varchar(max)) AS Volume ,
       DATEADD(year , 1976 + dbo.aenDB.volume - 1900 , 0) AS DatePosted ,
       '/journals/aen/aen' + CAST(dbo.aenDB.volume AS varchar(max)) + '_' + CAST(dbo.
       aenDB.number AS varchar(max)) + '_' + CAST(dbo.aenDB.articleNum AS varchar(max)) +
       dbo.aenDB.fileType AS URL ,
       3 AS MediaTypeId
   FROM
       dbo.aenDB INNER JOIN dbo.Periodicals AS Periodicals_2
   ON  Periodicals_2.PeriodicalId = 6 LEFT OUTER JOIN dbo.DocumentAuthors AS
   DocumentAuthors_2
   ON  DocumentAuthors_2.AuthorLast = dbo.aenDB.authorLast1 AND dbo.aenDB.authorFirst1
       LIKE '%' + DocumentAuthors_2.AuthorFirst + '%'
   UNION
   SELECT
       Periodicals_1.Title AS Journal , Periodicals_1.PeriodicalId AS PeriodicalId ,       
       NULL AS GUID ,
       DocumentAuthors_1.AuthorId ,
       dbo.scholar.paper_name + '(' + dbo.scholar.notation + ')' AS 'Title' ,
       dbo.scholar.author_name AS AuthorName ,
       dbo.scholar.author_name AS AuthorLast ,
       dbo.scholar.whenit AS Volume ,
       dbo.scholar.CreateDate AS 'DatePosted' ,
       '/journals/scholar/' + dbo.scholar.file_name AS URL ,
       3 AS MediaTypeId
   FROM
       dbo.scholar INNER JOIN dbo.Periodicals AS Periodicals_1
   ON  Periodicals_1.PeriodicalId = 7 LEFT OUTER JOIN dbo.DocumentAuthors AS
   DocumentAuthors_1
   ON  DocumentAuthors_1.AuthorFirst + ' ' + DocumentAuthors_1.AuthorMiddle + ' ' +
       DocumentAuthors_1.AuthorLast = dbo.scholar.author_name OR DocumentAuthors_1.
       AuthorFirst + ' ' + DocumentAuthors_1.AuthorLast = dbo.scholar.author_name
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[26] 4[32] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[84] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'PeriodicalsView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'PeriodicalsView'
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalsGetDetails]    Script Date: 04/05/2012 12:12:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PeriodicalsGetDetails] 
	-- Add the parameters for the stored procedure here
       @PeriodicalId int = 0
AS
BEGIN
      SELECT
          PeriodicalId ,
          Title ,
          Description ,
          Logo ,
          ShortDescription ,
          RedirectURL ,
          PublicationFrequency ,
          metaDescription ,
          metaKeywords ,
          CreateDate
      FROM
          Periodicals
      WHERE
          ( PeriodicalId = @PeriodicalId )
END
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalsGetArchives]    Script Date: 04/05/2012 12:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PeriodicalsGetArchives]
       @PeriodicalId int ,
       @title varchar(50) = 'RandomString' ,
       @author varchar(50) = 'RandomString' ,
       @subject varchar(50) = 'RandomString' ,
       @volume varchar(50) = 'RandomString'
AS
BEGIN

      IF @title = 'RandomString' AND @author = 'RandomString' AND @subject =
      'RandomString' AND @volume = 'RandomString'
         BEGIN

               IF @PeriodicalId = 1
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      freemarket.title ,
                      freemarket.AuthorFirst + ' ' + freemarket.AuthorLast AS AuthorName ,
                      freemarket.AuthorLast ,
                      freemarket.articledate AS DatePosted ,
                      DATENAME(month , freemarket.articledate) AS Month ,
                      '/freemarket_detail.aspx?control=' + CAST(freemarket.control AS
                      varchar(10)) AS URL ,
                      freemarket.[control] AS Id
                  FROM
                      dbo.freemarket
                  ORDER BY
                      freemarket.articledate DESC


-- Season, Volume
               IF @PeriodicalId = 2
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      misesreview.AuthorFirst + ' ' + misesreview.AuthorLast AS AuthorName ,
                      misesreview.AuthorLast ,
                      misesreview.title ,
                      CASE misesreview.issue_season
                        WHEN 1 THEN 'Spring'
                        WHEN 2 THEN 'Summer'
                        WHEN 3 THEN 'Fall'
                        WHEN 4 THEN 'Winter'
                      END + ' ' + CAST(misesreview.issue_year AS varchar(10)) AS Volume ,
                      misesreview.issue_year AS Season ,
                      '/misesreview_detail.aspx?control=' + CAST(misesreview.control AS
                      varchar(10)) AS URL ,
                      misesreview.[control] AS Id
                  FROM
                      dbo.misesreview
                  ORDER BY
                      misesreview.CreateDate DESC ,
                      PeriodicalId DESC

               IF @PeriodicalId = 3
                  SELECT
				      jls.[control] AS Id,
                      @PeriodicalId AS PeriodicalId ,
                      jls.number ,
                      jls.display ,
                      jls.title ,
                      jls.authorFirst1 + ' ' + jls.authorLast1 AS AuthorName ,
                      jls.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(jls.Volume AS varchar(10)) + ' Num. ' + CAST(jls.
                      [number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(jls.Volume AS varchar(10)) AS Season ,
                      '/journals/jls/' + CAST(jls.Volume AS varchar(10)) + '_' + CAST(jls
                      .number AS varchar(10)) + '/' + CAST(jls.Volume AS varchar(10)) +
                      '_' + CAST(jls.number AS varchar(10)) + '_' + CAST(jls.articleNum
                      AS varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.jls
                  ORDER BY
                      jls.Volume DESC ,
                      number DESC ,
                      jls.ArticleNum

               IF @PeriodicalId = 4
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      qjaeDB.number ,
                      qjaeDB.display ,
                      qjaeDB.title ,
                      qjaeDB.authorFirst1 + ' ' + qjaeDB.authorLast1 AS AuthorName ,
                      qjaeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(qjaeDB.Volume AS varchar(10)) + ' Num. ' + CAST(
                      qjaeDB.[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(qjaeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/qjae/pdf/qjae' + CAST(qjaeDB.Volume AS varchar(10)) +
                      '_' + CAST(qjaeDB.number AS varchar(10)) + '_' + CAST(qjaeDB.
                      articleNum AS varchar(10)) + '.pdf' AS URL,
                      [control] AS Id
                  FROM
                      dbo.qjaeDB
                  ORDER BY
                      [qjaeDB].Volume DESC ,
                      [qjaeDB].number DESC ,
                      ArticleNum

               IF @PeriodicalId = 5
                  SELECT
				      raeDB.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      raeDB.number ,
                      raeDB.display ,
                      raeDB.title ,
                      raeDB.authorFirst1 + ' ' + raeDB.authorLast1 AS AuthorName ,
                      raeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(raeDB.Volume AS varchar(10)) + ' Num. ' + CAST(raeDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(raeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/rae/pdf/RAE' + CAST(raeDB.Volume AS varchar(10)) + '_' +
                      CAST(raeDB.number AS varchar(10)) + '_' + CAST(raeDB.articleNum AS
                      varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.raeDB
                  ORDER BY
                      dbo.raeDB.[Volume] DESC ,
                      --number DESC ,
                      ArticleNum

               IF @PeriodicalId = 6
                  SELECT
				      aendb.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      aenDB.number ,
                      aenDB.display ,
                      aenDB.title ,
                      aenDB.authorFirst1 + ' ' + aenDB.authorLast1 AS AuthorName ,
                      aenDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(aenDB.Volume AS varchar(10)) + ' Num. ' + CAST(aenDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(aenDB.Volume AS varchar(10)) AS Season ,
                      '/journals/aen/aen' + CAST(aenDB.Volume AS varchar(10)) + '_' +
                      CAST(aenDB.number AS varchar(10)) + '_' + CAST(aenDB.articleNum AS
                      varchar(10)) + aenDB.[fileType] AS URL
                  FROM
                      dbo.aenDB
                  ORDER BY
                      aenDB.Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 7
                  SELECT
				      scholar.[control] As Id,
                      @PeriodicalId AS PeriodicalId ,
                      [paper_name] + '(' + notation + ')' AS 'Title' ,
                      [author_name] AS AuthorName ,
                      [author_name] AS AuthorLast ,
                      [whenit] AS 'DatePosted' ,
                      '' AS 'Month' ,
                      '/journals/scholar/' + [file_name] AS [url]
                  FROM
                      scholar
                  ORDER BY
                      control DESC

               IF @PeriodicalId = 8
                  SELECT
                      WardLibrary.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      title AS 'Title' ,
                      AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName ,
                      [AuthorLast] AS AuthorLast ,
                      donatedDate AS 'DatePosted' ,
                      '' AS 'Season' ,
                      '' AS 'Volume' ,
                      'book.aspx?Id=' + CAST(CONTROL AS varchar(10)) AS [url]
                  FROM
                      WardLibrary
                  --WHERE
                      --display = 'yes'
                  ORDER BY
                      control DESC




         END
      ELSE -- DO SEARCH
         BEGIN

               IF @title = ''
                  SET @title = 'RandomString'
               IF @author = ''
                  SET @author = 'RandomString'
               IF @subject = ''
                  SET @subject = 'RandomString'
               IF @volume = ''
                  SET @volume = 'RandomString'


               IF @PeriodicalId = 1
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      freemarket.title ,
                      freemarket.AuthorFirst + ' ' + freemarket.AuthorLast AS AuthorName ,
                      freemarket.AuthorLast ,
                      freemarket.articledate AS DatePosted ,
                      DATENAME(month , freemarket.articledate) AS Month ,
                      '/freemarket_detail.aspx?control=' + CAST(freemarket.control AS
                      varchar(10)) AS URL
                  FROM
                      dbo.freemarket
                  WHERE
                      freemarket.title LIKE '%' + @title + '%' OR freemarket.body LIKE
                      '%' + @title + '%' OR freemarket.body LIKE '%' + @subject + '%' OR
                      freemarket.AuthorLast LIKE '%' + @author + '%'
                  ORDER BY
                      freemarket.articledate DESC


-- Season, Volume
               IF @PeriodicalId = 2
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      misesreview.AuthorFirst + ' ' + misesreview.AuthorLast AS AuthorName ,
                      misesreview.AuthorLast ,
                      misesreview.title ,
                      CASE misesreview.issue_season
                        WHEN 1 THEN 'Spring'
                        WHEN 2 THEN 'Summer'
                        WHEN 3 THEN 'Fall'
                        WHEN 4 THEN 'Winter'
                      END + ' ' + CAST(misesreview.issue_year AS varchar(10)) AS Volume ,
                      misesreview.issue_year AS Season ,
                      '/misesreview_detail.aspx?control=' + CAST(misesreview.control AS
                      varchar(10)) AS URL
                  FROM
                      dbo.misesreview
                  WHERE
                      misesreview.title LIKE '%' + @title + '%' OR misesreview.body LIKE
                      '%' + @title + '%' OR misesreview.AuthorLast LIKE '%' + @author +
                      '%' OR misesreview.[authorfirst] LIKE '%' + @author + '%' OR
                      misesreview.[authorfirst] + ' ' + +misesreview.[authorlast] LIKE
                      '%' + @author + '%' OR misesreview.body LIKE '%' + @title + '%' OR
                                                                                         CASE
                                                                                         misesreview
                                                                                         .
                                                                                         issue_season
                                                                                           WHEN 1 THEN 'Spring'
                                                                                           WHEN 2 THEN 'Summer'
                                                                                           WHEN 3 THEN 'Fall'
                                                                                           WHEN 4 THEN 'Winter'
                                                                                         END
                      + ' ' + CAST(misesreview.issue_year AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      misesreview.CreateDate DESC ,
                      PeriodicalId DESC

               IF @PeriodicalId = 3
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      jls.number ,
                      jls.display ,
                      jls.title ,
                      jls.authorFirst1 + ' ' + jls.authorLast1 AS AuthorName ,
                      jls.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(jls.Volume AS varchar(10)) + ' Num. ' + CAST(jls.
                      [number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(jls.Volume AS varchar(10)) AS Season ,
                      '/journals/jls/' + CAST(jls.Volume AS varchar(10)) + '_' + CAST(jls
                      .number AS varchar(10)) + '/' + CAST(jls.Volume AS varchar(10)) +
                      '_' + CAST(jls.number AS varchar(10)) + '_' + CAST(jls.articleNum
                      AS varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.jls
                  WHERE
                      jls.title LIKE '%' + @title + '%' OR jls.AuthorLast1 LIKE '%' +
                      @author + '%' OR jls.AuthorLast2 LIKE '%' + @author + '%' OR jls.
                      authorFirst1 LIKE '%' + @author + '%' OR jls.authorFirst2 LIKE '%'
                      + @author + '%' OR 'Vol. ' + CAST(jls.Volume AS varchar(10)) +
                      ' Num. ' + CAST(jls.[number] AS varchar(10)) LIKE '%' + @volume +
                      '%'
                  ORDER BY
                      jls.Volume DESC ,
                      number DESC ,
                      jls.ArticleNum

               IF @PeriodicalId = 4
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      qjaeDB.number ,
                      qjaeDB.display ,
                      qjaeDB.title ,
                      qjaeDB.authorFirst1 + ' ' + qjaeDB.authorLast1 AS AuthorName ,
                      qjaeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(qjaeDB.Volume AS varchar(10)) + ' Num. ' + CAST(
                      qjaeDB.[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(qjaeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/qjae/pdf/qjae' + CAST(qjaeDB.Volume AS varchar(10)) +
                      '_' + CAST(qjaeDB.number AS varchar(10)) + '_' + CAST(qjaeDB.
                      articleNum AS varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.qjaeDB
                  WHERE
                      qjaeDB.title LIKE '%' + @title + '%' OR qjaeDB.AuthorLast1 LIKE '%'
                      + @author + '%' OR qjaeDB.AuthorLast2 LIKE '%' + @author + '%' OR
                      qjaeDB.authorFirst1 LIKE '%' + @author + '%' OR qjaeDB.authorFirst2
                      LIKE '%' + @author + '%' OR 'Vol. ' + CAST(qjaeDB.Volume AS varchar
                      (10)) + ' Num. ' + CAST(qjaeDB.[number] AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 5
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      raeDB.number ,
                      raeDB.display ,
                      raeDB.title ,
                      raeDB.authorFirst1 + ' ' + raeDB.authorLast1 AS AuthorName ,
                      raeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(raeDB.Volume AS varchar(10)) + ' Num. ' + CAST(raeDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(raeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/rae/pdf/RAE' + CAST(raeDB.Volume AS varchar(10)) + '_' +
                      CAST(raeDB.number AS varchar(10)) + '_' + CAST(raeDB.articleNum AS
                      varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.raeDB
                  WHERE
                      raeDB.title LIKE '%' + @title + '%' OR raeDB.AuthorLast1 LIKE '%' +
                      @author + '%' OR raeDB.AuthorLast2 LIKE '%' + @author + '%' OR
                      raeDB.authorFirst1 LIKE '%' + @author + '%' OR raeDB.authorFirst2
                      LIKE '%' + @author + '%' OR 'Vol. ' + CAST(raeDB.Volume AS varchar(
                      10)) + ' Num. ' + CAST(raeDB.[number] AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 6
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      aenDB.number ,
                      aenDB.display ,
                      aenDB.title ,
                      aenDB.authorFirst1 + ' ' + aenDB.authorLast1 AS AuthorName ,
                      aenDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(aenDB.Volume AS varchar(10)) + ' Num. ' + CAST(aenDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(aenDB.Volume AS varchar(10)) AS Season ,
                      '/journals/aen/aen' + CAST(aenDB.Volume AS varchar(10)) + '_' +
                      CAST(aenDB.number AS varchar(10)) + '_' + CAST(aenDB.articleNum AS
                      varchar(10)) + aenDB.[fileType] AS URL
                  FROM
                      dbo.aenDB
                  WHERE
                      aenDB.title LIKE '%' + @title + '%' OR aenDB.AuthorLast1 LIKE '%' +
                      @author + '%' OR aenDB.AuthorLast2 LIKE '%' + @author + '%' OR
                      aenDB.authorFirst1 LIKE '%' + @author + '%' OR aenDB.authorFirst2
                      LIKE '%' + @author + '%' OR 'Vol. ' + CAST(aenDB.Volume AS varchar(
                      10)) + ' Num. ' + CAST(aenDB.[number] AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 7
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      [paper_name] + '(' + notation + ')' AS 'Title' ,
                      [author_name] AS AuthorName ,
                      [author_name] AS AuthorLast ,
                      [whenit] AS 'DatePosted' ,
                      '' AS 'Month' ,
                      '/journals/scholar/' + [file_name] AS [url]
                  FROM
                      scholar
                  WHERE
                      [paper_name] LIKE '%' + @title + '%' OR [author_name] LIKE '%' +
                      @author + '%' OR [notation] LIKE '%' + @title + '%'
                  ORDER BY
                      control DESC

               IF @PeriodicalId = 8
                  SELECT
					  WardLibrary.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      title AS 'Title' ,
                      AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName ,
                      [AuthorLast] AS AuthorLast ,
                      donatedDate AS 'DatePosted' ,
                      '' AS 'Season' ,
                      '' AS 'Volume' ,
                      'book.aspx?Id=' + CAST(CONTROL AS varchar(10)) AS [url]
                  FROM
                      WardLibrary
                  WHERE
                      --display = 'yes' AND 
                      ( WardLibrary.comments LIKE '%' + @title + '%'
                                            OR WardLibrary.title LIKE '%' + @title + '%'
                                            OR WardLibrary.authorlast LIKE '%' + @author
                                            + '%' OR WardLibrary.authorfirst2 LIKE '%' +
                                            @author + '%' OR WardLibrary.authorfirst3
                                            LIKE '%' + @author + '%' OR WardLibrary.
                                            subject LIKE '%' + @subject + '%' OR
                                            AuthorFirst + ' ' + AuthorLast LIKE '%' +
                                            @author + '%' )
                  ORDER BY
                      control DESC


         END

END

--PeriodicalsGetArchives 3

--select * from aen

--[PeriodicalsGetArchives] 6,'Buchanan'
GO
/****** Object:  StoredProcedure [dbo].[QuizIncrementCount]    Script Date: 04/05/2012 12:12:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[QuizIncrementCount] @QuizId int
AS
BEGIN
      SET NOCOUNT ON ;
      UPDATE
          dbo.quizType
      SET
          NumTaken = NumTaken + 1
      WHERE
          QuizID = @QuizId
END
GO
/****** Object:  StoredProcedure [dbo].[QuizGetList]    Script Date: 04/05/2012 12:12:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[QuizGetList]
AS
BEGIN
      SET NOCOUNT ON ;
      SELECT
          *
      FROM
          dbo.quizType
      WHERE
          Display = 1
END
GO
/****** Object:  StoredProcedure [dbo].[QuoteGetRandom]    Script Date: 04/05/2012 12:12:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[QuoteGetRandom]
AS -- Random Quote

SELECT TOP 1
QuoteId ,
    Quote ,
    AuthorFirst + ' ' + AuthorLast AS Author,
    Source ,
    Subject ,
    Quotes.AuthorId
FROM
    Quotes tablesample(10 percent) JOIN DocumentAuthors
ON  DocumentAuthors.AuthorId = Quotes.AuthorId
--WHERE
--    QuoteId = @Quote
--ORDER BY NEWID()
GO
/****** Object:  StoredProcedure [dbo].[PageAddNew]    Script Date: 04/05/2012 12:12:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PageAddNew]
       @Title varchar(500) ,
       @Name varchar(500) ,
       @Folder varchar(255) ,
       @Content varchar(max) ,
       @Keywords varchar(500) ,
       @Description varchar(max)
AS
BEGIN

      IF
      ( SELECT
            COUNT(1)
        FROM
            page
        WHERE
            folder = @Folder AND pageName = @Name ) > 0
         UPDATE
             Page
         SET
             pageTitle = @Title ,
             pageName = @Name ,
             folder = @Folder ,
             [content] = @Content ,
             metaKeywords = @Keywords ,
             metaDescription = @Description ,
             EditDate = GETDATE()
         WHERE
             folder = @Folder AND pageName = @Name
      ELSE
         INSERT INTO
             Page
             (
               pageTitle ,
               pageName ,
               folder ,
               [content] ,
               metaKeywords ,
               metaDescription )
         VALUES
             (
               @Title ,
               @Name ,
               @Folder ,
               @Content ,
               @Keywords ,
               @Description )


END
GO
/****** Object:  StoredProcedure [dbo].[PageContentGetDetail]    Script Date: 04/05/2012 12:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PageContentGetDetail] @ContentName varchar(150) = 0
AS
BEGIN
      SET NOCOUNT ON ;

      SELECT
          Body
      FROM
          pagecontent
      WHERE
          NAME = @ContentName
END
GO
/****** Object:  StoredProcedure [dbo].[QuotesGetSubjectList]    Script Date: 04/05/2012 12:12:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[QuotesGetSubjectList]
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT DISTINCT
          Subject
      FROM
          QUOTES
END
GO
/****** Object:  StoredProcedure [dbo].[MediaCategoryGetParents]    Script Date: 04/05/2012 12:12:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavjdV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaCategoryGetParents]
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT
          CategoryId ,
          Category ,
          Description ,
          CreateDate
      FROM
          dbo.MediaCategory
      WHERE
          CategoryId IN ( SELECT
                              ParentCategory
                          FROM
                              dbo.MediaCategory ) OR ParentCategory = 0
      ORDER BY
          Category
END
GO
/****** Object:  StoredProcedure [dbo].[MediaCategoryGetAll]    Script Date: 04/05/2012 12:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavjdV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaCategoryGetAll]
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT
          CategoryId ,
          Category ,
          Description ,
          CreateDate
      FROM
          MediaCategory
      ORDER BY
          SortOrder ,
          CreateDate DESC ,
          Category
END
GO
/****** Object:  StoredProcedure [dbo].[MisesReviewGetReview]    Script Date: 04/05/2012 12:12:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MisesReviewGetReview] @control int
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT
          title ,
          authorFirst ,
          authorLast ,
          authorFirst2 ,
          authorlast2 ,
          authorFirst3 ,
          authorlast3 ,
          issue_Year ,
          issue_Season ,
          body ,
          CreateDate ,
          GUID
      FROM
          misesreview
      WHERE
          ( control = @control )
END
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalAddStory]    Script Date: 04/05/2012 12:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PeriodicalAddStory]
       @PeriodicalId int ,
       @EditedBy varchar(50) ,
       @authorfirst varchar(50) ,
       @authorlast varchar(50) ,
       @authorfirst2 varchar(50) ,
       @authorlast2 varchar(50) ,
       @authorfirst3 varchar(50) ,
       @authorlast3 varchar(50) ,
       @title varchar(255) ,
       @body varchar(max) ,
       @issue_season int ,
       @issue_year int
AS
BEGIN


      INSERT INTO
          misesreview
          (
            authorfirst ,
            authorlast ,
            authorfirst2 ,
            authorlast2 ,
            authorfirst3 ,
            authorlast3 ,
            title ,
            body ,
            issue_season ,
            issue_year )
      VALUES
          (
            @authorfirst ,
            @authorlast ,
            @authorfirst2 ,
            @authorlast2 ,
            @authorfirst3 ,
            @authorlast3 ,
            @title ,
            @body ,
            @issue_season ,
            @issue_year )


      SELECT
          SCOPE_IDENTITY()

END
GO
/****** Object:  StoredProcedure [dbo].[PageUpdate]    Script Date: 04/05/2012 12:12:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PageUpdate]
       @GUID uniqueidentifier ,
       @Title varchar(255) ,
       @Keywords varchar(500) ,
       @Description varchar(500) ,
       @Content varchar(max)
--@Name varchar(500),
--@Folder varchar(255),

AS
BEGIN


      UPDATE
          Page
      SET
          pageTitle = @Title ,
          [content] = @Content ,
          metaKeywords = @Keywords ,
          metaDescription = @Description ,
          EditDate = GETDATE()
      WHERE
          GUID = @GUID

END
GO
/****** Object:  StoredProcedure [dbo].[PagesGetGUID]    Script Date: 04/05/2012 12:12:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PagesGetGUID]
       @PageName varchar(255) ,
       @Path varchar(255)
AS
BEGIN

      DECLARE @GUID uniqueidentifier

      SET @GUID = ( SELECT
                        Page.GUID
                    FROM
                        dbo.Page
                    WHERE
                        ( Page.pageName = @PageName ) AND ( Page.folder LIKE @path ) )

IF (@GUID IS NULL)

SET @GUID = ( SELECT
                        Page.GUID
                    FROM
                        dbo.Page
                    WHERE
                        ( Page.pageName = @PageName ) AND ( Page.folder LIKE '/' + @path ) )
                        
                        
IF (@GUID IS NULL)

SET @GUID = ( SELECT
                        Page.GUID
                    FROM
                        dbo.Page
                    WHERE
                        ( Page.pageName = @PageName ) AND ( Page.folder LIKE @path + '/' ) )                        

--IF (@GUID IS NULL)

-- SET @GUID = ( SELECT
--                        Pages.GUID
--                    FROM
--                        dbo.Pages
--                    WHERE
--                        ( Pages.pageName = @PageName ) AND ( Pages.folder LIKE @path + '/'  ) )
                        
--IF (@GUID IS NULL)

-- SET @GUID = ( SELECT
--                        Pages.GUID
--                    FROM
--                        dbo.Pages
--                    WHERE
--                        ( Pages.pageName = @PageName ) AND ( Pages.folder LIKE '/' + @path   ) )                        

--IF (@GUID IS NULL)

--      SET @GUID = ( SELECT
--                        Pages.GUID
--                    FROM
--                        dbo.Pages
--                    WHERE
--                        ( Pages.pageName = @PageName ) AND ( Pages.folder LIKE @path ) )



      SELECT
          @GUID

END
GO
/****** Object:  StoredProcedure [dbo].[PageGetIdByGUID]    Script Date: 04/05/2012 12:12:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PageGetIdByGUID] @GUID uniqueidentifier
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT
          PageId
      FROM
          Page
      WHERE
          [GUID] = @GUID
END
GO
/****** Object:  StoredProcedure [dbo].[PageGetGUIDByPageId]    Script Date: 04/05/2012 12:12:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PageGetGUIDByPageId] @PageId int
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT
          GUID
      FROM
          Page
      WHERE
          PageId = @PageId
END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheRegisterTableStoredProcedure]    Script Date: 04/05/2012 12:11:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[AspNet_SqlCacheRegisterTableStoredProcedure] @tableName nvarchar(
       450)
AS
BEGIN

      DECLARE @triggerName AS nvarchar(3000)
      DECLARE @fullTriggerName AS nvarchar(3000)
      DECLARE @canonTableName nvarchar(3000)
      DECLARE @quotedTableName nvarchar(3000) 

         /* Create the trigger name */
      SET @triggerName = REPLACE(@tableName , '[' , '__o__')
      SET @triggerName = REPLACE(@triggerName , ']' , '__c__')
      SET @triggerName = @triggerName + '_AspNet_SqlCacheNotification_Trigger'
      SET @fullTriggerName = 'dbo.[' + @triggerName + ']' 

         /* Create the cannonicalized table name for trigger creation */ 
         /* Do not touch it if the name contains other delimiters */
      IF ( CHARINDEX('.' , @tableName) <> 0 OR CHARINDEX('[' , @tableName) <> 0 OR
           CHARINDEX(']' , @tableName) <> 0 )
         SET @canonTableName = @tableName
      ELSE
         SET @canonTableName = '[' + @tableName + ']' 

         /* First make sure the table exists */
      IF
      ( SELECT
            OBJECT_ID(@tableName , 'U') ) IS NULL
         BEGIN
               RAISERROR ( '00000001' ,
               16 ,
               1 )
               RETURN
         END

      BEGIN TRAN
         /* Insert the value into the notification table */
      IF NOT EXISTS ( SELECT
                          tableName
                      FROM
                          dbo.AspNet_SqlCacheTablesForChangeNotification WITH ( NOLOCK )
                      WHERE
                          tableName = @tableName )
         IF NOT EXISTS ( SELECT
                             tableName
                         FROM
                             dbo.AspNet_SqlCacheTablesForChangeNotification WITH (
                                                                                   TABLOCKX )
                         WHERE
                             tableName = @tableName )
            INSERT
                dbo.AspNet_SqlCacheTablesForChangeNotification
            VALUES
                (
                  @tableName ,
                  GETDATE() ,
                  0 )

         /* Create the trigger */
      SET @quotedTableName = QUOTENAME(@tableName , '''')
      IF NOT EXISTS ( SELECT
                          name
                      FROM
                          sysobjects WITH ( NOLOCK )
                      WHERE
                          name = @triggerName AND type = 'TR' )
         IF NOT EXISTS ( SELECT
                             name
                         FROM
                             sysobjects WITH ( TABLOCKX )
                         WHERE
                             name = @triggerName AND type = 'TR' )
            EXEC ( 'ALTER TRIGGER '+@fullTriggerName+' ON '+@canonTableName+
            '
                FOR INSERT, UPDATE, DELETE AS BEGIN
                SET NOCOUNT ON
                EXEC dbo.AspNet_SqlCacheUpdateChangeIdStoredProcedure N'
            +@quotedTableName+'
                END
                ' )
      COMMIT TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure]    Script Date: 04/05/2012 12:11:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure]
AS
SELECT
    tableName
FROM
    dbo.AspNet_SqlCacheTablesForChangeNotification
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCachePollingStoredProcedure]    Script Date: 04/05/2012 12:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[AspNet_SqlCachePollingStoredProcedure]
AS
SELECT
    tableName ,
    changeId
FROM
    dbo.AspNet_SqlCacheTablesForChangeNotification
RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[_DeleteOldData]    Script Date: 04/05/2012 12:11:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[_DeleteOldData] 
	
AS
BEGIN
	
	
	UPDATE revision SET documenttext = '' WHERE editdate < GETDATE() - 60 AND editor = 'bkmarcus' AND documenttext != ''
	UPDATE revision SET documenttext = '' WHERE editdate < GETDATE() - 60 AND editor = 'jtucker' AND documenttext != ''
	
	TRUNCATE TABLE exceptions
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[defrag_table_indexes]    Script Date: 04/05/2012 12:12:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****************************************************************************************************
Author:			Vasily Kabanov
Created:		2010-06-11
Parameters:		@objectID int - object_id of a user table or indexed view, see sys.tables.object_id
					-- and sys.views.object_id; if user table or view with the specified ID is not
					-- found in the same database, error is raised
				@jobID - the ID of the job which executes the procedure; if NULL, the log messages
					-- will be output by print
				@rebuildFrafmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index rebuild; default is 30 (recommended by Microsoft)
				@reorganizeFragmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index reorganise; default is 10 (recommended by Microsoft);
					-- must be less or equal to @rebuildFrafmentationThreshold
				@indexesDefragmented - output, the total number of index trees defragmented (using any method)
					-- every partition represents a separate index tree
				@indexesRebuilt	- out of indexesDefragmented, the number of index partitions
					-- defragmented by rebuilddinfg
				@indexesFailed - output, the number of indexes failed to defragment
				@warningCount - output, the number of non-critical failures, such as resetting
					-- allow_page_locks
Returns:		
				return code:
					0 - success (@indexesFailed will be 0)
					1 - general failure (such as all of the indexes failed)
					2 - the procedure was called in transaction
					3 - the specified object id is invalid or does not represent user table or view
					4 - the specified fragmentation thresholds are invalid
Comment:		The procedure defragments indexes on a table or indexed view.
				The decision is made as follows:
					- if leaf page count is 4 or fewer, no action is taken
					- if leaf level fragmentation is greater than @rebuildFrafmentationThreshold
						or first index level fragmentation is greater than @rebuildFrafmentationThreshold
						and at the number of pages at the first level is greater than 16 (2 extents)
						and the fragment count at first index level is greater than 4, the index is rebuilt
					- if leaf level fragmentation is greater than @reorganizeFragmentationThreshold, the
						index is reorganized; if allow_page_locks is off, it is set to on for the duration
						of the reorganization; if setting allow_page_locks to on fails, the index is rebuilt.
				If you'd like to change this logic scroll down to the section marked with comment
				"-- code section: defrag.action.decision.making" and change it as required.
				Partitioned indexes are defragmented partition by partition.
Examples:
				declare	@indexesDefragmented int
				declare	@indexesRebuilt int
				declare	@indexesFailed int
				declare	@warningCount int
				declare	@objectID int
				declare @retcode int
				
				select @objectID = object_id(N'dbo.TestFragmentation')
				
				exec @retcode = dbo.defrag_table_indexes
					@objectID = @objectID
					, @rebuildFrafmentationThreshold = 30
					, @reorganizeFragmentationThreshold = 10
					, @indexesDefragmented = @indexesDefragmented out
					, @indexesRebuilt = @indexesRebuilt out
					, @indexesFailed = @indexesFailed out
					, @warningCount = @warningCount out
				print 'Retcode = ' + convert(varchar(20), @retcode)
				print 'Defragmented = ' + convert(varchar(20), @indexesDefragmented)
				print 'Failed = ' + convert(varchar(20), @indexesFailed)
				print 'Warnings = ' + convert(varchar(20), @warningCount)

-- have a look at the fragmentation info
select		quotename(schema_name(o.schema_id)) + N'.' + quotename(object_name(s.object_id)) as ObjectName
			, i.name as IndexName
			, i.allow_page_locks
			, s.*
from		sys.dm_db_index_physical_stats(db_id(), object_id(N'dbo.TestFragmentation'), null, null, 'DETAILED') s
			, sys.objects o
			, sys.indexes i
where		o.object_id = s.object_id
			and i.object_id = o.object_id
			and i.index_id = s.index_id
			and index_level < 2
			and is_disabled = 0
							
****************************************************************************************************/
CREATE  PROCEDURE [dbo].[defrag_table_indexes]
  @objectID int
, @rebuildFrafmentationThreshold float = 30
, @reorganizeFragmentationThreshold float = 10
, @indexesDefragmented int = null output
, @indexesRebuilt	int = null output
, @indexesFailed int = null output
, @warningCount int = null output
as

set implicit_transactions off;
set nocount on;

declare		@retcode int;

select		@indexesDefragmented = 0
			, @indexesFailed = 0
			, @warningCount = 0
			, @indexesRebuilt = 0;

begin try;

	declare		@retCodeSuccess int
				, @retCodeGeneralFailure int
				, @retCodeFailCalledInTransaction int
				, @retCodeInvalidObject int
				, @retCodeInvalidThresholds int;
				
	select		@retCodeSuccess = 0
				, @retCodeGeneralFailure = 1
				, @retCodeFailCalledInTransaction = 2
				, @retCodeInvalidObject = 3
				, @retCodeInvalidThresholds = 4;
				
	select		@retcode = @retCodeSuccess;

	declare	@actionCodeRebuild smallint
			, @actionCodeReorganize smallint
			, @actionCodeNoAction smallint;
			
	select	@actionCodeRebuild = 1
			, @actionCodeReorganize = 2
			, @actionCodeNoAction = 0;

	if @rebuildFrafmentationThreshold is null
		or @reorganizeFragmentationThreshold is null
		or (@reorganizeFragmentationThreshold > @rebuildFrafmentationThreshold)
	begin
		set @retcode = @retCodeInvalidThresholds;
		raiserror('An invalid combination of fragmentation thresholds was specified', 16, 1);
	end

	if @@trancount > 0
	begin
		set @retcode = @retCodeFailCalledInTransaction;
		raiserror('The procedure defrag_table_indexes must not be executed in a transaction', 16, 1);
	end
	
	if not exists(
		select 1 from sys.tables where object_id = @objectID
		union all
		select 1 from sys.views  where object_id = @objectID
	)
	begin
		set @retcode = @retCodeInvalidObject;
		raiserror('The object ID is invalid', 16, 1);
	end

	declare @objectName sysname;
	select		@objectName = quotename(schema_name(o.schema_id)) + N'.' + quotename(object_name(o.object_id))
	from		sys.objects o
	where		o.object_id = @objectID;
	
	declare @tbAction table (
		index_id int not null
		, partition_number int not null
		, action_code smallint not null
		,  primary key (index_id, partition_number)
	);
	
	with FragmentData as (
		select		s.index_id
					, i.allow_page_locks
					, s.index_level
					, s.avg_fragmentation_in_percent
					, s.page_count
					, s.fragment_count
					, s.partition_number
		from		sys.dm_db_index_physical_stats(db_id(), @objectID, null, null, 'DETAILED') s
					, sys.indexes i
		where		
					i.object_id = s.object_id
					and i.index_id = s.index_id
					and i.is_disabled = 0
					and i.[type] > 0					-- ignore heaps
					and s.avg_fragmentation_in_percent is not null
	)
	, Flattened as (
		select		s.index_id
					, s.avg_fragmentation_in_percent as leaf_frag
					, s.page_count as leaf_page_count
					, f.avg_fragmentation_in_percent as first_level_frag
					, f.page_count first_level_page_count
					, f.fragment_count as first_level_fragment_count
					, s.partition_number
		from		FragmentData s
					left join FragmentData f
						on s.index_id = f.index_id
						and s.partition_number = f.partition_number
						and f.index_level = 1
		where		s.index_level = 0
	)
	, Maxed as (
		select		s.index_id
					, s.leaf_frag
					, s.leaf_page_count
					, s.first_level_frag
					, s.first_level_page_count
					, s.first_level_fragment_count
					, s.partition_number
					, case when s.first_level_frag > s.leaf_frag then s.first_level_frag else s.leaf_frag end as max_frag
		from		Flattened s
	)
	, Decision as (
		select		s.index_id
					, s.partition_number
					----------------------------------------------------------
					-- code section: defrag.action.decision.making
					, case
						when	s.leaf_page_count < 5
							then	@actionCodeNoAction												-- less than 1 extent, (...In general, rebuilding or reorganizing small indexes often does not reduce fragmentation, see ms-help://MS.SQLCC.v9/MS.SQLSVR.v9.en/tsqlref9/html/b796c829-ef3a-405c-a784-48286d4fb2b9.htm)
						when	(
									s.leaf_frag > @rebuildFrafmentationThreshold
								)
								or (
									s.first_level_frag > @rebuildFrafmentationThreshold				-- reorganize cannot defragment upper levels
									and s.first_level_page_count > 16								-- more than 2 extents
									and s.first_level_fragment_count > 4
								)
							then	@actionCodeRebuild												-- rebuild
						when	s.leaf_frag > @reorganizeFragmentationThreshold
							then	@actionCodeReorganize											-- reorganize
						else		@actionCodeNoAction												-- do nothing
					end as ActionCode
					-- end code section: defrag.action.decision.making
					------------------------------------------------------------
		from		Maxed s
	)
	insert	@tbAction (
				index_id
				, partition_number
				, action_code
			)
	select		t.index_id
				, case when ps.name is not null then t.partition_number else 0 end	-- set partition number to 0 if not partitioned
				, t.ActionCode
	from		Decision t
				inner join sys.indexes i
					on i.index_id = t.index_id
					and i.object_id = @objectID
				left join sys.partition_schemes ps
					on ps.data_space_id = i.data_space_id
	where		ActionCode > 0;

	declare		myc
	cursor local for
	select		quotename(i.name) as IndexName
				, a.partition_number
				, a.action_code
				, i.allow_page_locks
				, a.index_id
				--, case when ps.[name] is not null then convert(bit, 1) else 0 end as IsPartitioned
	from		@tbAction a
				inner join sys.indexes i
					on i.index_id = a.index_id
	where		i.object_id = @objectID
				
	open myc;
	
	declare	@indexID int;
	declare	@partitionNo int;
	declare @indexName sysname;
	declare @actionCode smallint;
	declare @pageLocksAllowed bit;
	
	declare @sql nvarchar(max);
	
	declare @tbResults table (
		IndexID int not null
		, PartitionNumber int not null
		, ActionName nvarchar(50) not null
		, ActionCode smallint not null
		, Succeeded bit not null default 0
		, ErrorMessage nvarchar(max) null
		, WarningMessage nvarchar(max) null
		, StartTime datetime not null default getdate()
		, EndTime datetime null
		, primary key (IndexID, PartitionNumber)
	);

	while 1 = 1
	begin
		fetch next from myc into @indexName, @partitionNo, @actionCode, @pageLocksAllowed, @indexID;
		if @@fetch_status != 0
		begin
			break;
		end
		
		declare @actionName sysname;
		declare @actionRebuild sysname;
		declare @actionReorganize sysname;
		declare @msg nvarchar(max);
		
		select		@actionRebuild = N'rebuild'
					, @actionReorganize = N'reorganize';
		
		select @actionName = case when @actionCode = @actionCodeReorganize then @actionReorganize else @actionRebuild end;
		
		insert		@tbResults (IndexID, PartitionNumber, ActionCode, ActionName)
		values		(@indexID, @partitionNo, @actionCode, @actionName); 
		
		if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		begin
			set @sql = N'alter index ' + @indexName + N' on ' + @objectName + N' set (allow_page_locks = on)';

			print N'Executing SQL: ' + @sql;

			begin try
				--print @sql;
				exec (@sql);
			end try
			begin catch
				update		@tbResults
				set			WarningMessage = N'Cannot set allow_page_locks = on, rebuilding (' + error_message() + N')'
				where		IndexID = @indexID;
				
				select		@actionCode = @actionCodeRebuild
							, @actionName = @actionRebuild;
							
				update		@tbResults
				set			ActionCode = @actionCode
				where		IndexID = @indexID;
			end catch
		end -- if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		
		select	@sql = N'alter index '
			+ @indexName
			+ N' on '
			+ @objectName
			+ N' '
			+ @actionName
			+ case
				when @partitionNo > 0
					then N' partition = ' + convert(nvarchar(10), @partitionNo)
					else N''
				end;

		print N'Executing SQL: ' + @sql;
		
		begin try
			exec (@sql);

			update		@tbResults
			set			Succeeded = 1
						, EndTime = getdate()
			where		IndexID = @indexID;
		end try
		begin catch
			update		@tbResults
			set			ErrorMessage = error_message()
			where		IndexID = @indexID;
		end catch
		
		if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		begin
			set @sql = N'alter index ' + @indexName + N' on ' + @objectName + N' set (allow_page_locks = off)';

			print N'Executing SQL: ' + @sql;

			begin try
				exec (@sql);
				
				update		@tbResults
				set			EndTime = getdate()
				where		IndexID = @indexID;
				
			end try
			begin catch
				set @msg = N'Cannot restore allow_page_locks = off (' + error_message() + N')';
				
				update		@tbResults
				set			WarningMessage = 
								case when WarningMessage is null
									then @msg
									else WarningMessage + N'; ' + @msg
								end
							, EndTime = getdate()
				where		IndexID = @indexID;
			end catch
		end -- if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		
	end -- while 1 = 1
	
	declare		@summary nvarchar(max);
	declare		@newline nchar(2);
	select		@newline = nchar(13) + nchar(10)
				, @summary = N'';
	
	select		@summary = @summary + (
					quotename(i.name)
					+ case
						when r.PartitionNumber > 0
							then N' (Partition#' + convert(nvarchar(10), r.PartitionNumber) + N')'
							else N''
						end
					+ N': '
					+ case when Succeeded = 1
						then N'success ('
							+ convert(nvarchar(20), datediff(ss, StartTime, EndTime))
							+ N' seconds'
						else N'failure ('
							+ ErrorMessage
					end
					+ N')'
					+ case when WarningMessage is not null
						then N'; Warning: '
							+ WarningMessage
						else N''
					end
					+ @newline)
	from		@tbResults r
				, sys.indexes i
	where		i.object_id = @objectID
				and i.index_id = r.IndexID;
	
	select		@indexesDefragmented = isnull(sum(convert(int, Succeeded)), 0)
				, @indexesFailed = isnull(sum(convert(int, 1 - Succeeded)), 0)
				, @indexesRebuilt = isnull(sum(case when Succeeded = 1 and ActionCode = @actionCodeRebuild then convert(int, 1) else 0 end), 0)
				, @warningCount = isnull(sum(case when WarningMessage is not null then convert(int, 1) else 0 end), 0)
	from		@tbResults r;

	select @msg = N'Maintenance report for '
		 + @objectName
		 + N':'
		 + @newline
		 + N'Index partitions defragmented: '
		 + convert(nvarchar(20), @indexesDefragmented)
		 + @newline
		 + N'Index partitions rebuilt: '
		 + convert(nvarchar(20), @indexesRebuilt)
		 + @newline
		 + N'Indexes failed: '
		 + convert(nvarchar(20), @indexesFailed)
		 + @newline
		 + N'Warnings: '
		 + convert(nvarchar(20), @warningCount)
		 + @newline
		 + N'Details:'
		 + @newline
		 + isnull(@summary, N'');

	print @msg;

	select		@retcode = case when @indexesFailed = 0 then @retCodeSuccess else @retCodeGeneralFailure end;
	
end try
begin catch
	declare @tmpcode int;
	exec @tmpcode = dbo.util_rethrow_exception
	
	if @retcode = @retCodeSuccess
	begin
		select @retcode = @tmpcode;
	end
end catch

return @retcode;
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure]    Script Date: 04/05/2012 12:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] @tableName nvarchar(
       450)
AS
BEGIN
      UPDATE
          dbo.AspNet_SqlCacheTablesForChangeNotification WITH ( ROWLOCK )
      SET
          changeId = changeId + 1
      WHERE
          tableName = @tableName
END
GO
/****** Object:  StoredProcedure [dbo].[AspNet_SqlCacheUnRegisterTableStoredProcedure]    Script Date: 04/05/2012 12:11:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[AspNet_SqlCacheUnRegisterTableStoredProcedure] @tableName nvarchar
       (450)
AS
BEGIN

      BEGIN TRAN
      DECLARE @triggerName AS nvarchar(3000)
      DECLARE @fullTriggerName AS nvarchar(3000)
      SET @triggerName = REPLACE(@tableName , '[' , '__o__')
      SET @triggerName = REPLACE(@triggerName , ']' , '__c__')
      SET @triggerName = @triggerName + '_AspNet_SqlCacheNotification_Trigger'
      SET @fullTriggerName = 'dbo.[' + @triggerName + ']' 

         /* Remove the table-row from the notification table */
      IF EXISTS ( SELECT
                      name
                  FROM
                      sysobjects WITH ( NOLOCK )
                  WHERE
                      name = 'AspNet_SqlCacheTablesForChangeNotification' AND type = 'U' )
         IF EXISTS ( SELECT
                         name
                     FROM
                         sysobjects WITH ( TABLOCKX )
                     WHERE
                         name = 'AspNet_SqlCacheTablesForChangeNotification' AND type =
                         'U' )
            DELETE  FROM
                    dbo.AspNet_SqlCacheTablesForChangeNotification
            WHERE
                    tableName = @tableName 

         /* Remove the trigger */
      IF EXISTS ( SELECT
                      name
                  FROM
                      sysobjects WITH ( NOLOCK )
                  WHERE
                      name = @triggerName AND type = 'TR' )
         IF EXISTS ( SELECT
                         name
                     FROM
                         sysobjects WITH ( TABLOCKX )
                     WHERE
                         name = @triggerName AND type = 'TR' )
            EXEC ( 'DROP TRIGGER '+@fullTriggerName )

      COMMIT TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[CalendarGetUpcomingEvents]    Script Date: 04/05/2012 12:12:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[CalendarGetUpcomingEvents] @limit int = 0
AS
BEGIN

      IF @limit > 0
         SET ROWCOUNT @limit

      SELECT
          EventId ,
          Title ,
          [Description] ,
          EventDate ,
          StartDate,
          EndDate ,
          IntroText ,
          EventImage ,
          Location ,
          Display ,
          CreateDate ,
          GUID
      FROM
          calendar WITH ( NOLOCK )
      WHERE
          display = 1 AND EndDate > GETDATE() - 1
      ORDER BY
          EndDate ASC


END
GO
/****** Object:  StoredProcedure [dbo].[FavoritesGetUserFavorites]    Script Date: 04/05/2012 12:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[FavoritesGetUserFavorites] 
	-- Add the parameters for the stored procedure here
       @UserId varchar(100) ,
       @IPaddress varchar(50)
AS
BEGIN

      IF @UserId <> '' AND @UserId <> 'NONE'
         UPDATE
             Favorites
         SET
             UserId = @UserId
         WHERE
             IPaddress = @IPaddress


      SELECT
          FavoriteId ,
          UserId ,
          URL ,
          Title ,
          IPaddress ,
          CreateDate
      FROM
          Favorites
      WHERE
          UserId = @UserId OR IPaddress = @IPaddress
      ORDER BY
          [CreateDate] DESC

END
GO
/****** Object:  StoredProcedure [dbo].[FavoriteAdd]    Script Date: 04/05/2012 12:12:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[FavoriteAdd]
       @UserId varchar(max) ,
       @URL varchar(max) ,
       @Title varchar(max) ,
       @IPaddress varchar(50)
AS
BEGIN

      INSERT INTO
          Favorites
          (
            URL ,
            Title ,
            UserId ,
            IPaddress )
      VALUES
          (
            @URL ,
            @Title ,
            @UserId ,
            @Ipaddress )

END
GO
/****** Object:  StoredProcedure [dbo].[ExceptionsGetReport]    Script Date: 04/05/2012 12:12:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[ExceptionsGetReport] 
	AS
BEGIN

SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	 SELECT
      Date = DATEPART(day,DateLastOccurred)
    , Hour = DATEPART(hour,DateLastOccurred) 
    , TotalCount = SUM(Frequency)
 FROM dbo.Exceptions WITH(READUNCOMMITTED)
 WHERE DateLastOccurred > DATEPART(day,GETDATE() -3)
 GROUP BY
      DATEPART(day,DateLastOccurred)
    , DATEPART(hour,DateLastOccurred)
 ORDER BY
      DATEPART(day,DateLastOccurred)
    , DATEPART(hour,DateLastOccurred)
    
END
GO
/****** Object:  StoredProcedure [dbo].[Exceptions_Log]    Script Date: 04/05/2012 12:12:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[Exceptions_Log]
(	
	@ExceptionHash varchar(128),
	@ServerName VARCHAR(64),
	@Exception varchar(200),
	@ExceptionMessage varchar(MAX),
	@UserAgent varchar(64),
	@IPAddress varchar(15),
	@HttpReferrer varchar (256),
	@HttpVerb varchar(24),
	@PathAndQuery varchar(512)
)
AS
BEGIN

SET Transaction Isolation Level Read UNCOMMITTED

IF EXISTS (SELECT ExceptionID FROM Exceptions WHERE ExceptionHash = @ExceptionHash and ServerName = @ServerName)

	UPDATE
		Exceptions
	SET
		DateLastOccurred = GetDate(),
		Frequency = Frequency + 1
	WHERE
		ExceptionHash = @ExceptionHash and ServerName = @ServerName
ELSE
	INSERT INTO 
		Exceptions
	(
		ExceptionHash,		
		ServerName,
		Exception,
		ExceptionMessage,
		UserAgent,
		IPAddress,
		HttpReferrer,
		HttpVerb,
		PathAndQuery,
		DateCreated,
		DateLastOccurred,
		Frequency
	)
	VALUES
	(
		@ExceptionHash,		
		@ServerName,
		@Exception,
		@ExceptionMessage,
		@UserAgent,
		@IPAddress,
		@HttpReferrer,
		@HttpVerb,
		@PathAndQuery,
		GetDate(),
		GetDate(),
		1
	)

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetContainsQuery]    Script Date: 04/05/2012 12:13:20 ******/
CREATE FUNCTION [dbo].[GetContainsQuery](@value [nvarchar](1000))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [FullTextQueryGenerator].[UserDefinedFunctions].[GetContainsQuery]
GO
/****** Object:  StoredProcedure [dbo].[FellowsGetBio]    Script Date: 04/05/2012 12:12:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[FellowsGetBio] @control int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
      SET NOCOUNT ON ;

      SELECT
          name ,
          bio
      FROM
          fellows
      WHERE
          ( control = @control )
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentSubjectsGetList]    Script Date: 04/05/2012 12:12:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentSubjectsGetList] @CategoryId int = 0
AS
BEGIN
      IF @CategoryId > 0
         SELECT
             Subject ,
             SubjectId
         FROM
             DocumentSubjects
         WHERE
             Visible = 1 AND [CategoryId] = @CategoryId
         ORDER BY
             SortOrder
      ELSE
         SELECT
             Subject ,
             SubjectId
         FROM
             DocumentSubjects
         WHERE
             Visible = 1
         ORDER BY
             SortOrder
END
GO
/****** Object:  StoredProcedure [dbo].[DonationInsert]    Script Date: 04/05/2012 12:12:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DonationInsert]
       @DonationID int OUTPUT ,
       @FirstName nvarchar(100) ,
       @LastName nvarchar(100) ,
       @Phone varchar(50) ,
       @Address nvarchar(2000) ,
       @City nvarchar(200) ,
       @State varchar(50) ,
       @Zip varchar(50) ,
       @Country varchar(50) ,
       @Email nvarchar(200) ,
       @MisesMember bit ,
       @Amount money ,
       @OtherAmount money ,
       @Designation varchar(500) ,
       @OtherDesignation varchar(500) ,
       @Recurring bit ,
       @RecurringInterval int ,
       @GiftEstate bit ,
       @GiftIncome bit ,
       @CardName varchar(100) ,
       @CardType varchar(50) ,
       @CardNumber varchar(50) ,
       @CardExpMonth int ,
       @CardExpYear int ,
       @Comments varchar(max) ,
       @ApprovalCode int ,
       @TransactionId varchar(50) ,
       @CreateUserID int
AS
BEGIN

      INSERT INTO
          Donations
          (
            FirstName ,
            LastName ,
            Phone ,
            [Address] ,
            [State] ,
            Zip ,
            City ,
            Country ,
            Email ,
            MisesMember ,
            Amount ,
            OtherAmount ,
            Designation ,
            OtherDesignation ,
            Recurring ,
            RecurringInterval ,
            GiftIncome ,
            GiftEstate ,
            CardName ,
            CardType ,
            CardNumber ,
            CardExpMonth ,
            CardExpYear ,
            Comments ,
            ApprovalCode ,
            CreateTime ,
            CreateUserID ,
            TransactionId )
      VALUES
          (
            @FirstName ,
            @LastName ,
            @Phone ,
            @Address ,
            @State ,
            @Zip ,
            @City ,
            @Country ,
            @Email ,
            @MisesMember ,
            @Amount ,
            @OtherAmount ,
            @Designation ,
            @OtherDesignation ,
            @Recurring ,
            @RecurringInterval ,
            @GiftIncome ,
            @GiftEstate ,
            @CardName ,
            @CardType ,
            @CardNumber ,
            @CardExpMonth ,
            @CardExpYear ,
            @Comments ,
            @ApprovalCode ,
            GETDATE() ,
            @CreateUserID ,
            @TransactionId )

      SET @DonationId = SCOPE_IDENTITY()

END
GO
/****** Object:  StoredProcedure [dbo].[DonationGetTotal]    Script Date: 04/05/2012 12:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[DonationGetTotal] 	
AS
BEGIN
	SELECT SUM(OtherAmount + Amount) FROM donations WHERE LastName NOT IN ('Veksler','Tucker') AND Email NOT IN ('heroic@gmail.org') AND email NOT LIKE '%mises.org%'
END
GO
/****** Object:  StoredProcedure [dbo].[DonationGetReport]    Script Date: 04/05/2012 12:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[DonationGetReport] 
	AS
BEGIN

SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	 SELECT
    --  Day = DATEPART(day,CreateTime) 
     Month = DATEPART(month,CreateTime) 
    , Year = DATEPART(year,CreateTime) 
    
     --, CAST(DATEPART(day,CreateTime) as VARCHAR(MAX))  + '/' +
     , CAST(DATEPART(month,CreateTime) as VARCHAR(MAX)) + '/' + CAST(DATEPART(year,CreateTime) as VARCHAR(MAX)) AS Date
    , Total = SUM(Amount + OtherAmount)
 FROM dbo.Donations WITH(READUNCOMMITTED)
 WHERE LastName NOT IN ('Veksler','Tucker') AND Email NOT IN ('heroic@gmail.org') AND email NOT LIKE '%mises.org%' AND FirstName != 'OLAOYE RAYMOND WILSON'
-- WHERE CreateTime > DATEPART(day,GETDATE() -30)
 GROUP BY
    --  DATEPART(day,CreateTime)
     DATEPART(month,CreateTime)
    , DATEPART(year,CreateTime) 
    ,CAST(DATEPART(month,CreateTime) as VARCHAR(MAX)) + '/' + CAST(DATEPART(year,CreateTime) as VARCHAR(MAX))
 ORDER BY
	 DATEPART(year,CreateTime) 
    , DATEPART(month,CreateTime) 
  --  ,  DATEPART(day,CreateTime) DESC
    
    
    
END
GO
/****** Object:  StoredProcedure [dbo].[DonationGetList]    Script Date: 04/05/2012 12:12:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE  [dbo].[DonationGetList] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT DonationID,
		   FirstName + ' ' + LastName AS Name, 
		   Email,
		   Amount + OtherAmount AS 'Amount',		   
		   Recurring,		   
		   CardType,		   
		   CreateTime		   
		   FROM dbo.Donations 
    WHERE LastName NOT IN ('Veksler','Tucker') AND Email NOT IN ('heroic@gmail.org') AND email NOT LIKE '%mises.org%'
    ORDER BY DonationID DESC
    
    
--    SELECT CreateTIme, Amount, CardName FROM dbo.Donations 
--WHERE DATEPART(YEAR, CreateTIme) = 2009
--AND DATEPART(MONTH,CreateTIme) = 1
--ORDER BY Amount DESC

END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetLibertarianStudiesSubjects]    Script Date: 04/05/2012 12:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentGetLibertarianStudiesSubjects]
AS
BEGIN
      SELECT
          SubjectId ,
          ShortSubject
      FROM
          DocumentSubjects
      WHERE
          CategoryID = 4 AND Visible = 1
      ORDER BY
          sortorder
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentAuthorsAdd]    Script Date: 04/05/2012 12:12:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentAuthorsAdd] 
	-- Add the parameters for the stored procedure here
       @AuthorFirst varchar(255) ,
       @AuthorMiddle varchar(255) ,
       @AuthorLast varchar(255)
AS
BEGIN

      INSERT INTO
          DocumentAuthors
          (
            AuthorFirst ,
            AuthorMiddle ,
            AuthorLast )
      VALUES
          (
            @AuthorFirst ,
            @AuthorMiddle ,
            @AuthorLast )
END
GO
/****** Object:  StoredProcedure [dbo].[CalendarGetEventDetail]    Script Date: 04/05/2012 12:11:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[CalendarGetEventDetail] @EventId int
AS
BEGIN
      SELECT
          EventId ,
          Title ,          
          EventDate ,
          StartDate,
          EndDate ,
          IntroText ,
          [Description] ,
          Location ,
          Display ,
          CreateDate
      FROM
          calendar
      WHERE
          EventId = @EventId
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentMediaTypeGetList]    Script Date: 04/05/2012 12:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentMediaTypeGetList]
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT
          MediaType ,
          MediaTypeId ,
          MediaIconPath
      FROM
          DocumentMediaType
      WHERE
            MediaTypeId NOT IN ( 1 , 2 , 8,12,14 )
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetAustrianEconomicsSubjects]    Script Date: 04/05/2012 12:12:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentGetAustrianEconomicsSubjects]
AS
BEGIN
      SELECT
          SubjectId ,
          ShortSubject
      FROM
          DocumentSubjects
      WHERE
          CategoryID = 1 AND Visible = 1
      ORDER BY
          sortorder
END
GO
/****** Object:  StoredProcedure [dbo].[MediaDetails]    Script Date: 04/05/2012 12:12:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
**********************************************************************************************************/

CREATE  PROCEDURE [dbo].[MediaDetails]
       @FileId int
as
set concat_null_yields_null off

declare @DocumentId int

select			@DocumentId = DocumentId
from			dbo.DocumentFiles
where			FileId = @FileId;

exec dbo.DocumentDetails @DocumentId = @DocumentId;

return @@error;
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetSourceJournalList]    Script Date: 04/05/2012 12:12:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentGetSourceJournalList]
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT DISTINCT
          source
      FROM
          Documents
      WHERE
          [Display] = 1 AND source <> '' AND PATINDEX('%http%' , source) = 0
      UNION ALL
      SELECT DISTINCT
          Title
      FROM
          Periodicals
      ORDER BY
          [source]
END
GO
/****** Object:  StoredProcedure [dbo].[FindDocuments]    Script Date: 04/05/2012 12:12:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}





/**********************************************************************************************************
Author:		Vasily Kabanov
Created:	2011-10-19
Arguments	@searchString - freetext search string, max 1000 characters long
			@pageSize - max number of documents to return, default 20
			@pageNo - zero-based page number to return
			@categoryId - category of documents to return, see dbo.Documents.CategoryId
				use null or negative number to ignore
			@subjectId - subject of documents to return, see dbo.DocumentSubjectLink;
				use null or negative number to ignore
			@authorId - author of documents to return, see dbo.Documents.Author1, dbo.Documents.Author1;
				use null or negative number to ignore
			@mediaTypeIdList - media types of ducuments to return (only documents with visible media of the
				listed types will be returned); list of media type IDs, e.g. '1;2;4',
				see dbo.DocumentFiles.MediaTypeId; specify empty string or null to ignore
			@idListDelimiter - the delimiter used in ID lists (e.g. @mediaTypeIdList), default is ';'
			@titleWeight - when search string is specified, assigns weight to the document title match as
				opposed to Description and PublicationInformation, see function dbo.SearchDocuments; default is 10;
			
Returns:	return code: 0 - success; otherwise - failure
			result set (
				DocumentId	-- see dbo.Documents table, all columns
				, GUID
				, Display
				, 0 as Featured
				, Title
				, Author1
				, Author2
				
				, CategoryId
				, CreateDate
				, EditDate
				, metaImage		-- always null
			) -- ordered by -- FullText match rank descending (if search string is specified) and DocumentId desc
Comment:	Implements comprehensive documents search (as comprehensive as there was need and time to implement).

Examples

-----------------------------
-- all options

declare @count int;
exec dbo.FindDocuments
	@categoryId = 170

exec dbo.FindDocuments
	@searchString = N'Austrian Perspective'
	, @categoryId = 0
	, @authorId = 299
	, @mediaTypeIdList = N'1;2;4;8;9;3'
	, @subjectId = 117
	, @pageNo = 0
	, @pageSize = 20
	, @count = @count out

print 'count = ' + convert(varchar(10), @count)
---------------------------
-- no options

declare @count int;

exec dbo.FindDocuments @pageNo = 2

print 'count = ' + convert(varchar(10), @count)
----------------
-- no search text
declare @count int;

exec dbo.FindDocuments
	@searchString = N''
	, @categoryId = 87
	, @authorId = 299
	, @mediaTypeIdList = N'1;2;4;8;'
	, @subjectId = 117
	, @pageNo = 0
	, @pageSize = 20
	, @count = @count out

print 'count = ' + convert(varchar(10), @count)
----------------
-- only search text
declare @count int;

exec dbo.FindDocuments
	@searchString = N'"Gary North"'
	, @titleWeight = 10
	, @pageNo = 0
	, @pageSize = 10
	, @mediaTypeIdList = null --N'1;2;8;14'
	, @count = @count out

print 'count = ' + convert(varchar(10), @count)

exec dbo.FindDocuments
	@searchString = N'Gary North'
	, @mediaTypeIdList = N'1;2;8;14'

**********************************************************************************************************/
CREATE  PROCEDURE [dbo].[FindDocuments]
	@searchString nvarchar(1000) = null
	, @pageSize int = 20
	, @pageNo int = 0
	, @categoryId int = null
	, @subjectId int = null
	, @authorId int = null
	, @mediaTypeIdList nvarchar(200) = null
	, @idListDelimiter nvarchar(4) = N';'
	, @titleWeight float = 10
	, @count int = null out
as
set nocount on;
set fmtonly off;



declare @sql nvarchar(max);

-- each CTE must return unique set of IDs
select	@sql = N'
with CategoryTree as (
	select		@categoryId as TopCategoryId
				, @categoryId as CategoryId
	union all
	select		p.TopCategoryId
				, ch.CategoryId
	from		dbo.MediaCategory ch
				, CategoryTree p
	where		p.CategoryId = ch.ParentCategory
)
, ByCategory as (
	select			d.DocumentId
	from			dbo.Documents d
					, CategoryTree t
	where			d.CategoryId = t.CategoryId
)
, BySubject as (
	select			DocumentId
	from			dbo.DocumentSubjectLink
	where			SubjectID = @subjectId
)
, ByAuthor as (
	select			DocumentId
	from			dbo.Documents
	where			Author1 = @authorId
	union
	select			DocumentId
	from			dbo.Documents
	where			Author2 = @authorId
)
, ByMediaType as (
	select			distinct
					DocumentId
	from			dbo.DocumentFiles f
					, dbo.SplitUniqueIntTable(@mediaTypeIdList, @idListDelimiter, 0) i
	where			f.MediaTypeId = i.val
)';

select	@searchString = dbo.GetContainsQuery(@searchString)
		, @mediaTypeIdList = ltrim(rtrim(@mediaTypeIdList));

-- a number of CTEs, one of them called "Ordered"
declare		@sqlAllIdsOrdered	nvarchar(max);
-- a number of CTEs, one of them called "IdPage"
declare		@sqlIdPage	nvarchar(max);

declare @from nvarchar(1000);
declare @where nvarchar(2000);

select	@from = N''
		, @where = N'';

declare		@searchStringLength int;
select		@searchStringLength = coalesce(len(@searchString), 0)
			, @categoryId = coalesce(@categoryId, -1)
			, @subjectId = coalesce(@subjectId, -1)
			, @authorId = coalesce(@authorId, -1)
			, @mediaTypeIdList = coalesce(@mediaTypeIdList, N'');

if @searchStringLength = 0
	and @categoryId < 0
	and @subjectId < 0
	and @authorId < 0
	and @mediaTypeIdList = N''
begin
	-- no search criteria, just return a page of all documents sorted by DocumentId (which should produce the same order
	-- as CreateDate, but will be cheaper)
	select @sqlAllIdsOrdered = N'
with Ordered as (
	select		DocumentId
				, row_number() over(order by DocumentId desc) as rnumber
	from		dbo.Documents
)
'
end -- if no criteria specified
else if @searchStringLength > 0
begin
	-- search text specified
	select		@sql = @sql + N'
, Ordered as (
	select			s.DocumentId
					, row_number() over (order by s.rank desc, s.DocumentId desc) as rnumber
	from			dbo.SearchDocuments(@searchString, @titleWeight) s
					join dbo.Documents d
						on d.DocumentId = s.DocumentId
						and d.Display = 1';
	
	if @categoryId >= 0
	begin
		select	@sql = @sql + N'
					join ByCategory c
						on c.DocumentId = s.DocumentId';
	end
	
	if @subjectId >= 0
	begin
		select	@sql = @sql + N'
					join BySubject sbj
						on sbj.DocumentId = s.DocumentId';
	end
	
	if @authorId >= 0
	begin
		select	@sql = @sql + N'
					join ByAuthor a
						on a.DocumentId = s.DocumentId';
	end
	
	if len(@mediaTypeIdList) > 0
	begin
		select	@sql = @sql + N'
					join ByMediaType mt
						on mt.DocumentId = s.DocumentId';
	end

	select		@sqlAllIdsOrdered = @sql + N'
)';

end -- else if len(@searchString) > 0
else
begin
	-- at least 1 advanced option (e.g. subject) is specified and no search string
	
	declare @tbTables table (
		ord tinyint not null identity(1, 1) primary key
		, tableName sysname not null	-- may be CTE
		, tableAlias sysname not null unique
	);
	
	insert	@tbTables (tableName, tableAlias)
	select	N'ByCategory', N'c'
	where	@categoryId >= 0;

	insert	@tbTables (tableName, tableAlias)
	select	N'BySubject', N'sbj'
	where	@subjectId >= 0;

	insert	@tbTables (tableName, tableAlias)
	select	N'ByAuthor', N'a'
	where	@authorId >= 0;

	insert	@tbTables (tableName, tableAlias)
	select	N'ByMediaType', N'mt'
	where	len(@mediaTypeIdList) > 0;
	
	declare @firstAlias sysname;
	
	select	@firstAlias = tableAlias
	from	@tbTables
	where	ord = 1;

	select	@from = @from
			+ tableName
			+ N' '
			+ tableAlias
			+ N'
			, '
	from	@tbTables;

	-- removing last comma
	select	@from = @from
				+ N'dbo.Documents d
';

	select	@where = @where
				+ t1.tableAlias
				+ N'.'
				+ N'DocumentId = '
				+ ta.tableAlias
				+ N'.'
				+ N'DocumentId
				and '
	from	@tbTables t1
			, @tbTables ta
	where	t1.ord = 1
			and ta.ord > 1;

	-- removing last 'and '
	select	@where = @where
				+ N'd.DocumentId = ' + @firstAlias + N'.DocumentId
				and d.Display = 1
';
	
	select		@sqlAllIdsOrdered = @sql + N'
, Ordered as (
	select		'
			+ @firstAlias + N'.DocumentId
			, row_number() over (order by ' + @firstAlias + '.DocumentId desc) as rnumber
	from	'
	+ @from
	+ N'
	where	'
	+ @where
	+ N'
)';

end

select		@sqlIdPage = @sqlAllIdsOrdered + N'
, IdPage as (
	select		top (@pageSize)
				DocumentId
				, rnumber
	from		Ordered
	where		rnumber >= @pageNo * @pageSize
)';

--------------------
-- now common final sql
declare		@sqlCount nvarchar(max);
select		@sqlCount = @sqlAllIdsOrdered + N'
select @count = count(*) from Ordered';

declare		@sqlDocPage nvarchar(max);
	select		@sqlDocPage = @sqlIdPage + N'
select		p.DocumentId
			, d.GUID
			, d.Display
			, 0 as Featured
			, d.Title
			, d.Author1
			, d.Author2
			
			, d.CategoryId
			, d.CreateDate
			, d.EditDate
			, null as metaImage
from		IdPage p
			join dbo.Documents d
				on p.DocumentId = d.DocumentId
order by	rnumber
';


declare @retcode int;

print N'count sql: ' + @sqlCount;

exec @retcode = sys.sp_executesql
	@stmt = @sqlCount
	, @params = N'
@searchString nvarchar(1000)
, @categoryId int
, @subjectId int
, @authorId int
, @mediaTypeIdList nvarchar(200)
, @idListDelimiter nvarchar(4)
, @titleWeight float
, @count int out'
	, @searchString = @searchString
	, @categoryId = @categoryId
	, @subjectId = @subjectId
	, @authorId = @authorId
	, @mediaTypeIdList = @mediaTypeIdList
	, @idListDelimiter = @idListDelimiter
	, @titleWeight = @titleWeight
	, @count = @count out
;


print N'doc page: ' + @sqlDocPage;

exec @retcode = sys.sp_executesql
	@stmt = @sqlDocPage
	, @params = N'
@searchString nvarchar(1000)
, @pageSize int
, @pageNo int
, @categoryId int
, @subjectId int
, @authorId int
, @mediaTypeIdList nvarchar(200)
, @idListDelimiter nvarchar(4)
, @titleWeight float'
	, @searchString = @searchString
	, @pageSize = @pageSize
	, @pageNo = @pageNo
	, @categoryId = @categoryId
	, @subjectId = @subjectId
	, @authorId = @authorId
	, @mediaTypeIdList = @mediaTypeIdList
	, @idListDelimiter = @idListDelimiter
	, @titleWeight = @titleWeight
;

--return @@error;
GO
/****** Object:  StoredProcedure [dbo].[DocumentTopAuthors]    Script Date: 04/05/2012 12:12:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentTopAuthors]
AS
BEGIN
      SET NOCOUNT ON ;

      SET CONCAT_NULL_YIELDS_NULL OFF

      SELECT TOP 30
          AuthorId ,
          AuthorLast ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName' ,
          Photo,
          COUNT(1) AS 'Count'
      FROM
          [Documents] d JOIN [DocumentAuthors] da
      ON  d.[Author1] = da.[AuthorId] OR d.[Author2] = da.[AuthorId]
      GROUP BY
          [AuthorId] ,
          [AuthorLast] ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast,
          Photo
      ORDER BY
          COUNT DESC

END
GO
/****** Object:  StoredProcedure [dbo].[DocumentDetails]    Script Date: 04/05/2012 12:12:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
**********************************************************************************************************/

CREATE  PROCEDURE [dbo].[DocumentDetails]
       @DocumentId int = 0 ,
       @FileId int = 0
AS
set concat_null_yields_null off

IF @FileId  > 0
BEGIN
	EXEC dbo.MediaDetails @FileId
	RETURN
END

DECLARE @ParentCategoryId int

SET @ParentCategoryId = ( SELECT TOP 1
                              ParentCategory
                          FROM
                              MediaCategory dc
                          WHERE
                              dc.CategoryId = ( SELECT
                                                    CategoryId
                                                FROM
                                                    dbo.Documents
                                                WHERE
                                                    DocumentId = @DocumentId ) )

SELECT
    Documents.DocumentId ,
    GUID ,
    1 as Display ,
    Title ,
    Author1 ,
    Author2 ,
    PublicationInformation ,
    ---- FullText ,
    Source ,
    --Description ,
    Documents.CreateDate ,
    CategoryId ,
    ProductId,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS AuthorName ,
    DocumentAuthors2.AuthorFirst + ' ' + DocumentAuthors2.AuthorMiddle + ' ' +
    DocumentAuthors2.AuthorLast AS CoAuthorName ,
    @ParentCategoryId AS ParentCategoryId ,
    ( SELECT TOP 1
          ParentCategory
      FROM
          MediaCategory dc
      WHERE
          dc.CategoryId = @ParentCategoryId ) AS GrandParentCategoryId ,
    0 AS filesize,
    Description,
    Keywords ,
    
    (SELECT TOP 1 Description FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreDescription,
    --SELECT ModelNumber, SearchKeywords FROM AbleCommerce.dbo.ac_Products
    (SELECT TOP 1 ModelNumber FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS ISBN,
    (SELECT TOP 1 SearchKeywords FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreKeywords,    
    --REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE [Name] = Documents.Title),'~','/store/') AS CoverImage
    Documents.CoverImageURL as CoverImage,
    --REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId),'~','/store/') AS CoverImage, 
    Documents.ProductId
FROM
    Documents 
 --   LEFT JOIN DocumentFiles ON DocumentFiles.DocumentId = Documents.DocumentId
    
 LEFT JOIN dbo.DocumentAuthors
ON  Documents.Author1 = DocumentAuthors.AuthorId
 LEFT JOIN dbo.DocumentAuthors DocumentAuthors2
ON  Documents.Author2 = DocumentAuthors2.AuthorId
WHERE
    ( Documents.DocumentId = @DocumentId )

SELECT
    DocumentSubjectLink.SubjectId
FROM
    DocumentSubjectLink JOIN DocumentSubjects
ON  DocumentSubjects.SubjectId = DocumentSubjectLink.SubjectId
WHERE
    DocumentId = @DocumentId

SELECT
    DocumentFiles.FileId ,
    DocumentFiles.DocumentId ,
    DocumentFiles.MediaTypeId ,
    DocumentFiles.URL ,
    DocumentFiles.CreateDate ,    
    DocumentMediaType.MediaTypeID AS Expr1 ,
    DocumentMediaType.MediaType ,
    DocumentMediaType.MediaIconPath ,
    DocumentMediaType.UploadPath ,
    DocumentMediaType.Description ,
    DocumentMediaType.CreateTime ,
    DocumentMediaType.MIMEtype ,
    DocumentMediaType.IsDeleted ,
    DocumentMediaType.Extensions ,
    DocumentMediaType.IsMedia ,
    DocumentFiles.fileSize ,
    DocumentFiles.duration,
    DocumentFiles.VolumeOrdinal,
    DocumentFiles.VolumeComment,
    DocumentFiles.Display
FROM
    DocumentFiles LEFT OUTER JOIN DocumentMediaType
ON  DocumentMediaType.MediaTypeID = DocumentFiles.MediaTypeId
WHERE
    ( DocumentFiles.DocumentId = @DocumentId )

return @@error;
GO
/****** Object:  StoredProcedure [dbo].[DocumentsGetImage]    Script Date: 04/05/2012 12:12:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentsGetImage] @DocumentId int
AS
BEGIN
      SELECT
          CoverImageURL As metaImage
      FROM
          dbo.Documents WITH ( NOLOCK )
      WHERE
          DocumentId = @DocumentId
END
GO
/****** Object:  StoredProcedure [dbo].[CalendarGetEventsList]    Script Date: 04/05/2012 12:11:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[CalendarGetEventsList]
AS
BEGIN
      EXEC dbo.CalendarGetUpcomingEvents

      SELECT TOP 30
          EventId ,
          Title ,
          EventDate ,
          StartDate,
          EndDate ,
          IntroText ,
          EventImage ,
          Location ,
          Display ,
          CreateDate
      FROM
          calendar WITH ( NOLOCK )
      WHERE
          display = 1 AND EndDate < GETDATE() - 1
      ORDER BY
          EndDate DESC
END
GO
/****** Object:  StoredProcedure [dbo].[defrag_database_indexes]    Script Date: 04/05/2012 12:12:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****************************************************************************************************
Author:			Vasily Kabanov
Created:		2010-06-13
Parameters:		
				@maxTableFailedToStop - int - the number of tables/views failed to defragment
					-- (indicated by an error raised in [dbo].[usp_ut_defrag_table_indexes])
					-- before aborting further work
				@rebuildFrafmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index rebuild; default is 30 (recommended by Microsoft);
					-- see [dbo].[usp_ut_defrag_table_indexes]
				@reorganizeFragmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index reorganise; default is 10 (recommended by Microsoft);
					-- must be less or equal to @rebuildFrafmentationThreshold
					-- see [dbo].[usp_ut_defrag_table_indexes]
				@tablesProcessed - int output - total number of tables or indexed views processed
					-- (for which the defragmentation procedure [dbo].[usp_ut_defrag_table_indexes]
					-- was called)
				@tablesFailed - int output - total number of tables or indexed views, during
					-- processing of which an error was raised by the defragmentation procedure
					-- [dbo].[usp_ut_defrag_table_indexes]
				@indexesDefragmented - int output - the total number of index trees defragmented
					-- (using any method); every partition represents a separate index tree
				@indexesRebuilt	- int out - of indexesDefragmented, the number of index partitions
					-- defragmented by rebuilding
				@indexesFailed - output, the number of indexes failed to defragment
				@warningCount - output, the number of non-critical failures, such as faulure
					-- resetting allow_page_locks
Returns:		return code:
					0 - success
					1 - general failure (such all of the indexes failed)
					2 - the procedure was called in transaction
Comment:		The procedure enumerates all indexed tables and views in the current database and
				invokes dbo.defrag_table_indexes for every one of them.
Examples:

exec	[dbo].defrag_database_indexes
****************************************************************************************************/
CREATE  PROCEDURE [dbo].[defrag_database_indexes]
  @maxTableFailedToStop int = 10
, @rebuildFrafmentationThreshold float = 30
, @reorganizeFragmentationThreshold float = 10
, @tablesProcessed int = null output
, @tablesFailed int = null output
, @indexesDefragmented int = null output
, @indexesRebuilt	int = null output
, @indexesFailed int = null output
, @warningCount int = null output
as

set implicit_transactions off;

set nocount on;

declare		@retcode int;

select		@indexesDefragmented = 0
			, @indexesFailed = 0
			, @warningCount = 0
			, @indexesRebuilt = 0
			, @tablesProcessed = 0
			, @tablesFailed = 0
			;

declare		@indexesDefragmentedLocal int
			, @indexesRebuiltLocal	int
			, @indexesFailedLocal int
			, @warningCountLocal int

declare		@retCodeSuccess int
			, @retCodeGeneralFailure int
			, @retCodeFailCalledInTransaction int
			, @retCodeInvalidObject int
			, @retCodeInvalidThresholds int
			, @startTime datetime
			, @endTime datetime;
			
select		@retCodeSuccess = 0
			, @retCodeGeneralFailure = 1
			, @retCodeFailCalledInTransaction = 2
			, @retCodeInvalidObject = 3
			, @retCodeInvalidThresholds = 4
			, @startTime = getdate()
			;

begin try;
	select		@retcode = @retCodeSuccess;
	
	declare		@msg nvarchar(max);

	-------------------------------------
	-- checking pre-conditions
	
	if @rebuildFrafmentationThreshold is null
		or @reorganizeFragmentationThreshold is null
		or (@reorganizeFragmentationThreshold > @rebuildFrafmentationThreshold)
	begin
		set @retcode = @retCodeInvalidThresholds;
		raiserror('An invalid combination of fragmentation thresholds was specified', 16, 1);
	end

	if @@trancount > 0
	begin
		set @retcode = @retCodeFailCalledInTransaction;
		raiserror('The procedure defrag_database_indexes must not be executed in a transaction', 16, 1);
	end
	
	-- end: checking pre-conditions
	-------------------------------------
	select	@msg = N'Commencing defragmentation of indexes in database '
		+ @@servername
		+ N'\'
		+ db_name();

	print @msg;

	declare myc cursor local for
	with TablesAndViews as (
		select		object_id
					, schema_id
		from		sys.tables
		union all
		select		object_id
					, schema_id
		from		sys.views
		where		objectproperty(object_id, 'IsIndexed') = 1
	)
	select		t.object_id
				, quotename(schema_name(t.schema_id)) + N'.' + quotename(object_name(t.object_id))
					as ObjectName
	from		TablesAndViews t
	
	open myc;

	declare		@objectID int
				, @objectName sysname;

	while 1 = 1
	begin
		fetch next from myc into @objectID, @objectName;
		
		if @@fetch_status != 0
		begin
			break;
		end
		
		begin try
			exec @retcode = [dbo].[defrag_table_indexes]
				@objectID = @objectID
				, @rebuildFrafmentationThreshold = @rebuildFrafmentationThreshold
				, @reorganizeFragmentationThreshold = @reorganizeFragmentationThreshold
				, @indexesDefragmented = @indexesDefragmentedLocal out
				, @indexesRebuilt = @indexesRebuiltLocal out
				, @indexesFailed = @indexesFailedLocal out
				, @warningCount = @warningCountLocal out
				
			select	@indexesRebuilt = @indexesRebuilt + @indexesRebuiltLocal
					, @indexesDefragmented = @indexesDefragmented + @indexesDefragmentedLocal
					, @indexesFailed = @indexesFailed + @indexesFailedLocal
					, @warningCount = @warningCount + @warningCountLocal
					, @tablesProcessed = @tablesProcessed + 1
					;
				
		end try
		begin catch
			select	@tablesFailed = @tablesFailed + 1;
			
			if @tablesFailed > @maxTableFailedToStop
			begin
				select		@retcode = @retCodeGeneralFailure
							, @msg = N'Maximum failed tables exceeded, aborting defragmentation';
				raiserror(@msg, 16, 1);
			end
		end catch
	end
	
	declare		@newline nchar(2);
	select		@newline = nchar(13) + nchar(10);
	select		@endTime = getdate();

	declare		@durationFormatted nvarchar(50);
	select		@durationFormatted =
					convert(nvarchar(10), datediff(hour, @startTime, @endTime))
					+ N' hours, '
					+ convert(nvarchar(10), datediff(minute, @startTime, @endTime) % 60)
					+ N' minutes, '
					+ convert(nvarchar(10), datediff(second, @startTime, @endTime) % 60)
					+ N' seconds';

	select @msg = N'Maintenance report for database '
				+ @@servername + N'\' + db_name()
				+ N':'
				
				+ @newline
				+ N'Time taken: '
				+ @durationFormatted
				
				+ @newline
				+ N'Tables/views processed: '
				+ convert(nvarchar(max), @tablesProcessed)
				
				+ @newline
				+ N'Tables/views failed to defragment: '
				+ convert(nvarchar(max), @tablesFailed)

				+ @newline
				+ N'Total indexes defragmented: '
				+ convert(nvarchar(max), @indexesDefragmented)

				+ @newline
				+ N'Total indexes rebuilt: '
				+ convert(nvarchar(max), @indexesRebuilt)

				+ @newline
				+ N'Total indexes failed to defragment: '
				+ convert(nvarchar(max), @indexesFailed)

				+ @newline
				+ N'Total warnings: '
				+ convert(nvarchar(max), @warningCount)
			
			, @retcode = @retCodeSuccess
		;
	
	print @msg;
	
end try
begin catch
	declare @tmpcode int;
	exec @tmpcode = dbo.util_rethrow_exception
	
	if @retcode = @retCodeSuccess
	begin
		select @retcode = @tmpcode;
	end
end catch

return @retcode;
GO
/****** Object:  StoredProcedure [dbo].[_MediaCategoryUpdateImages]    Script Date: 04/05/2012 12:11:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE  [dbo].[_MediaCategoryUpdateImages]
AS
BEGIN
      UPDATE
          dbo.MediaCategory
      SET
          CategoryImage = 'http://mises.org/media/poster/' + CAST
          (dbo.Documents.DocumentId AS varchar(max))
      FROM
          dbo.MediaCategory JOIN dbo.Documents
          ON dbo.Documents.CategoryId = dbo.MediaCategory.CategoryId
      WHERE
          CategoryImage IS NULL AND CoverImage IS NOT NULL
END
GO
/****** Object:  StoredProcedure [dbo].[_JLStoDocuments]    Script Date: 04/05/2012 12:11:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stephen W. Carson, JLStoDocuments>
-- Create date: <August 26, 2007>
-- Description:	<Copies info from the jls table into the Documents structure>
-- =============================================
CREATE  PROCEDURE  [dbo].[_JLStoDocuments] @last_id int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
      SET NOCOUNT ON ;

      INSERT INTO
          Documents
          (
            Display ,
--            oldMediaTypeId ,
            Source ,
            Title ,
            Author1 ,
            Author2 ,
            PubInfo ,
            metaDescription
             )
          SELECT
              0 ,
           --   3 ,
              'JLS' ,
              '"' + j.title + '"' ,
              a1.AuthorId ,
              a2.AuthorId,              
              '<em>Journal of Libertarian Studies</em>, ' + CAST(j.volume AS varchar(10))
              + '(' + CAST(j.number AS varchar(10)) + ').' ,
              'http://www.mises.org/journals/jls/' + CAST(j.volume AS varchar(10)) + '_'
              + CAST(j.number AS varchar(10)) + '/' + CAST(j.volume AS varchar(10)) + '_'
              + CAST(j.number AS varchar(10)) + '_' + CAST(j.articleNum AS varchar(10)) +
              '.pdf' 
          FROM
              jls j LEFT OUTER JOIN DocumentAuthors a1
          ON  j.authorFirst1 = a1.AuthorFirst AND j.authorLast1 = a1.AuthorLast LEFT
          OUTER JOIN DocumentAuthors a2
          ON  j.authorFirst2 = a2.AuthorFirst AND j.authorLast2 = a2.AuthorLast
          WHERE
              j.volume = 20 AND j.number = 2 AND j.articleNum = 1
          ORDER BY
              j.volume ,
              j.number ,
              j.articleNum

      SET @last_id = SCOPE_IDENTITY()

      INSERT INTO
          MediaAlternateFormat
          (
            DocumentID ,
            MediaTypeID ,
            URL 
            )
          SELECT
              @last_id ,
              3 ,
              metaDescription
          FROM
              Documents
          WHERE
              DocumentID = @last_id
END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetSubCategories]    Script Date: 04/05/2012 12:12:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaGetSubCategories] @CategoryId int = 0
AS
BEGIN
      SELECT
          MediaCategory.CategoryId ,
          MediaCategory.Category ,
          MediaCategory.Description ,
          MediaCategory.CategoryImage ,
          ISNULL(( SELECT TOP 1
                       Documents.CreateDate
                   FROM
                       dbo.Documents
                   WHERE
                       Documents.CategoryId = MediaCategory.CategoryId
                   ORDER BY
                       Documents.CreateDate DESC ) , '1/1/2050') AS EditDate
      FROM
          dbo.MediaCategory
      WHERE
          MediaCategory.ParentCategory = @CategoryId
      ORDER BY
          SortOrder ,
          Category
END
GO
/****** Object:  StoredProcedure [dbo].[RegistrationFormDetails]    Script Date: 04/05/2012 12:12:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date:  12/2005
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[RegistrationFormDetails] @FormId int
AS
BEGIN
      SET NOCOUNT ON

      SELECT
          *
      FROM
          RegistrationForms
      WHERE
          FormId = @FormId
      SELECT
          *
      FROM
          RegistrationProducts
      WHERE
          FormId = @FormId
      ORDER BY
          ProductOrder
      SELECT
          *
      FROM
          RegistrationQuestions
      WHERE
          FormId = @FormId
      ORDER BY
          QuestionOrder

END
GO
/****** Object:  UserDefinedFunction [dbo].[SearchDocuments]    Script Date: 04/05/2012 12:13:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/***************************************************************************************************************************
Author:		Vasily Kabanov
Created:	17/10/2011
Parameters:	@searchString - the CONTAINS search string; invalid syntax will result in error
			@titleWeight - weight to assign to title match rank;
Returns:	table (
				DocumentId
				, rank			-- abstract number indicating how close the document title, metaDescription and PubInfo
					-- match the search string; specify 1.0 to regard Title as important as the other searchable document
					-- attributes.
			) --UnOrdered!
Comment:	The function encapsulates fulltext search of the Documents table. It allows to increase the priority of
			document title when ranking matches. It returns unordered fulltext service results only, so the results
			will need to be joined, ordered, filtered and limited at higher levels.
			
Examples:

select			top(100)
				d.Title
				, d.DocumentId
				, s.rank
				, d.Author1
from			dbo.SearchDocuments(N'"Gary North"', 10) s
				, dbo.Documents d
where			d.Display = 1
				and d.DocumentId = s.DocumentId
				--and s.Rank > 200
order by		s.rank desc

***************************************************************************************************************************/
CREATE function [dbo].[SearchDocuments](@searchString nvarchar(1000), @titleWeight float)
returns table
as
return
select		
			coalesce(kt.[KEY], kr.[KEY]) as DocumentId
			, coalesce(kt.RANK, 0) * @titleWeight + coalesce(kr.RANK, 0) as rank
from		containstable(Documents, Title, @searchString) kt
			full join containstable(Documents, (Description, Keywords, PublicationInformation), @searchString) kr
				on kt.[KEY] = kr.[KEY]
GO
/****** Object:  StoredProcedure [dbo].[TagGetUserTags]    Script Date: 04/05/2012 12:13:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetUserTags] 
@User varchar(100) ,
       @User2 varchar(100) = ''
AS
BEGIN
	
      SELECT TOP 5000
          Tag.Tag ,
          COUNT(Tag.TagId) AS 'Count' ,
          Tag.TagId
      FROM
          Tag INNER JOIN TagMap
      ON  Tag.TagId = TagMap.TagId
      WHERE
          ( TagMap.TaggedBy = @User ) OR ( TagMap.TaggedBy = @User2 )
      GROUP BY
          Tag.TagId ,
          Tag.Tag
END
GO
/****** Object:  StoredProcedure [dbo].[TagGetAnonymousTaggers]    Script Date: 04/05/2012 12:13:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetAnonymousTaggers]
AS
BEGIN

--SELECT * FROM tag JOIN dbo.TagMap ON dbo.Tag.TagId = dbo.TagMap.TagId
--WHERE Tag.tagid NOT IN (SELECT tagid FROM tagmap WHERE TaggedBy = 'SearchEngineBot' OR TaggedBy = 'MisesBot')
--ORDER BY TagMap.tagid desc


      SELECT TOP 3000
          TaggedBy ,
          Tag ,
          TagMap.TaggedDate
      FROM
          TagMap JOIN Tag
      ON  TagMap.TagId = Tag.TagId
      WHERE
          TaggedBy NOT IN ('SearchEngineBot', 'MisesBot' , 'HeroicLife' , 'jtucker' )
      ORDER BY
          TagMap.TaggedDate DESC

END
GO
/****** Object:  StoredProcedure [dbo].[TagDelete]    Script Date: 04/05/2012 12:13:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagDelete]
       @tag varchar(75) = NULL ,
       @user varchar(100) = NULL ,
       @Identifier uniqueidentifier = NULL
AS
BEGIN

      IF @Identifier IS NOT NULL
         BEGIN

               IF @tag IS NOT NULL
                  DELETE  FROM
                          TagMap
                  FROM
                          TagMap INNER JOIN Tag
                          ON TagMap.TagId = Tag.TagId
                  WHERE
                          ( Tag.Tag = @Tag ) AND [TagMap].[ObjectId] = @Identifier
               ELSE
                  IF @user IS NOT NULL
                     DELETE  FROM
                             TagMap
                     FROM
                             TagMap INNER JOIN Tag
                             ON TagMap.TagId = Tag.TagId
                     WHERE
                             ( Tag.Tag = @Tag ) AND [TagMap].[ObjectId] = @Identifier AND
                             TagMap.TaggedBy = @user
                  ELSE
                     DELETE  FROM
                             TagMap
                     FROM
                             TagMap
                     WHERE
                             [TagMap].[ObjectId] = @Identifier

         END
      ELSE
         IF @tag IS NOT NULL
            DELETE  FROM
                    TagMap
            FROM
                    TagMap INNER JOIN Tag
                    ON TagMap.TagId = Tag.TagId
            WHERE
                    ( Tag.Tag = @Tag )
         ELSE
            DELETE  FROM
                    TagMap
            WHERE
                    ( TaggedBy = @user )


END
GO
/****** Object:  StoredProcedure [dbo].[TagAddNew]    Script Date: 04/05/2012 12:13:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagAddNew]
       @Tag varchar(70) ,
       @Identifier uniqueidentifier ,
       @User varchar(100)
AS
BEGIN
-- EXEC TagGetTopTags @TagCount = 2000
-- SELECT * FROM Tag JOIN TagMap ON Tag.TagId = TagMap.TagId WHERE TaggedBy <> 'MisesBot'
--delete FROM tagmap WHERE TaggedBy  = 'MisesBot'
--delete FROM [Tag] WHERE tagId NOT IN (SELECT tagid FROM tagmap)

        --IF @User = 'SearchEngineBot' 
        --    RETURN

      IF LEN(@Tag) < 2
         RETURN

      SET @Tag = LTRIM(RTRIM(@Tag))
      SET @Tag = REPLACE(@Tag , 'Econmics' , 'Economics')

      IF @Tag = 'mises'
         SET @Tag = 'Ludwig von Mises'

      IF @Tag = 'Rothbard' OR @Tag = 'Murray N.  Rothbard' OR @Tag = 'Murray Rothbard'
         SET @Tag = 'Murray N. Rothbard'



      IF @TAG LIKE '%http://%' OR @TAG LIKE '%@%' OR @Tag LIKE '%PDF%' OR @Tag LIKE
      '%Acrobat %' OR @Tag LIKE '%Palm Download%' OR @Tag = 'audio' OR @Tag LIKE
      '%Distiller%' OR @Tag = 'mises.org' OR @Tag = 'and th' OR @Tag = 'Speech' OR @Tag =
      'Unknown' OR @Tag LIKE '%mises institute%' OR @Tag = 'PrecisionScan' OR @Tag LIKE
      '%.qxd%' OR @Tag LIKE '%phendimetrazine%' OR @Tag LIKE '%www%' OR @Tag LIKE '%jews%' OR @Tag LIKE '%kidney%' OR @Tag LIKE '%.com%'
         RETURN

-- Check for cursewords
      IF EXISTS ( SELECT
                      *
                  FROM
                      MisesCommunity..cs_Censorship
                  WHERE
                      @Tag LIKE '%' + word + '%' )
         RETURN

-- Get Tag Id
      DECLARE @TagId int

      SET @TagId = ( SELECT
                         TagId
                     FROM
                         Tag
                     WHERE
                         Tag = @Tag )

-- Check for duplicates
      IF EXISTS ( SELECT
                      *
                  FROM
                      TagMap
                  WHERE
                      TagId = @TagId AND ObjectId = @Identifier AND TaggedBy = @User )
         RETURN


-- Add New Tag if Missing
      IF @TagId IS NULL
         BEGIN
               INSERT INTO
                   Tag
                   (
                     Tag )
               VALUES
                   (
                     @Tag )

               SET @TagID = SCOPE_IDENTITY()
         END

-- Insert tag mapping
      INSERT INTO
          TagMap
          (
            TagId ,
            ObjectId ,
            TaggedBy )
      VALUES
          (
            @TagID ,
            @Identifier ,
            @User )

END
GO
/****** Object:  StoredProcedure [dbo].[TagGetTopTags]    Script Date: 04/05/2012 12:13:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetTopTags] @TagCount int = 60
AS
BEGIN

-- SELECT * FROM tag JOIN [TagMap] ON [Tag].[TagId] = [TagMap].[TagId] WHERE [TaggedBy] <> 'MisesBot'

      SET ROWCOUNT @TagCount

      SELECT TOP 1000
          [Tag].TagId ,
          tag ,
          COUNT(TagMap.TagId) AS Count
      FROM
          [Tag] WITH ( NOLOCK ) JOIN [TagMap] WITH ( NOLOCK )
      ON  [Tag].[TagId] = [TagMap].[TagId]
      GROUP BY
          [Tag].[TagId] ,
          Tag.[Tag]
      ORDER BY
          COUNT(TagMap.TagId) DESC

END
GO
/****** Object:  StoredProcedure [dbo].[TagGetStats]    Script Date: 04/05/2012 12:13:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetStats]
AS
BEGIN

      SELECT
          ( SELECT
                COUNT(1)
            FROM
                tag ) AS UniqueTags ,
          ( SELECT
                COUNT(1)
            FROM
                TagMap
            WHERE
                TaggedBy = 'MisesBot' ) AS BotTags ,
          ( SELECT
                COUNT(DISTINCT ObjectId)
            FROM
                TagMap ) AS TotalTaggedDocs ,
          ( SELECT
                COUNT(DISTINCT ObjectId)
            FROM
                TagMap
            WHERE
                TaggedBy = 'MisesBot' ) AS BotTaggedDocs ,
          ( SELECT
                COUNT(DISTINCT ObjectId)
            FROM
                TagMap
            WHERE
                TaggedBy <> 'MisesBot' ) AS UserTaggedDocs ,
          ( SELECT
                COUNT(1)
            FROM
                TagMap
            WHERE
                TaggedDate > DATEADD(hour , -24 , GETDATE()) ) AS TodaysTagging ,
          ( SELECT
                COUNT(DISTINCT TagId)
            FROM
                TagMap
            WHERE
                TaggedDate > DATEADD(hour , -24 , GETDATE()) ) AS TodaysTags

      SELECT TOP 10
          TaggedBy ,
          COUNT(1) AS Count
      FROM
          TagMap
      WHERE
          TaggedDate > DATEADD(hour , -24 , GETDATE())-- AND TaggedBy NOT LIKE '%.%'
      GROUP BY
          TaggedBy
      ORDER BY
          COUNT DESC

      SELECT TOP 10
          TaggedBy ,
          COUNT(1) AS Count
      FROM
          TagMap 
--WHERE TaggedBy NOT LIKE '%.%'
      GROUP BY
          TaggedBy
      ORDER BY
          COUNT DESC


      SELECT TOP 25
          Tag ,
          COUNT(1) AS 'Count'
      FROM
          TagMap JOIN Tag
      ON  Tag.TagId = TagMap.TagId
      WHERE
          TaggedDate > DATEADD(hour , -24 , GETDATE())
      GROUP BY
          Tag
      ORDER BY
          COUNT DESC

END
GO
/****** Object:  StoredProcedure [dbo].[TagGetRelatedTags]    Script Date: 04/05/2012 12:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	 Get Related Tags
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetRelatedTags] @Tag varchar(70)
AS
BEGIN
      DECLARE @TagId int
      SET @TagId = ( SELECT TOP 1
                         TagId
                     FROM
                         Tag
                     WHERE
                         Tag = @Tag )

      SELECT TOP 30
          Tag.Tag ,
          COUNT(TagMap.TagId) AS 'Count'
      FROM
          TagMap INNER JOIN Tag
      ON  TagMap.TagId = Tag.TagId
      WHERE
          [TagMap].TagId <> @TagId AND ( TagMap.TagId IN ( SELECT DISTINCT
                                                               TagId
                                                           FROM
                                                               TagMap
                                                           WHERE
                                                               ( ObjectId IN ( SELECT
                                                                                   ObjectId
                                                                               FROM
                                                                                   TagMap
                                                                               WHERE
                                                                                   (
                                                                                   TagId
                                                                                   =
                                                                                   @TagId ) ) ) ) )
      GROUP BY
          TagMap.TagId ,
          Tag.Tag
      ORDER BY
          'Count' DESC
END
GO
/****** Object:  StoredProcedure [dbo].[TagGetDocumentTags]    Script Date: 04/05/2012 12:13:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetDocumentTags] @Identifier uniqueidentifier
AS
BEGIN

      SELECT
          Tag.Tag ,
          COUNT(Tag.TagId) AS 'Count' ,
          Tag.TagId
      FROM
          Tag WITH ( NOLOCK ) INNER JOIN TagMap WITH ( NOLOCK )
      ON  Tag.TagId = TagMap.TagId
      WHERE
          ( TagMap.ObjectId = @Identifier ) AND [TaggedBy] <> 'SearchEngineBot'
      GROUP BY
          Tag.TagId ,
          Tag.Tag

END
GO
/****** Object:  StoredProcedure [dbo].[TagGetDocumentsWithTag]    Script Date: 04/05/2012 12:13:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--TagGetDocumentsWithTags 'Bastiat'
--SELECT * FROM [SplitCVS] ('Bastiat')

-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetDocumentsWithTag] @Tag varchar(75)
AS
BEGIN
      SELECT TOP 200
          Documents.GUID ,
          Documents.[Title] ,
          CASE
               WHEN( SELECT
                         COUNT(1)
                     FROM
                         MediaAlternateFormat mf
                     WHERE
                         mf.DocumentId = Documents.DocumentId AND [MediaTypeId] IN ( 1 ,
                         2 ) ) > 0 THEN 'Media'
               WHEN( SELECT
                         COUNT(1)
                     FROM
                         MediaAlternateFormat mf WITH ( NOLOCK )
                     WHERE
                         mf.DocumentId = Documents.DocumentId AND [MediaTypeId] = 3 ) > 0
               THEN 'PDF'
               ELSE 'Content'
          END AS 'Type' ,
          [Documents].[DocumentId] AS Id ,
          '' AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN Documents WITH ( NOLOCK )
      ON  Documents.[GUID] = [TagMap].[ObjectId]
      WHERE
          Tag.Tag = @Tag AND [Documents].[Display] = 1
      UNION ALL
      SELECT TOP 200
          Page.[GUID] ,
          Page.[pageTitle] AS Title ,
          'Content' AS 'Type' ,
          [Page].[PageId] AS Id ,
          page.[folder] + '/' + page.[pageName] AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN Tag WITH ( NOLOCK )
      ON  TagMap.TagId = Tag.TagId INNER JOIN [Page] WITH ( NOLOCK )
      ON  [Page].[GUID] = [TagMap].[ObjectId]
      WHERE
          Tag.Tag = @Tag
      UNION ALL
      SELECT DISTINCT TOP 200
          DailyArticles.GUID ,
          DailyArticles.[Title] ,
          'DailyArticle' AS 'Type' ,
          [DailyArticles].[ArticleId] AS Id ,
          '' AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN DailyArticles WITH ( NOLOCK )
      ON  DailyArticles.[GUID] = [TagMap].[ObjectId]
      WHERE
          Tag.Tag = @Tag
      UNION ALL
      SELECT DISTINCT TOP 200
          [GUID] ,
          title ,
          PeriodicalsView.[Journal] AS 'Type' ,
          0 AS Id ,
          [PeriodicalsView].[URL] AS 'URL'
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN PeriodicalsView WITH ( NOLOCK )
      ON  PeriodicalsView.GUID = TagMap.ObjectId
      WHERE
          Tag.Tag = @Tag
      -- OR [Tag].[Tag] LIKE '%' + @Tags  + '%' 

      UNION ALL
      SELECT
          [GUID] ,
          NAME ,
          'Product' AS 'Type' ,
          products.ProductId AS Id ,
          '' AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN AbleCommerce..[ac_Products]
      products WITH ( NOLOCK )
      ON  TagMap.ObjectId = products.GUID
      WHERE
          Tag.Tag = @Tag


END
GO
/****** Object:  StoredProcedure [dbo].[TagGetRelatedDocuments]    Script Date: 04/05/2012 12:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--TagGetDocumentsWithTags 'Bastiat'
--SELECT * FROM [SplitCVS] ('Bastiat')

-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetRelatedDocuments]
       @DocumentId uniqueidentifier = NULL ,
       @ArticleId int = 0
AS
BEGIN


      IF @DocumentId IS NULL
         SET @DocumentId = ( SELECT
                                 [GUID]
                             FROM
                                 [DailyArticles]
                             WHERE
                                 [DailyArticles].[ArticleId] = @ArticleId )

      --CREATE  TABLE #Tags(TagId int)
      --INSERT INTO [#Tags] ([TagId])  (SELECT TagId FROM [TagMap] WHERE [TagMap].[ObjectId] = @DocumentId)	
      SELECT TOP 5
          Documents.GUID ,
          Documents.[Title] ,
          CASE
               WHEN( SELECT
                         COUNT(1)
                     FROM
                         MediaAlternateFormat mf
                     WHERE
                         mf.DocumentId = Documents.DocumentId AND [MediaTypeId] IN ( 1 ,
                         2 ) ) > 0 THEN 'Media'
               WHEN( SELECT
                         COUNT(1)
                     FROM
                         MediaAlternateFormat mf
                     WHERE
                         mf.DocumentId = Documents.DocumentId AND [MediaTypeId] = 3 ) > 0
               THEN 'PDF'
               ELSE 'Content'
          END AS 'Type' ,
          [Documents].[DocumentId] AS Id ,
          '' AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN Documents WITH ( NOLOCK )
      ON  Documents.[GUID] = [TagMap].[ObjectId]
      WHERE
          [Documents].[Display] = 1 AND [Tag].[TagId] IN ( SELECT
                                                               TagId
                                                           FROM
                                                               [TagMap]
                                                           WHERE
                                                               [TagMap].[ObjectId] =
                                                               @DocumentId )
      UNION ALL
      SELECT TOP 4
          Page.[GUID] ,
          Page.[pageTitle] AS Title ,
          'Content' AS 'Type' ,
          [Page].[PageId] AS Id ,
          '/' + page.[folder] + page.[pageName] AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN Tag WITH ( NOLOCK )
      ON  TagMap.TagId = Tag.TagId INNER JOIN [Page]
      ON  [Page].[GUID] = [TagMap].[ObjectId]
      WHERE
          ( Tag.TagId IN ( SELECT
                               TagId
                           FROM
                               [TagMap]
                           WHERE
                               [TagMap].[ObjectId] = @DocumentId ) )
      UNION ALL
      SELECT TOP 4
          DailyArticles.GUID ,
          DailyArticles.[Title] ,
          'DailyArticle' AS TYPE ,
          [DailyArticles].[ArticleId] AS Id ,
          '' AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN DailyArticles
      ON  DailyArticles.[GUID] = [TagMap].[ObjectId]
      WHERE
          TagMap.Objectid <> @DocumentId AND ( Tag.TagId IN ( SELECT
                                                                  TagId
                                                              FROM
                                                                  [TagMap]
                                                              WHERE
                                                                  [TagMap].[ObjectId] =
                                                                  @DocumentId ) )
      UNION ALL
      SELECT TOP 4
          [GUID] ,
          title ,
          PeriodicalsView.[Journal] AS 'Type' ,
          0 AS Id ,
          [PeriodicalsView].[URL] AS 'URL'
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN PeriodicalsView
      ON  PeriodicalsView.GUID = TagMap.ObjectId
      WHERE
          ( Tag.TagId IN ( SELECT
                               TagId
                           FROM
                               [TagMap]
                           WHERE
                               [TagMap].[ObjectId] = @DocumentId ) )
      UNION ALL
      SELECT TOP 4
          [GUID] ,
          NAME ,
          'Product' AS 'Type' ,
          products.ProductId AS Id ,
          '' AS URL
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN AbleCommerce..ac_Products products
      ON  TagMap.ObjectId = products.GUID
      WHERE
          ( Tag.TagId IN ( SELECT
                               TagId
                           FROM
                               [TagMap]
                           WHERE
                               [TagMap].[ObjectId] = @DocumentId ) )
      ORDER BY
          Id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spUpdateArticle]    Script Date: 04/05/2012 12:13:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spUpdateArticle]
(
 @ArticleId int ,
 @Title varchar(255) ,
 @Description varchar(1500) ,
 @ArticleText varchar(max) ,
 @ShowArticle bit ,
 @AuthorId int = 0 ,
 @CoAuthorId int = 0 ,
 @DisplayOrder int = 0 ,
 @PhotoHeight int = 0 ,
 @PhotoURL varchar(255) = NULL ,
 @DatePosted smalldatetime = GETDATE ,
 @EditBy varchar(75) = NULL )
AS
UPDATE
    DailyArticles
SET
    Title = @Title ,
    [Description] = @Description ,
    ArticleText = @ArticleText ,
    DatePosted = @Dateposted ,
    ShowArticle = @ShowArticle ,
    CoAuthorId = @CoAuthorId ,
    AuthorId = @AuthorId ,
    DisplayOrder = @DisplayOrder ,
    PhotoURL = @PhotoURL ,
    PhotoHeight = @PhotoHeight ,
    EditDate = GETDATE() ,
    EditBy = @EditBy
WHERE
    ( ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[TagGetUserTaggedDocuments]    Script Date: 04/05/2012 12:13:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[TagGetUserTaggedDocuments]
       @User varchar(100) ,
       @User2 varchar(100) = ''
AS
BEGIN

      -- Replace IP address with singed in user Id
      --IF @User2 <> ''
      --UPDATE TagMap SET TaggedBy = @User WHERE TaggedBy =@User2


      -- User Tagged Documents
      SELECT TOP 1000
          Documents.GUID ,
          Documents.[Title] ,
          CASE
               WHEN( SELECT
                         COUNT(1)
                     FROM
                         MediaAlternateFormat mf
                     WHERE
                         mf.DocumentId = Documents.DocumentId AND [MediaTypeId] IN ( 1 ,
                         2 ) ) > 0 THEN 'Media'
               WHEN( SELECT
                         COUNT(1)
                     FROM
                         MediaAlternateFormat mf
                     WHERE
                         mf.DocumentId = Documents.DocumentId AND [MediaTypeId] = 3 ) > 0
               THEN 'PDF'
               ELSE 'Content'
          END AS 'Type' ,
          [Documents].[DocumentId] AS Id ,
          '' AS URL ,
          Tag.[Tag]
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag] WITH ( NOLOCK )
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN Documents WITH ( NOLOCK )
      ON  Documents.[GUID] = [TagMap].[ObjectId]
      WHERE
          [Documents].[Display] = 1 AND [TagMap].TaggedBy = @User
      UNION ALL
      SELECT TOP 1000
          Page.[GUID] ,
          Page.[pageTitle] AS Title ,
          'Content' AS 'Type' ,
          [Page].[PageId] AS Id ,
          '/' + page.[folder] + page.[pageName] AS URL ,
          Tag.[Tag]
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN Tag
      ON  TagMap.TagId = Tag.TagId INNER JOIN [Page]
      ON  [Page].[GUID] = [TagMap].[ObjectId]
      WHERE
          [TagMap].TaggedBy = @User
      UNION ALL
      SELECT TOP 1000
          DailyArticles.GUID ,
          DailyArticles.[Title] ,
          'DailyArticle' AS TYPE ,
          [DailyArticles].[ArticleId] AS Id ,
          '' AS URL ,
          Tag.[Tag]
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag]
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN DailyArticles
      ON  DailyArticles.[GUID] = [TagMap].[ObjectId]
      WHERE
          [TagMap].TaggedBy = @User
      UNION ALL
      SELECT
          [GUID] ,
          NAME ,
          'Product' AS 'Type' ,
          products.ProductId AS Id ,
          '' AS URL ,
          Tag.[Tag]
      FROM
          TagMap WITH ( NOLOCK ) INNER JOIN [Tag]
      ON  [TagMap].[TagId] = [Tag].[TagId] INNER JOIN AbleCommerce..ac_Products products
      ON  TagMap.ObjectId = products.GUID
      WHERE
          [TagMap].TaggedBy = @User
      ORDER BY
          Id DESC
        
		-- User Tag Map

      --SELECT
      --    Tag.Tag ,
      --    COUNT(Tag.TagId) AS 'Count' ,
      --    Tag.TagId
      --FROM
      --    Tag INNER JOIN TagMap
      --ON  Tag.TagId = TagMap.TagId
      --WHERE
      --    ( TagMap.TaggedBy = @User ) OR ( TagMap.TaggedBy = @User2 )
      --GROUP BY
      --    Tag.TagId ,
      --    Tag.Tag

END
GO
/****** Object:  StoredProcedure [dbo].[SubjectsTopSubjects]    Script Date: 04/05/2012 12:13:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[SubjectsTopSubjects]
AS
BEGIN
      SELECT
          DocumentSubjectLink.SubjectID ,
          ShortSubject ,
          COUNT(1) AS 'Count'
      FROM
          DocumentSubjectLink JOIN DocumentSubjects
      ON  DocumentSubjectLink.SubjectID = DocumentSubjects.SubjectId
      WHERE
          DocumentSubjects.Visible = 1
      GROUP BY
          DocumentSubjectLink.SubjectID ,
          ShortSubject
      ORDER BY
          'Count' DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spGetAuthorList]    Script Date: 04/05/2012 12:12:59 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[spGetAuthorList]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT DISTINCT
    DailyArticles.AuthorId ,
    da.authorLast ,
    0 AS monthnum ,
    '' AS month ,
    da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS AuthorName ,
    ( SELECT TOP 1
          title
      FROM
          DailyArticles a
      WHERE
          a.AuthorId = DailyArticles.AuthorId
      ORDER BY
          dateposted DESC ) AS title ,
    ( SELECT TOP 1
          ArticleId
      FROM
          DailyArticles a
      WHERE
          a.AuthorId = DailyArticles.AuthorId
      ORDER BY
          dateposted DESC ) AS ArticleId ,
    ( SELECT TOP 1
          dateposted
      FROM
          DailyArticles a
      WHERE
          a.AuthorId = DailyArticles.AuthorId
      ORDER BY
          dateposted DESC ) AS dateposted
FROM
    DailyArticles JOIN DocumentAuthors da
ON  da.AuthorId = DailyArticles.AuthorId
WHERE
    ( ShowArticle = 1 )
ORDER BY
    da.AuthorLast
GO
/****** Object:  StoredProcedure [dbo].[spAddNewDocumentSubjectLink]    Script Date: 04/05/2012 12:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spAddNewDocumentSubjectLink]
       @DocumentId int ,
       @SubjectId int
AS
INSERT INTO
    DocumentSubjectLink
    (
      SubjectId ,
      DocumentId )
VALUES
    (
      @SubjectId ,
      @DocumentId )
GO
/****** Object:  StoredProcedure [dbo].[spAddNewArticle]    Script Date: 04/05/2012 12:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spAddNewArticle]
(
 @Title varchar(255) ,
 @Description text ,
--@AuthorFirst varchar(50)='',
--@AuthorLast varchar(50)='',
 @ArticleText text ,
 @ShowArticle bit ,
 @AuthorId int = 0 ,
 @CoAuthorId int = 0 ,
 @DisplayOrder int = 10 ,
 @PhotoHeight int = 0 ,
 @PhotoURL varchar(255) = NULL ,
 @EditBy varchar(75) = NULL ,
 @DatePosted smalldatetime = GETDATE )
AS
INSERT INTO
    DailyArticles
    (
      Title ,
      Description ,
      ArticleText ,
      ShowArticle ,
      DisplayOrder ,
      AuthorId ,
      CoAuthorId ,
      PhotoURL ,
      DatePosted ,
      PhotoHeight ,
      EditBy )
VALUES
    (
      @title ,
      @description ,
      @ArticleText ,
      @ShowArticle ,
      @DisplayOrder ,
      @AuthorId ,
      @CoAuthorId ,
      @PhotoURL ,
      @DatePosted ,
      @PhotoHeight ,
      @EditBy )
SELECT
    SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[SearchSite]    Script Date: 04/05/2012 12:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 1/30/2006
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[SearchSite] 
	-- Add the parameters for the stored procedure here
       @terms varchar(100)
AS
BEGIN

      SET CONCAT_NULL_YIELDS_NULL OFF

      SELECT
          'Daily Articles' 'SearchType' ,
          '/daily/{0}' ,
          ArticleId [IDfield] ,
          Title + ' by ' + da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS [Title]
      FROM
          dailyarticles WITH ( NOLOCK ) JOIN DocumentAuthors da
      ON  da.AuthorId = dailyarticles.AuthorId
      WHERE
          ShowArticle = 1 AND ( ( Title LIKE '%' + @terms + '%' ) OR ( Description LIKE
                                                                       '%' + @terms + '%' )
                                OR FREETEXT ( ArticleText,@terms )
--                      OR ( ArticleText LIKE '%' + @terms + '%' )
                              )
      ORDER BY
          title

      SELECT
          'Literature Document' 'SearchType' ,
          '/resources/{0}' ,
          DocumentId [IDfield] ,
          Title [Title]
      FROM
          Documents WITH ( NOLOCK )
      WHERE
          display = 1 AND ( ( title LIKE '%' + @terms + '%' ) OR ( PublicationInformation LIKE '%' +
                                                                   @terms + '%' ) 
                                                                   )
      ORDER BY
          title

      SELECT
          'Mises Store Products' 'SearchType' ,
          '/store/Product.aspx?ProductId={0}' ,
          ProductId [IDfield] ,
          [Name] [Title]
      FROM
          [AbleCommerce]..ac_Products WITH ( NOLOCK )
      WHERE
          ( ( name LIKE '%' + @terms + '%' ) OR ( description LIKE '%' + @terms + '%' ) )
      ORDER BY
          [name]


      SELECT
          'Mises Institute Events' 'SearchType' ,
          '/events/{0}' ,
          EventId [IDfield] ,
          Title [Title]
      FROM
          calendar WITH ( NOLOCK )
      WHERE
          display = 1 AND ( ( Description LIKE '%' + @terms + '%' ) OR ( Title LIKE '%' +
                                                                         @terms + '%' )
                            OR ( Location LIKE '%' + @terms + '%' ) )
      ORDER BY
          Title

      SELECT
          'Free Market articles' 'SearchType' ,
          '/freemarket_detail.aspx?control={0}' ,
          control [IDfield] ,
          title [Title]
      FROM
          freemarket WITH ( NOLOCK )
      WHERE
          ( ( title LIKE '%' + @terms + '%' ) OR ( authorfirst LIKE '%' + @terms + '%' )
          OR ( authorlast LIKE '%' + @terms + '%' ) OR ( authorfirst2 LIKE '%' + @terms +
                                                         '%' ) OR ( authorlast2 LIKE '%'
                                                                    + @terms + '%' ) OR (
                                                                                          subject1 LIKE '%' + @terms + '%' ) OR ( subject2 LIKE '%' + @terms + '%' ) OR ( subject3 LIKE '%' + @terms + '%' ) OR ( body LIKE '%' + @terms + '%' ) OR ( articledate LIKE '%' + @terms + '%' ) )
      ORDER BY
          title

      SELECT
          'Mises Review articles' 'SearchType' ,
          '/misesreview_detail.aspx?control={0}' ,
          control [IDfield] ,
          title [Title]
      FROM
          misesreview WITH ( NOLOCK )
      WHERE
          ( ( title LIKE '%' + @terms + '%' ) OR ( authorfirst LIKE '%' + @terms + '%' )
          OR ( authorlast LIKE '%' + @terms + '%' ) OR ( authorfirst2 LIKE '%' + @terms +
                                                         '%' ) OR ( authorlast2 LIKE '%'
                                                                    + @terms + '%' ) OR (
                                                                                          authorfirst3 LIKE '%' + @terms + '%' ) OR ( authorlast3 LIKE '%' + @terms + '%' ) OR ( body LIKE '%' + @terms + '%' ) )
      ORDER BY
          title


      SELECT
          'Journal of Libertarian Studies articles' 'SearchType' ,
          '{0}' ,
          jls.[link] [IDfield] ,
          title [Title]
      FROM
          jls WITH ( NOLOCK )
      WHERE
          ( title LIKE '%' + @terms + '%' ) OR ( authorFirst1 LIKE '%' + @terms + '%' )
          OR ( authorLast1 LIKE '%' + @terms + '%' ) OR ( authorFirst2 LIKE '%' + @terms
                                                          + '%' ) OR ( authorLast2 LIKE
                                                                       '%' + @terms + '%' )
      ORDER BY
          title

      SELECT
          'Quarterly Journal of Austrian Economics articles' 'SearchType' ,
          '/journals/qjae/qjae' + CAST(volume AS varchar(max)) + '_' + CAST(number AS
          varchar(max)) + '_' + CAST(ArticleNum AS varchar(max)) + '.pdf' ,
          '' [IDfield] ,
          title [Title]
      FROM
          qjaeDB WITH ( NOLOCK )
      WHERE
          ( title LIKE '%' + @terms + '%' ) OR ( authorFirst1 LIKE '%' + @terms + '%' )
          OR ( authorLast1 LIKE '%' + @terms + '%' ) OR ( authorFirst2 LIKE '%' + @terms
                                                          + '%' ) OR ( authorLast2 LIKE
                                                                       '%' + @terms + '%' )
      ORDER BY
          title


      SELECT
          'Austrian Economics Newsletter articles' 'SearchType' ,
          '/journals/aen/aen' + CAST(volume AS varchar(max)) + '_' + CAST(number AS
          varchar(max)) + '_' + CAST(ArticleNum AS varchar(max)) + fileType ,
          '' [IDfield] ,
          title [Title]
      FROM
          aenDB WITH ( NOLOCK )
      WHERE
          ( title LIKE '%' + @terms + '%' ) OR ( authorFirst1 LIKE '%' + @terms + '%' )
          OR ( authorLast1 LIKE '%' + @terms + '%' ) OR ( authorFirst2 LIKE '%' + @terms
                                                          + '%' ) OR ( authorLast2 LIKE
                                                                       '%' + @terms + '%' )
      ORDER BY
          title

      SELECT
          'Review of Austrian Economics articles' 'SearchType' ,
          '/journals/rae/pdf/rae' + CAST(volume AS varchar(max)) + '_' + CAST(number AS
          varchar(max)) + '_' + CAST(ArticleNum AS varchar(max)) + '.pdf' ,
          '' [IDfield] ,
          title [Title]
      FROM
          raeDB WITH ( NOLOCK )
      WHERE
          ( title LIKE '%' + @terms + '%' ) OR ( authorFirst1 LIKE '%' + @terms + '%' )
          OR ( authorLast1 LIKE '%' + @terms + '%' ) OR ( authorFirst2 LIKE '%' + @terms
                                                          + '%' ) OR ( authorLast2 LIKE
                                                                       '%' + @terms + '%' )
      ORDER BY
          title

      SELECT
          'Other Pages' 'SearchType' ,
          '' ,
          '/' + folder + '/' + pagename [IDfield] ,
          pagetitle [Title]
      FROM
          page WITH ( NOLOCK )
      WHERE
          ( pageName LIKE '%' + @terms + '%' ) OR ( pageTitle LIKE '%' + @terms + '%' )
--                OR ( content LIKE '%' + @terms + '%' )
          OR FREETEXT ( [content],@terms ) OR ( metaKeywords LIKE '%' + @terms + '%' ) OR
                                                                                          ( metaDescription LIKE '%' + @terms + '%' )
      ORDER BY
          pageName

      SELECT
          'Ward Library books ' 'SearchType' ,
          '/book.aspx?Id={0}' ,
          control [IDfield] ,
          Title [Title]
      FROM
          wardlibrary WITH ( NOLOCK )
      WHERE
          ( ( Title LIKE '%' + @terms + '%' ) OR ( publisher_info LIKE '%' + @terms + '%' )
          OR ( comments LIKE '%' + @terms + '%' ) )
      ORDER BY
          title

      SELECT
          'Faculty Biographies' 'SearchType' ,
                --'/fellow.aspx?Id={0}',
          web ,
          '' [IDfield] ,
                --control [IDfield],
          [name] [Title]
      FROM
          faculty WITH ( NOLOCK )
      WHERE
          ( display = 1 ) AND ( ( name LIKE '%' + @terms + '%' ) OR ( school LIKE '%' +
                                                                      @terms + '%' ) OR (
                                                                                          email LIKE '%' + @terms + '%' ) OR ( jobtitle LIKE '%' + @terms + '%' ) )
      ORDER BY
          name


--        SELECT  'Misc Pages' 'SearchType',
--                '',
--                'http://www.mises.org' + folder + '/' + pagename [IDfield],
--                pagetitle [Title]
--        FROM    pages WITH (NOLOCK)
--        WHERE   ( pageName LIKE '%' + @terms + '%' )
--                OR ( pageTitle LIKE '%' + @terms + '%' )
--                OR ( content LIKE '%' + @terms + '%' )
--                OR ( metaKeywords LIKE '%' + @terms + '%' )
--                OR ( metaDescription LIKE '%' + @terms + '%' )
--        ORDER BY pageName



END
GO
/****** Object:  StoredProcedure [dbo].[RevisionGetRecent]    Script Date: 04/05/2012 12:12:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[RevisionGetRecent]
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT DISTINCT TOP 30
          ArticleId ,
          Title ,
          dbo.DailyArticles.EditDate ,
          EditBy
      FROM
          dbo.DailyArticles INNER JOIN dbo.Revision
      ON  dbo.DailyArticles.GUID = dbo.Revision.DocumentGUID
      WHERE
          dbo.DailyArticles.ShowArticle = 0
      ORDER BY
          dbo.DailyArticles.EditDate DESC
END
GO
/****** Object:  StoredProcedure [dbo].[ReplaceGuideAuthor]    Script Date: 04/05/2012 12:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[ReplaceGuideAuthor] 
	-- Add the parameters for the stored procedure here
       @OldAuthorId int ,
       @NewAuthorId int
AS
BEGIN

--select * from DocumentAuthors WHERE AuthorLast + AuthorFirst IN 
--(
--SELECT AuthorLast + AuthorFirst
--FROM DocumentAuthors
--GROUP BY AuthorLast + AuthorFirst
--HAVING ( COUNT(AuthorLast + AuthorFirst) > 1 )
--) oRDER By AuthorLast



      UPDATE
          Documents
      SET
          Author1 = @NewAuthorId
      WHERE
          Author1 = @OldAuthorId

      UPDATE
          Documents
      SET
          Author2 = @NewAuthorId
      WHERE
          Author2 = @OldAuthorId

      UPDATE
          DailyArticles
      SET
          AuthorId = @NewAuthorId
      WHERE
          AuthorId = @OldAuthorId

      UPDATE
          DailyArticles
      SET
          CoAuthorId = @NewAuthorId
      WHERE
          CoAuthorId = @OldAuthorId

      DELETE  FROM
              DocumentAuthors
      WHERE
              ( AuthorId = @OldAuthorId ) AND @OldAuthorId NOT IN ( SELECT
                                                                        Author1
                                                                    FROM
                                                                        Documents
                                                                    WHERE
                                                                        Author1 =
                                                                        @OldAuthorId )

END
GO
/****** Object:  StoredProcedure [dbo].[ReplaceDocumentText]    Script Date: 04/05/2012 12:12:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[ReplaceDocumentText]
       @find varchar(500) ,
       @replace varchar(500) ,
       @patfind varchar(500) = ''
AS
BEGIN

      SELECT
          @patfind = '%' + @find + '%'

      UPDATE
          DailyArticles
      SET
          ArticleText = STUFF(ArticleText , PATINDEX(@patfind , ArticleText) , DATALENGTH
          (@find) , @replace)
      WHERE
          ArticleText LIKE @patfind

---- Study Guide
--
--        UPDATE  Documents
--        SET     Description = STUFF(Description,
--                                     PATINDEX(@patfind, Description),
--                                     DATALENGTH(@find), @replace)
--        WHERE   Description LIKE @patfind
--
-- --Pages
--
--        UPDATE  Page
--        SET     [Content] = STUFF([Content], PATINDEX(@patfind, [Content]),
--                                  DATALENGTH(@find), @replace)
--        WHERE   [Content] LIKE @patfind --AND Page.[folder] LIKE '%humanaction%'
--
---- Media URL
--		UPDATE  MediaAlternateFormat
--        SET     [URL] = STUFF([URL], PATINDEX(@patfind, [URL]),
--                                  DATALENGTH(@find), @replace)
--        WHERE   [URL] LIKE @patfind
--        
---- Product Descriptions
--		UPDATE  [AbleCommerce]..[ac_Products]
--        SET     [AbleCommerce]..[ac_Products].[Description] = STUFF([AbleCommerce]..[ac_Products].[Description], PATINDEX(@patfind, [AbleCommerce]..[ac_Products].[Description]),
--                                  DATALENGTH(@find), @replace)
--        WHERE   [AbleCommerce]..[ac_Products].[Description] LIKE @patfind        
--
END
GO
/****** Object:  StoredProcedure [dbo].[RedirectedURLGet]    Script Date: 04/05/2012 12:12:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[RedirectedURLGet] 
@RequestedURL varchar(150)
AS
BEGIN

      DECLARE @URL varchar(150)

--IF LEN(@RequestedURL) < 5 RETURN

      SET @URL = ( SELECT TOP 1
                       [RedirectURL]
                   FROM
                       [RedirectedURL]
                   WHERE
                       ExactMatchOnly = 1 AND [RequestedURL] = @RequestedURL
                   ORDER BY
                       Priority )

      IF @URL IS NULL
         BEGIN
               SET @URL = ( SELECT TOP 1
                                [RedirectURL]
                            FROM
                                [RedirectedURL]
                            WHERE
                                ExactMatchOnly = 0 AND @RequestedURL LIKE '%' +
                                RequestedURL + '%'
                            ORDER BY
                                Priority )
         END
         
         IF @URL IS NULL
         BEGIN
               SET @URL = ( SELECT TOP 1
                                '/document/' + cast(DocumentId as VARCHAR(MAX))                                                               
                            FROM
                                DocumentFiles                                
                            WHERE
                                @RequestedURL LIKE '%' + oldURL 
                                 + '%'
                            )
         END

      SELECT
          @URL

END
GO
/****** Object:  StoredProcedure [dbo].[QuizGetDetails]    Script Date: 04/05/2012 12:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[QuizGetDetails] @QuizId int
AS
BEGIN
      SET NOCOUNT ON ;
      SELECT
          *
      FROM
          dbo.QuizType
      WHERE
          QuizID = @QuizId
      SELECT
          *
      FROM
          dbo.QuizQuestion
      WHERE
          QuizID = @QuizId
      ORDER BY
          SortOrder
      SELECT
          dbo.QuizAnswer.*
      FROM
          dbo.QuizAnswer JOIN dbo.QuizQuestion
      ON  dbo.QuizAnswer.QuestionID = dbo.QuizQuestion.QuestionID
      WHERE
          dbo.QuizQuestion.QuizID = @QuizId
END
GO
/****** Object:  StoredProcedure [dbo].[PageDelete]    Script Date: 04/05/2012 12:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[PageDelete] @GUID uniqueidentifier
AS
BEGIN


      DELETE  FROM
              PAGE
      WHERE
              GUID = @GUID

      DELETE  FROM
              [DailyArticles]
      WHERE
              [GUID] = @GUID





END
GO
/****** Object:  StoredProcedure [dbo].[MediaTypeGetList]    Script Date: 04/05/2012 12:12:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaTypeGetList]
AS
BEGIN

      SET NOCOUNT ON ;

      SELECT
          MediaType ,
          MediaTypeId ,
          MediaIconPath
      FROM
          DocumentMediaType
      WHERE
          MediaTypeId NOT IN ( 9,7,3,4,5,6)
          AND MediaTypeID IN (SELECT DISTINCT MediaTypeId FROM dbo.DocumentFiles)
END
GO
/****** Object:  StoredProcedure [dbo].[MediaTopSubjects]    Script Date: 04/05/2012 12:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaTopSubjects]
AS
BEGIN
      SELECT
          DocumentSubjectLink.SubjectID ,
          ShortSubject ,
          COUNT(1) AS 'Count'
      FROM
          DocumentSubjectLink INNER JOIN DocumentFiles
      ON  DocumentSubjectLink.DocumentId = DocumentFiles.DocumentId JOIN
      DocumentSubjects
      ON  DocumentSubjectLink.SubjectID = DocumentSubjects.SubjectId
      WHERE
          DocumentSubjects.Visible = 1
      GROUP BY
          DocumentSubjectLink.SubjectID ,
          ShortSubject
      ORDER BY
          'Count' DESC
END
GO
/****** Object:  StoredProcedure [dbo].[MediaTopFullSubjects]    Script Date: 04/05/2012 12:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaTopFullSubjects]
AS
BEGIN
      SELECT TOP 15
          DocumentSubjectLink.SubjectID ,
          [Subject] ,
          COUNT(1) AS 'Count'
      FROM
          DocumentSubjectLink INNER JOIN DocumentFiles
      ON  DocumentSubjectLink.DocumentId = DocumentFiles.DocumentId JOIN
      DocumentSubjects
      ON  DocumentSubjectLink.SubjectID = DocumentSubjects.SubjectId
      WHERE
          DocumentSubjects.Visible = 1
      GROUP BY
          DocumentSubjectLink.SubjectID ,
          Subject
      ORDER BY
          'Count' DESC
END
GO
/****** Object:  StoredProcedure [dbo].[MediaTopAuthors]    Script Date: 04/05/2012 12:12:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DocumentGetTopAuthors
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaTopAuthors]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

BEGIN
      SELECT TOP 60
          d.Author1 AS AuthorId ,
          da.AuthorLast ,
          da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
          COUNT(1) AS 'Count'
      FROM
          Documents d JOIN [DocumentAuthors] da
      ON  d.[Author1] = da.[AuthorId] OR d.[Author2] = da.[AuthorId] JOIN
      DocumentFiles
      ON  DocumentFiles.DocumentId = d.DocumentId
      WHERE
          DocumentFiles.MediaTypeId IN ( 1 , 2 , 8 )
      GROUP BY
          d.Author1 ,
          da.AuthorLast ,
          da.AuthorFirst ,
          da.AuthorMiddle
      ORDER BY
          COUNT DESC
END
GO
/****** Object:  StoredProcedure [dbo].[MediaSearch]    Script Date: 04/05/2012 12:12:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaSearch] @SearchClause varchar(100)
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT
    MediaCategory.CategoryId ,
    MediaCategory.Category ,
    MediaCategory.Description ,
    MediaCategory.CategoryImage ,
    ISNULL(( SELECT TOP 1
                 Documents.CreateDate
             FROM
                 dbo.Documents
             WHERE
                 Documents.CategoryId = MediaCategory.CategoryId
             ORDER BY
                 Documents.CreateDate DESC ) , '1/1/2050') AS EditDate
FROM
    dbo.MediaCategory
WHERE
    MediaCategory.CategoryId = -1
ORDER BY
    EditDate DESC

SELECT
    Documents.DocumentId ,
    Documents.GUID ,
    Documents.Display ,
    Documents.Description AS 'Description' ,
    Documents.Title ,
    Documents.Source ,
    Documents.CreateDate ,
    [Documents].[EditDate] ,
    media.URL ,
    media.FileId ,
    DocumentMediaType.IsMedia,
    DocumentMediaType.MediaType ,
    DocumentMediaType.MediaIconPath ,
    mc.AuthorId ,
    mc.AuthorFirst + '  ' + mc.AuthorMiddle + '  ' + mc.AuthorLast AS Author ,
    
    mc2.AuthorId AS CoAuthorId ,
    mc2.AuthorFirst + '  ' + mc2.AuthorMiddle + '  ' + mc2.AuthorLast AS CoAuthor ,
    mc.[AuthorLast] AS LastName ,
    CASE
         WHEN Documents.CoverImageURL IS NULL THEN 0
         WHEN Documents.CoverImageURL IS NOT NULL THEN 1
    END AS HasImage
FROM
    Documents JOIN DocumentFiles media
ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN DocumentAuthors mc
ON  Documents.Author1 = mc.AuthorId
LEFT JOIN DocumentAuthors mc2
ON dbo.Documents.Author2 = mc.AuthorID
WHERE
    DocumentMediaType.IsMedia = 1 AND ( Documents.Title LIKE '%' + @SearchClause + '%'
                                         OR Documents.PublicationInformation LIKE '%' + @SearchClause +
                                         '%' OR Documents.Description LIKE '%' +
                                         @SearchClause + '%' OR Documents.Keywords
                                         LIKE '%' + @SearchClause + '%' OR mc.AuthorFirst
                                         LIKE @SearchClause OR mc.AuthorLast LIKE
                                         @SearchClause )
ORDER BY
    Documents.CreateDate DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetTopTagged]    Script Date: 04/05/2012 12:12:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David V
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaGetTopTagged]
	-- Add the parameters for the stored procedure here
       @p1 int = 0 ,
       @p2 int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF ;

      SELECT TOP 10
          ObjectId ,
          Documents.DocumentId ,
          Documents.Title ,
          Documents.Author1 ,
          Documents.CreateDate ,
          COUNT(1) AS 'Count' ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
      FROM
          dbo.Documents INNER JOIN dbo.TagMap
      ON  dbo.TagMap.ObjectId = dbo.Documents.GUID INNER JOIN dbo.DocumentFiles
      ON  dbo.Documents.DocumentId = dbo.DocumentFiles.DocumentId INNER JOIN dbo.
      DocumentAuthors da
      ON  da.AuthorId = dbo.Documents.Author1
      WHERE
          Documents.Display = 1 AND dbo.DocumentFiles.
          MediaTypeId IN ( 1 , 2 , 8 , 12 , 14 ) AND TaggedDate > ( GETDATE() - 30 )
      GROUP BY
          ObjectId ,
          Documents.DocumentId ,
          Documents.Title ,
          Documents.Author1 ,
          Documents.CreateDate ,
          da.authorFirst ,
          da.authorMiddle ,
          da.authorLast
      ORDER BY
          COUNT DESC


END
GO
/****** Object:  StoredProcedure [dbo].[GlobalGetStatistics]    Script Date: 04/05/2012 12:12:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[GlobalGetStatistics] @ShowPrivateData bit = 0
AS
BEGIN

      SET NOCOUNT ON ;
      SELECT
          ( SELECT
                COUNT(1)
            FROM
                dbo.DailyArticles ) AS DailyArticles ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 1 ) AS Audio ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 2 ) AS Video ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 3 ) AS PDF ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 4 ) AS Documents ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 5 ) AS WebPages ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 6 ) AS OfflineFiles ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 7 ) AS StoreLinks ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 8 ) AS AudioBooks ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 9 ) AS Ebook ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 12 ) AS 'Streaming Video' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg INNER JOIN dbo.MediaAlternateFormat maf
            ON  maf.DocumentId = sg.DocumentId
            WHERE
                maf.MediaTypeId = 14 ) AS 'MPEG4 Video' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Documents sg
            WHERE
                sg.DocumentId NOT IN ( SELECT
                                           DocumentId
                                       FROM
                                           dbo.MediaAlternateFormat ) ) AS HtmlContentPages ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.DocumentAuthors ) AS Authors ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.DocumentAuthors
            WHERE
                LEN(DocumentAuthors.BioText) > 0 ) AS 'Author Bios' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.PeriodicalsView ) AS 'Journal Articles' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Periodicals ) AS 'Journals' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.QuizType ) AS 'Quiz' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Page ) AS 'Pages' ,
          --( SELECT
          --      COUNT(1)
          --  FROM
          --      dbo.Pages ) AS 'Old Pages' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Biography ) AS 'Biography' ,
          ( SELECT
                COUNT(1)
            FROM
                dbo.Quotes ) AS 'Quotes'
      
END
GO
/****** Object:  StoredProcedure [dbo].[GetContentbyGUID]    Script Date: 04/05/2012 12:12:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[GetContentbyGUID] @GUID uniqueidentifier
AS
SET CONCAT_NULL_YIELDS_NULL OFF

BEGIN

      IF EXISTS ( SELECT
                      *
                  FROM
                      page
                  WHERE
                      GUID = @GUID )
         BEGIN
               DECLARE @PageId int
               SET @PageId = ( SELECT
                                   PageId
                               FROM
                                   Page
                               WHERE
                                   GUID = @GUID )

               SELECT
                   'Page' AS DocumentType ,
                   PageId AS Id ,
                   pageTitle AS Title ,
                   [content] AS Contents ,
                   page.[folder] + '/' + page.[pageName] AS URL ,
                   EditDate AS CreateDate ,
                   metaDescription AS Description ,
                   metaKeywords AS Keywords ,
                   -- Get Next/Prev links
                   ( SELECT TOP 1
                         GUID
                     FROM
                         Page
                     WHERE
                         PageId > @PageId AND Visible = 1 ) AS 'NextGUID' ,
                   ( SELECT TOP 1
                         GUID
                     FROM
                         Page
                     WHERE
                         PageId < @PageId AND Visible = 1
                     ORDER BY
                         PageId DESC ) AS 'PreviousGUID'
               FROM
                   Page
               WHERE
                   [GUID] = @GUID
         END
      ELSE
         IF EXISTS ( SELECT
                         *
                     FROM
                         Documents
                     WHERE
                         GUID = @GUID )
            BEGIN

                  DECLARE @DocumentId int
                  SET @DocumentId = ( SELECT
                                          DocumentId
                                      FROM
                                          Documents
                                      WHERE
                                          GUID = @GUID )


                  DECLARE @Keywords varchar(200)
                  SET @Keywords = ''
                  SELECT
                      @Keywords = @Keywords + CASE
                                                   WHEN LEN(@Keywords) > 0 THEN ','
                                                   ELSE ''
                                              END + Subject
                  FROM
                      DocumentSubjects ds JOIN DocumentSubjectLink dsl
                  ON  dsl.SubjectId = ds.SubjectId
                  WHERE
                      dsl.DocumentId = @DocumentId


                  DECLARE @Contents varchar(max)
                  SET @Contents = ( SELECT
                                        Description
                                    FROM
                                        Documents
                                    WHERE
                                        DocumentId = @DocumentId )

                  IF @Contents = '' OR @Contents IS NULL
                     SET @Contents = ( SELECT TOP 1
                                           URL
                                       FROM
                                           MediaAlternateFormat
                                       WHERE
                                           MediaAlternateFormat.DocumentId = @DocumentId )



                  SELECT
                      CASE
                           WHEN LEN(Description) = 0 AND ( SELECT
                                                                COUNT(1)
                                                            FROM
                                                                MediaAlternateFormat
                                                            WHERE
                                                                DocumentId = @DocumentId
                                                                AND [MediaTypeId] IN ( 1 ,
                                                                2 ) ) > 0 THEN 'Media'
                           WHEN LEN(Description) = 0 AND ( ( SELECT
                                                                  COUNT(1)
                                                              FROM
                                                                  MediaAlternateFormat
                                                              WHERE
                                                                  DocumentId =
                                                                  @DocumentId AND
                                                                  [MediaTypeId] = 3 ) > 0 )
                           THEN 'PDF'
                           ELSE 'Content'
                      END AS 'DocumentType' ,
                      DocumentId AS Id ,
                      Title ,
                      Description ,
                      PublicationInformation,
                      @Contents AS Contents ,
                      '' AS URL ,
                      EditDate AS CreateDate ,
                      @Keywords AS Keywords ,
                      -- Get Next/Prev links
                      ( SELECT TOP 1
                            GUID
                        FROM
                            Documents
                        WHERE
                            DocumentId > @DocumentId AND Documents.Display = 1 ) AS 'NextGUID' ,
                      ( SELECT TOP 1
                            GUID
                        FROM
                            Documents
                        WHERE
                            DocumentId < @DocumentId AND Documents.Display = 1
                        ORDER BY
                            DocumentId DESC ) AS 'PreviousGUID'
                  FROM
                      Documents
                  WHERE
                      GUID = @GUID

            END
         ELSE
            IF EXISTS ( SELECT
                            *
                        FROM
                            Calendar
                        WHERE
                            GUID = @GUID )
               SELECT
                   'Calendar' AS DocumentType ,
                   EventId AS Id ,
                   Title ,
                   IntroText AS Description ,
                   Description AS Contents ,
                   '' AS URL ,
                   CreateDate ,
                   EventDate + ' ' + Location AS Keywords
               FROM
                   Calendar
               WHERE
                   ( GUID = @GUID )
            ELSE
               IF EXISTS ( SELECT
                               *
                           FROM
                               DailyArticles
                           WHERE
                               GUID = @GUID )
                  BEGIN

                        DECLARE @DatePosted datetime
                        SET @DatePosted = ( SELECT
                                                DatePosted
                                            FROM
                                                DailyArticles
                                            WHERE
                                                GUID = @GUID )

                        SELECT
                            'DailyArticle' AS DocumentType ,
                            ArticleId AS Id ,
                            Title ,
                            Description ,
                            ArticleText AS Contents ,
                            '' AS URL ,
                            EditDate AS CreateDate ,
                            da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
                            da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast
                            + ',' + da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.
                            AuthorLast AS Keywords ,       

                            -- Get Next/Prev links
                            ( SELECT TOP 1
                                  daNext.GUID
                              FROM
                                  DailyArticles daNext
                              WHERE
                                  daNext.DatePosted > @DatePosted AND daNext.ShowArticle
                                  = 1
                              ORDER BY
                                  daNext.DatePosted ) AS 'NextId' ,
                            ( SELECT TOP 1
                                  daprevious.GUID
                              FROM
                                  DailyArticles daprevious
                              WHERE
                                  daprevious.DatePosted < @DatePosted AND daprevious.
                                  ShowArticle = 1
                              ORDER BY
                                  daprevious.DatePosted DESC ) AS 'PreviousId'
                        FROM
                            DailyArticles LEFT OUTER JOIN dbo.DocumentAuthors da
                        ON  da.AuthorId = DailyArticles.AuthorId OR da.AuthorId =
                            DailyArticles.CoAuthorId
                        WHERE
                            GUID = @GUID
                  END
               ELSE
                  IF EXISTS ( SELECT
                                  *
                              FROM
                                  RegistrationForms
                              WHERE
                                  GUID = @GUID )
                     SELECT
                         'Form' AS DocumentType ,
                         FormId AS Id ,
                         FormTitle AS Title ,
                         FormTitle AS Description ,
                         Introduction AS Contents ,
                         '' AS URL ,
                         CreateDate ,
                         '' AS Keywords
                     FROM
                         RegistrationForms
                     WHERE
                         GUID = @GUID
                  --ELSE
                  --   IF EXISTS ( SELECT
                  --                   *
                  --               FROM
                  --                   pages
                  --               WHERE
                  --                   GUID = @GUID )
                  --      SELECT
                  --          'Page' AS DocumentType ,
                  --          control AS Id ,
                  --          pageTitle AS Title ,
                  --          [content] AS Contents ,
                  --          '' AS URL ,
                  --          CreateDate ,
                  --          metaDescription AS Description ,
                  --          metaKeywords AS Keywords
                  --      FROM
                  --          Pages
                  --      WHERE
                  --          [GUID] = @GUID

      IF EXISTS ( SELECT
                      *
                  FROM
                      AbleCommerce..ac_Products
                  WHERE
                      GUID = @GUID )
         BEGIN

               DECLARE @ProductId int
               SET @ProductId = ( SELECT
                                      ProductId
                                  FROM
                                      AbleCommerce..ac_Products
                                  WHERE
                                      GUID = @GUID )
               SELECT
                   'Product' AS DocumentType ,
                   ProductId AS Id ,
                                      -- todo  AbleCommerce..ac_Manufacturers.Name + ' : ' +
                   NAME AS Title ,
                   '<img src="/store/' + ThumbnailUrl + '" alt="' + NAME +
                   '" align="left" />' +
                   '<span class="CommonMessageTitle"><a href="/store/Product1.aspx?ProductId='
                   + CAST(ProductId AS varchar(10)) +
                   '">
Buy Now!</a>
</span><br /><br />' + Description AS Contents ,
                   '' AS URL ,
                   CreatedDate AS 'CreateDate' ,
                   [NAME] + ' ' + Summary AS Description ,
                                       -- AbleCommerce..ac_Manufacturers.Name --+ ',' + (SELECT  TOP 1 FieldValue FROM MisesShop..CUSTOMFIELDS WHERE TableName = 'PRODUCTS' AND ForeignKey_ID = ProductId
                   '' AS Keywords ,
                   -- Get Next/Prev links
                   ( SELECT TOP 1
                         GUID
                     FROM
                         AbleCommerce..ac_Products JOIN AbleCommerce..ac_Manufacturers
                     ON  AbleCommerce..ac_Manufacturers.ManufacturerId = AbleCommerce..
                         ac_Products.ManufacturerId
                     WHERE
                         ProductId > @ProductId AND AbleCommerce..ac_Products.
                         DisablePurchase = 0 ) AS 'NextGUID' ,
                   ( SELECT TOP 1
                         GUID
                     FROM
                         AbleCommerce..ac_Products
                     WHERE
                         ProductId < @ProductId AND AbleCommerce..ac_Products.
                         DisablePurchase = 0
                     ORDER BY
                         ProductId DESC ) AS 'PreviousGUID'
               FROM
                   AbleCommerce..ac_Products
               WHERE
                   [GUID] = @GUID
         END

END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetRandomVideo]    Script Date: 04/05/2012 12:12:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	

exec dbo.MediaGetRandomVideo
**********************************************************************************************************/
CREATE  PROCEDURE  [dbo].[MediaGetRandomVideo]
as

select		top (1)
			m.[FileId],
			m.[DocumentId],
			m.[MediaTypeId],
			m.[URL],
			m.[fileSize],
			m.[duration],
			m.[CreateDate],
			m.[Display],
			NULL as oldURL,
			m.VolumeOrdinal,
			m.VolumeComment
from		dbo.Documents d
			inner join	dbo.DocumentFiles m
				on d.DocumentId = m.DocumentId
where		m.Display = 1
			and m.MediaTypeId = 16 --TODO or in (2, 14, 16) plus any new ones? Maybe need a media type type?!?
order by	newid()
GO
/****** Object:  StoredProcedure [dbo].[MediaGetMisesLive]    Script Date: 04/05/2012 12:12:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetMisesLive]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT
    sg.DocumentId ,
    sg.Title ,
    sg.Description AS Description ,
    media.URL ,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS Author ,
    sg.Author1 AS AuthorId ,
    sg.CreateDate ,
    Duration ,
    'TODO' AS Topics ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 0
         WHEN sg.CoverImageUrl IS NOT NULL THEN 1
    END AS HasImage ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 'none'
         WHEN sg.CoverImageUrl IS NOT NULL THEN 'inline'
    END AS Display
FROM
    dbo.Documents sg JOIN dbo.DocumentFiles media
ON  media.DocumentId = sg.DocumentId JOIN dbo.DocumentAuthors
ON  sg.Author1 = DocumentAuthors.AuthorId
WHERE
    sg.Source = 'MisesLive'
ORDER BY
    media.CreateDate DESC ,
    media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetMediaByType]    Script Date: 04/05/2012 12:12:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}
CREATE  PROCEDURE [dbo].[MediaGetMediaByType]
       @MediaTypeID int = 0 ,
       @MaxResults int = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF 

IF @MaxResults > 0
SET ROWCOUNT @MaxResults;

      SELECT
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.Description AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.FileId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.CoverImageURL IS NULL THEN 0
               WHEN Documents.CoverImageURL IS NOT NULL THEN 1
          END AS HasImage,
          media.VolumeOrdinal,
          media.VolumeComment
          
      FROM
          dbo.Documents LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId
      WHERE
          DocumentMediaType.MediaTypeId = @MediaTypeID          
      ORDER BY
          Documents.Title
GO
/****** Object:  StoredProcedure [dbo].[MediaGetLatestVideo]    Script Date: 04/05/2012 12:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetLatestVideo]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT TOP 6
    sg.DocumentId ,
    dbo.MediaCategory.Category + ' >> ' + sg.Title AS Title ,
    sg.PublicationInformation AS Description ,
    media.URL ,
    media.FileId ,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS Author ,
    sg.Author1 AS AuthorId ,
    sg.CreateDate ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 0
         WHEN sg.CoverImageUrl IS NOT NULL THEN 1
    END AS HasImage ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 'none'
         WHEN sg.CoverImageUrl IS NOT NULL THEN 'inline'
    END AS Display
FROM
    dbo.Documents sg JOIN dbo.DocumentFiles media
ON  media.DocumentId = sg.DocumentId JOIN dbo.DocumentAuthors
ON  sg.Author1 = DocumentAuthors.AuthorId JOIN dbo.MediaCategory
ON  sg.CategoryId = dbo.MediaCategory.CategoryId
WHERE
    media.MediaTypeId IN (2,16)
ORDER BY
    media.CreateDate DESC ,
    media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetLatestAudio]    Script Date: 04/05/2012 12:12:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetLatestAudio]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT TOP 10
    sg.DocumentId ,
    dbo.MediaCategory.Category + ' >> ' + sg.Title AS Title ,
    sg.PublicationInformation AS Description ,
    media.URL ,
    media.FileId ,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS Author ,
    sg.Author1 AS AuthorId ,
    sg.CreateDate ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 0
         WHEN sg.CoverImageUrl IS NOT NULL THEN 1
    END AS HasImage ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 'none'
         WHEN sg.CoverImageUrl IS NOT NULL THEN 'inline'
    END AS Display
FROM
    dbo.Documents sg JOIN dbo.DocumentFiles media
ON  media.DocumentId = sg.DocumentId JOIN dbo.DocumentAuthors
ON  sg.Author1 = DocumentAuthors.AuthorId JOIN dbo.MediaCategory
ON  sg.CategoryId = dbo.MediaCategory.CategoryId
WHERE
    media.MediaTypeId = 1 and media.Display = 1 and sg.Display = 1
ORDER BY
    media.CreateDate DESC ,
    media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetLatest]    Script Date: 04/05/2012 12:12:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetLatest]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT TOP 5
    sg.DocumentId ,
    dbo.MediaCategory.Category + ' >> ' + sg.Title AS Title ,
    sg.PublicationInformation AS Description ,
    media.URL ,
    media.FileId ,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS Author ,
    sg.Author1 AS AuthorId ,
    sg.CreateDate ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 0
         WHEN sg.CoverImageUrl IS NOT NULL THEN 1
    END AS HasImage ,
    CASE
         WHEN sg.CoverImageUrl IS NULL THEN 'none'
         WHEN sg.CoverImageUrl IS NOT NULL THEN 'inline'
    END AS Display
FROM
    dbo.Documents sg JOIN dbo.DocumentFiles media
ON  media.DocumentId = sg.DocumentId JOIN dbo.DocumentAuthors
ON  sg.Author1 = DocumentAuthors.AuthorId JOIN dbo.MediaCategory
ON  sg.CategoryId = dbo.MediaCategory.CategoryId
WHERE
    media.MediaTypeId IN ( 1 , 2 , 8, 16 )
ORDER BY
    media.CreateDate DESC ,
    media.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetFeed]    Script Date: 04/05/2012 12:12:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetFeed]
       @AuthorId int = 0 ,
       @CategoryId int = 0 ,
       @MediaTypeId int = 0 ,
       @MaxResults int = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF

IF @MaxResults > 0
   SET ROWCOUNT @MaxResults ;


DECLARE @MediaTypes TABLE
(
  TypeId int )

IF @MediaTypeId > 0
   INSERT INTO
       @MediaTypes
   VALUES
       (
         @MediaTypeId )
         
         
--if (@MediaTypeId = 2)         
--BEGIN
----2,12,14,16
-- INSERT INTO @MediaTypes   VALUES       (12 )
-- INSERT INTO @MediaTypes   VALUES       (14 )
-- INSERT INTO @MediaTypes   VALUES       (16 )
 --END
         
ELSE
   BEGIN

         INSERT INTO
             @MediaTypes
             (
               TypeId )
         VALUES
             (
               1 ) -- Audio 

         INSERT INTO
             @MediaTypes
             (
               TypeId )
         VALUES
             (
               2 ) -- Video

         INSERT INTO
             @MediaTypes
             (
               TypeId )
         VALUES
             (
               8 ) -- AudioBook
   END


IF @AuthorId > 0
   SELECT
       Documents.DocumentId ,
       Documents.Title ,
       Documents.GUID ,
       Documents.PublicationInformation AS Description ,
       media.URL ,
       media.FileId ,
       DocumentMediaType.MIMEtype ,
       
       authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
       Documents.Author1 ,
       Documents.Keywords ,
       Documents.CreateDate ,
       media.fileSize ,
       media.duration ,
       Documents.Description ,
       dbo.MediaCategory.iTunesCategoryCode ,
       dbo.MediaCategory.Category
   FROM
       Documents INNER JOIN DocumentFiles AS media
   ON  media.DocumentId = Documents.DocumentId INNER JOIN DocumentAuthors
   ON  Documents.Author1 = DocumentAuthors.AuthorId INNER JOIN dbo.MediaCategory
   ON  dbo.MediaCategory.CategoryId = dbo.Documents.CategoryId LEFT OUTER JOIN
   DocumentMediaType
   ON  DocumentMediaType.MediaTypeID = media.MediaTypeId
   WHERE
       media.MediaTypeId IN ( SELECT
                                  TypeId
                              FROM
                                  @MediaTypes ) AND Documents.Display = 1 AND ( Documents
                                                                                .Author1
                                                                                =
                                                                                @AuthorId
                                                                                OR
                                                                                Documents
                                                                                .Author2
                                                                                =
                                                                                @AuthorId )
   ORDER BY
       Documents.CreateDate
IF @CategoryId > 0
   BEGIN


         SELECT
             Documents.DocumentId ,
             Documents.Title ,
             Documents.GUID ,
             Documents.PublicationInformation AS Description ,
             media.URL ,
             media.FileId ,
             DocumentMediaType.MIMEtype ,
             media.filesize ,
             authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
             Documents.Author1 ,
             Documents.Keywords ,
             Documents.CreateDate ,
             media.fileSize ,
             media.duration ,
             Documents.Description ,
             dbo.MediaCategory.iTunesCategoryCode ,
             dbo.MediaCategory.Category
         FROM
             Documents INNER JOIN DocumentFiles AS media
         ON  media.DocumentId = Documents.DocumentId INNER JOIN DocumentAuthors
         ON  Documents.Author1 = DocumentAuthors.AuthorId INNER JOIN [MediaCategory]
         ON  [Documents].[CategoryId] = [MediaCategory].[CategoryId] LEFT OUTER JOIN
         DocumentMediaType
         ON  DocumentMediaType.MediaTypeID = media.MediaTypeId
         WHERE
             media.MediaTypeId IN ( SELECT
                                        TypeId
                                    FROM
                                        @MediaTypes ) AND Documents.Display = 1 AND (
                                                                                      [Documents]
                                                                                      .
                                                                                      [CategoryId]
                                                                                      =
                                                                                      @CategoryId
                                                                                      OR
                                                                                      Documents
                                                                                      .
                                                                                      CategoryId
                                                                                      IN
                                                                                      (
                                                                                      SELECT
                                                                                          m2.[CategoryId]
                                                                                      FROM
                                                                                          [MediaCategory] INNER JOIN [MediaCategory] m2
                                                                                      ON
                                                                                          m2.[ParentCategory] = @CategoryId ) )
         ORDER BY
             Documents.CreateDate

         SELECT
             Category ,
             [Description] ,
             CategoryImage
         FROM
             [MediaCategory]
         WHERE
             [CategoryId] = @CategoryId

   END
ELSE
   SELECT TOP ( 60 )
       Documents.DocumentId ,
       Documents.Title ,
       Documents.GUID ,
       Documents.PublicationInformation AS Description ,
       media.URL ,
       media.FileId ,
       DocumentMediaType.MIMEtype ,
       filesize ,
       authorFirst + ' ' + authorMiddle + ' ' + authorLast AS 'Author' ,
       Documents.Author1 ,
       Documents.Keywords ,
       Documents.CreateDate ,
       media.fileSize ,
       media.duration ,
       Documents.Description ,
       dbo.MediaCategory.iTunesCategoryCode ,
       dbo.MediaCategory.Category
   FROM
       Documents INNER JOIN DocumentFiles AS media
   ON  media.DocumentId = Documents.DocumentId INNER JOIN DocumentAuthors
   ON  Documents.Author1 = DocumentAuthors.AuthorId INNER JOIN [MediaCategory]
   ON  [Documents].[CategoryId] = [MediaCategory].[CategoryId] LEFT OUTER JOIN
   DocumentMediaType
   ON  DocumentMediaType.MediaTypeID = media.MediaTypeId
   WHERE
       ( media.MediaTypeId IN ( SELECT
                                    TypeId
                                FROM
                                    @MediaTypes ) ) AND ( Documents.Display = 1 )
   
   
   
   
   ORDER BY
       Documents.CreateDate DESC ,
       Documents.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[MediaGetFeatured]    Script Date: 04/05/2012 12:12:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaGetFeatured]
AS
BEGIN

      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF ;
    
    declare @docId int;
    SELECT top (1) @docId = documentid FROM dbo.DocumentFiles WHERE MediaTypeId = 16 order by newid();
    -- Get Headline Article

      SELECT 
          dbo.Documents.DocumentId ,
          dbo.Documents.GUID ,
          dbo.Documents.Display ,
          dbo.Documents.Title ,
          dbo.Documents.Author1 ,
          dbo.Documents.Author2 ,
          dbo.Documents.PublicationInformation ,
          dbo.Documents.Description AS 'Description' ,
          dbo.Documents.Keywords ,
          null as metaImage ,	-- this is retrieved independently and cached
--          dbo.Documents.oldURL ,
--          dbo.Documents.oldMediaTypeId ,
          
          dbo.Documents.Source ,
          --dbo.Documents.Description ,
          0 as filesize ,
          dbo.Documents.CategoryId ,
          dbo.Documents.CreateDate ,
          dbo.Documents.EditDate ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
          da2.authorFirst + ' ' + da2.authorMiddle + ' ' + da2.authorLast AS CoAuthor ,
          '/MediaPlayer.aspx?Id=' + CAST(DocumentId AS varchar(max)) AS 'URL'
      FROM
				dbo.Documents
				JOIN dbo.DocumentAuthors da
					ON  da.AuthorId = Documents.Author1
				LEFT JOIN dbo.DocumentAuthors da2
					ON  da2.AuthorId = Documents.Author2
      WHERE		documentid = @docId;

      SELECT
          dbo.DocumentFiles.FileId ,
          dbo.DocumentFiles.DocumentId ,
          dbo.DocumentFiles.MediaTypeId ,
          dbo.DocumentFiles.URL ,
          dbo.DocumentFiles.fileSize ,
          dbo.DocumentFiles.duration ,
          dbo.DocumentFiles.CreateDate ,
          dbo.DocumentFiles.VolumeOrdinal ,
          dbo.DocumentFiles.VolumeComment ,
          dmt.IsMedia ,
          dmt.MediaTypeId ,
          dmt.Description ,
          dmt.MediaIconPath
      FROM
          dbo.DocumentFiles INNER JOIN dbo.Documents
      ON  dbo.DocumentFiles.DocumentId = dbo.Documents.DocumentId LEFT OUTER JOIN
      DocumentMediaType dmt
      ON  dmt.MediaTypeID = DocumentFiles.MediaTypeId
      WHERE
          --Featured = 1
          Documents.documentid = @docId


END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetContributorList]    Script Date: 04/05/2012 12:12:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetContributorList]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT
    DocumentAuthors.AuthorId ,
    DocumentAuthors.AuthorFirst AS FirstName ,
    DocumentAuthors.AuthorMiddle AS MiddleName ,
    DocumentAuthors.AuthorLast AS LastName ,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS Author
FROM
    dbo.DocumentAuthors
WHERE
    DocumentAuthors.AuthorId IN ( SELECT
                                      Documents.Author1
                                  FROM
                                      dbo.Documents JOIN dbo.DocumentFiles media
                                  ON  media.DocumentId = Documents.DocumentId
                                  WHERE
                                      media.MediaTypeId IN ( 1 , 2 ) 
                                      AND media.Display = 1
                                      )
                                      
ORDER BY
    DocumentAuthors.AuthorLast
GO
/****** Object:  StoredProcedure [dbo].[MediaGetCategoryContent]    Script Date: 04/05/2012 12:12:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetCategoryContent]
       @CategoryId int = 0 ,
       @SubjectId int = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF 

--IF @CategoryId = -1 OR @CategoryId = 0
EXEC dbo.MediaGetSubCategories @CategoryId

IF @CategoryId >= 0
BEGIN
   IF @CategoryId = 210  -- Get Recent Media
      SELECT TOP 30
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.Description AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.FileId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.CoverImageURL IS NULL THEN 0
               WHEN Documents.CoverImageURL IS NOT NULL THEN 1
          END AS HasImage
      FROM
          dbo.Documents LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId
      WHERE
          DocumentMediaType.IsMedia = 1
                   -- AND Documents.CategoryId = @CategoryId
          AND Documents.Display = 1
          AND media.Display = 1
      ORDER BY
          Documents.CreateDate DESC
   ELSE
      SELECT
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.Description AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.FileId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.CoverImageURL IS NULL THEN 0
               WHEN Documents.CoverImageURL IS NOT NULL THEN 1
          END AS HasImage
      FROM
          dbo.Documents LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId
      WHERE
          DocumentMediaType.IsMedia = 1 AND Documents.CategoryId =
          @CategoryId AND Documents.Display = 1 AND media.Display = 1
      ORDER BY
          Documents.Title
          
          
 SELECT mc.CategoryId, mc.ParentCategory, mc.Category, mc.CategoryImage, parent.Category 
 AS ParentCategoryName, parent.CategoryId AS ParentCategoryId
 FROM dbo.MediaCategory mc
LEFT JOIN dbo.MediaCategory parent ON parent.CategoryId = mc.ParentCategory
 WHERE mc.CategoryId = @CategoryId

END

ELSE

BEGIN

   IF @SubjectId > 0
      SELECT 
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.Description AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.FileId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.CoverImageURL IS NULL THEN 0
               WHEN Documents.CoverImageURL IS NOT NULL THEN 1
          END AS HasImage
      FROM
          dbo.Documents LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId           
      WHERE
          DocumentMediaType.MediaTypeId IN ( 1 , 2 ) AND Documents.Display = 1 AND media.Display = 1 AND
          Documents.DocumentId IN ( SELECT
                                        DocumentId
                                    FROM
                                        DocumentSubjectLink
                                    WHERE
                                        DocumentSubjectLink.SubjectID = @SubjectId )
      ORDER BY
          Documents.Title
   ELSE  -- Get ALL media
      SELECT
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.Description AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.FileId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.CoverImageURL IS NULL THEN 0
               WHEN Documents.CoverImageURL IS NOT NULL THEN 1
          END AS HasImage
      FROM
          dbo.Documents LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId
      WHERE
          DocumentMediaType.MediaTypeId IN ( 1 , 2 ) AND Documents.Display = 1 AND media.Display = 1
      ORDER BY
          Documents.Title
          
 SELECT mc.CategoryId, mc.ParentCategory, mc.Category, mc.CategoryImage, parent.Category 
 AS ParentCategoryName, parent.CategoryId AS ParentCategoryId
 FROM dbo.MediaCategory mc
LEFT JOIN dbo.MediaCategory parent ON parent.CategoryId = mc.ParentCategory
 WHERE mc.CategoryId = @CategoryId          
          
END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetAuthorsAlpha]    Script Date: 04/05/2012 12:12:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

/**********************************************************************************************************
Author:		Vasily Kabanov
Created:	2011-10-02
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
			Reports number of documents containing audio or video files (media type mp3, wmv, mp4a) per author.
			!!TODO: check if Stream and mp4 need to be included too; it seems a concept of media type
			group/category is missing.
Old query was 
	SELECT
          d.Author1 AS AuthorId ,
          da.AuthorLast ,
          da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
          COUNT(1) AS 'Count'
      FROM
          Documents d JOIN [DocumentAuthors] da
      ON  d.[Author1] = da.[AuthorId] OR d.[Author2] = da.[AuthorId] JOIN
      DocumentFiles
      ON  DocumentFiles.DocumentId = d.DocumentId
      WHERE
          DocumentFiles.MediaTypeId IN ( 1 , 2 , 8 )          
      GROUP BY
          d.Author1 ,
          da.AuthorLast ,
          da.AuthorFirst ,
          da.AuthorMiddle        
      ORDER BY
          da.AuthorLast

Examples:

exec dbo.MediaGetAuthorsAlpha


**********************************************************************************************************/
CREATE  PROCEDURE [dbo].[MediaGetAuthorsAlpha]
as
set nocount on;

with DocumentsWithMedia as (
	select			d.DocumentId
					, d.Author1
					, d.Author2
	from			dbo.Documents d
	where			exists (
						select		*
						from		dbo.DocumentFiles r
						where		d.DocumentId = r.DocumentId
									and r.MediaTypeId IN ( 1 , 2 , 8 )
									and r.Display = 1
					)
					and d.Display = 1
)
, CountsFlat as (
	select			Author1 as AuthorId
					, count(*) as cnt
	from			DocumentsWithMedia
	group by		Author1
	union all
	select			Author2
					, count(*) as cnt
	from			DocumentsWithMedia
	--where			Author2 != Author1
	group by		Author2
)
, CountsTotal as (
	select			AuthorId
					, sum(cnt) as cnt
	from			CountsFlat
	group by		AuthorId
)
select		c.AuthorId
			, da.AuthorLast
			, da.AuthorFirst + coalesce(N' ' + da.AuthorMiddle + N' ', N' ') + da.AuthorLast AS Author
			, c.cnt AS [Count]
from		CountsTotal c
			, dbo.DocumentAuthors da
where		c.AuthorId = da.AuthorId;

return @@error;
GO
/****** Object:  StoredProcedure [dbo].[MediaGetAuthors]    Script Date: 04/05/2012 12:12:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DocumentGetTopAuthors
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaGetAuthors]
AS
SET CONCAT_NULL_YIELDS_NULL OFF

BEGIN
      SELECT
          d.Author1 AS AuthorId ,
          da.AuthorLast ,
          da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
          COUNT(1) AS 'Count'
      FROM
          Documents d JOIN [DocumentAuthors] da
      ON  d.[Author1] = da.[AuthorId] OR d.[Author2] = da.[AuthorId] JOIN
      DocumentFiles
      ON  DocumentFiles.DocumentId = d.DocumentId
      JOIN dbo.DocumentMediaType ON dbo.DocumentFiles.MediaTypeId = dbo.DocumentMediaType.MediaTypeID 
      WHERE
      dbo.DocumentMediaType.IsMedia = 1 AND dbo.DocumentMediaType.IsMedia = 1
      GROUP BY
          d.Author1 ,
          da.AuthorLast ,
          da.AuthorFirst ,
          da.AuthorMiddle
      ORDER BY
          COUNT DESC
END
GO
/****** Object:  StoredProcedure [dbo].[MediaGetAuthorContents]    Script Date: 04/05/2012 12:12:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[MediaGetAuthorContents] @AuthorId int
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT
    MediaCategory.CategoryId ,
    MediaCategory.Category ,
    MediaCategory.Description ,
    MediaCategory.CategoryImage ,
    ISNULL(( SELECT TOP 1
                 Documents.CreateDate
             FROM
                 dbo.Documents
             WHERE
                 Documents.CategoryId = MediaCategory.CategoryId
             ORDER BY
                 Documents.CreateDate DESC ) , '1/1/2050') AS EditDate
FROM
    dbo.MediaCategory
WHERE
    MediaCategory.CategoryId = -1


      SELECT
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.Description AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.FileId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.CoverImageURL IS NULL THEN 0
               WHEN Documents.CoverImageURL IS NOT NULL THEN 1
          END AS HasImage
      FROM
          dbo.Documents LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId
    
WHERE
    DocumentMediaType.MediaTypeId IN ( 1 , 2 ) AND ( Documents.Author1 = @AuthorId OR
                                                     Documents.Author2 = @AuthorId ) AND
    Documents.Display = 1
ORDER BY
    Documents.Title
GO
/****** Object:  StoredProcedure [dbo].[MediaGetAllFeatured]    Script Date: 04/05/2012 12:12:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaGetAllFeatured]
AS
BEGIN

      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF ;
    
    

      

      SELECT
          dbo.DocumentFiles.FileId ,
          dbo.DocumentFiles.DocumentId ,
          dbo.DocumentFiles.MediaTypeId ,
          dbo.DocumentFiles.URL ,
          dbo.DocumentFiles.fileSize ,
          dbo.DocumentFiles.duration ,
          dbo.DocumentFiles.CreateDate ,
          dmt.IsMedia ,
          dmt.MediaTypeId ,
          dmt.Description ,
          dmt.MediaIconPath
      FROM
          dbo.DocumentFiles INNER JOIN dbo.Documents
      ON  dbo.DocumentFiles.DocumentId = dbo.Documents.DocumentId LEFT OUTER JOIN
      DocumentMediaType dmt
      ON  dmt.MediaTypeID = DocumentFiles.MediaTypeId
      --WHERE
      --    Featured = 1


END
GO
/****** Object:  StoredProcedure [dbo].[MediaAddMetadata]    Script Date: 04/05/2012 12:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[MediaAddMetadata]
       @DocumentId int ,
       @FileId int ,
       @filesize bigint ,
       @duration decimal = 0 ,
       @Keywords varchar(500) ,
       @Description varchar(MAX) ,
       @CoverImage image = NULL
AS
BEGIN

      UPDATE
          DocumentFiles
      SET
          fileSize = @fileSize ,
          duration = @duration          
      WHERE
          FileId = @FileId
          
          -- update keywords
	  UPDATE
          Documents
      SET                
          Keywords = @Keywords,
          editdate = GETDATE()
      WHERE
          DocumentId = @DocumentId AND (Keywords IS NULL OR Keywords ='')          

		-- update description
      UPDATE
          Documents
      SET      
          Description = @Description ,          
          editdate = GETDATE()
      WHERE
          DocumentId = @DocumentId AND (Description IS NULL OR Description ='')

		-- save thumbnail
      IF @CoverImage IS NOT NULL
         BEGIN

               UPDATE
                   Documents
               SET
                   CoverImage = @CoverImage
               WHERE
                   ( DocumentId = @DocumentId )

               UPDATE
                   MediaCategory
               SET
                   CategoryImage =
                   'http://mises.org/media/poster/' + CAST(
                   @DocumentID AS varchar(10))
               WHERE
                   CategoryId IN ( SELECT
                                       CategoryId
                                   FROM
                                       Documents
                                   WHERE
                                       DocumentId = @DocumentId ) AND ( MediaCategory.CategoryImage IS NULL OR MediaCategory.CategoryImage = '' )

         END

END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleGet]    Script Date: 04/05/2012 12:12:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticleGet]
(
 @ArticleId int )
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT
    DailyArticles.ArticleId ,
    DailyArticles.GUID ,
    DailyArticles.Title ,
    DailyArticles.Description ,
    DailyArticles.ArticleText ,
    DailyArticles.Featured ,
    DailyArticles.Headline ,
    DailyArticles.ShowArticle ,
    DailyArticles.DatePosted ,
    da.AuthorFirst ,
    da.AuthorLast ,
    DailyArticles.AuthorId ,
    DailyArticles.CoAuthorId ,
    DailyArticles.DisplayOrder ,
    DailyArticles.CreatedDate ,
    da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS AuthorName ,
    ca.AuthorFirst + ' ' + ca.AuthorMiddle + ' ' + ca.AuthorLast AS CoAuthorName ,
    DailyArticles.PhotoURL ,
    DailyArticles.ThumbnailURL ,
    DailyArticles.PhotoHeight ,
    [DailyArticles].[EditBy] ,
    [DailyArticles].[EditDate] ,
    ( SELECT TOP 1
          daNext.ArticleId
      FROM
          DailyArticles daNext
      WHERE
          daNext.DatePosted > DailyArticles.DatePosted --AND daNext.ArticleId > DailyArticles.ArticleId
          AND ( daNext.ShowArticle = 1 OR daNext.Featured = 1 OR daNext.Headline = 1 )
      ORDER BY
          daNext.DatePosted ,
          daNext.ArticleId ) AS 'NextId' ,
    ( SELECT TOP 1
          daprevious.ArticleId
      FROM
          DailyArticles daprevious
      WHERE
          daprevious.DatePosted < DailyArticles.DatePosted -- AND daprevious.ArticleId < DailyArticles.ArticleId
          AND ( daprevious.ShowArticle = 1 OR daprevious.Featured = 1 OR daprevious.
                Headline = 1 )
      ORDER BY
          daprevious.DatePosted DESC ,
          daprevious.ArticleId DESC ) AS 'PreviousId'
FROM
    dbo.DailyArticles LEFT OUTER JOIN dbo.DocumentAuthors AS da
ON  da.AuthorId = DailyArticles.AuthorId LEFT OUTER JOIN dbo.DocumentAuthors AS ca
ON  ca.AuthorId = DailyArticles.CoAuthorId
WHERE
    ( DailyArticles.ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleAdd]    Script Date: 04/05/2012 12:12:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticleAdd]
(
 @ArticleId int OUTPUT ,
 @Title varchar(255) ,
 @Description text ,
--@AuthorFirst varchar(50)='',
--@AuthorLast varchar(50)='',
 @ArticleText text ,
 @ShowArticle bit ,
 @AuthorId int = 0 ,
 @Featured bit ,
 @Headline bit ,
 @CoAuthorId int = 0 ,
--@DisplayOrder int =10,
 @PhotoHeight int = 0 ,
 @PhotoURL varchar(255) = NULL ,
 @EditBy varchar(75) = NULL ,
 @DatePosted smalldatetime = GETDATE )
AS
INSERT INTO
    DailyArticles
    (
      Title ,
      Description ,
      ArticleText ,
      ShowArticle ,
      Featured ,
      Headline ,
      AuthorId ,
      CoAuthorId ,
      PhotoURL ,
      DatePosted ,
      PhotoHeight ,
      EditBy )
VALUES
    (
      @title ,
      @description ,
      @ArticleText ,
      @ShowArticle ,
      @Featured ,
      @Headline ,
      @AuthorId ,
      @CoAuthorId ,
      @PhotoURL ,
      @DatePosted ,
      @PhotoHeight ,
      @EditBy )

SET @ArticleId = SCOPE_IDENTITY()


DECLARE @RC int
DECLARE @RevisionId int
DECLARE @DocumentGUID uniqueidentifier

SET @DocumentGUID = ( SELECT
                          dbo.DailyArticles.GUID
                      FROM
                          dbo.DailyArticles
                      WHERE
                          ArticleId = @ArticleId )
EXECUTE @RC = [Mises].[dbo].[RevisionCreate] @RevisionId OUTPUT , @DocumentGUID ,
@ArticleText , @EditBy

SELECT
    @ArticleId
GO
/****** Object:  StoredProcedure [dbo].[ArticleGetDetails]    Script Date: 04/05/2012 12:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE  [dbo].[ArticleGetDetails]
(
 @ArticleId int )
AS
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT
    DailyArticles.ArticleId ,
    DailyArticles.GUID ,
    DailyArticles.Title ,
    DailyArticles.Description ,
    DailyArticles.ArticleText ,
    DailyArticles.ShowArticle ,
    DailyArticles.DatePosted ,
    da.AuthorFirst ,
    da.AuthorLast ,
    DailyArticles.AuthorId ,
    DailyArticles.CoAuthorId ,
    DailyArticles.DisplayOrder ,
    DailyArticles.CreatedDate ,
    da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS AuthorName ,
    ca.AuthorFirst + ' ' + ca.AuthorMiddle + ' ' + ca.AuthorLast AS CoAuthorName ,
    DailyArticles.PhotoURL ,
    DailyArticles.PhotoHeight ,
    [DailyArticles].[EditBy] ,
    [DailyArticles].[EditDate] ,
    ( SELECT TOP 1
          daNext.ArticleId
      FROM
          DailyArticles daNext
      WHERE
          daNext.DatePosted > DailyArticles.DatePosted AND daNext.ShowArticle = 1
      ORDER BY
          daNext.DatePosted ) AS 'NextId' ,
    ( SELECT TOP 1
          daprevious.ArticleId
      FROM
          DailyArticles daprevious
      WHERE
          daprevious.DatePosted < DailyArticles.DatePosted AND daprevious.ShowArticle = 1
      ORDER BY
          daprevious.DatePosted DESC ) AS 'PreviousId'
FROM
    dbo.DailyArticles LEFT OUTER JOIN dbo.DocumentAuthors AS da
ON  da.AuthorId = DailyArticles.AuthorId LEFT OUTER JOIN dbo.DocumentAuthors AS ca
ON  ca.AuthorId = DailyArticles.CoAuthorId
WHERE
    ( DailyArticles.ArticleId = @ArticleId )
GO
/****** Object:  StoredProcedure [dbo].[_PopulateNewDocTables]    Script Date: 04/05/2012 12:11:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_PopulateNewDocTables] 
AS
BEGIN

--truncate table documentfiles
--delete from Documents


--SET IDENTITY_INSERT Documents ON


--INSERT
--INTO
--	Documents
--	(DocumentId
--   , GUID
--   , Display
--   , Title
--   , Author1
--   , Author2
--   , CoverImage
--   , PublicationInformation
--   , Source
--   , Description
--   , Keywords
--   , CategoryId
--   , CreateDate
--   , EditDate
--   , ProductId
--   , ISBN)
--SELECT DocumentId
--	 , GUID
--	 , Display
--	 , Title
--	 , Author1
--	 , Author2
--	 , metaImage
--	 , PubInfo
--	 , Source
--	 , metaDescription
--	 , metaKeywords
--	 , CategoryId
--	 , CreateDate
--	 , EditDate
--	 , ProductId
--	 , ISBN
--FROM
--	Documents
--WHERE
--	DocumentId NOT IN (SELECT DocumentId
--					   FROM
--						   Documents)


--SET IDENTITY_INSERT Documents OFF


--SET IDENTITY_INSERT DocumentFiles ON

----delete from DocumentFiles where URL is null

--INSERT
--INTO
--	DocumentFiles
--	(
--	FileId
--  , URL
--  , DocumentId
--  , MediaTypeId
--  , fileSize
--  , duration
--  , CreateDate
--  , Display
--  , VolumeOrdinal
--  , VolumeComment)
--SELECT MediaId
--	 , URL
--	 , DocumentId
--	 , MediaTypeId
--	 , fileSize
--	 , duration
--	 , CreateDate
--	 , Display
--	 , VolumeOrdinal
--	 , VolumeComment
--FROM
--	MediaAlternateFormat
--WHERE
--	MediaId NOT IN (SELECT FileId
--					FROM
--						DocumentFiles)


--SET IDENTITY_INSERT DocumentFiles OFF

--UPDATE Documents
--SET
--	 PrivateComment = [Description]

	

--UPDATE Documents
--SET
--	PrivateComment = PrivateComment + '
-- ' + Description
--WHERE
--	(PrivateComment != Description) AND
--	(Description IS NOT NULL AND
--	Description != '')

-- update description with store data


UPDATE Documents
SET
	Documents.Description = ac_Products.Description
FROM
	AbleCommerce..ac_Products
WHERE
	(ac_Products.Description IS NOT NULL AND
	len(ac_Products.Description) > 100)
	AND
	Documents.ProductId = AbleCommerce..ac_Products.ProductId
	AND
	ac_Products.Description != Documents.Description
	--AND
	--len(ac_Products.Description) > len(Documents.Description)

-- Update images with store thumbnails
UPDATE Documents
SET
	Documents.CoverImageURL =
	REPLACE((SELECT TOP 1 ImageUrl
			 FROM
				 AbleCommerce.dbo.ac_Products
			 WHERE
				 Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId), '~', '/store')
FROM
	AbleCommerce..ac_Products
WHERE
	Documents.ProductId = AbleCommerce..ac_Products.ProductId

UPDATE Documents
SET
	Documents.CoverImageURL = 'media/poster/' + cast(Documents.DocumentId AS VARCHAR(MAX))
WHERE
	Documents.CoverImageURL IS NULL

UPDATE Documents SET 
EditDate = (SELECT top 1 CreateDate from DocumentFiles where DocumentFiles.DocumentId = Documents.DocumentId ORDER BY DocumentFiles.CreateDate desc)
WHERE
(SELECT top 1 CreateDate from DocumentFiles where DocumentFiles.DocumentId = Documents.DocumentId ORDER BY DocumentFiles.CreateDate desc) > EditDate

--update Documents SET Documents.ProductId = Documents.ProductId
--FROM Documents, Documents
--WHERE Documents.DocumentId = Documents.DocumentId
--AND ( Documents.ProductId IS NULL OR Documents.ProductId =0) 

--update DocumentFiles SET 
--DocumentFiles.fileSize = MediaAlternateFormat.fileSize,
--DocumentFiles.duration = MediaAlternateFormat.duration
--FROM DocumentFiles, MediaAlternateFormat
--WHERE DocumentFiles.FileId = MediaAlternateFormat.MediaId



--update Documents SET Documents.Author1 = Documents.Author1
--FROM Documents, Documents
--WHERE Documents.DocumentId = Documents.DocumentId




--update DocumentFiles SET DocumentFiles.URL = MediaAlternateFormat.URL
--FROM DocumentFiles, MediaAlternateFormat
--WHERE DocumentFiles.FileId = MediaAlternateFormat.MediaId
--AND DocumentFiles.URL LIKE 'http://blog.mises.org'
----AND DocumentFiles.DocumentId 



UPDATE Documents SET ProductId = (SELECT ProductId FROM AbleCommerce.dbo.ac_products products WHERE products.[Name] = Documents.title) WHERE ProductId = 0

update Documents
set Keywords = searchkeywords
from AbleCommerce..ac_Products  
where 
(Keywords IS NULL OR Keywords = '' ) AND 
searchkeywords IS NOT NULL and
Documents.ProductId = AbleCommerce..ac_Products.ProductId

update Documents
set ISBN = ModelNumber
from AbleCommerce..ac_Products  
where 
ModelNumber IS NOT NULL 
AND ISBN is NULL
AND Documents.ProductId = AbleCommerce..ac_Products.ProductId

	
	
END
GO
/****** Object:  StoredProcedure [dbo].[_FixHTML]    Script Date: 04/05/2012 12:11:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_FixHTML]
AS
BEGIN

      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , '<font[^>]*>' , '')
      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , '</font>' , '')
      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , ' class="MsoBodyText"' , '')

      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , '<p align="left">' , '<p>')
      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , '<p class="Mso[^>]*>' , '<p>')

      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , '<FONT[^>]*>' , '')
      UPDATE
          DailyArticles
      SET
          ArticleText = dbo.regexReplace(ArticleText , '</FONT>' , '')
      EXEC _SearchAndReplace @SearchStr = '<br>' , @ReplaceStr = '<br />'
      EXEC _SearchAndReplace @SearchStr = '<s>' , @ReplaceStr =
      '<span class="line-through">'
      EXEC _SearchAndReplace @SearchStr = '</s>' , @ReplaceStr = '</span>'
      EXEC _SearchAndReplace @SearchStr = '<center>' , @ReplaceStr =
      '<div style="text-align: center">'
      EXEC _SearchAndReplace @SearchStr = '</center>' , @ReplaceStr = '</div>'
      EXEC [_SearchAndReplace] @SearchStr = 'nowrap="true"' , @ReplaceStr =
      'nowrap="nowrap"'
      EXEC [_SearchAndReplace] @SearchStr = '<hr color="#0d4d7d" noshade="noshade" />' ,      @ReplaceStr = '<hr />'


EXEC _SearchAndReplace @SearchStr = 'https://www.mises.org' , @ReplaceStr ='https://mises.org'
EXEC _SearchAndReplace @SearchStr = 'http://www.mises.org' , @ReplaceStr ='http://mises.org'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/multimedia' , @ReplaceStr ='http://media.mises.org'
EXEC _SearchAndReplace @SearchStr = 'http://direct.mises.org/' , @ReplaceStr ='http://mises.org/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org:88' , @ReplaceStr ='http://direct.mises.org:88'

EXEC _SearchAndReplace @SearchStr = 'http://mises.org/images/' , @ReplaceStr ='http://images.mises.org/'
EXEC _SearchAndReplace @SearchStr = 'https://mises.org/images/' , @ReplaceStr ='https://images.mises.org/'
--EXEC _SearchAndReplace @SearchStr = 'http://images.mises.org/' , @ReplaceStr ='http://mises.org/images/'

--  <img src="http://images.mises.org/" alt="" border="0" /> [image]

EXEC _SearchAndReplace @SearchStr = '//mises.org/images/blog/' , @ReplaceStr ='//wp.mises.org/blog/'

EXEC _SearchAndReplace @SearchStr = '/store/oldimages/' , @ReplaceStr ='/store/Able5Images/'


EXEC _SearchAndReplace @SearchStr = 'http://mises.org/story' , @ReplaceStr ='http://mises.org/daily'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/fullarticle.aspx?Id=' , @ReplaceStr ='http://mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/fullstory.aspx?Id=' , @ReplaceStr ='http://mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/fullstory.asp?control=' , @ReplaceStr ='http://mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/studyguide.asp' , @ReplaceStr ='http://mises.org/literature.aspx'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/multimedia/' , @ReplaceStr ='http://media.mises.org/'
EXEC _SearchAndReplace @SearchStr = 'mises.org/article.aspx?Id=' , @ReplaceStr ='mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'articles.aspx?action=gallery' , @ReplaceStr ='articles.aspx'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/fullarticle.asp?record=' , @ReplaceStr ='http://mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/blog/' , @ReplaceStr ='http://blog.mises.org/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/fullarticle.asp?control=' , @ReplaceStr ='http://mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/fullarticle.asp?id=' , @ReplaceStr ='http://mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'mises.org/daily/control=' , @ReplaceStr ='mises.org/daily/'
EXEC _SearchAndReplace @SearchStr = 'https://mises.org/daily' , @ReplaceStr ='http://mises.org/daily'
EXEC _SearchAndReplace @SearchStr = 'https://mises.org/form.aspx?Id' , @ReplaceStr ='http://mises.org/form.aspx?Id'
EXEC _SearchAndReplace @SearchStr = 'http://mises.org/images2/' , @ReplaceStr ='http://mises.org/images/'
EXEC _SearchAndReplace @SearchStr = 'https://mises.org/images2/' , @ReplaceStr ='https://mises.org/images/'

EXEC _SearchAndReplace @SearchStr = 'mises.org/articles.aspx"' , @ReplaceStr ='mises.org/daily"'

EXEC _SearchAndReplace @SearchStr = '/articleRSS.aspx' , @ReplaceStr ='/dailyarticles.xml'
EXEC _SearchAndReplace @SearchStr = '/mediaRSS.aspx' , @ReplaceStr ='/media.xml'
EXEC _SearchAndReplace @SearchStr = '/guideRSS.aspx' , @ReplaceStr ='/literature.xml'
EXEC _SearchAndReplace @SearchStr = '/eventsRSS.aspx' , @ReplaceStr ='/events.xml'
EXEC _SearchAndReplace @SearchStr = '/freemarket_detail.asp?' , @ReplaceStr ='/freemarket_detail.aspx?'
EXEC _SearchAndReplace @SearchStr = '/misesreview_detail.asp?' , @ReplaceStr ='/misesreview_detail.aspx?'
EXEC _SearchAndReplace @SearchStr = '/Feeds/events.xml' , @ReplaceStr ='/events.xml'
EXEC _SearchAndReplace @SearchStr = '"/Feeds/newbooks.ashx' , @ReplaceStr ='"http://feeds.mises.org/MisesStore'
EXEC _SearchAndReplace @SearchStr = '/upcomingstory.asp?control=' , @ReplaceStr ='/events/'
EXEC _SearchAndReplace @SearchStr = '/upcomingstory.aspx?control=' , @ReplaceStr ='/events/'
EXEC _SearchAndReplace @SearchStr = '/upcomingstory.aspx?Id=' , @ReplaceStr ='/events/'
EXEC _SearchAndReplace @SearchStr = 'Controls/Media/DocumentImage.ashx?Id=' , @ReplaceStr ='media/poster/'

-- Set 2

EXEC _SearchAndReplace @SearchStr = '/images3/banner.gif' , @ReplaceStr ='/images3/banner_rotator.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/piano.jpg' , @ReplaceStr ='/images3/piano2.jpg' 
EXEC _SearchAndReplace @SearchStr = '/images3/fed.gif' , @ReplaceStr ='/images3/fed2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/fedfunds.gif' , @ReplaceStr ='/images3/fedfunds2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/fedfunds2.gif' , @ReplaceStr ='/images3/fedfunds22.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/hans.gif' , @ReplaceStr ='/images3/hans2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/HofL.gif' , @ReplaceStr ='/images3/HofL2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/Krugman.gif' , @ReplaceStr ='/images3/Krugman2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/leviathan.gif' , @ReplaceStr ='/images3/leviathan2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/MarketCommentary.gif' , @ReplaceStr ='/images3/MarketCommentary2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/mises.gif' , @ReplaceStr ='/images3/mises22.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/mises2.gif' , @ReplaceStr ='/images3/mises22.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/mises3.gif' , @ReplaceStr ='/images3/mises22.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/MisesMedia.gif' , @ReplaceStr ='/images3/MisesMedia2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/NYBOT.gif' , @ReplaceStr ='/images3/NYBOT2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/piano.gif' , @ReplaceStr ='/images3/piano2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/press.gif' , @ReplaceStr ='/images3/press2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/PWD2003.gif' , @ReplaceStr ='/images3/PWD2003_2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/qjae.gif' , @ReplaceStr ='/images3/qjae2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/RonPaul.gif' , @ReplaceStr ='/images3/RonPaul2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/steel.gif' , @ReplaceStr ='/images3/steel2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/shock.gif' , @ReplaceStr ='/images3/shock2.gif'
EXEC _SearchAndReplace @SearchStr = '/images3/students2b.jpg' , @ReplaceStr ='/images3/students2b2.jpg'
EXEC _SearchAndReplace @SearchStr = '/images3/atlas.jpg' , @ReplaceStr ='/images3/atlas2.jpg'
EXEC _SearchAndReplace @SearchStr = '/images3/Shostak/' , @ReplaceStr ='/images3/Shostak2/'

-- Set 3
EXEC _SearchAndReplace @SearchStr = '/images2/fm.gif' , @ReplaceStr ='/images2/fm2.gif'
EXEC _SearchAndReplace @SearchStr = '/images2/MU2002.jpg' , @ReplaceStr ='/images2/MU2002-2.jpg'
EXEC _SearchAndReplace @SearchStr = '/images2/nondurable.gif' , @ReplaceStr ='/images2/nondurable2.gif'
EXEC _SearchAndReplace @SearchStr = '/images2/Salerno.jpg' , @ReplaceStr ='/images2/Salerno2.jpg'

EXEC _SearchAndReplace @SearchStr = '/images/HelioBeltrao.jpg' , @ReplaceStr ='/images/people/HelioBeltrao.jpg'

-- Set 4

EXEC _SearchAndReplace @SearchStr = 'mises.org/images2/' , @ReplaceStr ='mises.org/images/' 
EXEC _SearchAndReplace @SearchStr = '"/images2/' , @ReplaceStr ='"/images/' 

EXEC _SearchAndReplace @SearchStr = 'mises.org/images3/' , @ReplaceStr ='mises.org/images/' 
EXEC _SearchAndReplace @SearchStr = '"/images3/' , @ReplaceStr ='"/images/' 


EXEC _SearchAndReplace @SearchStr = 'src="../images3/' , @ReplaceStr ='src="http://images.mises.org/' 
EXEC _SearchAndReplace @SearchStr = 'src="../images2/' , @ReplaceStr ='src="http://images.mises.org/' 

UPDATE dbo.DailyArticles SET PhotoURL = REPLACE(PhotoURL,'images2/','images/') WHERE PhotoURL LIKE '%images2%'
UPDATE dbo.DailyArticles SET PhotoURL = REPLACE(PhotoURL,'images3/','images/') WHERE PhotoURL LIKE '%images3%'
UPDATE dbo.DailyArticles SET PhotoURL = REPLACE(PhotoURL,'images4/','images/') WHERE PhotoURL LIKE '%images4%'

--EXEC _SearchAndReplace @SearchStr = 'images4/new.gif' , @ReplaceStr ='images4/new2.gif'
--EXEC _SearchAndReplace @SearchStr = 'images4/guido.jpg' , @ReplaceStr ='images4/guido4.jpg'
--EXEC _SearchAndReplace @SearchStr = 'images4/Salerno.jpg' , @ReplaceStr ='images4/Salerno3.jpg'
--EXEC _SearchAndReplace @SearchStr = 'images4/Christmas.jpg' , @ReplaceStr ='images4/Christmas2.jpg'
--EXEC _SearchAndReplace @SearchStr = 'images4/hazlitt.jpg' , @ReplaceStr ='images4/hazlitt5.jpg'
--EXEC _SearchAndReplace @SearchStr = 'images4/holly.gif' , @ReplaceStr ='images4/holly2.jpg'
--EXEC _SearchAndReplace @SearchStr = 'images4/saigon.gif' , @ReplaceStr ='images4/saigon2.jpg'
--EXEC _SearchAndReplace @SearchStr = 'images4/Shostak3' , @ReplaceStr ='images4/Shostak'
--EXEC _SearchAndReplace @SearchStr = 'images4/Shostak/' , @ReplaceStr ='images4/Shostak3/'

EXEC _SearchAndReplace @SearchStr = 'src="images4/' , @ReplaceStr ='src="images/'
EXEC _SearchAndReplace @SearchStr = 'src="/images4/' , @ReplaceStr ='src="/images/'
EXEC _SearchAndReplace @SearchStr = 'src="../images4/' , @ReplaceStr ='src="../images/'
EXEC _SearchAndReplace @SearchStr = 'mises.org/images4/' , @ReplaceStr ='mises.org/images/'


EXEC _SearchAndReplace @SearchStr = 'src="images2/' , @ReplaceStr ='src="images/'
EXEC _SearchAndReplace @SearchStr = 'src="/images2/' , @ReplaceStr ='src="/images/'
EXEC _SearchAndReplace @SearchStr = 'src="../images2/' , @ReplaceStr ='src="../images/'
EXEC _SearchAndReplace @SearchStr = 'mises.org/images2/' , @ReplaceStr ='mises.org/images/'

EXEC _SearchAndReplace @SearchStr = 'src="images3/' , @ReplaceStr ='src="images/'
EXEC _SearchAndReplace @SearchStr = 'src="/images3/' , @ReplaceStr ='src="/images/'
EXEC _SearchAndReplace @SearchStr = 'src="../images3/' , @ReplaceStr ='src="../images/'
EXEC _SearchAndReplace @SearchStr = 'mises.org/images3/' , @ReplaceStr ='mises.org/images/'

EXEC _SearchAndReplace @SearchStr = '/images2/' , @ReplaceStr ='/images/'
EXEC _SearchAndReplace @SearchStr = '/images3/' , @ReplaceStr ='/images/'
EXEC _SearchAndReplace @SearchStr = '/images4/' , @ReplaceStr ='/images/'





UPDATE dbo.DailyArticles SET PhotoURL = REPLACE(PhotoURL,'images2/','images/') WHERE PhotoURL LIKE '%images2%'
UPDATE dbo.DailyArticles SET PhotoURL = REPLACE(PhotoURL,'images3/','images/') WHERE PhotoURL LIKE '%images3%'
UPDATE dbo.DailyArticles SET PhotoURL = REPLACE(PhotoURL,'images4/','images/') WHERE PhotoURL LIKE '%images4%'


UPDATE dbo.DailyArticles SET ArticleText = REPLACE(ArticleText,'https://mises.org/images','http://mises.org/images') WHERE ArticleText LIKE '%https://mises.org/images%'
UPDATE dbo.DailyArticles SET ArticleText = REPLACE(ArticleText,'https://mises.org/store/Assets','http://mises.org/store/Assets') WHERE ArticleText LIKE '%https://mises.org/store/Assets%'



UPDATE Documents SET ProductId = (SELECT ProductId FROM AbleCommerce.dbo.ac_products products WHERE products.[Name] = Documents.title) WHERE ProductId = 0

--SELECT title, NAME, documents.productid FROM Documents INNER JOIN AbleCommerce.dbo.ac_products products ON products.ProductId = documents.ProductId
 
UPDATE Documents SET Description = REPLACE(Description,'xdvipdfmx (0.4)','') WHERE Description LIKE '%xdvipdfmx%'
UPDATE Documents SET Description = REPLACE(Description,'Adobe Acrobat 8.1 Paper Capture Plug-in','') WHERE Description LIKE '%Adobe Acrobat 8.1 Paper Capture Plug-in%'
UPDATE Documents SET Description = REPLACE(Description,'Acrobat Distiller 5.0.5 (Windows)','') WHERE Description LIKE '%Acrobat Distiller 5.0.5 (Windows)%'

UPDATE Documents SET Description = REPLACE(Description,'Acrobat Distiller 9.0.0 (Windows)','') WHERE Description LIKE '%Acrobat Distiller 9.0.0 (Windows)%'
UPDATE Documents SET Description = REPLACE(Description,'Acrobat PDFWriter 5.0 for Windows','') WHERE Description LIKE '%Acrobat PDFWriter 5.0 for Windows%'
UPDATE Documents SET Description = REPLACE(Description,'Acrobat Distiller 5.0 (Windows)','') WHERE Description LIKE '%Acrobat Distiller 5.0 (Windows)%'



update Documents
set Keywords = searchkeywords
from AbleCommerce..ac_Products  
where 
(Keywords IS NULL OR Keywords = '' )AND 
searchkeywords IS NOT NULL and
Documents.ProductId = AbleCommerce..ac_Products.ProductId


update AbleCommerce..ac_Products  
set searchkeywords = Keywords
from Documents d 
where 
searchkeywords IS NULL AND 
d.ProductId = AbleCommerce..ac_Products.ProductId

update Documents
set ISBN = ModelNumber
from AbleCommerce..ac_Products  
where 
ModelNumber IS NOT NULL and
Documents.ProductId = AbleCommerce..ac_Products.ProductId

UPDATE MediaAlternateFormat SET URL = replace(URL, '&hd=1','')


END
GO
/****** Object:  StoredProcedure [dbo].[_SearchDocumentText]    Script Date: 04/05/2012 12:11:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[_SearchDocumentText] @find varchar(500)
AS
BEGIN

      SELECT
          COUNT(1) AS 'Articles'
      FROM
          DailyArticles
      WHERE
          ArticleText LIKE '%' + @find + '%'

---- Study Guide

--      SELECT
--          COUNT(1) AS 'Documents'
--      FROM
--          Documents
--      WHERE
--          Description LIKE '%' + @find + '%'

-- Pages

      SELECT
          COUNT(1) AS 'Pages'
      FROM
          Page
      WHERE
          [Content] LIKE '%' + @find + '%'

END
GO
/****** Object:  StoredProcedure [dbo].[DocumentAdminLinks]    Script Date: 04/05/2012 12:12:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DocumentAdminLinks]
       @SubjectId int = 0 ,
       @AuthorId int = 0 ,
       @MediaType int = 0 ,
       @Source varchar(30) = NULL ,
       @SearchQuery varchar(50) = NULL
AS
SET CONCAT_NULL_YIELDS_NULL OFF

IF @SubjectId > 0
   BEGIN
         SELECT DISTINCT
             Documents.DocumentId ,
             Title ,
             PublicationInformation ,
             URL ,
             Source ,
            -- -- FullText ,
             AuthorFirst + '  ' + AuthorLast AS Author ,
             Author1 ,
             Author2 ,
             MediaType ,
             media.MediaTypeId ,
             MediaType ,
             MediaIconPath ,
             Documents.CreateDate
         FROM
             Documents LEFT JOIN DocumentFiles media
         ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
         ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN DocumentAuthors
         ON  DocumentAuthors.AuthorId = Documents.Author1 LEFT JOIN DocumentSubjectLink
         ON  DocumentSubjectLink.DocumentId = Documents.DocumentId
         WHERE
             SubjectId = @SubjectId
         ORDER BY
             CreateDate DESC ,
             Documents.DocumentId DESC
--Documents.Documents.DocumentId DESC

         SELECT
             Subject
         FROM
             DocumentSubjects
         WHERE
             SubjectId = @SubjectId

         SELECT TOP 10
             Subject ,
             SubjectID
         FROM
             DocumentSubjects
         WHERE
             CategoryId = ( SELECT
                                CategoryId
                            FROM
                                DocumentSubjects
                            WHERE
                                SubjectId = @SubjectId )

   END
ELSE
   IF @AuthorId > 0
      BEGIN
            SELECT DISTINCT
                Documents.DocumentId ,
                Title ,
                PublicationInformation ,
                media.URL ,
                Source ,
             --   -- FullText ,
                AuthorFirst + '  ' + AuthorLast AS Author ,
                Author1 ,
                Author2 ,
                MediaType ,
                media.MediaTypeId ,
                MediaType ,
                MediaIconPath ,
                media.CreateDate
            FROM
                Documents LEFT JOIN DocumentFiles media
            ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
            ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN
            DocumentAuthors
            ON  DocumentAuthors.AuthorId = Documents.Author1 LEFT JOIN
            DocumentSubjectLink
            ON  DocumentSubjectLink.DocumentId = Documents.DocumentId
            WHERE
                ( Author1 = @AuthorId OR Author2 = @AuthorId )
            ORDER BY
                CreateDate DESC ,
                Documents.DocumentId DESC

            SELECT
                AuthorFirst + '  ' + AuthorLast AS Author
            FROM
                DocumentAuthors
            WHERE
                DocumentAuthors.AuthorId = @AuthorId

      END
   ELSE
      IF @MediaType > 0
         SELECT DISTINCT
             Documents.DocumentId ,
             Title ,
             PublicationInformation ,
             URL ,
             Source ,
             -- FullText ,
             AuthorFirst + '  ' + AuthorLast AS Author ,
             Author1 ,
             Author2 ,
             MediaType ,
             media.MediaTypeId ,
             MediaType ,
             MediaIconPath ,
             media.CreateDate
         FROM
             Documents LEFT JOIN DocumentFiles media
         ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
         ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN DocumentAuthors
         ON  DocumentAuthors.AuthorId = Documents.Author1 LEFT JOIN DocumentSubjectLink
         ON  DocumentSubjectLink.DocumentId = Documents.DocumentId
         WHERE
             media.MediaTypeId = @MediaType
         ORDER BY
             CreateDate DESC ,
             Documents.DocumentId DESC
      ELSE
         IF LEN(@SOURCE) > 0
            SELECT DISTINCT
                Documents.DocumentId ,
                Title ,
                PublicationInformation ,
                URL ,
                Source ,
                -- FullText ,
                AuthorFirst + '  ' + AuthorLast AS Author ,
                Author1 ,
                Author2 ,
                MediaType ,
                media.MediaTypeId ,
                MediaType ,
                MediaIconPath ,
                media.CreateDate
            FROM
                Documents LEFT JOIN DocumentFiles media
            ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
            ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN
            DocumentAuthors
            ON  DocumentAuthors.AuthorId = Documents.Author1 LEFT JOIN
            DocumentSubjectLink
            ON  DocumentSubjectLink.DocumentId = Documents.DocumentId
            WHERE
                Source = @SOURCE
            ORDER BY
                CreateDate DESC ,
                Documents.DocumentId DESC
         ELSE
            IF LEN(@SearchQuery) > 0
               SELECT DISTINCT
                   Documents.DocumentId ,
                   Title ,
                   PublicationInformation ,
                   URL ,
                   Source ,
                   -- FullText ,
                   AuthorFirst + '  ' + AuthorLast AS Author ,
                   Author1 ,
                   Author2 ,
                   MediaType ,
                   media.MediaTypeId ,
                   MediaType ,
                   MediaIconPath ,
                   Documents.CreateDate
               FROM
                   Documents LEFT JOIN DocumentFiles media
               ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
               ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN
               DocumentAuthors
               ON  DocumentAuthors.AuthorId = Documents.Author1 LEFT JOIN
               DocumentSubjectLink
               ON  DocumentSubjectLink.DocumentId = Documents.DocumentId
               WHERE
                   Title LIKE '%' + @SearchQuery + '%' OR AuthorFirst LIKE '%' +
                   @SearchQuery + '%' OR AuthorLast LIKE '%' + @SearchQuery + '%' OR
                   PublicationInformation LIKE '%' + @SearchQuery + '%'
               ORDER BY
                   CreateDate DESC ,
                   Documents.DocumentId DESC
            ELSE
               SELECT DISTINCT
                   Documents.DocumentId ,
                   Title ,
                   PublicationInformation ,
                   URL ,
                   Source ,
                   -- FullText ,
                   AuthorFirst + '  ' + AuthorLast AS Author ,
                   Author1 ,
                   Author2 ,
                   MediaType ,
                   media.MediaTypeId ,
                   MediaType ,
                   MediaIconPath ,
                   Documents.CreateDate
               FROM
                   Documents LEFT JOIN DocumentFiles media
               ON  media.DocumentId = Documents.DocumentId LEFT JOIN DocumentMediaType
               ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN
               DocumentAuthors
               ON  DocumentAuthors.AuthorId = Documents.Author1 LEFT JOIN
               DocumentSubjectLink
               ON  DocumentSubjectLink.DocumentId = Documents.DocumentId
               ORDER BY
                   CreateDate DESC ,
                   Documents.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentDelete]    Script Date: 04/05/2012 12:12:12 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[DocumentDelete] @DocumentId int
AS
DELETE  FROM
        DocumentFiles
WHERE
        DocumentId = @DocumentId
DELETE  FROM
        DocumentSubjectLink
WHERE
        DocumentId = @DocumentId
DELETE  FROM
        Documents
WHERE
        DocumentId = @DocumentId
GO
/****** Object:  StoredProcedure [dbo].[DocumentAuthorsGetList]    Script Date: 04/05/2012 12:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentAuthorsGetList] @ContentType varchar(30) = NULL
AS
SET NOCOUNT ON ;
SET CONCAT_NULL_YIELDS_NULL OFF

IF @ContentType = 'DailyArticles'
   SELECT DISTINCT
       da.AuthorId ,
       AuthorLast ,
       da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
       ,Photo
   FROM
       DocumentAuthors da INNER JOIN DailyArticles
   ON  DailyArticles.AuthorId = da.AuthorId
   WHERE
       DailyArticles.ShowArticle = 1
   ORDER BY
       AuthorLast
ELSE
   IF @ContentType = 'Literature'
      SELECT DISTINCT
          da.AuthorId ,
          AuthorLast ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
          ,Photo
      FROM
          DocumentAuthors da INNER JOIN Documents
      ON  da.AuthorId = Documents.Author1
      WHERE
          Documents.Display = 1
      ORDER BY
          AuthorLast
   ELSE
      IF @ContentType = 'Media'
         SELECT DISTINCT
             da.AuthorId ,
             AuthorLast ,
             da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
             ,Photo
         FROM
             DocumentAuthors da INNER JOIN Documents
         ON  da.AuthorId = Documents.Author1 INNER JOIN DocumentFiles
         ON  DocumentFiles.DocumentId = Documents.DocumentId
         WHERE
             Documents.Display = 1
         ORDER BY
             AuthorLast
      ELSE
         IF @ContentType = 'Quotes'
            SELECT DISTINCT
                da.AuthorId ,
                AuthorLast ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
                ,Photo
            FROM
                DocumentAuthors da INNER JOIN [Quotes]
            ON  da.[AuthorId] = [Quotes].[AuthorId]
            ORDER BY
                AuthorLast
                
	ELSE
         IF @ContentType = 'LastNameFirst'
            SELECT DISTINCT
                da.AuthorId ,
                AuthorLast ,
                da.authorLast  + ', ' + da.authorFirst + ' ' + da.authorMiddle AS 'AuthorName'
                ,Photo
            FROM
                DocumentAuthors da            
            ORDER BY
                AuthorLast
      
                
         ELSE
            SELECT
                da.AuthorId ,
                AuthorLast ,
                da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS 'AuthorName'
                ,Photo
            FROM
                DocumentAuthors da
            ORDER BY
                AuthorLast
GO
/****** Object:  StoredProcedure [dbo].[DocumentAuthorsGetDetail]    Script Date: 04/05/2012 12:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentAuthorsGetDetail]
       @ArticleId int = 0 ,
       @AuthorId int = 0
AS
BEGIN

      SET CONCAT_NULL_YIELDS_NULL OFF

      IF @ArticleId > 0
         SELECT
             DocumentAuthors.AuthorId ,
             DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
             DocumentAuthors.AuthorLast AS Author ,
             Photo ,
             BioText ,
             ( SELECT TOP 1
                   ArticleId
               FROM
                   DailyArticles
               WHERE
                   AuthorId = DocumentAuthors.AuthorId
               ORDER BY
                   ArticleId DESC ) AS LatestArticleId
         FROM
             DocumentAuthors INNER JOIN DailyArticles
         ON  DailyArticles.AuthorId = DocumentAuthors.AuthorId
         WHERE
             DailyArticles.ArticleId = @ArticleId             
         UNION ALL
         SELECT
             DocumentAuthors.AuthorId ,
             DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
             DocumentAuthors.AuthorLast AS Author ,
             Photo ,
             BioText ,
             ( SELECT TOP 1
                   ArticleId
               FROM
                   DailyArticles
               WHERE
                   AuthorId = DocumentAuthors.AuthorId
               ORDER BY
                   ArticleId DESC ) AS LatestArticleId
         FROM
             DocumentAuthors INNER JOIN DailyArticles
         ON  DailyArticles.
             CoAuthorId = DocumentAuthors.AuthorId
         WHERE
             DailyArticles.ArticleId = @ArticleId
             
      ELSE
         SELECT
             AuthorId ,
             DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
             DocumentAuthors.AuthorLast AS Author ,
             Photo ,
             BioText
         FROM
             DocumentAuthors
         WHERE
             AuthorId = @AuthorId



END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetFullTextFeed]    Script Date: 04/05/2012 12:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticlesGetFullTextFeed]       
AS
SET CONCAT_NULL_YIELDS_NULL OFF

 SELECT TOP 5
          ArticleId ,
          Title ,
          GUID ,
          Photo ,
          da.authorId ,
          ArticleText ,
          dateposted ,
          description ,
          headline ,
          featured ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS AuthorName
      FROM
          DailyArticles JOIN DocumentAuthors da
      ON  da.AuthorId = DailyArticles.AuthorId
      WHERE
          DailyArticles.ShowArticle = 1
                   -- AND ( DailyArticles.AuthorId = @AuthorId
                --          OR DailyArticles.CoAuthorId = @AuthorId
                --        )
      ORDER BY
          Headline DESC ,
          DailyArticles.DatePosted DESC
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetFeed]    Script Date: 04/05/2012 12:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticlesGetFeed]
       @AuthorId int = 0
--@SortField varchar(50) = 'ORDER BY authorLast, dateposted DESC'

AS
SET CONCAT_NULL_YIELDS_NULL OFF

IF @AuthorId > 0
   SELECT TOP 100
       ArticleId ,
       Title ,
       GUID ,
       Photo ,
       dateposted ,
       da.authorId ,
       description ,
       headline ,
       featured ,
       da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS AuthorName
   FROM
       DailyArticles JOIN DocumentAuthors da
   ON  da.AuthorId = DailyArticles.AuthorId
   WHERE
       DailyArticles.ShowArticle = 1 AND ( DailyArticles.AuthorId = @AuthorId OR
                                           DailyArticles.CoAuthorId = @AuthorId )
   ORDER BY
       DailyArticles.DatePosted DESC
ELSE   
      SELECT TOP 30
          ArticleId ,
          Title ,
          GUID ,
          Photo ,
          da.AuthorId ,
          dateposted ,
          description ,
          headline ,
          featured ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS AuthorName
      FROM
          DailyArticles JOIN DocumentAuthors da
      ON  da.AuthorId = DailyArticles.AuthorId
      WHERE
          DailyArticles.ShowArticle = 1
      ORDER BY
          DailyArticles.DatePosted DESC
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesDeleteByArticleId]    Script Date: 04/05/2012 12:12:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DailyArticlesDeleteByArticleId] @ArticleId int
AS
BEGIN
      DELETE  FROM
              DailyArticles
      WHERE
              ArticleId = @ArticleId
END
GO
/****** Object:  View [dbo].[DailyView]    Script Date: 04/05/2012 12:07:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DailyView]
AS
SELECT     dbo.DailyArticles.ArticleId, dbo.DailyArticles.DatePosted, dbo.DailyArticles.Headline,dbo.DailyArticles.Description, dbo.DailyArticles.Title, da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author, da.AuthorId, dbo.DailyArticles.CoAuthorId, 
                      'http://images.mises.org/DailyArticleImages/' + CAST(dbo.DailyArticles.ArticleId AS varchar(MAX)) + '.jpg' AS ImageURL, 
                      'http://images.mises.org/DailyArticleBigImages/' + CAST(dbo.DailyArticles.ArticleId AS varchar(MAX)) + '.jpg' AS PhotoUrl, 
                      '/daily/' + CAST(dbo.DailyArticles.ArticleId AS varchar(MAX)) AS NavigateUrl, DATENAME(month, dbo.DailyArticles.DatePosted) AS Month
FROM         dbo.DailyArticles INNER JOIN
                      dbo.DocumentAuthors AS da ON da.AuthorId = dbo.DailyArticles.AuthorId
WHERE     (dbo.DailyArticles.ShowArticle = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1[56] 3) )"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3) )"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DailyArticles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 192
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "da"
            Begin Extent = 
               Top = 6
               Left = 230
               Bottom = 114
               Right = 381
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DailyView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DailyView'
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleUpdateArticleOrder]    Script Date: 04/05/2012 12:12:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[DailyArticleUpdateArticleOrder]
(
 @ArticleId int ,
 @DisplayOrder int )
AS
UPDATE
    DailyArticles
SET
    DisplayOrder = @DisplayOrder
WHERE
    ArticleId = @ArticleId
GO
/****** Object:  StoredProcedure [dbo].[DailyArticleUpdate]    Script Date: 04/05/2012 12:12:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticleUpdate]
(
 @ArticleId int ,
 @Title varchar(255) ,
 @Description varchar(1500) ,
 @ArticleText varchar(max) ,
 @ShowArticle bit ,
 @Featured bit ,
 @Headline bit ,
 @AuthorId int = 0 ,
 @CoAuthorId int = 0 ,
      --@DisplayOrder INT = 0,
 @PhotoHeight int = 0 ,
 @PhotoURL varchar(255) = NULL ,
 @DatePosted smalldatetime = GETDATE ,
 @EditBy varchar(75) = NULL )
AS
UPDATE
    DailyArticles
SET
    Title = @Title ,
    [Description] = @Description ,
    ArticleText = @ArticleText ,
    DatePosted = @Dateposted ,
    ShowArticle = @ShowArticle ,
    Featured = @Featured ,
    Headline = @Headline ,
    CoAuthorId = @CoAuthorId ,
    AuthorId = @AuthorId ,
--            DisplayOrder = @DisplayOrder,
  --  ThumbnailUrl = @PhotoURL ,
    PhotoHeight = @PhotoHeight ,
    EditDate = GETDATE() ,
    EditBy = @EditBy
WHERE
    ( ArticleId = @ArticleId )

DECLARE @RC int
DECLARE @RevisionId int
DECLARE @DocumentGUID uniqueidentifier

SET @DocumentGUID = ( SELECT
                          dbo.DailyArticles.GUID
                      FROM
                          dbo.DailyArticles
                      WHERE
                          ArticleId = @ArticleId )
EXECUTE @RC = [Mises].[dbo].[RevisionCreate] @RevisionId OUTPUT , @DocumentGUID ,
@ArticleText , @EditBy
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesTopAuthors]    Script Date: 04/05/2012 12:12:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DocumentGetTopAuthors
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DailyArticlesTopAuthors]
AS
BEGIN
      SELECT TOP 60
          d.AuthorId ,
          da.AuthorLast ,
          COUNT(1) AS 'Count'
      FROM
          [DailyArticles] d JOIN [DocumentAuthors] da
      ON  d.[AuthorId] = da.[AuthorId] OR d.[CoAuthorId] = da.[AuthorId]
      GROUP BY
          d.AuthorId ,
          da.AuthorLast
      ORDER BY
          COUNT DESC
END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesSummaryByDate]    Script Date: 04/05/2012 12:12:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DailyArticlesSummaryByDate]
@DatePosted DATETIME
AS
BEGIN

      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF 
    
    -- Get Headline Article

      SELECT
          DailyArticles.ArticleId ,
          DailyArticles.ArticleId ,
          DailyArticles.Title ,
          DailyArticles.Description ,
          DailyArticles.AuthorId ,
          DailyArticles.DisplayOrder ,
          DailyArticles.Headline ,
          DailyArticles.Featured ,
          DailyArticles.CoAuthorId ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
          da2.authorFirst + ' ' + da2.authorMiddle + ' ' + da2.authorLast AS CoAuthor ,
          DailyArticles.PhotoURL ,
          DailyArticles.DatePosted ,
          DailyArticles.PhotoHeight
      FROM
          dbo.DailyArticles(NOLOCK) JOIN dbo.DocumentAuthors da
      ON  da.AuthorId = DailyArticles.AuthorId LEFT JOIN dbo.DocumentAuthors da2
      ON  da2.AuthorId = DailyArticles.CoAuthorId
      WHERE
          DailyArticles.ShowArticle = 1
          AND DATEDIFF(dd, dbo.DailyArticles.DatePosted,@DatePosted) = 0
                ORDER BY
          Headline DESC
    --CASE WHEN DailyArticles.Featured = 1 THEN Featured END,     
    --CASE WHEN DailyArticles.Headline = 1 THEN Headline END  


END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetTodaysSummary]    Script Date: 04/05/2012 12:12:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DailyArticlesGetTodaysSummary]
AS
BEGIN

      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF 
    
    -- Get Headline Article

      SELECT
          DailyArticles.ArticleId ,
          DailyArticles.ArticleId ,
          DailyArticles.Title ,
          DailyArticles.Description ,
          DailyArticles.AuthorId ,
          DailyArticles.DisplayOrder ,
          DailyArticles.Headline ,
          DailyArticles.Featured ,
          DailyArticles.CoAuthorId ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
          da2.authorFirst + ' ' + da2.authorMiddle + ' ' + da2.authorLast AS CoAuthor ,
          DailyArticles.PhotoURL ,
          DailyArticles.DatePosted ,
          DailyArticles.PhotoHeight
      FROM
          dbo.DailyArticles(NOLOCK) JOIN dbo.DocumentAuthors da
      ON  da.AuthorId = DailyArticles.AuthorId LEFT JOIN dbo.DocumentAuthors da2
      ON  da2.AuthorId = DailyArticles.CoAuthorId
      WHERE
          ( DailyArticles.Featured = 1 OR Headline = 1 ) AND DailyArticles.ShowArticle =
          1
      ORDER BY
          Headline DESC
    --CASE WHEN DailyArticles.Featured = 1 THEN Featured END,     
    --CASE WHEN DailyArticles.Headline = 1 THEN Headline END  


END
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetRecent]    Script Date: 04/05/2012 12:12:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DailyArticlesGetRecent]
AS
BEGIN

      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF 

	-- Daily Articles
      SELECT TOP 4
          DailyArticles.ArticleId ,
          DailyArticles.Title ,
          da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS AuthorName ,
          DailyArticles.PhotoURL ,
          DailyArticles.AuthorId ,
          DailyArticles.PhotoHeight ,
          DatePosted
      FROM
          DailyArticles WITH ( NOLOCK ) INNER JOIN DocumentAuthors AS da
      ON  da.AuthorId = DailyArticles.AuthorId
      WHERE
          DailyArticles.ShowArticle = 1 AND dbo.DailyArticles.Featured = 0 AND dbo.
          DailyArticles.Headline = 0
      ORDER BY
          DailyArticles.DatePosted DESC
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentsGetBiographies]    Script Date: 04/05/2012 12:12:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentsGetBiographies]
AS
BEGIN
      SELECT
          Title ,
          Documents.DocumentId
      FROM
          Documents INNER JOIN [DocumentSubjectLink] dsl
      ON  dsl.[DocumentId] = documents.[DocumentId]
      WHERE
          SubjectId = 135
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentSave]    Script Date: 04/05/2012 12:12:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DocumentSave]
       @DocumentId int = 0 ,
       @Display bit ,
       @Featured bit ,
       @Title varchar(255) ,
       @Author1 int ,
       @Author2 int ,
       @PublicationInformation varchar(8000) ,
       -- @FullText bit ,
       @Source varchar(100) ,
       --@Description varchar(max) ,
       @Length int = 0 ,
       @CategoryId int ,
       @ProductId int ,
       @ParentCategoryId int ,
       @GrandParentCategoryId int ,
       @Description varchar(max) ,
       @Keywords varchar(350) ,
       @Identifier uniqueidentifier = '00000000-0000-0000-0000-000000000000'
AS
IF @Identifier = '00000000-0000-0000-0000-000000000000'
   SET @Identifier = NEWID()


IF @DocumentId > 0 -- Addding New Item
   BEGIN

         UPDATE
             Documents
         SET
             Display = @Display ,
             Title = @Title ,
             --Featured = @Featured ,
             Author1 = @Author1 ,
             Author2 = @Author2 ,
             PublicationInformation = @PublicationInformation ,
             --[FullText] = @-- FullText ,
             Source = @Source ,
             --Description = @Description ,
             CategoryId = @CategoryId ,
             ProductId = @ProductId ,
             Description = @Description ,
             Keywords = @Keywords ,
             EditDate = GETDATE()
         WHERE
             DocumentId = @DocumentId

         DELETE  FROM
                 DocumentSubjectLink
         WHERE
                 DocumentId = @DocumentId

   END
ELSE
   BEGIN

         INSERT INTO
             Documents
             (
               [GUID] ,
               Display ,               
               Title ,
               Author1 ,
               Author2 ,
               PublicationInformation ,
               --[FullText] ,
               Source ,
              -- Description ,
               CategoryId ,
               ProductId,
               Description ,
               Keywords )
         VALUES
             (
               @Identifier ,
               @Display ,
               @Title ,
               @Author1 ,
               @Author2 ,
               @PublicationInformation ,
              -- @-- FullText ,
               @Source ,
              -- @Description ,
               @CategoryId ,
               @ProductId,
               @Description ,
               @Keywords )

         SET @DocumentId = ( SELECT
                                 SCOPE_IDENTITY() )

   END

IF ( @ParentCategoryId > 0 ) AND NOT ( ( @CategoryId = @ParentCategoryId ) OR (
                                                                                @ParentCategoryId
                                                                                =
                                                                                @GrandParentCategoryId )
                                       OR ( @CategoryId = @GrandParentCategoryId ) OR (
                                                                                        @CategoryId
                                                                                        =
                                                                                        0 ) )
   BEGIN
         UPDATE
             dbo.MediaCategory
         SET
             ParentCategory = @ParentCategoryId
         WHERE
             CategoryId = @CategoryId ;

         IF @GrandParentCategoryId > 0
            BEGIN
                  UPDATE
                      dbo.MediaCategory
                  SET
                      ParentCategory = @GrandParentCategoryId
                  WHERE
                      CategoryId = @ParentCategoryId ;
            END
   END

SELECT
    @DocumentId
GO
/****** Object:  StoredProcedure [dbo].[Documents2UpdateFileURL]    Script Date: 04/05/2012 12:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[Documents2UpdateFileURL] 
	-- Add the parameters for the stored procedure here
	@OldPath varchar(max), 
	@NewPath varchar(max)
AS
BEGIN
	
	update DocumentFiles SET URL = replace(URL,@OldPath,@NewPath) WHERE URL = @OldPath
	
END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetSubjects]    Script Date: 04/05/2012 12:12:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentGetSubjects] 
	-- Add the parameters for the stored procedure here
       @DocumentId int = 0
--	,@GUID uniqueidentifier = null

AS
BEGIN
      SET NOCOUNT ON ;

      SELECT
          DocumentSubjects.SubjectId ,
          DocumentSubjects.ShortSubject
      FROM
          DocumentSubjects WITH ( NOLOCK ) JOIN [DocumentSubjectLink]
      ON  [DocumentSubjects].[SubjectId] = [DocumentSubjectLink].[SubjectID]
      WHERE
          DocumentSubjectLink.[DocumentId] = @DocumentId --OR [DocumentSubjectLink].[GUID] = @GUID

END
GO
/****** Object:  StoredProcedure [dbo].[DocumentsUpdateFileURL]    Script Date: 04/05/2012 12:12:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David
-- ALTER date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DocumentsUpdateFileURL] 	
	@OldPath varchar(max), 
	@NewPath varchar(max)
AS
BEGIN

UPDATE DocumentFiles
SET
	URL = replace(URL, @OldPath, @NewPath)
WHERE
	URL = @OldPath
	
END
GO
/****** Object:  View [dbo].[DocumentView]    Script Date: 04/05/2012 12:07:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--{{region:create_object}}

CREATE VIEW [dbo].[DocumentView]
AS



SELECT DISTINCT
		media.FileId,
       Documents.DocumentId ,
       Documents.Title ,
       Documents.Source ,
       ISNULL(Documents.GUID,NEWID()) AS 'GUID',
       Documents.Description ,
       Documents.PublicationInformation ,
       media.URL ,
       DocumentMediaType.MIMEtype ,              
       authorFirst + ' ' + ISNULL(authorMiddle,'') + ' ' + authorLast AS 'Author',
       Documents.Author1,
       Documents.Author2,
       Documents.EditDate,
       0 as FullText,
--       dsj.SubjectID,
       Documents.CreateDate ,
       media.fileSize ,
       media.duration ,
       media.MediaTypeId,
       (SELECT TOP 1 Description FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreDescription,
	  -- metaImage,        
    ISNULL(
    REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId),'~','/store/'),
    'media/poster/' + CAST(Documents.DocumentId as varchar(MAX))
    )  AS CoverImage, 
    Documents.ProductId
       
   FROM Documents  LEFT JOIN DocumentFiles AS media
   ON  media.DocumentId = Documents.DocumentId  LEFT JOIN DocumentAuthors
   ON  Documents.Author1 = DocumentAuthors.AuthorId  
--   LEFT JOIN [DocumentSubjectLink] dsj    ON  dsj.[DocumentId] = Documents.[DocumentId]  
LEFT JOIN   DocumentMediaType
   ON  DocumentMediaType.MediaTypeID = media.MediaTypeId
   WHERE
       ( Documents.Display = 1 )-- AND media.MediaTypeId = @MediaType
       AND dbo.DocumentMediaType.IsMedia =0
   --ORDER BY
       --Documents.CreateDate DESC ,
       --Documents.DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetNewLiterature]    Script Date: 04/05/2012 12:12:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentGetNewLiterature]
AS
BEGIN

      SET CONCAT_NULL_YIELDS_NULL OFF

      SELECT TOP 7
          Documents.DocumentId ,
          Documents.Title ,
          Documents.PublicationInformation AS Description ,
          URL = CASE media.URL + ''
                  WHEN '' THEN '/resources/' + CAST(Documents.DocumentId AS varchar(max))
                  ELSE media.URL
                END ,
          DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
          DocumentAuthors.AuthorLast AS Author ,
          Documents.CreateDate AS CreateDate ,
          MediaIconPath = CASE DocumentMediaType.MediaIconPath + ''
                            WHEN '' THEN '/images/Icons/html.png'
                            ELSE DocumentMediaType.MediaIconPath
                          END
      FROM
          dbo.Documents WITH ( NOLOCK ) LEFT JOIN dbo.DocumentFiles media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  media.MediaTypeId = DocumentMediaType.MediaTypeID
      WHERE
          Documents.Display = 1 AND media.URL <> '' AND DocumentMediaType.IsMedia = 0
      ORDER BY
          documents.CreateDate DESC

END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetKeywords]    Script Date: 04/05/2012 12:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- ALTER date: 
-- Description:	
-- =============================================
CREATE  PROCEDURE [dbo].[DocumentGetKeywords] 
	-- Add the parameters for the stored procedure here
       @DocumentId int
AS
BEGIN
      DECLARE @Keywords varchar(max)


      SET @Keywords = ''
      SELECT
          @Keywords = @Keywords + CASE
                                       WHEN LEN(@Keywords) > 0 THEN ','
                                       ELSE ''
                                  END + [ShortSubject]
      FROM
          DocumentSubjects ds JOIN DocumentSubjectLink dsl
      ON  dsl.SubjectId = ds.SubjectId
      WHERE
          dsl.DocumentId = @DocumentId
      SELECT
          @Keywords AS Keywords

END
GO
/****** Object:  StoredProcedure [dbo].[DocumentGetFeed]    Script Date: 04/05/2012 12:12:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DocumentGetFeed]
       @AuthorId int = 0 ,
       @SubjectId int = 0,
       @MediaType INT =0,
       @Limit INT = 5000,
       @SearchQuery VARCHAR(150) = null
AS
SET CONCAT_NULL_YIELDS_NULL OFF

IF @Limit > 0
	SET ROWCOUNT @Limit

IF @AuthorId > 0
   SELECT DocumentView.* FROM DocumentView
   WHERE
       ( Author1 = @AuthorId OR Author2 = @AuthorId )		
   ORDER BY
       CreateDate DESC ,
       DocumentId DESC
ELSE IF @SubjectId > 0
   SELECT DocumentView.* FROM DocumentView
   LEFT JOIN [DocumentSubjectLink] dsj   ON  dsj.[DocumentId] = DocumentView.[DocumentId]  
   WHERE
       dsj.[SubjectID] = @SubjectId
   ORDER BY
       CreateDate DESC ,
       DocumentView.DocumentId DESC
       
ELSE IF @MediaType > 0
   
     SELECT DocumentView.* FROM DocumentView
   WHERE
       MediaTypeId = @MediaType
   ORDER BY
       CreateDate DESC ,
       DocumentId DESC
   
ELSE IF LEN(@SearchQuery) > 0
  
  SELECT DocumentView.* FROM DocumentView
   WHERE
       (Title LIKE '%' + @SearchQuery + '%' 
       OR Author LIKE '%' + @SearchQuery + '%' 
       OR Description LIKE '%' + @SearchQuery + '%' 
       OR dbo.DocumentView.StoreDescription LIKE '%' + @SearchQuery + '%'
       OR dbo.DocumentView.Description LIKE '%' + @SearchQuery + '%'
       )
   ORDER BY
       CreateDate DESC ,
       DocumentId DESC       
ELSE
    SELECT DocumentView.* FROM DocumentView  
   ORDER BY
       CreateDate DESC ,
       DocumentId DESC
GO
/****** Object:  StoredProcedure [dbo].[DailyArticlesGetImageGallery]    Script Date: 04/05/2012 12:12:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[DailyArticlesGetImageGallery]
@AuthorId int = 0 ,
@SearchQuery varchar(100) = '',
@Limit int = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF

IF (@Limit > 0)
SET ROWCOUNT @Limit

IF @AuthorId > 0
SELECT ArticleId,DatePosted,
	   Title,
	   Author,
	   AuthorId,
	   ImageURL,
	   PhotoUrl,
	   NavigateUrl,
	   [Month] FROM DailyView WHERE @AuthorId = AuthorId OR @AuthorId = dbo.DailyView.CoAuthorId
ORDER BY Headline DESC ,[DatePosted] DESC

ELSE IF LEN(@SearchQuery) > 2

SELECT ArticleId,DatePosted,
	   Title,
	   Author,
	   AuthorId,
	   ImageURL,
	   PhotoUrl,
	   NavigateUrl,
	   [Month] FROM DailyView WHERE 
(
description LIKE @SearchQuery
OR
title LIKE @SearchQuery
OR AuthorId IN (SELECT ArticleId FROM dbo.DailyArticles WHERE FREETEXT(ArticleText,@SearchQuery))
)
ORDER BY Headline DESC ,[DatePosted] DESC

ELSE

SELECT ArticleId,DatePosted,
	   Title,
	   Author,
	   AuthorId,
	   ImageURL,
	   PhotoUrl,
	   NavigateUrl,
	   [Month] FROM DailyView
ORDER BY Headline DESC ,[DatePosted] DESC
GO
