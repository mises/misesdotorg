USE [Mises]
GO
/****** Object:  StoredProcedure [dbo].[PeriodicalsGetArchives]    Script Date: 05/24/2011 16:57:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
ALTER  PROCEDURE [dbo].[PeriodicalsGetArchives]
       @PeriodicalId int ,
       @title varchar(50) = 'RandomString' ,
       @author varchar(50) = 'RandomString' ,
       @subject varchar(50) = 'RandomString' ,
       @volume varchar(50) = 'RandomString'
AS
BEGIN

      IF @title = 'RandomString' AND @author = 'RandomString' AND @subject =
      'RandomString' AND @volume = 'RandomString'
         BEGIN

               IF @PeriodicalId = 1
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      freemarket.title ,
                      freemarket.AuthorFirst + ' ' + freemarket.AuthorLast AS AuthorName ,
                      freemarket.AuthorLast ,
                      freemarket.articledate AS DatePosted ,
                      DATENAME(month , freemarket.articledate) AS Month ,
                      '/freemarket_detail.aspx?control=' + CAST(freemarket.control AS
                      varchar(10)) AS URL ,
                      freemarket.[control] AS Id
                  FROM
                      dbo.freemarket
                  ORDER BY
                      freemarket.articledate DESC


-- Season, Volume
               IF @PeriodicalId = 2
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      misesreview.AuthorFirst + ' ' + misesreview.AuthorLast AS AuthorName ,
                      misesreview.AuthorLast ,
                      misesreview.title ,
                      CASE misesreview.issue_season
                        WHEN 1 THEN 'Spring'
                        WHEN 2 THEN 'Summer'
                        WHEN 3 THEN 'Fall'
                        WHEN 4 THEN 'Winter'
                      END + ' ' + CAST(misesreview.issue_year AS varchar(10)) AS Volume ,
                      misesreview.issue_year AS Season ,
                      '/misesreview_detail.aspx?control=' + CAST(misesreview.control AS
                      varchar(10)) AS URL ,
                      misesreview.[control] AS Id
                  FROM
                      dbo.misesreview
                  ORDER BY
                      misesreview.CreateDate DESC ,
                      PeriodicalId DESC

               IF @PeriodicalId = 3
                  SELECT
				      jls.[control] AS Id,
                      @PeriodicalId AS PeriodicalId ,
                      jls.number ,
                      jls.display ,
                      jls.title ,
                      jls.authorFirst1 + ' ' + jls.authorLast1 AS AuthorName ,
                      jls.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(jls.Volume AS varchar(10)) + ' Num. ' + CAST(jls.
                      [number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(jls.Volume AS varchar(10)) AS Season ,
                      '/journals/jls/' + CAST(jls.Volume AS varchar(10)) + '_' + CAST(jls
                      .number AS varchar(10)) + '/' + CAST(jls.Volume AS varchar(10)) +
                      '_' + CAST(jls.number AS varchar(10)) + '_' + CAST(jls.articleNum
                      AS varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.jls
                  ORDER BY
                      jls.Volume DESC ,
                      number DESC ,
                      jls.ArticleNum

               IF @PeriodicalId = 4
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      qjaeDB.number ,
                      qjaeDB.display ,
                      qjaeDB.title ,
                      qjaeDB.authorFirst1 + ' ' + qjaeDB.authorLast1 AS AuthorName ,
                      qjaeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(qjaeDB.Volume AS varchar(10)) + ' Num. ' + CAST(
                      qjaeDB.[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(qjaeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/qjae/pdf/qjae' + CAST(qjaeDB.Volume AS varchar(10)) +
                      '_' + CAST(qjaeDB.number AS varchar(10)) + '_' + CAST(qjaeDB.
                      articleNum AS varchar(10)) + '.pdf' AS URL,
                      [control] AS Id
                  FROM
                      dbo.qjaeDB
                  ORDER BY
                      [qjaeDB].Volume DESC ,
                      [qjaeDB].number DESC ,
                      ArticleNum

               IF @PeriodicalId = 5
                  SELECT
				      raeDB.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      raeDB.number ,
                      raeDB.display ,
                      raeDB.title ,
                      raeDB.authorFirst1 + ' ' + raeDB.authorLast1 AS AuthorName ,
                      raeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(raeDB.Volume AS varchar(10)) + ' Num. ' + CAST(raeDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(raeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/rae/pdf/RAE' + CAST(raeDB.Volume AS varchar(10)) + '_' +
                      CAST(raeDB.number AS varchar(10)) + '_' + CAST(raeDB.articleNum AS
                      varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.raeDB
                  ORDER BY
                      dbo.raeDB.[Volume] DESC ,
                      --number DESC ,
                      ArticleNum

               IF @PeriodicalId = 6
                  SELECT
				      aendb.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      aenDB.number ,
                      aenDB.display ,
                      aenDB.title ,
                      aenDB.authorFirst1 + ' ' + aenDB.authorLast1 AS AuthorName ,
                      aenDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(aenDB.Volume AS varchar(10)) + ' Num. ' + CAST(aenDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(aenDB.Volume AS varchar(10)) AS Season ,
                      '/journals/aen/aen' + CAST(aenDB.Volume AS varchar(10)) + '_' +
                      CAST(aenDB.number AS varchar(10)) + '_' + CAST(aenDB.articleNum AS
                      varchar(10)) + aenDB.[fileType] AS URL
                  FROM
                      dbo.aenDB
                  ORDER BY
                      aenDB.Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 7
                  SELECT
				      scholar.[control] As Id,
                      @PeriodicalId AS PeriodicalId ,
                      [paper_name] + '(' + notation + ')' AS 'Title' ,
                      [author_name] AS AuthorName ,
                      [author_name] AS AuthorLast ,
                      [whenit] AS 'DatePosted' ,
                      '' AS 'Month' ,
                      '/journals/scholar/' + [file_name] AS [url]
                  FROM
                      scholar
                  ORDER BY
                      control DESC

               IF @PeriodicalId = 8
                  SELECT
                      WardLibrary.[control] as Id,
                      @PeriodicalId AS PeriodicalId ,
                      title AS 'Title' ,
                      AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName ,
                      [AuthorLast] AS AuthorLast ,
                      donatedDate AS 'DatePosted' ,
                      '' AS 'Season' ,
                      '' AS 'Volume' ,
                      'book.aspx?Id=' + CAST(CONTROL AS varchar(10)) AS [url]
                  FROM
                      WardLibrary
                  --WHERE
                      --display = 'yes'
                  ORDER BY
                      control DESC




         END
      ELSE -- DO SEARCH
         BEGIN

               IF @title = ''
                  SET @title = 'RandomString'
               IF @author = ''
                  SET @author = 'RandomString'
               IF @subject = ''
                  SET @subject = 'RandomString'
               IF @volume = ''
                  SET @volume = 'RandomString'


               IF @PeriodicalId = 1
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      freemarket.title ,
                      freemarket.AuthorFirst + ' ' + freemarket.AuthorLast AS AuthorName ,
                      freemarket.AuthorLast ,
                      freemarket.articledate AS DatePosted ,
                      DATENAME(month , freemarket.articledate) AS Month ,
                      '/freemarket_detail.aspx?control=' + CAST(freemarket.control AS
                      varchar(10)) AS URL
                  FROM
                      dbo.freemarket
                  WHERE
                      freemarket.title LIKE '%' + @title + '%' OR freemarket.body LIKE
                      '%' + @title + '%' OR freemarket.body LIKE '%' + @subject + '%' OR
                      freemarket.AuthorLast LIKE '%' + @author + '%'
                  ORDER BY
                      freemarket.articledate DESC


-- Season, Volume
               IF @PeriodicalId = 2
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      misesreview.AuthorFirst + ' ' + misesreview.AuthorLast AS AuthorName ,
                      misesreview.AuthorLast ,
                      misesreview.title ,
                      CASE misesreview.issue_season
                        WHEN 1 THEN 'Spring'
                        WHEN 2 THEN 'Summer'
                        WHEN 3 THEN 'Fall'
                        WHEN 4 THEN 'Winter'
                      END + ' ' + CAST(misesreview.issue_year AS varchar(10)) AS Volume ,
                      misesreview.issue_year AS Season ,
                      '/misesreview_detail.aspx?control=' + CAST(misesreview.control AS
                      varchar(10)) AS URL
                  FROM
                      dbo.misesreview
                  WHERE
                      misesreview.title LIKE '%' + @title + '%' OR misesreview.body LIKE
                      '%' + @title + '%' OR misesreview.AuthorLast LIKE '%' + @author +
                      '%' OR misesreview.[authorfirst] LIKE '%' + @author + '%' OR
                      misesreview.[authorfirst] + ' ' + +misesreview.[authorlast] LIKE
                      '%' + @author + '%' OR misesreview.body LIKE '%' + @title + '%' OR
                                                                                         CASE
                                                                                         misesreview
                                                                                         .
                                                                                         issue_season
                                                                                           WHEN 1 THEN 'Spring'
                                                                                           WHEN 2 THEN 'Summer'
                                                                                           WHEN 3 THEN 'Fall'
                                                                                           WHEN 4 THEN 'Winter'
                                                                                         END
                      + ' ' + CAST(misesreview.issue_year AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      misesreview.CreateDate DESC ,
                      PeriodicalId DESC

               IF @PeriodicalId = 3
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      jls.number ,
                      jls.display ,
                      jls.title ,
                      jls.authorFirst1 + ' ' + jls.authorLast1 AS AuthorName ,
                      jls.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(jls.Volume AS varchar(10)) + ' Num. ' + CAST(jls.
                      [number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(jls.Volume AS varchar(10)) AS Season ,
                      '/journals/jls/' + CAST(jls.Volume AS varchar(10)) + '_' + CAST(jls
                      .number AS varchar(10)) + '/' + CAST(jls.Volume AS varchar(10)) +
                      '_' + CAST(jls.number AS varchar(10)) + '_' + CAST(jls.articleNum
                      AS varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.jls
                  WHERE
                      jls.title LIKE '%' + @title + '%' OR jls.AuthorLast1 LIKE '%' +
                      @author + '%' OR jls.AuthorLast2 LIKE '%' + @author + '%' OR jls.
                      authorFirst1 LIKE '%' + @author + '%' OR jls.authorFirst2 LIKE '%'
                      + @author + '%' OR 'Vol. ' + CAST(jls.Volume AS varchar(10)) +
                      ' Num. ' + CAST(jls.[number] AS varchar(10)) LIKE '%' + @volume +
                      '%'
                  ORDER BY
                      jls.Volume DESC ,
                      number DESC ,
                      jls.ArticleNum

               IF @PeriodicalId = 4
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      qjaeDB.number ,
                      qjaeDB.display ,
                      qjaeDB.title ,
                      qjaeDB.authorFirst1 + ' ' + qjaeDB.authorLast1 AS AuthorName ,
                      qjaeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(qjaeDB.Volume AS varchar(10)) + ' Num. ' + CAST(
                      qjaeDB.[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(qjaeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/qjae/pdf/qjae' + CAST(qjaeDB.Volume AS varchar(10)) +
                      '_' + CAST(qjaeDB.number AS varchar(10)) + '_' + CAST(qjaeDB.
                      articleNum AS varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.qjaeDB
                  WHERE
                      qjaeDB.title LIKE '%' + @title + '%' OR qjaeDB.AuthorLast1 LIKE '%'
                      + @author + '%' OR qjaeDB.AuthorLast2 LIKE '%' + @author + '%' OR
                      qjaeDB.authorFirst1 LIKE '%' + @author + '%' OR qjaeDB.authorFirst2
                      LIKE '%' + @author + '%' OR 'Vol. ' + CAST(qjaeDB.Volume AS varchar
                      (10)) + ' Num. ' + CAST(qjaeDB.[number] AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 5
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      raeDB.number ,
                      raeDB.display ,
                      raeDB.title ,
                      raeDB.authorFirst1 + ' ' + raeDB.authorLast1 AS AuthorName ,
                      raeDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(raeDB.Volume AS varchar(10)) + ' Num. ' + CAST(raeDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(raeDB.Volume AS varchar(10)) AS Season ,
                      '/journals/rae/pdf/RAE' + CAST(raeDB.Volume AS varchar(10)) + '_' +
                      CAST(raeDB.number AS varchar(10)) + '_' + CAST(raeDB.articleNum AS
                      varchar(10)) + '.pdf' AS URL
                  FROM
                      dbo.raeDB
                  WHERE
                      raeDB.title LIKE '%' + @title + '%' OR raeDB.AuthorLast1 LIKE '%' +
                      @author + '%' OR raeDB.AuthorLast2 LIKE '%' + @author + '%' OR
                      raeDB.authorFirst1 LIKE '%' + @author + '%' OR raeDB.authorFirst2
                      LIKE '%' + @author + '%' OR 'Vol. ' + CAST(raeDB.Volume AS varchar(
                      10)) + ' Num. ' + CAST(raeDB.[number] AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 6
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      aenDB.number ,
                      aenDB.display ,
                      aenDB.title ,
                      aenDB.authorFirst1 + ' ' + aenDB.authorLast1 AS AuthorName ,
                      aenDB.authorLast1 AS AuthorLast ,
                      'Vol. ' + CAST(aenDB.Volume AS varchar(10)) + ' Num. ' + CAST(aenDB
                      .[number] AS varchar(10)) AS Volume ,
                      'Volume ' + CAST(aenDB.Volume AS varchar(10)) AS Season ,
                      '/journals/aen/aen' + CAST(aenDB.Volume AS varchar(10)) + '_' +
                      CAST(aenDB.number AS varchar(10)) + '_' + CAST(aenDB.articleNum AS
                      varchar(10)) + aenDB.[fileType] AS URL
                  FROM
                      dbo.aenDB
                  WHERE
                      aenDB.title LIKE '%' + @title + '%' OR aenDB.AuthorLast1 LIKE '%' +
                      @author + '%' OR aenDB.AuthorLast2 LIKE '%' + @author + '%' OR
                      aenDB.authorFirst1 LIKE '%' + @author + '%' OR aenDB.authorFirst2
                      LIKE '%' + @author + '%' OR 'Vol. ' + CAST(aenDB.Volume AS varchar(
                      10)) + ' Num. ' + CAST(aenDB.[number] AS varchar(10)) LIKE '%' +
                      @volume + '%'
                  ORDER BY
                      Volume DESC ,
                      number DESC ,
                      ArticleNum

               IF @PeriodicalId = 7
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      [paper_name] + '(' + notation + ')' AS 'Title' ,
                      [author_name] AS AuthorName ,
                      [author_name] AS AuthorLast ,
                      [whenit] AS 'DatePosted' ,
                      '' AS 'Month' ,
                      '/journals/scholar/' + [file_name] AS [url]
                  FROM
                      scholar
                  WHERE
                      [paper_name] LIKE '%' + @title + '%' OR [author_name] LIKE '%' +
                      @author + '%' OR [notation] LIKE '%' + @title + '%'
                  ORDER BY
                      control DESC

               IF @PeriodicalId = 8
                  SELECT
                      @PeriodicalId AS PeriodicalId ,
                      title AS 'Title' ,
                      AuthorFirst + ' ' + AuthorLast + ' ' + coauthors AS AuthorName ,
                      [AuthorLast] AS AuthorLast ,
                      donatedDate AS 'DatePosted' ,
                      '' AS 'Season' ,
                      '' AS 'Volume' ,
                      'book.aspx?Id=' + CAST(CONTROL AS varchar(10)) AS [url]
                  FROM
                      WardLibrary
                  WHERE
                      --display = 'yes' AND 
                      ( WardLibrary.comments LIKE '%' + @title + '%'
                                            OR WardLibrary.title LIKE '%' + @title + '%'
                                            OR WardLibrary.authorlast LIKE '%' + @author
                                            + '%' OR WardLibrary.authorfirst2 LIKE '%' +
                                            @author + '%' OR WardLibrary.authorfirst3
                                            LIKE '%' + @author + '%' OR WardLibrary.
                                            subject LIKE '%' + @subject + '%' OR
                                            AuthorFirst + ' ' + AuthorLast LIKE '%' +
                                            @author + '%' )
                  ORDER BY
                      control DESC


         END

END

--PeriodicalsGetArchives 3

--select * from aen

--[PeriodicalsGetArchives] 6,'Buchanan'
