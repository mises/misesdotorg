USE [Mises]
GO

/****** Object:  Table [dbo].[RAEdb1]    Script Date: 05/24/2011 19:43:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RAEdb1](
	[control] [int] NOT NULL IDENTITY,
	[GUID] [uniqueidentifier] NULL,
	[display] [varchar](3) NULL,
	[volume] [int] NOT NULL,
	[number] [int] NOT NULL,
	[articleNum] [int] NOT NULL,
	[title] [varchar](255) NOT NULL,
	[authorFirst1] [varchar](50) NOT NULL,
	[authorLast1] [varchar](50) NOT NULL,
	[authorFirst2] [varchar](50) NULL,
	[authorLast2] [varchar](50) NULL,
	[link] [varchar](50) NULL,
 CONSTRAINT [PK_raeDB1] PRIMARY KEY CLUSTERED 
(
	[control] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[RAEdb1] ADD  CONSTRAINT [DF_raeDB1_GUID]  DEFAULT (newid()) FOR [GUID]
GO

INSERT INTO [Mises].[dbo].[RAEdb1]
           ([GUID]
           ,[display]
           ,[volume]
           ,[number]
           ,[articleNum]
           ,[title]
           ,[authorFirst1]
           ,[authorLast1]
           ,[authorFirst2]
           ,[authorLast2]
           ,[link])
SELECT [GUID]
      ,[display]
      ,[volume]
      ,[number]
      ,[articleNum]
      ,[title]
      ,[authorFirst1]
      ,[authorLast1]
      ,[authorFirst2]
      ,[authorLast2]
      ,[link]
  FROM [Mises].[dbo].[RAEdb]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_raeDB_GUID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RAEdb] DROP CONSTRAINT [DF_raeDB_GUID]
END

GO

/****** Object:  Table [dbo].[RAEdb]    Script Date: 05/24/2011 19:46:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RAEdb]') AND type in (N'U'))
DROP TABLE [dbo].[RAEdb]
GO

Exec sp_rename 'RAEdb1', 'RAEdb'

GO

