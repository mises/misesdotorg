﻿/*****************************************************************************
Combined database upgrade script for Mises website.
*****************************************************************************/

set nocount on

declare @product_name nvarchar(50)

declare @retcode int

begin try

set @product_name = N'MisesWebSiteDatabase';

begin tran

declare @lock_key sysname
set @lock_key = @product_name + N'_Installation'

exec @retcode = dbo.sp_getapplock
	@Resource = @lock_key
	, @LockMode = N'Exclusive'
	, @LockOwner = N'Transaction'
	, @LockTimeout = N'0'

if @retcode < 0
begin
	raiserror('Concurrent installation detected, cancelling the installation', 16, 1)
end

--SEGMENT the installation script body


--{{region:create_table_if_not_exists}}

if not exists (
	select		*
	from		sys.tables
	where		object_id = object_id(N'dbo.Documents')
)
begin
	create table dbo.Documents (
		[DocumentId] [int] identity(1,1) not null,
		[GUID] [uniqueidentifier] not null,
		[Display] [bit] not null,
		[Featured] [bit] null,
		[Title] [varchar](255) not null,
		[Author1] [int] not null,
		[Author2] [int] null,
		[PubInfo] [varchar](max) null,
		[metaDescription] [varchar](max) null,
		[metaKeywords] [varchar](350) null,
		[metaImage] varbinary(max) null,
		[_oldURL] [varchar](255) null,
		[_oldMediaTypeId] [int] null,
		[FullText] [bit] null,
		[Source] [varchar](100) null,
		[GuideContent] [varchar](max) null,
		[Length] [int] null,
		[CategoryId] [int] null,
		[CreateDate] [datetime] not null,
		[EditDate] [datetime] not null,
		[ProductId] [int] null
	);

-----------------------------------------------------------
-- legacy creation code
-- TODO: move to appropriate sections and make re-runnable when need to change or have time

ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_Documents_MediaCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[MediaCategory] ([CategoryId])
NOT FOR REPLICATION 

ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_Documents_MediaCategory]

ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_StudyGuideAuthors] FOREIGN KEY([Author1])
REFERENCES [dbo].[DocumentAuthors] ([AuthorId])
NOT FOR REPLICATION 

ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_StudyGuideAuthors]

ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_StudyGuideAuthors1] FOREIGN KEY([Author2])
REFERENCES [dbo].[DocumentAuthors] ([AuthorId])
NOT FOR REPLICATION 

ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_StudyGuideAuthors1]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_StudyGuide_GUID]  DEFAULT (newid()) FOR [GUID]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_Documents_Featured]  DEFAULT ((0)) FOR [Featured]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_StudyGuide_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_Documents_EditDate]  DEFAULT (getdate()) FOR [EditDate]

-- end legacy creation code
-----------------------------------------------------------

end


--{{region:create_table_if_not_exists}}

if not exists (
	select		*
	from		sys.tables
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
)
begin
	create table [dbo].[MediaAlternateFormat] (
		[MediaId] [int] identity(1,1) not null,
		[DocumentId] [int] not null,
		[MediaTypeId] [int] not null,
		VolumeOrdinal tinyint not null,
		VolumeComment varchar(50) null,
		[URL] [varchar](500) not null,
		[fileSize] [bigint] not null,
		[duration] [decimal](18, 0) not null,
		[CreateDate] [datetime] not null,
		[Display] [bit] not null,
	);
end



--{{region:verify_table_structure}}

if objectproperty(object_id(N'dbo.Documents'), 'TableHasPrimaryKey') = 0
begin
	alter table					dbo.Documents
	add constraint				PK_Documents
	primary key clustered		(DocumentId);
end

-- eliminating instances whe valid Author1 is not equal to valid Author2

------------------------------------------------------------------
-- one-off update section, delete when versioning is implemented
set nocount on;

update		dbo.Documents
set			Author2 = 0
where		Author1 != 0
			and Author2 = Author1;

update		dbo.Documents
set			Author1 = Author2
			, Author2 = 0
where		Author2 > 0
			and Author1 = 0;
-------------------------------------------------------------------

if not exists (
	select		*
	from		sys.check_constraints
	where		object_id = object_id(N'CHK_Documents_Authors')
				and parent_object_id = object_id(N'dbo.Documents')
)
begin
	alter table		dbo.Documents
	with check
	add constraint	CHK_Documents_Authors
	check			(
						Author1 >= 0
						and Author2 >= 0
						and (
							(
								Author1 = 0
								and Author2 = 0
							)
							or (
								Author1 > 0
								and Author2 != Author1
							)
						)
					);
end

if exists (
	select			*
	from			sys.columns
	where			object_id = object_id(N'dbo.Documents')
					and column_id = columnproperty(object_id, N'metaImage', 'ColumnID')
					and system_type_id = 34
)
begin
	alter table		dbo.Documents
	alter column	metaImage varbinary(max) null;
end



--{{region:verify_table_structure}}

if columnproperty(object_id(N'dbo.MediaAlternateFormat'), N'VolumeOrdinal', 'ColumnID') is null
begin
	alter table		dbo.MediaAlternateFormat
	add				VolumeOrdinal tinyint not null
	constraint		DF_MediaAlternateFormat_VolumeOrdinal default (1);

	-- automatically updating ordinal for duplicate {DocumentId, MediaTypeId} pairs
	exec(N'
	with Duplicates as (
		select			DocumentId
						, MediaTypeId
						, count(*) as cnt
		from			dbo.MediaAlternateFormat
		group by		DocumentId
						, MediaTypeId
		having			count(*) > 1
	)
	, Ordinals as (
		select			t.DocumentId
						, t.MediaTypeId
						, t.MediaId
						, row_number() over (partition by t.DocumentId, t.MediaTypeId order by t.URL) as GuessedVolumeOrdinal
		from			Duplicates d
						, dbo.MediaAlternateFormat t
		where			d.DocumentId = t.DocumentId
						and d.MediaTypeId = t.MediaTypeId
	)
	update		t
	set			t.VolumeOrdinal = o.GuessedVolumeOrdinal
	from		dbo.MediaAlternateFormat t
				, Ordinals o
	where		o.MediaId = t.MediaId
	');
end

if columnproperty(object_id(N'dbo.MediaAlternateFormat'), N'VolumeComment', 'ColumnID') is null
begin
	alter table			dbo.MediaAlternateFormat
	add					VolumeComment varchar(50) null;
end

-- need to drop this indexes in any case but should do it before dropping clustered index
-- to avoid rebuilding then dropping
if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'_dta_index_MediaAlternateFormat_7_1401772051__K2_K3_4_5_6'
)
begin
	drop index dbo.MediaAlternateFormat._dta_index_MediaAlternateFormat_7_1401772051__K2_K3_4_5_6;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'_dta_index_MediaAlternateFormat_7_1401772051__K2_K3'
)
begin
	drop index dbo.MediaAlternateFormat._dta_index_MediaAlternateFormat_7_1401772051__K2_K3;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'_dta_index_MediaAlternateFormat_7_1401772051__K3_K2_4_5_6'
)
begin
	drop index dbo.MediaAlternateFormat._dta_index_MediaAlternateFormat_7_1401772051__K3_K2_4_5_6;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'IDX_MediaAlternateFormat_Display'
)
begin
	drop index dbo.MediaAlternateFormat.IDX_MediaAlternateFormat_Display;
end

if exists (
	-- query below will return 1 row if primary key is on MediaId
	select		*
	from		sys.key_constraints c
				, sys.indexes i
				, sys.index_columns ic1
	where		c.parent_object_id = object_id(N'dbo.MediaAlternateFormat')
				and c.type = 'PK'
				and c.name = N'PK_AlternateMediaFormat'
				and i.index_id = c.unique_index_id
				and i.object_id = c.parent_object_id
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'MediaId', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic1.key_ordinal
				)
)
begin
	alter table			dbo.MediaAlternateFormat
	drop constraint		PK_AlternateMediaFormat;
end

if objectproperty(object_id(N'[dbo].[MediaAlternateFormat]'), 'TableHasClustIndex') = 0
begin
	alter table			dbo.MediaAlternateFormat
	add constraint		UC_AlternateMediaFormat_DocumentIdMediaTypeDisplay
	unique clustered	(DocumentId, MediaTypeId, VolumeOrdinal);
end



if objectproperty(object_id(N'[dbo].[MediaAlternateFormat]'), 'TableHasPrimaryKey') = 0
begin
	alter table					dbo.MediaAlternateFormat
	add constraint				PK_AlternateMediaFormat
	primary key nonclustered	(MediaId);
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'fileSize', 'ColumnID')
)
begin
	alter table		[dbo].[MediaAlternateFormat]
	add  constraint	[DF_MediaAlternateFormat_fileSize]
	default			((0))
	for				[fileSize];
end


if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'duration', 'ColumnID')
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	add  constraint		[DF_MediaAlternateFormat_duration]
	default				((0))
	for					[duration]
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'CreateDate', 'ColumnID')
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	add constraint		[DF_AlternateMediaFormat_CreateDate]
	default				(getdate())
	for					[CreateDate];
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'Display', 'ColumnID')
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	add constraint		[DF_MediaAlternateFormat_Visible]
	default				((1))
	for					[Display];
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'VolumeOrdinal', 'ColumnID')
)
begin
	alter table			dbo.MediaAlternateFormat
	add constraint		DF_MediaAlternateFormat_VolumeOrdinal
	default				(1)
	for					VolumeOrdinal;
end




--{{region:verify_table_indexes}}
if not exists (
	-- query below will return rows if there's a nonclustered key (DocumentId, MediaId, Display)
	-- this index is primarily for counting documents
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
				, sys.index_columns ic3
	where		i.object_id = object_id(N'dbo.MediaAlternateFormat')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'DocumentId', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'MediaTypeId', 'ColumnId')
				and i.object_id = ic3.object_id
				and i.index_id = ic3.index_id
				and ic3.key_ordinal = 3
				and ic3.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic3.key_ordinal
				)
)
begin
	create nonclustered index		IDX_MediaAlternateFormat_DocMedTypeDisplay
	on								dbo.MediaAlternateFormat (
										DocumentId
										, MediaTypeId
										, Display
									);
end

if not exists (
	-- query below will return rows if there's a nonclustered key (DocumentId, MediaId, Display)
	-- this index is primarily for counting documents
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
	where		i.object_id = object_id(N'dbo.MediaAlternateFormat')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'MediaTypeId', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic2.key_ordinal
				)
)
begin
	create nonclustered index		IDX_MediaAlternateFormat_MedTypeDisplay
	on								dbo.MediaAlternateFormat (
										MediaTypeId
										, Display
									);
end

if not exists (
	-- query below will return rows if there's a nonclustered key (DocumentId, MediaId, Display)
	-- this index is primarily for counting documents
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
	where		i.object_id = object_id(N'dbo.MediaAlternateFormat')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'MediaTypeId', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic2.key_ordinal
				)
)
begin
	create nonclustered index		IDX_MediaAlternateFormat_DisplayMedType
	on								dbo.MediaAlternateFormat (
										Display
										, MediaTypeId
									);
end




--{{region:verify_referencial_constraints}}

if not exists (
	select		*
	from		sys.foreign_key_columns
	where		referenced_object_id = object_id(N'[dbo].[DocumentMediaType]')
				and parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
)
begin
	alter table						[dbo].[MediaAlternateFormat]
	with nocheck add constraint		[FK_MediaAlternateFormat_DocumentMediaType]
	foreign key						([MediaTypeId])
	references						[dbo].[DocumentMediaType] ([MediaTypeID]);
end

alter table					[dbo].[MediaAlternateFormat]
nocheck constraint			[FK_MediaAlternateFormat_DocumentMediaType];



--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.DocumentDetails')
)
begin
	drop proc dbo.DocumentDetails;
end


--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaDetails')
)
begin
	drop proc dbo.MediaDetails;
end


--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaGetAuthorsAlpha')
)
begin
	drop proc dbo.MediaGetAuthorsAlpha;
end


--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaGetRandomVideo')
)
begin
	drop proc dbo.MediaGetRandomVideo;
end


--{{region:drop_object_if_exists}}

if exists (
	select		*
	from		sys.procedures
	where		object_id = object_id(N'dbo.util_rethrow_exception')
)
begin
	drop proc dbo.util_rethrow_exception
end


--{{region:drop_object_if_exists}}

if exists (
	select		*
	from		sys.sql_modules
	where		object_id = object_id(N'dbo.SplitStringTable')
				and objectproperty(object_id, 'IsTableFunction') = 1
)
begin
	drop function dbo.SplitStringTable;
end


exec(N'
--{{region:create_object}}

/***************************************************************************************************************************
Author:		Vasily Kabanov
Created:	23/07/2007
Parameters:	@vallist - the string containing a list of individual string values delimited by @delimiter
			@delimiter - the character sequence used to separate individual values in @vallist
Returns:	table (
				val nvarchar(100) not null - non-empty trimmed value
			)
Comment:	The utility function converts a delimited list of values into table format.
			Individual values are trimmed from both ends and empty values are removed from the result.
			Hence
				select * from dbo.SplitStringTable(''one,two,three'', '','')
			returns the same result as  
				select * from dbo.SplitStringTable('' one , two  ,,  , three '', '','')
Examples:	compare performance of

select		*
from		[SplitCVS](replicate(convert(varchar(max), ''inflation,''), 100000))

and

select		*
from		dbo.SplitStringTable(replicate(convert(varchar(max), ''inflation,''), 100000), '','')

***************************************************************************************************************************/
create function dbo.SplitStringTable (@vallist nvarchar(max), @delimiter nvarchar(4))
returns @tbret table (val nvarchar(100) not null)
as
begin
	declare @i1 int, @i2 int;
	declare @length int;
	declare @delim_len int;
	declare @val nvarchar(100);
	set @delim_len = len(@delimiter);
	set @length = len(@vallist);
	-- current start
	set @i1 = 1;
	-- next start
	while @i1 <= @length
	begin
		set @i2 = charindex(@delimiter, @vallist, @i1);
		if @i2 = 0
		begin
			set @i2 = @length + 1;
		end
		if @i2 >= @i1
		begin
			-- insert found substring
			set @val = ltrim(rtrim(substring(@vallist,@i1,@i2-@i1)));
			if @val != N''''
			begin
				insert @tbret values (@val);
			end
			set @i1 = @i2 + @delim_len;
		end
		else
		begin
			-- scroll to the end of the string, done
			set @i1 = @length + 1;
		end
	end
	return
end')

exec(N'
--{{region:create_object}}
/***********************************************************************************
Author:		Vasily
Created:	4/07/2007
Comment:	The utility procedure is intended to be used in CATCH blocks
			to re-throw the caught exception after handling it
			returns error number
Examples:	
			declare @retcode int
			set @retcode = 0
			begin try
				raiserror(''test'', 16, 1)
				print ''must not be printed''
			end try
			begin catch
				exec @retcode = dbo.util_rethrow_exception
			end catch
			print ''retcode = '' + convert(varchar(10), @retcode)
***********************************************************************************/
create proc dbo.util_rethrow_exception
as
set nocount on
	DECLARE 
			@ErrorMessage    NVARCHAR(4000),
			@ErrorNumber     INT,
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine       INT,
			@ErrorProcedure  NVARCHAR(200);
    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), ''-'');

	if isnull(@ErrorNumber,0) = 0
		goto _exit

    -- Building the message string that will contain original
    -- error information.
    SELECT @ErrorMessage = 
        N''Error %d, Level %d, State %d, Procedure %s, Line %d, '' + 
            ''Message: ''+ ERROR_MESSAGE();
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	_exit:
return @ErrorNumber
')

exec(N'
--{{region:create_object}}

/**********************************************************************************************************
Author:		Vasily Kabanov
Created:	2011-10-02
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
			Reports number of documents containing audio or video files (media type mp3, wmv, mp4a) per author.
			!!TODO: check if Stream and mp4 need to be included too; it seems a concept of media type
			group/category is missing.
Old query was 
	SELECT
          d.Author1 AS AuthorId ,
          da.AuthorLast ,
          da.AuthorFirst + '' '' + da.AuthorMiddle + '' '' + da.AuthorLast AS Author ,
          COUNT(1) AS ''Count''
      FROM
          Documents d JOIN [DocumentAuthors] da
      ON  d.[Author1] = da.[AuthorId] OR d.[Author2] = da.[AuthorId] JOIN
      MediaAlternateFormat
      ON  MediaAlternateFormat.DocumentId = d.DocumentId
      WHERE
          MediaAlternateFormat.MediaTypeId IN ( 1 , 2 , 8 )          
      GROUP BY
          d.Author1 ,
          da.AuthorLast ,
          da.AuthorFirst ,
          da.AuthorMiddle        
      ORDER BY
          da.AuthorLast

Examples:

exec dbo.MediaGetAuthorsAlpha


**********************************************************************************************************/
create proc dbo.MediaGetAuthorsAlpha
as
set nocount on;

with DocumentsWithMedia as (
	select			d.DocumentId
					, d.Author1
					, d.Author2
	from			dbo.Documents d
	where			exists (
						select		*
						from		dbo.MediaAlternateFormat r
						where		d.DocumentId = r.DocumentId
									and r.MediaTypeId IN ( 1 , 2 , 8 )
									and r.Display = 1
					)
					and d.Display = 1
)
, CountsFlat as (
	select			Author1 as AuthorId
					, count(*) as cnt
	from			DocumentsWithMedia
	group by		Author1
	union all
	select			Author2
					, count(*) as cnt
	from			DocumentsWithMedia
	--where			Author2 != Author1
	group by		Author2
)
, CountsTotal as (
	select			AuthorId
					, sum(cnt) as cnt
	from			CountsFlat
	group by		AuthorId
)
select		c.AuthorId
			, da.AuthorLast
			, da.AuthorFirst + coalesce(N'' '' + da.AuthorMiddle + N'' '', N'' '') + da.AuthorLast AS Author
			, c.cnt AS [Count]
from		CountsTotal c
			, dbo.DocumentAuthors da
where		c.AuthorId = da.AuthorId;

return @@error;

')

exec(N'
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
**********************************************************************************************************/

create procedure dbo.DocumentDetails
       @DocumentId int = 0 ,
       @MediaId int = 0
AS
set concat_null_yields_null off

IF @MediaId  > 0
BEGIN
	EXEC dbo.MediaDetails @MediaId
	RETURN
END

DECLARE @ParentCategoryId int

SET @ParentCategoryId = ( SELECT TOP 1
                              ParentCategory
                          FROM
                              MediaCategory dc
                          WHERE
                              dc.CategoryId = ( SELECT
                                                    CategoryId
                                                FROM
                                                    dbo.Documents
                                                WHERE
                                                    DocumentId = @DocumentId ) )

SELECT
    DocumentId ,
    GUID ,
    Display ,
    Featured ,
    Title ,
    Author1 ,
    Author2 ,
    PubInfo ,
    FullText ,
    Source ,
    GuideContent ,
    Documents.CreateDate ,
    CategoryId ,
    ProductId,
    DocumentAuthors.AuthorFirst + '' '' + DocumentAuthors.AuthorMiddle + '' '' +
    DocumentAuthors.AuthorLast AS AuthorName ,
    DocumentAuthors2.AuthorFirst + '' '' + DocumentAuthors2.AuthorMiddle + '' '' +
    DocumentAuthors2.AuthorLast AS CoAuthorName ,
    @ParentCategoryId AS ParentCategoryId ,
    ( SELECT TOP 1
          ParentCategory
      FROM
          MediaCategory dc
      WHERE
          dc.CategoryId = @ParentCategoryId ) AS GrandParentCategoryId ,
    [Length] ,
    metaDescription,
    metaKeywords ,
    
    (SELECT TOP 1 Description FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreDescription,
    --SELECT ModelNumber, SearchKeywords FROM AbleCommerce.dbo.ac_Products
    (SELECT TOP 1 ModelNumber FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS ISBN,
    (SELECT TOP 1 SearchKeywords FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreKeywords,
    metaImage,    
    --REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE [Name] = Documents.Title),''~'',''/store/'') AS CoverImage
    REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId),''~'',''/store/'') AS CoverImage, 
    Documents.ProductId
FROM
    Documents 
 LEFT JOIN dbo.DocumentAuthors
ON  Documents.Author1 = DocumentAuthors.AuthorId
 LEFT JOIN dbo.DocumentAuthors DocumentAuthors2
ON  Documents.Author2 = DocumentAuthors2.AuthorId
WHERE
    ( DocumentId = @DocumentId )

SELECT
    DocumentSubjectLink.SubjectId
FROM
    DocumentSubjectLink JOIN DocumentSubjects
ON  DocumentSubjects.SubjectId = DocumentSubjectLink.SubjectId
WHERE
    DocumentId = @DocumentId

SELECT
    MediaAlternateFormat.MediaId ,
    MediaAlternateFormat.DocumentId ,
    MediaAlternateFormat.MediaTypeId ,
    MediaAlternateFormat.URL ,
    MediaAlternateFormat.CreateDate ,    
    DocumentMediaType.MediaTypeID AS Expr1 ,
    DocumentMediaType.MediaType ,
    DocumentMediaType.MediaIconPath ,
    DocumentMediaType.UploadPath ,
    DocumentMediaType.Description ,
    DocumentMediaType.CreateTime ,
    DocumentMediaType.MIMEtype ,
    DocumentMediaType.IsDeleted ,
    DocumentMediaType.Extensions ,
    DocumentMediaType.IsMedia ,
    MediaAlternateFormat.fileSize ,
    MediaAlternateFormat.duration,
    MediaAlternateFormat.VolumeOrdinal,
    MediaAlternateFormat.VolumeComment
FROM
    MediaAlternateFormat LEFT OUTER JOIN DocumentMediaType
ON  DocumentMediaType.MediaTypeID = MediaAlternateFormat.MediaTypeId
WHERE
    ( MediaAlternateFormat.DocumentId = @DocumentId )

return @@error;')

exec(N'
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
**********************************************************************************************************/

create procedure dbo.MediaDetails
       @MediaId int
as
set concat_null_yields_null off

declare @DocumentId int

select			@DocumentId = DocumentId
from			dbo.MediaAlternateFormat
where			MediaId = @MediaId;

exec dbo.DocumentDetails @DocumentId = @DocumentId;

return @@error;')

exec(N'
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	

exec dbo.MediaGetRandomVideo
**********************************************************************************************************/
create  procedure  dbo.MediaGetRandomVideo
as

select		top (1)
			m.[MediaId],
			m.[DocumentId],
			m.[MediaTypeId],
			m.[URL],
			m.[fileSize],
			m.[duration],
			m.[CreateDate],
			m.[Display],
			m.VolumeOrdinal,
			m.VolumeComment
from		dbo.Documents d
			inner join	dbo.MediaAlternateFormat m
				on d.DocumentId = m.DocumentId
where		m.Display = 1
			and m.MediaTypeId = 16 --TODO or in (2, 14, 16) plus any new ones? Maybe need a media type type?!?
order by	newid()
')


-- gracefully and explicitly releasing the lock
exec dbo.sp_releaseapplock 
	@Resource = @lock_key
	, @LockOwner = N'Transaction';

commit tran

end try
begin catch

	DECLARE 
			@ErrorMessage    NVARCHAR(4000),
			@ErrorNumber     INT,
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine       INT,
			@ErrorProcedure  NVARCHAR(200);
    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	if xact_state() != 0
	begin
		-- gracefully and explicitly releasing the lock
		exec dbo.sp_releaseapplock 
			@Resource = @lock_key
			, @LockOwner = N'Transaction'
		-- there is a transaction and it must be rolled back
		rollback transaction;
	end

    -- Building the message string that will contain original
    -- error information.
    SELECT @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: '+ ERROR_MESSAGE();
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	return
end catch



