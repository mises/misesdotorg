﻿if exists (select * from sys.assemblies where [name] = N'FullTextQueryGenerator')
begin
	drop assembly [FullTextQueryGenerator]
end

create assembly		[FullTextQueryGenerator]
from				'FullTextQueryGenerator'
with				permission_set = safe;
