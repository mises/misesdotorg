﻿--{{region:create_table_if_not_exists}}

if not exists (
	select		*
	from		sys.tables
	where		object_id = object_id(N'dbo.Documents')
)
begin
	create table dbo.Documents (
		[DocumentId] [int] identity(1,1) not null,
		[GUID] [uniqueidentifier] not null,
		[Display] [bit] not null,
		[Featured] [bit] null,
		[Title] [varchar](255) not null,
		[Author1] [int] not null,
		[Author2] [int] null,
		[PubInfo] [varchar](max) null,
		[metaDescription] [varchar](max) null,
		[metaKeywords] [varchar](350) null,
		[metaImage] varbinary(max) null,
		[_oldURL] [varchar](255) null,
		[_oldMediaTypeId] [int] null,
		[FullText] [bit] null,
		[Source] [varchar](100) null,
		[GuideContent] [varchar](max) null,
		[Length] [int] null,
		[CategoryId] [int] null,
		[CreateDate] [datetime] not null,
		[EditDate] [datetime] not null,
		[ProductId] [int] null
	);

-----------------------------------------------------------
-- legacy creation code
-- TODO: move to appropriate sections and make re-runnable when need to change or have time


ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_Documents_MediaCategory]

ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_StudyGuideAuthors] FOREIGN KEY([Author1])
REFERENCES [dbo].[DocumentAuthors] ([AuthorId])
NOT FOR REPLICATION 

ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_StudyGuideAuthors]

ALTER TABLE [dbo].[Documents]  WITH NOCHECK ADD  CONSTRAINT [FK_StudyGuide_StudyGuideAuthors1] FOREIGN KEY([Author2])
REFERENCES [dbo].[DocumentAuthors] ([AuthorId])
NOT FOR REPLICATION 

ALTER TABLE [dbo].[Documents] NOCHECK CONSTRAINT [FK_StudyGuide_StudyGuideAuthors1]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_StudyGuide_GUID]  DEFAULT (newid()) FOR [GUID]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_Documents_Featured]  DEFAULT ((0)) FOR [Featured]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_StudyGuide_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]

ALTER TABLE [dbo].[Documents] ADD  CONSTRAINT [DF_Documents_EditDate]  DEFAULT (getdate()) FOR [EditDate]

-- end legacy creation code
-----------------------------------------------------------

end

go
--{{region:verify_table_structure}}

if objectproperty(object_id(N'dbo.Documents'), 'TableHasPrimaryKey') = 0
begin
	alter table					dbo.Documents
	add constraint				PK_Documents
	primary key clustered		(DocumentId);
end

-- eliminating instances whe valid Author1 is not equal to valid Author2

------------------------------------------------------------------
-- one-off update section, delete when versioning is implemented
set nocount on;

update		dbo.Documents
set			Author2 = 0
where		Author1 != 0
			and Author2 = Author1;

update		dbo.Documents
set			Author1 = Author2
			, Author2 = 0
where		Author2 > 0
			and Author1 = 0;
-------------------------------------------------------------------

if not exists (
	select		*
	from		sys.check_constraints
	where		object_id = object_id(N'CHK_Documents_Authors')
				and parent_object_id = object_id(N'dbo.Documents')
)
begin
	alter table		dbo.Documents
	with check
	add constraint	CHK_Documents_Authors
	check			(
						Author1 >= 0
						and Author2 >= 0
						and (
							(
								Author1 = 0
								and Author2 = 0
							)
							or (
								Author1 > 0
								and Author2 != Author1
							)
						)
					);
end

if exists (
	select			*
	from			sys.columns
	where			object_id = object_id(N'dbo.Documents')
					and column_id = columnproperty(object_id, N'metaImage', 'ColumnID')
					and system_type_id = 34
)
begin
	alter table		dbo.Documents
	alter column	metaImage varbinary(max) null;
end

go
--{{region:verify_table_indexes}}

if not exists (
	-- query below will return rows if there's a nonclustered index (Author2)
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
	where		i.object_id = object_id(N'dbo.Documents')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'Author2', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic2.key_ordinal
				)
)
begin
	-- including Display first because normally we would almost always want to find displayable docs
	create nonclustered index		IDX_Documents_DisplayAuthor2
	on								dbo.Documents (Display, Author2);
end

if not exists (
	-- query below will return rows if there's a nonclustered index (Author2)
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
	where		i.object_id = object_id(N'dbo.Documents')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'Author2', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic1.key_ordinal
				)
)
begin
	-- including Display first because normally we would almost always want to find displayable docs
	create nonclustered index		IDX_Documents_Author2
	on								dbo.Documents (Author2);
end

go
--{{region:verify_fulltext_indexes}}

if not exists (
	select		*
	from		sys.fulltext_index_columns
	where		object_id = object_id(N'dbo.Documents')
				and column_id = columnproperty(object_id, 'PubInfo', 'ColumnId')
)
begin
	alter		fulltext index
	on			dbo.Documents
	add			(PubInfo);
end

go

--{{region:verify_referencial_constraints}}

if not exists (
	select		*
	from		sys.foreign_keys
	where		referenced_object_id = object_id(N'dbo.MediaCategory')
				and parent_object_id = object_id(N'dbo.Documents')
)
begin
	alter table						dbo.Documents
	with nocheck add constraint		FK_Documents_MediaCategory
	foreign key						(CategoryId)
	references						dbo.MediaCategory (CategoryId)
	not for replication;
end

-- add ISBN column
if exists (
	select			*
	from			sys.columns
	where			object_id = object_id(N'dbo.Documents')
					and column_id = columnproperty(object_id, N'ISBN', 'ColumnID')
					and system_type_id = 34
)
begin
	alter table		dbo.Documents
	add column	ISBN varchar(50) null;
end
