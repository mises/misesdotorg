﻿--{{region:create_table_if_not_exists}}

if not exists (
	select		*
	from		sys.tables
	where		object_id = object_id(N'dbo.MediaCategory')
)
begin

	create table dbo.MediaCategory (
		CategoryId int identity(1,1) not null,
		ParentCategory int not null,
		Category varchar(255) not null,
		Description varchar(8000) null,
		CategoryImage varchar(100) null,
		iTunesCategoryCode varchar(50) null,
		CreateDate smalldatetime null,
		SortOrder int null
	);


-----------------------------------------------------------
-- legacy creation code
-- TODO: move to appropriate sections and make re-runnable when need to change or have time

	ALTER TABLE dbo.MediaCategory ADD  CONSTRAINT DF_MediaCategory_ParentCategory  DEFAULT ((102)) FOR [ParentCategory];
	ALTER TABLE dbo.MediaCategory ADD  CONSTRAINT DF_MediaCategory_CreateDate  DEFAULT (getdate()) FOR [CreateDate];

-- end legacy creation code
-----------------------------------------------------------

end

go
--{{region:verify_table_structure}}

if exists (
	-- query below will return 1 row if primary key is on MediaId
	select		*
	from		sys.key_constraints c
				, sys.indexes i
				, sys.index_columns ic1
	where		c.parent_object_id = object_id(N'dbo.MediaCategory')
				and c.type = 'PK'
				and c.name = N'PK_MediaCategory'
				and i.index_id = c.unique_index_id
				and i.type = 1
				and i.object_id = c.parent_object_id
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'CategoryId', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic1.key_ordinal
				)
)
begin
	if exists (
		select		*
		from		sys.foreign_keys
		where		referenced_object_id = object_id(N'dbo.MediaCategory')
					and parent_object_id = object_id(N'dbo.Documents')
	)
	begin
		alter table			dbo.Documents
		drop constraint		FK_Documents_MediaCategory
	end

	alter table			dbo.MediaCategory
	drop constraint		PK_MediaCategory;
end

if objectproperty(object_id(N'dbo.MediaCategory'), 'TableHasClustIndex') = 0
begin
	-- need to keep children together for range queries
	create unique clustered index	idx_MediaCategory_ParentSelf
	on								dbo.MediaCategory (ParentCategory, CategoryId);
end

if objectproperty(object_id(N'dbo.MediaCategory'), 'TableHasPrimaryKey') = 0
begin
	 alter table				dbo.MediaCategory
	 add constraint				PK_MediaCategory
	 primary key nonclustered	([CategoryId]);
end

go
--{{region:verify_table_indexes}}
