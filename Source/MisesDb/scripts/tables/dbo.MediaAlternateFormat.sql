﻿--{{region:create_table_if_not_exists}}

if not exists (
	select		*
	from		sys.tables
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
)
begin
	create table [dbo].[MediaAlternateFormat] (
		[MediaId] [int] identity(1,1) not null,
		[DocumentId] [int] not null,
		[MediaTypeId] [int] not null,
		VolumeOrdinal tinyint not null,
		VolumeComment varchar(50) null,
		[URL] [varchar](500) not null,
		[fileSize] [bigint] not null,
		[duration] [decimal](18, 0) not null,
		[CreateDate] [datetime] not null,
		[Display] [bit] not null,
	);
end

go
--{{region:verify_table_structure}}

if columnproperty(object_id(N'dbo.MediaAlternateFormat'), N'VolumeOrdinal', 'ColumnID') is null
begin
	alter table		dbo.MediaAlternateFormat
	add				VolumeOrdinal tinyint not null
	constraint		DF_MediaAlternateFormat_VolumeOrdinal default (1);

	-- automatically updating ordinal for duplicate {DocumentId, MediaTypeId} pairs
	exec(N'
	with Duplicates as (
		select			DocumentId
						, MediaTypeId
						, count(*) as cnt
		from			dbo.MediaAlternateFormat
		group by		DocumentId
						, MediaTypeId
		having			count(*) > 1
	)
	, Ordinals as (
		select			t.DocumentId
						, t.MediaTypeId
						, t.MediaId
						, row_number() over (partition by t.DocumentId, t.MediaTypeId order by t.URL) as GuessedVolumeOrdinal
		from			Duplicates d
						, dbo.MediaAlternateFormat t
		where			d.DocumentId = t.DocumentId
						and d.MediaTypeId = t.MediaTypeId
	)
	update		t
	set			t.VolumeOrdinal = o.GuessedVolumeOrdinal
	from		dbo.MediaAlternateFormat t
				, Ordinals o
	where		o.MediaId = t.MediaId
	');
end

if columnproperty(object_id(N'dbo.MediaAlternateFormat'), N'VolumeComment', 'ColumnID') is null
begin
	alter table			dbo.MediaAlternateFormat
	add					VolumeComment varchar(50) null;
end

-- need to drop these indexes in any case but should do it before dropping clustered index
-- to avoid rebuilding then dropping
if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'_dta_index_MediaAlternateFormat_7_1401772051__K2_K3_4_5_6'
)
begin
	drop index dbo.MediaAlternateFormat._dta_index_MediaAlternateFormat_7_1401772051__K2_K3_4_5_6;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'_dta_index_MediaAlternateFormat_7_1401772051__K2_K3'
)
begin
	drop index dbo.MediaAlternateFormat._dta_index_MediaAlternateFormat_7_1401772051__K2_K3;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'_dta_index_MediaAlternateFormat_7_1401772051__K3_K2_4_5_6'
)
begin
	drop index dbo.MediaAlternateFormat._dta_index_MediaAlternateFormat_7_1401772051__K3_K2_4_5_6;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'IDX_MediaAlternateFormat_Display'
)
begin
	drop index dbo.MediaAlternateFormat.IDX_MediaAlternateFormat_Display;
end

if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'IDX_MediaAlternateFormat_DocMedTypeDisplay'
)
begin
	drop index dbo.MediaAlternateFormat.IDX_MediaAlternateFormat_DocMedTypeDisplay;
end


if exists (
	select		*
	from		sys.indexes
	where		object_id = object_id(N'dbo.MediaAlternateFormat')
				and name = N'IDX_MediaAlternateFormat_DisplayMedType'
)
begin
	drop index dbo.MediaAlternateFormat.IDX_MediaAlternateFormat_DisplayMedType;
end

if exists (
	-- query below will return 1 row if primary key is on MediaId and clustered
	select		*
	from		sys.key_constraints c
				, sys.indexes i
				, sys.index_columns ic1
	where		c.parent_object_id = object_id(N'dbo.MediaAlternateFormat')
				and c.type = 'PK'
				and c.name = N'PK_AlternateMediaFormat'
				and i.index_id = c.unique_index_id
				and i.type = 1
				and i.object_id = c.parent_object_id
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'MediaId', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic1.key_ordinal
				)
)
begin
	alter table			dbo.MediaAlternateFormat
	drop constraint		PK_AlternateMediaFormat;
end


if exists (
	select		*
	from		sys.key_constraints k
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and name = N'UC_AlternateMediaFormat_DocumentIdMediaTypeDisplay'
				and objectproperty(object_id, 'CnstIsClustKey') = 1
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	drop constraint		UC_AlternateMediaFormat_DocumentIdMediaTypeDisplay;
end

if objectproperty(object_id(N'[dbo].[MediaAlternateFormat]'), 'TableHasClustIndex') = 0
begin
	alter table			dbo.MediaAlternateFormat
	add constraint		UC_AlternateMediaFormat_DisplayDocumentIdMediaTypeVolOrd
	unique clustered	(Display, DocumentId, MediaTypeId, VolumeOrdinal);
end

if objectproperty(object_id(N'[dbo].[MediaAlternateFormat]'), 'TableHasPrimaryKey') = 0
begin
	alter table					dbo.MediaAlternateFormat
	add constraint				PK_AlternateMediaFormat
	primary key nonclustered	(MediaId);
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'fileSize', 'ColumnID')
)
begin
	alter table		[dbo].[MediaAlternateFormat]
	add  constraint	[DF_MediaAlternateFormat_fileSize]
	default			((0))
	for				[fileSize];
end


if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'duration', 'ColumnID')
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	add  constraint		[DF_MediaAlternateFormat_duration]
	default				((0))
	for					[duration]
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'CreateDate', 'ColumnID')
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	add constraint		[DF_AlternateMediaFormat_CreateDate]
	default				(getdate())
	for					[CreateDate];
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'Display', 'ColumnID')
)
begin
	alter table			[dbo].[MediaAlternateFormat]
	add constraint		[DF_MediaAlternateFormat_Visible]
	default				((1))
	for					[Display];
end

if not exists (
	select		*
	from		sys.default_constraints
	where		parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
				and parent_column_id = columnproperty(parent_object_id, N'VolumeOrdinal', 'ColumnID')
)
begin
	alter table			dbo.MediaAlternateFormat
	add constraint		DF_MediaAlternateFormat_VolumeOrdinal
	default				(1)
	for					VolumeOrdinal;
end

go


--{{region:verify_table_indexes}}
if not exists (
	-- query below will return rows if there's a nonclustered key (Display, DocumentId, MediaId)
	-- this index is primarily for counting documents
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
				, sys.index_columns ic3
	where		i.object_id = object_id(N'dbo.MediaAlternateFormat')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'DocumentId', 'ColumnId')
				and i.object_id = ic3.object_id
				and i.index_id = ic3.index_id
				and ic3.key_ordinal = 3
				and ic3.column_id = columnproperty(i.object_id, N'MediaTypeId', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic3.key_ordinal
				)
)
begin
	create nonclustered index		IDX_MediaAlternateFormat_DisplayDocMedType
	on								dbo.MediaAlternateFormat (
										Display
										, DocumentId
										, MediaTypeId
									);
end

if not exists (
	-- query below will return rows if there's a nonclustered key (DocumentId, MediaId, Display)
	-- this index is primarily for counting documents
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
	where		i.object_id = object_id(N'dbo.MediaAlternateFormat')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'MediaTypeId', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic2.key_ordinal
				)
)
begin
	create nonclustered index		IDX_MediaAlternateFormat_MedTypeDisplay
	on								dbo.MediaAlternateFormat (
										MediaTypeId
										, Display
									);
end

if not exists (
	-- query below will return rows if there's a nonclustered key (Display, MediaTypeId, DocumentId)
	-- this index is primarily for counting documents
	select		*
	from		sys.indexes i
				, sys.index_columns ic1
				, sys.index_columns ic2
				, sys.index_columns ic3
	where		i.object_id = object_id(N'dbo.MediaAlternateFormat')
				and i.type = 2 -- nonclustered
				and i.object_id = ic1.object_id
				and i.index_id = ic1.index_id
				and ic1.key_ordinal = 1
				and ic1.column_id = columnproperty(i.object_id, N'Display', 'ColumnId')
				and i.object_id = ic2.object_id
				and i.index_id = ic2.index_id
				and ic2.key_ordinal = 2
				and ic2.column_id = columnproperty(i.object_id, N'MediaTypeId', 'ColumnId')
				and i.object_id = ic3.object_id
				and i.index_id = ic3.index_id
				and ic3.key_ordinal = 3
				and ic3.column_id = columnproperty(i.object_id, N'DocumentId', 'ColumnId')
				and not exists (
					select		*
					from		sys.index_columns ice
					where		ice.object_id = i.object_id
								and ice.index_id = i.index_id
								and ice.key_ordinal > ic3.key_ordinal
				)
)
begin
	create nonclustered index		IDX_MediaAlternateFormat_DisplayMedTypeDoc
	on								dbo.MediaAlternateFormat (
										Display
										, MediaTypeId
										, DocumentId
									);
end

go
--{{region:verify_referencial_constraints}}

if not exists (
	select		*
	from		sys.foreign_key_columns
	where		referenced_object_id = object_id(N'[dbo].[DocumentMediaType]')
				and parent_object_id = object_id(N'[dbo].[MediaAlternateFormat]')
)
begin
	alter table						[dbo].[MediaAlternateFormat]
	with nocheck add constraint		[FK_MediaAlternateFormat_DocumentMediaType]
	foreign key						([MediaTypeId])
	references						[dbo].[DocumentMediaType] ([MediaTypeID]);
end

alter table					[dbo].[MediaAlternateFormat]
nocheck constraint			[FK_MediaAlternateFormat_DocumentMediaType];

