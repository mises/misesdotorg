﻿--{{region:drop_object_if_exists}}

if exists (
	select		*
	from		sys.sql_modules
	where		object_id = object_id(N'dbo.SplitStringTable')
				and objectproperty(object_id, 'IsTableFunction') = 1
)
begin
	drop function dbo.SplitStringTable;
end
go
--{{region:create_object}}

/***************************************************************************************************************************
Author:		Vasily Kabanov
Created:	23/07/2007
Parameters:	@vallist - the string containing a list of individual string values delimited by @delimiter
			@delimiter - the character sequence used to separate individual values in @vallist
Returns:	table (
				val nvarchar(100) not null - non-empty trimmed value
			)
Comment:	The utility function converts a delimited list of values into table format.
			Individual values are trimmed from both ends and empty values are removed from the result.
			Hence
				select * from dbo.SplitStringTable('one,two,three', ',')
			returns the same result as  
				select * from dbo.SplitStringTable(' one , two  ,,  , three ', ',')
Examples:	compare performance of

select		*
from		[SplitCVS](replicate(convert(varchar(max), 'inflation,'), 100000))

and

select		*
from		dbo.SplitStringTable(replicate(convert(varchar(max), 'inflation,'), 100000), ',')

***************************************************************************************************************************/
create function dbo.SplitStringTable (@vallist nvarchar(max), @delimiter nvarchar(4))
returns @tbret table (val nvarchar(100) not null)
as
begin
	declare @i1 int, @i2 int;
	declare @length int;
	declare @delim_len int;
	declare @val nvarchar(100);
	set @delim_len = len(@delimiter);
	set @length = len(@vallist);
	-- current start
	set @i1 = 1;
	-- next start
	while @i1 <= @length
	begin
		set @i2 = charindex(@delimiter, @vallist, @i1);
		if @i2 = 0
		begin
			set @i2 = @length + 1;
		end
		if @i2 >= @i1
		begin
			-- insert found substring
			set @val = ltrim(rtrim(substring(@vallist,@i1,@i2-@i1)));
			if @val != N''
			begin
				insert @tbret values (@val);
			end
			set @i1 = @i2 + @delim_len;
		end
		else
		begin
			-- scroll to the end of the string, done
			set @i1 = @length + 1;
		end
	end
	return
end