﻿--{{region:drop_object_if_exists}}

if exists (
	select		*
	from		sys.sql_modules
	where		object_id = object_id(N'dbo.SearchDocuments')
				and objectproperty(object_id, 'IsTableFunction') = 1
)
begin
	drop function dbo.SearchDocuments;
end
go
--{{region:create_object}}

/***************************************************************************************************************************
Author:		Vasily Kabanov
Created:	17/10/2011
Parameters:	@searchString - the CONTAINS search string; invalid syntax will result in error
			@titleWeight - weight to assign to title match rank;
Returns:	table (
				DocumentId
				, rank			-- abstract number indicating how close the document title, metaDescription and PubInfo
					-- match the search string; specify 1.0 to regard Title as important as the other searchable document
					-- attributes.
			) --UnOrdered!
Comment:	The function encapsulates fulltext search of the Documents table. It allows to increase the priority of
			document title when ranking matches. It returns unordered fulltext service results only, so the results
			will need to be joined, ordered, filtered and limited at higher levels.
			
Examples:

select			top(100)
				d.Title
				, d.DocumentId
				, s.rank
				, d.Author1
from			dbo.SearchDocuments(N'"Gary North"', 10) s
				, dbo.Documents d
where			d.Display = 1
				and d.DocumentId = s.DocumentId
				--and s.Rank > 200
order by		s.rank desc

***************************************************************************************************************************/
create function dbo.SearchDocuments(@searchString nvarchar(1000), @titleWeight float)
returns table
as
return
select		
			coalesce(kt.[KEY], kr.[KEY]) as DocumentId
			, coalesce(kt.RANK, 0) * @titleWeight + coalesce(kr.RANK, 0) as rank
from		containstable(Documents, Title, @searchString) kt
			full join containstable(Documents, (metaDescription, metaKeywords, PubInfo), @searchString) kr
				on kt.[KEY] = kr.[KEY]
