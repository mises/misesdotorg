﻿--{{region:drop_object_if_exists}}

if exists (
	select		*
	from		sys.assembly_modules
	where		[object_id] = object_id(N'dbo.GetContainsQuery')
				and objectproperty( [object_id], N'IsScalarFunction' ) = 1
)
begin
	drop function dbo.GetContainsQuery
end

go
--{{region:create_object}}

create function [dbo].GetContainsQuery(@value nvarchar(1000))
returns nvarchar(max)
with execute as caller
as
external name [FullTextQueryGenerator].UserDefinedFunctions.GetContainsQuery
