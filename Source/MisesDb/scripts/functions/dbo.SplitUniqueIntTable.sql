﻿--{{region:drop_object_if_exists}}
if exists (
	select		*
	from		sys.sql_modules
	where		object_id = object_id(N'dbo.SplitUniqueIntTable')
				and objectproperty(object_id, 'IsTableFunction') = 1
)
begin
	drop function dbo.SplitUniqueIntTable
end
go
--{{region:create_object}}

/***************************************************************************************************************************
Author:			Vasily Kabanov
Created:		23/07/2007
Parameters:		@vallist - the delimited list of values to parse
				@delimiter - the character sequence used to separate individual values in the string
				@ignore_conversion_errors - if 0, then parsing fails if there's a non-numeric value in the list, otherwise
					all non-numeric values are silently ignored
Comment:		The utility function transforms a flat text string list of values into typed tabular form.
				Safe to use in the from clause (returned table contains primary key: quick lookups etc)
Examples:		
				select * from dbo.SplitUniqueIntTable('1,2,3,6,5', N',', 0)
***************************************************************************************************************************/
create function dbo.SplitUniqueIntTable (
	@vallist nvarchar(max), @delimiter nvarchar(4) = N';', @ignore_conversion_errors bit = 1)
returns @tbret table (val int not null primary key)
as
begin
	insert				@tbret
	select distinct		convert(int, val)
	from				dbo.SplitStringTable(@vallist, @delimiter)
	where				(@ignore_conversion_errors = 0 or 1 = isnumeric(val))
	return
end
go
