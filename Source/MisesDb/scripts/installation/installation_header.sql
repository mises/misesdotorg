﻿set nocount on

declare @product_name nvarchar(50)

declare @retcode int

begin try

set @product_name = N'MisesWebSiteDatabase';

begin tran

declare @lock_key sysname
set @lock_key = @product_name + N'_Installation'

exec @retcode = dbo.sp_getapplock
	@Resource = @lock_key
	, @LockMode = N'Exclusive'
	, @LockOwner = N'Transaction'
	, @LockTimeout = N'0'

if @retcode < 0
begin
	raiserror('Concurrent installation detected, cancelling the installation', 16, 1)
end

--SEGMENT the installation script body
