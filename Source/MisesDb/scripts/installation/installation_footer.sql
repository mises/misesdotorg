﻿
-- gracefully and explicitly releasing the lock
exec dbo.sp_releaseapplock 
	@Resource = @lock_key
	, @LockOwner = N'Transaction';

commit tran

end try
begin catch

	DECLARE 
			@ErrorMessage    NVARCHAR(4000),
			@ErrorNumber     INT,
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine       INT,
			@ErrorProcedure  NVARCHAR(200);
    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	if xact_state() != 0
	begin
		-- gracefully and explicitly releasing the lock
		exec dbo.sp_releaseapplock 
			@Resource = @lock_key
			, @LockOwner = N'Transaction'
		-- there is a transaction and it must be rolled back
		rollback transaction;
	end

    -- Building the message string that will contain original
    -- error information.
    SELECT @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: '+ ERROR_MESSAGE();
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	return
end catch

