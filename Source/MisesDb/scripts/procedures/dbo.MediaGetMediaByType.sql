﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaGetMediaByType')
)
begin
	drop proc dbo.MediaGetMediaByType;
end

go
--{{region:create_object}}
CREATE  PROCEDURE dbo.MediaGetMediaByType
       @MediaTypeID int = 0 ,
       @MaxResults int = 0
AS
SET CONCAT_NULL_YIELDS_NULL OFF 

IF @MaxResults > 0
SET ROWCOUNT @MaxResults;

      SELECT
          Documents.DocumentId ,
          Documents.Title ,
          Documents.CategoryId ,
          Documents.CreateDate ,
          Documents.metaDescription AS 'Description' ,
          media.DocumentId ,
          media.MediaTypeId ,
          media.URL ,
          media.MediaId ,
          Documents.EditDate ,
          media.CreateDate ,
          DocumentAuthors.AuthorId ,
          DocumentAuthors2.AuthorId AS CoAuthorId ,
          DocumentMediaType.MediaType ,
          DocumentMediaType.MediaIconPath ,
          -- MediaCategory.[Category] ,
          DocumentAuthors.AuthorFirst + '  ' + DocumentAuthors.AuthorMiddle + '  ' +
          DocumentAuthors.AuthorLast AS Author ,
          DocumentAuthors2.AuthorFirst + '  ' + DocumentAuthors2.AuthorMiddle + '  ' + DocumentAuthors2.AuthorLast AS CoAuthor ,
          DocumentAuthors.[AuthorLast] AS LastName ,
          DocumentAuthors2.[AuthorLast] AS CoLastName ,
          CASE
               WHEN Documents.metaImage IS NULL THEN 0
               WHEN Documents.metaImage IS NOT NULL THEN 1
          END AS HasImage,
          media.VolumeOrdinal,
          media.VolumeComment
          
      FROM
          dbo.Documents LEFT JOIN dbo.MediaAlternateFormat media
      ON  media.DocumentId = Documents.DocumentId LEFT JOIN dbo.DocumentAuthors
      ON  Documents.Author1 = DocumentAuthors.AuthorId LEFT JOIN dbo.DocumentMediaType
      ON  DocumentMediaType.MediaTypeId = media.MediaTypeId LEFT JOIN dbo.MediaCategory
      ON  Documents.CategoryId = MediaCategory.CategoryId
      
      LEFT JOIN dbo.DocumentAuthors AS DocumentAuthors2
      ON  Documents.Author2 = DocumentAuthors2.AuthorId
      WHERE
          DocumentMediaType.MediaTypeId = @MediaTypeID          
      ORDER BY
          Documents.Title

