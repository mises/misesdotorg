﻿if exists (
	select		*
	from		sys.procedures
	where		object_id = object_id(N'dbo.defrag_table_indexes')
)
begin
	drop proc dbo.defrag_table_indexes;
end
go

/****************************************************************************************************
Author:			Vasily Kabanov
Created:		2010-06-11
Parameters:		@objectID int - object_id of a user table or indexed view, see sys.tables.object_id
					-- and sys.views.object_id; if user table or view with the specified ID is not
					-- found in the same database, error is raised
				@jobID - the ID of the job which executes the procedure; if NULL, the log messages
					-- will be output by print
				@rebuildFrafmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index rebuild; default is 30 (recommended by Microsoft)
				@reorganizeFragmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index reorganise; default is 10 (recommended by Microsoft);
					-- must be less or equal to @rebuildFrafmentationThreshold
				@indexesDefragmented - output, the total number of index trees defragmented (using any method)
					-- every partition represents a separate index tree
				@indexesRebuilt	- out of indexesDefragmented, the number of index partitions
					-- defragmented by rebuilddinfg
				@indexesFailed - output, the number of indexes failed to defragment
				@warningCount - output, the number of non-critical failures, such as resetting
					-- allow_page_locks
Returns:		
				return code:
					0 - success (@indexesFailed will be 0)
					1 - general failure (such as all of the indexes failed)
					2 - the procedure was called in transaction
					3 - the specified object id is invalid or does not represent user table or view
					4 - the specified fragmentation thresholds are invalid
Comment:		The procedure defragments indexes on a table or indexed view.
				The decision is made as follows:
					- if leaf page count is 4 or fewer, no action is taken
					- if leaf level fragmentation is greater than @rebuildFrafmentationThreshold
						or first index level fragmentation is greater than @rebuildFrafmentationThreshold
						and at the number of pages at the first level is greater than 16 (2 extents)
						and the fragment count at first index level is greater than 4, the index is rebuilt
					- if leaf level fragmentation is greater than @reorganizeFragmentationThreshold, the
						index is reorganized; if allow_page_locks is off, it is set to on for the duration
						of the reorganization; if setting allow_page_locks to on fails, the index is rebuilt.
				If you'd like to change this logic scroll down to the section marked with comment
				"-- code section: defrag.action.decision.making" and change it as required.
				Partitioned indexes are defragmented partition by partition.
Examples:
				declare	@indexesDefragmented int
				declare	@indexesRebuilt int
				declare	@indexesFailed int
				declare	@warningCount int
				declare	@objectID int
				declare @retcode int
				
				select @objectID = object_id(N'dbo.TestFragmentation')
				
				exec @retcode = dbo.defrag_table_indexes
					@objectID = @objectID
					, @rebuildFrafmentationThreshold = 30
					, @reorganizeFragmentationThreshold = 10
					, @indexesDefragmented = @indexesDefragmented out
					, @indexesRebuilt = @indexesRebuilt out
					, @indexesFailed = @indexesFailed out
					, @warningCount = @warningCount out
				print 'Retcode = ' + convert(varchar(20), @retcode)
				print 'Defragmented = ' + convert(varchar(20), @indexesDefragmented)
				print 'Failed = ' + convert(varchar(20), @indexesFailed)
				print 'Warnings = ' + convert(varchar(20), @warningCount)

-- have a look at the fragmentation info
select		quotename(schema_name(o.schema_id)) + N'.' + quotename(object_name(s.object_id)) as ObjectName
			, i.name as IndexName
			, i.allow_page_locks
			, s.*
from		sys.dm_db_index_physical_stats(db_id(), object_id(N'dbo.TestFragmentation'), null, null, 'DETAILED') s
			, sys.objects o
			, sys.indexes i
where		o.object_id = s.object_id
			and i.object_id = o.object_id
			and i.index_id = s.index_id
			and index_level < 2
			and is_disabled = 0
							
****************************************************************************************************/
create  procedure dbo.defrag_table_indexes
  @objectID int
, @rebuildFrafmentationThreshold float = 30
, @reorganizeFragmentationThreshold float = 10
, @indexesDefragmented int = null output
, @indexesRebuilt	int = null output
, @indexesFailed int = null output
, @warningCount int = null output
as

set implicit_transactions off;
set nocount on;

declare		@retcode int;

select		@indexesDefragmented = 0
			, @indexesFailed = 0
			, @warningCount = 0
			, @indexesRebuilt = 0;

begin try;

	declare		@retCodeSuccess int
				, @retCodeGeneralFailure int
				, @retCodeFailCalledInTransaction int
				, @retCodeInvalidObject int
				, @retCodeInvalidThresholds int;
				
	select		@retCodeSuccess = 0
				, @retCodeGeneralFailure = 1
				, @retCodeFailCalledInTransaction = 2
				, @retCodeInvalidObject = 3
				, @retCodeInvalidThresholds = 4;
				
	select		@retcode = @retCodeSuccess;

	declare	@actionCodeRebuild smallint
			, @actionCodeReorganize smallint
			, @actionCodeNoAction smallint;
			
	select	@actionCodeRebuild = 1
			, @actionCodeReorganize = 2
			, @actionCodeNoAction = 0;

	if @rebuildFrafmentationThreshold is null
		or @reorganizeFragmentationThreshold is null
		or (@reorganizeFragmentationThreshold > @rebuildFrafmentationThreshold)
	begin
		set @retcode = @retCodeInvalidThresholds;
		raiserror('An invalid combination of fragmentation thresholds was specified', 16, 1);
	end

	if @@trancount > 0
	begin
		set @retcode = @retCodeFailCalledInTransaction;
		raiserror('The procedure defrag_table_indexes must not be executed in a transaction', 16, 1);
	end
	
	if not exists(
		select 1 from sys.tables where object_id = @objectID
		union all
		select 1 from sys.views  where object_id = @objectID
	)
	begin
		set @retcode = @retCodeInvalidObject;
		raiserror('The object ID is invalid', 16, 1);
	end

	declare @objectName sysname;
	select		@objectName = quotename(schema_name(o.schema_id)) + N'.' + quotename(object_name(o.object_id))
	from		sys.objects o
	where		o.object_id = @objectID;
	
	declare @tbAction table (
		index_id int not null
		, partition_number int not null
		, action_code smallint not null
		,  primary key (index_id, partition_number)
	);
	
	with FragmentData as (
		select		s.index_id
					, i.allow_page_locks
					, s.index_level
					, s.avg_fragmentation_in_percent
					, s.page_count
					, s.fragment_count
					, s.partition_number
		from		sys.dm_db_index_physical_stats(db_id(), @objectID, null, null, 'DETAILED') s
					, sys.indexes i
		where		
					i.object_id = s.object_id
					and i.index_id = s.index_id
					and i.is_disabled = 0
					and i.[type] > 0					-- ignore heaps
					and s.avg_fragmentation_in_percent is not null
	)
	, Flattened as (
		select		s.index_id
					, s.avg_fragmentation_in_percent as leaf_frag
					, s.page_count as leaf_page_count
					, f.avg_fragmentation_in_percent as first_level_frag
					, f.page_count first_level_page_count
					, f.fragment_count as first_level_fragment_count
					, s.partition_number
		from		FragmentData s
					left join FragmentData f
						on s.index_id = f.index_id
						and s.partition_number = f.partition_number
						and f.index_level = 1
		where		s.index_level = 0
	)
	, Maxed as (
		select		s.index_id
					, s.leaf_frag
					, s.leaf_page_count
					, s.first_level_frag
					, s.first_level_page_count
					, s.first_level_fragment_count
					, s.partition_number
					, case when s.first_level_frag > s.leaf_frag then s.first_level_frag else s.leaf_frag end as max_frag
		from		Flattened s
	)
	, Decision as (
		select		s.index_id
					, s.partition_number
					----------------------------------------------------------
					-- code section: defrag.action.decision.making
					, case
						when	s.leaf_page_count < 5
							then	@actionCodeNoAction												-- less than 1 extent, (...In general, rebuilding or reorganizing small indexes often does not reduce fragmentation, see ms-help://MS.SQLCC.v9/MS.SQLSVR.v9.en/tsqlref9/html/b796c829-ef3a-405c-a784-48286d4fb2b9.htm)
						when	(
									s.leaf_frag > @rebuildFrafmentationThreshold
								)
								or (
									s.first_level_frag > @rebuildFrafmentationThreshold				-- reorganize cannot defragment upper levels
									and s.first_level_page_count > 16								-- more than 2 extents
									and s.first_level_fragment_count > 4
								)
							then	@actionCodeRebuild												-- rebuild
						when	s.leaf_frag > @reorganizeFragmentationThreshold
							then	@actionCodeReorganize											-- reorganize
						else		@actionCodeNoAction												-- do nothing
					end as ActionCode
					-- end code section: defrag.action.decision.making
					------------------------------------------------------------
		from		Maxed s
	)
	insert	@tbAction (
				index_id
				, partition_number
				, action_code
			)
	select		t.index_id
				, case when ps.name is not null then t.partition_number else 0 end	-- set partition number to 0 if not partitioned
				, t.ActionCode
	from		Decision t
				inner join sys.indexes i
					on i.index_id = t.index_id
					and i.object_id = @objectID
				left join sys.partition_schemes ps
					on ps.data_space_id = i.data_space_id
	where		ActionCode > 0;

	declare		myc
	cursor local for
	select		quotename(i.name) as IndexName
				, a.partition_number
				, a.action_code
				, i.allow_page_locks
				, a.index_id
				--, case when ps.[name] is not null then convert(bit, 1) else 0 end as IsPartitioned
	from		@tbAction a
				inner join sys.indexes i
					on i.index_id = a.index_id
	where		i.object_id = @objectID
				
	open myc;
	
	declare	@indexID int;
	declare	@partitionNo int;
	declare @indexName sysname;
	declare @actionCode smallint;
	declare @pageLocksAllowed bit;
	
	declare @sql nvarchar(max);
	
	declare @tbResults table (
		IndexID int not null
		, PartitionNumber int not null
		, ActionName nvarchar(50) not null
		, ActionCode smallint not null
		, Succeeded bit not null default 0
		, ErrorMessage nvarchar(max) null
		, WarningMessage nvarchar(max) null
		, StartTime datetime not null default getdate()
		, EndTime datetime null
		, primary key (IndexID, PartitionNumber)
	);

	while 1 = 1
	begin
		fetch next from myc into @indexName, @partitionNo, @actionCode, @pageLocksAllowed, @indexID;
		if @@fetch_status != 0
		begin
			break;
		end
		
		declare @actionName sysname;
		declare @actionRebuild sysname;
		declare @actionReorganize sysname;
		declare @msg nvarchar(max);
		
		select		@actionRebuild = N'rebuild'
					, @actionReorganize = N'reorganize';
		
		select @actionName = case when @actionCode = @actionCodeReorganize then @actionReorganize else @actionRebuild end;
		
		insert		@tbResults (IndexID, PartitionNumber, ActionCode, ActionName)
		values		(@indexID, @partitionNo, @actionCode, @actionName); 
		
		if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		begin
			set @sql = N'alter index ' + @indexName + N' on ' + @objectName + N' set (allow_page_locks = on)';

			print N'Executing SQL: ' + @sql;

			begin try
				--print @sql;
				exec (@sql);
			end try
			begin catch
				update		@tbResults
				set			WarningMessage = N'Cannot set allow_page_locks = on, rebuilding (' + error_message() + N')'
				where		IndexID = @indexID;
				
				select		@actionCode = @actionCodeRebuild
							, @actionName = @actionRebuild;
							
				update		@tbResults
				set			ActionCode = @actionCode
				where		IndexID = @indexID;
			end catch
		end -- if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		
		select	@sql = N'alter index '
			+ @indexName
			+ N' on '
			+ @objectName
			+ N' '
			+ @actionName
			+ case
				when @partitionNo > 0
					then N' partition = ' + convert(nvarchar(10), @partitionNo)
					else N''
				end;

		print N'Executing SQL: ' + @sql;
		
		begin try
			exec (@sql);

			update		@tbResults
			set			Succeeded = 1
						, EndTime = getdate()
			where		IndexID = @indexID;
		end try
		begin catch
			update		@tbResults
			set			ErrorMessage = error_message()
			where		IndexID = @indexID;
		end catch
		
		if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		begin
			set @sql = N'alter index ' + @indexName + N' on ' + @objectName + N' set (allow_page_locks = off)';

			print N'Executing SQL: ' + @sql;

			begin try
				exec (@sql);
				
				update		@tbResults
				set			EndTime = getdate()
				where		IndexID = @indexID;
				
			end try
			begin catch
				set @msg = N'Cannot restore allow_page_locks = off (' + error_message() + N')';
				
				update		@tbResults
				set			WarningMessage = 
								case when WarningMessage is null
									then @msg
									else WarningMessage + N'; ' + @msg
								end
							, EndTime = getdate()
				where		IndexID = @indexID;
			end catch
		end -- if @actionCode = @actionCodeReorganize and @pageLocksAllowed = 0
		
	end -- while 1 = 1
	
	declare		@summary nvarchar(max);
	declare		@newline nchar(2);
	select		@newline = nchar(13) + nchar(10)
				, @summary = N'';
	
	select		@summary = @summary + (
					quotename(i.name)
					+ case
						when r.PartitionNumber > 0
							then N' (Partition#' + convert(nvarchar(10), r.PartitionNumber) + N')'
							else N''
						end
					+ N': '
					+ case when Succeeded = 1
						then N'success ('
							+ convert(nvarchar(20), datediff(ss, StartTime, EndTime))
							+ N' seconds'
						else N'failure ('
							+ ErrorMessage
					end
					+ N')'
					+ case when WarningMessage is not null
						then N'; Warning: '
							+ WarningMessage
						else N''
					end
					+ @newline)
	from		@tbResults r
				, sys.indexes i
	where		i.object_id = @objectID
				and i.index_id = r.IndexID;
	
	select		@indexesDefragmented = isnull(sum(convert(int, Succeeded)), 0)
				, @indexesFailed = isnull(sum(convert(int, 1 - Succeeded)), 0)
				, @indexesRebuilt = isnull(sum(case when Succeeded = 1 and ActionCode = @actionCodeRebuild then convert(int, 1) else 0 end), 0)
				, @warningCount = isnull(sum(case when WarningMessage is not null then convert(int, 1) else 0 end), 0)
	from		@tbResults r;

	select @msg = N'Maintenance report for '
		 + @objectName
		 + N':'
		 + @newline
		 + N'Index partitions defragmented: '
		 + convert(nvarchar(20), @indexesDefragmented)
		 + @newline
		 + N'Index partitions rebuilt: '
		 + convert(nvarchar(20), @indexesRebuilt)
		 + @newline
		 + N'Indexes failed: '
		 + convert(nvarchar(20), @indexesFailed)
		 + @newline
		 + N'Warnings: '
		 + convert(nvarchar(20), @warningCount)
		 + @newline
		 + N'Details:'
		 + @newline
		 + isnull(@summary, N'');

	print @msg;

	select		@retcode = case when @indexesFailed = 0 then @retCodeSuccess else @retCodeGeneralFailure end;
	
end try
begin catch
	declare @tmpcode int;
	exec @tmpcode = dbo.util_rethrow_exception
	
	if @retcode = @retCodeSuccess
	begin
		select @retcode = @tmpcode;
	end
end catch

return @retcode;