﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaGetFeatured')
)
begin
	drop proc dbo.MediaGetFeatured;
end

go
--{{region:create_object}}

-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
create procedure dbo.MediaGetFeatured
AS
BEGIN

      SET NOCOUNT ON ;
      SET CONCAT_NULL_YIELDS_NULL OFF ;
    
    declare @docId int;
    SELECT top (1) @docId = documentid FROM dbo.MediaAlternateFormat WHERE MediaTypeId = 16 order by newid();
    -- Get Headline Article

      SELECT 
          dbo.Documents.DocumentId ,
          dbo.Documents.GUID ,
          dbo.Documents.Display ,
          dbo.Documents.Featured ,
          dbo.Documents.Title ,
          dbo.Documents.Author1 ,
          dbo.Documents.Author2 ,
          dbo.Documents.PubInfo ,
          dbo.Documents.metaDescription AS 'Description' ,
          dbo.Documents.metaKeywords ,
          null as metaImage ,	-- this is retrieved independently and cached
--          dbo.Documents.oldURL ,
--          dbo.Documents.oldMediaTypeId ,
          dbo.Documents.FullText ,
          dbo.Documents.Source ,
          dbo.Documents.GuideContent ,
          dbo.Documents.Length ,
          dbo.Documents.CategoryId ,
          dbo.Documents.CreateDate ,
          dbo.Documents.EditDate ,
          da.authorFirst + ' ' + da.authorMiddle + ' ' + da.authorLast AS Author ,
          da2.authorFirst + ' ' + da2.authorMiddle + ' ' + da2.authorLast AS CoAuthor ,
          '/MediaPlayer.aspx?Id=' + CAST(DocumentId AS varchar(max)) AS 'URL'
      FROM
				dbo.Documents
				JOIN dbo.DocumentAuthors da
					ON  da.AuthorId = Documents.Author1
				LEFT JOIN dbo.DocumentAuthors da2
					ON  da2.AuthorId = Documents.Author2
      WHERE		documentid = @docId;

      SELECT
          dbo.MediaAlternateFormat.MediaId ,
          dbo.MediaAlternateFormat.DocumentId ,
          dbo.MediaAlternateFormat.MediaTypeId ,
          dbo.MediaAlternateFormat.URL ,
          dbo.MediaAlternateFormat.fileSize ,
          dbo.MediaAlternateFormat.duration ,
          dbo.MediaAlternateFormat.CreateDate ,
          dbo.MediaAlternateFormat.VolumeOrdinal ,
          dbo.MediaAlternateFormat.VolumeComment ,
          dmt.IsMedia ,
          dmt.MediaTypeId ,
          dmt.Description ,
          dmt.MediaIconPath
      FROM
          dbo.MediaAlternateFormat INNER JOIN dbo.Documents
      ON  dbo.MediaAlternateFormat.DocumentId = dbo.Documents.DocumentId LEFT OUTER JOIN
      DocumentMediaType dmt
      ON  dmt.MediaTypeID = MediaAlternateFormat.MediaTypeId
      WHERE
          --Featured = 1
          Documents.documentid = @docId


END

