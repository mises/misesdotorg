﻿if exists (
	select		*
	from		sys.procedures
	where		object_id = object_id(N'dbo.defrag_database_indexes')
)
begin
	drop proc dbo.defrag_database_indexes;
end
go

/****************************************************************************************************
Author:			Vasily Kabanov
Created:		2010-06-13
Parameters:		
				@maxTableFailedToStop - int - the number of tables/views failed to defragment
					-- (indicated by an error raised in [dbo].[usp_ut_defrag_table_indexes])
					-- before aborting further work
				@rebuildFrafmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index rebuild; default is 30 (recommended by Microsoft);
					-- see [dbo].[usp_ut_defrag_table_indexes]
				@reorganizeFragmentationThreshold - float - the minimum fragmentation in percent to invoke
					-- index reorganise; default is 10 (recommended by Microsoft);
					-- must be less or equal to @rebuildFrafmentationThreshold
					-- see [dbo].[usp_ut_defrag_table_indexes]
				@tablesProcessed - int output - total number of tables or indexed views processed
					-- (for which the defragmentation procedure [dbo].[usp_ut_defrag_table_indexes]
					-- was called)
				@tablesFailed - int output - total number of tables or indexed views, during
					-- processing of which an error was raised by the defragmentation procedure
					-- [dbo].[usp_ut_defrag_table_indexes]
				@indexesDefragmented - int output - the total number of index trees defragmented
					-- (using any method); every partition represents a separate index tree
				@indexesRebuilt	- int out - of indexesDefragmented, the number of index partitions
					-- defragmented by rebuilding
				@indexesFailed - output, the number of indexes failed to defragment
				@warningCount - output, the number of non-critical failures, such as faulure
					-- resetting allow_page_locks
Returns:		return code:
					0 - success
					1 - general failure (such all of the indexes failed)
					2 - the procedure was called in transaction
Comment:		The procedure enumerates all indexed tables and views in the current database and
				invokes dbo.defrag_table_indexes for every one of them.
Examples:

exec	[dbo].defrag_database_indexes
****************************************************************************************************/
create  procedure [dbo].defrag_database_indexes
  @maxTableFailedToStop int = 10
, @rebuildFrafmentationThreshold float = 30
, @reorganizeFragmentationThreshold float = 10
, @tablesProcessed int = null output
, @tablesFailed int = null output
, @indexesDefragmented int = null output
, @indexesRebuilt	int = null output
, @indexesFailed int = null output
, @warningCount int = null output
as

set implicit_transactions off;

set nocount on;

declare		@retcode int;

select		@indexesDefragmented = 0
			, @indexesFailed = 0
			, @warningCount = 0
			, @indexesRebuilt = 0
			, @tablesProcessed = 0
			, @tablesFailed = 0
			;

declare		@indexesDefragmentedLocal int
			, @indexesRebuiltLocal	int
			, @indexesFailedLocal int
			, @warningCountLocal int

declare		@retCodeSuccess int
			, @retCodeGeneralFailure int
			, @retCodeFailCalledInTransaction int
			, @retCodeInvalidObject int
			, @retCodeInvalidThresholds int
			, @startTime datetime
			, @endTime datetime;
			
select		@retCodeSuccess = 0
			, @retCodeGeneralFailure = 1
			, @retCodeFailCalledInTransaction = 2
			, @retCodeInvalidObject = 3
			, @retCodeInvalidThresholds = 4
			, @startTime = getdate()
			;

begin try;
	select		@retcode = @retCodeSuccess;
	
	declare		@msg nvarchar(max);

	-------------------------------------
	-- checking pre-conditions
	
	if @rebuildFrafmentationThreshold is null
		or @reorganizeFragmentationThreshold is null
		or (@reorganizeFragmentationThreshold > @rebuildFrafmentationThreshold)
	begin
		set @retcode = @retCodeInvalidThresholds;
		raiserror('An invalid combination of fragmentation thresholds was specified', 16, 1);
	end

	if @@trancount > 0
	begin
		set @retcode = @retCodeFailCalledInTransaction;
		raiserror('The procedure defrag_database_indexes must not be executed in a transaction', 16, 1);
	end
	
	-- end: checking pre-conditions
	-------------------------------------
	select	@msg = N'Commencing defragmentation of indexes in database '
		+ @@servername
		+ N'\'
		+ db_name();

	print @msg;

	declare myc cursor local for
	with TablesAndViews as (
		select		object_id
					, schema_id
		from		sys.tables
		union all
		select		object_id
					, schema_id
		from		sys.views
		where		objectproperty(object_id, 'IsIndexed') = 1
	)
	select		t.object_id
				, quotename(schema_name(t.schema_id)) + N'.' + quotename(object_name(t.object_id))
					as ObjectName
	from		TablesAndViews t
	
	open myc;

	declare		@objectID int
				, @objectName sysname;

	while 1 = 1
	begin
		fetch next from myc into @objectID, @objectName;
		
		if @@fetch_status != 0
		begin
			break;
		end
		
		begin try
			exec @retcode = [dbo].[defrag_table_indexes]
				@objectID = @objectID
				, @rebuildFrafmentationThreshold = @rebuildFrafmentationThreshold
				, @reorganizeFragmentationThreshold = @reorganizeFragmentationThreshold
				, @indexesDefragmented = @indexesDefragmentedLocal out
				, @indexesRebuilt = @indexesRebuiltLocal out
				, @indexesFailed = @indexesFailedLocal out
				, @warningCount = @warningCountLocal out
				
			select	@indexesRebuilt = @indexesRebuilt + @indexesRebuiltLocal
					, @indexesDefragmented = @indexesDefragmented + @indexesDefragmentedLocal
					, @indexesFailed = @indexesFailed + @indexesFailedLocal
					, @warningCount = @warningCount + @warningCountLocal
					, @tablesProcessed = @tablesProcessed + 1
					;
				
		end try
		begin catch
			select	@tablesFailed = @tablesFailed + 1;
			
			if @tablesFailed > @maxTableFailedToStop
			begin
				select		@retcode = @retCodeGeneralFailure
							, @msg = N'Maximum failed tables exceeded, aborting defragmentation';
				raiserror(@msg, 16, 1);
			end
		end catch
	end
	
	declare		@newline nchar(2);
	select		@newline = nchar(13) + nchar(10);
	select		@endTime = getdate();

	declare		@durationFormatted nvarchar(50);
	select		@durationFormatted =
					convert(nvarchar(10), datediff(hour, @startTime, @endTime))
					+ N' hours, '
					+ convert(nvarchar(10), datediff(minute, @startTime, @endTime) % 60)
					+ N' minutes, '
					+ convert(nvarchar(10), datediff(second, @startTime, @endTime) % 60)
					+ N' seconds';

	select @msg = N'Maintenance report for database '
				+ @@servername + N'\' + db_name()
				+ N':'
				
				+ @newline
				+ N'Time taken: '
				+ @durationFormatted
				
				+ @newline
				+ N'Tables/views processed: '
				+ convert(nvarchar(max), @tablesProcessed)
				
				+ @newline
				+ N'Tables/views failed to defragment: '
				+ convert(nvarchar(max), @tablesFailed)

				+ @newline
				+ N'Total indexes defragmented: '
				+ convert(nvarchar(max), @indexesDefragmented)

				+ @newline
				+ N'Total indexes rebuilt: '
				+ convert(nvarchar(max), @indexesRebuilt)

				+ @newline
				+ N'Total indexes failed to defragment: '
				+ convert(nvarchar(max), @indexesFailed)

				+ @newline
				+ N'Total warnings: '
				+ convert(nvarchar(max), @warningCount)
			
			, @retcode = @retCodeSuccess
		;
	
	print @msg;
	
end try
begin catch
	declare @tmpcode int;
	exec @tmpcode = dbo.util_rethrow_exception
	
	if @retcode = @retCodeSuccess
	begin
		select @retcode = @tmpcode;
	end
end catch

return @retcode;