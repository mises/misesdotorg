﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaGetAuthorsAlpha')
)
begin
	drop proc dbo.MediaGetAuthorsAlpha;
end

go
--{{region:create_object}}

/**********************************************************************************************************
Author:		Vasily Kabanov
Created:	2011-10-02
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
			Reports number of documents containing audio or video files (media type mp3, wmv, mp4a) per author.
			!!TODO: check if Stream and mp4 need to be included too; it seems a concept of media type
			group/category is missing.
Old query was 
	SELECT
          d.Author1 AS AuthorId ,
          da.AuthorLast ,
          da.AuthorFirst + ' ' + da.AuthorMiddle + ' ' + da.AuthorLast AS Author ,
          COUNT(1) AS 'Count'
      FROM
          Documents d JOIN [DocumentAuthors] da
      ON  d.[Author1] = da.[AuthorId] OR d.[Author2] = da.[AuthorId] JOIN
      MediaAlternateFormat
      ON  MediaAlternateFormat.DocumentId = d.DocumentId
      WHERE
          MediaAlternateFormat.MediaTypeId IN ( 1 , 2 , 8 )          
      GROUP BY
          d.Author1 ,
          da.AuthorLast ,
          da.AuthorFirst ,
          da.AuthorMiddle        
      ORDER BY
          da.AuthorLast

Examples:

exec dbo.MediaGetAuthorsAlpha


**********************************************************************************************************/
create proc dbo.MediaGetAuthorsAlpha
as
set nocount on;

with DocumentsWithMedia as (
	select			d.DocumentId
					, d.Author1
					, d.Author2
	from			dbo.MediaDocuments d
)
, CountsFlat as (
	select			Author1 as AuthorId
					, count(*) as cnt
	from			DocumentsWithMedia
	group by		Author1
	union all
	select			Author2
					, count(*) as cnt
	from			DocumentsWithMedia
	--where			Author2 != Author1
	group by		Author2
)
, CountsTotal as (
	select			AuthorId
					, sum(cnt) as cnt
	from			CountsFlat
	group by		AuthorId
)
select		c.AuthorId
			, da.AuthorLast
			, da.AuthorFirst + coalesce(N' ' + da.AuthorMiddle + N' ', N' ') + da.AuthorLast AS Author
			, c.cnt AS [Count]
from		CountsTotal c
			, dbo.DocumentAuthors da
where		c.AuthorId = da.AuthorId;

return @@error;

