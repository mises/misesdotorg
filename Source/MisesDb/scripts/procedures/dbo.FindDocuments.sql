﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.FindDocuments')
)
begin
	drop proc dbo.FindDocuments;
end

go
--{{region:create_object}}

/**********************************************************************************************************
Author:		Vasily Kabanov
Created:	2011-10-19
Arguments	@searchString - freetext search string, max 1000 characters long
			@pageSize - max number of documents to return, default 20
			@pageNo - zero-based page number to return
			@categoryId - category of documents to return, see dbo.Documents.CategoryId
				use null or negative number to ignore
			@subjectId - subject of documents to return, see dbo.DocumentSubjectLink;
				use null or negative number to ignore
			@authorId - author of documents to return, see dbo.Documents.Author1, dbo.Documents.Author1;
				use null or negative number to ignore
			@mediaTypeIdList - media types of ducuments to return (only documents with visible media of the
				listed types will be returned); list of media type IDs, e.g. '1;2;4',
				see dbo.MediaAlternateFormat.MediaTypeId; specify empty string or null to ignore
			@idListDelimiter - the delimiter used in ID lists (e.g. @mediaTypeIdList), default is ';'
			@titleWeight - when search string is specified, assigns weight to the document title match as
				opposed to metaDescription and PubInfo, see function dbo.SearchDocuments; default is 10;
			
Returns:	return code: 0 - success; otherwise - failure
			result set (
				DocumentId	-- see dbo.Documents table, all columns
				, GUID
				, Display
				, Featured
				, Title
				, Author1
				, Author2
				, Length
				, CategoryId
				, CreateDate
				, EditDate
				, metaImage		-- always null
			) -- ordered by fulltext match rank descending (if search string is specified) and DocumentId desc
Comment:	Implements comprehensive documents search (as comprehensive as there was need and time to implement).

Examples

-----------------------------
-- all options

declare @count int;
exec dbo.FindDocuments
	@searchString = N'Austrian Perspective'
	, @categoryId = 0
	, @authorId = 299
	, @mediaTypeIdList = N'1;2;4;8;9;3'
	, @subjectId = 117
	, @pageNo = 0
	, @pageSize = 20
	, @count = @count out

print 'count = ' + convert(varchar(10), @count)
---------------------------
-- no options

declare @count int;

exec dbo.FindDocuments @pageNo = 2

print 'count = ' + convert(varchar(10), @count)
----------------
-- no search text
declare @count int;

exec dbo.FindDocuments
	@searchString = N''
	, @categoryId = 87
	, @authorId = 299
	, @mediaTypeIdList = N'1;2;4;8;'
	, @subjectId = 117
	, @pageNo = 0
	, @pageSize = 20
	, @count = @count out

print 'count = ' + convert(varchar(10), @count)
----------------
-- only search text
declare @count int;

exec dbo.FindDocuments
	@searchString = N'"Gary North"'
	, @titleWeight = 10
	, @pageNo = 0
	, @pageSize = 10
	, @mediaTypeIdList = null --N'1;2;8;14'
	, @count = @count out

print 'count = ' + convert(varchar(10), @count)

exec dbo.FindDocuments
	@searchString = N'Gary OR North'
	, @mediaTypeIdList = N'1;2;8;14'

**********************************************************************************************************/
create proc dbo.FindDocuments
	@searchString nvarchar(1000) = null
	, @pageSize int = 20
	, @pageNo int = 0
	, @categoryId int = null
	, @subjectId int = null
	, @authorId int = null
	, @mediaTypeIdList nvarchar(200) = null
	, @idListDelimiter nvarchar(4) = N';'
	, @titleWeight float = 10
	, @count int = null out
as
set nocount on;
set fmtonly off;

declare @sql nvarchar(max);

-- each CTE must return unique set of IDs
-- category hierarchy being correctly configured (e.g. no circular references)
select	@sql = N'
with CategoryTree as (
	select		@categoryId as TopCategoryId
				, @categoryId as CategoryId
	union all
	select		p.TopCategoryId
				, ch.CategoryId
	from		dbo.MediaCategory ch
				, CategoryTree p
	where		p.CategoryId = ch.ParentCategory
)
, ByCategory as (
	select			d.DocumentId
	from			dbo.Documents d
					, CategoryTree t
	where			d.CategoryId = t.CategoryId
)
, BySubject as (
	select			DocumentId
	from			dbo.DocumentSubjectLink
	where			SubjectID = @subjectId
)
, ByAuthor as (
	select			DocumentId
	from			dbo.Documents
	where			Author1 = @authorId
	union
	select			DocumentId
	from			dbo.Documents
	where			Author2 = @authorId
)
, ByMediaType as (
	select			distinct
					DocumentId
	from			dbo.MediaAlternateFormat f
					, dbo.SplitUniqueIntTable(@mediaTypeIdList, @idListDelimiter, 0) i
	where			f.MediaTypeId = i.val
)';

select	@searchString = dbo.GetContainsQuery(@searchString)
		, @mediaTypeIdList = ltrim(rtrim(@mediaTypeIdList));

-- a number of CTEs, one of them called "Ordered"
declare		@sqlAllIdsOrdered	nvarchar(max);
-- a number of CTEs, one of them called "IdPage"
declare		@sqlIdPage	nvarchar(max);

declare @from nvarchar(1000);
declare @where nvarchar(2000);

select	@from = N''
		, @where = N'';

declare		@searchStringLength int;
select		@searchStringLength = coalesce(len(@searchString), 0)
			, @categoryId = coalesce(@categoryId, -1)
			, @subjectId = coalesce(@subjectId, -1)
			, @authorId = coalesce(@authorId, -1)
			, @mediaTypeIdList = coalesce(@mediaTypeIdList, N'');

if @searchStringLength = 0
	and @categoryId < 0
	and @subjectId < 0
	and @authorId < 0
	and @mediaTypeIdList = N''
begin
	-- no search criteria, just return a page of all documents sorted by DocumentId (which should produce the same order
	-- as CreateDate, but will be cheaper)
	select @sqlAllIdsOrdered = N'
with Ordered as (
	select		DocumentId
				, row_number() over(order by DocumentId desc) as rnumber
	from		dbo.Documents
)
'
end -- if no criteria specified
else if @searchStringLength > 0
begin
	-- search text specified
	select		@sql = @sql + N'
, Ordered as (
	select			s.DocumentId
					, row_number() over (order by s.rank desc, s.DocumentId desc) as rnumber
	from			dbo.SearchDocuments(@searchString, @titleWeight) s
					join dbo.Documents d
						on d.DocumentId = s.DocumentId
						and d.Display = 1';
	
	if @categoryId >= 0
	begin
		select	@sql = @sql + N'
					join ByCategory c
						on c.DocumentId = s.DocumentId';
	end
	
	if @subjectId >= 0
	begin
		select	@sql = @sql + N'
					join BySubject sbj
						on sbj.DocumentId = s.DocumentId';
	end
	
	if @authorId >= 0
	begin
		select	@sql = @sql + N'
					join ByAuthor a
						on a.DocumentId = s.DocumentId';
	end
	
	if len(@mediaTypeIdList) > 0
	begin
		select	@sql = @sql + N'
					join ByMediaType mt
						on mt.DocumentId = s.DocumentId';
	end

	select		@sqlAllIdsOrdered = @sql + N'
)';

end -- else if len(@searchString) > 0
else
begin
	-- at least 1 advanced option (e.g. subject) is specified and no search string
	
	declare @tbTables table (
		ord tinyint not null identity(1, 1) primary key
		, tableName sysname not null	-- may be CTE
		, tableAlias sysname not null unique
	);
	
	insert	@tbTables (tableName, tableAlias)
	select	N'ByCategory', N'c'
	where	@categoryId >= 0;

	insert	@tbTables (tableName, tableAlias)
	select	N'BySubject', N'sbj'
	where	@subjectId >= 0;

	insert	@tbTables (tableName, tableAlias)
	select	N'ByAuthor', N'a'
	where	@authorId >= 0;

	insert	@tbTables (tableName, tableAlias)
	select	N'ByMediaType', N'mt'
	where	len(@mediaTypeIdList) > 0;
	
	declare @firstAlias sysname;
	
	select	@firstAlias = tableAlias
	from	@tbTables
	where	ord = 1;

	select	@from = @from
			+ tableName
			+ N' '
			+ tableAlias
			+ N'
			, '
	from	@tbTables;

	-- removing last comma
	select	@from = @from
				+ N'dbo.Documents d
';

	select	@where = @where
				+ t1.tableAlias
				+ N'.'
				+ N'DocumentId = '
				+ ta.tableAlias
				+ N'.'
				+ N'DocumentId
				and '
	from	@tbTables t1
			, @tbTables ta
	where	t1.ord = 1
			and ta.ord > 1;

	-- removing last 'and '
	select	@where = @where
				+ N'd.DocumentId = ' + @firstAlias + N'.DocumentId
				and d.Display = 1
';
	
	select		@sqlAllIdsOrdered = @sql + N'
, Ordered as (
	select		'
			+ @firstAlias + N'.DocumentId
			, row_number() over (order by ' + @firstAlias + '.DocumentId desc) as rnumber
	from	'
	+ @from
	+ N'
	where	'
	+ @where
	+ N'
)';

end

select		@sqlIdPage = @sqlAllIdsOrdered + N'
, IdPage as (
	select		top (@pageSize)
				DocumentId
				, rnumber
	from		Ordered
	where		rnumber >= @pageNo * @pageSize
)';

--------------------
-- now common final sql
declare		@sqlCount nvarchar(max);
select		@sqlCount = @sqlAllIdsOrdered + N'
select @count = count(*) from Ordered';

declare		@sqlDocPage nvarchar(max);
	select		@sqlDocPage = @sqlIdPage + N'
select		p.DocumentId
			, d.GUID
			, d.Display
			, d.Featured
			, d.Title
			, d.Author1
			, d.Author2
			, d.Length
			, d.CategoryId
			, d.CreateDate
			, d.EditDate
			, null as metaImage
from		IdPage p
			join dbo.Documents d
				on p.DocumentId = d.DocumentId
order by	rnumber
';


declare @retcode int;

print N'count sql: ' + @sqlCount;

exec @retcode = sys.sp_executesql
	@stmt = @sqlCount
	, @params = N'
@searchString nvarchar(1000)
, @categoryId int
, @subjectId int
, @authorId int
, @mediaTypeIdList nvarchar(200)
, @idListDelimiter nvarchar(4)
, @titleWeight float
, @count int out'
	, @searchString = @searchString
	, @categoryId = @categoryId
	, @subjectId = @subjectId
	, @authorId = @authorId
	, @mediaTypeIdList = @mediaTypeIdList
	, @idListDelimiter = @idListDelimiter
	, @titleWeight = @titleWeight
	, @count = @count out
;


print N'doc page: ' + @sqlDocPage;

exec @retcode = sys.sp_executesql
	@stmt = @sqlDocPage
	, @params = N'
@searchString nvarchar(1000)
, @pageSize int
, @pageNo int
, @categoryId int
, @subjectId int
, @authorId int
, @mediaTypeIdList nvarchar(200)
, @idListDelimiter nvarchar(4)
, @titleWeight float'
	, @searchString = @searchString
	, @pageSize = @pageSize
	, @pageNo = @pageNo
	, @categoryId = @categoryId
	, @subjectId = @subjectId
	, @authorId = @authorId
	, @mediaTypeIdList = @mediaTypeIdList
	, @idListDelimiter = @idListDelimiter
	, @titleWeight = @titleWeight
;

--return @@error;

