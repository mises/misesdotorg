﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaDetails')
)
begin
	drop proc dbo.MediaDetails;
end

go
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
**********************************************************************************************************/

create procedure dbo.MediaDetails
       @MediaId int
as
set concat_null_yields_null off

declare @DocumentId int

select			@DocumentId = DocumentId
from			dbo.MediaAlternateFormat
where			MediaId = @MediaId;

exec dbo.DocumentDetails @DocumentId = @DocumentId;

return @@error;