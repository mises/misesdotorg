--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.ProductGetRandom')
)
begin
	drop proc dbo.ProductGetRandom;
end

go
--{{region:create_object}}


CREATE PROCEDURE [dbo].[ProductGetRandom]
AS 
    SELECT TOP 1
            ProductId ,
            [ac_Products].Name ,
            [ac_Manufacturers].[Name] AS Author ,
            [ac_Manufacturers].[ManufacturerId] AS AuthorId ,
            [ThumbnailUrl] ,
            '/store/Product.aspx?ProductId=' + CAST (ProductId AS VARCHAR(MAX)) +  '&utm_source=Homepage&utm_medium=FeaturedProd&utm_term=Widget&utm_campaign=Featured_Widget' AS URL,
            [ThumbnailAltText] ,
            [Summary] ,
            Price
    FROM    AbleCommerce.dbo.ac_Products WITH ( NOLOCK )
            INNER JOIN AbleCommerce.dbo.[ac_Manufacturers] [ac_Manufacturers]
            WITH ( NOLOCK ) ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
    ORDER BY NEWID()
