﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.DocumentDetails')
)
begin
	drop proc dbo.DocumentDetails;
end

go
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	
**********************************************************************************************************/

create procedure dbo.DocumentDetails
       @DocumentId int = 0 ,
       @MediaId int = 0
AS
set concat_null_yields_null off

IF @MediaId  > 0
BEGIN
	EXEC dbo.MediaDetails @MediaId
	RETURN
END

DECLARE @ParentCategoryId int

SET @ParentCategoryId = ( SELECT TOP 1
                              ParentCategory
                          FROM
                              MediaCategory dc
                          WHERE
                              dc.CategoryId = ( SELECT
                                                    CategoryId
                                                FROM
                                                    dbo.Documents
                                                WHERE
                                                    DocumentId = @DocumentId ) )

SELECT
    DocumentId ,
    GUID ,
    Display ,
    Featured ,
    Title ,
    Author1 ,
    Author2 ,
    PubInfo ,
    FullText ,
    Source ,
    GuideContent ,
    Documents.CreateDate ,
    CategoryId ,
    ProductId,
    DocumentAuthors.AuthorFirst + ' ' + DocumentAuthors.AuthorMiddle + ' ' +
    DocumentAuthors.AuthorLast AS AuthorName ,
    DocumentAuthors2.AuthorFirst + ' ' + DocumentAuthors2.AuthorMiddle + ' ' +
    DocumentAuthors2.AuthorLast AS CoAuthorName ,
    @ParentCategoryId AS ParentCategoryId ,
    ( SELECT TOP 1
          ParentCategory
      FROM
          MediaCategory dc
      WHERE
          dc.CategoryId = @ParentCategoryId ) AS GrandParentCategoryId ,
    [Length] ,
    metaDescription,
    metaKeywords ,
    
    (SELECT TOP 1 Description FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreDescription,
    --SELECT ModelNumber, SearchKeywords FROM AbleCommerce.dbo.ac_Products
    (SELECT TOP 1 ModelNumber FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS ISBN,
    (SELECT TOP 1 SearchKeywords FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreKeywords,
    metaImage,    
    --REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE [Name] = Documents.Title),'~','/store/') AS CoverImage
    REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId),'~','/store/') AS CoverImage, 
    Documents.ProductId
FROM
    Documents 
 LEFT JOIN dbo.DocumentAuthors
ON  Documents.Author1 = DocumentAuthors.AuthorId
 LEFT JOIN dbo.DocumentAuthors DocumentAuthors2
ON  Documents.Author2 = DocumentAuthors2.AuthorId
WHERE
    ( DocumentId = @DocumentId )

SELECT
    DocumentSubjectLink.SubjectId
FROM
    DocumentSubjectLink JOIN DocumentSubjects
ON  DocumentSubjects.SubjectId = DocumentSubjectLink.SubjectId
WHERE
    DocumentId = @DocumentId

SELECT
    MediaAlternateFormat.MediaId ,
    MediaAlternateFormat.DocumentId ,
    MediaAlternateFormat.MediaTypeId ,
    MediaAlternateFormat.URL ,
    MediaAlternateFormat.CreateDate ,    
    DocumentMediaType.MediaTypeID AS Expr1 ,
    DocumentMediaType.MediaType ,
    DocumentMediaType.MediaIconPath ,
    DocumentMediaType.UploadPath ,
    DocumentMediaType.Description ,
    DocumentMediaType.CreateTime ,
    DocumentMediaType.MIMEtype ,
    DocumentMediaType.IsDeleted ,
    DocumentMediaType.Extensions ,
    DocumentMediaType.IsMedia ,
    MediaAlternateFormat.fileSize ,
    MediaAlternateFormat.duration,
    MediaAlternateFormat.VolumeOrdinal,
    MediaAlternateFormat.VolumeComment
FROM
    MediaAlternateFormat LEFT OUTER JOIN DocumentMediaType
ON  DocumentMediaType.MediaTypeID = MediaAlternateFormat.MediaTypeId
WHERE
    ( MediaAlternateFormat.DocumentId = @DocumentId )

return @@error;