﻿--{{region:drop_object_if_exists}}

if exists (
	select		*
	from		sys.procedures
	where		object_id = object_id(N'dbo.util_rethrow_exception')
)
begin
	drop proc dbo.util_rethrow_exception
end
go

--{{region:create_object}}
/***********************************************************************************
Author:		Vasily
Created:	4/07/2007
Comment:	The utility procedure is intended to be used in CATCH blocks
			to re-throw the caught exception after handling it
			returns error number
Examples:	
			declare @retcode int
			set @retcode = 0
			begin try
				raiserror('test', 16, 1)
				print 'must not be printed'
			end try
			begin catch
				exec @retcode = dbo.util_rethrow_exception
			end catch
			print 'retcode = ' + convert(varchar(10), @retcode)
***********************************************************************************/
create proc dbo.util_rethrow_exception
as
set nocount on
	DECLARE 
			@ErrorMessage    NVARCHAR(4000),
			@ErrorNumber     INT,
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine       INT,
			@ErrorProcedure  NVARCHAR(200);
    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	if isnull(@ErrorNumber,0) = 0
		goto _exit

    -- Building the message string that will contain original
    -- error information.
    SELECT @ErrorMessage = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	_exit:
return @ErrorNumber

go