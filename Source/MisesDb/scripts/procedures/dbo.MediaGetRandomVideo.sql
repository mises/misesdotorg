﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.procedures
	where			object_id = object_id(N'dbo.MediaGetRandomVideo')
)
begin
	drop proc dbo.MediaGetRandomVideo;
end

go
--{{region:create_object}}

/**********************************************************************************************************
Author:		
Created:	
Arguments
Returns:	return code: 0 - success; otherwise - failure
Comment:	

exec dbo.MediaGetRandomVideo
**********************************************************************************************************/
create  procedure  dbo.MediaGetRandomVideo
as

select		top (1)
			m.[MediaId],
			m.[DocumentId],
			m.[MediaTypeId],
			m.[URL],
			m.[fileSize],
			m.[duration],
			m.[CreateDate],
			m.[Display],
			m.VolumeOrdinal,
			m.VolumeComment
from		dbo.Documents d
			inner join	dbo.MediaAlternateFormat m
				on d.DocumentId = m.DocumentId
where		m.Display = 1
			and m.MediaTypeId = 16 --TODO or in (2, 14, 16) plus any new ones? Maybe need a media type type?!?
order by	newid()
