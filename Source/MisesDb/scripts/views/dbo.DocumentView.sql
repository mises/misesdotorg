﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.views
	where			object_id = object_id(N'dbo.DocumentView')
)
begin
	drop view dbo.DocumentView;
end

go
--{{region:create_object}}

CREATE VIEW [dbo].[DocumentView]
AS



SELECT DISTINCT
       Documents.DocumentId ,
       Documents.Title ,
       Documents.Source ,
       ISNULL(Documents.GUID,NEWID()) AS 'GUID',
       Documents.PubInfo AS Description ,
       media.URL ,
       DocumentMediaType.MIMEtype ,       
       Documents.Length ,
       authorFirst + ' ' + ISNULL(authorMiddle,'') + ' ' + authorLast AS 'Author',
       Documents.Author1,
       Documents.Author2,
       Documents.EditDate,
--       dsj.SubjectID,
       Documents.CreateDate ,
       media.fileSize ,
       media.duration ,
       media.MediaTypeId,
       (SELECT TOP 1 Description FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId) AS StoreDescription,
	  -- metaImage,        
    ISNULL(
    REPLACE((SELECT TOP 1 ImageUrl FROM AbleCommerce.dbo.ac_Products WHERE Documents.ProductId = AbleCommerce.dbo.ac_Products.ProductId),'~','/store/'),
    'media/poster/' + CAST(Documents.DocumentId as varchar(MAX))
    )  AS CoverImage, 
    Documents.ProductId,
       metaDescription
   FROM Documents  LEFT JOIN MediaAlternateFormat AS media
   ON  media.DocumentId = Documents.DocumentId  LEFT JOIN DocumentAuthors
   ON  Documents.Author1 = DocumentAuthors.AuthorId  
--   LEFT JOIN [DocumentSubjectLink] dsj    ON  dsj.[DocumentId] = Documents.[DocumentId]  
LEFT JOIN   DocumentMediaType
   ON  DocumentMediaType.MediaTypeID = media.MediaTypeId
   WHERE
       ( Documents.Display = 1 )-- AND media.MediaTypeId = @MediaType
       AND dbo.DocumentMediaType.IsMedia =0
   /*ORDER BY
       Documents.CreateDate DESC ,
       Documents.DocumentId DESC */





GO



