﻿--{{region:drop_object_if_exists}}

if exists (
	select			*
	from			sys.views
	where			object_id = object_id(N'dbo.MediaDocuments')
)
begin
	drop view dbo.MediaDocuments;
end

go
--{{region:create_object}}

/**********************************************************************************************************
Author:		Vasily Kabanov
Created:	2011-10-17
Columns:	see dbo.Documents, this view just filters Documents table, some presumably irrelevant columns
			are omitted for brevity purposes, but can be added.
Comment:	Returns only displayable documents with media files (mp3, wmv, mp4a, mp4)

select * from dbo.MediaDocuments
where	DocumentId < 1200

create index

**********************************************************************************************************/
create view dbo.MediaDocuments
as
select			d.DocumentId
				, d.GUID
				, d.Featured
				, d.Title
				, d.Author1
				, d.Author2
				, d.PubInfo
				, d.metaDescription
				, d.metaKeywords
				, d.metaImage
				, d._oldURL
				, d._oldMediaTypeId
				, d.Source
				, d.GuideContent
				, d.Length
				, d.CategoryId
				, d.CreateDate
				, d.EditDate
				, d.ProductId
from			dbo.Documents d
where			exists (
					select		*
					from		dbo.MediaAlternateFormat r
					where		d.DocumentId = r.DocumentId
								and r.MediaTypeId IN ( 1 , 2 , 8, 14 )
								and r.Display = 1
				)
				and d.Display = 1

