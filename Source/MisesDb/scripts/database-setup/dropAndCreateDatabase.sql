use master
print 'checking for presence of database: @MisesDatabase@'
if exists (select * from sysdatabases where name='@MisesDatabase@')
begin

print 'dropping database @MisesDatabase@'

drop database @MisesDatabase@

end

go

print 'creating database @MisesDatabase@'
create database @MisesDatabase@

go
use @MisesDatabase@
/****** Object:  FullTextCatalog [AbleCommerceFullText]    Script Date: 10/31/2009 03:16:23 ******/
CREATE FULLTEXT CATALOG [MisesFullText]
WITH ACCENT_SENSITIVITY = OFF
AS DEFAULT
AUTHORIZATION [sys]
GO

use master
print 'checking for presence of database: @AbleCommerceDatabase@'
if exists (select * from sysdatabases where name='@AbleCommerceDatabase@')
begin

print 'dropping database @AbleCommerceDatabase@'

drop database @AbleCommerceDatabase@

end

go

print 'creating database @AbleCommerceDatabase@'
create database @AbleCommerceDatabase@

go
use @AbleCommerceDatabase@
/****** Object:  FullTextCatalog [MisesFullText]    Script Date: 10/31/2009 03:16:23 ******/
CREATE FULLTEXT CATALOG [MisesFullText]
WITH ACCENT_SENSITIVITY = OFF
AS DEFAULT
AUTHORIZATION [sys]
GO