﻿#region

using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using Mises.Data;
using Mises.MetaParser.TextParsing;


#endregion

namespace Mises.MetaParser.Imaging
{
    public class ArticleImageSearch
    {
        /// <summary>
        ///   Hardcoded to only get missing images
        /// </summary>
        /// <param name = "getMissingDataOnly"></param>
        public static void GetArticleImages(bool getMissingDataOnly)
        {
            int num = 0;
            DateTime now = DateTime.Now;
            Console.WriteLine("Begin conversion..." + DateTime.Now);
            ParserUtility.WriteLog("Begin conversion..." + DateTime.Now);

            var db = DataHelper.MisesDataContext;

            var dailys = from daily in db.DailyArticleTBs.OrderByDescending(a=> a.ArticleId)
                         //where daily.PhotoURL.Length == 0                         
                         select new
                                    {
                                        daily.ArticleId,
                                        daily.PhotoURL,
                                        daily.ArticleText,
                                        daily.AuthorId
                                    };
                                    

            dailys.ToList().ForEach(row =>
                                        {
                                            try
                                            {
                                                bool dontCreateImage = false;

                                                string photoURL = row.PhotoURL;
                                                if (string.IsNullOrEmpty(photoURL))
                                                {
                                                    photoURL = ParserUtility.FindValue(row.ArticleText, @"src=""", @"""");
                                                    if (photoURL.Contains("\""))
                                                    {
                                                        photoURL = photoURL.Substring(0, photoURL.IndexOf("\""));
                                                    }

                                                    if (photoURL.Length > 255) photoURL = null;

                                                    string url = ParserUtility.FindValue(row.ArticleText, @"href=""", @"""");
                                                    if (url.EndsWith(".jpg") || url.EndsWith(".gif") ||
                                                        url.EndsWith(".png") ||
                                                        url.EndsWith(".jpeg"))
                                                    {
                                                        photoURL = url;
                                                    }


                                                    if (string.IsNullOrEmpty(photoURL) && row.AuthorId > 0)
                                                    {
                                                        // No images found, so use author image
                                                        photoURL =
                                                            db.DocumentAuthorTBs.First(author => author.AuthorId == row.AuthorId).Photo;

                                                        dontCreateImage = true;
                                                    }

                                                    try
                                                    {
                                                        if (!string.IsNullOrEmpty(photoURL))
                                                        {
                                                            DailyArticleTB article2 =
                                                                db.DailyArticleTBs.First(article => article.ArticleId == row.ArticleId);
                                                            article2.PhotoURL = photoURL;
                                                            db.SubmitChanges();
                                                            num++;
                                                        }
                                                    }
                                                    catch
                                                    {
                                                        //  throw;
                                                    }
                                                }
                                                try
                                                {
                                                    if (!dontCreateImage)
                                                        SaveArticleHeaderImages(row.ArticleId, photoURL, 120);
                                                }
                                                catch (ArgumentException)
                                                {
                                                    string error = string.Format("Bad HTML {0} at {1}", photoURL,
                                                                                 row.ArticleId);
                                                    Console.WriteLine(error);
                                                    ParserUtility.WriteLog(error);
                                                }
                                                catch (WebException)
                                                {
                                                    string error = string.Format("404 getting {0} at {1}", photoURL,
                                                                                 row.ArticleId);
                                                    Console.WriteLine(error);
                                                    ParserUtility.WriteLog(error);
                                                }
                                                catch (SqlException)
                                                {
                                                    string error = string.Format("{0} at {1}", photoURL, row.ArticleId);
                                                    Console.WriteLine(error);
                                                    ParserUtility.WriteLog(error);
                                                }

                                                Console.WriteLine("+");
                                            }
                                            catch (Exception exception2)
                                            {
                                                ParserUtility.WriteError(exception2, string.Empty);
                                            }
                                        });

            db.SubmitChanges();

            Console.WriteLine(num + " files updated");
            ParserUtility.WriteLog(num + " files updated");
            Console.WriteLine("Time: " + ((DateTime.Now - now)));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        private static void SaveArticleHeaderImages(int articleId, string imageUrl, int height)
        {
            if (imageUrl == string.Empty) return;

            if (!imageUrl.ToLower().Contains("http:"))
                imageUrl = "http://mises.freecapitalists.org//" + imageUrl;


            //Domain.Utility.Imaging.CreateProportionalImageThumbnail(imageUrl,
            //                                                        DataHelper.WebsiteFolder  + @"images\DailyArticleImages\" +
            //                                                        articleId + ".jpg", 0, height, true);
            //Domain.Utility.Imaging.CreateProportionalImageThumbnail(imageUrl,
            //                                                        DataHelper.WebsiteFolder  + @"images\thumbnails\" + articleId +
            //                                                        ".jpg", 155, 116, true);

            Domain.Utility.Imaging.SaveImageFromUrl(imageUrl,
                                                    DataHelper.WebsiteFolder  + @"images\DailyArticleBigImages\" + articleId +
                                                    ".jpg");
        }

        public static void RegenerateThumbnails()
        {
            DateTime now = DateTime.Now;
            Console.WriteLine("Begin conversion..." + DateTime.Now);
            ParserUtility.WriteLog("Begin conversion..." + DateTime.Now);

            var db = DataHelper.MisesDataContext;

            var dailys = from daily in db.DailyArticleTBs
                         where daily.PhotoURL.Length > 0
                         select new
                                    {
                                        daily.ArticleId,
                                        daily.PhotoURL,
                                        daily.ThumbnailURL,
                                        daily.ArticleText,
                                        daily.AuthorId
                                    };

            dailys.ToList().ForEach(row =>
                                        {
                                            string photoURL = row.PhotoURL;

                                            try
                                            {
                                                SaveArticleHeaderImages(row.ArticleId, photoURL, 120);
                                                Console.WriteLine("+");
                                            }
                                            catch (ArgumentException)
                                            {
                                                string error = string.Format("Bad HTML {0} at {1}", photoURL,
                                                                             row.ArticleId);
                                                Console.WriteLine(error);
                                                ParserUtility.WriteLog(error);
                                                Console.WriteLine("html ex");
                                            }
                                            catch (WebException)
                                            {
                                                string error = string.Format("404 getting {0} at {1}", photoURL,
                                                                             row.ArticleId);
                                                Console.WriteLine(error);
                                                ParserUtility.WriteLog(error);
                                                Console.WriteLine("404 ");
                                            }
                                            catch (SqlException)
                                            {
                                                string error = string.Format("{0} at {1}", photoURL, row.ArticleId);
                                                Console.WriteLine(error);
                                                ParserUtility.WriteLog(error);
                                                Console.WriteLine("sql ex");
                                            }


                                            catch (Exception exception2)
                                            {
                                                ParserUtility.WriteError(exception2, string.Empty);
                                                Console.WriteLine("ex");
                                            }
                                        });

            db.SubmitChanges();

            Console.WriteLine("Time: " + ((DateTime.Now - now)));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        public static void DeleteOldThumbnails()
        {
            DateTime now = DateTime.Now;
            Console.WriteLine("Begin delete process..." + DateTime.Now);
            ParserUtility.WriteLog("Begin delete process" + DateTime.Now);

            var db = DataHelper.MisesDataContext;

            var dailys = from daily in db.DailyArticleTBs
                         //where daily.PhotoURL.Length == 0
                         where (daily.ThumbnailURL.Length > 5 && daily.ArticleId > 2385)
                         select new
                                    {
                                        daily.ArticleId,
                                        daily.PhotoURL,
                                        daily.ThumbnailURL,
                                        daily.ArticleText,
                                        daily.AuthorId
                                    };


            dailys.ToList().ForEach(row =>
                                        {
                                            try
                                            {
                                                Console.WriteLine(row.ThumbnailURL);
                                                ArchiveImage("images/", DataHelper.WebsiteFolder  + @"images\", row.ThumbnailURL,
                                                             row.PhotoURL, row.ArticleId);
                                                ArchiveImage("../images/", DataHelper.WebsiteFolder  + @"images\", row.ThumbnailURL,
                                                             row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("images2/", DataHelper.WebsiteFolder  + @"images2\", row.ThumbnailURL,
                                                //             row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("../images2/", DataHelper.WebsiteFolder  + @"images2\", row.ThumbnailURL,
                                                //             row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("images3/", DataHelper.WebsiteFolder  + @"images3\", row.ThumbnailURL,
                                                //             row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("../images3/", DataHelper.WebsiteFolder  + @"images3\", row.ThumbnailURL,
                                                //             row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("images/", DataHelper.WebsiteFolder  + @"images4\", row.ThumbnailURL,
                                                //             row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("../images/", DataHelper.WebsiteFolder  + @"images4\", row.ThumbnailURL,
                                                //             row.PhotoURL, row.ArticleId);
                                                ArchiveImage("http://mises.freecapitalists.org/images", DataHelper.WebsiteFolder  + @"images\",
                                                             row.ThumbnailURL, row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("http://mises.freecapitalists.org//images2/", DataHelper.WebsiteFolder  + @"images2\",
                                                //             row.ThumbnailURL, row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("http://mises.freecapitalists.org//images3/", DataHelper.WebsiteFolder  + @"images3\",
                                                //             row.ThumbnailURL, row.PhotoURL, row.ArticleId);
                                                //ArchiveImage("http://mises.freecapitalists.org//images/", DataHelper.WebsiteFolder  + @"images4\",
                                                //             row.ThumbnailURL, row.PhotoURL, row.ArticleId);
                                            }
                                            catch (Exception exception2)
                                            {
                                                ParserUtility.WriteError(exception2, string.Empty);
                                            }
                                        });

            db.SubmitChanges();

            Console.WriteLine("Time: " + ((DateTime.Now - now)));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        private static void ArchiveImage(string rootUrl, string filePath, string thumbnailUrl, string photoUrl,
                                         int ArticleId)
        {
            if (thumbnailUrl.StartsWith(rootUrl) || thumbnailUrl.StartsWith("/" + rootUrl))
            {
                var filename = thumbnailUrl.Replace(rootUrl, "");
                var originalLocation = filePath + filename;
                if (File.Exists(originalLocation))
                {
                    File.Move(originalLocation, DataHelper.WebsiteFolder  + @"images\archived\" + filename);
                    Console.WriteLine("moved:" + originalLocation);
                }


                if (!string.IsNullOrEmpty(thumbnailUrl) && !thumbnailUrl.StartsWith("/images/people/"))
                {
                    //if (!thumbnailUrl.ToLower().StartsWith("http:"))
                    //    thumbnailUrl = ("http://mises.freecapitalists.org//" + thumbnailUrl.Replace("../", "")).Replace(@"//", @"/");

                    //string mysql = string.Format("UPDATE wp_posts SET post_content = REPLACE(post_content,'{0}','http://s3.amazonaws.com/veksler-backup/DailyArticleImages/{1}.jpg');", thumbnailUrl, ArticleId);
                    //Console.WriteLine(mysql);
                    //ExecuteMysql(mysql);

                    DataHelper.ExecuteNonQuery(
                        "UPDATE dbo.DocumentAuthors SET Photo = REPLACE(Photo,@old,@new)",
                        new SqlParameter("@old", thumbnailUrl),
                        new SqlParameter("@new",
                                         string.Format("http://s3.amazonaws.com/veksler-backup/DailyArticleImages/{0}.jpg", ArticleId)));

                    //BusinessBase.ExecuteNonQuery(
                    //    "UPDATE dbo.DailyArticles SET ArticleText = REPLACE(ArticleText,@old,@new)",
                    //    new SqlParameter("@old", thumbnailUrl),
                    //    new SqlParameter("@new", string.Format("http://s3.amazonaws.com/veksler-backup/DailyArticleImages/{0}.jpg", ArticleId)));

                    //BusinessBase.ExecuteNonQuery(
                    //    "UPDATE dbo.DailyArticles SET ArticleText = REPLACE(ArticleText,@old,@new)",
                    //    new SqlParameter("@old", photoUrl),
                    //    new SqlParameter("@new",
                    //                     string.Format("http://s3.amazonaws.com/veksler-backup/DailyArticleBigImages/{0}.jpg",
                    //                                   ArticleId)));

                    //if (!photoUrl.ToLower().StartsWith("http:"))
                    //    photoUrl = ("http://mises.freecapitalists.org//" + photoUrl.Replace("../", "")).Replace(@"//", @"/");

                    //string mysql = string.Format("UPDATE wp_posts SET post_content = REPLACE(post_content,'{0}','http://s3.amazonaws.com/veksler-backup/DailyArticleBigImages/{1}.jpg');", photoUrl, ArticleId);
                    //Console.WriteLine(mysql);
                    //ExecuteMysql(mysql);
                }
            }

            if (photoUrl.StartsWith(rootUrl) || photoUrl.StartsWith("/" + rootUrl))
            {
                var filename = photoUrl.Replace(rootUrl, "");
                var originalLocation = filePath + filename;
                if (File.Exists(originalLocation))
                {
                    File.Move(originalLocation, DataHelper.WebsiteFolder  + @"images\archived\" + filename);
                    Console.WriteLine("moved:" + originalLocation);
                }
            }
        }

        private static void ExecuteMysql(string query)
        {
            var blog = new MySqlHelper(ConfigurationManager.ConnectionStrings["MisesBlog"].ConnectionString);
            blog.ExecuteNonQuery(query);
        }
    }
}