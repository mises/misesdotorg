#region

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using Mises.Data;

#endregion

namespace Mises.MetaParser.Imaging
{
    public class Imaging
    {
        private const string InvalidPixelFormat = "Pixel format of the image is not supported.";

        

        public static Bitmap GetImageFromURL(string url)
        {
            Bitmap bitmap;
            var client = new WebClient();
            using (Stream stream = client.OpenRead(DataFormat.GetAbsoluteURL(url)))
            {
                bitmap = new Bitmap(stream);
                stream.Close();
            }
            return bitmap;
        }

        public static Image ImageResize(Image image, int height, int width)
        {
            var bitmap = new Bitmap(width, height, image.PixelFormat);
            if (((((((bitmap.PixelFormat == PixelFormat.Format1bppIndexed) |
                     (bitmap.PixelFormat == PixelFormat.Format4bppIndexed)) |
                    (bitmap.PixelFormat == PixelFormat.Format8bppIndexed)) |
                   (bitmap.PixelFormat == PixelFormat.Undefined)) | (bitmap.PixelFormat == PixelFormat.Undefined)) |
                 (bitmap.PixelFormat == PixelFormat.Format16bppArgb1555)) |
                (bitmap.PixelFormat == PixelFormat.Format16bppGrayScale))
            {
                throw new NotSupportedException(InvalidPixelFormat);
            }
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.InterpolationMode = (InterpolationMode.HighQualityBicubic);
            graphics.DrawImage(image, 0, 0, bitmap.Width, bitmap.Height);
            graphics.Dispose();
            return bitmap;
        }

        public static Image resizeImage(Image imgToResize, Size size)
        {
            int num = imgToResize.Width;
            int num2 = imgToResize.Height;
            float num3 = 0f;
            float num4 = 0f;
            float num5 = 0f;
            num4 = ((size.Width)/((float) num));
            num5 = ((size.Height)/((float) num2));
            if (num5 < num4)
            {
                num3 = num5;
            }
            else
            {
                num3 = num4;
            }
            var width = ((int) (num*num3));
            var height = ((int) (num2*num3));
            try
            {
                return ImageResize(imgToResize, height, width);
            }
            catch
            {
                var image = new Bitmap(width, height);
                Graphics graphics = Graphics.FromImage(image);
                graphics.InterpolationMode = (InterpolationMode.HighQualityBicubic);
                graphics.DrawImage(imgToResize, 0, 0, width, height);
                graphics.Dispose();
                return image;
            }
        }
    }
}