#region

using System;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Mises.Data;
using Mises.MetaParser.Catalog;
using Mises.MetaParser.DailyArticles;
using Mises.MetaParser.Imaging;
using Mises.MetaParser.Media;
using Mises.MetaParser.Store;
using Mises.MetaParser.Tagging;
using Mises.MetaParser.TextParsing;

#endregion

namespace Mises.MetaParser
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //new CleanupDuplicateMedia().CleanupMedia();
           // return;

         
            string ConversionType = ConfigurationManager.AppSettings["ConversionType"].ToLower();
            bool GetMissingDataOnly = Convert.ToBoolean(ConfigurationManager.AppSettings["GetMissingDataOnly"]);

            switch (ConversionType)
            {
                case "PDF":
                    ConvertPDF(GetMissingDataOnly);
                    break;
                case "media":
                    UpdateMediaData(GetMissingDataOnly);
                    break;
                case "ASP":
                    ConvertASP();
                    break;
                case "authors":
                    GetAuthorImages(GetMissingDataOnly);
                    break;
                case "tags":
                    GetTags();
                    break;

                case "articleimages":
                    ArticleImageSearch.GetArticleImages(GetMissingDataOnly);
                    break;


                case "fixauthorimages":
                    FixAuthorImages();
                    break;

                case "fixdailythumbs":
                    MissingFileFixer.FixArticleThumbnails();
                    break;

                case "cataloggenerator":
                    new CatalogBuilder().Generate();
                    break;
                case "dailyhardcopy":
                    new DailyHardCopyBuilder().Generate();
                    break;


                case "regeneratethumbnails":
                    ArticleImageSearch.RegenerateThumbnails();
                    break;

                case "deletethumbnails":
                    ArticleImageSearch.DeleteOldThumbnails();
                    break;

                case "updatedigitalgoods":
                    UpdateDigitalGoods.Process();
                    break;

                case "streamingmedia":
                    MediaServices.MediaServices.GenerateStreamingEndpoints();
                    break;

                case "epub":
                    GetEpubMetaData(GetMissingDataOnly);
                    break;

                case "all":

                    //ArticleImageSearch.GetArticleImages(GetMissingDataOnly);
                    //GetExternalFileMetaData(GetMissingDataOnly);
                    //return;

                    GetTags();
                    GetFileMetaData(GetMissingDataOnly);
                    ConvertPDF(GetMissingDataOnly);
                    UpdateMediaData(GetMissingDataOnly);
                    GetAuthorImages(GetMissingDataOnly);
                    MediaServices.MediaServices.GenerateStreamingEndpoints();
                    GetEpubMetaData(GetMissingDataOnly);
                    break;

                default:
                    throw new Exception("Invalid/missing conversion option!");
            }
        }

        private static void GetEpubMetaData(bool getMissingDataOnly)
        {
            var db = DataHelper.MisesDataContext;

            IQueryable<DocumentFileTB> files = from mediaFiles in db.DocumentFileTBs
                                               where mediaFiles.MediaTypeId == 9
                                               select mediaFiles;

            if (getMissingDataOnly)
                files = from file in files where file.fileSize == 0 select file;

            int fileCount = 0;
            var meta = new EPubMetaParser();
            DateTime start = DateTime.Now;
            Console.WriteLine("Get Epub Meta Data..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

            ParallelQuery<DocumentFileTB> pFiles = from pRow in files.AsParallel()
                                                   select pRow;

            pFiles.ForAll(file =>
                              {
                                  try
                                  {
                                      if (meta.GetFileMetadata(file))
                                      {
                                          fileCount++;
                                          Console.Write("+");
                                      }
                                      else
                                      {
                                          Console.Write("-");
                                          ParserUtility.WriteLog("Skipped: " + file.URL);
                                      }
                                  }
                                  catch (Exception ex)
                                  {
                                      ParserUtility.WriteError(ex, string.Empty);
                                  }
                              });

            db.SubmitChanges(ConflictMode.ContinueOnConflict);

            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        private static void GetFileMetaData(bool getMissingDataOnly)
        {
            var db = DataHelper.MisesDataContext;

            IQueryable<DocumentFileTB> files = from mediaFiles in db.DocumentFileTBs
                                               where
                                                   mediaFiles.MediaTypeId != 12 &&
                                                   mediaFiles.MediaTypeId != 5 &&
                                                   mediaFiles.URL.Contains("mises.org") &&
                                                   !mediaFiles.URL.Contains(".asp")
                                               select mediaFiles;

            if (getMissingDataOnly)
                files = from file in files where file.fileSize == 0 select file;

            int fileCount = 0;
            var meta = new FileMetaParser();
            DateTime start = DateTime.Now;
            Console.WriteLine("Get File Meta Data..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

            ParallelQuery<DocumentFileTB> pFiles = from pRow in files.AsParallel()
                                                   select pRow;

            pFiles.ForAll(file =>
                              {
                                  try
                                  {
                                      if (meta.GetFileMetadata(file))
                                      {
                                          fileCount++;
                                          Console.Write("+");
                                      }
                                      else
                                      {
                                          Console.Write("-");
                                          ParserUtility.WriteLog("Skipped: " + file.URL);
                                      }
                                  }
                                  catch (Exception ex)
                                  {
                                      ParserUtility.WriteError(ex, string.Empty);
                                  }
                              });

            db.SubmitChanges(ConflictMode.ContinueOnConflict);

            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        
        ///<summary>
        ///  Gets the store tags.
        ///</summary>
        private static void GetTags()
        {
            int fileCount = 0;
            try
            {
                fileCount = TagParser.UpdateTags();
            }
            catch (Exception ex)
            {
                ParserUtility.WriteError(ex, string.Empty);
            }

            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("---------------" + Environment.NewLine);
        }


        ///<summary>
        ///  Get meta-data from PDF files
        ///</summary>
        ///<param name="getMissingDataOnly"> if set to <c>true</c> [get missing data only]. </param>
        private static void ConvertPDF(bool getMissingDataOnly)
        {
            var db = DataHelper.MisesDataContext;

            IQueryable<DocumentFileTB> files = from mediaFiles in db.DocumentFileTBs
                                               where
                                                   mediaFiles.URL.EndsWith(".pdf") &&
                                                   mediaFiles.URL.Contains("mises.org")
                                               select mediaFiles;

            if (getMissingDataOnly)
                files = from file in files where (file.fileSize == 0) select file;

            int fileCount = 0;
            var meta = new PDFmetadata();
            DateTime start = DateTime.Now;
            Console.WriteLine("Get Metadata from PDF..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

            var pFiles = (from pRow in files select pRow).ToList();

            pFiles.ForEach(file =>
                               {
                                   try
                                   {
                                       meta.URL = file.URL;
                                       meta.FileId = file.FileId;
                                       meta.DocumentId = file.DocumentId;
                                       if (meta.GetFileMetadata())
                                       {
                                           fileCount++;
                                           Console.Write("+");
                                       }
                                       else
                                       {
                                           Console.Write("-");
                                           ParserUtility.WriteLog("Skipped: " + meta.URL);
                                       }
                                   }
                                   catch (Exception ex)
                                   {
                                       ParserUtility.WriteError(ex, string.Empty);
                                   }
                               });

            Console.WriteLine(fileCount + " PDF records updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " PDF records updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }


        ///<summary>
        ///  Get meta-data from audio/visual files
        ///</summary>
        ///<param name="GetMissingDataOnly"> if set to <c>true</c> [get missing data only]. </param>
        private static void UpdateMediaData(bool GetMissingDataOnly)
        {
            int fileCount = 0;
            DateTime start = DateTime.Now;
            Console.WriteLine("Update MediaData..." + DateTime.Now.ToShortTimeString());

            var db = new MisesModel();

            var files =
                from media in
                    db.Documents.Include("DocumentFiles").Include("DocumentAuthor").Include("MediaCategory").Where(
                        d => d.DocumentFiles.Any())
                where
                    (media.DocumentFiles.Any(d => d.MediaTypeId == 1
                                                  || d.MediaTypeId == 2
                                                  || d.MediaTypeId == 8 ||
                                                  d.MediaTypeId == 14)
                     //&& media.DocumentFiles.Any(d => d.fileSize == 0))
                     && media.CoverImage == null
                     
                     )
                select media;
            

            //if (GetMissingDataOnly)
            //    files = files.Where(f => f.fileSize == 0);

            //var pFiles = from file in files.AsParallel() select file;

            files.ToList().ForEach(row => row.DocumentFiles.ToList().ForEach(df => fileCount += ProcessMediaFile(row, df)));

            if (fileCount > 0)
                DataHelper.ExecuteNonQuery("_MediaCategoryUpdateImages", null);

            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        private static int ProcessMediaFile(Document row, DocumentFile df)
        {
            try
            {
                var file = new MediaFile
                               {
                                   URL = df.URL.Contains("mises.org:88")? row.Source: df.URL,
                                   FileId =df.FileId,
                                   DocumentId=row.DocumentId,
                                   Description=row.Description,
                                   Keywords =row.Keywords,
                               };
                
                bool? updated = file.UpdateProperties();

                switch (updated)
                {
                    case true:
                        Console.Write("+");
                        return 1;
                    case null:
                        break;
                    case false:
                        Console.Write("-");
                        ParserUtility.WriteLog(
                            "Skipped: " + file.URL);
                        return 0;
                }
            }
            catch (Exception ex)
            {
                ParserUtility.WriteError(ex, string.Empty);
            }
            return 0;
        }

        ///<summary>
        ///  Convert HTML/ASP pages to managed database content
        ///</summary>
        private static void ConvertASP()
        {
            int fileCount = 0;
            DateTime start = DateTime.Now;
            Console.WriteLine("Convert ASP..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());
            // Get a list of files
            String[] files =
                Directory.GetFiles(ConfigurationManager.AppSettings["TargetFolder"],
                                   ConfigurationManager.AppSettings["FileMask"], SearchOption.AllDirectories);

            foreach (HtmlPage currentPage in from t in files.AsParallel()
                                             where
                                                 (((((((((t.ToLower().EndsWith(".asp") &&
                                                          !t.Contains(
                                                              ConfigurationManager.AppSettings["ArchiveLocation"])) &&
                                                         !t.ToLower().Contains(@"\include\")) &&
                                                        !t.ToLower().Contains(@"\includes\")) &&
                                                       !t.Contains(@"\virtualtour\")) &&
                                                      !t.ToLower().Contains(@"\ewebeditpro\")) &&
                                                     !t.Contains(@"\Manager\")) &&
                                                    !t.Contains(@"\chat\")) && !t.Contains(@"\qjae518\")) &&
                                                  !t.ToLower().Contains(@"\cuteSoft_client\")) &&
                                                 !t.Contains(@"journals\")
                                             select new HtmlPage
                                                        {
                                                            InitialDirectory =
                                                                ConfigurationManager.AppSettings["TargetFolder"],
                                                            ArchiveLocation =
                                                                ConfigurationManager.AppSettings["ArchiveLocation"],
                                                            FileLocation = t
                                                        })
            {
                Console.WriteLine(currentPage.FileLocation);
                ParserUtility.WriteLog(currentPage.FileLocation);

                try
                {
                    currentPage.processFile();
                }
                catch (Exception ex)
                {
                    ParserUtility.WriteError(ex, string.Empty);
                }

                fileCount++;
                Console.WriteLine("---------------" + Environment.NewLine);
            }

            Console.WriteLine(fileCount + " files");
            Console.WriteLine("Time: " + (DateTime.Now - start));
        }


        private static void GetAuthorImages(bool GetMissingDataOnly)
        {
            int fileCount = 0;
            DateTime start = DateTime.Now;
            Console.WriteLine("Get author images..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

            string sql = GetMissingDataOnly
                             ? "select AuthorId, AuthorFirst, AuthorMiddle, AuthorLast, Photo from DocumentAuthors where Photo IS NULL or Photo = '' ORDER BY AuthorLast"
                             : "select AuthorId, AuthorFirst, AuthorMiddle, AuthorLast, Photo from DocumentAuthors";

            DataTable authors =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text, sql).Tables[0];

            string TargetFolder = ConfigurationManager.AppSettings["TargetFolder"];
            string[] files = Directory.GetFiles(TargetFolder);
            int authorsNotFound = 0;

            foreach (DataRow row in authors.Rows.AsParallel())
            {
                try
                {
                    String First = row["AuthorFirst"].ToString();
                    String Middle = row["AuthorMiddle"].ToString();
                    String Last = row["AuthorLast"].ToString();
                    Int32 AuthorId = Int32.Parse(row["AuthorId"].ToString());

                    String fileName = string.Empty;
                    String fileExtention = string.Empty;


                    //  Does this person have an image?
                    foreach (string mFile in files)
                    {
                        fileName = mFile.Replace(TargetFolder, string.Empty).Replace("\\", string.Empty);
                        fileName = fileName.Substring(0, fileName.Length - 4);
                        fileExtention = mFile.Substring(mFile.Length - 4);

                        string[] authorName = fileName.Split(Convert.ToChar("_"));

                        if (authorName.Length == 1 &&
                            (String.Compare(authorName[0], Last, true) == 0)
                            ||
                            (authorName.Length == 2 && String.Compare(authorName[0], First, true) == 0
                             && String.Compare(authorName[1], Last, true) == 0)
                            ||
                            (authorName.Length == 2 && String.Compare(authorName[0], Last, true) == 0
                             && String.Compare(authorName[1], First, true) == 0)
                            ||
                            (authorName.Length == 3 && String.Compare(authorName[0], Last, true) == 0
                             && String.Compare(authorName[1], Middle, true) == 0
                             && String.Compare(authorName[1], First, true) == 0
                            )
                            ||
                            (authorName.Length == 3 && String.Compare(authorName[0], First, true) == 0
                             && String.Compare(authorName[1], Middle, true) == 0
                             && String.Compare(authorName[1], Last, true) == 0
                            )
                            )
                        {
                            // Match found
                            break;
                        }
                        else
                        {
                            // No match
                            fileName = string.Empty;
                        }
                    }

                    if (fileName != string.Empty)
                    {
                        fileName = fileName + fileExtention;
                        Console.WriteLine(fileName);

                        const string sqlUpdate = "UPDATE DocumentAuthors SET Photo=@PhotoURL WHERE AuthorId =@AuthorId";

                        String PhotoURL = "/images/people/" + fileName;
                        SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text, sqlUpdate,
                                                  new SqlParameter("@PhotoURL", PhotoURL),
                                                  new SqlParameter("@AuthorId", AuthorId));

                        fileCount++;
                        Console.WriteLine("+");
                    }
                    else
                    {
                        // TODO:
                        authorsNotFound++;
                        Console.WriteLine("-");
                    }
                }
                catch (Exception ex)
                {
                    ParserUtility.WriteError(ex, string.Empty);
                }
            }

            //Utility.WriteError(Utility.PadWord(First) + Utility.PadWord(Middle) + Utility.PadWord(Last));

            ParserUtility.WriteLog(string.Format("{0} author image thumbs not found.", authorsNotFound));


            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        private static void FixAuthorImages()
        {
            int fileCount = 0;
            DateTime start = DateTime.Now;
            Console.WriteLine("Fix author images..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

            var db = DataHelper.MisesDataContext;

            var authors = from author in db.DocumentAuthorTBs select author;

            authors.ToList().ForEach(author =>
                                         {
                                             if (author.Photo != null && !author.Photo.Contains("/images/people/") &&
                                                 (author.Photo.Contains("mises.freecapitalists.org/images") ||
                                                  author.Photo.Contains("/images")))
                                             {
                                                 string filename =
                                                     author.Photo.Substring(author.Photo.LastIndexOf(@"/") + 1);

                                                 string archive = DataHelper.WebsiteFolder  + @"images\archived\";
                                                 string people = DataHelper.WebsiteFolder  + @"images\people\";
                                                 const string url = "http://mises.freecapitalists.org/images/people/";

                                                 if (File.Exists(archive + filename))
                                                 {
                                                     if (!File.Exists(people + filename))
                                                         File.Copy(archive + filename, people + filename);
                                                     author.Photo = url + filename;
                                                     fileCount++;
                                                 }
                                             }
                                         });
            db.SubmitChanges();

            //Utility.WriteError(Utility.PadWord(First) + Utility.PadWord(Middle) + Utility.PadWord(Last));

            ParserUtility.WriteLog(string.Format("{0} authors found.", fileCount));

            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        private static void FixArticleThumbnails()
        {
            int fileCount = 0;
            DateTime start = DateTime.Now;
            Console.WriteLine("Fix thumbs..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

            string archive = DataHelper.WebsiteFolder  + @"images\archived\";
            string people = DataHelper.WebsiteFolder  + @"images\people\";
            string thumbs = DataHelper.WebsiteFolder  + @"images\DailyArticleImages\";

            var db = DataHelper.MisesDataContext;

            var articles = from article in db.DailyArticleTBs select new { article.ArticleId, article.ThumbnailURL };

            articles.ToList().ForEach(article =>
                                          {
                                              if (article.ThumbnailURL != null &&
                                                  (article.ThumbnailURL.Contains("mises.freecapitalists.org/images") ||
                                                   article.ThumbnailURL.Contains("/images")))
                                              {
                                                  string filename =
                                                      article.ThumbnailURL.Substring(
                                                          article.ThumbnailURL.LastIndexOf(@"/") + 1);

                                                  string thumbFileName = thumbs + article.ArticleId + ".jpg";

                                                  if (File.Exists(archive + filename))
                                                  {
                                                      if (File.Exists(thumbFileName))
                                                      {
                                                          File.Delete(thumbFileName);
                                                      }

                                                      File.Copy(archive + filename, thumbFileName);
                                                      //CreateHardLink(thumbFileName, archive + filename, IntPtr.Zero);
                                                      fileCount++;
                                                      Console.WriteLine(thumbFileName + " copied");
                                                  }

                                                  if (File.Exists(people + filename))
                                                  {
                                                      if (File.Exists(thumbFileName))
                                                      {
                                                          File.Delete(thumbFileName);
                                                      }

                                                      File.Copy(people + filename, thumbFileName);
                                                      //CreateHardLink(thumbFileName, people + filename, IntPtr.Zero);
                                                      fileCount++;
                                                      Console.WriteLine(thumbFileName + " copied");
                                                  }
                                              }
                                          });
            db.SubmitChanges();

            //Utility.WriteError(Utility.PadWord(First) + Utility.PadWord(Middle) + Utility.PadWord(Last));

            ParserUtility.WriteLog(string.Format("{0} authors found.", fileCount));

            Console.WriteLine(fileCount + " files updated");
            if (fileCount > 0) ParserUtility.WriteLog(fileCount + " files updated");
            Console.WriteLine("Time: " + (DateTime.Now - start));
            Console.WriteLine("---------------" + Environment.NewLine);
        }

        //private static void FixArticleThumbnails()
        //{
        //    int fileCount = 0;
        //    DateTime start = DateTime.Now;
        //    Console.WriteLine("Begin conversion..." + DateTime.Now.ToShortTimeString());
        //    Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());

        //    const string archive = DataHelper.WebsiteFolder  + @"images\archived\";
        //    const string people = DataHelper.WebsiteFolder  + @"images\people\";
        //    const string thumbs = DataHelper.WebsiteFolder  + @"images\DailyArticleImages\";

        //    var db = new MisesDBDataContext(BusinessBase.ConnectionString);

        //    var articles = from article in db.DailyArticleTBs select new { article.ArticleId, article.ThumbnailURL };

        //    articles.ToList().ForEach(article =>
        //                                  {
        //                                      if (article.ThumbnailURL != null &&
        //                                          (article.ThumbnailURL.Contains("mises.freecapitalists.org/images") ||
        //                                           article.ThumbnailURL.Contains("/images")))
        //                                      {
        //                                          string filename =
        //                                              article.ThumbnailURL.Substring(
        //                                                  article.ThumbnailURL.LastIndexOf(@"/") + 1);

        //                                          string thumbFileName = thumbs + article.ArticleId + ".jpg";

        //                                          if (File.Exists(archive + filename))
        //                                          {
        //                                              if (File.Exists(thumbFileName))
        //                                              {
        //                                                  File.Delete(thumbFileName);
        //                                              }

        //                                              File.Copy(archive + filename, thumbFileName);
        //                                              //CreateHardLink(thumbFileName, archive + filename, IntPtr.Zero);
        //                                              fileCount++;
        //                                              Console.WriteLine(thumbFileName + " copied");
        //                                          }

        //                                          if (File.Exists(people + filename))
        //                                          {
        //                                              if (File.Exists(thumbFileName))
        //                                              {
        //                                                  File.Delete(thumbFileName);
        //                                              }

        //                                              File.Copy(people + filename, thumbFileName);
        //                                              //CreateHardLink(thumbFileName, people + filename, IntPtr.Zero);
        //                                              fileCount++;
        //                                              Console.WriteLine(thumbFileName + " copied");
        //                                          }
        //                                      }
        //                                  });
        //    // db.SubmitChanges();

        //    //Utility.WriteError(Utility.PadWord(First) + Utility.PadWord(Middle) + Utility.PadWord(Last));

        //    Utility.WriteLog(string.Format("{0} authors found.", fileCount));

        //    Console.WriteLine(fileCount + " files updated");
        //    if (fileCount > 0) Utility.WriteLog(fileCount + " files updated");
        //    Console.WriteLine("Time: " + (DateTime.Now - start));
        //    Console.WriteLine("---------------" + Environment.NewLine);
        //}

        //[DllImport("Kernel32.dll", CharSet = CharSet.Unicode)]
        //private static extern bool CreateHardLink(
        //    string lpFileName,
        //    string lpExistingFileName,
        //    IntPtr lpSecurityAttributes
        //    );
    }
}