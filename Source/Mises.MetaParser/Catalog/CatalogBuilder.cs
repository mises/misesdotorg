﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using Mises.Data;
using Mises.MetaParser.TextParsing;
using System.Text.RegularExpressions;

#endregion

namespace Mises.MetaParser.Catalog
{
    internal class CatalogBuilder
    {

        #region Constants

        readonly string webRoot = ConfigurationManager.AppSettings["TargetFolder"];
        static readonly string DocumentFolderRoot = ConfigurationManager.AppSettings["DocumentFolderRoot"];

        private readonly string docCatalogRoot = DocumentFolderRoot + @"books\";
        private readonly string mediaCatalogRoot = DocumentFolderRoot + @"media\";

        // if true, this will MOVE the files!
        private const bool MoveFiles = true;



        private const string infoFileTemplate =
            @"
Title: 
{0}
Author: 
{4}
Updated: 
{3}
Publication Info:
{2}
Description: 
{1}
ISBN #:
{6}
Source:
{8}
Time(s):
{9}
Original URL:
http://mises.freecapitalists.org//document/{5}/{10}
{7}
";

        #endregion

        #region Properties


        string currentFolder;
        string currentDocument;

        public readonly Dictionary<string, string> UpdatedFiles = new Dictionary<string, string>();

        private Dictionary<int, MediaCategory> categories;
        public Dictionary<int, MediaCategory> Categories
        {
            get
            {
                if (categories == null)
                {
                    var model = new MisesModel();
                    categories = model.MediaCategory.Select(c => new { c.CategoryId, c }).ToDictionary(c => c.CategoryId, c => c.c);
                }
                return categories;
            }
        }

        private List<string> GetParentCategories(int categoryId)
        {
            var category = Categories.Single(c => c.Key == categoryId).Value;
            var list = new List<string> { category.Category };

            while (category.ParentCategory > 0)
            {
                category = Categories.Single(c => c.Key == category.ParentCategory).Value;
                list.Add(category.Category);
            }

            return list;
        }

        #endregion

        public void Generate()
        {
            #region Inititialize


            ParserUtility.WriteLog("*************Starting catalog builder ***************************");

            if (!Directory.Exists(docCatalogRoot))
            {
                Directory.CreateDirectory(docCatalogRoot);
            }

            if (!Directory.Exists(mediaCatalogRoot))
            {
                Directory.CreateDirectory(mediaCatalogRoot);
            }

            if (!Directory.Exists(webRoot))
            {
                throw new ArgumentException("The webRoot directory is invalid");
            }

            var model = Data.DataHelper.EntityDataModel;

            #endregion

            model.Documents.Include("DocumentFiles").Include("DocumentAuthor").Include("MediaCategory").NoTracking().Where(d => d.DocumentFiles.Any(df=> !df.URL.StartsWith("http://library.mises.org/")))
                //   .Where(d => d.DocumentFiles.Any(m => m.URL.Contains("http://socserv.mcmaster.ca/econ/ugcm/3ll3/bawerk/austrian")))
                //  .Where(d => d.DocumentId == 4059)
                // .Where(d => d.DocumentFiles.Any(df => df.MediaTypeId == 1 || df.MediaTypeId == 2))
                //.Where(d => d.Title.Contains("After Mill"))
                //.Take(250)
                 .OrderByDescending(a => a.DocumentId)
                .ToList().ForEach(doc =>
                {
                    // get a list of files
                    if (doc.DocumentFiles.Count == 0) return;

                    ParserUtility.WriteLog(string.Format("{0} : {1}", doc.DocumentId, doc.Title));

                    string authorName = (doc.DocumentAuthor.AuthorFirst +
                                        (string.IsNullOrWhiteSpace(doc.DocumentAuthor.AuthorMiddle) ? "" : " " + doc.DocumentAuthor.AuthorMiddle)
                                        + " " + doc.DocumentAuthor.AuthorLast).Trim();

                    try
                    {
                        // save them to the Library 
                        bool createMetaFiles = false;

                        doc.DocumentFiles.ToList().ForEach(
                            file =>
                            {
                                string folder;
                                string filename;
                                bool hasFiles = GetFile(doc.Title, file, authorName, doc.MediaCategory, out folder, out filename);

                                if (hasFiles)
                                    createMetaFiles = true;

                                currentFolder = folder;
                                currentDocument = filename;
                            }
                        );

                        if (createMetaFiles)
                            CreateInfoFiles(doc, authorName, currentFolder, currentDocument);

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        ParserUtility.WriteError(ex, "error saving file " + doc.Title);
                    }


                });

            ParserUtility.WriteLog("DONE!");
            Console.ReadLine();
        }

        private void CreateInfoFiles(Document doc, string authorName, string folder, string fileName)
        {
            if (string.IsNullOrWhiteSpace(folder))
            {
                Console.WriteLine("empty folder for " + doc.Title);
            }

            string imageUrl = "";

            #region Update Store Data

            string storeLink = "";
            if (doc.ProductId > 0)
            {
                int productId = (int)doc.ProductId;
                imageUrl = doc.CoverImageURL.Replace("Thumbnails", "").Replace("_T.", ".");

                storeLink = @"Mises Store:
http://store.mises.org/product.aspx?ProductId=" + productId.ToString();
            }
            // create info file

            string duration = "";
            if (doc.DocumentFiles.Any(file => file.duration > 0))
            {
                duration = doc.DocumentFiles.FirstOrDefault(f => f.duration > 0).duration.ToString();
            }

            string description = string.Format(infoFileTemplate,
                                               doc.Title,
                                               doc.Description,
                                               doc.PublicationInformation,
                                               doc.EditDate,
                                               authorName,
                                               doc.DocumentId,
                                               doc.ISBN,
                                               storeLink,
                                               doc.Source,
                                               duration,
                                               DataFormat.ConvertTextToSlug(doc.Title.ToString())
                );

            #endregion Update Store Data

            string infoPath = folder + fileName;

            if (infoPath.Length > DataFormat.maxFileLength)
            {
                infoPath = infoPath.Substring(0, DataFormat.maxFileLength);
            }

            string docPath = infoPath + "-Info.txt";
            docPath = DataFormat.SanitizePath(docPath);
            string imgPath = infoPath + ".jpg";
            File.WriteAllText(docPath, description);


            if (!(File.Exists(imgPath)))
            {
                if (doc.ProductId > 0 || doc.CoverImage != null)
                {
                    if (string.IsNullOrWhiteSpace(imageUrl))
                    {
                        imageUrl = doc.CoverImageURL;
                    }

                    Domain.Utility.Imaging.SaveImageFromUrl(imageUrl, imgPath);
                }                
            }


        }

        private bool GetFile(string title, DocumentFile file, string authorName, MediaCategory category, out string folder, out string documentName)
        {
            if (file.URL.StartsWith("http://www.youtube.com") || file.URL.Contains("mises.org:88")
                || file.URL.Contains("/page/")
                || file.URL.Contains("/daily/")
                || file.URL.Contains("http://www.ConciseGuideToEconomics.com/")
                || file.URL.Contains("/journals/")
                || file.URL.Contains("library.mises.org")
                )
            {
                folder = currentFolder;
                documentName = currentDocument;
                return false;
            }

            string directory;

            #region Get FileName

            if (category == null) // this is a book
            {
                directory = docCatalogRoot + DataFormat.SanitizePathElement(authorName) + @"\";
                documentName = DataFormat.SanitizePathElement(title);

            }
            else // this is media
            {
                directory = mediaCatalogRoot + DataFormat.SanitizePathElement(category.Category) + @"\";
                documentName = DataFormat.SanitizePathElement(title + " " + authorName);
            }

            documentName = DataFormat.SanitizeFileName(documentName);
            directory = DataFormat.SanitizePath(directory);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            #endregion Get FileName

            string oldFilePathAndName;
            DataFormat.GetPathFromURL(webRoot, file.URL, out oldFilePathAndName);

            // Get Extension

            string extension = "";
            if (file.URL.Contains("."))
            {
                extension = file.URL.Substring(file.URL.LastIndexOf(".", System.StringComparison.OrdinalIgnoreCase));
                // .asp to .html
                extension = extension.Replace(".asp", ".html").Replace(".aspx", ".html").Replace(".php", ".html").Replace(".jsp", ".html");
            }

            if (extension.Length > 5)
            { // for rewritten urls
                extension = ".html";
            }

            // Sanitize FileName

            string newPath = DataFormat.SanitizePath(directory + DataFormat.SanitizeFileName(documentName));


            // check for duplicate file
            if (File.Exists(newPath + extension) && File.Exists(oldFilePathAndName)) // duplicate file detected
            {
                Console.WriteLine("file exists, checking length for " + newPath + extension);
                var newFileProperties = new FileInfo(newPath + extension);
                if (newFileProperties.Length != new FileInfo(oldFilePathAndName).Length)
                {
                    if (file.VolumeOrdinal > 1)
                    {
                        newPath += "_Vol_" + file.VolumeOrdinal;
                    }
                    else
                    {
                        Console.WriteLine("file exists, but sizes are different! appending _2");
                        newPath += "_2";
                    }

                }
            }

            // add file extension
            newPath = newPath + extension;

            // database record already updated; only update meta files
            if (newPath == oldFilePathAndName)
            {
                ParserUtility.WriteLog(oldFilePathAndName + " already updated, skipping");
                folder = directory;
                return true;
            }


            if (File.Exists(oldFilePathAndName))
            {
                // old file exists, so move it to new folder

                if (!File.Exists(newPath))
                {
                    try
                    {
                        if (MoveFiles)
                        // ReSharper disable HeuristicUnreachableCode
                        {
                            ParserUtility.WriteLog("MOVING " + newPath);
                            File.Move(oldFilePathAndName, newPath);
                        }
                        // ReSharper restore HeuristicUnreachableCode
                        else
                        {
                            ParserUtility.WriteLog("COPYING " + newPath);
                            File.Copy(oldFilePathAndName, newPath);
                        }
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        ParserUtility.WriteError(ex, "error saving to " + newPath);

                        throw;
                    }


                    //UpdateFileUrl(file.URL, newPath, file.FileId);


                }
                else
                {
                    // delete file (for finding orphans)
                    // Utility.WriteLog("DELETING " + oldFilePathAndName);

                    //File.Delete(oldFilePathAndName);
                    //   Utility.WriteLog("skipping " + newPath);
                }

            }
            else if (!File.Exists(newPath))
            {
                // Online File; Download it:
                // but don't update DB record

                try
                {
                    // download
                    ParserUtility.WriteLog("DOWNLOADING # " + file.DocumentId + ": " + file.URL);

                    if (!file.URL.StartsWith("http:"))
                    {
                        file.URL = "http://direct.mises.org/" + file.URL;
                    }

                    var client = new WebClient();
                    //\\ hACK TODO ***************************************************************************************************************************  client.DownloadFile(file.URL, newPath);
                    //client.DownloadFile("http://local.mises.org/a.txt", newPath);

                    client.DownloadFile(file.URL, newPath);

                }
                catch (Exception ex)
                {
                    ParserUtility.WriteError(ex, "Error downloading " + file.URL);
                    File.Delete(newPath);
                }

            }
            else // file already moved and renamed
            {


                Console.WriteLine("skipping " + newPath);
            }

            if (File.Exists(newPath))
            {
                UpdateFileUrl(file.URL, newPath, file.FileId);
            }

            folder = directory;
            return true;
        }




        private void UpdateFileUrl(string oldUrl, string newPath, int FileId)
        {

            if (oldUrl.StartsWith("http://") && !oldUrl.StartsWith("http://mises.freecapitalists.org/") && !oldUrl.StartsWith("http://media.mises.org") && !oldUrl.StartsWith("http://library.mises.org"))
            {
                return;
            }

            if (oldUrl.EndsWith(".asp") || oldUrl.EndsWith(".aspx"))
                return;

            //    if (UpdatedFiles.ContainsKey(oldUrl)) return;



            string newURL = "http://library.mises.org" + newPath.Replace(docCatalogRoot, "/books/").Replace(mediaCatalogRoot, "/media/").Replace(@"\", @"/").Replace(@"//", @"/");

            //UpdatedFiles.Add(oldUrl, newPath);

            const string query = @"UPDATE DocumentFiles SET URL = @NewPath WHERE FileId = @FileId";
            int changed = SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text, query, new SqlParameter("@NewPath", newURL),
                                      new SqlParameter("@FileId", FileId));

            lock (LogFileLock)
            {
                File.AppendAllText(docCatalogRoot + "UpdatedPaths.txt", oldUrl + ", " + newURL + Environment.NewLine);
            }
        }




        public static readonly object LogFileLock = new object();
    }
}