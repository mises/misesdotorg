﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using Mises.Data;
using Mises.MetaParser.TextParsing;
using System.Text.RegularExpressions;

#endregion

namespace Mises.MetaParser.Catalog
{
    internal class DailyHardCopyBuilder
    {

        #region Constants

        private readonly string docCatalogRoot = DataHelper.DocumentFolder + @"MisesDaily\";

        private const int maxLength = 256; // maximum path length is 260 chars



        #endregion


        public void Generate()
        {
            ParserUtility.WriteLog("*************Starting daily archive builder ***************************");

            if (!Directory.Exists(docCatalogRoot))
            {
                Directory.CreateDirectory(docCatalogRoot);
            }

            if (!Directory.Exists(DataHelper.WebsiteFolder))
            {
                throw new ArgumentException("The webRoot directory is invalid");
            }

            var model = new MisesModel();

            model.DailyArticles.Include("DocumentAuthor")

                .OrderByDescending(a => a.ArticleId).Where(d => d.ShowArticle).OrderBy(d => d.DatePosted).Select(
                    d => new { d.ArticleId, d.Title, d.DocumentAuthor.AuthorFirst, d.DocumentAuthor.AuthorMiddle, d.DocumentAuthor.AuthorLast }).ToList().ForEach(doc =>
                                                 {
                                                     ParserUtility.WriteLog(doc.Title);
                                                     try
                                                     {
                                                         string authorName = doc.AuthorFirst +
                                                                         (string.IsNullOrWhiteSpace(
                                                                             doc.AuthorMiddle)
                                                                              ? ""
                                                                              : " " + doc.AuthorMiddle)
                                                                         + " " + doc.AuthorLast;


                                                         SaveDailyArticleToFile(doc.ArticleId, doc.Title, authorName);

                                                     }
                                                     catch (Exception ex)
                                                     {
                                                         Console.WriteLine(ex);
                                                         ParserUtility.WriteError(ex, "error saving file " + doc.Title);
                                                     }


                                                 });

            ParserUtility.WriteLog("DONE!");
            Console.ReadLine();
        }

        private void SaveDailyArticleToFile(int articleId, string title, string authorName)
        {
            const string url = "http://direct.mises.org/daily/{0}/?nochrome=1";
            const string filenameFormat = "{0}_{1}_{2}.html";
            string savePath = docCatalogRoot + MakeValidFileName(string.Format(filenameFormat, articleId, authorName, title));

            if (System.IO.File.Exists(savePath) && new FileInfo(savePath).Length > 0)
                return;

            var client = new WebClient();
            //client.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(client_DownloadFileCompleted);
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_DownloadStringCompleted);
            //client.DownloadFileAsync(new Uri(string.Format(url, articleId)), savePath);
            client.DownloadStringAsync(new Uri(string.Format(url, articleId)), savePath);
        }

        void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            // file downloaded
            if (e.Error != null)
            {
                ParserUtility.WriteError(e.Error, "Error downloading " + e.Error.Data);
                return;
            }

            // hardcode urls

            string savePath = e.UserState.ToString();

            var content = e.Result;

            content = content.Replace("href=\"/css/", "href=\"css\\");
            content = content.Replace("src=\"/Scripts/themes", "src=\"http://mises.freecapitalists.org//Scripts/themes");
            content = content.Replace("src=\"/Scripts/", "src=\"Scripts\\");
            content = content.Replace(@"src=""//", @"src=""http://");
            content = content.Replace(@"src=""/", @"src=""http://mises.freecapitalists.org//");


            File.WriteAllText(savePath, content);
        }

        void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            // file downloaded
            if (e.Error != null)
            {
                ParserUtility.WriteError(e.Error, "Error downloading " + e.Error.Data);
            }

            // hardcode urls


        }



        //        private void CreateInfoFile(dynamic doc, string authorName, string folder, string duration, string fileName)
        //        {
        //            //try
        //            //{
        //            //    string docPath = folder + DataFormat.ConvertTextToSlug(doc.Title) + "-" + DataFormat.ConvertTextToSlug(authorName) + ".html";
        //            //    var client = new WebClient();
        //            //    client.DownloadFile("http://mises.freecapitalists.org//document/" + doc.DocumentId, docPath);
        //            //}
        //            //catch (Exception ex)
        //            //{
        //            //    Utility.WriteError(ex, doc.DocumentId);
        //            //}            

        //            if (string.IsNullOrWhiteSpace(folder))
        //            {
        //                Console.WriteLine("empty folder for " + doc.Title);
        //            }

        //            string imageUrl = "";
        //            string storeDescription = "";
        //            string storeLink = "";
        //            if (doc.ProductId > 0)
        //            {
        //                int productId = doc.ProductId;
        //                var model = new MisesModel();
        //                var product = model.ProductsViews.SingleOrDefault(p => p.ProductId == productId);

        //                if (product != null)
        //                    storeDescription = product.Description;

        //                imageUrl = product.ImageUrl;

        //                storeLink = @"Mises Store:
        //http://store.mises.org/product.aspx?ProductId=" + productId.ToString();
        //            }
        //            // create info file
        //            string description = string.Format(infoFileTemplate,
        //                                               doc.Title,
        //                                               (!string.IsNullOrWhiteSpace(storeDescription)
        //                                                    ? storeDescription
        //                                                    : doc.Description),
        //                                               doc.PublicationInformation,
        //                                               doc.EditDate,
        //                                               authorName,
        //                                               doc.DocumentId,
        //                                               doc.ISBN,
        //                                               storeLink,
        //                                               doc.Source,
        //                                               duration
        //                );

        //            string infoPath = folder + fileName;

        //            if (infoPath.Length > maxLength)
        //            {
        //                infoPath = infoPath.Substring(0, maxLength);
        //            }
        //            string docPath = infoPath + ".txt";
        //            string imgPath = infoPath + ".jpg";
        //            File.WriteAllText(docPath,
        //                              description);

        //            if (!File.Exists(imgPath))
        //            {
        //                byte[] image = doc.CoverImage;
        //                if (image != null && image.Length > 0)
        //                {
        //                    File.WriteAllBytes(imgPath, image);
        //                }
        //                else if (!string.IsNullOrWhiteSpace(imageUrl))
        //                {
        //                    //Utility.GetFileStreamFromURL(imageUrl);
        //                    Domain.Utility.Imaging.SaveImageFromUrl(imageUrl, imgPath);
        //                }
        //            }


        //        }

        //private void GetFile(string title, DocumentFile file, string authorName, string ISBN,
        //                     MediaCategory category, out string folder, out string documentName)
        //{
        //    if (file.URL.StartsWith("http://www.youtube.com") || file.URL.Contains("mises.org:88"))
        //    {
        //        folder = currentFolder;
        //        documentName = currentDocument;
        //        return;
        //    }

        //    string directory;
        //    //if (file.MediaTypeId == 3 || file.MediaTypeId == 9 || file.MediaTypeId == 4)
        //    if (category == null) // this is a book
        //    {
        //        directory = docCatalogRoot;
        //        directory += @"\" + authorName.ToSlug() + @"\";
        //        documentName = title.ToSlug();
        //    }
        //    else // this is media
        //    {
        //    directory = mediaCatalogRoot;
        //    documentName = (title + " " + authorName).ToSlug();

        //    var parents = GetParentCategories(category.CategoryId);
        //    parents.Reverse();

        //    parents.ForEach(cat =>
        //                           {
        //                               string cat2 = cat.ToSlug();
        //                               // The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.
        //                               if (cat2.Length > 248) cat2 = cat2.Substring(0, 248);
        //                               directory += @"\" + cat2 + @"\";
        //                           });



        //    if (!Directory.Exists(directory))
        //    {
        //        Directory.CreateDirectory(directory);
        //    }




        //    if (!string.IsNullOrWhiteSpace(ISBN))
        //        documentName += "-" + ISBN;

        //    string oldFilePathAndName;
        //    Utility.GetPathFromURL(webRoot, file.URL, out oldFilePathAndName);

        //    string extension = "";
        //    if (file.URL.Contains("."))
        //    {
        //        extension = file.URL.Substring(file.URL.LastIndexOf(".")).Replace(".asp", ".html").Replace(".aspx", ".html").Replace(".php", ".html").Replace(".jsp", ".html");
        //    }

        //    if (extension.Length > 5)
        //    { // for rewritten urls
        //        extension = ".html";
        //    }

        //    string newPath = directory + MakeValidFileName(documentName);


        //    if (newPath.Length > maxLength)
        //    {
        //        newPath = newPath.Substring(0, maxLength);
        //    }

        //    // check for duplicate file
        //    if (File.Exists(newPath + extension) && File.Exists(oldFilePathAndName)) // duplicate file detected
        //    {
        //        Console.WriteLine("file exists, checking length for " + newPath + extension);
        //        var newFileProperties = new FileInfo(newPath + extension);
        //        if (newFileProperties.Length != new FileInfo(oldFilePathAndName).Length)
        //        {
        //            newPath += "-2";
        //        }
        //    }
        //    newPath = newPath + extension;


        //    if (File.Exists(oldFilePathAndName))
        //    {
        //        if (!File.Exists(newPath))
        //        {
        //            try
        //            {
        //            if (MoveFiles)
        //            // ReSharper disable HeuristicUnreachableCode
        //            {
        //                Utility.WriteLog("MOVING " + newPath);
        //                File.Move(oldFilePathAndName, newPath);
        //            }
        //            // ReSharper restore HeuristicUnreachableCode
        //            else
        //            {
        //                Utility.WriteLog("copying " + newPath);
        //                File.Copy(oldFilePathAndName, newPath);
        //            }
        //            }
        //            catch (ArgumentOutOfRangeException ex)
        //            {
        //                Utility.WriteError(ex, "error saving to " + newPath);
        //                throw;
        //            }                   


        //            UpdateFileUrl(file.URL, newPath);

        //        }
        //        else
        //        {
        //            // delete file (for finding orphans)
        //            Utility.WriteLog("DELETING " + oldFilePathAndName);
        //            File.Delete(oldFilePathAndName);
        //         //   Utility.WriteLog("skipping " + newPath);
        //        }

        //    }
        //    else
        //    {
        //        try
        //        {
        //            if (!File.Exists(newPath))
        //            {
        //                // download
        //                Utility.WriteLog("downloading " + file.URL);
        //                //Utility.GetFileStreamFromURL(DataFormat.GetAbsoluteURL(file.URL));
        //                var client = new WebClient();
        //                client.DownloadFile(file.URL, newPath);
        //            }
        //            else
        //            {
        //                Utility.WriteLog("skipping " + newPath);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Utility.WriteError(ex, "Error downloading " + file.URL);
        //        }

        //    }

        //    folder = directory;
        //}



        //private void UpdateFileUrl(string oldUrl, string newPath)
        //    {
        //        string newURL = newPath.Replace(docCatalogRoot, "/books/").Replace(mediaCatalogRoot, "/media/").Replace(@"\", @"/").Replace(@"//", @"/");
        //        UpdatedFiles.Add(oldUrl, newPath);

        //        //var db = new MisesModel();
        //        //const string cmd = "EXEC _SearchAndReplace @SearchStr = '{0}' , @ReplaceStr ='{1}'";
        //        //db.ExecuteStoreCommand(cmd, oldUrl, newPath);

        //        lock (LogFileLock)
        //        {
        //            File.AppendAllText(docCatalogRoot + "UpdatedPaths.txt", oldUrl + ", " + newURL + Environment.NewLine);
        //        }
        //    }

        private static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string validName = Regex.Replace(name, invalidReStr, "_");

            if (validName.Length > maxLength)
                validName = validName.Substring(0, maxLength);

            return validName;
        }

        //    public static readonly object LogFileLock = new object();
        //}

    }
}