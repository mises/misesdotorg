﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Mises.Data;

namespace Mises.MetaParser.Catalog
{
    public class CleanupDuplicateMedia
    {
        private List<string> newFiles = new List<string>();
        private string webRoot = ConfigurationManager.AppSettings["TargetFolder"];
        private string DocumentFolderRoot = ConfigurationManager.AppSettings["DocumentFolderRoot"];
        private string ArchiveLocation = ConfigurationManager.AppSettings["ArchiveLocation"];

        public void CleanupMedia()
        {
            var model = Data.DataHelper.EntityDataModel;

            var files = model.DocumentFiles.Where(m => m.URL.StartsWith("http://library.mises.org/books"));
            
            files.OrderByDescending(f=> f.DocumentId).ToList().ForEach(file =>
            {
                string newPath;
                if (DataFormat.GetPathFromURL(DocumentFolderRoot, file.URL, out newPath))
                {
                    var fileInfo = new FileInfo(newPath);

                    var filesInDir = fileInfo.Directory.GetFiles();

                    filesInDir.ToList().ForEach(otherFile =>
                    {
                        if (otherFile.Length == fileInfo.Length && !string.Equals(otherFile.Name,fileInfo.Name,StringComparison.CurrentCultureIgnoreCase))
                        {
                            Console.WriteLine(fileInfo.FullName);
                            Console.WriteLine(otherFile.FullName);
                            otherFile.Delete();
                        }
 
                    });
                }
            });



        }
    }
}

