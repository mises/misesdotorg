﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Mises.Data;

namespace Mises.MetaParser.Catalog
{
    public class CleanupDuplicates
    {
        List<string> newFiles = new List<string>();
        string webRoot = ConfigurationManager.AppSettings["TargetFolder"];
        string DocumentFolderRoot = ConfigurationManager.AppSettings["DocumentFolderRoot"];
        string ArchiveLocation = ConfigurationManager.AppSettings["ArchiveLocation"];

        public void Cleanup()
        {
            var model = Data.DataHelper.EntityDataModel;

            model.DocumentFiles.ToList().ForEach(file=>
                                                                                       {
                                                                                           string newPath;

                                                                                           if (DataFormat.GetPathFromURL(webRoot, file.URL, out newPath))
                                                                                           {
                                                                                               newFiles.Add(newPath);
                                                                                           }
                                                                                       });


            CleanupFiles(DocumentFolderRoot + @"media\");

        }

        private void CleanupFiles(string documentFolderRoot)
        {
            Directory.GetFiles(documentFolderRoot).ToList().ForEach(file=>
                                                                        {
                                                                            if (!newFiles.Contains(file))
                                                                            {
                                                                                var info = new FileInfo(file);

                                                                                if (info.Extension != ".jpg" && info.Extension != ".config")
                                                                                { 

                                                                                string name = info.Name;

                                                                                Console.WriteLine(file);

                                                                                if (File.Exists(ArchiveLocation + name))
                                                                                {
                                                                                    File.Delete(ArchiveLocation + name);
                                                                                }

                                                                                File.Move(file, ArchiveLocation + name);

                                                                                }
                                                                            }
                                                                        });
            Directory.GetDirectories(documentFolderRoot).ToList().ForEach(CleanupFiles);


        }
    }
}
