﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Mises.Data;
using Mises.MetaParser.TextParsing;

namespace Mises.MetaParser.DailyArticles
{
    public class MissingFileFixer
    {

        internal static void FixArticleThumbnails()
        {
            int fileCount = 0;
            DateTime start = DateTime.Now;
            Console.WriteLine("Fix thumbs..." + DateTime.Now.ToShortTimeString());
            //Utility.WriteLog("Begin conversion..." + DateTime.Now.ToShortTimeString());


            //const string imageRoot = DataHelper.WebsiteFolder  + @"images\";
            string targetImageRoot = DataHelper.WebsiteFolder  + @"images\MisesDaily\";

            ParserUtility.WriteLog("*************Starting image match ***************************");

            var db = DataHelper.MisesDataContext;

            var articles = from article in db.DailyArticleTBs select article;
            //new { article.ArticleId, article.ThumbnailURL, article.ArticleText };

            foreach (var article in articles)
            {
                var images = new Dictionary<string, string>();
                var replacements = new Dictionary<string, string>();

                // http://s3.amazonaws.com/veksler-backup/DailyArticleBigImages/2.jpg align=middle WIDTH=65 HEIGHT=39> Click here for an <a href=mu_app.asp>
                //MatchCollection matches = Regex.Matches(article.ArticleText, @"<img src=.*(?:""|\>|\s)");
                // 
                MatchCollection matches = Regex.Matches(article.ArticleText, @"src=""(?<Link>.*?)""");
                foreach (Match tag in matches)
                {
                    string imageURL = tag.Value.Replace("src=", "").Replace("\"", "").Trim('"');

                    if (!images.ContainsKey(imageURL))
                        images.Add(imageURL, "");
                }



                foreach (KeyValuePair<string, string> keyValuePair in images)
                {
                    Console.WriteLine("Starting image match for " + article.ArticleId);

                    string url = keyValuePair.Key;

                    if (url.StartsWith("http://store.mises.org/Assets/")) continue;
                    url = url.Replace("mises.freecapitalists.org/images", "mises.org/images");
                    if (url == "http://mises.freecapitalists.org//images/") continue;
                    if (url.Contains("youtube.com")) continue;
                    if (url.Contains("wp.mises.org/blog")) continue;

                    string fileName;
                    if (!DataFormat.GetPathFromURL(DataHelper.WebsiteFolder, url, out fileName))
                    {
                        // file is missing from this location

                        // fix url:
                        int s = url.LastIndexOf("/") + 1;
                        if (s < 0) s = 0;
                        string filename = url.Substring(s);
                        try
                        {
                            Domain.Utility.Imaging.SaveImageFromUrl(url, targetImageRoot + filename);
                        }
                        catch (IOException ex)
                        {
                            ParserUtility.WriteError(ex, article.ArticleId + " : " + url);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            Console.WriteLine(url);
                            ParserUtility.WriteLog(article.ArticleId + " NOT found : " + url);
                            ParserUtility.WriteError(ex, article.ArticleId + " : " + url);
                            continue;
                        }
                        string value = "http://mises.freecapitalists.org//images/MisesDaily/" + filename;

                        if (System.IO.File.Exists(targetImageRoot + filename))
                        {
                            ParserUtility.WriteLog(article.ArticleId + " found : " + url);
                            replacements.Add(url, value);
                        }


                    }
                }

                foreach (KeyValuePair<string, string> keyValuePair in replacements)
                {
                    Console.WriteLine(keyValuePair.Key);
                    article.ArticleText = article.ArticleText.Replace(keyValuePair.Key, keyValuePair.Value);
                }

                if (replacements.Any())
                {
                    db.SubmitChanges();
                    Console.WriteLine(replacements.Count + "replacements");
                }

            }
        }
    }
}
