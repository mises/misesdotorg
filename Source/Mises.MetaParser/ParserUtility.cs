#region

using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Mises.Data;
using Mises.Domain.Utility;

#endregion

namespace Mises.MetaParser.TextParsing
{
    /// <summary>
    ///   String parsing/concatenation methods for MetaParser
    /// </summary>
    internal class ParserUtility
    {
        public const StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase;



        /// <summary>
        ///   Gets the image from URL.
        /// </summary>
        /// <param name = "url">The URL.</param>
        /// <returns></returns>
        public static Stream GetFileStreamFromURL(string url)
        {
            try
            {
                var client = new WebClient();
                using (Stream stream = client.OpenRead(DataFormat.GetAbsoluteURL(url)))
                {
                    return stream;
                    //stream.Close();
                }
            }
            catch (Exception ex)
            {
                WriteError(ex, "Failed: " + url);
                return null;
            }
        }

        #region Text Parsing

        /// <summary>
        ///   Clean up HTML for parser
        /// </summary>
        /// <param name = "input">The input.</param>
        /// <returns></returns>
        public static String FormatText(ref String input)
        {
            if (-1 != input.IndexOf("<% title="))
            {
                input = input.Replace("<% title=\"", "<title>");
                int start = input.IndexOf("\" %>");
                if (start == -1)
                {
                    start = input.IndexOf("%>");
                }
                input = input.Remove(start, 4);
                input = input.Insert(start, "<title>");
            }

            var str = new StringBuilder(input);
            str.Replace("  ", " ").Replace("  ", " ");
            //str.Replace("</P></P>", "</p>").Replace("<p><p>", "<p>");
            str.Replace("<br>", "<br />").Replace("<Br>", "<br />").Replace("<BR>", "<br />");
            str.Replace("<b>", "<strong>").Replace("</b>", "</strong>");
            str.Replace("<!-- ", "<!--").Replace("<!-- ", "<!--");
            str.Replace(" -->", "-->").Replace(" -->", "-->");
            str.Replace("/include/", "include/");
            str.Replace("..include/", "include/");


            return str.ToString();
        }

        public static string[] GetSearchWords(string text)
        {
            const string pattern = @"\S+";
            var re = new Regex(pattern);

            MatchCollection matches = re.Matches(text);
            var words = new string[matches.Count];
            for (int i = 0; i < matches.Count; i++)
            {
                words[i] = matches[i].Value;
            }
            return words;
        }

        public static String FindValue(String input, string start, string end)
        {
            return FindValue(input, start, end, 0);
        }

        public static String FindValue(String input, string start, string end, int maxLength)
        {
            int startOfValue = input.IndexOf(start, comparisonType);
            string value = string.Empty;
            if (maxLength > input.Length)
            {
                maxLength = 0;
            }
            if (-1 != startOfValue)
            {
                startOfValue += start.Length;

                int endOfValue;
                if (maxLength == 0 || maxLength + startOfValue > input.Length)
                {
                    endOfValue = input.IndexOf(end, startOfValue, comparisonType);
                }
                else
                {
                    endOfValue = input.IndexOf(end, startOfValue, maxLength, comparisonType);
                }

                if (-1 == endOfValue || startOfValue == endOfValue)
                {
                    return string.Empty;
                }

                //  value = input.Substring(first + start.Length, last - first - end.Length + 1);
                value = input.Substring(startOfValue, endOfValue - startOfValue);
            }
            return value;
        }

        public static bool contains(string input, string value)
        {
            return -1 != input.IndexOf(value, comparisonType);
        }

        public static bool IsHeaderFooterFile(String filename)
        {
            filename = filename.ToLower();
            if (filename.Contains("footer") || filename.Contains("header"))
            {
                return true;
            }
            return false;
        }


        public static string PadWord(string input)
        {
            return PadWord(input, " ");
        }

        public static string PadWord(string input, string separator)
        {
            if (string.Empty != input)
            {
                return input + separator;
            }
            return string.Empty;
        }

        #endregion Text Parsing

        #region Logging

        public static readonly object LogFileLock = new object();
        public static readonly object ErrorFileLock = new object();

        internal static void WriteError(Exception ex, string p)
        {
            string error = DateTime.Now.ToString("HH:MM:s") + ": " + p + " " + ex.Message + Environment.NewLine;
            Console.WriteLine(error);

            ExceptionLogging.LogException(Assembly.GetExecutingAssembly(), ex);

            lock (ErrorFileLock)
            {
                File.AppendAllText("errors.txt", error);
            }
        }

        public static void WriteLog(string log)
        {
            log = DateTime.Now.ToString("HH:MM:s") + ": " + log;
            Console.WriteLine(log);

            lock (LogFileLock)
            {
                File.AppendAllText("log.txt", log + Environment.NewLine);
            }
        }

        #endregion
    }
}