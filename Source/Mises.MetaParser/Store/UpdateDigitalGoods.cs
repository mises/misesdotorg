﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using Mises.Data;
using Mises.MetaParser.TextParsing;

namespace Mises.MetaParser.Store
{
    public class UpdateDigitalGoods
    {
        public static void Process()
        {
            const string query =
                @"
SELECT  DISTINCT
 df.URL As LiteratureFileName
, ServerFileName As AbleFileName
FROM
	ac_Products prod1
	JOIN ac_ProductDigitalGoods
		ON ac_ProductDigitalGoods.ProductId = prod1.ProductId
	JOIN ac_DigitalGoods dg
		ON dg.DigitalGoodId = ac_ProductDigitalGoods.DigitalGoodId
	JOIN ac_Products prod2
		ON prod2.sku = REPLACE(prod1.Sku, 'EBOK', '')
	JOIN Mises..Documents AS docs
		ON prod2.ProductId = docs.ProductId
	JOIN Mises..DocumentFiles df
		ON docs.DocumentId = df.DocumentId AND df.MediaTypeId = 9
		
WHERE ServerFileName LIKE '%.epub'		

";


            string ABLE_PATH = DataHelper.StoreFolder +  @"App_Data\DigitalGoods\";
            string BACKUP_PATH = DataHelper.StoreFolder +  @"App_Data\DigitalGoods\Backups\";
            string LITERATURE_PATH = DataHelper.WebsiteFolder  + @"books\";

            int replaced = 0;
            int identical = 0;
            int missing = 0;

            Console.WriteLine("Backing up replaced files to " + BACKUP_PATH);

            string connString = ConfigurationManager.ConnectionStrings["Store"].ConnectionString;

            using (
                SafeDataReader reader = SqlHelper.ExecuteReader(connString, CommandType.Text, query))
            {
                while (reader.Read())
                {
                    string ableName = ABLE_PATH + reader.GetString("AbleFileName");
                    string litName = reader.GetString("LiteratureFileName");
                    litName = LITERATURE_PATH + DataFormat.GetFileNameFromURL(litName);
                    string backup = BACKUP_PATH + reader.GetString("AbleFileName");

                    if (File.Exists(litName))
                    {
                        if (new FileInfo(ableName).Length != new FileInfo(litName).Length)
                        {
                            if (System.IO.File.Exists(backup))
                            {
                                System.IO.File.Delete(backup);
                            }


                            ParserUtility.WriteLog("Replacing: " + ableName + " with " + litName);

                            if (File.Exists(ableName))
                            {
                                File.Move(ableName, backup);
                            }
                            File.Copy(litName, ableName, true);

                            File.Replace(litName, ableName, backup);
                            replaced++;
                        }
                        else
                        {
                            ParserUtility.WriteLog("Identical: " + ableName + " with " + litName);
                            identical++;
                        }
                    }
                    else
                    {
                        File.Copy(ableName, litName);
                        ParserUtility.WriteLog("Missing file: " + litName);
                        missing++;
                    }
                }
            }

            ParserUtility.WriteLog(string.Format("Replaced: {0} Identical: {1} Missing: {2}", replaced, identical, missing));


            Console.ReadKey();
        }
    }
}