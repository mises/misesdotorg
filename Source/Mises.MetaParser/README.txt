MetaParser - parse metadata HTML, ASP, mp2, wmv, and PDF files.

Look in App.config for connection strings and data type parameter.  
Look in Scripts.sql for Stored Procedure calls and parameters.

To view the results of a run, look in log.txt and errors.txt in the bin directory.


- David Veksler (heroic@gmail.com)
