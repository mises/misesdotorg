#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mises.Data;

#endregion

namespace Mises.MetaParser.TextParsing
{
    public class HtmlPage
    {
        public const string elementEnd = "\">";
        public const string endTag = "</body>";
        public const string endTag2 = "<!--#include virtual=\"include/footer.asp\"-->";
        public const string endTag3 = "<!--#include file=\"include/footer.asp\"-->";
        public const string header1 = "<p align=\"center\"><b>{0}</b></p>";

        public const string IsDynamic1 = "<%";
        public const string IsDynamic2 = "Server.CreateObject";
        public const string metaDesc = "<meta Name=\"DESCRIPTION\" Content=\"";
        public const string metaKeys = "<META Name=\"KEYWORDS\" Content=\"";
        public const string startTag = "<body>";
        public const string startTag2 = "<!--#include virtual=\"include/header.asp\"-->";
        public const string startTag3 = "<!--#include file=\"include/header.asp\"-->";
        public const string startTag4 = "<!--#include virtual=\"include/agdheader.asp\"-->";
        public const string titletagEnd = "</title>";
        public const string titletagStart = "<title>";
        public bool ArchiveData = true;

        public string ArchiveLocation;
        public string FileLocation;
        public String InitialDirectory;
        private string Path;
        public bool WriteToDB = true;
        private string contents;
        private string description;
        private string fileName;


        // private int Id;
        private string keywords;
        private DateTime modifydate;
        private string text;
        private string title;


        internal void processFile()
        {
            if (ConfigurationManager.AppSettings["ArchiveData"].ToLower() == "false")
            {
                ArchiveData = false;
            }

            text = File.ReadAllText(FileLocation);
            text = ParserUtility.FormatText(ref text);

            if (text.Length < 10)
            {
                Console.WriteLine("Empty file!");
                return;
            }

            if (text.Contains(IsDynamic1))
            {
                Console.Write("-- Dynamic code found: " + text.Substring(text.IndexOf(IsDynamic1), 10));
                ParserUtility.WriteLog(FileLocation + "-- Dynamic code found: " + text.Substring(text.IndexOf(IsDynamic1), 10));
                return;
            }

            GetMetaData();

            Console.WriteLine(String.Format("{0} :: {1}", FileLocation, title));

            GetContents();

            if (!SaveFile())
            {
                throw new Exception("Failed to update file: " + FileLocation);
            }

            if (ArchiveData)
            {
                ArchiveFile();
            }
        }


        public void GetMetaData()
        {
            var info = new FileInfo(FileLocation);
            modifydate = info.LastWriteTime;
            fileName = info.Name;
            Path = FileLocation.Replace(InitialDirectory, string.Empty);
            Path = Path.Replace(fileName, string.Empty);

            ArchiveLocation = InitialDirectory + ArchiveLocation + Path + fileName;

            // Title
            title = ParserUtility.FindValue(text, titletagStart, titletagEnd, 500);

            if (!ParserUtility.IsHeaderFooterFile(FileLocation) && (string.Empty == title))
            {
                //throw new Exception("Missing Title: " + FileLocation + Environment.NewLine);
                Console.WriteLine("Missing Title: " + FileLocation + Environment.NewLine);
                title = fileName;
            }
            // Bug: will fail on " /> meta tag
            description = ParserUtility.FindValue(text, metaDesc, elementEnd, 1000);
            keywords = ParserUtility.FindValue(text, metaKeys, elementEnd, 500);
        }

        private void GetContents()
        {
            String validStart = string.Empty;
            String validEnd = string.Empty;

            if (ParserUtility.contains(text, startTag))
            {
                validStart = startTag;
            }
            else if (ParserUtility.contains(text, startTag2))
            {
                validStart = startTag2;
            }
            else if (ParserUtility.contains(text, startTag3))
            {
                validStart = startTag3;
            }
            else if (ParserUtility.contains(text, startTag4))
            {
                validStart = startTag4;
            }

            if (ParserUtility.contains(text, endTag))
            {
                validEnd = endTag;
            }
            else if (ParserUtility.contains(text, endTag2))
            {
                validEnd = endTag2;
            }
            else if (ParserUtility.contains(text, endTag3))
            {
                validEnd = endTag3;
            }

            if (!ParserUtility.IsHeaderFooterFile(FileLocation) && (validStart == string.Empty || validEnd == string.Empty))
            {
                throw new Exception("Valid start/end tag not found!" + FileLocation);
            }

            contents = ParserUtility.FindValue(text, validStart, validEnd);

            if (!ParserUtility.IsHeaderFooterFile(FileLocation) && (string.Empty == contents))
            {
                throw new Exception("Missing content!" + FileLocation);
            }
        }


        private bool SaveFile()
        {
            if (!WriteToDB)
            {
                return true;
            }

            Path = Path.Replace("\\", "/");

            int RowsUpdated =
                SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.StoredProcedure, "PageAddNew",
                                          new SqlParameter("@Title", title), new SqlParameter("@Name", fileName),
                                          new SqlParameter("@Folder", Path), new SqlParameter("@Content", contents),
                                          new SqlParameter("@Keywords", keywords),
                                          new SqlParameter("@Description", description));

            if (RowsUpdated == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ArchiveFile()
        {
            var info = new FileInfo(ArchiveLocation);

            if (!Directory.Exists(info.DirectoryName))
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            File.Move(FileLocation, ArchiveLocation);
        }
    }
}