#region

using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Mises.Domain.Utility;

#endregion

namespace Mises.MetaParser.TextParsing
{
    /// <summary>
    ///   String parsing/concatenation methods for MetaParser
    /// </summary>
    internal class Utility
    {
        public const StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase;

        /// <summary>
        ///   Clean up HTML for parser
        /// </summary>
        /// <param name = "input">The input.</param>
        /// <returns></returns>
        public static String FormatText(ref String input)
        {
            if (-1 != input.IndexOf("<% title="))
            {
                input = input.Replace("<% title=\"", "<title>");
                int start = input.IndexOf("\" %>");
                if (start == -1)
                {
                    start = input.IndexOf("%>");
                }
                input = input.Remove(start, 4);
                input = input.Insert(start, "<title>");
            }

            var str = new StringBuilder(input);
            str.Replace("  ", " ").Replace("  ", " ");
            //str.Replace("</P></P>", "</p>").Replace("<p><p>", "<p>");
            str.Replace("<br>", "<br />").Replace("<Br>", "<br />").Replace("<BR>", "<br />");
            str.Replace("<b>", "<strong>").Replace("</b>", "</strong>");
            str.Replace("<!-- ", "<!--").Replace("<!-- ", "<!--");
            str.Replace(" -->", "-->").Replace(" -->", "-->");
            str.Replace("/include/", "include/");
            str.Replace("..include/", "include/");


            return str.ToString();
        }

        public static string[] GetSearchWords(string text)
        {
            const string pattern = @"\S+";
            var re = new Regex(pattern);

            MatchCollection matches = re.Matches(text);
            var words = new string[matches.Count];
            for (int i = 0; i < matches.Count; i++)
            {
                words[i] = matches[i].Value;
            }
            return words;
        }

        public static String FindValue(String input, string start, string end)
        {
            return FindValue(input, start, end, 0);
        }

        public static String FindValue(String input, string start, string end, int maxLenght)
        {
            int first = input.IndexOf(start, comparisonType);
            string value = "";
            if (maxLenght > input.Length)
            {
                maxLenght = 0;
            }
            if (-1 != first)
            {
                int last;
                if (maxLenght == 0 || maxLenght + first > input.Length)
                {
                    last = input.IndexOf(end, first, comparisonType);
                }
                else
                {
                    last = input.IndexOf(end, first, maxLenght, comparisonType);
                }

                if (-1 == last || first == last)
                {
                    return "";
                }

                //  value = input.Substring(first + start.Length, last - first - end.Length + 1);
                value = input.Substring(first + start.Length, last - first - start.Length);
            }
            return value;
        }

        public static bool contains(string input, string value)
        {
            return -1 != input.IndexOf(value, comparisonType);
        }

        public static bool IsHeaderFooterFile(String filename)
        {
            filename = filename.ToLower();
            if (filename.Contains("footer") || filename.Contains("header"))
            {
                return true;
            }
            return false;
        }


        public static bool GetPathFromURL(string webRoot, string URL, out string fileName)
        {
            //TODO: FIX to relative URL
            URL =
                URL.Replace("http://media.mises.org", "/multimedia").Replace("http://www.mises.org", "").Replace(
                    "http://mises.org", "").Replace("/", @"\");

            URL =
                URL.Replace("http://www.libertarianpapers.org/", @"F:\web\LibertarianPapers.org\articles").Replace("/", @"\");

            //http://mises.org/books/LiteratureAndLiberty.epub
            

            if (URL.StartsWith(@"\mp3\"))
            {
                URL = @"\MultiMedia" + URL;
            }

            fileName = webRoot + URL;

            if (!File.Exists(fileName))
            {
                return false;
            }
            return true;
        }

        public static string PadWord(string input)
        {
            return PadWord(input, " ");
        }

        public static string PadWord(string input, string separator)
        {
            if ("" != input)
            {
                return input + separator;
            }
            return "";
        }

        public static object LogFileLock = new object();
        public static object ErrorFileLock = new object();

        internal static void WriteError(Exception ex, string p)
        {
            ExceptionLogging.LogException(Assembly.GetExecutingAssembly(), ex);

            lock (ErrorFileLock)
            {
                File.AppendAllText("errors.txt", p + Environment.NewLine);
            }
        }

        #region Logging

        public static void WriteLog(string log)
        {
            lock (LogFileLock)
            {
                File.AppendAllText("log.txt", log + Environment.NewLine);
            }
        }

        #endregion
    }
}