﻿#region

using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using Mises.Domain.Contracts;
using Mises.Domain.Contracts.Google;

#endregion

namespace Mises.Services
{
    public class MockFromFileGoogleSearchProvider : ISearchProvider
    {
        #region ISearchProvider Members

        public PaginableResult<SearchProviderResult> Search(String encodedTerm, Int32 pageIndex, Int32 pageSize)
        {
            String responseString;

            var filepath = @"C:\temp\mockresults\results." + (pageIndex + 1);
            if (!File.Exists(filepath))
                filepath = filepath.Replace((pageIndex + 1).ToString(), "1"); // assumes this is always here

            using (var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    responseString = reader.ReadToEnd();
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            GoogleCustomSearchResponse responseJson = serializer.Deserialize<GoogleCustomSearchResponse>(responseString);

            // Transform the results into a collection of the appropriate vcards...
            //TODO allow all for now (means it doesn't mess with google paging etc) Regex regex = new Regex(@"^(http://)?[\w\.]*mises\.org/(authors|media)/\d+/[\w-]+$");
            Regex regexType = new Regex(@"^*mises.org/(authors|media)/\d+");
                //...if a match then the second group should be 'media' or 'author'
            Regex regexId = new Regex(@"^*/(\d+)/"); //...if a match then the second group should be the numeric value

            var matches = responseJson.Items /*.Where(i => regex.Match(i.Link).Success)*/.Select(i =>
                                                                                                     {
                                                                                                         var type =
                                                                                                             MisesType.
                                                                                                                 Undefined;
                                                                                                             //TODO not sure this is the best way - re-evaluate when time

                                                                                                         var id =
                                                                                                             Int32.
                                                                                                                 MinValue;
                                                                                                         Match match =
                                                                                                             regexId.
                                                                                                                 Match(
                                                                                                                     i.
                                                                                                                         Link);
                                                                                                         if (
                                                                                                             match.
                                                                                                                 Success &&
                                                                                                             match.
                                                                                                                 Groups.
                                                                                                                 Count >
                                                                                                             1)
                                                                                                             Int32.
                                                                                                                 TryParse
                                                                                                                 (match.
                                                                                                                      Groups
                                                                                                                      [1
                                                                                                                      ].
                                                                                                                      Value,
                                                                                                                  out id);

                                                                                                         var typeString
                                                                                                             =
                                                                                                             String.
                                                                                                                 Empty;
                                                                                                         match =
                                                                                                             regexType.
                                                                                                                 Match(
                                                                                                                     i.
                                                                                                                         Link);
                                                                                                         if (
                                                                                                             match.
                                                                                                                 Success &&
                                                                                                             match.
                                                                                                                 Groups.
                                                                                                                 Count >
                                                                                                             1)
                                                                                                             typeString
                                                                                                                 =
                                                                                                                 match.
                                                                                                                     Groups
                                                                                                                     [1]
                                                                                                                     .
                                                                                                                     Value;

                                                                                                         switch (typeString)
                                                                                                         {
                                                                                                             case "authors":
                                                                                                                 type =
                                                                                                                     MisesType
                                                                                                                         .
                                                                                                                         Authors;
                                                                                                                 break;
                                                                                                             case "media":
                                                                                                                 type =
                                                                                                                     MisesType
                                                                                                                         .
                                                                                                                         Media;
                                                                                                                 break;
                                                                                                         }

                                                                                                         return
                                                                                                             new SearchProviderResult
                                                                                                                 {
                                                                                                                     Type
                                                                                                                         =
                                                                                                                         type,
                                                                                                                     Id
                                                                                                                         =
                                                                                                                         id,
                                                                                                                     Link
                                                                                                                         =
                                                                                                                         i
                                                                                                                         .
                                                                                                                         Link
                                                                                                                 };
                                                                                                     });

            //TODO presuming we can't guarantee only media|authors then need to loop around the above until the required pagesize is filled
            //TODO validate matches / returns etc

            var result = new PaginableResult<SearchProviderResult>
                             {
                                 PageIndex = pageIndex,
                                 PageSize = pageSize,
                                 Items = matches.ToList(),
                                 TotalItems = responseJson.Queries.Request[0].TotalResults
                             };

            return result;
        }

        #endregion
    }
}