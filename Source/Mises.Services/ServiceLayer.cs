﻿#region

using Mises.Domain.Contracts;

#endregion

namespace Mises.Services
{
    public class ServiceLayer : IServiceLayer
    {
        public ServiceLayer(IMediaService mediaService)
        {
            MediaService = mediaService;

            this.CreateMaps();
            //TODO put somewhere else? this is as good as an entry point into this assembly so here until proves otherwise
        }

        #region IServiceLayer Members

        public IMediaService MediaService { get; private set; }

        #endregion
    }
}