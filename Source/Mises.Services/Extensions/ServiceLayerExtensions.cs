﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Mises.Data;
using Mises.Domain.Contracts;
using Mises.Domain.Documents;
using Mises.Domain.Media;
using Document = Mises.Data.Document;
using DocumentDTO = Mises.Domain.Contracts.DocumentDTO;

#endregion

namespace Mises.Services
{
    public static class ServiceLayerExtensions
    {
        internal static void CreateMaps(this ServiceLayer serviceLayer)
        {
            if (Mapper.FindTypeMapFor<PaginableResult<SearchProviderResult>, PaginableResult<DocumentDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<SearchProviderResult>, PaginableResult<DocumentDTO>>()
                    .ForMember(dest => dest.Items, opt => opt.Ignore());
            }
            if (Mapper.FindTypeMapFor<PaginableResult<SearchProviderResult>, PaginableResult<MisesDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<SearchProviderResult>, PaginableResult<MisesDTO>>()
                    .ForMember(dest => dest.Items, opt => opt.Ignore());
            }
            if (Mapper.FindTypeMapFor<SearchProviderResult, UnknownLinkDTO>() == null)
            {
                Mapper.CreateMap<SearchProviderResult, UnknownLinkDTO>();
            }

            #region Articles

            if (Mapper.FindTypeMapFor<PaginableResult<DailyArticle>, PaginableResult<ArticleSummaryDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<DailyArticle>, PaginableResult<ArticleSummaryDTO>>();
            }

            if (Mapper.FindTypeMapFor<DailyArticle, ArticleSummaryDTO>() == null)
            {
                Mapper.CreateMap<DailyArticle, ArticleSummaryDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ArticleId))
                    .ForMember(dest => dest.PostedDate, opt => opt.MapFrom(src => src.DatePosted))
                    .ForMember(dest => dest.DescriptionHtml, opt => opt.MapFrom(src => src.Description))
                    .AfterMap((src, dest) =>
                                  {
                                      // Unflatten the authors into a collection...
                                      dest.Authors = new List<AuthorSummaryDTO>
                                                         {
                                                             Mapper.Map<DocumentAuthor, AuthorSummaryDTO>(
                                                                 src.DocumentAuthor)
                                                         };
                                      if (src.DocumentAuthor1 != null)
                                          dest.Authors.Add(
                                              Mapper.Map<DocumentAuthor, AuthorSummaryDTO>(src.DocumentAuthor1));
                                  });
            }

            #endregion

            #region Documents

            #endregion

            // NEW DTOs...
            if (Mapper.FindTypeMapFor<PaginableResult<Document>, PaginableResult<DocumentDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<Document>, PaginableResult<DocumentDTO>>();
            }

            if (Mapper.FindTypeMapFor<Document, DocumentDTO>() == null)
            {
                Mapper.CreateMap<Document, DocumentDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.DocumentId))
                    .ForMember(dest => dest.Identifier, opt => opt.MapFrom(src => src.GUID))
                    .ForMember(dest => dest.PublishInfoHtml, opt => opt.MapFrom(src => src.PublicationInformation))
                    .ForMember(dest => dest.DescriptionHtml, opt => opt.MapFrom(src => src.Description))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreateDate))
                    .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.MediaCategory))
                    .AfterMap((src, dest) =>
                                  {
                                      // Unflatten the authors into a collection...
                                      dest.Authors = new List<AuthorSummaryDTO>
                                                         {
                                                             Mapper.Map<DocumentAuthor, AuthorSummaryDTO>(
                                                                 src.DocumentAuthor)
                                                         };
                                      if (src.DocumentAuthor2 != null)
                                          dest.Authors.Add(
                                              Mapper.Map<DocumentAuthor, AuthorSummaryDTO>(src.DocumentAuthor2));

                                      // Subclass each media...
                                      dest.Media = new List<MediaDTO>();
                                      src.DocumentFiles.Where(m => m.Display).ToList()
                                          //...filter out 'not-to-display' items
                                          .ForEach(m =>
                                                       {
                                                           if (VideoTypes.AsArray().Any(t => t == m.MediaTypeId))
                                                               dest.Media.Add(
                                                                   Mapper.Map<DocumentFile, VideoDTO>(m));
                                                           else if (AudioTypes.AsArray().Any(t => t == m.MediaTypeId))
                                                               dest.Media.Add(
                                                                   Mapper.Map<DocumentFile, AudioDTO>(m));
                                                           else if (LiteratureTypes.AsArray().Any(t => t == m.MediaTypeId))
                                                               dest.Media.Add(
                                                                   Mapper.Map<DocumentFile, LiteratureDTO>(m));
                                                           /*TODO other subclasses?
								else*/
                                                       });
                                  });
            }

            if (Mapper.FindTypeMapFor<PaginableResult<MediaCategory>, PaginableResult<CategorySummaryDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<MediaCategory>, PaginableResult<CategorySummaryDTO>>();
            }

            if (Mapper.FindTypeMapFor<MediaCategory, CategorySummaryDTO>() == null)
            {
                Mapper.CreateMap<MediaCategory, CategorySummaryDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CategoryId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Category))
                    .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.CategoryImage));
            }

            if (Mapper.FindTypeMapFor<PaginableResult<DocumentSubjects>, PaginableResult<SubjectSummaryDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<DocumentSubjects>, PaginableResult<SubjectSummaryDTO>>();
            }

            if (Mapper.FindTypeMapFor<DocumentSubjects, SubjectSummaryDTO>() == null)
            {
                Mapper.CreateMap<DocumentSubjects, SubjectSummaryDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.SubjectId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Subject))
                    .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Photo));
            }

            if (Mapper.FindTypeMapFor<DocumentSubjects, SubjectDTO>() == null)
            {
                Mapper.CreateMap<DocumentSubjects, SubjectDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.SubjectId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Subject))
                    .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Photo));
            }

            if (Mapper.FindTypeMapFor<MediaCategory, CategoryDTO>() == null)
            {
                Mapper.CreateMap<MediaCategory, CategoryDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CategoryId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Category))
                    .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.CategoryImage));
            }

            if (Mapper.FindTypeMapFor<DocumentFile, VideoDTO>() == null)
            {
                Mapper.CreateMap<DocumentFile, VideoDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.FileId))
                    //TODO some way to avoid this here and just use inheritance chain?
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreateDate))
                    .ForMember(dest => dest.MimeType, opt => opt.MapFrom(src => VideoTypes.ToMimeType(src.MediaTypeId)))
                    .ForMember(dest => dest.PosterUrl,
                               opt => opt.MapFrom(src => String.Concat("/media/poster/", src.DocumentId)))
                    .ForMember(dest => dest.Url, opt => opt.MapFrom(src =>
                                                                        {
                                                                            if ((src.MediaTypeId ==
                                                                                 (Int32) MediaTypes.Youtube) &&
                                                                                (src.URL.IndexOf("v=") >= 0))
                                                                                return
                                                                                    src.URL.Substring(
                                                                                        src.URL.IndexOf("v=") + 2);
                                                                            else
                                                                                return src.URL;
                                                                        }));
            }

            if (Mapper.FindTypeMapFor<DocumentFile, AudioDTO>() == null)
            {
                Mapper.CreateMap<DocumentFile, AudioDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.FileId))
                    //TODO some way to avoid this here and just use inheritance chain?
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreateDate))
                    .ForMember(dest => dest.MimeType, opt => opt.MapFrom(src => AudioTypes.ToMimeType(src.MediaTypeId)))
                    .ForMember(dest => dest.PosterUrl,
                               opt => opt.MapFrom(src => String.Concat("/media/poster/", src.DocumentId)));
            }

            if (Mapper.FindTypeMapFor<DocumentFile, LiteratureDTO>() == null)
            {
                Mapper.CreateMap<DocumentFile, LiteratureDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.FileId))
                    //TODO some way to avoid this here and just use inheritance chain?
                    //.ForMember(dest => dest.Identifier, opt => opt.MapFrom(src => src.Identifier))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreateDate))
                    .ForMember(dest => dest.MimeType,
                               opt => opt.MapFrom(src => LiteratureTypes.ToMimeType(src.MediaTypeId)))
                    .ForMember(dest => dest.PosterUrl,
                               opt => opt.MapFrom(src => String.Concat("/media/poster/", src.DocumentId)));
            }

            // OLD domain...
            if (Mapper.FindTypeMapFor<ContentDocument, ContentDocumentDTO>() == null)
            {
                Mapper.CreateMap<ContentDocument, ContentDocumentDTO>()
                    .ForMember(dest => dest.AuthorId,
                               opt => opt.MapFrom(src => src.Author1));
            }

            if (Mapper.FindTypeMapFor<MediaCategory, ContentDocumentDTO>() == null)
            {
                Mapper.CreateMap<MediaCategory, ContentDocumentDTO>()
                    .ForMember(dest => dest.CategoryTitle,
                               opt => opt.MapFrom(src => src.Category));
            }

            if (Mapper.FindTypeMapFor<DocumentFile, ContentDocumentDTO>() == null)
            {
                Mapper.CreateMap<DocumentFile, ContentDocumentDTO>()
                    .ForMember(dest => dest.CreatedDate,
                               opt => opt.MapFrom(src => src.CreateDate))
                    .ForMember(dest => dest.VideoUrl,
                               opt => opt.MapFrom(src =>
                                                      {
                                                          if ((src.MediaTypeId == (Int32) MediaTypes.Youtube) &&
                                                              (src.URL.IndexOf("v=") >= 0))
                                                          {
                                                              return src.URL.Substring(src.URL.IndexOf("v=") + 2);
                                                          }
                                                          else
                                                          {
                                                              return src.URL;
                                                          }
                                                      }))
                    .ForMember(dest => dest.VideoType,
                               opt => opt.MapFrom(src => VideoTypes.ToMimeType(src.MediaTypeId)));
            }

            if (Mapper.FindTypeMapFor<MediaGetAuthorsAlpha_Result, AuthorSummaryDTO>() == null)
            {
                Mapper.CreateMap<MediaGetAuthorsAlpha_Result, AuthorSummaryDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AuthorId))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.AuthorLast))
                    .AfterMap((src, dest) =>
                                  {
                                      var nameparts = src.Author.Split(' ');
                                      if (nameparts.Length > 1)
                                          dest.FirstName = nameparts[0];
                                      if (nameparts.Length > 2)
                                          dest.MiddleName = nameparts[1];
                                  });
            }

            if (Mapper.FindTypeMapFor<PaginableResult<DocumentAuthor>, PaginableResult<AuthorDTO>>() == null)
            {
                Mapper.CreateMap<PaginableResult<DocumentAuthor>, PaginableResult<AuthorDTO>>();
            }

            if (Mapper.FindTypeMapFor<DocumentAuthor, AuthorSummaryDTO>() == null)
            {
                Mapper.CreateMap<DocumentAuthor, AuthorSummaryDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AuthorId))
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.AuthorFirst))
                    .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.AuthorMiddle))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.AuthorLast))
                    .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => src.Photo))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreateDate));
            }

            if (Mapper.FindTypeMapFor<DocumentAuthor, AuthorDTO>() == null)
            {
                Mapper.CreateMap<DocumentAuthor, AuthorDTO>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AuthorId))
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.AuthorFirst))
                    .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.AuthorMiddle))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.AuthorLast))
                    .ForMember(dest => dest.BioInfoHtml, opt => opt.MapFrom(src => src.BioText))
                    .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => src.Photo))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreateDate))
                    .ForMember(dest => dest.Articles, opt => opt.Ignore())
                    .ForMember(dest => dest.Documents, opt => opt.Ignore())
                    .ForMember(dest => dest.Publications, opt => opt.Ignore())
                    .AfterMap((src, dest) =>
                                  {
                                      //TODO move all this to the repository/somewhere else?

                                      // Daily
                                      if (dest.Articles == null)
                                      {
                                          dest.Articles = new PaginableResult<ArticleSummaryDTO>
                                                              {
                                                                  PageIndex = 0,
                                                                  PageSize = 6 //TODO
                                                              };
                                      }
                                      var articles = src.DailyArticles.Union(src.DailyArticles1);
                                      dest.Articles.TotalItems = articles.Count();
                                      articles =
                                          articles.OrderByDescending(a => a.DatePosted).Skip(dest.Articles.PageIndex*
                                                                                             dest.Articles.PageSize).
                                              Take(dest.Articles.PageSize);
                                      dest.Articles.Items = new List<ArticleSummaryDTO>();
                                      Mapper.Map(articles, dest.Articles.Items);

                                      // Media
                                      if (dest.Documents == null)
                                      {
                                          dest.Documents = new PaginableResult<DocumentDTO>
                                                               {
                                                                   PageIndex = 0,
                                                                   PageSize = 6 //TODO
                                                               };
                                      }
                                      var documents = src.Documents.Union(src.Documents1);
                                      dest.Documents.TotalItems = documents.Count();
                                      dest.Documents.Counts.Video =
                                          documents.Count(d => d.DocumentFiles.Any(
                                              m => VideoTypes.AsArray().Any(t => t == m.MediaTypeId)));
                                      dest.Documents.Counts.Audio =
                                          documents.Count(d => d.DocumentFiles.Any(
                                              m => AudioTypes.AsArray().Any(t => t == m.MediaTypeId)));
                                      //dest.Documents.Counts.Literature = documents.Where(d => d.DocumentFiles.Any(m => LiteratureTypes.AsArray().Any(t => t == m.MediaTypeId))).Count();
                                      documents =
                                          documents.OrderByDescending(d => d.CreateDate).Skip(dest.Documents.PageIndex*
                                                                                              dest.Documents.PageSize).
                                              Take(dest.Documents.PageSize);

                                      dest.Documents.Items = new List<DocumentDTO>();
                                      Mapper.Map(documents, dest.Documents.Items);

                                      // Literature
                                      if (dest.Publications == null)
                                      {
                                          dest.Publications = new PaginableResult<DocumentDTO>
                                                                  {
                                                                      PageIndex = 0,
                                                                      PageSize = 6 //TODO
                                                                  };
                                      }

                                      var publications = src.Documents.Union(src.Documents1);

                                      publications = publications.Where(
                                          p =>
                                          p.DocumentFiles.Any(
                                              m => LiteratureTypes.AsArray().Any(t => t == m.MediaTypeId))).ToList();

                                      dest.Publications.Counts.Literature =
                                          publications.Count(d => d.DocumentFiles.Any(
                                              m => LiteratureTypes.AsArray().Any(t => t == m.MediaTypeId)));
                                      dest.Publications.TotalItems = dest.Publications.Counts.Literature;
                                      publications =
                                          publications.OrderByDescending(d => d.CreateDate).Skip(
                                              dest.Publications.PageIndex*dest.Publications.PageSize).Take(
                                                  dest.Publications.PageSize);

                                      dest.Publications.Items = new List<DocumentDTO>();
                                      Mapper.Map(publications, dest.Publications.Items);
                                  });
            }
        }
    }
}