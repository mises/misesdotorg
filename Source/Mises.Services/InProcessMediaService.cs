﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Mises.Data;
using Mises.Domain.Contracts;
using DocumentDTO = Mises.Domain.Contracts.DocumentDTO;

#endregion

namespace Mises.Services
{
    public class InProcessMediaService : IMediaService
    {
		private static readonly int[] _mediaTypes = new int[] { 1, 2, 8, 14 };

        public InProcessMediaService(IMediaRepository mediaRepository, IAuthorRepository authorRepository,
                                     ISearchProvider searchProvider)
        {
            SearchProvider = searchProvider;
            MediaRepository = mediaRepository;
            AuthorRepository = authorRepository;
        }

        public IAuthorRepository AuthorRepository { get; private set; }
        public IMediaRepository MediaRepository { get; private set; }

        #region IMediaService Members

        public ISearchProvider SearchProvider { get; set; }

        public PaginableResult<MisesDTO> Search(String term, Int32 pageIndex, Int32 pageSize)
        {
            //var providerResult = SearchProvider.Search(term, pageIndex, pageSize);
			var providerResult = MediaRepository.GetDocuments(term, -1, -1, -1, _mediaTypes, pageIndex, pageSize);
			var providerDTOs = new PaginableResult<DocumentDTO>();
			Mapper.Map(providerResult, providerDTOs);

            var result = new PaginableResult<MisesDTO>
                             {
                                 Items = new List<MisesDTO>(providerDTOs.Items)
								 , TotalItems = providerResult.TotalItems
								 , PageIndex = providerResult.PageIndex
								 , PageSize = providerResult.PageSize
								 , ListSize = providerResult.ListSize
								 , RouteSegmentName = providerResult.RouteSegmentName
								 , Counts = providerResult.Counts
                             };

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public PaginableResult<ArticleSummaryDTO> GetArticleSummaries(IArticleFilter filter, Int32 pageIndex,
                                                                      Int32 pageSize)
        {
            var docs = AuthorRepository.GetArticleSummaries(filter, pageIndex, pageSize);
                //TODO should really live in articles repo but for now
            var result = new PaginableResult<ArticleSummaryDTO>();

            Mapper.Map(docs, result);

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public DocumentDTO GetDocumentFromMediaId(Int32 id)
        {
            var doc = MediaRepository.GetDocumentFromFileId(id);
            DocumentDTO result = null;

            if (doc != null)
            {
                result = Mapper.Map<Document, DocumentDTO>(doc);
            }

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public DocumentDTO GetDocument(Int32 id)
        {
            var doc = MediaRepository.GetDocument(id);
            DocumentDTO result = null;

            if (doc != null)
            {
                result = Mapper.Map<Document, DocumentDTO>(doc);
            }

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public PaginableResult<CategorySummaryDTO> GetCategories(ICategoryFilter filter, Int32 pageIndex = 0,
                                                                 Int32 pageSize = Int32.MaxValue)
        {
            var mo = MediaRepository.GetCategories(filter, pageIndex, pageSize);
            var dto = new PaginableResult<CategorySummaryDTO>();

            Mapper.Map(mo, dto);

            return dto;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public PaginableResult<SubjectSummaryDTO> GetSubjects(ISubjectFilter filter = null, Int32 pageIndex = 0,
                                                              Int32 pageSize = Int32.MaxValue)
        {
            var mo = MediaRepository.GetSubjects(filter, pageIndex, pageSize);
            var dto = new PaginableResult<SubjectSummaryDTO>();

            Mapper.Map(mo, dto);

            return dto;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public SubjectDTO GetSubject(Int32 id, Int32 documentPageIndex, Int32 documentPageSize)
        {
            var subject = MediaRepository.GetSubject(id);

            var documentsFilter = new DocumentsBySubjectFilter(id);
            var result = new SubjectDTO
                             {
                                 Documents = GetDocuments(documentsFilter, documentPageIndex, documentPageSize)
                             };

            Mapper.Map(subject, result);

            Mapper.Map(subject, result);

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public PaginableResult<DocumentDTO> GetDocuments(IDocumentFilter filter, Int32 pageIndex, Int32 pageSize)
        {
            var docs = MediaRepository.GetDocuments(filter, pageIndex, pageSize);
            var result = new PaginableResult<DocumentDTO>();

            Mapper.Map(docs, result);

            return result;
        }

		public PaginableResult<DocumentDTO> GetDocuments(
			string searchString
			, int categoryId
			, int subjectId
			, int authorId
			, int[] mediaTypeIds
			, Int32 pageIndex, Int32 pageSize)
		{
			var docs = MediaRepository.GetDocuments(searchString, categoryId, subjectId, authorId, mediaTypeIds, pageIndex, pageSize);
			var result = new PaginableResult<DocumentDTO>();

			Mapper.Map(docs, result);

			return result;
		}

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public DocumentDTO GetRandomVideo()
        {
            var doc = MediaRepository.GetRandomVideo();
            var result = Mapper.Map<Document, DocumentDTO>(doc);

            result.Media = result.Media.ToList();
            //result.Media = result.Media.Where(m => m.MimeType == VideoTypes.Youtube).ToList();

            return result;
        }

        #endregion

        #region Authors

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public AuthorDTO GetAuthor(Int32 id, Int32 articlePageIndex, Int32 articlePageSize, Int32 documentPageIndex,
                                   Int32 documentPageSize)
        {
            var author = AuthorRepository.GetAuthor(id);

            var result = new AuthorDTO
                             {
                                 Articles = new PaginableResult<ArticleSummaryDTO>
                                                {
                                                    PageIndex = articlePageIndex,
                                                    PageSize = articlePageSize
                                                },
                                 Documents = new PaginableResult<DocumentDTO>
                                                 {
                                                     PageIndex = documentPageIndex,
                                                     PageSize = documentPageSize
                                                 },
                                 // Added by DavcidV
                                 Publications = new PaginableResult<DocumentDTO>
                                                    {
                                                        PageIndex = documentPageIndex,
                                                        PageSize = documentPageSize
                                                    }
                             };

            Mapper.Map(author, result);

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public IEnumerable<AuthorSummaryDTO> GetAuthorSummaries()
        {
            var authors = AuthorRepository.GetAuthorSummaries();
            var result = new List<AuthorSummaryDTO>();

            Mapper.Map(authors, result);

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public PaginableResult<AuthorDTO> GetAuthors(IAuthorFilter filter = null, Int32 pageIndex = 0,
                                                     Int32 pageSize = Int32.MaxValue)
        {
            var authors = AuthorRepository.GetAuthors(filter, pageIndex, pageSize);
            var result = new PaginableResult<AuthorDTO>();

            Mapper.Map(authors, result);

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public IDictionary<Char, Int32> GetAuthorCountsBySurnameInitial()
        {
            var result = AuthorRepository.GetCountsBySurnameInitial();

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name = "filter"></param>
        /// <returns></returns>
        public MisesDTO GetLatestForAuthor(Int32 authorId)
        {
            var contribution = AuthorRepository.GetLatestContribution(authorId);
            MisesDTO result = null;

            if (contribution is Document)
                result = Mapper.Map<Document, DocumentDTO>(contribution as Document);
            else if (contribution is DailyArticle)
                result = Mapper.Map<DailyArticle, ArticleSummaryDTO>(contribution as DailyArticle);

            return result;
        }

        #endregion
    }
}