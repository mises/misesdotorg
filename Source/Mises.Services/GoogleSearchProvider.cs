﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using Mises.Domain.Contracts;
using Mises.Domain.Contracts.Google;

#endregion

namespace Mises.Services
{
    public class GoogleSearchProvider : ISearchProvider
    {
        public GoogleSearchProvider(String apiKey, String cseId)
        {
            APIKey = apiKey;
            CSEId = cseId;
        }

        public String APIKey { get; set; }
        public String CSEId { get; set; }

        #region ISearchProvider Members

        public PaginableResult<SearchProviderResult> Search(String encodedTerm, Int32 pageIndex, Int32 pageSize)
        {
            var result = new PaginableResult<SearchProviderResult>
                             {
                                 PageIndex = pageIndex,
                                 PageSize = pageSize //,
                                 //Items = matches.ToList(),
                                 //TotalItems = responseJson.Queries.Request[0].TotalResults
                             };

            String responseString = String.Empty;

            //TODO config at least some of these...?
            StringBuilder requestUriString = new StringBuilder("https://www.googleapis.com/customsearch/v1?");
            requestUriString.AppendFormat("key={0}", APIKey);
            requestUriString.AppendFormat("&cx={0}", CSEId);
            requestUriString.AppendFormat("&q={0}", encodedTerm);
                //TODO Microsoft.JScript.GlobalObject.unescape(Microsoft.JScript.GlobalObject.decodeURIComponent(encodedTerm))? or just one or the other?
            requestUriString.AppendFormat("&start={0}", (pageIndex*pageSize) + 1); //start-index
            requestUriString.AppendFormat("&num={0}", pageSize); //max-results
            requestUriString.AppendFormat("&alt=json");

            WebRequest request = WebRequest.Create(requestUriString.ToString());
            request.Method = WebRequestMethods.Http.Get;

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        responseString = reader.ReadToEnd();
                    }
                }
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            GoogleCustomSearchResponse responseJson = serializer.Deserialize<GoogleCustomSearchResponse>(responseString);

            if (responseJson.Items == null)
            {
                result.Items = new List<SearchProviderResult>();
                result.TotalItems = 0;
            }
            else
            {
                Regex regexMedia = new Regex(@"^*mises.org/media/(\d+)");

                var matches = responseJson.Items.Select(i =>
                                                            {
                                                                var type = MisesType.Undefined;
                                                                var id = Int32.MinValue;

                                                                var match = regexMedia.Match(i.Link);
                                                                if (match.Success)
                                                                {
                                                                    var linkIdRaw = match.Groups[0].Value;
                                                                    if (match.Groups.Count > 1)
                                                                        linkIdRaw = match.Groups[1].Value;
                                                                    else
                                                                    {
                                                                        linkIdRaw =
                                                                            linkIdRaw.Substring(
                                                                                linkIdRaw.IndexOf(@"/media/") + 7);
                                                                        if (linkIdRaw.Contains(@"/"))
                                                                            linkIdRaw = linkIdRaw.Substring(0,
                                                                                                            linkIdRaw.
                                                                                                                IndexOf(
                                                                                                                    @"/"));
                                                                    }
                                                                    var linkId = 0;
                                                                    if (Int32.TryParse(linkIdRaw, out linkId))
                                                                    {
                                                                        id = linkId;
                                                                        type = MisesType.Media;
                                                                    }
                                                                }

                                                                return new SearchProviderResult
                                                                           {
                                                                               Type = type,
                                                                               Id = id,
                                                                               Link = i.Link,
                                                                               Title = i.Title,
                                                                               TitleHtml = i.HtmlTitle,
                                                                               SnippetHtml = i.HtmlSnippet
                                                                           };
                                                            });

                result.Items = matches.ToList();
                result.TotalItems = responseJson.Queries.Request[0].TotalResults;
            }

            return result;
        }

        #endregion
    }
}