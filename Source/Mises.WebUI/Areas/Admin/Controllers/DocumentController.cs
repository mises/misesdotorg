﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using Mises.Data;
//using Mises.Data.DataModels;

//namespace Mises.Web.Areas.Admin.Controllers
//{ 
//    public class DocumentController : Controller
//    {
//        private MisesModel db = DataHelper.EntityDataModel;

//        //
//        // GET: /Admin/Document/

//        public ViewResult Index()
//        {
//            var Documents = db.Documents.Include("DocumentAuthor").Include("DocumentAuthor1");
//            return View(Documents.ToList());
//        }

//        //
//        // GET: /Admin/Document/Details/5

//        public ViewResult Details(int id)
//        {
//            Documents Documents = db.Documents.Single(d => d.DocumentId == id);
//            return View(Documents);
//        }

//        //
//        // GET: /Admin/Document/Create

//        public ActionResult Create()
//        {
//            ViewBag.Author1 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst");
//            ViewBag.Author2 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst");
//            return View();
//        } 

//        //
//        // POST: /Admin/Document/Create

//        [HttpPost]
//        public ActionResult Create(Mises.Data.Documents Documents)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Documents.AddObject(Documents);
//                db.SaveChanges();
//                return RedirectToAction("Index");  
//            }

//            ViewBag.Author1 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst", Documents.Author2);
//            ViewBag.Author2 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst", Documents.Author2);
//            return View(Documents);
//        }
        
//        //
//        // GET: /Admin/Document/Edit/5
 
//        public ActionResult Edit(int id)
//        {
//            Documents Documents = db.Documents.Single(d => d.DocumentId == id);
//            ViewBag.Author1 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst", Documents.Author1);
//            ViewBag.Author2 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst", Documents.Author2);
//            return View(Documents);
//        }

//        //
//        // POST: /Admin/Document/Edit/5

//        [HttpPost]
//        [ValidateInput(false)]   
//        public ActionResult Edit(Documents Documents)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Documents.Attach(Documents);
//                db.ObjectStateManager.ChangeObjectState(Documents, EntityState.Modified);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            ViewBag.Author1 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst", Documents.Author1);
//            ViewBag.Author2 = new SelectList(db.DocumentAuthors, "AuthorId", "AuthorFirst", Documents.Author2);
//            return View(Documents);
//        }

//        //
//        // GET: /Admin/Document/Delete/5
 
//        public ActionResult Delete(int id)
//        {
//            Documents Documents = db.Documents.Single(d => d.DocumentId == id);
//            return View(Documents);
//        }

//        //
//        // POST: /Admin/Document/Delete/5

//        [HttpPost, ActionName("Delete")]
//        public ActionResult DeleteConfirmed(int id)
//        {            
//            Documents Documents = db.Documents.Single(d => d.DocumentId == id);
//            db.Documents.DeleteObject(Documents);
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            db.Dispose();
//            base.Dispose(disposing);
//        }
//    }
//}