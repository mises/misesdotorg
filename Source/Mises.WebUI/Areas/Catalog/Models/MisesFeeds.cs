﻿#region

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using Mises.Data;
using Mises.Domain.Feeds;

#endregion

namespace LiteratureCatalog.Models
{
    public class MisesFeeds : StanzaFeedBase
    {
        #region Properties

        private static readonly string Conn = DataHelper.ConnectionString;
        private readonly int? maxResults = 500;
        private MisesDBDataContext db;

        #endregion

        #region Constructor

        public MisesFeeds(HttpRequestBase httpRequest)
            : base(httpRequest)
        {
        }

        #endregion

        #region Feed Index (Table of Contents)

        public IEnumerable<SyndicationItem> GetIndexFeedItems()
        {
            var postItems = new List<SyndicationItem>
                                {new SyndicationItem("All ePub Books", "", null, "", new DateTimeOffset(DateTime.Now))};

            postItems[0].Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Epub"), "", "", Type, 0));
            postItems[0].Links.Add(
                new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/book.png")),
                                    "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));


            postItems.Add(new SyndicationItem("Last 500 Books (PDF & Epub)", "", null, "",
                                              new DateTimeOffset(DateTime.Now)));
            postItems[1].Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Books"), "", "", Type, 0));
            postItems[1].Links.Add(
                new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/date.png")),
                                    "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));

            postItems.Add(new SyndicationItem("Top Authors", "", null, "", new DateTimeOffset(DateTime.Now)));
            postItems[2].Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/TopAuthors"), "", "", Type, 0));
            postItems[2].Links.Add(
                new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/authors.png")),
                                    "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));

            postItems.Add(new SyndicationItem("All Authors (500+)", "", null, "", new DateTimeOffset(DateTime.Now)));
            postItems[3].Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Authors"), "", "", Type, 0));
            postItems[3].Links.Add(
                new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/authors.png")),
                                    "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));


            postItems.Add(new SyndicationItem("Journals", "", null, "", new DateTimeOffset(DateTime.Now)));
            postItems[4].Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Journals"), "", "", Type, 0));
            postItems[4].Links.Add(
                new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/journal.png")),
                                    "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));


            postItems.Add(new SyndicationItem("Subjects", "", null, "", new DateTimeOffset(DateTime.Now)));
            postItems[5].Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Subjects"), "", "", Type, 0));
            postItems[5].Links.Add(
                new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/LetterA.png")),
                                    "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));


            return postItems;
        }

        #endregion

        #region Literature Feed Getters

        public SyndicationFeed SearchFeed(string s)
        {
            string title = "Search: " + s;
            const string description = "";


            db = new MisesDBDataContext(Conn);

            return CreateFeedFromDocumentList(GetDocumentsFromDatabase(null, null, null, s), title,
                                              description);
        }

        public SyndicationFeed GetJournalsFeed()
        {
            const string title = "Journals";
            const string description = "";

            db = new MisesDBDataContext(Conn);

            var journals = from periodicals in db.PeriodicalTBs select periodicals;

            var postItems = journals.ToList().Select(
                p =>
                    {
                        var item = new SyndicationItem(p.Title, "", null, "", new DateTimeOffset(DateTime.Now));

                        item.Links.Add(
                            new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Journal?journalId=" + p.PeriodicalId))
                                {MediaType = Type});

                        item.Authors.Add(new SyndicationPerson("", p.ShortDescription, null));

                        if (!string.IsNullOrWhiteSpace(p.Logo))
                        {
                            try
                            {
                                item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(p.Logo)),
                                                                   "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                            }
                            catch (Exception)
                            {
                            }
                        }

                        return item;
                    });

            return new SyndicationFeed(title, description, new Uri(feedUri), postItems)
                       {
                           Copyright = new TextSyndicationContent(Configuration.Copyright),
                           Language = "en-US"
                       };
        }

        public SyndicationFeed GetJournalFeed(int journalId)
        {
            string title = "Journal Feed";
            const string description = "";

            db = new MisesDBDataContext(Conn);

            var periodicals = from periodical in db.PeriodicalsViewTBs
                              where periodical.PeriodicalId == journalId
                              select periodical;

            var postItems = periodicals.ToList().Select(
                p =>
                {
                    var item = new DocumentGetFeedResult();
                    title = p.Journal;

                    item.Author = p.AuthorName;
                    if (p.AuthorId != null) item.Author1 = (int)p.AuthorId;
                    item.Title = string.Format("{0} ({1})", p.title, p.Journal);
                    item.MediaTypeId = 3;
                    if (p.DatePosted != null) item.CreateDate = (DateTime)p.DatePosted;
                    if (p.GUID != null) item.GUID = (Guid)p.GUID;
                    item.URL = p.URL;
                    item.CoverImage = "/Theme/images/mobile/LetterA.png";
                    return item;
                });

            return CreateFeedFromDocumentList(postItems, title,
                                              description);
        }

        public SyndicationFeed GetSubjectsFeed()
        {
            const string title = "Subjects";
            const string description = "";

            db = new MisesDBDataContext(Conn);

            var subjects = db.DocumentSubjectsGetList(null);

            IEnumerable<SyndicationItem> postItems = subjects.Select(
                p =>
                {
                    var item = new SyndicationItem(p.Subject, "", null, "", new DateTimeOffset(DateTime.Now));

                    item.Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Subject?subjectId=" + p.SubjectId)) { MediaType = Type });

                    item.Links.Add(
                        new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/LetterA.png")),
                                            "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                    return item;
                });

            return new SyndicationFeed(title, description, new Uri(feedUri), postItems)
                       {
                           Copyright = new TextSyndicationContent(Configuration.Copyright),
                           Language = "en-US"
                       };
        }

        public SyndicationFeed GetSubjectFeed(int subjectId)
        {
            const string title = "Mises Literature Catalog - Authors";
            const string description = "";


            db = new MisesDBDataContext(Conn);

            return CreateFeedFromDocumentList(GetDocumentsFromDatabase(null, subjectId, null, null), title,
                                              description);
        }

        internal SyndicationFeed GetAuthorFeed()
        {
            const string title = "Mises Literature Catalog - Authors";
            const string description = "";

            db = new MisesDBDataContext(Conn);

            ISingleResult<DocumentAuthorsGetListResult> authors = db.DocumentAuthorsGetList("Literature");

            IEnumerable<SyndicationItem> postItems = authors.Select(
                p =>
                {
                    var item = new SyndicationItem(p.AuthorName, "", null, "", new DateTimeOffset(DateTime.Now));

                    item.Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Author?authorId=" + p.AuthorId)) { MediaType = Type });

                    if (!string.IsNullOrWhiteSpace(p.Photo))
                    {
                        item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(p.Photo)),
                                                           "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                    }
                    else
                    {
                        item.Links.Add(
                            new SyndicationLink(
                                new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/authors.png")),
                                "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                    }

                    return item;
                });

            return CreateFeedFromSyndicationItemList(postItems, title, description);
        }

        internal SyndicationFeed GetTopAuthorFeed()
        {
            const string title = "Mises Literature Catalog - Authors";
            const string description = "";

            db = new MisesDBDataContext(Conn);

            ISingleResult<DocumentTopAuthorsResult> authors = db.DocumentTopAuthors();

            IEnumerable<SyndicationItem> postItems = authors.Select(
                p =>
                {
                    var item = new SyndicationItem(p.AuthorName, "", null, "", new DateTimeOffset(DateTime.Now));

                    item.Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Author?authorId=" + p.AuthorId)) { MediaType = Type });

                    if (!string.IsNullOrWhiteSpace(p.Photo))
                    {
                        item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(p.Photo)),
                                                           "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                    }
                    else
                    {
                        item.Links.Add(
                            new SyndicationLink(
                                new Uri(DataFormat.GetAbsoluteURL("/Theme/images/mobile/authors.png")),
                                "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                    }

                    return item;
                });

            return CreateFeedFromSyndicationItemList(postItems, title, description);
        }

        public SyndicationFeed GetAuthorFeed(int authorId)
        {
            const string title = "Mises Literature Catalog - Authors";
            const string description = "";


            db = new MisesDBDataContext(Conn);

            return CreateFeedFromDocumentList(GetDocumentsFromDatabase(authorId, null, null, null), title,
                                              description);
        }


        internal SyndicationFeed GetFeedForMediaType(int mediaTypeId)
        {
            const string title = "Mises Literature Catalog - ePubs";
            const string description = "Latest EPub books from Mises.org";


            List<DocumentGetFeedResult> results = GetDocumentsFromDatabase(null, null, mediaTypeId, null);

            return CreateFeedFromDocumentList(results, title, description);
        }

        internal SyndicationFeed GetFeedForCategory(int categoryId)
        {
            const string title = "Mises Literature Catalog - Category";
            const string description = "The complete guide to Austrian School literature, with links where possible";


            List<DocumentGetFeedResult> results = GetDocumentsFromDatabase(null, categoryId, null, null);

            return CreateFeedFromDocumentList(results, title, description);
        }

        public SyndicationFeed GetFeedForSubject(int subjectId)
        {
            const string title = "Mises Literature Catalog - Subject";
            const string description = "The complete guide to Austrian School literature, with links where possible";


            List<DocumentGetFeedResult> results = GetDocumentsFromDatabase(null, subjectId, null, null);

            return CreateFeedFromDocumentList(results, title, description);
        }

        #endregion

        #region Data Access

        private List<DocumentGetFeedResult> GetDocumentsFromDatabase(int? authorId, int? subjectId, int? mediaTypeId,
                                                                     string searchQuery)
        {
            db = new MisesDBDataContext(Conn); // BusinessBase.ConnectionString
            List<DocumentGetFeedResult> documents =
                db.DocumentGetFeed(authorId, subjectId, mediaTypeId, maxResults, searchQuery).ToList();

            // eliminate duplicates by choosing epub if there are multiple formats
            var nodupes = new List<DocumentGetFeedResult>();

            documents.ForEach(doc =>
                                  {
                                      if (nodupes.Count(p => p.DocumentId == doc.DocumentId) == 0)
                                      {
                                          if (doc.MediaTypeId == 3 || doc.MediaTypeId == 9)
                                              nodupes.Add(doc);
                                      }
                                      else // swap
                                      {
                                          DocumentGetFeedResult currentDoc =
                                              nodupes.First(p => p.DocumentId == doc.DocumentId);
                                          if (currentDoc.MediaTypeId != 9 && currentDoc.MediaTypeId == 9)
                                          {
                                              nodupes.Remove(currentDoc);
                                              nodupes.Add(doc);
                                          }
                                      }
                                  });
            return nodupes;
        }

        #endregion
    }
}