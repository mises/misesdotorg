﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using Mises.Data;
using Mises.Domain.Feeds;
using Mises.Domain.Mobile;

#endregion

namespace LiteratureCatalog.Models
{
    public class StanzaFeedBase
    {
        #region Properties

        protected const int MaxDescriptionLength = 1000;
        protected const string Type = "application/atom+xml";
        protected readonly string feedUri;
        protected readonly HttpRequestBase request;


        protected string Host
        {
            get
            {
                if (request != null)
                    return "http://" + request.Url.Host + "/";// + "/Catalog";
                else
                    return "http://localhost/Catalog/";
            }
        }

        #endregion

        protected StanzaFeedBase(HttpRequestBase httpRequest)
        {
            request = httpRequest;
            feedUri = (request != null) ? request.Url.ToString() : "http://localhost/";
        }

        protected SyndicationFeed CreateFeedFromDocumentList(IEnumerable<DocumentGetFeedResult> results, string title,
                                                             string description)
        {
            results.ToList().ForEach(r => { r.URL = DataFormat.GetAbsoluteURL(r.URL); });

            IEnumerable<SyndicationItem> postItems = results.Where(
                p => !string.IsNullOrWhiteSpace(p.URL) && p.MediaTypeId != 7  && p.URL.Contains("http://")).Select(
                    p =>
                        {
                            var item = new SyndicationItem(p.Title, p.Description, new Uri(p.URL));

                            if (p.CreateDate != DateTime.MinValue)
                            {
                                item.PublishDate = p.CreateDate;
                            }


                            item.Authors.Add(new SyndicationPerson("", p.Author, null));

                            if (p.ProductId > 0)
                            {
                                if (p.StoreDescription.Length > MaxDescriptionLength)
                                    p.StoreDescription = p.StoreDescription.Substring(0, MaxDescriptionLength) +
                                                         @"... (tap ""purchase"" link below for more info.)";

                                //var link = string.Format("http://store.mises.org/Product.aspx?ProductId={0}&utm_source=MisesCatalog", p.ProductId);

                                //  p.StoreDescription += string.Format(@"<h4><a rel=""alternate"" href=""{0}"">See more at the Mises Store</a></h4>", link);

                                p.StoreDescription += Analytics.GetAnalyticsImageTag(request.RequestContext.HttpContext);

                                var content = new TextSyndicationContent(p.StoreDescription,
                                                                         TextSyndicationContentKind.Html);


                                item.Content = content;
                            }
                            else
                            {
                                item.Content =
                                    new TextSyndicationContent(
                                        string.Format("{0}<br />{1}<br />{2}", p.Description, p.PublicationInformation,
                                                      Analytics.GetAnalyticsImageTag(request.RequestContext.HttpContext)),
                                        TextSyndicationContentKind.Html);
                            }




                            string mimeType = "application/epub+zip";

                            if (p.URL.ToLower().EndsWith(".pdf"))
                                mimeType = "application/pdf";

                            item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(p.URL)), "", "",
                                                               mimeType, ((p.fileSize != null) ? (long) p.fileSize : 0)));
                            if (!string.IsNullOrWhiteSpace(p.CoverImage))
                            {
                                item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(
                                    p.CoverImage.Replace("ProductImages", "ProductImages/Thumbnails").Replace(".jpg",
                                                                                                              "_T.jpg")
                                                                               )),
                                                                   "x-stanza-cover-image-thumbnail", "", "image/jpeg", 0));
                                item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(p.CoverImage)),
                                                                   "x-stanza-cover-image", "", "image/jpeg", 0));
                            }

                            if (p.ProductId > 0)
                            {
                                // Add purchase link:      
                                item.Links.Add(
                                    new SyndicationLink(
                                        new Uri(
                                            string.Format(
                                                "http://store.mises.org/Product.aspx?ProductId={0}&utm_source=MisesCatalog",
                                                p.ProductId)), "alternate", "Purchase at the Mises Store",
                                        "text/html", 0));
                            }

                            item.Links.Add(
                                new SyndicationLink(
                                    new Uri(
                                        string.Format(
                                            "http://mises.freecapitalists.org//resources/{0}",
                                            p.DocumentId)), "alternate", "Mises Literature Entry",
                                    "text/html", 0));


                            return item;
                        });

            return CreateFeedFromSyndicationItemList(postItems, title, description);
        }

        public SyndicationFeed CreateFeedFromSyndicationItemList(IEnumerable<SyndicationItem> postItems, string title,
                                                                 string description)
        {
            var feed = new SyndicationFeed(title, description, new Uri(feedUri), postItems)
                           {
                               Copyright = new TextSyndicationContent(Configuration.Copyright),
                               Language = "en-US"
                           };

            var self = new SyndicationLink(new Uri(Host + HttpUtility.UrlEncode("/Catalog/")), "self", "", Type, 0);
            feed.Links.Add(self);

            feed.Links.Add(new SyndicationLink(new Uri(Host + "/Catalog/Catalog/Search/?q={searchTerms}"), "search",
                                               "Search Catalog", Type, 0));

            return feed;
        }
    }
}