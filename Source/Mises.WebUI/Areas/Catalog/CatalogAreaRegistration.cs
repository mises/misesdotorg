﻿using System.Web.Mvc;

namespace Mises.Web.Areas.Catalog
{
    public class CatalogAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Catalog";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Catalog_default",
                "Catalog/{controller}/{action}/{id}",
                new { controller="Catalog", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
