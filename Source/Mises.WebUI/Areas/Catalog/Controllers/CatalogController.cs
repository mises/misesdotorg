﻿#region

using System.Collections.Generic;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using LiteratureCatalog.Models;

#endregion

namespace Mises.Web.Areas.Catalog.Controllers
{
    public class CatalogController : Controller
    {
        public FeedResult Index()
        {
            const string title = "Mises Syndicated Catalog - Contents";
            const string description = "The complete guide to Austrian School literature, with links where possible";

            var feeds = new MisesFeeds(Request);
            IEnumerable<SyndicationItem> postItems = feeds.GetIndexFeedItems();

            return
                new FeedResult(
                    new Atom10FeedFormatter(feeds.CreateFeedFromSyndicationItemList(postItems, title, description)));
        }


        public FeedResult Search(string q)
        {
            // Mises.Domain.Documents.
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.SearchFeed(q);

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult Books()
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetFeedForSubject(117);

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult EPub()
        {
            const int mediaTypeId = 9;
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetFeedForMediaType(mediaTypeId);

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult Authors()
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetAuthorFeed();

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult TopAuthors()
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetTopAuthorFeed();

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult Author(int authorId)
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetAuthorFeed(authorId);

            return new FeedResult(new Atom10FeedFormatter(feed));
        }


        public FeedResult Journals()
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetJournalsFeed();

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult Journal(int journalId)
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetJournalFeed(journalId);

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult Subjects()
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetSubjectsFeed();

            return new FeedResult(new Atom10FeedFormatter(feed));
        }

        public FeedResult Subject(int subjectId)
        {
            var feeds = new MisesFeeds(Request);
            SyndicationFeed feed = feeds.GetSubjectFeed(subjectId);

            return new FeedResult(new Atom10FeedFormatter(feed));
        }
    }
}