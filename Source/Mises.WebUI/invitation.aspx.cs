﻿#region

using System;
using System.Web.UI;
using Mises.Domain.Feeds;
using Mises.Domain.Utility;

#endregion

partial class invitation : Page
{
    protected void Send_Click(object sender, EventArgs e)
    {
        string message = txtMessage.Text + Environment.NewLine + Environment.NewLine;

        message += "(This invitation was sent by " + txtName.Text + " from " + Request.GetOriginalHostAddress() + ")" +
                   Environment.NewLine;

        string subject = "Invitation to subscribe for Mises Daily Articles from " + txtName.Text;


        if (txtEmail1.Text != "")
        {
            EmailHelper.SendMail(Configuration.Email, txtEmail1.Text, subject, message);
        }
        if (txtEmail2.Text != "")
        {
            EmailHelper.SendMail(Configuration.Email, txtEmail2.Text, subject, message);
        }
        if (txtEmail3.Text != "")
        {
            EmailHelper.SendMail(Configuration.Email, txtEmail3.Text, subject, message);
        }
        if (txtEmail4.Text != "")
        {
            EmailHelper.SendMail(Configuration.Email, txtEmail4.Text, subject, message);
        }
        if (txtEmail5.Text != "")
        {
            EmailHelper.SendMail(Configuration.Email, txtEmail5.Text, subject, message);
        }


        lblTitle.Text = "Thank you.  Your invitation has been sent.";
        Send.Enabled = false;
    }
}