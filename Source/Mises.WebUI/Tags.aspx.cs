#region

using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using Mises.Domain.Documents;
using Mises.Domain.Tags;
using Mises.Domain.Utility;
using d = Mises.Domain.Documents;

#endregion

partial class Tags : MyPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }

        if (Request.QueryString["user"] != null)
        {
            litHeader.Text = string.Format("Browse by user: {0}", Request.QueryString["user"]);
            Page.Title = litHeader.Text;

            var tagging = new Tagging();
            gvTaggedPages.DataSource = tagging.GetUserTags(Request.QueryString["user"]);
            gvTaggedPages.DataBind();

            cldRelatedTags.DataSourceID = null;
            cldRelatedTags.DataSource = tagging.TagItemList;
            cldRelatedTags.DataBind();
        }
        else
        {
            litHeader.Text = string.Format("Browse by tag: {0}", Request.QueryString["tag"]);
            Page.Title = litHeader.Text;
            string tag = Request.QueryString["tag"];
            List<Document> tagList = Tagging.GetDocumentsWithTag(tag);
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;

            if (tagList.Count == 0)
            {
                Response.Redirect(baseUrl);
            }

            gvTaggedPages.DataSource = tagList;
            gvTaggedPages.DataBind();

            var link = new HtmlLink();
            link.Attributes.Add("type", "application/rss+xml");
            link.Attributes.Add("rel", "alternate");
            link.Attributes.Add("title", "Documents tagged \"" + tag + "\"");
            link.Attributes.Add("href", string.Format("{0}/Feed/Tags/{1}", baseUrl, tag));
            Page.Header.Controls.AddAt(1, link);

            lnkRSS.NavigateUrl = string.Format("{0}/Feed/Tags/{1}", baseUrl, tag);
            lnkRSS.Text = "Documents tagged \"" + tag + "\"";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }
}