﻿#region

using System.Configuration;
using System.Data.Services;
using System.Web.Configuration;
using Mises.Data;

#endregion

public class MisesDataService : DataService<MisesModel>
{
    // This method is called only once to initialize service-wide policies.
    public static void InitializeService(IDataServiceConfiguration config)
    {
        // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
        // Examples:
        // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
        // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);

        config.SetEntitySetAccessRule("*", EntitySetRights.AllRead);
        config.SetEntitySetAccessRule("Donations", EntitySetRights.None);
        config.SetEntitySetAccessRule("ManagerUsers", EntitySetRights.None);
        config.SetEntitySetAccessRule("People", EntitySetRights.None);
    }

    protected override MisesModel CreateDataSource()
    {
        var connectionStringsSection =
            WebConfigurationManager.GetSection("connectionStrings") as ConnectionStringsSection;
        return new MisesModel(connectionStringsSection.ConnectionStrings["MisesEntities"].ConnectionString);
    }
}