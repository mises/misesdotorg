﻿#region

using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Mises.Data;
using Mises.Domain.Media;

#endregion

[ServiceContract(Namespace = "")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class VideoFeed
{
    [OperationContract]
    public IEnumerable<DocumentDTO> GetFeaturedVideos()
    {
        var fd = new FeaturedDocument();
        return fd.FeaturedDocuments;
//        if (HttpContext.Current.Cache["__FeaturedDocumentsCache"] == null)
//        {
//            var fd = new FeaturedDocument();
//            var featuredDocuments = fd.FeaturedDocuments;
//            HttpContext.Current.Cache.Insert("__FeaturedDocumentsCache", featuredDocuments, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(1));
//
        // added to cache and returned
//            return featuredDocuments;
//
//        }
//        else
//        {
        // grabed from cache and returned
//            var featuredDocuments = (List<DocumentDTO>)HttpContext.Current.Cache["__FeaturedDocumentsCache"];
//            return featuredDocuments;
//        }
    }
}


/*


if (HttpContext.Current.Cache["__FeaturedDocumentsCache"] == null)
        {
            var fd = new FeaturedDocument();
            var featuredDocuments = fd.FeaturedDocuments;
            HttpContext.Current.Cache.Insert("__FeaturedDocumentsCache", featuredDocuments, null,  System.Web.Caching.Cache.NoAbsoluteExpiration,TimeSpan.FromHours(1));

            // added to cache and returned
            return featuredDocuments;
            
        }
        else
        {
            // grabed from cache and returned
            var featuredDocuments = (List<DocumentDTO>) HttpContext.Current.Cache["__FeaturedDocumentsCache"];
            return featuredDocuments;
        }

*/