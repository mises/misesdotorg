﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using Mises.Data;
using Mises.Domain.Feeds;

#endregion

namespace Mises.Domain
{
    public class QuotesFeed : INewsFeed
    {
        #region INewsFeed Members

        public SyndicationFeedFormatter feed(string format)
        {
            var db = DataHelper.MisesDataContext;
            QuoteGetRandomResult quote = db.QuoteGetRandom().FirstOrDefault();

            //Setting up the feed formatter.

            var feed = new SyndicationFeed(
                "Mises Quotes",
                "Ludig von Mises Institute Mises Quotes",
                new Uri("http://mises.freecapitalists.org//quotes.aspx"));
            feed.Authors.Add(new SyndicationPerson(quote.Author));
            feed.Categories.Add(new SyndicationCategory("Mises Quotes"));

            var item = new SyndicationItem(quote.Subject, quote.Quote,
                                           new Uri("http://mises.freecapitalists.org//quotes.aspx?action=subject&subject=" +
                                                   quote.Subject));

            item.Authors.Add(new SyndicationPerson(quote.Author, quote.Source,
                                                   "http://mises.freecapitalists.org//quotes.aspx?action=source&Source=" + quote.Source));
            feed.Items = new List<SyndicationItem> {item};

            // Processing and serving the feed according to the required format
            // i.e. either RSS or Atom.
            switch (format)
            {
                case "rss":
                    return new Rss20FeedFormatter(feed);
                case "atom":
                    return new Atom10FeedFormatter(feed);
                default:
                    return null;
            }
        }

        #endregion
    }
}

//Ends Namespace