#region

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.ServiceModel.Syndication;
using Mises.Data;
using Mises.Domain.Feeds;

#endregion

namespace Mises.Domain.Articles
{
    ///<summary>
    ///  http://localhost/Feeds/Articles.svc/feed/atom/
    ///  http://localhost/Feeds/Articles.svc/feed/atom/?authorId=4
    ///</summary>
    public class ArticlesFeed : INewsAuthorFeed
    {
        //TODO: FIX to relative URL


        private const string Rss = "rss";
        private const string Atom = "atom";
        private const string FeedTitle = "Mises Dailies";

        private const string FeedDescription =
            "Mises Daily : Mises Institute on Austrian Economics and Libertarianism";

        //private const string FullTextFeedTitle = "Mises Institute Daily Articles (Full-text version)";
        //TODO: FIX to relative URL
        private const string AuthorArchiveUrlFormat = "http://mises.freecapitalists.org//daily/author/{0}";
        private const string ImageMediaTypeDescriptor = "image/jpeg";
        private const string EnclosureRelationshipType = "enclosure";
        private const string ArticlesCategory = "Articles";
        private const string EconomicsCategory = "Economics";
        private const string AuthorFeedTitleFormat = "Mises Daily archives for {0}";
        //TODO: FIX to relative URL
        private const string DailyArticlesImageUrl = "http://mises.freecapitalists.org//images/DailyArticles.gif";
        private const string MisesFeedCopyright = "Copyright 2002-2008 Mises Institute";
        private static readonly Uri FeedUri = new Uri("http://mises.freecapitalists.org//daily");

        #region INewsAuthorFeed Members

        public SyndicationFeedFormatter feed(string format, int authorId)
        {
            ISingleResult<DailyArticlesGetFullTextFeedResult> articlesRs = DailyArticle.GetRSSFullTextFeed();

            var articles = articlesRs.Select(article => new DailyArticle().ArticleFromLinqResult(article));


            //IEnumerable<DailyArticle> articles = DailyArticle.GetRSSFeed().Rows.Cast<DataRow>().Select(
            //    dr => new DailyArticle
            //              {
            //                  ArticleId = DataFormat.ObjToInteger(dr[ArticleFields.ArticleId]),
            //                  AuthorId = DataFormat.ObjToInteger(dr[ArticleFields.AuthorId]),
            //                  Title = DataFormat.ObjToString(dr[ArticleFields.Title]),
            //                  AuthorName = DataFormat.ObjToString(dr[ArticleFields.AuthorName]),
            //                  DatePosted = Convert.ToDateTime(dr[ArticleFields.DatePosted]),
            //                  Description = DataFormat.ObjToString(dr[ArticleFields.Description]),
            //                  PhotoURL = DataFormat.ObjToString((dr[ArticleFields.Photo])),
            //                  Identifier = new Guid(dr[ArticleFields.Guid].ToString())
            //              }).
            //    ToList();


            var items = new List<SyndicationItem>();

            foreach (DailyArticle article in articles)
            {
                var item = new SyndicationItem
                    (DataFormat.StripHTML(article.Title),
                     String.Format(@"<img src=""{0}"" vspace=""4"" hspace=""4"" style=""margin: 10px;"" /><br />",
                                   DataFormat.GetAbsoluteURL(String.Format("/images/DailyArticleImages/{0}.jpg",
                                                                           article.ArticleId)))
                     +
                     article.Description,
                     new Uri(article.URL, UriKind.Absolute),
                     article.Identifier.ToString(),
                     article.DatePosted
                    ) {PublishDate = article.DatePosted};

                item.Authors.Add(new SyndicationPerson("", article.AuthorName,
                                                       String.Format(AuthorArchiveUrlFormat,
                                                                     article.AuthorId)));

                if (!string.IsNullOrEmpty(article.PhotoURL))
                    item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(article.PhotoURL)),
                                                       EnclosureRelationshipType,
                                                       article.Title, ImageMediaTypeDescriptor, 1000));

                items.Add(item);
            }

            SyndicationFeed feed = BuildSyndicationFeed(items);


            if (authorId > 0 && feed.Items.Any())
            {
                feed.Title =
                    new TextSyndicationContent("Mises Daily archives for " + feed.Items.First().Authors[0].Name);
            }


            return BuildFeed(feed, format);
        }


        public SyndicationFeedFormatter FullTextFeed(string format, int AuthorId)
        {
            ISingleResult<DailyArticlesGetFullTextFeedResult> articlesRs = DailyArticle.GetRSSFullTextFeed();
            
            var articles = articlesRs.Select(article => new DailyArticle().ArticleFromLinqResult(article));

            var items = new List<SyndicationItem>();

            foreach (DailyArticle article in articles)
            {
                var item = new SyndicationItem
                    (DataFormat.StripHTML(article.Title),
                     article.ReplaceText(),
                     new Uri(article.URL, UriKind.Absolute),
                     article.Identifier.ToString(),
                     article.DatePosted
                    ) {PublishDate = article.DatePosted};

                item.Authors.Add(new SyndicationPerson("", article.AuthorName,
                                                       String.Format(AuthorArchiveUrlFormat,
                                                                     article.AuthorId)));
                if (!string.IsNullOrEmpty(article.PhotoURL))
                    item.Links.Add(new SyndicationLink(new Uri(DataFormat.GetAbsoluteURL(article.PhotoURL)),
                                                       EnclosureRelationshipType,
                                                       article.Title, ImageMediaTypeDescriptor, 1000));


                items.Add(item);
            }
            SyndicationFeed feed = BuildSyndicationFeed(items);


            if (AuthorId > 0 && feed.Items.Any())
            {
                feed.Title =
                    new TextSyndicationContent(String.Format(AuthorFeedTitleFormat, feed.Items.First().Authors[0].Name));
            }

            return BuildFeed(feed, format);
        }

        #endregion

        private static SyndicationFeedFormatter BuildFeed(SyndicationFeed feed, string format)
        {
            switch (format)
            {
                case Rss:
                    return new Rss20FeedFormatter(feed);
                case Atom:
                    return new Atom10FeedFormatter(feed);
                default:
                    return null;
            }
        }

        private SyndicationFeed BuildSyndicationFeed(List<SyndicationItem> items)
        {
            var feed = new SyndicationFeed(
                FeedDescription,
                FeedDescription,
                FeedUri) {Items = items};


            feed.Categories.Add(new SyndicationCategory(ArticlesCategory));
            feed.Categories.Add(new SyndicationCategory(EconomicsCategory));

            feed.ImageUrl = new Uri(DailyArticlesImageUrl);
            feed.Copyright = new TextSyndicationContent(MisesFeedCopyright);
            return feed;
        }
    }
}