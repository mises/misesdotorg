﻿#region

using System;
using System.Data;
using System.Linq;
using System.ServiceModel.Syndication;
using Mises.Data;
using Mises.Domain.Feeds;

#endregion

namespace Mises.Domain.Events
{
    public class EventsFeed : INewsFeed
    {
        #region INewsFeed Members

        public SyndicationFeedFormatter feed(string format)
        {
            DataSet row = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                   CommandType.StoredProcedure, "dbo.CalendarGetUpcomingEvents");

            //Setting up the feed formatter.
            // TODO: FIX to relative URL
            var feed = new SyndicationFeed(
                "Upcoming Events",
                "Ludig von Mises Institute Upcoming Events",
                new Uri("http://mises.freecapitalists.org//events/"));
            feed.Authors.Add(new SyndicationPerson(Configuration.Email));
            feed.Categories.Add(new SyndicationCategory("Event Calendar"));

            // TODO: FIX to relative URL
            feed.Items = row.Tables[0].Rows.Cast<DataRow>().Select(dr => new SyndicationItem
                                                                             (dr["title"].ToString(),
                                                                              dr["StartDate"] + " " + dr["Location"] +
                                                                              Environment.NewLine +
                                                                              dr["IntroText"],
                                                                              new Uri(
                                                                                  String.Format(
                                                                                      "http://mises.freecapitalists.org//events/{0}",
                                                                                      dr["eventid"])),
                                                                              dr["GUID"].ToString(),
                                                                              DateTime.Parse(dr["EndDate"].ToString())
                                                                             )
                );
            // Processing and serving the feed according to the required format
            // i.e. either RSS or Atom.
            switch (format)
            {
                case "rss":
                    return new Rss20FeedFormatter(feed);
                case "atom":
                    return new Atom10FeedFormatter(feed);
                default:
                    return null;
            }
        }

        #endregion
    }
}

//Ends Namespace