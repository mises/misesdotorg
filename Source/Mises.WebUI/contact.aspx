<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" Title="Contact the Ludwig von Mises Institute" AutoEventWireup="true"
         ValidateRequest="false" Inherits="contact" Codebehind="~/contact.aspx.cs" %>

<%@ Register Src="~/Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="margin-right: auto; margin-left: auto" cellspacing="0" cellpadding="0" width="650"  border="0">
        <tbody>
            <tr>
                <td valign="top">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
                    <asp:Panel ID="pnlContactForm" runat="server">
                        <h2>
                            Contact the Ludwig von Mises Institute</h2>
                        <hr style="color: #0d4d7d"  />
                        <br />
                        <asp:Image runat="server"  Style="float: right" AlternateText="feedback" ImageUrl="~/images/feedback.gif"  />
                        <asp:Label id="lblMessage" runat="server" Font-Bold="True">Send us a message: </asp:Label>
                        <table border="0">
                            <tr>
                                <td align="left">
                                    <span style="font-size: small">Name</span>
                                </td>
                                <td align="left">
                                    <span style="font-size: small">Email Address</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:TextBox ID="txtName"  MaxLength="50" Columns="20" name="name" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter your name."
                                                                ControlToValidate="txtName">*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" MaxLengthh="50" Columns="20"  runat="server" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter a valid email address."
                                                                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter a valid email address."
                                                                ControlToValidate="txtEmail">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <span style="font-size: small">Country</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:TextBox runat="server" ID="txtCountry" MaxLength="50" Columns="25" name="country" runat="server" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <span style="font-size: small">Comments</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <textarea id="txtComments" name="comments" rows="8" runat="server" width="500" style="width: 504px"></textarea>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter a comment."
                                                                ControlToValidate="txtComments">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="Submit1"  type="submit" Text="Send Feedback" CssClass="ui-state-default ui-corner-all ui-button" 
                                    name="Submit1" onclick="Submit1_Click" 
                            />
                        <input type="reset" value="Clear" /></asp:Panel>
                    <!-- TODO: FIX to relative URL -->
                    &nbsp;<a href="http://blog.mises.org"></a>
                    <asp:Panel ID="pnlBlogSubmit" runat="server" Visible="False">
                        <hr style="color: #0d4d7d"  />
                        <strong>Submit a Blog</strong><br />
                        <table border="0">
                            <tr>
                                <td style="width: 185px">
                                    Name:
                                    <asp:TextBox ID="txtBlogName" runat="server">
                                    </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                                              ErrorMessage="You must provide your name." ControlToValidate="txtBlogName">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 185px">
                                    Email:
                                    <asp:TextBox ID="txtBlogEmail" runat="server">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 185px">
                                    Blog text (include in text all URLs for links or images. Mises.org will format 
                                    for you)
                                    <asp:TextBox ID="txtBlogText" runat="server" TextMode="MultiLine" Width="600px" Height="217px"
                                                 Columns="10">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 185px">
                                </td>
                            </tr>
                        </table>
                        <asp:Button ID="btnBlogSubmit" runat="server" Text="Submit Post" OnClick="btnBlogSubmit_Click">
                        </asp:Button>
                    </asp:Panel>
                    <br />
                    <asp:Label ID="lblSubmitResult" runat="server" CssClass="PageTitleRed"></asp:Label><br />
                    <uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="ContactFormFooter" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>