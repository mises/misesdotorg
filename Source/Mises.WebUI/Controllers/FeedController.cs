﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using LinqKit;
using Mises.Data;
using Mises.Domain.Contracts;
using Mises.Domain.Documents;
using Mises.Domain.Feeds;
using Mises.Domain.Tags;
using RssToolkit.Rss;
using DailyArticle = Mises.Domain.Articles.DailyArticle;
using Document = Mises.Data.Document;
using DocumentType = Mises.Domain.Documents.DocumentType;

#endregion

namespace Mises.Web.Controllers
{
    public class FeedController : Controller
    {
        #region Daily

        public ActionResult Daily()
        {
            SyndicationFeed feed =
                new SyndicationFeed("Daily Articles",
                                    "This is a test feed",
                                    new Uri(Request.Url.AbsoluteUri),
                                    "MisesDaily",
                                    DateTime.Now);

            //var db = new Mises.Data.MisesDBDataContext(Mises.Data.BusinessBase.ConnectionString);

            var dailyArticles = DailyArticle.GetTodaysArticles();

            List<SyndicationItem> items =
                dailyArticles.Select(
                    trigger =>
                    new SyndicationItem(trigger.Title, trigger.Description, new Uri(trigger.URL), trigger.Id.ToString(),
                                        trigger.DatePosted)).ToList();


            feed.Items = items;

            return new RssActionResult { Feed = feed };
        }

        #endregion Daily

        #region Literature

        [OutputCache(Duration = 60,VaryByParam = "*")]
        public ActionResult Literature(int AuthorId = 0, int SubjectId = 0, string Subject = null, int MediaTypeId = 0)
        {
            SyndicationFeed feed =
               new SyndicationFeed("Mises Institute New Literature",
                                   "Mises Institute New Literature",
                                   new Uri("http://mises.freecapitalists.org//Literature"),
                                   "MisesFeed",
                                   DateTime.Now)
                   {ImageUrl = new Uri("http://mises.freecapitalists.org//images/front/studyguide2.gif"), Language = "en-us"};


            //var img = new MediaImage();
            //img.Link = "http://mises.freecapitalists.org//Literature";
            //img.Url = "http://mises.freecapitalists.org//images/front/studyguide2.gif";
            //img.Title = "Mises Institute New Literature";
            ////Rss.Channel.Images = new List<MediaImage>();
            ////Rss.Channel.Images.Add(img);
            //Rss.Channel.Image = img;

            // iTunes Stuff
            //var rating = new MediaRating();
            //rating.Scheme = "urn:simple";
            //rating.Text = "noadult";

            //Rss.Channel.Keywords = new List<string>();
            //Rss.Channel.Keywords.Add("Mises");

            // Categories

            feed.Categories.Add(new SyndicationCategory("Economics"));
            feed.Categories.Add(new SyndicationCategory("History"));
            feed.Categories.Add(new SyndicationCategory("Society & Culture"));

            feed.Authors.Add(new SyndicationPerson("webmaster@freecapitalists.org", Configuration.Organization, "http://mises.freecapitalists.org/"));
            feed.Copyright = new TextSyndicationContent(Configuration.Copyright);

            var items2 = new BindingList<SyndicationItem>();

            var media = new DocumentRepository(Data.DataHelper.EntityDataModel);

            var extensions = feed.ElementExtensions;

            extensions.Add(new SyndicationElementExtension("explicit", Configuration.itunesNs, "no"));

            var documents = media.GetLiteratureFeed(AuthorId, MediaTypeId, SubjectId, Subject);

            foreach (Document document in documents)
            {
               document.Description = document.Description.PadRight(1000).Substring(0, 1000).Trim();

                var item = new SyndicationItem(document.Title, document.Description, new Uri("http://mises.freecapitalists.org//document/" + document.DocumentId + "/" + document.Title.ToSlug()))
                {
                    PublishDate = new DateTimeOffset(document.CreateDate),
                    Summary = TextSyndicationContent.CreateHtmlContent(document.Description),
                    Id = document.GUID.ToString()
                };

                item.Authors.Add(new SyndicationPerson("", document.DocumentAuthor.ToString(), null));

                if (document.DocumentAuthor2 != null && document.DocumentAuthor2.AuthorId > 0)
                {
                    item.Authors.Add(new SyndicationPerson("", document.DocumentAuthor2.ToString(), null));
                }

                //  http://stackoverflow.com/questions/9320560/syndicationfeed-attributeextensions-namespace-prefix

                extensions.Add(new SyndicationElementExtension("author", Configuration.itunesUNs, document.DocumentAuthor.ToString()));
                extensions.Add(new SyndicationElementExtension("subtitle", Configuration.itunesUNs, DataFormat.StripHTML(document.PublicationInformation)));

                if (!string.IsNullOrWhiteSpace(document.Description))
                {
                    extensions.Add(new SyndicationElementExtension("summary", Configuration.itunesUNs, DataFormat.StripHTML(document.Description)));
                }

                if (!string.IsNullOrWhiteSpace(document.Keywords))
                {
                    extensions.Add(new SyndicationElementExtension("keywords", Configuration.itunesUNs, document.Keywords.PadRight(255).Substring(0, 254)));
                }

                var files = document.DocumentFiles.Where(df => df.DocumentMediaType.IsMedia && df.Display).ToList();


                files.ForEach(file =>
                {

                    if (file.duration > 0)
                    {
                        extensions.Add(new SyndicationElementExtension("duration", Configuration.itunesUNs, file.duration));
                    }

                    string mimetype = "";

                    if (file.DocumentMediaType.MIMEtype != null)
                        mimetype = file.DocumentMediaType.MIMEtype.Replace("//mpeg", "");

                    SyndicationLink link = SyndicationLink.CreateMediaEnclosureLink(new Uri(file.URL), mimetype, file.fileSize);

                    //item.Content.Duration = TimeSpan.FromSeconds(Int32.Parse(row.duration.ToString())).ToString();

                    item.Links.Add(link);


                });



                items2.Add(item);
                feed.Items = items2;
                feed.Items.ToList().Add(item);
            }


            if (AuthorId > 0 && items2.Any())
            {
                feed.Title = new TextSyndicationContent("Mises Literature Archives for " + items2[0].Authors[0].Name);
            }

            if (SubjectId > 0 && items2.Any())
            {
                feed.Title = new TextSyndicationContent("Mises Subject Archives for " + Subject);
            }

            if (MediaTypeId > 0 && items2.Any())
            {
                feed.Title = new TextSyndicationContent("Mises feed for new " + ((MediaTypes)MediaTypeId));
            }

            return new RssActionResult { Feed = feed };
        }

        #endregion

        #region Tags

        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Tags(string id)
        {
            SyndicationFeed feed =
                new SyndicationFeed("Mises Institute Tagged Documents",
                                    "Tagged Documents from The Mises Institute on Austrian Economics and Libertarianism",
                                    new Uri(Request.Url.AbsoluteUri),
                                    "Tags",
                                    DateTime.Now)
                    {
                        BaseUri = new Uri("http://mises.freecapitalists.org//clouds.aspx"),
                        ImageUrl = new Uri("http://mises.freecapitalists.org//Theme/images/ico/tags.png"),
                        Items =
                            Tagging.GetDocumentsWithTag(id).Select(
                                doc =>
                                new SyndicationItem(doc.Title, doc.Description + doc.DocumentTypeString,
                                                    new Uri(DataFormat.GetAbsoluteURL(doc.URL)),
                                                    doc.Identifier.ToString(), new DateTimeOffset(doc.CreateDate))).
                            ToList()
                    };



            return new RssActionResult { Feed = feed };
        }

        #endregion Tags

        [OutputCache(Duration = 60, VaryByParam = "*")]
        public ActionResult Media(int AuthorId = 0, int MediaTypeId = 0, int CategoryId = 0)
        {
            SyndicationFeed feed =
               new SyndicationFeed("Mises Institute Media", ContentHelper.PageContent(null, "MediaHeader").ToHtmlString().StripHTML(),
                                   new Uri("http://mises.freecapitalists.org//Media"), Request.Url.ToString(), DateTime.Now);


            feed.ImageUrl = new Uri("http://mises.freecapitalists.org/images/front/podcast.gif");
            feed.Language = "en-us";

            #region Categories
            feed.Categories.Add(new SyndicationCategory("Society & Culture"));
            feed.Categories.Add(new SyndicationCategory("History"));
            feed.Categories.Add(new SyndicationCategory("Non-Profit"));

            feed.AttributeExtensions.Add(new XmlQualifiedName(Configuration.itunesPrefix, "http://www.w3.org/2000/xmlns/"), Configuration.itunesNs);
            feed.AttributeExtensions.Add(new XmlQualifiedName(Configuration.itunesUPrefix, "http://www.w3.org/2000/xmlns/"), Configuration.itunesUNs);
            var itemExtensions = feed.ElementExtensions;



            var extensions = feed.ElementExtensions;

            extensions.Add(new SyndicationElementExtension("explicit", Configuration.itunesNs, "no"));


            var categoryElemOwner = XName.Get("owner", Configuration.itunesNs);
            extensions.Add(
                new XElement(categoryElemOwner,
                             new XElement(categoryElemOwner, "name", Configuration.Organization),
                                 new XElement("email", Configuration.Email)
                    ).CreateReader()
                );

            var categoryElem = XName.Get("category", Configuration.itunesNs);
            extensions.Add(
                new XElement(categoryElem,
                             new XAttribute("text", "Government & Organizations"),
                             new XElement(categoryElem,
                                          new XAttribute("text", "Non-Profit")
                                 )
                    ).CreateReader()
                );

            var categoryElem2 = XName.Get("category", Configuration.itunesNs);
            extensions.Add(
                new XElement(categoryElem2,
                    new XAttribute("text", "Society & Culture"),
                        new XElement(categoryElem2,
                            new XAttribute("text", "History")
                        )
                ).CreateReader()
            );

            #endregion Categories

            feed.Copyright = new TextSyndicationContent(Configuration.Copyright);

            var items2 = new BindingList<SyndicationItem>();

            var media = new DocumentRepository(Data.DataHelper.EntityDataModel);

            var documents = media.GetMediaFeed(AuthorId, MediaTypeId, CategoryId);

            foreach (Document document in documents)
            {
                var item = new SyndicationItem(document.Title, document.Description, new Uri("http://mises.freecapitalists.org//document/" + document.DocumentId + "/" + document.Title.ToSlug()))
                               {
                                   PublishDate = new DateTimeOffset(document.CreateDate),
                                   Summary = TextSyndicationContent.CreateHtmlContent(document.Description),
                                   Id = document.GUID.ToString()
                               };

                //item.AddPermalink(new Uri(string.Format()));

                item.Authors.Add(new SyndicationPerson("", document.DocumentAuthor.ToString(), null));

                if (document.DocumentAuthor2 != null && document.DocumentAuthor2.AuthorId > 0)
                {
                    item.Authors.Add(new SyndicationPerson("", document.DocumentAuthor2.ToString(), null));
                }

                //  http://stackoverflow.com/questions/9320560/syndicationfeed-attributeextensions-namespace-prefix

                extensions.Add(new SyndicationElementExtension("author", Configuration.itunesUNs, document.DocumentAuthor.ToString()));
                extensions.Add(new SyndicationElementExtension("subtitle", Configuration.itunesUNs, document.PublicationInformation));

                if (!string.IsNullOrWhiteSpace(document.Description))
                {
                    extensions.Add(new SyndicationElementExtension("summary", Configuration.itunesUNs, document.Description));
                }

                if (!string.IsNullOrWhiteSpace(document.Keywords))
                {
                    extensions.Add(new SyndicationElementExtension("keywords", Configuration.itunesUNs, document.Keywords.PadRight(255).Substring(0, 254)));
                }

                if (document.MediaCategory != null)
                {
                    extensions.Add(new SyndicationElementExtension("category", Configuration.itunesUNs, "category"));

                    var itemCategoryElem = XName.Get("category", Configuration.itunesUNs);

                    //itemExtensions.Add(new XAttribute("code", document.MediaCategory.iTunesCategoryCode));

                    if (document.MediaCategory.iTunesCategoryCode != null)
                        itemExtensions.Add(
                            new XElement(itemCategoryElem,
                                new XAttribute("code", document.MediaCategory.iTunesCategoryCode)
                            ).CreateReader()
                        );

                }


                var files = document.DocumentFiles.Where(df => df.DocumentMediaType.IsMedia && df.Display).ToList();


                files.ForEach(file =>
                                                                                                                   {

                                                                                                                       if (file.duration > 0)
                                                                                                                       {
                                                                                                                           extensions.Add(new SyndicationElementExtension("duration", Configuration.itunesUNs, file.duration));
                                                                                                                       }

                                                                                                                       string mimetype = "";

                                                                                                                       if (file.DocumentMediaType.MIMEtype != null)
                                                                                                                           mimetype = file.DocumentMediaType.MIMEtype.Replace("//mpeg", "");

                                                                                                                       SyndicationLink link = SyndicationLink.CreateMediaEnclosureLink(new Uri(file.URL), mimetype, file.fileSize);

                                                                                                                       //item.Content.Duration = TimeSpan.FromSeconds(Int32.Parse(row.duration.ToString())).ToString();

                                                                                                                       item.Links.Add(link);


                                                                                                                   });



                items2.Add(item);
                feed.Items = items2;
                feed.Items.ToList().Add(item);
            }


            #region Filters

            if (AuthorId > 0)
            {

                var author = DocumentAuthors.GetAuthorByAuthorId(AuthorId);

                if (author != null)
                {
                    string name = author.ToString();

                    if (!string.IsNullOrWhiteSpace(author.Born))
                    {
                        name += string.Format(" ({0} - {1})", author.Born, author.Died);
                    }
                    feed.Title = new TextSyndicationContent(name);
                    feed.Description = new TextSyndicationContent(author.BioText);

                    if (!string.IsNullOrWhiteSpace(author.Photo))
                        feed.ImageUrl = new Uri(DataFormat.GetAbsoluteURL(author.Photo));
                    feed.Authors.Add(new SyndicationPerson("", name, string.Format("http://mises.freecapitalists.org//authors/{0}/{1}", AuthorId, name.ToSlug())));


                }
            }
            else
            {
                // generic author
                feed.Authors.Add(new SyndicationPerson(Configuration.Email, Configuration.Organization, "http://" + Request.Url.Host));
            }

            if (MediaTypeId > 0 && items2.Any())
            {
                feed.Title = new TextSyndicationContent("Mises Media: new " + ((MediaTypes)MediaTypeId));
            }

            if (CategoryId > 0 && items2.Any())
            {
                var category = documents.First().MediaCategory;
                if (category != null)
                {
                    feed.Title = new TextSyndicationContent("Mises Media: " + category.Category);
                    feed.Description = new TextSyndicationContent(category.Description);
                    if (!string.IsNullOrWhiteSpace(category.CategoryImage))
                        feed.ImageUrl = new Uri(DataFormat.GetAbsoluteURL(category.CategoryImage));
                }
            }

            #endregion Filters

            // <itunes:image>http://mises.freecapitalists.org/images/front/podcast.gif</itunes:image>
            extensions.Add(new SyndicationElementExtension("image", Configuration.itunesNs, feed.ImageUrl.ToString()));


            // <image href="http://mises.freecapitalists.org/images/front/podcast.gif" xmlns="http://www.itunes.com/dtds/podcast-1.0.dtd"></image>
            var imageElement = XName.Get("image", Configuration.itunesNs);
            extensions.Add(
                new XElement(imageElement,
                             new XAttribute("href", feed.ImageUrl.ToString())
                    ).CreateReader()
                );

            // DOesn't work
            //<itunes:image href=" http://mises.freecapitalists.org/images/front/podcast.gif "/>
            //extensions.Add(new XElement("image",new XAttribute("href", feed.ImageUrl.ToString())).CreateReader()
            //        );



            return new RssActionResult { Feed = feed };
        }


        /// <summary>
        /// http://feeds.mises.org/MisesStore
        /// http://mises.freecapitalists.org//Feed/NewProducts
        /// </summary>
        /// <returns></returns>
        public ActionResult NewProducts()
        {
            SyndicationFeed feed =
                new SyndicationFeed("New Books from the Mises Institute", "New Books from The Mises Institute on Austrian Economics and Libertarianism",
                                    new Uri("http://store.mises.org"), Request.Url.ToString(), DateTime.Now);


            var db =
            new AbleCommerceDBDataContext(DataHelper.StoreConnectionString);
            var books = db.spGetNewBooks();

            var items = new BindingList<SyndicationItem>();

            books.ForEach(
                    book =>
                    {
                        var item = new SyndicationItem(book.Name, book.DESCRIPTION,
                                                   new Uri(String.Format("http://store.mises.org/{0}",
                                                                         book.DisplayPage))) { Id = book.Sku, PublishDate = book.CreatedDate };

                        item.Authors.Add(new SyndicationPerson("", book.AuthorName, ""));

                        items.Add(item);
                    });

            feed.Items = items;

            return new RssActionResult { Feed = feed };
        }

    }
}