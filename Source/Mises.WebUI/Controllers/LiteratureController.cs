﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookCatalog.Models;
using LinqKit;
using Mises.Data;
using Mises.Data.DataModels;
using Mises.Domain.Documents;
using Document = Mises.Data.Document;

namespace Mises.Web.Controllers
{
    public class LiteratureController : Controller
    {
        //
        // GET: /Literature/

        #region Filter Lists

        public ActionResult Authors(int id = 0, int page = 0)
        {
            var mises = DataHelper.EntityDataModel;

            List<AuthorViewModel> authorList = (from author in mises.DocumentAuthors
                                                where author.Documents.Any(d => d.DocumentFiles.Any(f => !f.DocumentMediaType.IsMedia))
                                                select new AuthorViewModel

                              {
                                  AuthorFirst = author.AuthorFirst,
                                  AuthorMiddle = author.AuthorMiddle,
                                  AuthorLast = author.AuthorLast,
                                  AuthorId = author.AuthorId,
                                  BioText = author.BioText,
                                  Born = author.Born,
                                  Died = author.Died,
                                  Books = author.Documents.Count,
                                  Photo = author.Photo

                              }).OrderBy(a => a.AuthorLast).ToList();

            return View(authorList);
            //return View(mises.DocumentAuthor.ToList());
        }

        #endregion

        #region Browse

        public const int PageSize = 50;

        public ActionResult Index(int page = 0, bool titleSort = false)
        {
            return GetDocumentList(page, titleSort: titleSort);
        }

        public ActionResult Author(int id, int page = 0, bool titleSort = false)
        {
            return GetDocumentList(page, null, id, titleSort: titleSort);
        }


        public ActionResult Subject(int id, int page = 0, bool titleSort = false)
        {
            return GetDocumentList(page, null, 0, id, titleSort: titleSort);
        }

        public ActionResult Source(string id, int page = 0, bool titleSort = false)
        {
            if (id == null) id = "books";
            return GetDocumentList(page, id.ToLower() == "books" ? "books" : null, 0, 0, id);
        }

        public ActionResult Format(int id, int page = 0, bool titleSort = false)
        {
            return GetDocumentList(page, null, 0, 0, null, id, titleSort: titleSort);
        }

        public ActionResult ClientSearch(string text)
        {
            if (string.IsNullOrWhiteSpace(text) || (text.Length < 2))
            {
                // return blank response
                Response.Cache.SetMaxAge(TimeSpan.FromDays(30));
                return new JsonResult { Data = new SelectList(new List<AutoCompleteModel>(), "Url", "Name") };
            }


            var mises = DataHelper.EntityDataModel;
            var docs = mises.Documents.Include("DocumentAuthor").Include("DocumentFiles").Where(b => b.Title.Contains(text) || b.DocumentAuthor.AuthorLast.Contains(text) || b.Description.Contains(text) || b.Keywords.Contains(text))
                .Select(f => new { Author = f.DocumentAuthor.AuthorLast, f.Title, f.DocumentId, f.CoverImageURL, File = f.DocumentFiles.FirstOrDefault() });

            var results = new List<AutoCompleteModel>();

            docs.ToList().ForEach(doc => results.Add(new AutoCompleteModel
            {
                Key = doc.DocumentId,
                Title = doc.Title,
                Name = string.Format(@"<img src=""{0}"" alt=""{0}"" />{1} : {2}", doc.File != null ? NavigationModels.GetMediaTypeFromId(doc.File.MediaTypeId).MediaIconPath : NavigationModels.GetMediaTypeFromId(4).MediaIconPath, doc.Author, doc.Title),
                Thumbnail = doc.CoverImageURL,
                Url = string.Format("/document/{0}/{1}", doc.DocumentId, doc.Title.ToSlug())
            }));

            var jsonData = new SelectList(results.Take(50).ToList(), "Url", "Name");
            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = jsonData
            };

        }

        public ActionResult Search(string q, int page = 0)
        {
            string term = q.Substring(q.IndexOf(":", System.StringComparison.Ordinal) + 2);
            var books = new List<LiteratureViewModel>();

            const string defaultMediaCategory = "Text";

            var dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                         "dbo.DocumentGetLinks",
                                         new SqlParameter("@SearchQuery", DataFormat.StripHTML(term.Trim())),
                                         new SqlParameter("@MediaCategory", defaultMediaCategory)).Tables[0];
            int? count = dt.Rows.Count;

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    books.Add(new LiteratureViewModel()
                    {
                        Title = row.Field<string>("Title"),
                        Author = row.Field<string>("Author"),
                        CoAuthor = row.Field<string>("CoAuthor"),
                        //Source = row.Field<string>("Source"),
                        Description = row.Field<string>("Description") + (row.Field<int>("DocumentId") == 0 ? " - " + row.Field<string>("Source") : string.Empty),
                        PublicationInformation = row.Field<string>("Source"),
                        DocumentId = row.Field<int>("DocumentId"),
                        AuthorId = row.Field<int?>("Author1"),
                        CoAuthorId = row.Field<int?>("Author2"),
                        EditDate = row.Field<DateTime>("DatePosted"),
                        Thumbnail = "media/poster/" + row.Field<int>("DocumentId"),
                        Formats = new List<DocumentFile>() { new DocumentFile() { MediaTypeId = row.Field<int>("MediaTypeId"), URL = row.Field<string>("URL"), DocumentMediaType = new DocumentMediaType() { MediaIconPath = row.Field<string>("MediaIconPath"), }, VolumeComment = row.Field<string>("Source") } }
                        // GUID = row.Field<Guid?>("GUID")


                        //Author = doc.DocumentAuthor != null ? doc.DocumentAuthor.AuthorFirst + " " + doc.DocumentAuthor.AuthorMiddle + " " + doc.DocumentAuthor.AuthorLast : "",
                        //AuthorId = doc.DocumentAuthor.AuthorId,
                        //Description = doc.Description,
                        //DocumentId = doc.DocumentId,
                        //Thumbnail = doc.CoverImageURL,
                        //Title = doc.Title,
                        //EditDate = doc.EditDate,
                        //Formats = doc.DocumentFiles.ToList()
                    });

                    

                }
            }
            else
            {
                var db2 = DataHelper.MisesDataContext;
                // "1;2;4;8;9;3"
                const string documentTypes = "3,4,5,6,7,8,9";
                var searchResults = db2.FindDocuments(term, 1000, page, null, null, null, documentTypes, ",", null, ref count).ToList();

                searchResults.ForEach(doc => books.Add(new LiteratureViewModel()
                    {
                        //Author = doc.Author1 != null ? doc.DocumentAuthor.AuthorFirst + " " + doc.DocumentAuthor.AuthorMiddle + " " + doc.DocumentAuthor.AuthorLast : "",
                        GUID = doc.GUID,
                        AuthorId = doc.Author1,
                        CoAuthorId = doc.Author2,
                        // Description = doc.Description,
                        DocumentId = doc.DocumentId,
                        //  Thumbnail = doc.metaImage,
                        Title = doc.Title,
                        EditDate = doc.EditDate,
                        Thumbnail = "media/poster/" + doc.DocumentId,
                        //  Formats = doc.DocumentFiles.ToList()
                    }));


            }
            // @Html.Pager((int)@ViewBag.PageSize, (int)ViewBag.CurrentPage, (int)@ViewBag.Total)
            ViewBag.PageSize = 1000;
            ViewBag.CurrentPage = page;
            ViewBag.Total = count;

            return View("Index", books);
            //return GetDocumentList(page, term, 0, 0, null, 0);
        }

        public ActionResult AdvancedSearch(int page = 0)
        {
            return View();
        }

        #endregion Browse



        //public ActionResult Test()
        //{
        //    var db = DataHelper.EntityDataModel;

        //    var query2 = (from docs in db.Documents where docs.DocumentId == 3088 orderby docs.DocumentId descending select docs).ToList();
        //    query2.ForEach(v => Response.Write(string.Format("<h2>{0}</h2>", v.DocumentId.ToString())));


        //    //var query = mises.Documents.Where(d => d.DocumentId == 3088).OrderBy(d => d.EditDate).ToList();
        //    return Content(query2.Skip(1).First().DocumentAuthor.AuthorLast.ToSlug());

        //}
        #region Data Access


        public ActionResult GetDocumentList(int page, string filter = null, int AuthorId = 0, int SubjectId = 0, string source = null, int MediaTypeId = 0, bool titleSort = false)
        {
            //bool? titleSortTemp = (bool?)ViewData["titleSort"];
            //if (titleSortTemp == true)
            //{
            //    titleSort = true;
            //}



            ViewBag.PageSize = PageSize;
            ViewBag.CurrentPage = page;
            ViewBag.Total = 0;

            if (page > 0)
            {
                page = page - 1;    
            }

            var db = DataHelper.EntityDataModel;

            DataHelper.SetTransactionIsolationLevelReadUncommited(db);

            int[] bookDocTypes = { 4, 6, 9, 3 };

            IQueryable<Document> query = db.Documents.Include("DocumentAuthor").Include("DocumentAuthor2").Include("DocumentFiles").Where(docs => !docs.DocumentFiles.Any() || docs.DocumentFiles.Any(file => bookDocTypes.Contains(file.MediaTypeId))).Where(v => v.Display);

            //if (MediaTypeId == 0 && filter == null)
            //{
            //    query = query.Where(d => bookDocTypes.Contains(d.DocumentFiles.URL.Value));
            //}

            #region Filters

            if (AuthorId > 0)
            {
                query = query.Where(a => a.DocumentAuthor.AuthorId == AuthorId || a.DocumentAuthor2.AuthorId == AuthorId);

                // get author info

                var author = db.DocumentAuthors.SingleOrDefault(d => d.AuthorId == AuthorId);

                if (author != null)
                {
                    ViewBag.Author = new AuthorViewModel
                    {
                        AuthorFirst = author.AuthorFirst,
                        AuthorMiddle = author.AuthorMiddle,
                        AuthorLast = author.AuthorLast,
                        AuthorId = author.AuthorId,
                        BioText = author.BioText,
                        Born = author.Born,
                        Died = author.Died,
                        Books = author.Documents.Count,
                        Photo = author.Photo
                    };
                }
                else
                {
                    ViewBag.Author = new AuthorViewModel { AuthorLast = "Author not found" };
                }
            }

            if (SubjectId > 0)
            {
                //query = query.Where(a => a.Author1 == AuthorId || a.Author2 == AuthorId);
                var documentIds = db.DocumentSubjectLink.Where(d => d.SubjectID == SubjectId).Select(d => d.DocumentId);
                query = query.Where(d => documentIds.Contains(d.DocumentId));
            }

            if (MediaTypeId > 0)
            {
                query = query.Where(a => a.DocumentFiles.Any(m => m.MediaTypeId == MediaTypeId));
            }

            if (!String.IsNullOrWhiteSpace(source) && source.ToLower() != "books")
            {
                query = query.Where(a => a.Source == source);
            }

            if (filter != null && filter.Equals("books", StringComparison.InvariantCultureIgnoreCase))
            {
                query = query.Where(a => a.DocumentFiles.Any(m => m.MediaTypeId == 9)); // 9 = ebook
            }
            else if (filter != null)
            {
                return this.Search(filter);

                //query = query.Where(b => b.Title.Contains(filter) || b.Description.Contains(filter) || b.Keywords.Contains(filter));
            }

            #endregion Filters
            ViewBag.Total = query.Count();

            //var query2 = from q in query
            //             let file = q.DocumentFiles.OrderByDescending(df => df.CreateDate).GroupBy(df => df.DocumentId).FirstOrDefault()
            //             orderby file.FirstOrDefault().CreateDate, q.EditDate descending 
            //             select q;

            
            if (!titleSort)
            {
                // normal paging
                query = query.OrderByDescending(d => d.EditDate).Skip(PageSize * page).Take(PageSize);
            }

            query.ForEach(doc =>
                {
                    if (doc.ProductId > 0)
                    {
                        doc.DocumentFiles.Add(new DocumentFile
                        {
                            VolumeComment = "Mises Store Link",
                            URL =
                                string.Format(
                                    "http://store.mises.org/Product.aspx?ProductId={0}&utm_source=Resources",
                                    doc.ProductId),
                            MediaTypeId = 7,
                            DocumentMediaType = new DocumentMediaType() { MediaIconPath = "/images/icons/store.png", Description = "Mises Store Link", },
                            CreateDate = doc.CreateDate
                        });
                    }

                }
                );

            var books = new List<LiteratureViewModel>();

            query.ForEach(doc => books.Add(new LiteratureViewModel()
            {
                Author = doc.DocumentAuthor != null ? doc.DocumentAuthor.AuthorFirst + " " + doc.DocumentAuthor.AuthorMiddle + " " + doc.DocumentAuthor.AuthorLast : "",
                AuthorId = doc.DocumentAuthor.AuthorId,
                Description = doc.Description,
                DocumentId = doc.DocumentId,
                Thumbnail = doc.CoverImageURL,
                Title = doc.Title,
                EditDate = doc.EditDate,
                Formats = doc.DocumentFiles.ToList()
            }));

            if (titleSort)
            {
                books = books.OrderBy(book => book.TitleForSort).Skip(PageSize * page).Take(PageSize).ToList();
            }

            return View("Index", books);
        }



        #endregion


    }

    public class AutoCompleteModel
    {
        public int Key { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string Title { get; set; }
    }
}
