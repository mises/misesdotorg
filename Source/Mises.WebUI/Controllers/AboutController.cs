﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mises.Data;

namespace Mises.Web.Controllers
{
    public class AboutController : Controller
    {
        //
        // GET: /About/

        public ActionResult Index(int? id)
        {
            if (id == null || id == 0) return Redirect("/page/1448");

            var db = DataHelper.EntityDataModel;

            var document = db.Documents.Include("DocumentFiles").SingleOrDefault(d => d.DocumentId == id);
            

            if (document.DocumentFiles.Any(p => p.URL.StartsWith("/page")))
            {
                var page = document.DocumentFiles.FirstOrDefault(f=> f.URL.StartsWith("/page"));
                return Redirect(page.URL + "/" + page.Document.Title.ToSlug());
            }
            else
            {
                return Redirect("/document/" + document.DocumentId + "/" + document.Title.ToSlug());
            }

           // return Content(document.ToString());

        }

    }
}
