﻿#region

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mises.Data;
using Mises.Domain.Utility;
using RedirectedURL = Mises.Domain.Utility.RedirectedURL;

#endregion

namespace Mises.Web.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        private string NewURL { get; set; }

        public ActionResult ClassicAsp(string url)
        {
            return Http404(null, url);
        }


        public ActionResult Http404(string aspxerrorpath, string requestedURL)
        {
            ViewBag.Title = "Page not found";

            if (aspxerrorpath != null)
                requestedURL = aspxerrorpath;

            try
            {
                Response.StatusCode = 404;
                if (requestedURL == null) return View("Http404");

                string baseURL = Request.Url.Scheme + "://" + Request.Url.Authority;

                requestedURL = requestedURL.Replace(baseURL + "/Error/Http404?404;" + baseURL + ":80", string.Empty)
                        .Replace(baseURL + "/Error/Http404/?aspxerrorpath=", string.Empty)
                        .Replace(baseURL + "/Error/Http404?aspxerrorpath=", string.Empty)
                        .Replace(baseURL + "/Error/Http404?404;" + baseURL + ":80", string.Empty);

                if (!string.IsNullOrWhiteSpace(aspxerrorpath))
                {
                    requestedURL = aspxerrorpath;
                }

                // Don't check these urls:
                if (requestedURL.Contains("App_Themes")) return View("Http404");
                //if (requestedURL.Contains("http://store.mises.org")) return View("Http404");

                if (requestedURL.Contains(".asp")) // requesting an old-format html page
                {
                    string aspPage = requestedURL.Substring(requestedURL.LastIndexOf("/") + 1);
                    string path = requestedURL.Replace(aspPage, "");

                    path = path.Trim('/');

                    Guid _Identifier = (Mises.Domain.Documents.Document.GetPageGUID(aspPage, path));

                    var mm = DataHelper.EntityDataModel;
                    var page = mm.PageTableSet.SingleOrDefault(p => p.GUID == _Identifier);

                    if (page == null)
                    {
                        var pages = mm.PageTableSet.SingleOrDefault(p => p.GUID == _Identifier);

                        if (pages != null)
                        {
                            page = new PageTable() { content = pages.content, pageTitle = pages.pageTitle, GUID = pages.GUID, PageId = pages.PageId, metaDescription = pages.metaDescription };

                        }
                    }

                    ViewBag.PageId = page.PageId;
                    ViewData.Add("Id",page.PageId);
                    Response.StatusCode = 200;
                    return View("Page", page);

                    //return RedirectToAction("Index", "Resources", new {Id = _Identifier});

                    //if (Guid.Empty != _Identifier)
                    //{
                    //    Response.RedirectPermanent(string.Format("http://{0}/resources.aspx?Id={1}", HttpContext.Request.Url.Host, _Identifier), true);
                    //}
                }



                //try
                //{
                //    ExceptionLogging.LogException(Context, new Exception(requestedURL));
                //}
                //catch
                //{
                //}

                // temporary: logging all 404's
                //try
                //{
                //    ExceptionLogging.LogHttpError(HttpContext, this.Request);
                //}
                //catch (Exception ex)
                //{
                //    Debug.WriteLine(ex);
                //}
                
                try
                {
                    CheckFilesystem(requestedURL);
                }
                catch { }
                try
                {
                    DynamicURLRedirection(requestedURL);
                }
                catch { }
                try
                {
                    CheckDatabase(requestedURL);
                }
                catch { }

                if (NewURL != null)
                {
                    if (!NewURL.StartsWith("http") && !NewURL.StartsWith("/"))
                        NewURL = "/" + NewURL;

                    return RedirectPermanent(NewURL);
                }
                else
                {

                    //if (requestedURL.EndsWith(".jpg")) return View("Http404");
                    //if (requestedURL.EndsWith(".jpeg")) return View("Http404");
                    //if (requestedURL.EndsWith(".gif")) return View("Http404");
                    //if (requestedURL.EndsWith(".png")) return View("Http404");

                    // Response.Status = "404 Not Found";
                    Response.StatusCode = 404;
                    Response.StatusDescription = requestedURL;
                    return View("Http404");
                }
            }
            catch //(Exception exception)
            {
                //  ExceptionLogging.LogException(HttpContext, exception);
            }

            return View("Http404");
        }

        #region Handle 404




        private void CheckDatabase(string requestedURL)
        {
            string redirectURL = RedirectedURL.GetRedirectedURL(requestedURL);
            if (!string.IsNullOrWhiteSpace(redirectURL))
            {
                NewURL = redirectURL;
                NewURL = (redirectURL);
            }
        }

        private void DynamicURLRedirection(string requestedURL)
        {
            if (requestedURL.Contains("blog/"))
            {
                try
                {
                    // TODO: FIX to relative URL 
                    NewURL = ("http://blog.mises.org/" +
                             requestedURL.Substring(requestedURL.LastIndexOf("blog/archive")));
                }
                catch
                {
                    // TODO: FIX to relative URL
                    NewURL = ("http://blog.mises.org/");
                }
            }
            else if (requestedURL.Contains("/blogs/"))
            {
                NewURL = (requestedURL.Replace("/blogs", "/Community/blogs"));
            }
            else if (requestedURL.Contains("forums"))
            {
                NewURL = ("http://mises.freecapitalists.org//community/forums/");
            }
            //else if (requestedURL.Contains("fullstory") || requestedURL.Contains("fullarticle"))
            //{
            //    // /fullarticle.asp?Id=679&month=32
            //    requestedURL =
            //        requestedURL.Replace("id", "Id").Replace("control", "Id").Replace("record", "Id").Replace(
            //            "fullarticle.asp?Id=", "daily/").Replace("fullstory.asp?Id=", "daily/").Replace("&month=", "/");
            //    NewURL = (requestedURL);
            //}
            else if (requestedURL.Contains("articles.asp?author="))
            {
                NewURL = (requestedURL.Replace("articles.asp", "articles.aspx"));
            }
            //else if (requestedURL.Contains("wardlibrary_detail.asp?control="))
            //{
            //    NewURL = (requestedURL.Replace("/library", "/").Replace("wardlibrary_detail.asp?control=",
            //                                                           "book.aspx?Id="));
            //}
            //else if (requestedURL.Contains("misesreview_detail.asp?"))
            //{
            //    NewURL = (requestedURL.Replace("misesreview_detail.asp", "misesreview_detail.aspx"));
            //}
            //else if (requestedURL.Contains("freemarket_detail.asp?"))
            //{
            //    NewURL = (requestedURL.Replace("freemarket_detail.asp", "freemarket_detail.aspx"));
            //}
            //else if (requestedURL.Contains("StudyGuideDisplay.asp?SubjID="))
            //{
            //    NewURL = (requestedURL.Replace("StudyGuideDisplay.asp?SubjID=",
            //                                  "literature.aspx?action=subject&amp;Id="));
            //}
            //else if (requestedURL.Contains("studyguide"))
            //{
            //    NewURL = (requestedURL.Replace("studyguide", "literature"));
            //}
            //else if (requestedURL.Contains("upcomingstory.asp"))
            //{
            //    NewURL = (requestedURL.Replace("upcomingstory.asp", "event.aspx"));
            //}
            else if (requestedURL.Contains("fellows.asp"))
            {
                //NewURL = ("/faculty.aspx");
                NewURL = (requestedURL.Replace("fellows.asp?control=", "fellow.aspx?Id="));
            }
            //else if (requestedURL.Contains("quiz.asp?"))
            //{
            //    NewURL = (requestedURL.Replace("quiz.asp?QuizID=", "quiz.aspx?QuizID="));
            //}
        }


        private void CheckFilesystem(string requestedURL)
        {
            if (requestedURL.Contains("?"))
            {
                return;
            }

            string fileName = DataFormat.GetFileNameFromURL(requestedURL);

            // In-depth PDF search:
            if (requestedURL.EndsWith(".pdf", StringComparison.CurrentCultureIgnoreCase))
            {
                string path = Server.MapPath("/Books/" + requestedURL);

                if (System.IO.File.Exists(path))
                {
                    NewURL = ("/Books/" + requestedURL);
                    return;
                }

                if (System.IO.File.Exists(Server.MapPath("/Books/" + fileName)))
                {
                    NewURL = ("/Books/" + fileName);
                    return;
                }

                path = Server.MapPath("/PDF/" + requestedURL);
                if (System.IO.File.Exists(path))
                {
                    //Response.ContentType = "application/pdf"
                    NewURL = ("/pdf/" + requestedURL);
                    return;
                }

                var pdfFolders = System.IO.Directory.GetDirectories(Server.MapPath("/PDF/")).ToList();

                pdfFolders.ForEach(folder =>
                                                {
                                                    if (System.IO.File.Exists(folder + requestedURL))
                                                    {
                                                        string directory = folder.Substring(folder.LastIndexOf(@"\"));
                                                        NewURL = ("/pdf/" + directory + "/" + requestedURL);
                                                        return;
                                                    }
                                                });


                // MP3 Search
            }
            else if (requestedURL.EndsWith(".mp3", StringComparison.CurrentCultureIgnoreCase))
            {
                string path = Server.MapPath("/Multimedia/mp3/" + requestedURL);
                if (System.IO.File.Exists(path))
                {
                    Response.ContentType = "audio/mpeg";
                    Response.RedirectPermanent("/Multimedia/mp3/" + requestedURL);
                }
            }
            else if (requestedURL.EndsWith(".xml", StringComparison.CurrentCultureIgnoreCase))
            {
                if (System.IO.File.Exists(Server.MapPath("/Feeds/" + fileName)))
                {
                    Response.ContentType = "application/rss+xml";
                    try
                    {
                        NewURL = ("/Feeds/" + fileName);
                    }
                    catch (HttpException)
                    {
                        Response.RedirectPermanent("/Feeds/" + fileName, false);
                    }
                }
            }
            else if (requestedURL.EndsWith(".txt", StringComparison.CurrentCultureIgnoreCase) &&
                     System.IO.File.Exists(Server.MapPath("/Text/" + fileName)))
            {
                Response.ContentType = "text/plain";
                Response.RedirectPermanent("/Text/" + fileName);
            }
            else if (requestedURL.EndsWith(".pdb", StringComparison.CurrentCultureIgnoreCase) &&
                     System.IO.File.Exists(Server.MapPath("/Palm/" + fileName)))
            {
                Response.ContentType = "application/vnd.palm";
                NewURL = ("/Palm/" + fileName);
            }
            else if (requestedURL.EndsWith(".js", StringComparison.CurrentCultureIgnoreCase) &&
                     System.IO.File.Exists(Server.MapPath("/Scripts/" + fileName)))
            {
                Response.ContentType = "application/javascript";
                NewURL = ("/Scripts/" + fileName);
            }
            else if (System.IO.File.Exists(Server.MapPath(fileName + ".asp")))
            {
                NewURL = (fileName + ".asp");
            }
            else if (System.IO.File.Exists(Server.MapPath(fileName + ".aspx")))
            {
                NewURL = (fileName + ".aspx");
            }
            //else if (requestedURL.EndsWith(".jpg", StringComparison.CurrentCultureIgnoreCase) ||
            //     requestedURL.EndsWith(".gif", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    string path = Server.MapPath("/images/" + fileName);
            //    if (File.Exists(path))
            //    {
            //        Response.ContentType = "image/jpeg";
            //        Server.Transfer("/images/" + requestedURL);
            //    }
            //    else if (File.Exists(Server.MapPath("/images/archived/" + fileName)))
            //    {
            //        Response.ContentType = "image/jpeg";
            //        Server.Transfer("/images/archived/" + fileName);
            //    }
            //}
            else if (fileName.EndsWith(".jpg") || fileName.EndsWith(".gif"))
            {
                if (System.IO.File.Exists(Server.MapPath("/Images/" + fileName)))
                {
                    if (fileName.EndsWith("jpg"))
                    {
                        Response.ContentType = "image/jpeg";
                        NewURL = ("/images/" + fileName);
                    }
                    else if (fileName.EndsWith("gif"))
                    {
                        Response.ContentType = "image/gif";
                        NewURL = ("/images/" + fileName);
                    }
                    else
                    {
                        NewURL = ("/images/" + fileName);
                    }
                }
                else if (System.IO.File.Exists(Server.MapPath("/Images/archived/" + fileName)))
                {
                    if (fileName.EndsWith("jpg"))
                    {
                        Response.ContentType = "image/jpeg";
                        NewURL = ("/images/archived/" + fileName);
                    }
                    else if (fileName.EndsWith("gif"))
                    {
                        Response.ContentType = "image/gif";
                        NewURL = ("/images/archived/" + fileName);
                    }
                    else
                    {
                        NewURL = ("/images/" + fileName);
                    }
                }
                else
                {
                    // Loop through all folders in images
                    var folders = System.IO.Directory.GetDirectories(Server.MapPath("/Images"));

                    folders.ToList().ForEach(folder =>
                                                 {
                                                     if (System.IO.File.Exists(folder + @"\" + fileName))
                                                     {
                                                         var path = folder.Replace(Server.MapPath(@"\Images\"), "");
                                                         NewURL = "/images/" + path + "/" + fileName;
                                                     }
                                                 });

                }
            }
        }

        #endregion
    }
}