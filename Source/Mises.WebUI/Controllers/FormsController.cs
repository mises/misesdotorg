﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Utility;
using Mises.Web.ViewModels.Forms;
using Donations = Mises.Domain.Payments.Donations;

namespace Mises.Web.Controllers
{
    public class FormsController : Controller
    {
        //
        // GET: /Forms/

        [HttpGet]

        [RequireHttps]
        public ActionResult RonPaul()
        {
            return View("RonPaul", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult Donate2013()
        {
            return View("Donate2013", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult HateTaxes()
        {
            return View("HateTaxes", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult GivingMisesU()
        {
            return View("GivingMisesU", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult GivingAERC()
        {
            return View("GivingAERC", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult GivingAcademy()
        {
            return View("GivingAcademy", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult GivingHighSchool()
        {
            return View("GivingHighSchool", new RegistrationFormViewModel());
        }

        [RequireHttps]
        public ActionResult GivingeBooks()
        {
            return View("GivingeBooks", new RegistrationFormViewModel());
        }
        [RequireHttps]
        public ActionResult GivingWebsite()
        {
            return View("GivingWebsite", new RegistrationFormViewModel());
        }
        [RequireHttps]
        public ActionResult GivingFellowships()
        {
            return View("GivingFellowships", new RegistrationFormViewModel());
        }
        [RequireHttps]
        public ActionResult GivingOralHistory()
        {
            return View("GivingOralHistory", new RegistrationFormViewModel());
        }
        [RequireHttps]
        public ActionResult GivingNewBooks()
        {
            return View("GivingNewBooks", new RegistrationFormViewModel());
        }
        [RequireHttps]
        public ActionResult GivingMisesUGeneral()
        {
            return View("GivingMisesUGeneral", new RegistrationFormViewModel());
        }
        [RequireHttps]
        public ActionResult GivingMisesCircles()
        {
            return View("GivingMisesCircles", new RegistrationFormViewModel());
        }

        [HttpPost]
        [RequireHttps]
        public ActionResult Submit(RegistrationFormViewModel model)
        {
            var donations = new Donations(Request.Url.ToString()) { donation = { TestMode = false } };

            try
            {

                bool success = TryUpdateModel(model);

                if (model == null) return RonPaul();

                if (!DataFormat.IsCreditCardValid(model.CreditCardNumber))
                {
                    //  Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    Response.AddHeader("error", "true");
                    return Content("Invalid credit card number.");
                }


                donations.donation.IPaddress = CloudFlare.GetOriginalHostAddress(Request);
                donations.IPaddress = CloudFlare.GetOriginalHostAddress(Request);

                GetFormValues(donations, model);

                donations.ProcessDonation();

                if (donations.Success == false)
                {
                    Response.AddHeader("error", "true");
                }

                ViewBag.AnalyticsProductId = "CustomForm";
                donations.donation.FormUrl = HttpUtility.UrlPathEncode(Request.UrlReferrer.PathAndQuery);

                return PartialView("ConfirmationMessage", donations);

            }
            catch (Exception ex)
            {

                Response.AddHeader("error", "true");

                donations.WebConfirmationMessage = ex.ToString();
                return PartialView("ConfirmationMessage", donations);
            }
        }

        private void GetFormValues(Donations donate, RegistrationFormViewModel model)
        {
            try
            {
                if (model.Email == "heroic@gmail.com") donate.donation.TestMode = true;

                if (Request.Url.Host == "localhost" || Request.Url.Host == "alpha.mises.org" ||
          Request.Url.Host == "beta.mises.org")
                {
                    donate.donation.TestMode = true;
                }
            }
            catch (Exception)
            {
            }

            if (model.amount != null)
                model.amount = model.amount.Replace("$", "").Replace("US", "");

            if (Conversion.Val(model.amount) > 0)
            {
                donate.donation.Amount = Convert.ToDecimal(model.amount);
            }
            else
            {
                donate.donation.Amount = Convert.ToDecimal(model.CustomAmount);
            }



            donate.donation.Designation = !string.IsNullOrWhiteSpace(model.Designation) ? model.Designation : "";

            //donate.donation.OtherDesignation = txtDonationDesignation;
            donate.donation.CardName = model.FirstName + " " + model.LastName;
            donate.donation.CardNumber = model.CreditCardNumber;
            //donate.donation.CardType = ddCreditCardType.SelectedValue;
            donate.CardVerification = model.CardSecurityCode;
            donate.donation.CardExpMonth = model.SelectedMonth;
            donate.donation.CardExpYear = model.SelectedYear;
            //donate.donation.Comments = txtComments.Trim();
            //donate.donation.Recurring = chkRecurring.Checked;
            //donate.InMemoryof = txtInMemoryOf;
            //donate.donation.MisesMemberStatus = chkMisesMember.Checked;
            //donate.donation.GiftEstate = chkGiftEstate.Checked;
            //donate.donation.GiftIncome = chkGiftIncome.Checked;
            donate.donation.FirstName = model.FirstName;
            donate.donation.LastName = model.LastName;
            donate.donation.Address = model.Address;
            donate.donation.Address2 = "";
            donate.donation.City = model.City;
            donate.donation.State = model.State;
            donate.donation.Country = model.Country ?? "US";
            donate.donation.Zip = model.PostalCode;
            donate.donation.Email = model.Email;
            donate.donation.Phone = model.PhoneNumber;
            donate.donation.FormUrl = Request.Url.ToString();

            donate.donation.Recurring = model.IsRecurring;
            donate.donation.Anonymous = model.IsAnonymous;

            //donate.donation.IPaddress = Request.GetOriginalHostAddress();
            //donate.IPaddress = Request.GetOriginalHostAddress();

            if (Request.UrlReferrer != null)
            { donate.donation.Referrer = Request.UrlReferrer.ToString(); }
        }

    }
}
