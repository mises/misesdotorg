﻿#region

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Mises.Data;
using Mises.Domain;
using Mises.Domain.Contracts;
using Telerik.Web.Mvc;
using DailyArticle = Mises.Domain.Articles.DailyArticle;

#endregion

namespace Mises.Web.Controllers
{
    public class DailyController : Controller
    {
        #region ViewFormat enum

        public enum ViewFormat
        {
            Album,
            List
        }

        #endregion

        private const int pagesize = 50;

        public DailyController(IServiceLayer services) //: base(services)
        {
        }

        public DailyController()
        {
        }

        public ActionResult Index()
        {
           return View();
        }

        public ActionResult Preview(int id, string preview, string slug)
        {
            return Detail(id, "preview", slug);
        }

        [OutputCache(Duration = 360, VaryByParam = "*", VaryByCustom = "userName")]
        public ActionResult Detail(int id, string preview, string slug)
        {
            if (id == 0) return RedirectToAction("Index");
            ViewBag.Id = id;

            var Article = new DailyArticle();

            try
            {
                Article = new DailyArticle(id);
            }
            catch (ArgumentOutOfRangeException)
            {
                Article.Title = "Article not found. Did you enter the Id # correctly?";
                return View("Article", Article);
            }

            if (preview == "preview") Article.IsPreview = true;

            if (Article.ShowArticle == false && string.IsNullOrWhiteSpace(preview) && slug != "preview" &&
                !(AdminHelper.IsAdmin(HttpContext.Request.Cookies)))
            {
                Article = new DailyArticle { Title = "You must sign in to view this article." };
                return View("Article", Article);
            }
            ViewBag.Title = string.Format("{0} - {1} - Mises Daily",
                                          Server.HtmlDecode(DataFormat.StripHTML(Article.Title)),
                                          Article.AuthorName);


            var facebookMeta = new StringBuilder();

            // add facebook image:

            //facebookMeta.AppendFormat("<meta property=\"og:author\" content=\"{0}\" />\n", Article.AuthorName);

            facebookMeta.AppendFormat("<meta property=\"og:title\" content=\"{0}\" />\n", Server.HtmlEncode(Article.Title.StripHTML()));



            facebookMeta.AppendFormat("<meta property=\"og:image\" content=\"{0}\" />\n",
                                      string.Format("http://s3.amazonaws.com/veksler-backup/DailyArticleImages/{0}.jpg",
                                                    Article.ArticleId));

            facebookMeta.AppendFormat("<meta property=\"og:url\" content=\"{0}\" />\n", Article.URL);

            facebookMeta.AppendFormat("<meta property=\"og:type\" content=\"article\" />\n");
            facebookMeta.AppendFormat("<meta property=\"fb:app_id\" content=\"{0}\" />\n", "216712848362908");

            ViewBag.Meta = facebookMeta.ToString();


            //Comments1.DocumentId = article.Identifier.ToString();

            //if (TagCloud != null)
            //{
            //    TagCloud.DocumentIdentifier = Article.Identifier;
            //}
            //Bookmark.Bind(Article.URL, Article.Title, Article.Description);


            return View("Article", Article);
        }

        [OutputCache(Duration = 360, VaryByParam = "*")]
        [HttpGet]
        public ActionResult Index(string format, String filter = null, Int32 page = 0, int id = 0,
                                  string searchQuery = null, int limit = 0)
        {

            if (Request.QueryString["author"] != null)
            {
                string author = Request.QueryString["author"];
                var da = DataHelper.EntityDataModel.DocumentAuthors.FirstOrDefault(daa => daa.AuthorLast == author);

                if (da != null)
                {
                    return RedirectToActionPermanent("Index", "Daily", new { format = "author", id = da.AuthorId });
                }
            }


            var db = DataHelper.MisesDataContext;
            var model = DataHelper.EntityDataModel;

            var vm = new DailyViewModel
                         {
                             CurrentPage = page,
                             PageSize = pagesize,
                             Title =
                                 "Mises Daily - daily articles on Austrian Economics from the Ludwig von Mises Institute"
                         };

            switch (format)
            {
                case "GetArticlesForDay":
                    return ArticlesForDay(Request.QueryString["DatePosted"]);
                case "author":
                    vm.Author = model.DocumentAuthors.SingleOrDefault(a => a.AuthorId == id);
                    vm.Title = vm.Author + " - Mises Daily Archive";
                    vm.Format = ViewFormat.Album;
                    break;
                case "authors": //??
                case "authorlist":
                    vm.Title = "Mises Daily Authors";
                    return AuthorList();
            }


            var viewFormat = (String.Compare(format, "Album", true) == 0) ? ViewFormat.Album : ViewFormat.List;
            if (viewFormat == ViewFormat.Album)
            {
                vm.Total = db.DailyArticlesGetImageGallery(id, searchQuery, null).Count();
                var articles =
                    db.DailyArticlesGetImageGallery(id, searchQuery, null).Skip(pagesize * page).Take(pagesize).ToList();
                articles.ForEach(a => { a.NavigateUrl = string.Format("{0}/{1}", a.NavigateUrl, a.Title.ToSlug()); });
                vm.Articles = articles;
            }
            else
            {
                vm.Total = db.DailyArticlesGetArchive(false, id, searchQuery, false).Count();
                List<DailyArticlesGetArchiveResult> articles = db.DailyArticlesGetArchive(false, id, searchQuery, false).ToList();
                articles.ForEach(
                    a => { a.NavigateUrl = string.Format("/daily/{0}/{1}", a.ArticleId, a.title.ToSlug()); });
                vm.Articles = articles;
            }

            ViewBag.CurrentPage = page;
            ViewBag.PageSize = pagesize;
            ViewBag.Total = vm.Total;
            vm.Format = viewFormat;

            return View(vm);
            //return Index3(filter, 0, String.Empty, page);
        }




        //        [OutputCache(Duration = 360, VaryByParam = "*")]
        public PartialViewResult AlbumView(int page = 0, int authorId = 0, string searchQuery = null, int limit = 0)
        {
            ViewBag.CurrentPage = page;
            ViewBag.PageSize = pagesize;

            var db = DataHelper.MisesDataContext;
            ViewBag.Total = db.DailyArticlesGetImageGallery(authorId, searchQuery, null).Count();

            List<DailyArticlesGetImageGalleryResult> articles =
                db.DailyArticlesGetImageGallery(authorId, searchQuery, null).Skip(pagesize * page).Take(pagesize).ToList();

            if (limit > 0)
            {
                articles = articles.Take(limit).ToList();
                if (ViewBag.Total > limit) ViewBag.Total = limit;
            }
            articles.ForEach(a => { a.NavigateUrl = a.NavigateUrl + "/" + a.Title.ToSlug(); });


            return PartialView(articles);
        }

        [OutputCache(Duration = 360, VaryByParam = "*")]
        public PartialViewResult ListView(int page = 0, int authorId = 0, string searchQuery = null, int limit = 0)
        {
            //ViewBag.CurrentPage = page;
            //ViewBag.PageSize = pagesize;

            var db = DataHelper.MisesDataContext;
            ViewBag.Total = db.DailyArticlesGetArchive(false, authorId, searchQuery, false).Count();

            List<DailyArticlesGetArchiveResult> articles =
                db.DailyArticlesGetArchive(false, authorId, searchQuery, false).ToList();

            articles.ForEach(a => { a.NavigateUrl = "/" + a.ArticleId + "/" + a.title.ToSlug(); });

            if (limit > 0) articles = articles.Take(limit).ToList();

            return PartialView(articles);
        }

        [OutputCache(Duration = 360, VaryByParam = "*")]
        [GridAction]
        public ActionResult GetDailyArchiveList()
        {
            var db = DataHelper.MisesDataContext;
            var articles = db.DailyArticlesGetArchive(false, 0, null, false).ToList();

            articles.ForEach(a => { a.NavigateUrl = "/" + a.ArticleId + "/" + a.title.ToSlug(); });
            return View(new GridModel(articles));
        }

        /// <summary>
        /// Not sure what this is for
        /// </summary>
        /// <param name="AuthorId"></param>
        /// <returns></returns>
        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult AuthorArchive(int AuthorId)
        {
            ViewBag.AuthorId = AuthorId;
            return View("AuthorArchive");
        }

        public ActionResult AuthorList()
        {
            var db = DataHelper.MisesDataContext;
            List<DocumentAuthorsGetListResult> authors = db.DocumentAuthorsGetList("DailyArticles").ToList();
            return View("AuthorList", authors);
        }


        #region Web Services

        public ActionResult ArticlesForDay(string DatePosted)
        {
            var dbContext = DataHelper.MisesDataContext;

            if (null != DatePosted)
            {
                DatePosted = DatePosted.Replace("UTC", "GMT");

                DateTime dtDate;

                if (
                    !DateTime.TryParse(DatePosted, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal,
                                       out dtDate))
                    throw new Exception("DatePosted must be a UTC String (Javascripts Date.toUTCString()");

                if (dtDate > DateTime.Now.AddDays(1))
                {
                    return Json(new List<MisesDaily>(), JsonRequestBehavior.AllowGet);
                }
                //throw new Exception("DatePosted cannot be greater than today's date");

                ISingleResult<DailyArticlesSummaryByDateResult> articles = dbContext.DailyArticlesSummaryByDate(dtDate);

                List<MisesDaily> articlesList = articles.Select(article => new MisesDaily
                {
                    ArticleId = article.ArticleId,
                    Title =
                        DataFormat.StripHTML(
                            article.Title),
                    Description = article.Description,
                    AuthorName = article.Author,
                    CoAuthorName = article.CoAuthor,
                    PhotoURL = article.PhotoURL,
                    DatePosted =
                        article.DatePosted.
                        ToLongDateString(),
                    AuthorId = article.AuthorId,
                    ArticleUrl =
                        string.Format("/daily/{0}",
                                      article.ArticleId +
                                      "/" +
                                      article.Title.
                                          ToSlug())
                }).OrderByDescending(a => a.Description.Length).ToList();

                var result = new JsonResult { Data = articlesList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                return result;
            }
            else
            {
                ISingleResult<DailyArticlesGetTodaysSummaryResult> articles = dbContext.DailyArticlesGetTodaysSummary();

                List<MisesDaily> articlesList = articles.Select(article => new MisesDaily
                {
                    ArticleId = article.ArticleId,
                    Title =
                        DataFormat.StripHTML(
                            article.Title),
                    Description = article.Description,
                    AuthorName = article.Author,
                    CoAuthorName = article.CoAuthor,
                    PhotoURL = article.PhotoURL,
                    DatePosted =
                        article.DatePosted.
                        ToLongDateString(),
                    AuthorId = article.AuthorId
                }).OrderByDescending(a => a.Description.Length).ToList();

                var result = new JsonResult { Data = articlesList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                return result;
            }
        }

        private class MisesDaily
        {
            public int ArticleId { get; set; }

            public string Title { get; set; }

            public string Description { get; set; }

            public string AuthorName { get; set; }

            public string CoAuthorName { get; set; }

            public string PhotoURL { get; set; }

            public string DatePosted { get; set; }

            public int AuthorId { get; set; }

            public string ArticleUrl { get; set; }
        }

        #endregion
    }
}