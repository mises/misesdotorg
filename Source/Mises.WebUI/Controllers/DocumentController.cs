﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BookCatalog.Models;
using Mises.Data;
using Mises.Domain;
using Mises.Domain.Books;
using Mises.Domain.Utility;

namespace Mises.Web.Controllers
{
    public class DocumentController : Controller
    {

        #region Detail

        public ActionResult View(int id)
        {
            var db = DataHelper.EntityDataModel;
            var doc = db.Documents.Include("DocumentFiles").Include("DocumentAuthor").Include("DocumentAuthor2").Include("DocumentFiles.DocumentMediaType").SingleOrDefault(d => d.DocumentId == id);

            if (doc == null)
            {
                return Redirect(db.DocumentFiles.Any(f => f.FileId == id) ? string.Format("/media/{0}/{1}", id, Request.QueryString["slug"]) : "/Error/Http404");
            }

            // check for media
            try
            {
                if (doc.DocumentFiles.Any() && doc.DocumentFiles.All(d => d.DocumentMediaType.IsMedia))
                {
                    return Redirect("/media/" + doc.DocumentFiles.FirstOrDefault().FileId + "/" + doc.Title.ToSlug());
                }

                // check for page conversion

                if ((doc.ProductId == null || doc.ProductId == 0) && doc.DocumentFiles.Count == 1 && doc.DocumentFiles.Single().MediaTypeId == 5)
                {
                    return Redirect(doc.DocumentFiles.Single().URL);
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(Request.RequestContext.HttpContext, ex);
            }


            //var storeInfo = db.Documents.First(d => d.DocumentId == id);

            var vm = new LiteratureViewModel()
            {
                Description = doc.Description,
                PublicationInformation = doc.PublicationInformation,
                DocumentId = doc.DocumentId,
                Title = doc.Title,
                EditDate = doc.EditDate,
                Thumbnail = "//mises.freecapitalists.org/" + doc.CoverImageURL,
                ISBN = doc.ISBN,
                GUID = doc.GUID,
                ProductId = doc.ProductId,
                Keywords = doc.Keywords,
                Formats = new List<DocumentFile>(doc.DocumentFiles)
                //Formats = new List<FileModel>() { new FileModel() { MediaTypeId = (int)doc.MediaTypeId, URL = doc.URL, MediaImage = NavigationModels.GetMediaTypeFromId((int)doc.MediaTypeId).MediaIconPath, FileType = doc.MIMEtype } }
            };

            if (vm.Description == null) vm.Description = "(No description available.)";
            //vm.Description = doc.PrivateComment;

            if (doc.ProductId > 0)
            {
                var book = StoreRepository.GetBookByProductId((int)doc.ProductId);

                if (book != null)
                {
                    vm.Formats.Add(new DocumentFile() { MediaTypeId = 7, URL = string.Format("http://store.mises.org/Product.aspx?ProductId={0}&utm_source=Resources", doc.ProductId), CreateDate = doc.EditDate, Display = true});
                    vm.Price = book.Price.ToString("C");
                }



            }

            if (doc.DocumentAuthor != null)
            {
                vm.AuthorId = doc.DocumentAuthor.AuthorId;
                vm.Author = doc.DocumentAuthor != null
                                ? doc.DocumentAuthor.AuthorFirst + " " + doc.DocumentAuthor.AuthorMiddle + " " +
                                  doc.DocumentAuthor.AuthorLast
                                : "";
            }

            if (doc.DocumentAuthor2 != null)
            {
                vm.CoAuthorId = doc.DocumentAuthor2.AuthorId;
                vm.CoAuthor = doc.DocumentAuthor2.AuthorFirst + " " + doc.DocumentAuthor2.AuthorMiddle + " " + doc.DocumentAuthor2.AuthorLast;
            }

            //doc.DocumentFiles.ToList().ForEach(f => vm.Formats.Add(new FileModel()
            //{
            //    URL = f.URL,
            //    FileType = NavigationModels.GetMediaTypeFromId(f.MediaTypeId).MediaType,
            //    MediaImage = NavigationModels.GetMediaTypeFromId(f.MediaTypeId).MediaIconPath,
            //    FileSize = f.fileSize,
            //    Duration = f.duration
            //}));

            if (!AdminHelper.IsAdmin(Request.Cookies))
            {
                vm.Formats = vm.Formats.Where(f => f.Display).ToList();
            }

            return View(vm);

            //return GetDocumentList(documentId);
        }

        #endregion


    }
}
