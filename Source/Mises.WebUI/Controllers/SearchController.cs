﻿using System.Web.Mvc;

namespace Mises.Web.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/

        public ActionResult Index(string term)
        {
            return Redirect("http://mises.freecapitalists.org//search.aspx?q=" + term);
            //return View();
        }

        
    }
}
