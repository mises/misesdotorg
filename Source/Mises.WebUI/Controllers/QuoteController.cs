﻿#region

using System.Web.Mvc;
using Mises.Domain.Services;

#endregion

namespace Mises.Web.Controllers
{
    public class QuoteController : Controller
    {
        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Random(string format = null)
        {
            return Content(quotes.GetRandomQuote(format));
        }
    }
}