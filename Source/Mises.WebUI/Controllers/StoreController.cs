﻿#region

using System.Web.Mvc;
using Mises.Data;
using Mises.Domain;
using Mises.Domain.Books;

#endregion

namespace Mises.Web.Controllers
{
    public class MisesShopController : Controller
    {
        //
        // GET: /Store/

        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult RandomBook()
        {
            var book = StoreRepository.GetBookByProductId();
            return new JsonResult {Data = book, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }
    }
}