﻿#region

using System;
using System.Web.Mvc;
using Mises.Domain.Tags;

#endregion

namespace Mises.Web.Controllers
{
    public class TagController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("/clouds.aspx");
        }

        public ActionResult Add(string Id, string Tags)
        {
            //try
            //{
            Tagging.TagDocument(Tags, Guid.Parse(Id));

            var result = new JsonResult {Data = "Tag added:" + Tags, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
            return result;
            //}
            //catch (Exception ex)
            //{
            //    return new ServiceResponse { Error = new ServiceError(ex.Message, ex.ToString()) };
            //}
        }

        public ActionResult Get(string Id)
        {
            var tags = Tagging.GetDocumentTags(Guid.Parse(Id));

            return new JsonResult {Data = tags, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }
    }
}