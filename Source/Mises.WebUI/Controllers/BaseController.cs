﻿#region

using System.Collections.Generic;
using System.Web.Mvc;
using Mises.Domain.Contracts;
using Telerik.Web.Mvc;

#endregion

namespace Mises.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        //should be DI only and this private set
        //protected ILogger Logger { get; private set; }
        //protected PageViewModelBase PageModel;

        //public IApplicationSettings ApplicationManagement { get; private set; }

        public BaseController()
        {
        }

        public BaseController(IServiceLayer services /*, ILogger logger*/)
        {
            Services = services;


            //this.Logger = logger;
            //this.PageModel = new DefaultViewModel(this.Services);

            // grab the application settings to ensure we kick off any required actions
            //this.ApplicationManagement = this.PageModel.ApplicationManagement;
        }

        protected IServiceLayer Services { get; set; }

        // helper method to create strongly typed GridModel
        protected GridModel<T> CreateGridModel<T>(IEnumerable<T> data)
        {
            return new GridModel<T>(data);
        }
    }
}