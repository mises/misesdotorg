﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;
using AutoMapper;
using BookCatalog.Models;
using Microsoft.JScript;
using Mises.Data;
using Mises.Domain;
using Mises.Domain.Contracts;
using Mises.Services;
using Mises.Web.ViewModels;
using Mises.Web.ViewModels.Media;
using Mises.WebControls.Helpers;
using DocumentDTO = Mises.Domain.Contracts.DocumentDTO;


#endregion

using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core.Objects;

namespace Mises.Web.Controllers
{
    public class MediaController : BaseController //TODO rename to articles (covers all documents/articles etc)
    {
        public MediaController(IServiceLayer services) : base(services)
		{
		} 
        #region Paul's Media Page Action Handlers

       

        [HttpGet]
        //[OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Index()
        {
            var routeValues = new RouteValueDictionary(new { filter = "video", controller = "Media", action = "Index2" });
            return Index3("video", 0, String.Empty, 1, routeValues);
        }

        [HttpGet]
       // [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Index2(String filter, Int32 page)
        {
            return Index3(filter, 0, String.Empty, page);
        }

        [HttpGet]
       // [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Index3(String filter, Int32 id, String slug, Int32 page,
                                   RouteValueDictionary routeValues = null)
        {
            if (filter.Equals("poster",StringComparison.InvariantCultureIgnoreCase))
            {
                return poster(page);
            }

            // Get to-level categories for filter:

            var topCategories = Services.MediaService.GetCategories(new TopLevelCategoryFilter()).Items;
            //Mapper.Map(topCategories, vm.CategoryBreadcrumb);

            ViewBag.TopLevelCategories = new BrowseViewModel<CategorySummaryDTO>
            {
                PageIndex = 0,
                PageSize = 100,
                ItemViewName = "CategorySummary",
                Title = "Media Categories",
                TitleAppend = id > 0 ? slug.Replace('-', ' ') : String.Empty,
                Items = topCategories
            };


            ViewBag.Id = id;

            var vm = new IndexViewModel
                         {
                             Documents = new BrowseViewModel
                                             {
                                                 PageIndex = page - 1,
                                                 //TODO validate page
                                                 PageSize = ApplicationDefaults.PageSize,
                                                 GroupSize = 1,
                                                 ItemViewName = "ItemSummary",
                                                 Preference = MediaPreferenceTypes.Video,
                                                 RouteValues = routeValues
                                             },
                             Authors = Services.MediaService.GetAuthorSummaries(),
                             Categories = Services.MediaService.GetCategories(new TopLevelCategoryFilter()).Items,
                             Subjects = Services.MediaService.GetSubjects().Items
                         };


            if (filter == "search")
            {
                var term = Request.QueryString["q"];

				vm.CategoryId = ParseInt(Request.QueryString["cid"], -1);
				vm.SubjectId = ParseInt(Request.QueryString["sid"], -1);
				vm.AuthorId = ParseInt(Request.QueryString["aid"], -1);

                if (String.IsNullOrWhiteSpace(term) && !vm.AdvancedOptionSelected)
                {
                    vm.Documents.Items = new List<MisesDTO>();
                }
                else
                {
					term = System.Web.HttpUtility.UrlDecode(term);
                    try
                    {
						var types = new int[] { 1, 2, 8, 14 };

                        vm.Documents.QueryString = Request.QueryString.ToString();
                        var dto = Services.MediaService.GetDocuments(term, vm.CategoryId, vm.SubjectId, vm.AuthorId, types, vm.Documents.PageIndex, vm.Documents.PageSize);
                        Mapper.Map(dto, vm.Documents);
                    }
                    catch //TODO (Exception ex)
                    {
                        //TODO log it
                        vm.Errored = true;
                    }
                }
                vm.FilterPrepend = "Search results for";
                vm.FilterText = GlobalObject.unescape(GlobalObject.decodeURIComponent(term));
            }
            else
            {
                //TODO date
                IDocumentFilter searchFilter = null;


                switch (filter.ToLower())
                {
                    case "video":
                        searchFilter = new DocumentsWithVideoFilter();
                        vm.FilterPrepend = "Media containing";
                        //vm.FilterText = "video";
                       // vm.FilterText = "Mises Video";
                        break;
                    case "poster":
                        return poster(page);
                    case "sitemap":
                        return Sitemap();
                    case "gallery":
                        return Gallery(page);
                    case "audio":
                        searchFilter = new DocumentsWithAudioFilter();
                        vm.Documents.Preference = MediaPreferenceTypes.Audio;
                        vm.FilterPrepend = "Audio containing";
                        vm.FilterText = "Mises Audio";
                        break;
                    case "subjects":
                        searchFilter = new DocumentsBySubjectFilter(id);
                        vm.FilterPrepend = "Media for Subject";
                        vm.FilterText = slug.Replace('-', ' ');
                        break;
                    case "authors":
                    case "author":

                        if (id == 0)
                            return Authors(id, page);

                        vm.FeedAppend = String.Concat("authorid=", id);
                        searchFilter = new DocumentsByAuthorFilter(id);
                        vm.FilterPrepend = "Media for author";
                        vm.FilterText = slug.Replace('-', ' ');
                        break;
                    case "categories":
                        {
                            vm.FeedAppend = String.Concat("categoryid=", id);

                            vm.CategoryBreadcrumb = new BrowseViewModel<CategorySummaryDTO>
                                                        {
                                                            PageIndex = 0,
                                                            PageSize = 100,
                                                            ItemViewName = "CategorySummary"
                                                        };
                            var ancestorsFilter = new CategoryAncestorsFilter(id);
                            var ancestorsDto = Services.MediaService.GetCategories(ancestorsFilter,
                                                                                   vm.CategoryBreadcrumb.PageIndex,
                                                                                   vm.CategoryBreadcrumb.PageSize);
                            Mapper.Map(ancestorsDto, vm.CategoryBreadcrumb);

                            vm.CategoryChildren = new BrowseViewModel<CategorySummaryDTO>
                                                      {
                                                          PageIndex = 0,
                                                          PageSize = 100,
                                                          ItemViewName = "CategorySummary",
                                                          Title = id == 0 ? "Media Categories" : "Sub-category",
                                                          TitleAppend = id > 0 ? slug.Replace('-', ' ') : String.Empty
                                                      };
                            var childrenFilter = new CategoryChildrenFilter(id);
                            var childrenDto = Services.MediaService.GetCategories(childrenFilter, vm.CategoryChildren.PageIndex,
                                                                                  vm.CategoryChildren.PageSize);
                            Mapper.Map(childrenDto, vm.CategoryChildren);

                            if (id == 0)
                            {
                                vm.FilterPrepend = "Media";
                            }
                            else
                            {
                                vm.FilterPrepend = "Media for category";
                                vm.FilterText = slug.Replace('-', ' ');
                                vm.Title = vm.FilterText;
                                ViewBag.Title = slug;
                            }

                            if (id == 210) // == Recent Uploads
                                searchFilter = new DocumentsWithMediaFilter(50);
                            else
                                searchFilter = new DocumentsByCategoryFilter(id);
                        }
                        break;
                }

                var dto = Services.MediaService.GetDocuments(searchFilter, vm.Documents.PageIndex, vm.Documents.PageSize);
                Mapper.Map(dto, vm.Documents);
            }

            vm.Documents.ListSize = ApplicationDefaults.ListSize;
            vm.Documents.RouteSegmentName = "page";

            if (Request.IsAjaxRequest())
            {
                return PartialView("DocumentSummaryList", vm.Documents);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(ViewBag.Title))
                {
                    ViewBag.Title = "Audio/Video Catalog: free lectures, audio books and courses from the Ludwig von Mises Institute";
                    //vm.Title = String.Format("Media on {0}", Request.Url.Host);
                    vm.Title = "Audio/Video";
                }

                return View("Index", vm);
            }
        }

        [HttpGet]
        public ActionResult Browse(String filter, Int32 id, Int32 page)
        {
            var vm = new BrowseViewModel
                         {
                             PageIndex = page - 1,
                             //TODO validate page
                             PageSize = ApplicationDefaults.SummaryPageSize,
                             GroupSize = 1,
                             ItemViewName = "DocumentSummaryLink"
                         };

            IDocumentFilter searchFilter;

            if (String.Compare(filter, "video", true) == 0)
            {
                searchFilter = new DocumentsWithVideoFilter();
                vm.Title = "Video";
                vm.Preference = MediaPreferenceTypes.Video;
            }
            else if (String.Compare(filter, "audio", true) == 0)
            {
                searchFilter = new DocumentsWithAudioFilter();
                vm.Title = "Audio";
                vm.Preference = MediaPreferenceTypes.Audio;
            }
            else if (String.Compare(filter, "literature", true) == 0)
            {
                searchFilter = new DocumentsWithLiteratureFilter();
                vm.Title = "Literature";
                vm.Preference = MediaPreferenceTypes.Literature;
            }
            else
                throw new Exception("Browse - invalid route data"); //TODO

            if (Request.IsAjaxRequest())
            {
                if (String.IsNullOrWhiteSpace(Request.Params["ajaxsrc"]) || (Request.Params["ajaxsrc"] == "featured"))
                {
                    var dto = Services.MediaService.GetDocumentFromMediaId(id);
                    return PartialView("Media/MediaWidget", new DetailViewModel(vm.Preference, dto));
                }
                else
                {
                    var dto = Services.MediaService.GetDocuments(searchFilter, vm.PageIndex, vm.PageSize);
                    //TODO too slow. AND filters in Index.
                    Mapper.Map(dto, vm);
                    vm.ListSize = ApplicationDefaults.ListSize;
                    vm.RouteSegmentName = "page";
                    return PartialView("DocumentSummaryList", vm);
                }
            }
            else
            {
                var dto = Services.MediaService.GetDocuments(searchFilter, vm.PageIndex, vm.PageSize);
                Mapper.Map(dto, vm);
                vm.ListSize = ApplicationDefaults.ListSize;
                vm.RouteSegmentName = "page";

                setFeatured(vm, id, page, searchFilter, dto);

                ViewBag.Title = vm.Title;
                vm.Title = String.Format("{0} on {1}", vm.Title, Request.Url.Host);
                return View(vm);
            }
        }

        [HttpGet]
        public ActionResult BrowsePartial(BrowseViewModel vm)
        {
            return PartialView(vm);
        }

        private void setFeatured(BrowseViewModel vm, Int32 id, Int32 page, IDocumentFilter searchFilter,
                                 PaginableResult<DocumentDTO> dto)
        {
            // Try the list before another round trip (should bundle into 1 but for now)...
            if (id > 0)
            {
                vm.Featured = dto.Items.FirstOrDefault(i => i.Id == id); //...id so try the list
            }
            else if (page == 1)
            {
                vm.Featured = dto.Items.FirstOrDefault(); //...first page and no id so pick from the top
            }

            if (vm.Featured == null)
            {
                if (id > 0)
                {
                    //...failed to get from list so go to the db for this id
                    vm.Featured = Services.MediaService.GetDocumentFromMediaId(id);
                }
                else
                {
                    //...get the latest
                    var latest = Services.MediaService.GetDocuments(searchFilter, 0, 1);
                    if (latest != null && latest.Items != null && latest.Items.Count > 0)
                        vm.Featured = latest.Items.First();
                }
            }

            if (vm.Featured != null)
            {
                Request.RequestContext.RouteData.Values["id"] = vm.Featured.Id;
            }
        }

        public ActionResult Detail(Int32 id, Int32 authorPage)
        {
            var vm = new DetailViewModel();

            var doc = !String.IsNullOrEmpty(Request.QueryString["d"])
                          ? Services.MediaService.GetDocument(id)
                          : Services.MediaService.GetDocumentFromMediaId(id);

            if (doc == null)
            {
                                return Redirect("/resources/" + id);
            }
                //throw new Exception("Document not found! " + id);

            Mapper.Map(doc, vm);

            if (doc.Authors == null || doc.Authors.Count == 0 || doc.Authors[0] == null)
            {
              doc.Authors = new List<AuthorSummaryDTO>();
            }

            var searchFilter = new DocumentAuthorIdListFilter(doc.Authors.Select(a => a.Id));
            //TODO doesn't filter out the above document
            vm.AuthorDocuments = Services.MediaService.GetDocuments(searchFilter, authorPage - 1,
                                                                    ApplicationDefaults.SummaryPageSize);
            vm.AuthorDocuments.ListSize = ApplicationDefaults.ListSize;
            vm.AuthorDocuments.RouteSegmentName = "authorPage";

            if (Request.IsAjaxRequest())
            {
                var ajaxsrc = Request.Params["ajaxsrc"];
                switch (ajaxsrc)
                {
                    case "documents":
                        return PartialView("DocumentSummaryList", new BrowseViewModel(vm.AuthorDocuments));
                    case "video":
                        return PartialView("Media/MediaWidget", new DetailViewModel(MediaPreferenceTypes.Video, doc));
                    case "audio":
                        return PartialView("Media/MediaWidget", new DetailViewModel(MediaPreferenceTypes.Audio, doc));
                    default:
                        return View(vm);
                }
            }
            else
            {
                return View(vm);
            }
        }

        [HttpGet]
        public JsonResult SearchCategoriesJSON(String startsWith)
        {
            var filter = new CategoryNameFilter(startsWith);

            var dto = Services.MediaService.GetCategories(filter, 0, Int32.MaxValue);

            var result = new JsonResult
                             {
                                 JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                 Data = dto.Items.Select(i => new
                                                                  {
                                                                      label = i.Name,
                                                                      value =
                                                                  String.Format("{0}/{1}", i.Id, i.Name.ToSlug())
                                                                  })
                             };


            return result;
        }

        [HttpGet]
        public JsonResult SearchSubjectsJSON(String startsWith)
        {

            var filter = new SubjectNameFilter(startsWith);

            var dto = Services.MediaService.GetSubjects(filter, 0, Int32.MaxValue);

            var result = new JsonResult
                             {
                                 JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                 Data = dto.Items.Select(i => new
                                                                  {
                                                                      label = i.Name,
                                                                      value =
                                                                  String.Format("{0}/{1}", i.Id, i.Name.ToSlug())
                                                                  })
                             };

            return result;
        }

        #endregion Paul's Media Action Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id">The document id.</param>
        /// <returns></returns>
        [HttpGet]
       // [OutputCache(CacheProfile = "CacheOneDay")]
        public ActionResult poster(int Id)
        {
            if (Id == 0) return Redirect(Request.Url.Scheme + "://mises.freecapitalists.org/images/Theme/images/image-missing.png");

            Response.Cache.SetLastModified(DateTime.Now.AddMonths(-1));
            Response.Cache.SetMaxAge(TimeSpan.FromDays(30));
            var model = DataHelper.EntityDataModel;
            model.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            var image = model.Documents.Where(d => d.DocumentId == Id).Select(document => new { document.CoverImage, document.CoverImageURL }).SingleOrDefault();
            const int maxDimension = 240;

            if (image.CoverImage == null)
            {
                if (image.CoverImageURL != null && !image.CoverImageURL.StartsWith("media/poster/"))
                    return Redirect(image.CoverImageURL);

                // redirect to default image
                return Redirect(Request.Url.Scheme + "://mises.freecapitalists.org/images/Theme/images/image-missing.png");
            }

            return new ImageResult(new MemoryStream(image.CoverImage), "image/jpeg", null, maxDimension);
        }

        public ActionResult Sitemap()
        {
            GenerateSitemap();

            return new FilePathResult(AppDomain.CurrentDomain.BaseDirectory + @"sitemap.xml", @"text/xml");

        }

        private const int pagesize = 100;


        /// <summary>
        /// Galleries the specified page.
        /// http://mises.freecapitalists.org//media/gallery
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns></returns>
        public ActionResult Gallery(Int32 page = 0)
        {
            var model = DataHelper.EntityDataModel;
            model.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            ViewBag.PageSize = pagesize;
            ViewBag.CurrentPage = page;
            page = page - 1;

            ViewBag.Total = model.Documents.Count(d => d.DocumentFiles.Any(m => m.DocumentMediaType.MediaTypeID == 2 || m.DocumentMediaType.MediaTypeID == 12 || m.DocumentMediaType.MediaTypeID == 14 || m.DocumentMediaType.MediaTypeID == 16));

            var docs = model.Documents.Include("DocumentFiles").Include("DocumentAuthor").Include("DocumentAuthor2").Where(
                d => d.DocumentFiles.Any(m => m.DocumentMediaType.MediaTypeID == 2 || m.DocumentMediaType.MediaTypeID == 12 || m.DocumentMediaType.MediaTypeID == 14 || m.DocumentMediaType.MediaTypeID == 16)).OrderByDescending(d => d.DocumentId).Skip(page * pagesize).Take(pagesize);

            //docs.ToList().ForEach(a => { a.NavigateUrl = string.Format("{0}/{1}", a.NavigateUrl, a.Title.ToSlug()); });

            return View("gallery", docs.ToList());

        }

        public ActionResult Authors(int id = 0, int page = 0)
        {
            var mises = DataHelper.EntityDataModel;

            List<AuthorViewModel> authorList = (from author in mises.DocumentAuthors where author.Documents.Any(d=> d.DocumentFiles.Any(f=> f.DocumentMediaType.IsMedia))
                                                select new AuthorViewModel
                                                {
                                                    AuthorFirst = author.AuthorFirst,
                                                    AuthorMiddle = author.AuthorMiddle,
                                                    AuthorLast = author.AuthorLast,
                                                    AuthorId = author.AuthorId,
                                                    BioText = author.BioText,
                                                    Born = author.Born,
                                                    Died = author.Died,
                                                    Books = author.Documents.Count,
                                                    Photo = author.Photo

                                                }).OrderBy(a => a.AuthorLast).ToList();

            return View("Authors", authorList);
            //return View(mises.DocumentAuthor.ToList());
        }


        #region Private Methods

        //TODO move into manager and add/refine output
        private void GenerateSitemap()
        {
            const String rootdomain = "http://mises.freecapitalists.org/";
            String xmlFile = HttpContext.Server.MapPath(@"~/sitemap.xml");

            using (XmlTextWriter writer = new XmlTextWriter(xmlFile, Encoding.UTF8))
            {
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();

                writer.WriteStartElement("urlset");
                writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xsi:schemaLocation",
                                            "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
                writer.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

                // Root page
                writer.WriteStartElement("url");
                writer.WriteElementString("loc", rootdomain);
                writer.WriteElementString("changefreq", "always");
                writer.WriteElementString("priority", "1.0");
                writer.WriteEndElement();

                const int pageSize = 1000;

                // Media...
                var documentFilter = new DocumentsWithMediaFilter();
                var media = Services.MediaService.GetDocuments(documentFilter, 0, pageSize);
                for (var i = 0; i < media.TotalPages; i++)
                {
                    foreach (var item in media.Items)
                    {
                        writer.WriteStartElement("url");
                        writer.WriteElementString("loc",
                                                  String.Format("{0}/media/{1}/{2}", rootdomain, item.Id,
                                                                item.Title.ToSlug()));
                        writer.WriteElementString("lastmod", item.CreatedDate.ToString("yyyy-MM-dd"));
                        //TODO better as last date media was added?
                        writer.WriteElementString("changefreq", "monthly");
                        writer.WriteElementString("priority", "0.8");
                        writer.WriteEndElement();
                    }

                    if (i + 1 < media.TotalPages)
                        media = Services.MediaService.GetDocuments(documentFilter, (i + 1), pageSize);
                }

                var authors = Services.MediaService.GetAuthors(null, 0, pageSize);
                for (var i = 0; i < authors.TotalPages; i++)
                {
                    foreach (var item in authors.Items)
                    {
                        writer.WriteStartElement("url");
                        writer.WriteElementString("loc",
                                                  String.Format("{0}/authors/{1}/{2}", rootdomain, item.Id,
                                                                item.FullName.ToSlug()));
                        writer.WriteElementString("lastmod", item.CreatedDate.ToString("yyyy-MM-dd"));
                        //TODO better as last post/activity date?
                        writer.WriteElementString("changefreq", "daily");
                        writer.WriteElementString("priority", "0.8");
                        writer.WriteEndElement();
                    }

                    if (i + 1 < authors.TotalPages)
                        authors = Services.MediaService.GetAuthors(null, (i + 1), pageSize);
                }

                writer.WriteEndElement(); //...close urlset
                writer.WriteEndDocument(); //...close document
                writer.Flush();

                writer.Close();
            }
        }

		/// <summary>
		///		Try parse and return default value if unsuccessful
		/// </summary>
		private int ParseInt(string value, int defaultValue)
		{
			int retval;
			if (!int.TryParse(value, out retval))
			{
				retval = defaultValue;
			}
			return retval;
		}

        #endregion
    }
}