﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AutoMapper;

using Mises.Domain;
using Mises.Domain.Contracts;
using Mises.Services;
using Mises.Web.ViewModels.Categories;

namespace Mises.Web.Controllers
{
	public class CategoriesController : BaseController
	{
		public CategoriesController() : base(null)
		{
			this.Services = new ServiceLayer(new InProcessMediaService(new MediaRepository(), new AuthorRepository()));
		}

		[HttpGet]
		public ActionResult Index(Int32 childrenPage, Int32 documentsPage)
		{
			var vm = new IndexViewModel()
			{
				Title = "Media Categories",
				Children = new ViewModels.BrowseViewModel<CategorySummaryDTO>()
				{
					PageIndex = childrenPage - 1, //TODO validate page
					PageSize = ApplicationDefaults.SummaryPageSize,
					ItemViewName = "CategorySummary"
				},
				Documents = new ViewModels.Media.BrowseViewModel()
				{
					PageIndex = documentsPage - 1, //TODO validate page
					PageSize = ApplicationDefaults.SummaryPageSize,
					GroupSize = 2
				}
			};
			
			var id = 0;

			var childrenFilter = new CategoryChildrenFilter(id);
			var childrenDto = this.Services.MediaService.GetCategories(childrenFilter, vm.Children.PageIndex, vm.Children.PageSize);
			Mapper.Map<PaginableResult<CategorySummaryDTO>, ViewModels.BrowseViewModel<CategorySummaryDTO>>(childrenDto, vm.Children);
			vm.Children.Title = "Top level categories";
			vm.Children.ListSize = ApplicationDefaults.ListSize;
			vm.Children.RouteSegmentName = "childrenPage";

			var searchFilter = new DocumentsByCategoryFilter(id);
			var dto = this.Services.MediaService.GetDocuments(searchFilter, vm.Documents.PageIndex, vm.Documents.PageSize);
			Mapper.Map<PaginableResult<DocumentDTO>, ViewModels.Media.BrowseViewModel>(dto, vm.Documents);
			vm.Documents.ListSize = ApplicationDefaults.ListSize;
			vm.Documents.RouteSegmentName = "documentsPage";

			if (Request.IsAjaxRequest())
			{
				if (String.IsNullOrWhiteSpace(Request.Params["ajaxsrc"]) || (Request.Params["ajaxsrc"] == "documents"))
					return PartialView("DocumentSummaryList", vm.Documents);
				else
					return PartialView("ItemSummaryListItems", vm.Children);
			}
			else
			{
				return this.RazorView(vm);
			}
		}

		[HttpGet]
		public ActionResult Detail(Int32 id, Int32 childrenPage, Int32 documentsPage)
		{
			var vm = new DetailViewModel()
			{
				Title = "Categories",
				Breadcrumb = new ViewModels.BrowseViewModel<CategorySummaryDTO>()
				{
					PageIndex = 0,
					PageSize = 100,
					ItemViewName = "CategorySummary"
				},
				Children = new ViewModels.BrowseViewModel<CategorySummaryDTO>() {
					PageIndex = childrenPage - 1, //TODO validate page
					PageSize = ApplicationDefaults.SummaryPageSize,
					ItemViewName = "CategorySummary"
				},
				Documents = new ViewModels.Media.BrowseViewModel()
				{
					PageIndex = documentsPage - 1, //TODO validate page
					PageSize = ApplicationDefaults.SummaryPageSize,
					GroupSize = 2
				}
			};

			var ancestorsFilter = new CategoryAncestorsFilter(id);
			var ancestorsDto = this.Services.MediaService.GetCategories(ancestorsFilter, vm.Breadcrumb.PageIndex, vm.Breadcrumb.PageSize);
			Mapper.Map<PaginableResult<CategorySummaryDTO>, ViewModels.BrowseViewModel<CategorySummaryDTO>>(ancestorsDto, vm.Breadcrumb);
			vm.Children.Title = "Breadcrumb";
			vm.Breadcrumb.ListSize = ApplicationDefaults.ListSize;

			var childrenFilter = new CategoryChildrenFilter(id);
			var childrenDto = this.Services.MediaService.GetCategories(childrenFilter, vm.Children.PageIndex, vm.Children.PageSize);
			Mapper.Map<PaginableResult<CategorySummaryDTO>, ViewModels.BrowseViewModel<CategorySummaryDTO>>(childrenDto, vm.Children);
			vm.Children.Title = "Sub-categories";
			vm.Children.ListSize = ApplicationDefaults.ListSize;
			vm.Children.RouteSegmentName = "childrenPage";

			var searchFilter = new DocumentsByCategoryFilter(id);
			var dto = this.Services.MediaService.GetDocuments(searchFilter, vm.Documents.PageIndex, vm.Documents.PageSize);
			Mapper.Map<PaginableResult<DocumentDTO>, ViewModels.Media.BrowseViewModel>(dto, vm.Documents);
			vm.Documents.ListSize = ApplicationDefaults.ListSize;
			vm.Documents.RouteSegmentName = "documentsPage";

			if (Request.IsAjaxRequest())
			{
				if (String.IsNullOrWhiteSpace(Request.Params["ajaxsrc"]) || (Request.Params["ajaxsrc"] == "documents"))
					return PartialView("DocumentSummaryList", vm.Documents);
				else
					return PartialView("ItemSummaryListItems", vm.Children);
			}
			else
			{
				vm.Title = String.Format("Category '{0}'", vm.Breadcrumb.Items.First().Name);
				return this.RazorView(vm);
			}
		}

		[HttpGet]
		public ActionResult ItemSummaryList(Mises.Web.ViewModels.BrowseViewModel<CategorySummaryDTO> vm)
		{
			return PartialView(vm);
		}
	}
}
