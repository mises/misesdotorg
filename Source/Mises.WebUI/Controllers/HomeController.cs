﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using AutoMapper;
using BookCatalog.Models;
using LinqKit;
using Mises.Data;
using Mises.Domain;
using Mises.Domain.Contracts;
using Mises.Domain.Utility;
using Mises.Services;
using Mises.Web.ViewModels;
using DocumentDTO = Mises.Domain.Contracts.DocumentDTO;

#endregion

namespace Mises.Web.Controllers
{
    // view (vm) controller (dto) service (dto) assembler (do) domain (do) repository
    // assembler for dto <-> do is automapper - also used to avoid most of the hand cranking for vm <-> dto
    // controllers only know interfaces to services (which are DI'd), services only knows about the domain (and only passes around dtos), domain only knows repository, repository (currently isn't one) only knows persistence implementation (EF in this case). 
    /// <summary>
    ///   Summary description for HomeController
    /// </summary>
    public class HomeController : BaseController
    {
        public HomeController(IServiceLayer services)
            : base(services)
        {
        }

        //[HttpGet]
        //[OutputCache(Duration = 360, VaryByParam = "*")]
        //public ActionResult Index2()
        //{
        //    Response.Cache.SetCacheability(HttpCacheability.Public);

        //    return new FilePathResult(AppDomain.CurrentDomain.BaseDirectory +@"default.html", @"text/html");
        //}

        [HttpGet]
        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Index()
        {
            ViewBag.ShowSidebar = false;

            var db = DataHelper.MisesDataContext;
            
            var articles = db.DailyArticlesGetTodaysSummary().ToList();

            var vm = new HomeViewModel
                         {
                             FeaturedArticle = articles.Where(r => r.Headline),
                             TodaysArticles = articles.Where(r => r.Headline == false).OrderByDescending(d=> d.DatePosted),
                             StoreItems = db.StoreGetBestsellers(5).Where(d=> d.DESCRIPTION != null).ToList().Take(3).ToList()
                         };

            //GetBlogPosts(vm);

            //GetBastiatBlogPosts(vm);

            var ads = from widget in Mises.Data.DataHelper.EntityDataModel.AcademyCourses select widget;
            var rand = new Random();

            var ads2 = ads.ToList().OrderBy(a => rand.Next()).Take(4).ToList();

            vm.Courses = ads2;

            vm.Events = db.CalendarGetUpcomingEvents(null).ToList();

            vm.Audio = db.MediaGetLatestAudio().ToList();

            var model = DataHelper.EntityDataModel;
            DataHelper.SetTransactionIsolationLevelReadUncommited(model);

            var bookQuery = model.Documents.Include("DocumentFiles").Include("DocumentAuthor").Where(d => d.Display && d.DocumentFiles.Any(b => b.MediaTypeId == 3 || b.MediaTypeId == 9)).OrderByDescending(d => d.EditDate).Take(5);

            var books = new List<LiteratureViewModel>();

            bookQuery.ForEach(doc => books.Add(new LiteratureViewModel()
            {
                Author = doc.DocumentAuthor != null ? doc.DocumentAuthor.AuthorFirst + " " + doc.DocumentAuthor.AuthorMiddle + " " + doc.DocumentAuthor.AuthorLast : "",
                AuthorId = doc.DocumentAuthor.AuthorId,
                Description = doc.Description != null ? DataFormat.StripHTML(doc.Description).PadRight(120).Substring(0, 120).Trim() : "",
                DocumentId = doc.DocumentId,
                Thumbnail = doc.CoverImageURL,
                Title = doc.Title,
                EditDate = doc.EditDate,
                Formats = doc.DocumentFiles.ToList()
            }));

            bookQuery.ForEach(q => q.DocumentFiles.ForEach(file =>
            {
                file.URL = "http://library.mises.org" + file.URL;
            }));

            vm.Books = books;


            return View("Index", vm);
        }

        private void GetBastiatBlogPosts(HomeViewModel vm)
        {
            try
            {
                DataSet ds;

                const string blogCacheKey = "MisesBastiatBlogCache";
                if (HttpContext.Cache.Get(blogCacheKey) == null)
                {
                    var blog = new MySqlHelper(ConfigurationManager.ConnectionStrings["BastiatBlog"].ConnectionString);

                    const string sqlGetBlogPosts =
                        @"
SELECT 
bas_posts.ID,
post_title AS title,
LEFT(post_content,800) AS description,
bas_posts.guid AS link,
post_date AS pubDate,
display_name AS author,
user_url 
FROM bas_posts 
JOIN bas_users ON bas_users.id = bas_posts.post_author
WHERE post_type = 'post' 
AND post_status='publish'
AND display_name  != 'Mises Daily'
ORDER BY post_date DESC
LIMIT 4";
                    ds = blog.ExecuteQuery(sqlGetBlogPosts);
                    HttpContext.Cache.Add(blogCacheKey, ds, null, DateTime.Now.AddMinutes(20), TimeSpan.Zero,
                                          CacheItemPriority.High, null);
                }
                else
                {
                    ds = HttpContext.Cache.Get(blogCacheKey) as DataSet;
                }

                if (ds != null) vm.BastiatDataset = ds.Tables[0];
                //if (ds != null) dlMisesBlog.DataSource = ds.Tables[0];
                //dlMisesBlog.DataBind();
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(HttpContext, ex);
            }
        }

        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult VideoPod()
        {
           var vm = RandomVideo();

            return View("VideoPod", vm);
        }

        [OutputCache(VaryByParam = "*", Duration = 5)]
        public VideoPodViewModel RandomVideo()
        {
            DocumentDTO dto = Services.MediaService.GetRandomVideo();

            var vm = Mapper.Map<DocumentDTO, VideoPodViewModel>(dto);

            return vm;
        }

    }
}