﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Caching;
using System.Web.Mvc;
using Mises.Data;
using Mises.Domain.Utility;
using Mises.Web.ViewModels;

#endregion

namespace Mises.Web.Controllers
{
    public class SidebarController : Controller
    {
        private const string blogCacheKey = "blogKey3";
        //private const string communityCacheKey = "communityKey";

        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult FeaturedProduct()
        {
            return View("Sidebars/Community");
        }

//        [OutputCache(Duration = 360, VaryByParam = "*")]
//        public ActionResult Community()
//        {
//            var vm = new SidebarViewModel();


//            try
//            {
//                try
//                {
//                    DataSet ds;

//                    if (HttpContext.Cache.Get(communityCacheKey) == null)
//                    {
//                        ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
//                                                      "dbo.CommunityGetFeed");
//                        HttpContext.Cache.Add(communityCacheKey, ds, null, DateTime.Now.AddMinutes(10), TimeSpan.Zero,
//                                              CacheItemPriority.Normal, null);
//                    }
//                    else
//                    {
//                        ds = HttpContext.Cache.Get(communityCacheKey) as DataSet;
//                    }

//                    if (ds != null)
//                    {
//                        vm.Community = ds.Tables[0];
//                        //dlAustrianNetworkForums.DataSource = ds.Tables[0];
//                        //dlAustrianNetworkForums.DataBind();

//                        try
//                        {

//                            foreach (DataRow dr in ds.Tables[1].Rows.Cast<DataRow>().Where(dr => dr.RowState != DataRowState.Deleted))
//                            {
//                                DateTime postDate;
//                                DateTime.TryParse(Convert.ToString(dr["PubDate"]), out postDate);
//                                string applicationKey = dr["ApplicationKey"].ToString();
//                                string baseURL = Request.Url.Scheme + "://" + Request.Url.Authority;
//                                dr["link"] = string.Format(baseURL + "/Community/blogs/{0}/archive/{1}/{2}/{3}.aspx",
//                                                           applicationKey,
//                                                           postDate.Year, postDate.Month.ToString("00"),
//                                                           postDate.Day.ToString("00"));
//                            }


//                            vm.Blogs = ds.Tables[1];
//                            //dlAustrianNetworkBlogs.DataSource = ds.Tables[1];
//                            //dlAustrianNetworkBlogs.DataBind();
//                        }
//                        catch (Exception ex)
//                        {
//                            ExceptionLogging.LogException(HttpContext, ex);
//                        }
//                    }
//                }
//                catch (SqlException ex)
//                {
//                    ExceptionLogging.LogException(HttpContext, ex);
//                }

//                try
//                {
//                    DataSet ds;

//                    if (HttpContext.Cache.Get(blogCacheKey) == null)
//                    {
//                    var blog = new MySqlHelper(ConfigurationManager.ConnectionStrings["BastiatBlog"].ConnectionString);

//                    const string sqlGetBlogPosts =
//                        @"
//SELECT 
//bas_posts.ID,
//post_title AS title,
//bas_posts.guid AS link,
//post_date AS pubDate,
//display_name AS author,
//user_url 
//FROM bas_posts 
//JOIN bas_users ON bas_users.id = bas_posts.post_author
//WHERE post_type = 'post' 
//AND post_status='publish'
//AND display_name  != 'Mises Daily'
//ORDER BY post_date DESC
//LIMIT 10";
//                        ds = blog.ExecuteQuery(sqlGetBlogPosts);
//                        HttpContext.Cache.Add(blogCacheKey, ds, null, DateTime.Now.AddMinutes(20), TimeSpan.Zero,
//                                              CacheItemPriority.Normal, null);
//                    }
//                    else
//                    {
//                        ds = HttpContext.Cache.Get(blogCacheKey) as DataSet;
//                    }

//                    vm.BastiatBlog = ds.Tables[0];
//                    //if (ds != null) dlMisesBlog.DataSource = ds.Tables[0];
//                    //dlMisesBlog.DataBind();
//                }
//                catch (Exception ex)
//                {
//                    ExceptionLogging.LogException(HttpContext, ex);
//                }
//            }
//            catch (Exception ex)
//            {
//                ExceptionLogging.LogException(HttpContext, ex);
//            }

//            return View("Sidebars/Community",vm);
//        }

        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult BastiatBlog()
        {
            var vm = new SidebarViewModel();

            try
            {
               
                try
                {
                    DataSet ds;

                    if (HttpContext.Cache.Get(blogCacheKey) == null)
                    {
                        var blog = new MySqlHelper(ConfigurationManager.ConnectionStrings["BastiatBlog"].ConnectionString);

                        const string sqlGetBlogPosts =
                            @"
SELECT 
bas_posts.ID,
post_title AS title,
bas_posts.guid AS link,
post_date AS pubDate,
display_name AS author,
user_url 
FROM bas_posts 
JOIN bas_users ON bas_users.id = bas_posts.post_author
WHERE post_type = 'post' 
AND post_status='publish'
AND display_name  != 'Mises Daily'
ORDER BY post_date DESC
LIMIT 10";
                        ds = blog.ExecuteQuery(sqlGetBlogPosts);
                        HttpContext.Cache.Add(blogCacheKey, ds, null, DateTime.Now.AddMinutes(20), TimeSpan.Zero,
                                              CacheItemPriority.Normal, null);
                    }
                    else
                    {
                        ds = HttpContext.Cache.Get(blogCacheKey) as DataSet;
                    }

                    vm.BastiatBlog = ds.Tables[0];
                    //if (ds != null) dlMisesBlog.DataSource = ds.Tables[0];
                    //dlMisesBlog.DataBind();
                }
                catch (Exception ex)
                {
                    ExceptionLogging.LogException(HttpContext, ex);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(HttpContext, ex);
            }

            return View("Sidebars/BastiatBlog", vm);
        }

        [OutputCache(Duration = 190)]
        public ActionResult Literature()
        {
            var db = DataHelper.EntityDataModel;
//List<DocumentGetNewLiteratureResult> lit = db.DocumentGetNewLiterature().ToList();

            var lit = db.Documents.Include("DocumentFiles").Include("DocumentAuthor").NoTracking().Where(d => d.Display && d.DocumentFiles.Any(df => df.MediaTypeId == 3 || df.MediaTypeId == 9)).OrderByDescending(d => d.EditDate).Take(9);
            
            return View("Sidebars/Literature", lit);
        }

        [OutputCache(Duration = 190)]
        public ActionResult About()
        {
            var db = DataHelper.MisesDataContext;
            var biographies = db.DocumentsGetBiographies().ToList();
            return View("Sidebars/About", biographies);
        }

        [OutputCache(Duration = 190)]
        public ActionResult UpcomingEvents()
        {
            var db = DataHelper.MisesDataContext;
            var events = db.CalendarGetUpcomingEvents(4).ToList();

            return View("Sidebars/UpcomingEvents", events);
        }

        [OutputCache(Duration = 190)]
        public ActionResult Media()
        {
            var db = DataHelper.MisesDataContext;
            ViewBag.Authors = db.MediaTopAuthors();
            ViewBag.TaggedMedia = db.MediaGetTopTagged(null,null);
            ViewBag.Subjects = db.MediaTopFullSubjects();
            ViewBag.NewMedia = db.MediaGetLatest();

            return View("Sidebars/Media");
        }
    }
}