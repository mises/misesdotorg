﻿using System;
using System.Linq;
using System.Web.Mvc;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Events;
using Mises.Web.ViewModels;
using System.Collections.Generic;

namespace Mises.Web.Controllers
{
    public class EventsController : Controller
    {
        private readonly MisesModel model = DataHelper.EntityDataModel;

        //
        // GET: /Events/

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public ActionResult Index(int id = 0, string filter = "")
        {
            if (id == 0)
            {
                var futureEvents =
                    model.Calendar.NoTracking().Where(e => e.Display).Where(e => e.EndDate >= DateTime.Now).Where(e => filter == "" || e.Title.Contains(filter)).OrderBy(
                        e => e.EndDate);



                var pastEvents =
                    model.Calendar.NoTracking().Where(e => e.Display).Where(e => e.EndDate <= DateTime.Now).Where(e => filter == "" || e.Title.Contains(filter)).OrderByDescending(
                        e => e.EventId).Select(e => new
                        {
                            e.Title,
                            e.IntroText,
                            e.CreateDate,
                            e.EndDate,
                            e.EventId,
                            //e.EventImage, 
                            e.Location,
                            e.StartDate
                        });

                var vm = new EventsViewModel { UpcomingEvents = futureEvents, PastEvents = new List<Calendar>() };

                pastEvents.OrderByDescending(e=> e.StartDate).Take(60).ToList().ForEach(evnt => vm.PastEvents.Add(new Calendar()
                                                                         {
                                                                             Title = evnt.Title,
                                                                             EventId = evnt.EventId,
                                                                             StartDate = evnt.StartDate,
                                                                             EndDate = evnt.EndDate,
                                                                             IntroText = evnt.IntroText,
                                                                             Location = evnt.Location,
                                                                             //EventImage = evnt.EventImage
                                                                         }));

                vm.PastEvents = vm.PastEvents.OrderByDescending(e => e.StartDate).ToList();

                return View("Index", vm);
            }
            else // event detail
            {
                var db = DataHelper.MisesDataContext;

                int eventId = id;
                if (eventId == 0)
                {
                    eventId = Convert.ToInt32(Conversion.Val(Request.QueryString["control"]));
                }
                if (eventId == 0)
                {
                    return View("Detail");
                }

                CalendarGetEventDetailResult calendarEvent = db.CalendarGetEventDetail(eventId).FirstOrDefault();

                return View("Detail", calendarEvent);
            }
        }

        public ActionResult iCal(int id = 0)
        {
            var db = DataHelper.MisesDataContext;

            if (id > 0)
            {
                Response.Clear();
                Response.ContentType = "text/calendar";

                var calendarEvent = db.CalendarGetEventDetail(id).FirstOrDefault();

                return Content(Events.GetICalEvent(calendarEvent));
            }
            else
            {
                var events = db.CalendarGetUpcomingEvents(null);

                // Create a new iCalendar
                iCalendar iCal = new iCalendar();
                iCal.AddProperty("X-WR-CALNAME", ViewBag.Title);

                //iCal.Calendar.Properties.Add(new CalendarProperty(""));
                events.ToList().ForEach(
                    p => Events.CalendarEventToICalEvent(iCal, new CalendarGetEventDetailResult
                                                                   {
                                                                       CreateDate = p.CreateDate,
                                                                       Display = p.Display,
                                                                       EndDate = p.EndDate,
                                                                       EventId = p.EventId,
                                                                       IntroText = p.IntroText,
                                                                       Location = p.Location,
                                                                       StartDate = p.StartDate,
                                                                       Title = p.Title,
                                                                   }));


                Response.Clear();
                Response.ContentType = "text/calendar";
                Response.AddHeader("content-disposition", "attachment;filename=Mises_Event_Calendar.ics");

                iCalendarSerializer serializer = new iCalendarSerializer(iCal);

                return Content(serializer.SerializeToString(iCal));
            }
        }
    }
}