﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookCatalog.Models;
using Mises.Data;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core.Objects;


namespace Mises.Web.Controllers
{
    public class PageController : Controller
    {
        //private MisesModel db = DataHelper.EntityDataModel;

        //
        // GET: /Page/

        public ActionResult Index(int id)
        {
            var db = DataHelper.EntityDataModel;
            db.PageTableSet.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            PageTable page = db.PageTableSet.Single(p => p.PageId == id);
            ViewData.Add("DocumentId",page.PageId);

            return View("Index", page);
        }
    }
}