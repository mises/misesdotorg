﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mises.Data;
using Mises.Domain.Services;
using Mises.Web.ViewModels;

#endregion

namespace Mises.Web.Controllers
{
    public class ThemeController : Controller
    {
        //
        // GET: /Theme/

        private QuoteGetRandomResult quote;

        public QuoteGetRandomResult Quote
        {
            get { return quote ?? (quote = quotes.RandomQuote()); }
        }

        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult Footer()
        {

            var db = DataHelper.EntityDataModel;

            var model = new FooterViewModel();

            model.Literature = db.Documents.Include("DocumentFiles").Include("DocumentAuthor").NoTracking().Where(d=> d.Display && d.DocumentFiles.Any(df => df.MediaTypeId == 3 || df.MediaTypeId == 9)).OrderByDescending(d => d.EditDate).Take(9);
            model.Audio = db.Documents.Include("DocumentFiles").Include("DocumentAuthor").NoTracking().Where(d => d.Display && d.DocumentFiles.Any(df => df.MediaTypeId == 1 || df.MediaTypeId == 1)).OrderByDescending(d => d.EditDate).Take(8);

            model.Video = db.Documents.Include("DocumentFiles").Include("DocumentAuthor").NoTracking().Where(d => d.Display && d.DocumentFiles.Any(df => df.MediaTypeId == 2 || df.MediaTypeId == 14 || df.MediaTypeId == 16)).OrderByDescending(d => d.EditDate).Take(8);
            

            model.Quote = Quote;

            return View("../Shared/Footer", model);
        }
    }
}