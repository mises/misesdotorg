﻿#region

using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Mises.Domain;
using Mises.Domain.Contracts;
using Mises.Services;
using Mises.Web.ViewModels.Authors;

#endregion

namespace Mises.Web.Controllers
{
    public class AuthorsController : BaseController
    {


        public AuthorsController(IServiceLayer services) : base(services)
        {
        }  
     



        [HttpGet]
        public ActionResult AuthorBiography(int AuthorId = 0, int ArticleId = 0)
        {
            AuthorRepository authorRepository = new AuthorRepository();

            var authors = authorRepository.GetAuthorInfo(AuthorId, ArticleId);
            if (authors.Count == 0)
                return Content("");

            return View("../Daily/AuthorBiography", authors);
        }

        #region Media Actions

        [HttpGet]
        [OutputCache(Duration = 86400, VaryByParam = "*")]
        public ActionResult Index()
        {
            return View();
            // return RedirectToRoute("authors-browse", new { letter = 'a', page = 1 });
        }

        [HttpGet]
        public JsonResult SearchJSON(String startsWith)
        {
            var filter = new AuthorSurnameFilter(startsWith);

            var dto = Services.MediaService.GetAuthors(filter, 0, Int32.MaxValue);

            var result = new JsonResult
                             {
                                 JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                                 Data = dto.Items.Select(i => new
                                                                  {
                                                                      label =
                                                                  String.Format("{0}, {1}", i.LastName, i.FirstName),
                                                                      value =
                                                                  String.Format("{0}/{1}", i.Id,
                                                                                String.Format("{0}{1} {2}", i.FirstName,
                                                                                              String.IsNullOrWhiteSpace(
                                                                                                  i.MiddleName)
                                                                                                  ? String.Empty
                                                                                                  : String.Concat(" ",
                                                                                                                  i.
                                                                                                                      MiddleName),
                                                                                              i.LastName).ToSlug())
                                                                  })
                             };

            return result;
        }

        [HttpGet]
        public ActionResult Browse(Char letter, Int32 page)
        {
            var vm = new BrowseViewModel
            {
                Title = "Authors" //String.Format("Authors on {0}", Request.Url.Host);
            };

            if (vm.AlphabetChars.All(c => c != letter) || (page < 1))
            {
                var l = letter.ToString().ToLower()[0];
                letter = (vm.AlphabetChars.Any(c => c == l)) ? l : 'a';

                if (page < 1)
                    page = 1;

                return RedirectToRoute("authors-browse", new { letter, page });
            }

            var found = false;
            var counts = Services.MediaService.GetAuthorCountsBySurnameInitial();

            for (Int32 i = 0; i < 25; i++)
            {
                var ci = vm.AlphabetChars.IndexOf(letter);

                vm.Alphabet[ci].Value = (counts.ContainsKey(letter) ? counts[letter] : 0).ToString();
                vm.Alphabet[ci].Selected = !found && (vm.Alphabet[ci].Value != "0");

                if (vm.Alphabet[ci].Selected)
                    found = true;

                letter = Convert.ToChar(letter + 1);
                if (letter == vm.AlphabetChars.Last())
                    letter = vm.AlphabetChars.First();
            }

            var item = vm.Alphabet.FirstOrDefault(t => t.Selected);
            if (item != null)
            {
                vm.PageIndex = page - 1; //TODO validate page
                vm.PageSize = ApplicationDefaults.PageSize;

                var searchFilter = new AuthorSurnameInitialFilter(letter);
                var dto = Services.MediaService.GetAuthors(searchFilter, vm.PageIndex, vm.PageSize);

                Mapper.Map(dto, vm);
                vm.ListSize = ApplicationDefaults.ListSize;
                vm.RouteSegmentName = "page";
            }

            return View(vm);
        }

        [HttpGet]
        public ActionResult Detail(Int32 id, Int32 articlePage, Int32 documentPage = 0)
        {
            if (Request.IsAjaxRequest())
            {
                if ((Request.Params["ajaxsrc"] == String.Empty) || (Request.Params["ajaxsrc"] == "articles"))
                {
                    IArticleFilter filter = new ArticlesByAuthorFilter(id);
                    var dto = Services.MediaService.GetArticleSummaries(filter, articlePage - 1,
                                                                        ApplicationDefaults.SummaryPageSize);
                    dto.RouteSegmentName = "articlePage";
                    dto.ListSize = ApplicationDefaults.ListSize;
                    return PartialView("ArticleSummaryList", new ViewModels.Articles.BrowseViewModel(dto));
                }
                else
                {
                    IDocumentFilter filter = new DocumentsByAuthorFilter(id);
                    var dto = Services.MediaService.GetDocuments(filter, documentPage - 1,
                                                                 ApplicationDefaults.SummaryPageSize);
                    dto.RouteSegmentName = "documentPage";
                    dto.ListSize = ApplicationDefaults.ListSize;
                    return PartialView("DocumentSummaryList", new ViewModels.Media.BrowseViewModel(dto));
                }
            }
            else
            {
                var vm = new DetailViewModel();
                var dto = Services.MediaService.GetAuthor(id, articlePage - 1, ApplicationDefaults.SummaryPageSize,
                                                          documentPage - 1, ApplicationDefaults.SummaryPageSize);
                Mapper.Map(dto, vm);

                vm.Articles.RouteSegmentName = "articlePage";
                vm.Articles.ListSize = ApplicationDefaults.ListSize;
                vm.Documents.RouteSegmentName = "documentPage";
                vm.Documents.ListSize = ApplicationDefaults.ListSize;

                vm.Publications.RouteSegmentName = "documentPage";
                vm.Publications.ListSize = ApplicationDefaults.ListSize;

                vm.Title = vm.FullName;
                return this.RazorView(vm);
            }
        }

        [HttpGet]
        public ActionResult DetailCard(AuthorDTO author, MisesDTO featured)
        {
            // Get the featured from the db (can't use the lists as the documents/articles aren't deep loaded)...
            //NOTE: A lot around here currently assumes descending date order
            if ((featured == null) || (featured.Id == 0))
            {
                featured = Services.MediaService.GetLatestForAuthor(author.Id);
            }

            var vm = new DetailViewModel
            {
                Featured = featured
            };

            Mapper.Map(author, vm);

            return PartialView(vm);
        }

        #endregion Media Actions
    }


}