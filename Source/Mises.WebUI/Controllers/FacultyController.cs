﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mises.Data;
using Mises.Web.ViewModels;

namespace Mises.Web.Controllers
{
    public class FacultyController : Controller
    {
        public ActionResult Index()
        {
            // SELECT [control], [name], [school], [email], LTRIM(RTRIM([web])) As 'web' FROM [Faculty] WHERE (groups = 'Associated Scholars' OR groups = 'Other Scholars Working in the Austrian Tradition' OR groups = 'Senior Faculty') AND [display] = 1 order by name

            var model = DataHelper.EntityDataModel;

            var vm = new FacultyModel
                         {
                             SeniorFaculty =
                                 model.Faculty.Where(
                                     f =>
                                     f.groups == "Senior Faculty").Where(f => f.display).OrderBy(f => f.name).ToList(),
                             AssociatedScholars = 
                                 model.Faculty.Where(
                                     f =>
                                     f.groups == "Associated Scholars").Where(
                                         f => f.display).OrderBy(f => f.name).ToList(),
                             Staff =
                                 model.Faculty.Where(f => f.groups == "Staff").Where(f => f.display).OrderBy(f => f.name)
                                 .ToList(),
                                 AdjunctFaculty = new List<Faculty>()
                         };

            return View("Index", vm);
        }

        public ActionResult OtherScholarsAustrianTradition()
        {
            var model = DataHelper.EntityDataModel;

            var vm = new FacultyModel
            {
                SeniorFaculty = new List<Faculty>(),
                AssociatedScholars =  new List<Faculty>(),
                Staff = new List<Faculty>(),

                AdjunctFaculty = 
                    model.Faculty.Where(
                        f =>
                        f.groups ==  "Other Scholars Working in the Austrian Tradition").Where(
                            f => f.display).OrderBy(f => f.name).ToList(),
            };

            return View("Others", vm);
        }

        public ActionResult Detail(int id)
        {
            var db = DataHelper.MisesDataContext;
            FellowsGetBioResult fellow = db.FellowsGetBio(id).FirstOrDefault();

            if (fellow == null) return View(new FellowsGetBioResult());

            ViewBag.Title = DataFormat.StripHTML(fellow.name);
            return View(fellow);
        }
    }
}