﻿<%@ Page Title="Mises Audio/Video" Language="C#" MasterPageFile="~/MasterPages/Content.master"
         AutoEventWireup="false" MaintainScrollPositionOnPostback="true"
         Inherits="MediaPage" Codebehind="media.aspx.cs" %>
<%@ OutputCache Duration="180" VaryByParam="*" %>
<%@ Import Namespace="Mises.Data" %>
<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<%@ Register Src="Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>
<%@ Register Assembly="RssToolkit" Namespace="RssToolkit.Web.WebControls" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Media/FeaturedMedia.ascx" TagName="FeaturedMedia" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <asp:Literal runat="server" ID="litHeader">Mises Media</asp:Literal>
    </h1>
    <asp:Image runat="server" ID="imgAuthorHeader" Visible="false" />
    <p runat="server" id="pHeaderText">
        <uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="MediaHeader" />
    </p>
    <uc2:FeaturedMedia ID="FeaturedMedia1" runat="server" />
    <!-- /.featured-media -->
    <div class="box medialibrary">
        <div class="filterhead">
            <ul class="filter">
                <li class="first">Filter by:</li>
                <li><a href="?action=category&amp;ID=-1">Date</a></li>
                <li><a href="?action=category">Category</a>
                    <ul>
                        <li class="first selected"><a href="#">Category</a></li>
                        <asp:SqlDataSource ID="sqlMediaCategories" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                                           SelectCommand="MediaGetSubCategories" EnableCaching="True"></asp:SqlDataSource>
                        <asp:Repeater ID="rptCategoryList" runat="server" DataSourceID="sqlMediaCategories">
                            <ItemTemplate>
                                <li><a href="/media/category/<%#Eval("CategoryId")%>/<%#DataFormat.GenerateSlug(Eval("Category"))%>">
                                        <%#Eval("Category")%></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </li>
                <li><a href="media.aspx?action=subject">Subject</a>
                    <ul>
                        <%--<li class="first selected"><a href="#">Clear Filter</a></li>--%>
                        <asp:Repeater ID="rptSubjectList" runat="server" DataSourceID="sqlMediaSubjects">
                            <ItemTemplate>
                                <li><a href="/media.aspx?action=subject&amp;ID=<%#Eval("SubjectId")%>">
                                        <%#Eval("ShortSubject")%></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </li>
                <li class="last"><a href="?action=author">Author</a>
                <ul>
                    <%--<li class="first selected"><a href="?action=author">Clear Filter</a></li>--%>
                    <asp:Repeater ID="rptAuthorList" runat="server" DataSourceID="sqlMediaAuthors">
                        <ItemTemplate>
                            <li><a href="<%#string.Format("/media/author/{0}/{1}", Eval("AuthorId"),
                                           DataFormat.GenerateSlug(Eval("Author")))%>">
                                    <%#Eval("Author")%>
                                    (<%#Eval("Count")%>entries.)</a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <li><a href="?action=MediaType">Media Type</a> </li>
            </ul>
            <!-- /filterhead -->
        </div>
        <!-- / .box -->
    </div>
    <cc1:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False" Width="650px"
                  CssClass="tlibrary tliteraturel">
        <Columns>
            <asp:TemplateField HeaderText="Subject">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("SubjectId", "media.aspx?action=subject&amp;Id={0}")%>'
                                   Text='<%#Eval("ShortSubject")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </cc1:GridView>

    <div id="alpha" runat="server" visible="false"> 
            <div class="alphabet">
                <asp:Literal ID="litAuthorLetters" runat="server" Text="Literature Library"></asp:Literal>
            </div>
        </div>

    <cc1:GridView ID="gvAuthors" runat="server" AutoGenerateColumns="False" Width="650px"
                  CssClass="tlibrary tliteraturel">
        <Columns>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <img src="/images/icons/icon_category.gif" alt="Folder" />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#string.Format("/media/author/{0}/{1}", Eval("AuthorId"),
                                                  DataFormat.GenerateSlug(Eval("Author")))%>'
                                   Text='<%#Eval("Author")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </cc1:GridView>
    <asp:SqlDataSource ID="sqlMediaAuthors" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="MediaGetAuthorsAlpha" SelectCommandType="StoredProcedure" EnableCaching="True">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlMediaSubjects" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="MediaTopSubjects" SelectCommandType="StoredProcedure" EnableCaching="True">
    </asp:SqlDataSource>
    <h4><a id="categories" name="categories"></a>
        <a href="/media">Top</a> &gt;
        <asp:Literal ID="lblAction" runat="server"></asp:Literal></h4>
    <cc1:GridView ID="gvCategories" runat="server" AutoGenerateColumns="False" CssClass="tlibrary tmedial"
                  ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif" SortColumnHeaderIsBold="True"
                  SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <Columns>
            <asp:TemplateField HeaderText="Category" SortExpression="Category" ItemStyle-CssClass="title">
                <ItemTemplate>
                    <h3>
                        <a href='/media/category/<%#Eval("CategoryId")%>/<%#DataFormat.GenerateSlug(Eval("Category"))%>'>
                            <%#DataBinder.Eval(Container, "DataItem.Category")%>
                        </a>
                    </h3>
                    <asp:Image runat="server" ID="imgCategory" CssClass="CategoryImage" ImageAlign="Right"
                               Visible="false" />
                    <p>
                        <%#DataBinder.Eval(Container, "DataItem.Description")%></p>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Feed" HeaderStyle-CssClass="feed">
                <ItemTemplate>
                    <asp:HyperLink runat="server" ID="lnk" CssClass="podcast" NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.CategoryId", "Feed/Media/?CategoryId={0}")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </cc1:GridView>
    <!-- Media File List -->
    <cc1:GridView ID="gvMediaItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                  CssClass="tlibrary tmedial" AllowSorting="True" PageSize="50" EnableNextPrevNumericPager="True"
                  ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif"
                  EnableSelection="False" SortDirectionAlt="Ascending" SortExpressionAlt="">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Media File:" SortExpression="Title">
                <ItemTemplate>
                    <h5>
                        <a href='/media/<%#DataBinder.Eval(Container, "DataItem.FileId")%>/<%#DataFormat.GenerateSlug(Eval("Title"))%>'>
                            <img src='<%#DataBinder.Eval(Container, "DataItem.MediaIconPath")%>' alt='<%#DataBinder.Eval(Container, "DataItem.MediaType")%>' />
                            <%#DataBinder.Eval(Container, "DataItem.Title")%>
                        </a>
                    </h5>
                    <p>
                        <%#DataBinder.Eval(Container, "DataItem.Description")%></p>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="LastName">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#string.Format("/media/author/{0}/{1}", Eval("AuthorId"),
                                                  DataFormat.GenerateSlug(Eval("Author")))%>'
                                   Text='<%#Eval("Author")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="CoAuthor" SortExpression="LastName">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#string.Format("/media/CoAuthor/{0}/{1}", Eval("CoAuthorId"),
                                                  DataFormat.GenerateSlug(Eval("CoAuthor")))%>'
                                   Text='<%#Eval("CoAuthor")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField HeaderText="Date" DataField="CreateDate" SortExpression="CreateDate"
                            HtmlEncode="false" DataFormatString="{0:D}" />
            <asp:TemplateField HeaderText="Feed" HeaderStyle-CssClass="feed">
                <ItemTemplate>
                    <asp:HyperLink runat="server" CssClass="podcast" ID="lnk" NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.AuthorId", "Feed/Media/?AuthorId={0}")%>'></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle CssClass="feed"></HeaderStyle>
            </asp:TemplateField>
        </Columns>
    </cc1:GridView>
    <cc1:GridView ID="gvMediaType" runat="server" AutoGenerateColumns="False" Width="650px"
                  CssClass="tlibrary tliteraturel">
        <Columns>
            <asp:ImageField DataAlternateTextField="MediaType" DataImageUrlField="MediaIconPath"
                            HeaderText="Type" SortExpression="MediaType">
            </asp:ImageField>
            <asp:HyperLinkField DataNavigateUrlFields="MediaTypeId" DataNavigateUrlFormatString="media.aspx?action=MediaType&amp;Id={0}"
                                DataTextField="MediaType" HeaderText="MediaType" />
        </Columns>
    </cc1:GridView>
    <!--Author Archive -->
    <div id="divAuthorArchive" runat="server" visible="false">
        <h3>
            <asp:HyperLink ID="lnkAuthorDailyArchiveLink" runat="server">Mises Daily Archives</asp:HyperLink>
        </h3>
        <h3>
            <asp:HyperLink ID="lnkAuthorLiterature" runat="server">Mises Literature Archives</asp:HyperLink>
        </h3>
    </div>
    
    <%--<script type='text/javascript' src="/Scripts/silverlight.js"></script>--%>
</asp:Content>