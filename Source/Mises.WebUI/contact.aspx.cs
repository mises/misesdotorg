#region

using System;
using System.Configuration;
using System.Web.UI;
using Mises.Data;
using Mises.Domain.Utility;
using Configuration = Mises.Domain.Feeds.Configuration;

#endregion

partial class contact : Page
{
    private void Page_Load(object sender, EventArgs e)
    {
        //Put user code to initialize the page here
        if (! Page.IsPostBack)
        {
            if (Request.QueryString["form"] == "blog")
            {
                pnlContactForm.Visible = false;
                pnlBlogSubmit.Visible = true;
            }
        }
    }


    protected void Submit1_Click(object sender, EventArgs e)
    {
        string contactEmail;
        try
        {
            contactEmail = ConfigurationManager.AppSettings["ContactEmail"];
        }
        catch (Exception)
        {
            contactEmail = Configuration.Email;
        }

        string message = DataFormat.StripHTML(txtComments.Value.Trim());

        message = "Submitted:  " + DateTime.Now + "(Central) " + Environment.NewLine + Environment.NewLine +
                  "Email:  " + txtEmail.Text + Environment.NewLine + "Name:  " + txtName.Text +
                  Environment.NewLine + "Country:  " + txtCountry.Text + Environment.NewLine + "Comments:  " +
                  message;


        EmailHelper.SendMail(txtEmail.Text.Trim(), contactEmail, "Mises.org message from " + txtName.Text.Trim(),
                             message);

        lblSubmitResult.Text = "Thank you.  Your message has been sent.";
        pnlContactForm.Visible = false;
    }

    protected void btnBlogSubmit_Click(object sender, EventArgs e)
    {
        if (! Page.IsValid)
        {
            return;
        }

        if (txtBlogText.Text.Contains("http"))
        {
            lblSubmitResult.Text =
                "Due to spam, please do not include any active \"http\" links in your submission.";
            return;
        }

        pnlBlogSubmit.Visible = false;

        const string contactEmail = "kristy@mises.com";


        EmailHelper.SendMail(txtBlogEmail.Text.Trim(), contactEmail,
                             "Mises Blog Submission from " + txtBlogName.Text.Trim(),
                             Environment.NewLine + txtBlogText.Text.Trim() + Environment.NewLine);
        lblSubmitResult.Text = "Thank you.  Your blog has been sent and will be reviewed soon.";
    }
}