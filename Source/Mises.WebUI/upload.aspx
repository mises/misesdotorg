<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" Title="upload form" Inherits="upload" Codebehind="~/upload.aspx.cs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    &nbsp;<table style="margin-right: auto; margin-left: auto" >
              <tr>
                  <td style="width: 100%" align="center" >
                      This form allows you to upload most files.<br />
                      Your name:<br />
                      &nbsp;<asp:TextBox ID="txtName" Runat="server"></asp:TextBox>
                      <br />
                      Your file:<br />
                      <asp:FileUpload ID="uploadedFile" Runat="server" /><br />
                      <asp:Label ID="lblStatus" runat="server" CssClass="ErrorMessage" Font-Bold="True"
                                 ForeColor="Red"></asp:Label>
                      <br />
                      &nbsp;<asp:Button ID="btnUpload" Runat="server" Text="Upload" OnClick="btnUpload_Click"  CssClass="ui-state-default ui-corner-all ui-button"  /></td>
              </tr>
          </table>
    &nbsp;<br />
    <br />
    <br />
    <asp:Label ID="lblResults" Runat="server"></asp:Label>

</asp:Content>