#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Payments;
using Mises.Domain.Utility;
using Donations = Mises.Domain.Payments.Donations;

#endregion

public partial class RegistrationPage : Page
{
    private readonly List<Product> _products = new List<Product>();
    private readonly List<Question> _questions = new List<Question>();
    private decimal _currentSum = -1M;
    private bool _quantityOne;
    public RegistrationForm RegistrationForm { get; set; }

    private string email;

    public Controls_ContactInfo ContactInfo;

    private void Page_Load(object sender, EventArgs e)
    {

        btnSubmitRegistration.Click += btnSendContribution_Click;
        btnGetTotal.Click += btnGetTotal_Click;
        dlProducts.ItemDataBound += dlProducts_ItemDataBound;
        dlQuestions.ItemDataBound += dlQuestions_ItemDataBound;
        txtCustomAmount.TextChanged += UpdateTotal;

        if (IsPostBack) return;

        if (Request.UrlReferrer != null)
        {
            ViewState["UrlReferrer"] = Request.UrlReferrer;
        }

        // Load Form

        if (FormId == 0)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;
            Response.Redirect(baseUrl);
        }


        try
        {
            RegistrationForm = new RegistrationForm(FormId);
        }
        catch (ArgumentOutOfRangeException)
        {
            litTitle.Text = "This registration form does not exist.";
            return;
        }

        if (!Request.IsSecureConnection && Request.Url.Host != "localhost" && Request.Url.Host != "beta.mises.org" && Request.Url.Host != "local.mises.org")
        {
            //TODO: FIX to relative URL 
            Response.Redirect("https://mises.freecapitalists.org/forms/" + FormId + "/" + RegistrationForm.Title.ToSlug());
        }

        if (!RegistrationForm.ShowSidebar)
        {
            Context.Items["ShowSidebar"] = false;
        }


        if (RegistrationForm.GetAddressInfo == false)
        {
            ContactInfo.GetNameOnly();
        }

        //btnGetTotal.Visible = RegistrationForm.ProcessPayment;

        if (RegistrationForm.TakeCreditCard == false)
        {
            //TODO: FIX to relative URL
            btnGetTotal.Visible = false;
            PaymentInfo.Visible = false;
            //Me.PaymentInfo.PaymentComments &= "(Disabling CC info is not done yet.)"
        }

        if (RegistrationForm.EnterAmountPrompt)
        {
            pnlCustomAmount.Visible = true;
        }


        Page.Title = DataFormat.StripHTML(RegistrationForm.Title);
        litTitle.Text = RegistrationForm.Title;
        litDescription.Text = RegistrationForm.Introduction;
        lblFooterText.Text = RegistrationForm.FooterText;
        lblDonationThankYou.Text = RegistrationForm.ThankYouMessage;
        ContactInfo.ImageURL = RegistrationForm.Image;
        chkRecurring.Visible = RegistrationForm.RecurringPrompt;

        if (RegistrationForm.GetAddressInfo || RegistrationForm.TakeCreditCard || RegistrationForm.ProcessPayment)
        {
            ContactInfo.Visible = true;
        }

        if (RegistrationForm.TakeCreditCard)
        {
            PaymentInfo.Visible = true;
        }

        GetTotalCost();
    }

    protected int FormId
    {
        get { return Convert.ToInt32(Conversion.Val(Request.QueryString["Id"])); }
    }

    #region Load Form

    protected void dlProductsDataBound(object sender, DataListItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) && !_quantityOne)
        {
            _quantityOne = true;
        }
    }

    protected void dlProducts_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        // Format Fields
        if (((Literal)(e.Item.FindControl("SelectQuantity"))).Text == "True")
        {
            ((e.Item.FindControl("txtQuantity"))).Visible = true;
            ((e.Item.FindControl("lblQuantity"))).Visible = true;
        }
        else
        {
            ((e.Item.FindControl("lblQuantity"))).Visible = false;
            ((TextBox)(e.Item.FindControl("txtQuantity"))).Text = "1";
            ((e.Item.FindControl("txtQuantity"))).Visible = false;
        }

        try
        {
            if (((Literal)(e.Item.FindControl("ProductId"))).Text == RegistrationForm.PrimaryProductId.ToString())
            {
                //Throw New Exception(_regform.PrimaryProductId.ToString)
                var chkPrice = (CheckBox)(e.Item.FindControl("chkPrice"));
                chkPrice.Checked = true;
                chkPrice.Enabled = false;
            }
        }
        catch
        {
        }
    }

    // generate questions
    protected void dlQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        // Format QUestions
        int questionType = Convert.ToInt32(((Literal)(e.Item.FindControl("lblQuestionType"))).Text);
        switch (questionType)
        {
            case 0:
            case 1: // Textbox
                {
                    break;
                }
            case 2: // TextArea
                {
                    var txtAnswer = (TextBox)(e.Item.FindControl("txtAnswer"));
                    txtAnswer.TextMode = TextBoxMode.MultiLine;
                    txtAnswer.Rows = 3;
                    txtAnswer.Columns = 70;
                    break;
                }
            case 3: // Yes/No
                {
                    break;
                }
            case 4: // Number
                {
                    var txtAnswer = (TextBox)(e.Item.FindControl("txtAnswer"));
                    txtAnswer.Columns = 15;
                    break;
                }
            case 5: // Amount Picker
                {
                    divAmountPicker.Visible = true;
                    ddDonationAmount.Visible = true;
                    string questionLiteral = ((Literal)(e.Item.FindControl("QuestionLiteral"))).Text;
                    lblDonationPrompt.Text = questionLiteral;
                    lblEnterAmount.Text = "Or Enter Amount:";
                    e.Item.Visible = false;
                    e.Item.Controls.Clear();
                    dlQuestions.Visible = false; // bug

                    break;
                }
        }
    }

    #endregion

    #region Event Handlers

    protected void UpdateTotal(object sender, EventArgs e)
    {
        GetTotalCost();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }

    protected void btnSendContribution_Click(object sender, EventArgs e)
    {
        ProcessRegistration();
        LoadComplete += form_LoadComplete;
    }

    private void form_LoadComplete(object sender, EventArgs e)
    {
        GetTotalCost();
    }

    protected void btnGetTotal_Click(object sender, EventArgs e)
    {
        GetTotalCost();
    }

    #endregion

    #region Process Registration

    private void ProcessRegistration()
    {
        int formId = Convert.ToInt32(Request.QueryString["Id"]);


        RegistrationForm = new RegistrationForm(formId)
                               {
                                   Title = litTitle.Text
                               };

        int donationId = 0;

        if (GetTotalCost() > 0)
        {
            Donations donate = new Donations(Request.Url.ToString())
            {
                IPaddress = Request.GetOriginalHostAddress(),
                CardVerification = PaymentInfo.CardVerification,
                donation =
                {
                    CardName = PaymentInfo.CardName,
                    CardNumber = PaymentInfo.AccountNumber,
                    CardType = PaymentInfo.CardType,
                    CardExpMonth = PaymentInfo.MonthExpiration,
                    CardExpYear = PaymentInfo.YearExpiration,
                    FirstName = ContactInfo.FirstName,
                    LastName = ContactInfo.LastName,
                    Address = ContactInfo.Address,
                    City = ContactInfo.City,
                    State = ContactInfo.State,
                    Country = ContactInfo.Country,
                    Zip = ContactInfo.PostalCode,
                    Email = ContactInfo.Email,
                    Phone = ContactInfo.Phone,
                    Amount = (decimal)GetTotalCost(),
                    Designation = RegistrationForm.Title,
                    OtherDesignation = string.Empty,
                    Comments = PaymentInfo.PaymentComments,
                    Referrer = ViewState["UrlReferrer"] != null ? ViewState["UrlReferrer"].ToString() : Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : string.Empty,
                    FormUrl = Request.Url.ToString(),
                    Recurring = chkRecurring.Checked,
                    IPaddress = Request.GetOriginalHostAddress()
                }
            };

            if (chkRecurring.Checked)
            {
                donate.donation.RecurringInterval = 30;
                donate.donation.Recurring = true;
            }

            if (Request.Url.Host == "localhost" || Request.Url.Host.StartsWith("local") ||
                ContactInfo.Email == "heroic@gmail.com" || !RegistrationForm.ProcessPayment)
            {
                donate.donation.TestMode = true;
            }

            //// fraud validation Dec 2012
            //if (Request.UrlReferrer == null && ViewState["UrlReferrer"] == null)
            //{
            //    donate.TestMode = true;
            //    donate.WebConfirmationMessage += "(No referrer: Test mode.)";
            //}

            if (Request.GetOriginalHostAddress() == "2001:4800:7810:512:1e4d:174e:ff04:9e78")
            {
                donate.donation.TestMode = true;
                donate.WebConfirmationMessage += "(Invalid IP.)";
            }

            string registrationResult = donate.ProcessRegistration(RegistrationForm.Title);

            donationId = donate.donation.DonationID;

            SaveQuestionAnswers(donate.donation.DonationID);

            pnlResults.Visible = true;

            email = GenerateEmail();

            if (donate.Success || !RegistrationForm.ProcessPayment)
            {
                lblDonationThankYou.Text += donate.WebConfirmationMessage;
                lblPaymentMessage.Text = registrationResult;
                pnlResults.Visible = true;
                theForm.Visible = false;
            }
            else
            {
                lblDonationThankYou.Visible = false;
                divError.Visible = true;
                return;
            }
        }
        else
        {
            email = GenerateEmail();
            // hide form
            theForm.Visible = false;
        }



        string errorMsg = string.Empty;
        string subject = DataFormat.StripHTML(RegistrationForm.Title);

        try // Send to Guest
        {
            EmailHelper.SendMail("payments@freecapitalists.org", ContactInfo.Email, subject, email, RegistrationForm.SendConfirmationAsHtml);
        }
        catch (Exception ex)
        {
            errorMsg += " There was an error sending a confirmation email to " + ContactInfo.Email + ". <br />" +
                        Environment.NewLine + ex;
            lblPaymentMessage.Text += errorMsg;

            ExceptionLogging.LogException(Context, ex);
        }
        // Send to Institute
        string sendTo = "payments@freecapitalists.org";
        if (!string.IsNullOrWhiteSpace(RegistrationForm.EmailAddress))
        {
            sendTo = RegistrationForm.EmailAddress;
        }

        try
        {

            if (donationId > 0)
            {
                email = string.Format(@"View more details at http://mises.freecapitalists.org//Manager/Registration.aspx?Id={0}

{2}
                {1}", donationId, email, Environment.NewLine);
            }

            EmailHelper.SendMail("payments@freecapitalists.org", sendTo, subject, email + errorMsg, RegistrationForm.SendConfirmationAsHtml);


            //   EmailHelper.SendMail("webmaster@freecapitalists.org", sendTo, subject, email + errorMsg);
        }
        catch (Exception ex)
        {
            errorMsg += " There was an error sending this form.  Please contact us to confirm registration." +
                        Environment.NewLine;

            EmailHelper.SendMail("webmaster@freecapitalists.org", "webmaster@freecapitalists.org", "Mises Form Error", ex.Message);

            lblPaymentMessage.Text += errorMsg;
        }


        // Show Confirmation Dialog
        pnlFormDetail.Visible = false;
        pnlCustomAmount.Visible = false;
        PaymentInfo.Visible = false;
        ContactInfo.Visible = false;
        btnSubmitRegistration.Visible = false;
        pnlResults.Visible = true;
    }

    private void SaveQuestionAnswers(int donationId)
    {
        var model = DataHelper.EntityDataModel;

        Questions.ForEach(question => model.RegistrationQuestionAnswers.AddObject(new RegistrationQuestionAnswer()
            {
                QuestionId = question.QuestionId,
                DonationId = donationId,
                FormId = FormId,
                Answer = question.Answer,
                CreateDate = DateTime.Now
            }));

        Products.ForEach(product => model.RegistrationProductSelections.AddObject(new RegistrationProductSelection()
        {
            ProductId = product.ProductId,
            DonationId = donationId,
            FormId = FormId,
            Quantity = product.Quantity,
            Price = (decimal?)product.Price,
            CreateDate = DateTime.Now
        }));

        model.SaveChanges();

    }


    public string GenerateEmail()
    {
        string strSelections = Environment.NewLine + Environment.NewLine;
        strSelections += "------------------------" + Environment.NewLine;
        // Add Products
        foreach (Product product in Products.Distinct())
        {
            strSelections += "Product: " + product.ProductName + Environment.NewLine;
            if (product.Quantity > 1)
                strSelections += "Quantity: " + product.Quantity + Environment.NewLine;
            if (product.Price > 1)
                strSelections += "Price: " + product.Price.ToString("C") + Environment.NewLine;
            strSelections += "------------------------" + Environment.NewLine;
        }
        // Add Questions
        foreach (Question question in Questions.Distinct().Where(question => !string.IsNullOrWhiteSpace(question.Answer)))
        {
            strSelections += "Q: " + question.QuestionText + Environment.NewLine;
            strSelections += "A: " + question.Answer + Environment.NewLine;
            strSelections += "------------------------" + Environment.NewLine;
        }

        string strMsg = RegistrationForm.Title + Environment.NewLine + "Date: " +
                        DateTime.Today.ToLongDateString() + Environment.NewLine;


        if (RegistrationForm.EmailConfirmation != RegistrationForm.EmailConfirmation.StripHTML())
        {
            // this is a rich text email
            RegistrationForm.SendConfirmationAsHtml = true;
        }
        strMsg += RegistrationForm.EmailConfirmation;



        try
        { // try is for for unit testing

            if (PaymentInfo.Visible || GetTotalCost() > 0 || !string.IsNullOrWhiteSpace(txtCustomAmount.Text))
            {
                strMsg += "Payment Comments: " + PaymentInfo.PaymentComments + Environment.NewLine + Environment.NewLine;
                strMsg += "Total to charge CC: " +
                          Strings.FormatCurrency(GetTotalCost(), 2, TriState.UseDefault, TriState.UseDefault,
                                                 TriState.UseDefault) + Environment.NewLine + Environment.NewLine;
            }

            if (pnlCustomAmount.Visible || !string.IsNullOrWhiteSpace(txtCustomAmount.Text))
            {
                try
                {
                    strMsg += "User-specified payment: " +
              Strings.FormatCurrency(txtCustomAmount.Text, 2, TriState.UseDefault, TriState.UseDefault,
                                     TriState.UseDefault);
                }
                catch (Exception)
                {


                }

            }



            strMsg += "--------" + Environment.NewLine + Environment.NewLine;

            strMsg += "First Name                    : " + ContactInfo.FirstName + Environment.NewLine;
            strMsg += "Last Name                    : " + ContactInfo.LastName + Environment.NewLine;
            strMsg += "Address                 : " + ContactInfo.Address + Environment.NewLine;
            strMsg += "City                    : " + ContactInfo.City + Environment.NewLine;
            strMsg += "State                   : " + ContactInfo.State + Environment.NewLine;
            strMsg += "Zip                     : " + ContactInfo.PostalCode + Environment.NewLine;
            strMsg += "Country                 : " + ContactInfo.Country + Environment.NewLine;
            strMsg += "Daytime Phone           : " + ContactInfo.Phone + Environment.NewLine;
            strMsg += "E-mail                  : " + ContactInfo.Email + Environment.NewLine;
            if (PaymentInfo.Visible || GetTotalCost() > 0 || PaymentInfo.Amount > 0)
            {


                strMsg += "Amount Charged        : " + GetTotalCost() + Environment.NewLine;
                strMsg += "Credit Card Name        : " + PaymentInfo.CardName + Environment.NewLine;
                strMsg += "Credit Card Type        : " + PaymentInfo.CardType + Environment.NewLine;
                // Send full number if not processing now

                //If IsInternal = True Then
                //    strMsg &= "Credit Card Number       : " & Me.PaymentInfo.AccountNumber & vbCrLf
                //Else
                //    strMsg &= "Credit Card Number       : xxxxxxxxxxx" & Right(Me.PaymentInfo.AccountNumber, 4) & vbCrLf
                //End If

                strMsg += "Credit Card Number       : xxxxxxxxxxx" +
                          PaymentInfo.AccountNumber.Substring(PaymentInfo.AccountNumber.Length - 4) + Environment.NewLine;

                strMsg += "Expiration Date         : " + PaymentInfo.MonthExpiration + " / " + PaymentInfo.YearExpiration +
                          Environment.NewLine;
                strMsg += "Name on Card            : " + PaymentInfo.CardName + Environment.NewLine + Environment.NewLine;

                if (chkRecurring.Checked)
                {
                    strMsg += "Make this payment recurring." + Environment.NewLine;
                }

            }
        }
        catch
        {
            // for unit testing
        }

        strMsg += strSelections + Environment.NewLine;

        if (RegistrationForm.SendConfirmationAsHtml)
        {
            strMsg = strMsg.Replace(Environment.NewLine, "<br />");
        }

        return strMsg;
    }

    /// <summary>
    ///   Gets the total cost.
    ///   For each selected product, get the name, cost, and quantity
    /// </summary>
    /// <value>The total cost.</value>
    /// David
    /// 1/26/2006
    public double GetTotalCost()
    {
        _currentSum = 0M;

        GetProductTotal();

        // if (Page.IsPostBack) // there is a bug that causes checked to show as true even though it's false

        if (!string.IsNullOrWhiteSpace(txtCustomAmount.Text))
        {
            _currentSum += Convert.ToDecimal(txtCustomAmount.Text);
        }

        if (ddDonationAmount.Visible)
            _currentSum += Convert.ToDecimal(ddDonationAmount.SelectedValue);

        PaymentInfo.Amount = (double)_currentSum;
        return (double)_currentSum;
    }

    private List<Question> Questions
    {
        get
        {
            try
            {
                if (_questions.Count > 0) return _questions;

                foreach (var question in from DataListItem item in dlQuestions.Items
                                         select new Question
                                                    {
                                                        QuestionId =
                                                            Convert.ToInt32(
                                                                ((Literal)(item.FindControl("QuestionIdLiteral"))).Text),
                                                        QuestionText =
                                                            ((Literal)(item.FindControl("QuestionLiteral"))).Text,
                                                        Answer = ((TextBox)(item.FindControl("txtAnswer"))).Text,
                                                    })
                {
                    // TODO: Required Field Check
                    _questions.Add(question);
                }
            }
            catch
            {
            }
            return _questions;
        }
    }

    private List<Product> Products
    {
        get
        {
            if (_products.Count > 0) return _products;

            GetProductTotal();

            return _products;
        }
    }

    private decimal GetProductTotal()
    {
        if (dlProducts == null) return 0;

        _currentSum = 0M;
        foreach (DataListItem item in dlProducts.Items)
        {
            var chkPrice = (CheckBox)(item.FindControl("chkPrice"));
            var txtQuantity = (TextBox)(item.FindControl("txtQuantity"));

            var product = new Product
                              {

                                  ProductName = ((Literal)(item.FindControl("lblProductName"))).Text,
                                  Price = Double.Parse(chkPrice.Text, NumberStyles.Any),
                                  ProductId = Convert.ToInt32(((Literal)(item.FindControl("ProductId"))).Text),
                                  Quantity = Convert.ToDecimal(Conversion.Val(txtQuantity.Text))
                              };

            if (txtQuantity.Visible)
            {
                chkPrice.Checked = product.Quantity > 0;
            }

            if (chkPrice.Checked && product.Price > 0 && product.Quantity > 0M)
            {
                _currentSum += (decimal)(int.Parse(txtQuantity.Text) * Double.Parse(chkPrice.Text, NumberStyles.Any));
            }

            if (product.Quantity > 0 && chkPrice.Checked)
            {
                _products.Add(product);
            }

        }

        return _currentSum;
    }

    #endregion
}