#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Data;

#endregion

public partial class search : Page
{
    private int timeOut;

    public search()
    {
        Init += Page_Init;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        timeOut = Server.ScriptTimeout;
        Server.ScriptTimeout = 3600;

        btnSearch.Click += btnSearch_Click;
        Load += Page_Load;
        Unload += Page_Unload;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        Server.ScriptTimeout = timeOut;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        if (!String.IsNullOrWhiteSpace(Request.QueryString["q"]))
        {
            txtSearchField.Text = DataFormat.StripHTML(Request.QueryString["q"]);
            btnSearch_Click(null, null);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearchField.Text == string.Empty)
        {
            return;
        }
        string[] str = {"IDfield"};
        int i = 0;
        string litTOC = "<strong><ul>";

        //var db = new MisesDBDataContext((new System.Data.SqlClient.SqlConnection(BusinessBase.ConnectionString)));
        //IMultipleResults searchResults = db.SearchSite(txtSearchField.Text);

        DataSet ds;
        using (
            ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                          "dbo.SearchSite", new SqlParameter("@terms", txtSearchField.Text)))
        {
            foreach (DataTable table in ds.Tables.Cast<DataTable>().Where(table => table.Rows.Count != 0))
            {
                i += 1;
                litTOC += "<li><a href=#" + i + ">" + table.Rows.Count + " " + table.Rows[0][0] +
                          " found.</a></li>";

                var field = new HyperLinkField
                                {
                                    HeaderText = table.Rows[0][0].ToString(),
                                    DataTextField = "Title",
                                    DataNavigateUrlFields = str,
                                    DataNavigateUrlFormatString = Convert.ToString(table.Rows[0][1])
                                };

                var grid = new GridView();
                grid.Columns.Add(field);
                grid.ControlStyle.CopyFrom(emptyGrid.ControlStyle);
                grid.HeaderStyle.CopyFrom(emptyGrid.HeaderStyle);
                grid.RowStyle.CopyFrom(emptyGrid.RowStyle);
                grid.AllowSorting = false;
                grid.SkinID = "Black";
                grid.UseAccessibleHeader = false;
                grid.AutoGenerateColumns = false;
                grid.DataSource = table;
                grid.DataBind();
                var lit = new Literal {Text = "<a name=\"" + i + "\"><br/><hr/><br/></a>"};
                PlaceHolder1.Controls.Add(lit);
                PlaceHolder1.Controls.Add(grid);
            }
        }

        var lit2 = new Literal {Text = litTOC + "</ul></strong><br />"};
        plcContents.Controls.Add(lit2);

        if (PlaceHolder1.Controls.Count == 0)
        {
            lit2.Text = litTOC + "<h3>No results found.</h3>";
        }
    }
}