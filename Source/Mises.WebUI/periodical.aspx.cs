#region

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Domain.Periodicals;
using GridView = Mises.WebControls.GridView;

#endregion

partial class periodical : Page
{
    private string _currentSeason;
    private string currentMonth;

    public SortDirection SortDirection
    {
        get
        {
            return ViewState["SortDirection"] != null
                       ? (SortDirection) (ViewState["SortDirection"])
                       : SortDirection.Ascending;
        }
        set { ViewState["SortDirection"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        gvMonthlyArchives.PageIndexChanging += gvMonthlyArchives_PageIndexChanging;
        gvSeasonalArchives.PageIndexChanging += gvSeasonal_PageIndexChanged2;
        gvLibrary.PageIndexChanging += gvLibrary_PageIndexChanged2;
        gvMonthlyArchives.Sorting += gv_SortCommand;
        gvSeasonalArchives.Sorting += gvSeasonal_SortCommand;
        gvLibrary.Sorting += gvLibrary_Sorting;
        gvMonthlyArchives.RowDataBound += gvMonthlyArchives_RowDataBound;
        gvSeasonalArchives.RowDataBound += gvSeasonal_ItemDataBound;
        btnSearch.Click += btnSearch_Click;

        if (! Page.IsPostBack)
        {
            BindPeriodicals();
        }
    }

    private void BindPeriodicals()
    {
        int PeriodicalId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        if (PeriodicalId == 0)
        {
            Response.RedirectPermanent("/page/1450/Books-and-Periodicals-Available-on-Misesorg/",true);
        }

        Periodical periodical = new Periodical(PeriodicalId);


        if (periodical.PeriodicalId == 0)
        {
            return;
        }

        if (! string.IsNullOrEmpty(periodical.Logo))
        {
            imgLogo.Src = periodical.Logo;
            imgLogo.Alt = periodical.Title + " Logo";
        }
        else
        {
            imgLogo.Visible = false;
        }
        lnkImg.HRef = "/periodical.aspx?Id=" + periodical.PeriodicalId;
        lnkImg.Title = periodical.Title;

        Page.Header.Title = periodical.Title;
        Page.Header.Keywords = periodical.metaKeywords;
        Page.Header.Description = periodical.metaDescription;

        lblDescription.Text = periodical.Description;
        GridView gv = GetGrid(periodical.PeriodicalId);
        DataSet ds = new DataSet();

        if (! (string.IsNullOrEmpty(Request.QueryString["search"])) ||
            ! (string.IsNullOrEmpty(Request.QueryString["author"])))
        {
            txtTitle.Text = Request.QueryString["search"];
            txtAuthor.Text = Request.QueryString["author"];
            btnSearch_Click(null, null);
            return;
        }

        if (! (string.IsNullOrEmpty(Request.QueryString["volume"])))
        {
            string volume = Request.QueryString["volume"];
            gv.DataSource = periodical.GetPeriodicalArchives(volume);
            gv.DataBind();
            return;
        }

        switch (Request.QueryString["action"])
        {
            case "authors":
                if (gv.SortExpressionAlt != "")
                {
                    ds = periodical.GetPeriodicalAuthors();
                    ds.Tables[0].DefaultView.Sort = gv.SortExpressionAlt +
                                                    ((gv.SortDirectionAlt == SortDirection.Descending) ? " DESC" : "");
                    gv.DataSource = ds.Tables[0].DefaultView;
                }
                else
                {
                    ds = periodical.GetPeriodicalAuthors();
                    ds.Tables[0].DefaultView.Sort = gv.SortExpressionAlt +
                                                    ((gv.SortDirectionAlt == SortDirection.Descending) ? " DESC" : "");
                    gv.DataSource = ds.Tables[0].DefaultView;
                }
                gv.Columns[3].HeaderText = "Last Periodical";
                gv.Columns[1].Visible = false;
                gv.PageSize = 500;
                gv.EnableViewState = false;
                gv.DataBind();
                //Case "title"

                break;
            default: // Date
                // Default: Show Archives by Month
                if (gv.SortExpressionAlt != "")
                {
                    ds = periodical.GetPeriodicalArchives();
                    ds.Tables[0].DefaultView.Sort = gv.SortExpressionAlt +
                                                    ((gv.SortDirectionAlt == SortDirection.Descending) ? " DESC" : "");
                    gv.DataSource = ds.Tables[0].DefaultView;
                }
                else
                {
                    gv.DataSource = periodical.GetPeriodicalArchives();
                }
                gv.DataBind();
                break;
        }
    }

    /// <summary>
    ///   Gets the grid to used by the Periodical Id
    /// </summary>
    /// <param name = "PeriodicalId">The periodical id.</param>
    /// <returns></returns>
    private GridView GetGrid(int PeriodicalId)
    {
        GridView gv;
        switch (PeriodicalId)
        {
            case 1:
            case 7:
                gv = gvMonthlyArchives;
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                gv = gvSeasonalArchives;
                break;
            default:
                gv = gvLibrary;
                lblSubject.Visible = true;
                txtSubject.Visible = true;
                btnSearch.Text = "Search Library";
                break;
        }
        return gv;
    }

    protected void gvMonthlyArchives_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMonthlyArchives.PageIndex = e.NewPageIndex;
        BindPeriodicals();
    }

    private void gvSeasonal_PageIndexChanged2(object source, GridViewPageEventArgs e)
    {
        gvSeasonalArchives.PageIndex = e.NewPageIndex;
        BindPeriodicals();
    }

    private void gvLibrary_PageIndexChanged2(object source, GridViewPageEventArgs e)
    {
        gvLibrary.PageIndex = e.NewPageIndex;
        BindPeriodicals();
    }


    private void gv_SortCommand(object source, GridViewSortEventArgs e)
    {
        BindPeriodicals();
    }

    private void gvSeasonal_SortCommand(object source, GridViewSortEventArgs e)
    {
        BindPeriodicals();
    }


    protected void gvLibrary_Sorting(object sender, GridViewSortEventArgs e)
    {
        BindPeriodicals();
    }

    protected void gvMonthlyArchives_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Conversion.Val(Request.QueryString["Id"]) == 7)
        {
            return;
        }

        // Add Monthly Breaks
        if (Request.QueryString["action"] == null && e.Row.RowType == DataControlRowType.DataRow) // if title view
        {
            string activeMonth = gvMonthlyArchives.DataKeys[e.Row.RowIndex].Value.ToString();
            if (currentMonth != activeMonth)
            {
                currentMonth = activeMonth;
                HyperLink link = new HyperLink();
                Literal lit = new Literal();

                link = (HyperLink) (e.Row.Cells[1].FindControl("HyperLink1"));
                string archives = "</td></tr>" + Environment.NewLine + "<tr><th colspan=\"5\">" + Environment.NewLine;
                archives += "<h4>" + currentMonth + " " + Convert.ToDateTime(e.Row.Cells[2].Text).Year + "</h4>" +
                            Environment.NewLine;
                archives += "</th></tr>" + Environment.NewLine + "<tr><td>" + Environment.NewLine;
                lit.Text = archives;
                e.Row.Cells[0].Controls.Add(lit);
                e.Row.Cells[0].Controls.Add(link);
            }
        }
    }

    private void gvSeasonal_ItemDataBound(object sender, GridViewRowEventArgs e)
    {
        // Add Seasonal Breaks
        if (Request.QueryString["action"] == null && e.Row.RowType == DataControlRowType.DataRow) // if title view
        {
            string currentSeason = gvSeasonalArchives.DataKeys[e.Row.RowIndex].Value.ToString();
            if (_currentSeason != currentSeason)
            {
                _currentSeason = currentSeason;
                HyperLink link = (HyperLink) (e.Row.Cells[0].FindControl("HyperLink1"));
                Literal lit = new Literal();

                string archives = "</td></tr>" + Environment.NewLine + "<tr><th colspan=\"5\">" + Environment.NewLine;
                archives += "<h4>" + currentSeason + "</h4>" + Environment.NewLine;
                archives += "</th></tr>" + Environment.NewLine + "<tr><td>" + Environment.NewLine;
                lit.Text = archives;
                e.Row.Cells[0].Controls.Add(lit);
                e.Row.Cells[0].Controls.Add(link);
            }
        }
    }


    /// <summary>
    ///   Search Periodicals
    /// </summary>
    /// <param name = "sender">The source of the event.</param>
    /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
    private void btnSearch_Click(object sender, EventArgs e)
    {
        int PeriodicalId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        if (PeriodicalId == 0)
        {
            return;
        }

        Periodical Periodical = new Periodical(PeriodicalId);
        System.Web.UI.WebControls.GridView gv = GetGrid(PeriodicalId);

        gv.PageIndex = 0;
        string title = Server.HtmlEncode(txtTitle.Text.Trim());
        string author = Server.HtmlEncode(txtAuthor.Text.Trim());
        string subject = Server.HtmlEncode(txtSubject.Text.Trim());
        gv.DataSource = Periodical.Search(title, author, subject);
        gv.DataBind();

        if (Periodical.results > 0)
        {
            litTitle.Text = string.Format("{0} Search: {1} results.", Periodical.Title, Periodical.results);
            gv.Visible = true;
        }
        else
        {
            litTitle.Text = string.Format("Sorry, no results for \"{0} {1} {2}\"", title, author, subject);
            gv.Visible = false;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}