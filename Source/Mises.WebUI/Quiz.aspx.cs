﻿#region

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Mises.Domain.Quiz;

#endregion

public partial class Quiz : Page
{
    private Mises.Domain.Quiz.Quiz quiz;

    protected void Page_Load(object sender, EventArgs e)
    {
        repQuestions.ItemDataBound += Question_DataBound;
        quiz = new Mises.Domain.Quiz.Quiz
                   {
                       QuizID =
                           Request.QueryString["QuizId"] != null ? Convert.ToInt32(Request.QueryString["QuizID"]) : 4
                   };
        quiz.GetQuiz();

        lblNumTaken.Text = String.Format("({0:0,0} have taken this quiz.)", quiz.TimesTaken);

        if (!IsPostBack)
        {
            repQuestions.DataSource = quiz.Questions;
            repQuestions.DataBind();

            lblTitle.Text = quiz.QuizTitle;
            Title = String.Format("{0} :: Mises Institute", quiz.QuizTitle);
            litDescription.Text = quiz.Description;
            lblEmailDirections.Text = quiz.EmailPrompt;
            btnSubmitQuiz.Text = quiz.SubmitQuizLabel;

            if (!string.IsNullOrEmpty(quiz.Language))
            {
                Page.Header.Controls.Add(new HtmlMeta {Name = "content-language", Content = quiz.Language});

                Page.UICulture = quiz.Language;

                switch (quiz.Language)
                {
                    case "fr":
                        Page.Culture = "fr-FR";
                        break;
                    case "es":
                        Page.Culture = "es-MX";
                        break;
                }
            }
        }
    }

    private void Question_DataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // Bind Questions
            int questionId = ((Question) e.Item.DataItem).QuestionId;

            var answers = (RadioButtonList) e.Item.FindControl("rdoAnswers");
            answers.DataSource = quiz.Answers.Where(answer => answer.QuestionId == questionId);
            answers.DataBind();
        }
    }

    protected void btnSubmitQuiz_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in repQuestions.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                // Get the answer
                var answers = (RadioButtonList) item.FindControl("rdoAnswers");

                if (answers.SelectedItem != null)
                {
                    int answerId = Convert.ToInt32(answers.SelectedItem.Value);
                    quiz.SelectedAnswers.Add(answerId);

                    // Get value of this Answerid
                    Answer answer = (quiz.Answers.Where(a => a.AnswerId == answerId)).Single();

                    quiz.Score += answer.AnswerValue;
                    item.Controls.AddAt(2, new Literal
                                               {
                                                   Text =
                                                       String.Format("<br /><em style=\"color:Red;\">{0} {1}</em>",
                                                                     quiz.YourAnswerLabel, answer.AnswerExplanation)
                                               });
                }
                else
                {
                    Validate();
                    return;
                }
            }
        }
        litDescription.Text = string.Format("<h2>{0}</h2>", quiz.ScoreText);
        quiz.SendResultEmail(txtEmailAdress.Text);

        quiz.IncrementNumTaken();
    }
}