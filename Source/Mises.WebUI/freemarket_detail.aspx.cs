﻿#region

using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Data;


#endregion

public partial class freemarket_detail : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int control = 0;
        try
        {
            control = Convert.ToInt32(Conversion.Val(Request.QueryString["control"]));
        }
        catch (Exception)
        {
            Response.Redirect("/");
        }

        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
        var freemarket = from fm in db.FreeMarketTBs where fm.control == control select fm;
        var issue = freemarket.SingleOrDefault();

        if (issue == null) return;

        // If this is a pdf, redirect to it
        if (issue.PDFurl != "")
        {
            Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + "/" + issue.PDFurl);
        }
        Page.Title = "The Free Market: " + Convert.ToString(issue.title);
        lblBody.Text = Convert.ToString(issue.body);

        Bookmark1.Bind(Request.Url.ToString(), Page.Title, Page.Title);
        if (TagCloud != null)
        {
            TagCloud.DocumentIdentifier = new Guid(issue.GUID.ToString());
        }
    }
}