#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Utility;

#endregion

partial class MyMises_login : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //On Error Resume Next

        const string CookieName = "ABLECOMMERCE1";

        //If URL Tokens are not found and Cookie is available, check for Cookie Tokens

        AddText("IsAuthenticated: " + User.Identity.IsAuthenticated);
        AddText("User.Identity.Name: " + User.Identity.Name);
        //  AddText("Value Count: " & Request.Cookies(CookieName).Values.Count.ToString)
        try
        {
            AddText("Val(Request.Cookies(CookieName)(\"UserID\")): " +
                    Conversion.Val(Request.Cookies[CookieName]["UserID"]));
        }
        catch (Exception ex)
        {
            ExceptionLogging.LogException(Context, ex);
            AddText("Val(Request.Cookies(CookieName)(\"UserID\")): " + ex);
        }


        if (Context.Request.Url.ToString().Contains("org/store/"))
        {
            AddText("Store!");
            return;
        }
        if (Context.User != null && Context.User.Identity.IsAuthenticated)
        {
            AddText("Context.User.Identity.IsAuthenticated");
            return;
        }

        if (Request.Cookies[CookieName] != null && Conversion.Val(Request.Cookies[CookieName]["UserID"]) > 0)
        {
            AddText("Get UserId...");

            var UserId = (int) Conversion.Val(Request.Cookies[CookieName]["UserID"]);
            if (UserId == 0)
            {
                return;
            }
            string UserName = "";
            int GroupId = 0;
            using (
                SafeDataReader reader =
                    SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["MisesShop"].ConnectionString,
                                            CommandType.StoredProcedure, "dbo.spGetUserName",
                                            new SqlParameter("@UserId", UserId)))
            {
                while (reader.Read())
                {
                    UserName = reader.GetString("UserName");
                    GroupId = reader.GetInt32("Group_Id");
                }
            }

            AddText("username:" + UserName);
            AddText("groupid:" + GroupId);


            if (UserName != "ANONYMOUS")
            {
                AddText("NOT ANONYMOUS");
                if (GroupId == 1 || GroupId == 8)
                {
                    AddText("SETTING ADMIN");
                    Response.Cookies[CookieName]["IsAdmin"] = "True";
                }
                AddText("SETTING AUTH");
                FormsAuthentication.SetAuthCookie(UserName, true);
                AddText("REDIRECT");
                FormsAuthentication.RedirectFromLoginPage(UserName, true);
                //Response.Redirect(Context.Request.RawUrl.ToString)
            }
            else
            {
                AddText("IS ANONYMOUS");
                Response.Cookies[CookieName]["UserID"] = "0";
            }
        }

        //  System.Web.Security.FormsAuthentication.SetAuthCookie(Request.Cookies(CookieName)("UserID"), True)


        AddText("Response.Cookies(" + CookieName + ")(UserID)=" + Request.Cookies[CookieName]["UserID"]);
        AddText("Response.Cookies(" + CookieName + ")(SessionID)=" + Request.Cookies[CookieName]["SessionID"]);
        AddText("Response.Cookies(" + CookieName + ")(UserName)=" + Request.Cookies[CookieName]["UserName"]);
        if (Request.Cookies[CookieName]["UserID"].Length > 0 & Request.Cookies[CookieName]["SessionID"].Length > 0)
        {
            AddText("Cookie Tokens Found");
            if (Information.IsNumeric(Request.Cookies[CookieName]["UserID"]))
            {
                AddText("Cookie Tokens Valid Format");
                //  blnCookieTokens = True
                // strSessionID = Request.Cookies(CookieName)("SessionID")
                // intUserID = Request.Cookies(CookieName)("UserID")
            }
            else
            {
                AddText("Cookie Tokens Are Not Valid Format");
                //  blnCookieTokens = False
            }
        }
        else
        {
            AddText("Cookie Tokens Not Found");
            //  blnCookieTokens = False
        }
    }

    private void AddText(string text)
    {
        litDebugText.Text += Environment.NewLine + text + "<br/>" + "<br/>";
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}