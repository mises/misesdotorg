<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false" Inherits="MyMises_Default" Title="My Mises" MaintainScrollPositionOnPostback="true" Codebehind="default.aspx.cs" %>

<%@ Register Src="../Controls/Tagging/EditTags.ascx" TagName="EditTags" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3>
        <asp:Label ID="lblPageTitle" runat="server"></asp:Label></h3>
    <h4>
        <asp:HyperLink ID="lnkSignIn" runat="server" NavigateUrl="/login.aspx?ReturnUrl=%2fmymises%2fdefault.aspx">Viewing 
            anonymous favorites. To view your favorites, sign in.</asp:HyperLink><br />
        <br />
        My Saved Favorites</h4>
    <asp:GridView ID="gvFavorites" runat="server" AllowSorting="True" 
                  AutoGenerateColumns="False" DataKeyNames="FavoriteId"
                  PageSize="50">
        <Columns>
            <%--<asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType" />--%>
            <asp:HyperLinkField DataNavigateUrlFields="URL" DataTextField="Title" HeaderText="Page"
                                SortExpression="Title" />
            <asp:BoundField DataField="CreateDate" HeaderText="Date" SortExpression="CreateDate" />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" SortExpression="FavoriteId" />
        </Columns>
    </asp:GridView>
    &nbsp;
    <asp:Button ID="btnViewAnon" runat="server" Text="View favorites of all anonymous users." />
    <h4>
        <uc2:EditTags ID="EditTags1" runat="server" />
    </h4>
    <h4>
        <br />
        <a href="/clouds.aspx">Mises Tag Clouds</a>
        <br />
        More features coming soon!<br />
        <br />
    </h4></asp:Content>