#region

using System;
using System.Threading;
using System.Web.UI;
using Mises.Domain.Utility;

#endregion

partial class MyMises_EmailPage : Page
{
    protected void btnSend_Click(object sender, EventArgs e)
    {
        // Send Current Page to a Friend
        string message = null;
        string SendTo = null;
        string SendFrom = null;
        SendTo = txtEmail.Text;
        SendFrom = "noreply@freecapitalists.org";

        message =
            string.Format(
                "(You are receiving this link because {0} visited mises.org and sent this link to you.  You have not been added to a mailing list.)",
                txtName.Text);
        message += Environment.NewLine + Environment.NewLine;

        message += txtURL.Text + Environment.NewLine;

        if (string.IsNullOrEmpty(SendTo))
        {
            return;
        }

        try
        {
            EmailHelper.SendMail(SendFrom, SendTo, "Mises.org link: " + txtTitle.Text, message, false);
        }
        catch (Exception)
        {
            Thread.Sleep(1000);
            EmailHelper.SendMail(SendFrom, SendTo, "Mises.org link: " + txtTitle.Text, message, false);
        }
        lblTitle.Text = "Message sent!";
        Response.Write("<script language=\"JavaScript\">window.close();</script>");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        txtTitle.Text = Request.QueryString["title"];
        txtURL.Text = Request.QueryString["url"];

        if (txtURL.Text == string.Empty)
        {
            Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        btnSend.Click += btnSend_Click;
        Load += Page_Load;
    }
}