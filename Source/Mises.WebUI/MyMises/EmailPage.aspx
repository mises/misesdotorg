<%@ Page Language="C#" AutoEventWireup="false" Inherits="MyMises_EmailPage" Codebehind="EmailPage.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Tell Others</title>
        <link href="/css/screen.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="MisesEmailPopup">
            <form id="form1" runat="server">
                <div class="logo"><span>LvMI</span></div>
                <h2>
                    <asp:Literal ID="Label2" runat="server" Text="Send Page By Email"></asp:Literal>
                </h2>
                <div class="box featured">
                    <div class="content">
                        <p><asp:Label id="lblTitle" runat="server" Text="Title:" AssociatedControlID="txtTitle"></asp:Label><asp:TextBox ID="txtTitle" runat="server" Columns="60"></asp:TextBox></p>
			
                        <p><asp:Label id="lblURL" runat="server" Text="URL:" AssociatedControlID="txtURL"></asp:Label><asp:TextBox ID="txtURL" runat="server" Columns="60" ReadOnly="True"></asp:TextBox></p>
			
                        <p><label id="lblName" for="txtName">
                               Your Name:</label><asp:TextBox ID="txtName" runat="server" Columns="60"></asp:TextBox></p>
			
                        <p><label id="lblEmail" for="txtEmail">
                               Friends Email:</label><asp:TextBox ID="txtEmail" runat="server" Columns="60"></asp:TextBox></p>
			
                        <p><asp:Button ID="btnSend" runat="server" Text="Send Page" /></p>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName">
                            Your name/email is required.</asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required.">
                            Friends email is required.</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            Enter a valid email.</asp:RegularExpressionValidator>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>