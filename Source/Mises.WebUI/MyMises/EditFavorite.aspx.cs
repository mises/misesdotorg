#region

using System;
using System.Web.UI;
using Mises.Domain.Membership;
using Mises.Domain.Utility;

#endregion

partial class MyMises_EditFavorite : Page
{
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Favorites.SaveFavorite(txtURL.Text, txtTitle.Text, Context.User.Identity.Name, Request.GetOriginalHostAddress());
        Response.Write("<script type=\"text/javascript\" language=\"JavaScript\">window.close();</script>");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        txtTitle.Text = Request.QueryString["title"];
        txtURL.Text = Request.QueryString["url"];
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        btnSave.Click += btnSave_Click;
        Load += Page_Load;
    }
}