<%@ Page Language="C#" AutoEventWireup="false" Inherits="MyMises_EditFavorite" Codebehind="EditFavorite.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Edit Favorite | MyMises</title>
        <link href="/css/screen.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="MyMises">
            <form id="form1" runat="server">
                <div class="CommonSidebarInnerArea">
                    <div class="logo"><span>LvMI</span></div>
                    <h3 class="CommonSidebarHeader">
                        Save to <a href="/mymises/" target="_blank">MyMises</a></h3>
                    <div class="CommonSidebarContent">
                        <p>To view your saved favorites, go to <a href="/mymises/" target="_blank">
                                                                   MyMises</a>.</p>

                        <p><asp:Label ID="lblTitle" runat="server" Text="Title:" AssociatedControlID="txtTitle"></asp:Label><asp:TextBox ID="txtTitle" runat="server" Columns="60"></asp:TextBox></p>

                        <p><asp:Label ID="lblURL" runat="server" Text="URL:" AssociatedControlID="txtURL"></asp:Label><asp:TextBox ID="txtURL" runat="server" Columns="60"></asp:TextBox></p>
                        <p><asp:Button ID="btnSave" runat="server" Text="Save Page" /></p>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>