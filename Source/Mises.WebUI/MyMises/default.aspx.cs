#region

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Membership;
using Mises.Domain.Utility;

#endregion

partial class MyMises_Default : Page
{
    #region Properties

    public string SortField
    {
        get
        {
            if (ViewState["sort"] != null)
            {
                return Convert.ToString(ViewState["sort"]);
            }
            else
            {
                return "";
            }
        }
        set { ViewState["sort"] = value; }
    }

    public string SortDir
    {
        get
        {
            if (ViewState["sortDir"] != null)
            {
                return Convert.ToString(ViewState["sortDir"]);
            }
            else
            {
                return "";
            }
        }
        set { ViewState["sortDir"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        EditTags1.GetUserTags();
        if (Page.IsPostBack)
        {
            return;
        }

        if (! Context.User.Identity.IsAuthenticated)
        {
            //FormsAuthentication.RedirectToLoginPage()
            lnkSignIn.Visible = true;
            Bind();
        }
        else
        {
            lnkSignIn.Visible = false;
            lblPageTitle.Text = "Welcome " + User.Identity.Name;
            Bind();
        }
    }


    protected void gvFavorites_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Favorites.DeleteFavorite(Convert.ToInt32(gvFavorites.DataKeys[e.RowIndex].Value));
        Bind();
    }

    private void Bind()
    {
        string UserName = null;
        if (Context.User.Identity.IsAuthenticated)
        {
            UserName = Context.User.Identity.Name;
        }
        else
        {
            UserName = "NONE";
        }

        if (ViewState["Anon"] != null)
        {
            UserName = "";
            //tagging.GetUserTags(Context.User.Identity.Name, Request.UserHostAddress)
        }


        string IPAddress = Request.GetOriginalHostAddress();

        DataSet ds = Favorites.GetUserFavorites(UserName, IPAddress);
        ds.Tables[0].DefaultView.Sort = SortField;
        gvFavorites.DataSource = ds.Tables[0].DefaultView;
        gvFavorites.DataBind();
    }

    protected void btnViewAnon_Click(object sender, EventArgs e)
    {
        ViewState["Anon"] = true;
        Bind();
    }


    protected void gvFavorites_Sorting(object sender, GridViewSortEventArgs e)
    {
        SortField = e.SortExpression;
        if (SortDir == "ASC")
        {
            SortDir = "DESC";
        }
        else
        {
            SortDir = "ASC";
        }
        Bind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        gvFavorites.RowDeleting += gvFavorites_RowDeleting;
        btnViewAnon.Click += btnViewAnon_Click;
        gvFavorites.Sorting += gvFavorites_Sorting;
    }
}