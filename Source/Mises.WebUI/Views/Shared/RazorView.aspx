﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>"
         AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = ViewBag.Title;
    }

</script>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	<%=ViewBag.Title%>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% Html.RenderPartial((String) ViewBag._ViewName); %>
</asp:Content>