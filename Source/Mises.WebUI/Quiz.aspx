<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="true" Inherits="Quiz" Codebehind="Quiz.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></h1>
    <p>
        <asp:Label ID="lblNumTaken" runat="server" Text=""></asp:Label></p>
    <p>
        <asp:Literal runat="server" ID="litDescription"></asp:Literal>
    </p>
    <div id="Questions">
        <ol>
            <asp:Repeater ID="repQuestions" runat="server">
                <ItemTemplate>
                    <li>
                        <p class="Question">
                            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("QuestionText")%>'></asp:Label>
                        </p>
                        <asp:RequiredFieldValidator ID="val" runat="server" ErrorMessage="You must select an option."
                                                    ControlToValidate="rdoAnswers" SetFocusOnError="True"
                                                    Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <p class="Answers">
                            <asp:RadioButtonList ID="rdoAnswers" runat="server" DataValueField="AnswerId" DataTextField="AnswerText"
                                                 RepeatLayout="Flow">
                            </asp:RadioButtonList>
                        </p>
                        <div class="hr">
                            <hr />
                        </div>
                    </li>
                </ItemTemplate>
                <SeparatorTemplate>
                </SeparatorTemplate>
            </asp:Repeater>
        </ol>
    </div>
    <div id="submit">
        <p>
            <asp:Label ID="lblEmailDirections" runat="server" Text="If you would like the results of this quiz emailed to you, enter your email address in the space below:"></asp:Label>
        </p>
        <p>
            <label for="<%#txtEmailAdress.ClientID%>">
                Email:</label><asp:TextBox ID="txtEmailAdress" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter a valid email address."
                                            ControlToValidate="txtEmailAdress" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
            <asp:Button ID="btnSubmitQuiz" OnClick="btnSubmitQuiz_Click" CssClass="submit" runat="server"
                        Text="Submit Quiz" />
        </p>
    </div>
</asp:Content>