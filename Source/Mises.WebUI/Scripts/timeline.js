﻿ var tl;

 function onLoad() {
     var eventSource = new Timeline.DefaultEventSource(0);

     var theme = Timeline.ClassicTheme.create();
     theme.event.bubble.width = 320;
     theme.event.bubble.height = 220;

     var zones = [
         {
             start: "1700",
             end: "2100",
             magnify: 5,
             unit: Timeline.DateTime.DECADE
         },
         {
             start: "1800",
             end: "2100",
             magnify: 3,
             unit: Timeline.DateTime.YEAR,
             multiple: 5
         }
     ];
     var zones2 = [
         {
             start: "1700",
             end: "2100",
             magnify: 5,
             unit: Timeline.DateTime.DECADE
         },
         {
             start: "1800",
             end: "2100",
             magnify: 3,
             unit: Timeline.DateTime.YEAR,
             multiple: 10
         }
     ];

     var d = Timeline.DateTime.parseGregorianDateTime("1700");
     var bandInfos = [
         Timeline.createHotZoneBandInfo({
                 width: "75%",
                 intervalUnit: Timeline.DateTime.CENTURY,
                 intervalPixels: 250,
                 zones: zones,
                 eventSource: eventSource,
                 date: d,
                 timeZone: -6,
                 theme: theme
             }),
         Timeline.createHotZoneBandInfo({
                 width: "25%",
                 intervalUnit: Timeline.DateTime.CENTURY,
                 intervalPixels: 70,
                 zones: zones2,
                 eventSource: eventSource,
                 date: d,
                 showEventText: false,
                 trackHeight: 0.5,
                 trackGap: 0.2,
                 theme: theme
             })
     ];
     bandInfos[1].syncWith = 0;
     bandInfos[1].highlight = true;
     bandInfos[1].eventPainter.setLayout(bandInfos[0].eventPainter.getLayout());

     tl = Timeline.create(document.getElementById("tl"), bandInfos, Timeline.HORIZONTAL);
     tl.loadXML("Feeds/timelineFeed.ashx", function(xml, url) {
         eventSource.loadXML(xml, url);
     });

     setupFilterHighlightControls(document.getElementById("controls"), tl, [0, 1], theme);
 }

 var resizeTimerID = null;

 function onResize() {
     if (resizeTimerID == null) {
         resizeTimerID = window.setTimeout(function() {
             resizeTimerID = null;
             tl.layout();
         }, 500);
     }
 }