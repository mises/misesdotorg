﻿/// <reference path="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5-vsdoc.js"/>

window.log = function () {
    log.history = log.history || []; // store logs to an array for reference
    log.history.push(arguments);
    arguments.callee = arguments.callee.caller;
    if (this.console) console.log(Array.prototype.slice.call(arguments));
};
// make it safe to use console.log always
(function (b) {

    function c() {
    }

    for (var d = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","), a; a = d.pop(); ) b[a] = b[a] || c;
})(window.console = window.console || {});

// Email Anti-Spam

function themail(em, dmn, subj) {
    emailE = (em + '@' + dmn);
    document.write('<a href="mailto:' + emailE + '?subject=' + subj + '">' + emailE + '</a>');
}

// Daily Articles

var d = new Date();
var currentDay = d.setDate(d.getDate());

$(document).ready(function () {

    $(".datepicker").datepicker();
    $(".dialog").dialog();
    $(".button").button();


    $(".hpdprev").click(function () {
        $(".hpdnext").show();

        GetDaysArticles((new Date(currentDay).setDate(new Date(currentDay).getDate() - 1)), false);
    });

    $(".hpdnext").click(function () {
        GetDaysArticles((new Date(currentDay).setDate(new Date(currentDay).getDate() + 1)), true);
    });

    $("#tabs").tabs({ spinner: 'Loading..<img src="/css/images/ui-anim_basic_16x16.gif">' });
    $(".tabs").tabs({ spinner: 'Loading..<img src="/css/images/ui-anim_basic_16x16.gif">' });
    $("#tabs-community").tabs();


    $(".change").live('click', function () {
        videoPod();
        return false;
    });



    $("#media-formats").removeClass("no-js").tabs();

    $("#media-index .filters").accordion({ collapsible: true, autoHeight: false }).find("h3 a:first").click();

    // collapse if content is not used
    if ($("#AuthorId").val() == "" && $("#SubjectId").val() == "" && $("#CategoryId").val() == "") {
        $("#media-index .filters").accordion("activate", false);
    }

    $("#media-index").mediaSearch(false); //TODO ajax or not - make this a variable that is set on the server or from cookie?
    $(".detail.search").mediaSearch(false); //TODO and here


    if ($(".article-list").length > 0) {
        $.getScript("/Scripts/libs/jquery.jcarousel.js", function () {
            $(".article-list").carousel().closest(".no-js").removeClass("no-js").find(".pagination-container").hide();
        });
    }

    /*TODO if wanting an accordion...
    first wrap the following h3 & div sets with <div id="post-types" class="post-types" />
    $("#post-types").children("h3").wrapInner('<a href="#media-posts" />').end().accordion({ autoHeight: false, clearStyle: true, collapsible: true }).removeClass("no-js");
    */

    if ($(".article").find("a[rel=media], a[rel=video], a[rel=audio]").length > 0) {

        $.getScript("/Scripts/libs/jquery.colorbox-min.js", function () {

            $(".article").find("a[rel=media], a[rel=video], a[rel=audio]").colorbox({
                current: "{current} of {total}",
                fixed: true,
                initialWidth: 634,
                maxWidth: 634,
                title: function () { return "<span></span>"; },
                onOpen: function () { $(".slplayer, .audio-object-wrapper").hide(); },
                onComplete: function () {
                    $("#cboxLoadedContent").find(".slplayer").silverlightPlayer();
                },
                onCleanup: function () { $(".slplayer, .audio-object-wrapper").show(); }
            });

        });



    }

    $(".slplayer").silverlightPlayer();

    $("input.autocomplete").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: this.element.data("source"),
                dataType: "json",
                data: { startsWith: request.term },
                success: function (data) { response(data); }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            window.location = $(this).data("target-base") + "/" + ui.item.value;
            return false;
        },
        open: function () { $(".slplayer").hide(); },
        close: function () { $(".slplayer").show(); }
    });

    // Vasily's changes
    //    $("select.autocomplete").autocompleteCombobox();

    //    $("select.autosubmit").bind("autocompletecomboboxselected", function (event, data) {
    //    	window.location = data.item.value;
    //    	return true;
    //    });


    $("select.autocomplete").autocompleteCombobox().bind("autocompletecomboboxselected", function (event, data) {
        window.location = data.item.value;
        return false;
    });
    
    // temporary for 30 for 30 campaign:
    
    $('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_dlQuestions_ctl00_txtAnswer').hide();
    $('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_chkRecurring').click(function () {

        if ($('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_chkRecurring').val() == 'on' || Number($('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_PaymentInfo_lblGetCost').text().replace(/[^0-9\.]+/g,"")) >= 100) {
            $('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_dlQuestions_ctl00_txtAnswer').show();
        } else {
            $('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder1_dlQuestions_ctl00_txtAnswer').hide();
        }
    });


});

/*
* Silverlight playback functionality...
*/
$.fn.silverlightPlayer = function () {
    return this.each(function () {

        var self = this;

        var init = function () {
            var $cnt = $(self);
            var src = '/assets/wmvplayer.xaml';
            var cfg = {
                file: $cnt.data('file'),
                image: $cnt.data('image'),
                //windowless:'true', <- is said to be too costly (would help with zindex problems)
                height: parseInt($cnt.css("height")),
                width: parseInt($cnt.css("width"))
            };
            var ply = new jeroenwijering.Player(self, src, cfg);
        };

        if (typeof (jeroenwijering) === "undefined") {
            var gotten = 0;

            $.getScript('/Scripts/libs/silverlight.js', function () {
                gotten++;
                if (gotten == 2)
                    init();
            });

            $.getScript('/Scripts/libs/wmvplayer.js', function () {
                gotten++;
                if (gotten == 2)
                    init();
            });
        } else {
            init();
        }

        return;
    });
};

$.widget("ui.autocompleteCombobox", {
    _create: function () {
        var self = this,
            select = this.element.hide(),
            selected = select.children(":selected"),
            value = selected.val() ? selected.text() : "";
        var input = this.input = $("<input>")
            .insertAfter(select)
            .val(value)
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(select.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text)))
                            return {
                                label: text.replace(
                                        new RegExp(
                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                                $.ui.autocomplete.escapeRegex(request.term) +
                                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "<strong>$1</strong>"),
                                value: text,
                                option: this
                            };
                    }));
                },
                select: function (event, ui) {
                    ui.item.option.selected = true;
                    self._trigger("selected", event, {
                        item: ui.item.option
                    });
                },
                open: function () { $(".slplayer").hide(); },
                close: function () { $(".slplayer").show(); },
                change: function (event, ui) {
                    if (!ui.item) {
                        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                                valid = false;
                        select.children("option").each(function () {
                            if ($(this).text().match(matcher)) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if (!valid) {
                            // remove invalid value, as it didn't match anything
                            $(this).val("");
                            select.val("");
                            input.data("autocomplete").term = "";
                            return false;
                        }
                    }
                }
            })
            .addClass("ui-widget ui-widget-content ui-corner-left");

        input.data("autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };

        this.button = $("<button type='button'>&nbsp;</button>")
            .attr("tabIndex", -1)
            .attr("title", "Show All Items")
            .insertAfter(input)
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass("ui-corner-all")
            .addClass("ui-corner-right ui-button-icon")
            .click(function () {
                // close if already visible
                if (input.autocomplete("widget").is(":visible")) {
                    input.autocomplete("close");
                    return;
                }

                // work around a bug (likely same cause as #5265)
                $(this).blur();

                // pass empty string as value to search for, displaying all results
                input.autocomplete("search", "");
                input.focus();
            });
    },

    destroy: function () {
        this.input.remove();
        this.button.remove();
        this.element.show();
        $.Widget.prototype.destroy.call(this);
    }
});

/*
* Carousel functionality...
*/
$.fn.carousel = function () {

    var loadingmarkup = "<div class='jcarousel-loading'><div>loading...<div></div>";
    var errormarkup = "<div class='message-info ui-corner-all'><span class='icon'></span><p>Oops, something has gone wrong.</p></div>";

    function itemLoadCallback(carousel, state) {

        var $carousel = carousel.container;
        var startIndex = null;

        for (var i = carousel.first; i <= carousel.last; i++) {
            if (!carousel.locked && !carousel.has(i)) {

                // Just find and use the appropriate pagination link url...
                var $pageli = $carousel.find(".pagination-container:first li.active");
                if ($pageli.length == 0)
                    return;

                var ajaxurl = $pageli.removeClass("active")[state]().addClass("active").children("a").attr("href");
                if (typeof (ajaxurl) === "undefined")
                    return;

                carousel.lock();
                $carousel.append(loadingmarkup);
                startIndex = i;

                // Append something to identify the triggerer (so the appropriate response can be sent back down if more than one of these on the page)...
                ajaxurl += "?ajaxsrc=" + carousel.list.data("ajax-src");

                $.ajax({
                    url: ajaxurl,
                    cache: false,
                    dataType: "html"
                })
                    .success(function (response) {
                        if ((response == null) || (response.trim() === "")) {
                            $carousel.append(errormarkup);
                            $carousel.find(".jcarousel-prev, .jcarousel-next").one("click", function () { $carousel.find(".message-info").remove(); });
                        } else {
                            var i = startIndex;
                            var $new = $(response).filter("li");
                            $new.each(function () {
                                carousel.add(i, this);
                                i++;
                            });

                            //TODO make common with document ready
                            $new.find("a[rel=media], a[rel=video], a[rel=audio]").colorbox({
                                current: "{current} of {total}",
                                fixed: true,
                                initialWidth: 634,
                                maxWidth: 634,
                                title: function () { return "<span></span>"; },
                                onComplete: function () { $("#cboxLoadedContent").find(".slplayer").silverlightPlayer(); }
                            });
                        }
                    })
                    .error(function (x, y, z) {
                        $carousel.append(errormarkup);
                        $carousel.find(".jcarousel-prev, .jcarousel-next").one("click", function () { $carousel.find(".message-info").remove(); });
                    })
                    .complete(function () {
                        $carousel.find(".jcarousel-item-placeholder").remove().end().find(".jcarousel-loading").remove();
                        carousel.unlock();
                    });

            }
        }
    }

    ;

    return this.each(function () {
        var $list = $(this);

        $list.find("a.document-summary").live("click", function () {

            var carousel = $list.data('jcarousel');

            if (!carousel.locked) {
                carousel.lock();

                var $breakoutcontainer = $list.closest("#media-controller"); //TODO make generic - specify the breakout container
                $breakoutcontainer.append(loadingmarkup);

                // Just find and use the appropriate pagination link url...
                var ajaxurl = $(this).attr("href");

                $.ajax({
                    url: ajaxurl,
                    cache: false,
                    dataType: "html"
                })
                    .success(function (response) {
                        if ((response == null) || (response.trim() === "")) {
                            $list.append(errormarkup);
                            $list.find(".jcarousel-prev, .jcarousel-next").one("click", function () { $carousel.find(".message-info").remove(); });
                        } else {
                            $breakoutcontainer.children(".media.widget:first").replaceWith(response); //TODO make generic - see above
                            $breakoutcontainer.find(".slplayer").silverlightPlayer();
                            $breakoutcontainer.find(".flashplayer").flashPlayer();
                        }
                    })
                    .error(function (x, y, z) {
                        $list.append(errormarkup);
                        $list.find(".jcarousel-prev, .jcarousel-next").one("click", function () { $carousel.find(".message-info").remove(); });
                    })
                    .complete(function () {
                        $breakoutcontainer.children(".jcarousel-loading").remove();
                        carousel.unlock();
                    });
            }

            return false;
        });

        $list.jcarousel({
            size: $list.data("list-count"),
            itemLoadCallback: itemLoadCallback,
            vertical: $list.hasClass("jcarousel-list-vertical"),
            scroll: $list.data("scroll-amount")
        });
    });
};

/*
* Search functionality...
*/
$.fn.mediaSearch = function (viaAjax) {

    var ajaxSearch = function (data) {

        var markup = null;
        var fading = true;
        var waiting = true;
        var addmarkup = function () {
            $("#cse-media-results").empty().fadeIn(500).append(markup);
            $("#cse-media-loading").hide();
        };

        $("#cse-media-loading").show();
        $("#cse-media-results").fadeOut(500, function () {
            if (!waiting)
                addmarkup();
            else
                fading = false;
        });

        $.ajax({
            url: "/media/searchresults",
            cache: false,
            data: data,
            dataType: "html"
        })
            .success(function (response) {
                markup = response;
                if (!fading)
                    addmarkup();
                else
                    waiting = false;
            })
            .error(function () {
                markup = "<div class='message-info ui-corner-all'><span class='icon'></span><p>Oops, something has gone wrong.</p></div>";
                if (!fading)
                    addmarkup();
                else
                    waiting = false;
            });
    };

    return this.each(function () {

        //my api key: AIzaSyCIxlX7-5p_JfpC4r731BHXKAZFXU0m0_g
        //my cse: 003724607652872098273:4ssehjify8s

        //mises api key: ?
        //mises cse: 008192686144554149800:lssgwvjfvx4

        //"https://www.googleapis.com/customsearch/v1?key=AIzaSyCIxlX7-5p_JfpC4r731BHXKAZFXU0m0_g&cx=008192686144554149800:lssgwvjfvx4&q=flowers&alt=json"
        //https://www.google.com/cse?cx=003724607652872098273:4ssehjify8s&q=einstein&output=xml

        //TODO remove ids to allow multiple of these on the page (not sure will ever be needed so not spending time on this now)

        var $cnt = $(this);

        $("#mediasearchstring").addClass("ui-widget ui-widget-content ui-corner-left");

        $("#mediasearch", $cnt).button().removeClass("ui-corner-all").addClass("ui-corner-right")
            .click(function () {

                var searchstring = $("#mediasearchstring").val();
                var authorId = $("#AuthorId").val();
                var subjectId = $("#SubjectId").val();
                var categoryId = $("#CategoryId").val();

                if ((searchstring != null && $.trim(searchstring).length > 0) || authorId != "" || subjectId != "" || categoryId != "") {

                    var data = {
                        encodedTerm: encodeURIComponent(escape(searchstring)),
                        page: 1,
                        authorId: authorId,
                        subjectId: subjectId,
                        categoryId: categoryId
                    };

                    if (viaAjax) {
                        ajaxSearch(data);
                    }
                    else {
                        window.location = "/media/search/" + data.page + "?q=" + data.encodedTerm
                         + "&aid=" + authorId
                         + "&sid=" + subjectId
                         + "&cid=" + categoryId;
                    }
                }
                ;

                return false;
            });

        $("#cse-media-results").click(function (event) {

            var $clicked = $(event.target);

            if ($clicked.is("ul.pagination a")) {

                var params = $clicked.attr("href").substring($clicked.attr("href").indexOf("/search/") + 8).split("/");

                var data = {
                    encodedTerm: params[0],
                    page: params[1]
                };

                if (viaAjax)
                    ajaxSearch(data);
                else
                    window.location = "/media/search/" + data.encodedTerm + "/" + data.page;

                return false;
            }
        });

    });
};

/*
* Video pod functionality...
*/

function videoPod() {
    $('#VideoPod').html("<h3 class='loading'><div class='msg'>loading...</div></h3>");
    var thetime = new Date();
    $.get('/Home/VideoPod/?s=' + thetime.getMinutes() + thetime.getSeconds(), function (data) {
        $('#VideoPod').html(data);
    });
};


/**
* Pulse plugin for jQuery 
* ---
* @author James Padolsey (http://james.padolsey.com)
* @version 0.1
* @updated 16-DEC-09
* ---
* Note: In order to animate color properties, you need
* the color plugin from here: http://plugins.jquery.com/project/color
* ---
* @info http://james.padolsey.com/javascript/simple-pulse-plugin-for-jquery/
*/

jQuery.fn.pulse = function (prop, speed, times, easing, callback) {

    if (isNaN(times)) {
        callback = easing;
        easing = times;
        times = 10000000;
    }

    var optall = jQuery.speed(speed, easing, callback),
        queue = optall.queue !== false,
        largest = 0;

    for (var p in prop) {
        largest = Math.max(prop[p].length, largest);
    }

    optall.times = optall.times || times;

    return this[queue ? 'queue' : 'each'](function () {

        var counts = {},
            opt = jQuery.extend({}, optall),
            self = jQuery(this);

        pulse();

        function pulse() {

            var propsSingle = {},
                doAnimate = false;

            for (var p in prop) {

                // Make sure counter is setup for current prop
                counts[p] = counts[p] || { runs: 0, cur: -1 };

                // Set "cur" to reflect new position in pulse array
                if (counts[p].cur < prop[p].length - 1) {
                    ++counts[p].cur;
                } else {
                    // Reset to beginning of pulse array
                    counts[p].cur = 0;
                    ++counts[p].runs;
                }

                if (prop[p].length === largest) {
                    doAnimate = opt.times > counts[p].runs;
                }

                propsSingle[p] = prop[p][counts[p].cur];

            }

            opt.complete = pulse;
            opt.queue = false;

            if (doAnimate) {
                self.animate(propsSingle, opt);
            } else {
                optall.complete.call(self[0]);
            }

        }

    });

};


function GetDaysArticles(day, isNext) {
    var today = new Date();
    var service_URL = BASE_URL + '/daily/GetArticlesForDay/?DatePosted=';
    currentDay = day;
    var myURL = service_URL + encodeURIComponent(new Date(day).toUTCString());
    //alert(myURL);

    // Check if requesting today's date
    if (new Date(day).toLocaleDateString() == today.toLocaleDateString())
        $(".hpdnext").hide();

    $.ajax({
        url: myURL,
        dataType: 'json',
        success: function (data) {
            //alert("response:" + data);

            if (data == null) {
                alert("No response received!");
                return;
            }
            if (data.Error != null) {
                $(".hpdnext").hide();
                return;
            } else if (data == null || data.length == 0) {
                // Don't run template
                $("#sl_dates").html('No articles on ' + new Date(day).toLocaleDateString());
                $("#sl_dates").show(500);

                if (new Date(day).toLocaleDateString() == today.toLocaleDateString()) {
                    // nothing today
                } else if (isNext)
                    setTimeout(function () {

                        var nextDay = (new Date(currentDay).setDate(new Date(currentDay).getDate() + 1));
                        // stop if next day is > today
                        if (nextDay > new Date(currentDay)) {
                            return;
                        }
                        GetDaysArticles(nextDay, isNext);
                    }, 1000);
                else
                    setTimeout(function () {
                        GetDaysArticles((new Date(currentDay).setDate(new Date(currentDay).getDate() - 1)), isNext);
                    }, 1000);

                return;
            }
            data.DatePosted = new Date(day).toLocaleDateString();

            $("#sl_dates").html(data.DatePosted);
            $("#sl_dates").show(500);

            $("#hp_dailyf").hide();
            $("#hp_dailys").hide();

            var headline = data[0];
            $("#hp_dailyf").html($("#headlineTemplate").tmpl(headline));

            $("#hp_dailyf").show(500, function () {

                if (data.length > 1) {
                    featured = new Array();

                    $.each(data, function (index, value) {
                        if (index > 0)
                            featured.push(value);
                    });

                    $("#hp_dailys").html($("#featuredTemplate").tmpl(featured));
                    $("#hp_dailys").show(500);
                }

            });
        },
        error: function (error, type, exception) {
//            alert(error);
//            alert(type);
//            alert(exception);
        }
    });

}


// Tagging

function AddTag(Id) {
    $.ajax({
        type: "POST",
        url: "/Tag/",
        data: "id=" + Id + "&Tags=" + $("#txtTag").val(),
        success: function (msg) {
            alert("Result: " + msg);
        }
    });

    $.getJSON('/Tag/Id=' + Id, function (data) {

        $('p.tagcloud').hide('fast', function () {
            var tagHtml = '';
            $.each(data, function (index, value) {

                tagHtml += '<a href="/tag/' + data[index].TagText + '" title="' + data[index].TagCount + ' documents" class="CommonTag5" rel="tag">' + data[index].TagText + '</a>';

            });

            $('p.tagcloud').html(tagHtml);
            $('p.tagcloud').show('fast');

        });


    });

    return false;
}


function change(id, newClass) {
    identity = document.getElementById(id);
    identity.className = newClass;
}

function GetRandomProduct(day) {
    var service_URL = BASE_URL + '/MisesShop/RandomBook';
    $.ajax({
        url: service_URL,
        dataType: 'json',
        success: function (data) {
            if (data == null) {
                // alert("No response received!")
                return;
            } else if ((typeof (data) === "undefined") || (data.length == 0)) {
                return;
            }
            $("#featuredProduct").html($("#featuredProductTemplate").tmpl(data));

        },
        error: function (error, type, exception) {
            //alert(error); alert(type); alert(exception); 
        }
    });
}


function searchOnChange(e) {
    //window.location.href = '/Literature/Search/?q=' + e.value;

    if (e == null) {
        //e = $('#txtSmartSearch').val();
        return;
    }
    else {
        e = e.value;
        
        if (e[0] != '/') {
            window.location.href = '/literature.aspx?action=search&q=' + e;
        }
    }
    window.location.href = e;
}