#region

using System;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class MasterPages_DefaultMain : MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Sidebar1.Visible = Request.QueryString["notheme"] == null;

        if (Request.QueryString["menu"] != null)
        {
            Sidebar1.Controls.Clear();

            var text = ContentHelper.GetContentForPage(Request.QueryString["menu"]);

            Sidebar1.Controls.Add(new Literal() { Text = text });

        }
    }

}