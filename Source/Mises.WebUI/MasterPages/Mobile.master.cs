#region

using System;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic;
using Mises.Data;

#endregion

partial class MasterPages_Mobile : MisesMasterPage
{
    private const string CookieName = "ABLECOMMERCE1";

    public string DocumentId
    {
        get
        {
            if (ViewState["DocumentId"] != null)
            {
                return ViewState["DocumentId"].ToString();
            }
            return "0";
        }
        set { ViewState["DocumentId"] = value; }
    }

    public string EditId
    {
        get { return DocumentId; }
        set
        {
            DocumentId = value;
            if (IsAdmin())
            {
                editLink.InnerHtml = "<a href=\"" + GetEditURL(value) + "\"><em>Edit</em></a>";
                editLink.Attributes.Add("class", "active");
                editLink.Visible = true;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.GetCurrent(Page).AuthenticationService.Path = "/Authentication_JSON_AppService.axd";

        //Response.ContentType = "application/xhtml+xml"
        if (Page.IsPostBack)
        {
            return;
        }

        if (Request.IsSecureConnection)
        {
            NonSecure.Visible = false;
            Secure.Visible = true;
        }


        if (Request.QueryString["Id"] != null)
        {
            DocumentId = Request.QueryString["Id"];
            EditId = Request.QueryString["Id"];
        }

        if (Request.QueryString["control"] != null)
        {
            EditId = Request.QueryString["control"];
        }

        string URL = Request.Url.ToString();
        if (URL.Contains("about"))
        {
            lnkAbout.Attributes.Add("class", "active");
            if (Conversion.Val(EditId) == 0)
            {
                EditId = "3228";
            }
        }
        else if (URL.Contains("article") || URL.Contains("story"))
        {
            lnkArticles.Attributes.Add("class", "active");
        }
        else if (URL.Contains("literature") || URL.Contains("resources") || URL.Contains("periodical"))
        {
            lnkLiterature.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : New Documents/Publications", "/guide.xml");
        }
        else if (URL.Contains("media"))
        {
            lnkMedia.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : New Media", "/media.xml");
        }
        else if (URL.Contains("event"))
        {
            lnkEvents.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : Upcoming Events", "/events.xml");
        }
        else if (URL.Contains("publications"))
        {
            lnkLiterature.Attributes.Add("class", "active");
            if (Conversion.Val(EditId) == 0)
            {
                EditId = "3230";
            }
        }
        else if (URL.Contains("default"))
        {
            lnkHome.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : Daily Articles", "/dailyarticles.xml");
            AddRSSLink("Mises Institute : Upcoming Events", "/events.xml");
            AddRSSLink("Mises Institute : New Documents/Publications", "/literature.xml");
            AddRSSLink("Mises Institute : New Media", "/media.xml");
        }

        if (IsAdmin())
        {
            manageLink.Visible = true;
            manageLink.Attributes.Add("class", "active");
        }
    }

    private void AddRSSLink(string title, string URL)
    {
        var link = new HtmlLink();
        link.Attributes.Add("type", "application/rss+xml");
        link.Attributes.Add("rel", "alternate");
        link.Attributes.Add("title", title);
        link.Attributes.Add("href", URL);
        Page.Header.Controls.Add(link);
    }

    public bool IsAdmin()
    {
        return ((Request.Cookies[CookieName] != null && Request.Cookies[CookieName]["IsAdmin"] != null &&
                 Request.Cookies[CookieName]["IsAdmin"] == "1")) ||
               ((Request.Cookies["IsAdminLoggedIn"] != null && Request.Cookies["IsAdminLoggedIn"].Value == "1"));
    }


    private string GetEditURL(string Id)
    {
        string URL;
        string currentURL = Request.Url.ToString();

        if (DataFormat.IsGUID(Id))
        {
            URL = string.Format("/manager/page.aspx?Id={0}", Id);
        }
        else
        {
            if (currentURL.Contains("/daily/") || currentURL.Contains("article.aspx") ||
                currentURL.Contains("article2.aspx"))
            {
                URL = string.Format("/manager/article.aspx?Id={0}", Id);
            }
            else if (currentURL.Contains("media"))
            {
                URL = string.Format("/manager/document.aspx?Id={0}&format=media", Id);
            }
            else if (currentURL.Contains("/events/") || currentURL.Contains("event"))
            {
                URL = "/manager/events/?Id=" + Id;
            }
            else if (currentURL.Contains("/form.aspx"))
            {
                URL = "/manager/formEditor.aspx?formId=" + Id;
            }
            else if (currentURL.Contains("/file.aspx"))
            {
                URL = "/manager/content.aspx";
            }
            else if (currentURL.Contains("/periodical.aspx"))
            {
                URL = "/manager/periodicals/JournalArchives.aspx?Id=" + Id;
            }
            else if (currentURL.Contains("/periodical.aspx"))
            {
                URL = "/manager/periodicals.aspx?Id=" + Id;
            }
            else if (currentURL.Contains("/literature.aspx") || currentURL.Contains("/media.aspx"))
            {
                URL = "/manager/content.aspx";
            }
            else if (currentURL.Contains("misesreview"))
            {
                URL = "/manager/periodicals/misesreview.aspx?Id=" + Request.QueryString["control"];
            }
            else if (currentURL.Contains("quotes.aspx"))
            {
                URL = "/manager/quotes.aspx";
            }
            else
            {
                URL = "/manager/document.aspx?format=literature&Id=" + Id;
            }
        }

        return URL;
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }
}