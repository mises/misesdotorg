#region

using System;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic;
using Mises.Domain;

#endregion

public partial class MasterPages_Main : MisesMasterPage
{
    public string DocumentId
    {
        get
        {
            if (ViewState["DocumentId"] != null)
            {
                return ViewState["DocumentId"].ToString();
            }
            return "0";
        }
        set { ViewState["DocumentId"] = value; }
    }

    public string EditId
    {
        get { return DocumentId; }
        set
        {
            DocumentId = value;
            if (IsAdmin())
            {
                editLink.InnerHtml = "<a href=\"" +
                                     AdminHelper.GetEditURL(value, Request.Url.ToString(), Request.QueryString) +
                                     "\"><em>Edit</em></a>";
                editLink.Attributes.Add("class", "active");
                editLink.Visible = true;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.GetCurrent(Page).AuthenticationService.Path = "/Authentication_JSON_AppService.axd";

        //Response.ContentType = "application/xhtml+xml"
        if (Page.IsPostBack)
        {
            return;
        }

        if (Request.Params["notheme"] != null)
        {
            header.Visible = false;
            if (Footer1 != null) Footer1.Visible = false;
            header.Visible = false;    
        }
        

        if (Request.QueryString["Id"] != null)
        {
            string id = Request.QueryString["Id"];
            if (id.IndexOf('/') > 0)
            {
                id = id.Substring(0, id.IndexOf('/'));
            }
            DocumentId = id;
            EditId = id;
        }

        if (Request.QueryString["MediaId"] != null)
        {
            EditId = Request.QueryString["MediaId"];
        }

        if (Request.QueryString["control"] != null)
        {
            EditId = Request.QueryString["control"];
        }

        string URL = Request.Url.ToString();
        if (URL.Contains("about"))
        {
            lnkAbout.Attributes.Add("class", "active");
            if (Conversion.Val(EditId) == 0)
            {
                EditId = "3228";
            }
        }
        else if (URL.Contains("article") || URL.Contains("story"))
        {
            lnkArticles.Attributes.Add("class", "active");
        }
        else if (URL.Contains("literature") || URL.Contains("resources") || URL.Contains("periodical"))
        {
            lnkLiterature.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : New Documents/Publications", "/guide.xml");
        }
        else if (URL.Contains("media"))
        {
            lnkMedia.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : New Media", "/media.xml");
        }
        else if (URL.Contains("event"))
        {
            lnkEvents.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : Upcoming Events", "/events.xml");
        }
        else if (URL.Contains("publications"))
        {
            lnkLiterature.Attributes.Add("class", "active");
            if (Conversion.Val(EditId) == 0)
            {
                EditId = "3230";
            }
        }
        else if (URL.Contains("default.aspx"))
        {
            lnkHome.Attributes.Add("class", "active");
            AddRSSLink("Mises Institute : Daily Articles", "/dailyarticles.xml");
            AddRSSLink("Mises Institute : Upcoming Events", "/events.xml");
            AddRSSLink("Mises Institute : New Documents/Publications", "/literature.xml");
            AddRSSLink("Mises Institute : New Media", "/media.xml");

            Page.Header.Description =
                "The Ludwig von Mises Institute is the research and educational center of classical liberalism and the Austrian School of economics. Working in the intellectual tradition of Ludwig von Mises (1881-1973) and Murray N. Rothbard (1926-1995), with a vast array of publications, programs, and fellowships, the Mises Institute seeks a radical shift in the intellectual climate.";
            Page.Header.Keywords =
                "Austrian Economics, Austrian Biography, Mises, Rothbard, Research in Austrian Economics, Free Market Theory, Libertarianism, Auburn, classical liberalism, capitalism";
        }

        if (IsAdmin())
        {
            manageLink.Visible = true;
            manageLink.Attributes.Add("class", "active");
        }
    }

    private void AddRSSLink(string title, string URL)
    {
        var link = new HtmlLink();
        link.Attributes.Add("type", "application/rss+xml");
        link.Attributes.Add("rel", "alternate");
        link.Attributes.Add("title", title);
        link.Attributes.Add("href", URL);
        Page.Header.Controls.Add(link);
    }

    public bool IsAdmin()
    {
        return AdminHelper.IsAdmin(Request.Cookies);
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }
}