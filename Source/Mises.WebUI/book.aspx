<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false" Inherits="book" Title=": Ward and Massey Libraries of the Mises Institute" Codebehind="book.aspx.cs" EnableViewState="false" %>
<%@ OutputCache Duration="180" VaryByParam="*" %>
<%@ Register Src="Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:PageContentControl ID="PageContentControl2" runat="server" ContentName="BookPageHeader" />
    <asp:DetailsView ID="dvBook" runat="server" AutoGenerateRows="False" BackColor="White"
                     BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="Control"
                     ForeColor="Black" GridLines="Vertical" Height="50px"
                     Width="80%">
        <FooterStyle BackColor="#CCCC99" />
        <RowStyle BackColor="#F7F7DE" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <Fields>
            <asp:TemplateField HeaderText="Title:" SortExpression="title">
                <itemtemplate>
                    <h2>
                        <asp:Literal ID="litTitle" runat="server" Text='<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>'></asp:Literal>
                    </h2>
                </itemtemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="AuthorName" DataNavigateUrlFormatString="~/periodical.aspx?Id=8&search={0}"
                                DataTextField="AuthorName" HeaderText="Author:" />
            <asp:BoundField  HeaderText="Checked Out By:" />
            <asp:BoundField DataField="Comments" HeaderText="Comments:" SortExpression="comments" />
            <asp:BoundField DataField="Title" HeaderText="Subject" SortExpression="Title" />
            <asp:BoundField DataField="publisher_info" HeaderText="Publisher" SortExpression="publisher_info" />
        </Fields>
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    <uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="BookPageFooter" />
</asp:Content>