﻿#region

using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mises.Domain.Utility;

#endregion

namespace Mises.Web
{
    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new RedirectMobileDevicesToMobileArea(), 1);
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            #region Ignore static files
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.Ignore("{resource}.aspx/{*pathinfo}");
            routes.Ignore("{resource}.ashx/{*pathinfo}");
            routes.Ignore("{resource}.png/{*pathinfo}");
            routes.Ignore("{resource}.gif/{*pathinfo}");
            routes.Ignore("{resource}.jpg/{*pathinfo}");
            routes.Ignore("{resource}.html/{*pathinfo}");
            routes.Ignore("{resource}.php/{*pathinfo}");
            routes.Ignore("{resource}.xml/{*pathinfo}");
            routes.IgnoreRoute("Community");
            #endregion Ignore static files

            

            routes.RouteExistingFiles = false;
            const string _ContraintId = @"\d+|\<\#\=.+?\#\>";

            routes.MapRoute("page-detail","page/{id}/{slug}", new { controller = "Page", action = "Index", slug = String.Empty },new { id = @"\d+"  });

            routes.MapRoute("about-detail","about/{id}/{slug}", new { controller = "About", action = "Index", slug = String.Empty },new { id = @"\d+"  });

            routes.MapRoute("events-detail","events/{id}/{slug}", new { controller = "Events", action = "Index", slug = String.Empty },new { id = @"\d+" });

            routes.MapRoute("fellow-index", "fellow/{id}", new { controller = "Faculty", action = "Detail" }, new { id = @"\d+" });

            routes.MapRoute(null, "search/{term}", new { controller = "Search", action = "Index" });


            #region Media

            routes.MapRoute("media-detail","media/{id}/{slug}/{authorPage}",
                new { controller = "Media", action = "Detail", slug = String.Empty, authorPage = 1 },new { id = @"\d+" , authorPage = @"\d+" });

            routes.MapRoute("media-index2","media/{filter}/{page}",new { controller = "Media", action = "Index2" /*, filter = "video"*/, page = 1 },
                new { page = @"\d+" });
                
            
            routes.MapRoute("media-index3",
                "media/{filter}/{id}/{slug}/{page}",
                new { controller = "Media", action = "Index3" /*, filter = "video", id = 0, slug = "blank"*/, page = 1 },
                new { id = @"\d+", page = @"\d+" });

            routes.MapRoute("categories-index",
                "media/categories/{childrenPage}/{documentsPage}",
                new { controller = "Categories", action = "Index", childrenPage = 1, documentsPage = 1 },
                new { childrenPage = @"\d+", documentsPage = @"\d+" });

            routes.MapRoute("category-detail",
                "media/categories/{id}/{slug}/{childrenPage}/{documentsPage}",
                new { controller = "Categories", action = "Detail", id = 0, childrenPage = 1, documentsPage = 1 },
                new { id = @"\d+", childrenPage = @"\d+", documentsPage = @"\d+" }
                );

            routes.MapRoute("authors-browse",
                "authors/browse/{letter}/{page}",
                new { controller = "Authors", action = "Browse", letter = 'a', page = 1 },
                new { letter = "[a-z]", page = @"\d+" }
                );


            routes.MapRoute("author-detail",
                "authors/{id}/{slug}/{articlePage}/{documentPage}",
                new { controller = "Authors", action = "Detail", articlePage = 1, documentPage = 1 },
                new { id = @"\d+", articlePage = @"\d+", documentPage = @"\d+" }
                );

            // hack
            routes.MapRoute("author-detail2",
                "authors/{id}",
                new { controller = "Authors", action = "Detail", articlePage = 1, documentPage = 1 },
                new { id = @"\d+", articlePage = @"\d+", documentPage = @"\d+" }
                );

            routes.MapRoute("authors-index",
                "authors",
                new { controller = "Authors", action = "Index" }
                );

            routes.MapRoute("media-browse",
                "media/browse/{filter}/{id}/{page}",
                new { controller = "Media", action = "Browse", id = 0, page = 1 },
                new { id = @"\d+", page = @"\d+" }
                );


            routes.MapRoute("Media2",
                "Media/{action}/{id}", 
                new { controller = "Media", action = "Index", id = UrlParameter.Optional });

            #endregion Media

            #region Daily
            
            routes.MapRoute("daily-preview","preview/{id}/{slug}",new { controller = "Daily", action = "Preview", slug = String.Empty, preview = "" },new { id = @"\d+" });
            routes.MapRoute("MisesDaily-view", "MisesDaily/{action}/{id}", new { controller = "Daily", action = "Index", id = UrlParameter.Optional });
            
            routes.MapRoute("daily-detail","daily/{id}/{slug}/{preview}",new { controller = "Daily", action = "Detail", slug = String.Empty, preview = "" },new { id = @"\d+" });
            
            routes.MapRoute("daily-author", "daily/author/{id}/{page}", new { controller = "Daily", action = "Index", format = "author",page=0 });

            routes.MapRoute("daily-album", "daily/{format}/{page}", new { controller = "Daily", action = "Index", format = "List", page = 0 });
            
            //routes.MapRoute("daily-author-list", "daily/authors", new { controller = "Daily", action = "Index", format = "authorlist", slug = UrlParameter.Optional }); 


            #endregion Daily

            

            routes.MapRoute("detail2","{controller}/{id}/{slug}",new { controller = "Document", action = "View", id = _ContraintId, slug = UrlParameter.Optional },
               new { id = @"\d+" },new[] { "Mises.Web.Controllers" });

            routes.MapRoute("PathWithName","{controller}/{action}/{id}/{name}", new { controller = "Home", action = "Index", id = UrlParameter.Optional, name = UrlParameter.Optional });
            routes.MapRoute("Default","{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional } );
            routes.MapRoute("Error","{*url}",new { controller = "Error", action = "Http404" });
        }

        #region Other than MVC

        protected void Application_Start(object sender, EventArgs e)
        {
            this.CreateMaps();

            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            //  ViewEngines.Engines.Clear();
            //ViewEngines.Engines.Add(new RazorViewEngine());
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            string sessionId = Session.SessionID;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;

            if (app.Request.IsSecureConnection || app.Request.IsAuthenticated || app.Request.IsLocal) return;

            string host = app.Request.Url.Host;

            //if (host != "mises.org" && host != "www.mises.org" && host != "direct.mises.org" && host != "localhost" &&
            //    host != "beta.mises.org" && host != "alpha.mises.org" && host != "sg.mises.org" && host != "m.mises.org" && host != "mobile.mises.org" && host != "data.mises.org" &&
            //    host != "mises.freecapitalists.org/images" && host != "local.mises.org" && host != "legacy.mises.org")
            //{
            //    string URL = app.Request.Url.ToString();

            //    switch (host)
            //    {
            //        case "blogs.mises.org":
            //            Response.RedirectPermanent(URL.Replace("blogs.mises.org", "mises.org/Community/blogs"));
            //            break;
            //        case "people.mises.org":
            //            Response.RedirectPermanent(URL.Replace("people.mises.org", "mises.org/Community/members") +
            //                              ((URL.Contains("default.aspx")) ? "" : "/default.aspx"));
            //            break;
            //        default:
            //            Response.RedirectPermanent("http://mises.freecapitalists.org//");
            //            break;
            //    }
            //}
            //else if (host == "mobile.mises.org" || host == "m.mises.org")
            //{
            //    if (app.Request.Url.ToString() == "http://m.mises.org/default.aspx")
            //        Response.RedirectPermanent("http://mises.freecapitalists.org//mobile/");
            //}
        }


        public override string GetVaryByCustomString(HttpContext context, string arg)
        {
            return arg == "userName" ? context.User.Identity.Name : string.Empty;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception ex = Server.GetLastError();

            if (ex == null) return;
            try
            {
                ExceptionLogging.LogException(Context, ex);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            {
            }
            // ReSharper restore EmptyGeneralCatchClause

            if (Application["LastError"] != null &&
                Convert.ToDateTime(Application["LastError"]) >= DateTime.Now.AddSeconds(-45)) return;
            Application["LastError"] = DateTime.Now;

#if RELEASE
			ExceptionLogging.SendExceptionEmail(ex, Request);
#endif
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

        #endregion
    }
}