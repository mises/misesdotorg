<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" EnableViewState="false" Inherits="freemarket_detail" Codebehind="~/freemarket_detail.aspx.cs" %>

<%@ Register Src="Controls/Navigation/Bookmark.ascx" TagName="Bookmark" TagPrefix="uc1" %>
<%@ Register Src="Controls/Tagging/TagCloud.ascx" TagName="TagCloud" TagPrefix="uc2" %>
<%@ OutputCache Duration="10" VaryByParam="*" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p style="text-align: center">
        <a href="/periodical.aspx?Id=1" rel="directory">
            <img src="/images/FreeMarket.gif" alt="The Free Market" width="268"
                 height="66" /></a><br />
        The Mises Institute monthly, <a href="/donate.aspx">free with membership</a>
    </p>
    <br />
    <asp:Literal ID="lblBody" runat="server"></asp:Literal>
    <p>
        <a href="#" onclick=" history.go(-1); ">Back</a>
        <uc2:TagCloud ID="TagCloud" runat="server" />
        <uc1:Bookmark ID="Bookmark1" runat="server" />
    </p>
</asp:Content>