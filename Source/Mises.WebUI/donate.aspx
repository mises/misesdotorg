<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" Title="Become a Member of the Mises Institute" AutoEventWireup="false"
         MaintainScrollPositionOnPostback="true" Inherits="donate"
         EnableEventValidation="false" Codebehind="donate.aspx.cs" %>

<%@ Register Src="Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        /* Donate */

        .donate label
        {
            color: #333;
        }

        .donate ol li
        {
            padding-left: 10px;
        }

        .donate input
        {
            font-size: 14px;
        }

        fieldset.donate.col1
        {
            float: left;
            width: 250px;
            /*overflow: hidden; */
            overflow: visible;
        }

        fieldset.donate.col1 ol
        {
            width: 250px;
            overflow: visible;
        }

        fieldset.donate.col1 li
        {
            width: 230px;
        }

        fieldset.donate.submit
        {
            clear: both;
        }

        fieldset.donate.col1 li.col1
        {
            width: auto;
            margin-right: 10px;
        }

        fieldset.donate li.col1 select
        {
            width: 70px;
        }

        fieldset.donate.col1 li.col2
        {
            width: auto;
        }

        fieldset.donate li.col2 input.text
        {
            width: 105px;
        }

        fieldset.donate span.LV_validation_message
        {
            display: none;
        }

        fieldset.donate.submit
        {
            padding-right: 170px;
        }

        fieldset.donate.submit input.buttonimg
        {
            float: right;
            margin-right: 0;
            margin-left: 15px;
        }
    </style>
    <h2>
        We Need Your Support</h2>
    <asp:Panel ID="pnlResults" runat="server" Visible="False">
        <h4>
            <asp:Label ID="lblMessage" runat="server" Font-Size="Medium"></asp:Label>
        </h4>
        <br />
        <p id="Receipt" style="border: solid 1px black; width: 400px;">
            <asp:Literal ID="litReceipt" runat="server"></asp:Literal>
        </p>
    </asp:Panel>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <div id="IntroText" runat="server">
        <uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="DonatePageContent" />
    </div>
    <div id="DonationForm" runat="server">
        <fieldset class="extended donate">
            <%--<legend><span>Donate</span></legend>--%>
            <ul>
                <li class="required">
                    <label for="<%=txtEmail.ClientID%>">
                        Email</label>
                    <a name="Email" id="Email"></a>
                    <asp:TextBox ID="txtEmail" class="text" name="Email" runat="server" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                                    ErrorMessage="You must enter a valid email address." ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    Display="Dynamic">*</asp:RegularExpressionValidator>
                </li>
                <li class="required">
                    <label for="<%=txtPhone.ClientID%>">
                        Phone</label>
                    <asp:TextBox ID="txtPhone" class="text" name="phone" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPhone"
                                                ErrorMessage="You must enter a valid phone number." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtFirst.ClientID%>">
                        First Name</label>
                    <asp:TextBox ID="txtFirst" class="text" name="FirstName" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFirst"
                                                ErrorMessage="You must enter a first name." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtLast.ClientID%>">
                        Last Name</label>
                    <asp:TextBox ID="txtLast" class="text" name="last_name" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtLast"
                                                ErrorMessage="You must enter a last name." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtStreet.ClientID%>">
                        Street Address</label>
                    <asp:TextBox ID="txtStreet" runat="server" CssClass="text" AutoCompleteType="HomeStreetAddress"></asp:TextBox>
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="text" AutoCompleteType="BusinessStreetAddress"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtStreet"
                                                ErrorMessage="You must provide your address." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtCity.ClientID%>">
                        City</label>
                    <asp:TextBox ID="txtCity" class="text" name="City" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCity"
                                                ErrorMessage="You must enter a city." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtState.ClientID%>">
                        State</label>
                    <asp:TextBox ID="txtState" class="text" name="State" runat="server" />
                     <em>Enter "N/A" for none.</em>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtState"
                                                ErrorMessage="You must enter a state." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtPostalCode.ClientID%>">
                        Zip/Postal Code</label>
                    <asp:TextBox ID="txtPostalCode" class="text" name="Zip" runat="server" />
                     <em>Enter "N/A" for none.</em>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtPostalCode"
                                                ErrorMessage="You must enter a postal code." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=ddCountries.ClientID%>">
                        Country</label>
                    <asp:DropDownList ID="ddCountries" runat="server" Width="190" DataSourceID="sqlGetCountries"
                                      DataTextField="Name" DataValueField="Name">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlGetCountries" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                                       EnableCaching="True" SelectCommand="CountriesGetList" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </li>
                <li class="required">
                    <label for="ddDesignation">
                        Designate my contribution to</label>
                    <select id="ddDesignation" name="Designation" runat="server">
                        <option selected="selected">Wherever most needed</option>
                        <option>Audio and Video Production</option>
                        <option>Publishing</option>
                        <option>Student Fellowships</option>
                        <option>Teaching Conferences</option>
                        <option>Web Development</option>
                    </select>
                </li>
                <li class="required">
                    <label for="<%=ddDonationAmount.ClientID%>">
                        Donation Amount</label>
                    <asp:DropDownList ID="ddDonationAmount" runat="server">
                        <asp:ListItem Value="60" Selected="True">$60 LvMI Member</asp:ListItem>
                        <asp:ListItem Value="100">$100</asp:ListItem>
                        <asp:ListItem Value="250">$250</asp:ListItem>
                        <asp:ListItem Value="500">$500</asp:ListItem>
                        <asp:ListItem Value="1000">$1000</asp:ListItem>
                        <asp:ListItem Value="2000">$2000 Mises U Scholarship</asp:ListItem>
                        <asp:ListItem Value="0">Other - please specify below</asp:ListItem>
                    </asp:DropDownList>
                </li>
                <li>
                    <label for="">
                        or select &quot;Other&quot; above and enter amount below</label>
                    <asp:TextBox ID="txtOtherDonationAmount" class="text" name="OtherAmount" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtOtherDonationAmount"
                                        ErrorMessage='<li>To keep processing costs down, the minimum donation amount is $10. For special consideration, please <a href="mailto:info@freecapitalists.org">Email Us</a></li>'
                                        Type="Currency" MinimumValue="10" MaximumValue="999999" Display="Dynamic">*</asp:RangeValidator>
                </li>
                <li>
                    <label for="">
                        In Memory or In Honor of:</label>
                    <asp:TextBox ID="txtInMemoryOf" class="text" name="InMemoryOf" runat="server"></asp:TextBox>
                </li>
                <li>
                    <asp:CheckBox ID="chkRecurring" runat="server" Text="Make this donation recurring every month?">
                    </asp:CheckBox>
                </li>
                <%--<li>
                    <asp:CheckBox ID="chkGiftIncome" runat="server" Text="Contact me about gifts that can provide retirement income for myself or another loved one. ">
                    </asp:CheckBox>
                </li>--%>
                <li>
                    <asp:CheckBox ID="chkGiftEstate" runat="server" Text="Contact me about making a gift through my estate plan." />
                </li>
                <li class="required">
                    <label for="<%=ddCreditCardType.ClientID%>">
                        Credit Card Type</label>
                    <asp:DropDownList ID="ddCreditCardType" runat="server">
                        <asp:ListItem Value="Visa">Visa</asp:ListItem>
                        <asp:ListItem Value="MasterCard">MasterCard</asp:ListItem>
                        <asp:ListItem Value="AMEX">American Express</asp:ListItem>
                        <asp:ListItem Value="Discover">Discover</asp:ListItem>
                        <asp:ListItem Value="Diners Club">Diners Club</asp:ListItem>
                    </asp:DropDownList>
                </li>
                <li class="required">
                    <label for="<%=txtCardName.ClientID%>">
                        Name On Card</label>
                    <asp:TextBox ID="txtCardName" class="text" name="CardName" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtCardName"
                                                ErrorMessage="You must enter a credit card name." Display="Dynamic">*</asp:RequiredFieldValidator>
                </li>
                <li class="required">
                    <label for="<%=txtCardNum.ClientID%>">
                        Card Number</label>
                    <asp:TextBox ID="txtCardNum" class="text" runat="server"></asp:TextBox>
                    <asp:CustomValidator ID="valLunCode" runat="server" ErrorMessage="Please enter a valid credit card number."
                                         Display="Dynamic" EnableClientScript="False">*</asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtCardNum"
                                                ErrorMessage="You must enter a credit card number." Display="Dynamic">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCardNum"
                                                    ErrorMessage="Please enter a valid credit card number." ValidationExpression="\d{15,20}"
                                                    Display="Dynamic">*</asp:RegularExpressionValidator>
                </li>
                <li class="required">
                    <label for="<%=txtCardVerification.ClientID%>">
                        Card Security/Verification Code (on back of card)</label>
                    <asp:TextBox ID="txtCardVerification" class="text" name="CardNumber" runat="server" />
                </li>
                <li class="required">
                    <label for="<%=ddMonthExpiration.ClientID%>">
                        Expiration Date</label>
                    <asp:DropDownList ID="ddMonthExpiration" runat="server">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddYearExpiration" runat="server">
                    </asp:DropDownList>
                </li>
                <li class="required">
                    <label for="<%=txtComments.ClientID%>">
                        Additional Comments (e.g. preferred mailing address)</label>
                    <asp:TextBox ID="txtComments" name="Comments" Style="width: 300px;" runat="server"
                                 TextMode="MultiLine"></asp:TextBox>
                </li>
            </ul>
        </fieldset>
        <asp:Button ID="btnSendContribution" runat="server" Text="Send Contribution"  CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
    </div>
    
    <uc1:PageContentControl ID="PageContentControl2" runat="server" ContentName="DonatePageFooter" />
    
   
</asp:Content>