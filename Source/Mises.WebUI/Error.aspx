<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" Inherits="Error" Codebehind="~/Error.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="content">
        <strong>
            <asp:Label ID="lblResult" runat="server" /></strong>
        <br />
        <br />
        <asp:Panel ID="lblFeedback" runat="server">
            <p>
                Oops!  We're having unexpected technical difficulties.  Our highly-trained team of engineers has already been scrambled to fix things.
                <br />
                <br />
                If you&#39;d like to be notified when it is fixed, you may provide an email address and comments to the webmaster.
            </p>
            <table>
                <tr>
                    <td>
                        Email:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Description:<br />
                        <asp:TextBox ID="txtDescription" runat="server" Height="200" TextMode="multiline"
                                     Width="400" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Please type &quot;Mises&quot; here to prevent spam.
                    </td>
                    <td>
                        <asp:TextBox ID="txtSpamValidation" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:Button OnClick="Send_Click" Text="Send" runat="server"  CssClass="ui-state-default ui-corner-all ui-button" ID="Button1" />
        </asp:Panel>
    </div>
</asp:Content>