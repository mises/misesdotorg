<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false" Inherits="Tags" Title="Tags - Ludwig von Mises Institute" EnableViewState="false" Codebehind="Tags.aspx.cs" %>
<%@ Register Namespace="VRK.Controls" TagPrefix="vrk" Assembly="VRK.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Label ID="litHeader" runat="server"></asp:Label>
    </h2>
    <div class="CommonContent">
        <asp:ObjectDataSource ID="dsRelatedTags" runat="server" SelectMethod="GetRelatedTagCloud"
                              TypeName="Mises.Domain.Tags.Tagging" OldValuesParameterFormatString="original_{0}" EnableCaching="False">
            <SelectParameters>
                <asp:QueryStringParameter Name="tag" QueryStringField="tag" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <div id="CommonTagCloud">
            <h4>
                Related Tags:</h4>
            <vrk:Cloud ID="cldRelatedTags" runat="server" DataSourceID="dsRelatedTags" DataTextField="Name"
                       DataTitleField="Weight" DataTitleFormatString="{0} documents" DataWeightField="Weight"
                       DataHrefField="URL" CssClass="tagcloud" ItemCssClassPrefix="CommonTag">
            </vrk:Cloud>
        </div>
        <div class="CommonSearchResultsArea">
            <h3>
                Tagged Documents:
                <asp:HyperLink ID="lnkRSS" runat="server" CssClass="rss"></asp:HyperLink></h3>
            <asp:GridView ID="gvTaggedPages" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="DocumentTypeString" HeaderText="Type" SortExpression="DocumentTypeString" />
                    <asp:HyperLinkField DataNavigateUrlFields="URL" DataTextField="Title" HeaderText="Title"
                                        SortExpression="Title" />
                </Columns>
            </asp:GridView>
        </div>
        <h4>
            <a href="/clouds.aspx">
                <img src="//mises.freecapitalists.org/images/Theme/images/ico/tags.png" alt="Tags" />
                Mises Tag Clouds</a></h4>
    </div>
</asp:Content>