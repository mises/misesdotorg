<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false"
    Inherits="periodical" Title="Periodical Archives" CodeBehind="periodical.aspx.cs"
    EnableViewStateMac="false" %>

<%--<%@ OutputCache Duration="30" VaryByParam="*" %>--%>
<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a href="#" runat="server" id="lnkImg">
        <img id="imgLogo" runat="server" class="left" src="" alt="" />
    </a>
    <asp:Literal ID="lblDescription" runat="server"></asp:Literal>
    <h4>
        <span class="search">Search: </span>
    </h4>
    <span class="search">
        <asp:Label ID="Label1" runat="server" AssociatedControlID="txtTitle" Text="Title"></asp:Label>
        <asp:TextBox ID="txtTitle" type="text" size="20" runat="server"></asp:TextBox>
        <asp:Label ID="Label2" runat="server" AssociatedControlID="txtAuthor" Text="Author"></asp:Label>
        <asp:TextBox ID="txtAuthor" type="text" size="20" runat="server"></asp:TextBox>
        <asp:Label ID="lblSubject" runat="server" AssociatedControlID="txtsubject" Text="Subject"
            Visible="False"></asp:Label>
        <asp:TextBox ID="txtSubject" type="text" size="20" runat="server" Visible="False"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search Journal" AccessKey="s" EnableViewState="False"
            ToolTip="Search the Periodicals" Width="146px"></asp:Button>
    </span>
    <h3>
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </h3>
    <br />
    <cc1:GridView PagerStyle-CssClass="pager" ID="gvMonthlyArchives" runat="server" PageSize="100"
        DataKeyNames="Month" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False"
        Width="100%" EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
        SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("title")%>' NavigateUrl='<%# Eval("URL")%>'> </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="AuthorLast">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%# Eval("PeriodicalId")%>&amp;author=<%# Eval("AuthorLast")%>">
                        <%# Eval("AuthorName")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DatePosted" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"
                SortExpression="DatePosted">
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
        </Columns>
        <PagerStyle CssClass="pager"></PagerStyle>
    </cc1:GridView>
    <cc1:GridView PagerStyle-CssClass="pager" ID="gvSeasonalArchives" runat="server"
        DataKeyNames="Season" PageSize="100" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False"
        Width="100%" EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
        SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("title")%>' NavigateUrl='<%# Eval("URL")%>'> </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="AuthorLast">
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%# Eval("PeriodicalId")%>&amp;author=<%# Eval("AuthorLast")%>">
                        <%# Eval("AuthorName")%>
                    </a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Volume" SortExpression="Volume">
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%# Eval("PeriodicalId")%>&amp;volume=<%# Eval("Volume")%>">
                        <%# Eval("Volume")%>
                    </a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
        </Columns>
    </cc1:GridView>
    <cc1:GridView PagerStyle-CssClass="pager" ID="gvLibrary" runat="server" PageSize="100"
        AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
        EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
        SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <a href="/library/<%# Eval("Id") %>/<%# Eval("title").ToString().ToSlug() %>">
                        <%# Eval("title")%></a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="AuthorLast">
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%# Eval("PeriodicalId")%>&amp;author=<%# Eval("AuthorName")%>">
                        <%# Eval("AuthorName")%>
                    </a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:BoundField DataField="DatePosted" ReadOnly="True" HeaderText="Date Added" SortExpression="DatePosted"
                HtmlEncode="False" DataFormatString="{0:d}">
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
        </Columns>
    </cc1:GridView>
</asp:Content>
