#region

using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Utility;
using Donations = Mises.Domain.Payments.Donations;

#endregion

partial class donate : Page
{
    private void Page_Load(object sender, EventArgs e)
    {
        if (!Request.IsSecureConnection && Request.Url.Host != "localhost" && Request.Url.Host != "beta.mises.org" && Request.Url.Host != "local.mises.org")
        {
            Response.Redirect("https://mises.freecapitalists.org/donate.aspx");
        }

        if (Page.IsPostBack)
        {
            return;
        }

        string[] months = Regex.Split("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec", " ");
        for (int i = 0; i <= 11; i++)
        {
            var month = new ListItem { Value = (i + 1).ToString(), Text = months[i] };
            ddMonthExpiration.Items.Add(month);
        }
        for (int i = 0; i <= 8; i++)
        {
            var year = new ListItem { Value = (DateTime.Now.Year + i).ToString() };
            year.Text = year.Value;
            ddYearExpiration.Items.Add(year);
        }
    }

    private void ServerValidation(object source, ServerValidateEventArgs args)
    {
        if (txtCardNum.Text == string.Empty) args.IsValid = true;
        args.IsValid = DataFormat.IsCreditCardValid(txtCardNum.Text);
    }

    /// <summary>
    ///   Submit Contribution
    /// </summary>
    /// <param name = "sender">The source of the event.</param>
    /// <param name = "e">The <see cref = "System.EventArgs" /> instance containing the event data.</param>
    protected void btnSendContribution_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }

        var donations = new Donations(Request.Url.ToString()) {donation = {TestMode = false}};

        if (Request.Url.Host == "localhost" || Request.Url.Host == "alpha.mises.org" ||
            Request.Url.Host == "beta.mises.org" || txtEmail.Text == "heroic@gmail.com")
        {
            donations.donation.TestMode = true;
        }

        GetFormValues(donations);

        donations.ProcessDonation();

        lblMessage.Text = donations.WebConfirmationMessage;
        litReceipt.Text = donations.DonationReceipt;

        pnlResults.Visible = true;
        IntroText.EnableViewState = false;
        IntroText.Visible = false;
        DonationForm.Visible = false;
    }

    private void GetFormValues(Donations donate)
    {
        txtOtherDonationAmount.Text = txtOtherDonationAmount.Text.Replace("$", "").Replace("US", "");

        if (Conversion.Val(txtOtherDonationAmount.Text) > 0 && Convert.ToDouble(txtOtherDonationAmount.Text) > 0)
        {
            donate.donation.Amount = Convert.ToDecimal(txtOtherDonationAmount.Text);
        }
        else
        {
            donate.donation.Amount = Convert.ToDecimal(ddDonationAmount.SelectedValue);
        }
        donate.donation.Designation = ddDesignation.Items[ddDesignation.SelectedIndex].Value;
        //donate.donation.OtherDesignation = txtDonationDesignation.Text;
        donate.donation.CardName = txtCardName.Text;
        donate.donation.CardNumber = txtCardNum.Text;
        donate.donation.CardType = ddCreditCardType.SelectedValue;
        donate.CardVerification = txtCardVerification.Text;
        donate.donation.CardExpMonth = Convert.ToInt32(ddMonthExpiration.SelectedValue);
        donate.donation.CardExpYear = Convert.ToInt32(ddYearExpiration.SelectedValue);
        donate.donation.Comments = txtComments.Text.Trim();
        donate.donation.Recurring = chkRecurring.Checked;
        donate.InMemoryof = txtInMemoryOf.Text;
        //donate.donation.MisesMemberStatus = chkMisesMember.Checked;
        donate.donation.GiftEstate = chkGiftEstate.Checked;
        //donate.donation.GiftIncome = chkGiftIncome.Checked;
        donate.donation.FirstName = txtFirst.Text;
        donate.donation.LastName = txtLast.Text;
        donate.donation.Address = txtStreet.Text;
        donate.donation.Address2 = txtAddress2.Text;
        donate.donation.City = txtCity.Text;
        donate.donation.State = txtState.Text;
        donate.donation.Country = ddCountries.SelectedValue;
        donate.donation.Zip = txtPostalCode.Text;
        donate.donation.Email = txtEmail.Text;
        donate.donation.Phone = txtPhone.Text;
        donate.donation.FormUrl = Request.Url.ToString();
        donate.donation.IPaddress = Request.GetOriginalHostAddress();
        donate.IPaddress = Request.GetOriginalHostAddress();

        if (Request.UrlReferrer != null)
        { donate.donation.Referrer = Request.UrlReferrer.ToString(); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        btnSendContribution.Click += btnSendContribution_Click;
        valLunCode.ServerValidate += ServerValidation;
    }
}