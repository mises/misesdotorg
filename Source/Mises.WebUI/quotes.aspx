<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false" Inherits="studyguide" Title="The Quotable Mises - Ludwig von Mises Institute" Codebehind="quotes.aspx.cs" %>
<%@ OutputCache Duration="180" VaryByParam="*" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p>
        <asp:Label ID="lblRandomQuote" runat="server"></asp:Label></p>
    <br />
    <p>
        <a href="quotes.aspx" title="The Quotable Mises, and others">
            <img style="border-style: solid; border-width: 0px; float: left"  alt="Quotable Mises"  src="/images/qm.gif" /></a>This
        database of quotations from Mises was prepared for <a href="/store/Quotable-Mises-The-P218C0.aspx">
                                                               <em>The Quotable Mises</em></a> edited by Mark Thornton, available from the
        <a href="/store/Quotable-Mises-The-P218C0.aspx">Mises Institute store
            for $20</a>. Send corrections to the <a href="mailto:mthornton@freecapitalists.org">editor</a>.
        Here is a <a href="/web/2015" target="_blank">source page</a> on
        the editions of the books referenced.
    </p>
    <p>
        <a href="/content/fun.asp#quote">Put Mises quotes on your site</a>.<br />
    </p>
    <strong>Search:</strong>
    <asp:TextBox ID="txtSearchQuery" runat="server"></asp:TextBox>
    <asp:Button ID="btnAskTheGuide" runat="server" Text="Go" />
    <br />
    <br />
    <div class="Filter">
        <strong>Browse By: </strong><a href="quotes.aspx?action=title"><strong>Title</strong></a><strong>
                                                                                                     | </strong><a href="quotes.aspx?action=subject"><strong>Subject</strong></a><strong>
                                                                                                                                                                                     | </strong><a href="quotes.aspx?action=author"><strong>Author</strong></a><strong> |
                                                                                                                                                                                                                                                               </strong><a href="quotes.aspx?action=source"><strong>Source</strong></a>
    </div>
    <div style="background-color: #f4f4f4; font-weight: bold;">
        Top&gt;
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </div>
    <asp:GridView ID="gvQuotes" PagerSettings-Position="TopAndBottom" runat="server"
                  AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" HeaderStyle-CssClass="gridheader"
                  PagerStyle-CssClass="pager" PageSize="30" UseAccessibleHeader="False" Width="660px">
        <Columns>
            <asp:BoundField DataField="Author" HeaderText="Author" SortExpression="Author" />
            <asp:BoundField DataField="Quote" HeaderText="Quote" SortExpression="Quote" />
            <asp:BoundField DataField="Source" HeaderText="Source" SortExpression="Source" />
            <asp:BoundField DataField="Page" HeaderText="Page" SortExpression="Page" />
            <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />
        </Columns>
        <PagerStyle CssClass="pager" />
        <HeaderStyle CssClass="gridheader" />
        <PagerSettings PageButtonCount="30" Position="TopAndBottom" />
    </asp:GridView>
    <asp:GridView HeaderStyle-CssClass="gridheader" PagerStyle-CssClass="pager" ID="gvAuthors"
                  runat="server" AutoGenerateColumns="False" Width="650px" UseAccessibleHeader="False">
        <Columns>
            <asp:TemplateField HeaderText="Author">
                <ItemTemplate>
                    <img src="/images/icons/icon_category.gif" alt="Folder" />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("AuthorId", "quotes.aspx?action=author&amp;Id={0}")%>'
                                   Text='<%#Eval("AuthorName")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pager" />
        <HeaderStyle CssClass="gridheader" />
    </asp:GridView>
    <asp:GridView HeaderStyle-CssClass="gridheader" PagerStyle-CssClass="pager" ID="gvSubjects"
                  runat="server" AutoGenerateColumns="False" Width="650px" UseAccessibleHeader="False">
        <Columns>
            <asp:TemplateField HeaderText="Subject">
                <ItemTemplate>
                    <img src="/images/icons/icon_category.gif" alt="Folder" />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("Subject", "quotes.aspx?action=subject&subject={0}")%>'
                                   Text='<%#Eval("Subject")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pager" />
        <HeaderStyle CssClass="gridheader" />
    </asp:GridView>
    <asp:GridView HeaderStyle-CssClass="gridheader" PagerStyle-CssClass="pager" ID="gvSource"
                  runat="server" AutoGenerateColumns="False" Width="650px" UseAccessibleHeader="False">
        <Columns>
            <asp:TemplateField HeaderText="Source">
                <ItemTemplate>
                    <img src="/images/icons/icon_category.gif" alt="Folder" />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("Source", "quotes.aspx?action=source&Source={0}")%>'
                                   Text='<%#Eval("Source")%>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pager" />
        <HeaderStyle CssClass="gridheader" />
    </asp:GridView>
</asp:Content>