﻿#region

using System;
using System.Web.UI;
using Mises.Domain.Feeds;
using Mises.Domain.Utility;

#endregion

public partial class upload : Page
{
    public void btnUpload_Click(object sender, EventArgs e)
    {
        //sPath = Server.MapPath(".")
        string sPath = Server.MapPath("Upload");
            
        if (!System.IO.Directory.Exists(sPath))
            System.IO.Directory.CreateDirectory(sPath);
        string sPathFriendly;

        //Upload to same path as script
        //Internet Anonymous User must have write permissions

        if (sPath.Substring(sPath.Length - 1) != "\\")
        {
            sPathFriendly = sPath; //Friendly path name for display
            sPath = sPath + "\\";
        }
        else
        {
            sPathFriendly = sPath.Substring(0, sPath.Length - 1);
        }


        //Save as same file name being posted
        //The code below resolves the file name
        //(removes path info)
        string sFile = uploadedFile.PostedFile.FileName;
        string[] sSplit = sFile.Split('\\');
        sFile = sSplit[sSplit.GetUpperBound(0)];
        string sFullPath = sPath + sFile;

        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;
        lblStatus.Text = "Uploaded to " + baseUrl + "/upload/" + sFile;

        try
        {
            uploadedFile.PostedFile.SaveAs(sFullPath);
            lblResults.Text = "Upload of File " + sFile + " to " + sPathFriendly + " succeeded";
            uploadedFile.Visible = false;
            txtName.Visible = false;
            EmailHelper.SendMail(Configuration.Email, "contact@freecapitalists.org", "New File Upload from " + txtName.Text.Trim(),
                                 "You have a new file upload from " + txtName.Text);
        }
        catch (Exception ex)
        {
            lblResults.Text = "Upload of File " + sFile + " to " + sPathFriendly + " failed for the following reason: " +
                              ex.Message;

            ExceptionLogging.LogException(Context, ex);
        }
        finally
        {
            lblResults.Font.Bold = true;
            lblResults.Visible = true;
        }
    }
}