﻿#region

using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Articles
{
    /// <summary>
    ///   Summary description for BrowseViewModel
    /// </summary>
    public class BrowseViewModel : BrowseViewModel<ArticleSummaryDTO>
    {
        public BrowseViewModel()
        {
        }

        public BrowseViewModel(PaginableResult<ArticleSummaryDTO> articles)
        {
            Mapper.Map(articles, this);
        }
    }
}