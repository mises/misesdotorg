﻿using System.Collections.Generic;
using Mises.Data;
using Mises.Web.Controllers;

namespace Mises.Web
{
    public class DailyViewModel
    {
        public DailyController.ViewFormat Format { get; set; }

        public string Title { get; set; }

        public DocumentAuthor Author { get; set; }

        public dynamic Articles { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int Total { get; set; }
    }
}