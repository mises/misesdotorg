using System.Data;

namespace Mises.Web.ViewModels
{
    public class SidebarViewModel
    {
        public DataTable Community { get; set; }

        public DataTable Blogs { get; set; }

        public DataTable BastiatBlog { get; set; }
    }
}
