﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mises.Web.ViewModels.Forms
{
    public class RegistrationFormViewModel
    {
        public RegistrationFormViewModel()
        {
            string[] monthsRegex = Regex.Split("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec", " ");
            Months = new List<SelectListItem>();
            for (int i = 0; i <= 11; i++)
            {
                var month = new SelectListItem {Value = (i + 1).ToString(), Text = monthsRegex[i]};

                Months.Add(month);
            }

            Years = new List<SelectListItem>();
            for (int i = 0; i <= 10; i++)
            {
                var year = new SelectListItem()
                {
                    Value = (DateTime.Now.Year + i).ToString(),
                    Text = (DateTime.Now.Year + i).ToString()
                };
                Years.Add(year);
            }
        }

        private const string error = " ";
        //[Required]
        //[DataType(DataType.EmailAddress)]
        //public string Name { get; set; }

        [Required(ErrorMessage = error)]
        [StringLength(100, MinimumLength = 2, ErrorMessage = error)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = error)]
        public string LastName { get; set; }

        [Required(ErrorMessage = error)]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Required(ErrorMessage = error)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.Currency, ErrorMessage = error)]
        [Range(0, 99999, ErrorMessage = error)]
        public decimal? CustomAmount { get; set; }

        [DataType(DataType.Currency,ErrorMessage = error)]
        [StringLength(10, MinimumLength = 2, ErrorMessage = error)]
        public string amount { get; set; }

        [Required(ErrorMessage = error)]
        public string CreditCardNumber { get; set; }

        

        [Required(ErrorMessage = error)]
        public string City { get; set; }

        [Required(ErrorMessage = error)]
        public string State { get; set; }

        [Required(ErrorMessage = error)]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = error)]
        public string Email { get; set; }

        public int SelectedMonth { get; set; }
        public List<SelectListItem> Months { get; set; }

        public int SelectedYear { get; set; }
        public List<SelectListItem> Years { get; set; }

        [Required(ErrorMessage = error)]
        [StringLength(4, MinimumLength = 2, ErrorMessage = error)]
        public string CardSecurityCode { get; set; }

        [Required(ErrorMessage = error)]
        public string Country { get; set; }

        [Display(Name = "Make this gift recurring every month")]
        public bool IsRecurring { get; set; }

        [Display(Name = "I wish to be anonymous")]
        public bool IsAnonymous { get; set; }

        public string Designation { get; set; }
    }
}