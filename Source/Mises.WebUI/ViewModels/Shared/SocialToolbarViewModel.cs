﻿#region

using System;
using System.Web;

#endregion

namespace Mises.Web.ViewModels
{
    public class SocialToolbarViewModel
    {
        public SocialToolbarViewModel(String url)
        {
            Url = url;
        }

        public String Url { get; set; }

        public String EncodedUrl
        {
            get { return HttpUtility.UrlEncode(Url); }
        }
    }
}