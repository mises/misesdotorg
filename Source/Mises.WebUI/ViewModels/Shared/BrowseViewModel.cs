﻿#region

using System;
using System.Web.Routing;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels
{
    public class BrowseViewModel<T> : PaginableResult<T>
    {
        public String QueryString;
        public RouteValueDictionary RouteValues;

        private int? _groupSize;

        private string _itemViewName;

        private string _listOrientation;

        private int? _scrollAmount;

        private string _theme;

        public BrowseViewModel()
        {
            _itemViewName = null;
            _theme = null;
            _listOrientation = null;
            _scrollAmount = null;
            _groupSize = null;
            new PaginableResult<T>();
        }

        public int GroupSize
        {
            get
            {
                int? nullable = _groupSize;
                return (nullable.HasValue ? nullable.GetValueOrDefault() : 1);
            }
            set { _groupSize = value; }
        }

        public string ItemViewName
        {
            get { return _itemViewName ?? "DocumentSummary"; }
            set { _itemViewName = value; }
        }

        public string ListOrientation
        {
            get { return _listOrientation ?? "horizontal"; }
            set { _listOrientation = value; }
        }

        public int ScrollAmount
        {
            get
            {
                int? nullable = _scrollAmount;
                return (nullable.HasValue ? nullable.GetValueOrDefault() : 2);
            }
            set { _scrollAmount = value; }
        }

        public string Theme
        {
            get { return _theme ?? "jcarousel-skin-black"; }
            set { _theme = value; }
        }

        public string Title { get; set; }
        public string TitleAppend { get; set; }
    }
}