﻿#region

using System;

#endregion

namespace Mises.Web.ViewModels
{
    /// <summary>
    ///   Summary description for BaseViewModel
    /// </summary>
    public abstract class BaseViewModel // : IViewModel
    {
        private String _title;

        public String Title
        {
            get { return _title ?? "Mises.org"; }
            set { _title = value; }
        }
    }
}