﻿#region

using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Documents
{
    /// <summary>
    ///   Summary description for BrowseViewModel
    /// </summary>
    public class BrowseViewModel : BrowseViewModel<DocumentDTO>
    {
        public BrowseViewModel()
        {
        }

        public BrowseViewModel(PaginableResult<DocumentDTO> documents)
        {
            Items = documents.Items;

            //Mapper.Map(documents, this);
            //this.Items = documents.Items.ToList();
        }
    }
}