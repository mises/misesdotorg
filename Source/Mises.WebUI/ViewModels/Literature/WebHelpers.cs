﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookCatalog.Models
{
    public static class WebHelpers
    {
        public static HtmlString GetTitle(this HtmlHelper htmlHelper)
        {
            string title = htmlHelper.ViewData["Title"] != null ? htmlHelper.ViewData["Title"].ToString() : "";

            var action = htmlHelper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue;
            if ((string)action != "Index")
            {

                title = action.ToString();
                var name = htmlHelper.ViewContext.Controller.ValueProvider.GetValue("name");
                var id = htmlHelper.ViewContext.Controller.ValueProvider.GetValue("id");
                var q = htmlHelper.ViewContext.Controller.ValueProvider.GetValue("q");


                if (name != null)
                {
                    title = action + ": " + name.RawValue.ToString();
                }
                else if (id != null)
                {
                    title = action + ": " + id.RawValue.ToString();
                }
                else if (q != null)
                {
                    string[] term = (string[])q.RawValue;
                    title = action + " for '" + term[0] + "'";
                }
            }

            return new HtmlString(title);
        }

    }
}