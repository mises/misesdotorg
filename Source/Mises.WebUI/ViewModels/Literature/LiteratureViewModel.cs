﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mises.Data;
using Mises.Data.DataModels;


namespace BookCatalog.Models
{
    public class LiteratureViewModel
    {
        

        public LiteratureViewModel()
        {
            Formats = new List<DocumentFile>();
        }

        public int DocumentId { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string URL { get { 
            
            if (DocumentId > 0)
            {
                return string.Format("~/document/{0}/{1}", DocumentId, Title.ToSlug());    
            }
            else
            {
                return Formats.Any() ? this.Formats.First().URL : string.Empty;
            }
            
            
        }
        }

      //  public string PurchaseURL { get; set; }

        public string Thumbnail { get; set; }

        public int? AuthorId { get; set; }

        public string Description { get; set; }

        public string PublicationInformation { get; set; }

       // public string MediaImage { get; set; }

        public DateTime EditDate { get; set; }

        public List<DocumentFile> Formats { get; set; }

        public int? CoAuthorId { get; set; }

        public string CoAuthor { get; set; }

        public string ISBN { get; set; }

        public Guid GUID { get; set; }

        public int? ProductId { get; set; }

        public string Price { get; set; }

        public string Keywords { get; set; }

        public DateTime LastUpdated
        {
            get {
                if (Formats.Count > 0)
                {
                    var fileAdded = Formats.OrderByDescending(f => f.CreateDate).FirstOrDefault().CreateDate;
                    if (fileAdded > EditDate) return fileAdded;
                }
                
                return EditDate;
                
            }
            
        }

        public string TitleForSort
        {
            get
            {
                char[] chars = {'"', '.','\''};
                string sortedName = Title.Trim(chars);

                if (sortedName.StartsWith("The "))
                    sortedName = sortedName.Remove(4);

                if (sortedName.StartsWith("A "))
                    sortedName = sortedName.Remove(2);

                return sortedName;
            }
        }
    }

    //public class FileModel
    //{
    //    public string URL { get; set; }

    //    public int MediaTypeId { get; set; }

    //    public string MediaImage { get; set; }

    //    public string FileType { get; set; }

    //    public long FileSize { get; set; }

    //    public decimal Duration { get; set; }
    //}

}