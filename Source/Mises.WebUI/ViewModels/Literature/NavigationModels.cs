﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Web.Mvc;
using System.Linq;
using Mises.Data;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core.Objects;

namespace BookCatalog.Models
{
    public static class NavigationModels
    {
        public static IEnumerable<KeyValuePair<int, string>> Authors(int max = 20)
        {
            var model = DataHelper.EntityDataModel;
            model.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            var anonAuthors = from doc in model.Documents
                              group doc by doc.DocumentAuthor
                                  into names
                                  let author = names.FirstOrDefault().DocumentAuthor
                                  select new
                                             {
                                                 AuthorId = names.Key,
                                                 Count = names.Count(),
                                                 author.AuthorFirst,
                                                 author.AuthorMiddle,
                                                 author.AuthorLast
                                             };

            var authors = anonAuthors.OrderByDescending(a=> a.Count).Take(max).ToDictionary(d => d.AuthorId.AuthorId, d => (d.AuthorFirst + ' ' + d.AuthorMiddle + ' ' + d.AuthorLast).ToString()).Take(max);
            //model.Documents.GroupBy(d=> d.Author1).Select(d => new { d.Author1, d.Author }).ToDictionary(d => d.Author1, d => d.Author).Take(100);

            return authors;
        }

        public static IEnumerable<KeyValuePair<int, string>> Categories()
        {
            var model = DataHelper.EntityDataModel;
            model.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            var authors = model.DocumentSubjects.Where(v => v.Visible == true).Select(d => new { d.SubjectId, d.Subject }).Distinct().ToDictionary(d => d.SubjectId, d => d.Subject).Take(100);

            return authors;
        }

        public static IEnumerable<string> Sources()
        {
            var model = DataHelper.EntityDataModel;
            model.Documents.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            var sources = model.Documents.Where(d => d.Source.Length > 0).Where(s=> !s.Source.EndsWith("wmv")).Select(d => d.Source).Distinct().Take(50).ToList();
            sources.Insert(0, "Books");
            return sources;
        }

        public static SelectList SourcesList()
        {
            return new SelectList(Sources(), "Key", "Key");


        }

        public static List<DocumentMediaType> Formats()
        {
            var model = DataHelper.EntityDataModel;
            model.DocumentMediaTypes.MergeOption= System.Data.Objects.MergeOption.NoTracking;
            var formats = model.DocumentMediaTypes.ToList();

            return formats;
        }

        private static List<DocumentMediaType> mediaTypes;

        public static List<DocumentMediaType> MediaTypes
        {
            get
            {
                if (mediaTypes != null) return mediaTypes;

                var model = DataHelper.EntityDataModel;
                model.DocumentMediaTypes.MergeOption= System.Data.Objects.MergeOption.NoTracking;
                var formats = model.DocumentMediaTypes;
                mediaTypes = formats.ToList();
                return mediaTypes;
            }
        }

        public static DocumentMediaType GetMediaTypeFromId(int mediaTypeId)
        {
            var mediatype = MediaTypes.SingleOrDefault(i => i.MediaTypeID == mediaTypeId);

            if (mediatype == null)
            {
                mediatype = new DocumentMediaType() {Description = "Missing media type!",MediaType = ""};
            }
            return mediatype;

        }
    }
}