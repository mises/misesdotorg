﻿#region

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Authors
{
    /// <summary>
    ///   Summary description for AuthorsViewModel
    /// </summary>
    public class BrowseViewModel : BrowseViewModel<AuthorDTO>
    {
        public readonly String AlphabetChars = "abcdefghijklmnopqrstuvwxyz";

        public BrowseViewModel()
        {
            Alphabet = new List<SelectListItem>();

            foreach (var c in AlphabetChars)
            {
                Alphabet.Add(new SelectListItem {Text = c.ToString().ToUpper(), Value = "0", Selected = false});
            }
        }

        public IList<SelectListItem> Alphabet { get; set; }
    }
}