﻿#region

using System;
using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Authors
{
    /// <summary>
    ///   Summary description for BrowseViewModel
    /// </summary>
    public class DetailViewModel : BaseViewModel
    {
        public DetailViewModel()
        {
        }

        public DetailViewModel(AuthorDTO author)
        {
            Mapper.Map(author, this);
        }

        public Int32 Id { get; set; }
        public String FullName { get; set; }
        public String BioInfoHtml { get; set; }
        public String PhotoUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public PaginableResult<ArticleSummaryDTO> Articles { get; set; }
        public PaginableResult<DocumentDTO> Documents { get; set; }
        public PaginableResult<DocumentDTO> Publications { get; set; }

        public MisesDTO Featured { get; set; }

        public Int32 TotalCount
        {
            get { return Articles.TotalItems + Documents.TotalItems + Publications.TotalItems; }
        }
    }
}