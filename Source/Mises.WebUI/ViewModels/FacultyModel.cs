using System.Collections.Generic;

namespace Mises.Web.ViewModels
{
    public class FacultyModel
    {
        public List<Data.Faculty> SeniorFaculty { get; set; }

        public List<Data.Faculty> AdjunctFaculty { get; set; }

        public List<Data.Faculty> Staff { get; set; }

        public List<Data.Faculty> AssociatedScholars { get; set; }
    }
}