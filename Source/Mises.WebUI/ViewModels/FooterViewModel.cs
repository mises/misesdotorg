using System.Linq;
using Mises.Data;

namespace Mises.Web.ViewModels
{
    public class FooterViewModel
    {
        public IQueryable<Document> Literature { get; set; }

        public IQueryable<Document> Audio { get; set; }

        public IQueryable<Document> Video { get; set; }

        public QuoteGetRandomResult Quote { get; set; }
    }
}