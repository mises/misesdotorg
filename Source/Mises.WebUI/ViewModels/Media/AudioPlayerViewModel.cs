﻿#region

using System;
using System.Web;
using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    ///<summary>
    ///</summary>
    public class AudioPlayerViewModel : BaseViewModel
    {
        private String _guid = String.Empty;

        public AudioPlayerViewModel(HttpBrowserCapabilitiesBase browser, AudioDTO audio)
        {
            Browser = browser;

            Mapper.Map(audio, this);
        }

        public String Guid
        {
            get
            {
                if (String.IsNullOrEmpty(_guid))
                    _guid = System.Guid.NewGuid().ToString();

                return _guid;
            }
        }

        public HttpBrowserCapabilitiesBase Browser { get; set; }

        public Int32 Id { get; set; }
        public String MimeType { get; set; }
        public String Url { get; set; }

        //TODO revise these when more info about browser/version support for which containers/codecs etc
        public Boolean CanPlayNatively
        {
            get
            {
                var isCapable = false;

                if (Browser.Browser == "Firefox")
                {
                    if ((MimeType != AudioTypes.Mp3) && (MimeType != AudioTypes.Mp4a))
                        isCapable = true;
                }
                else
                    isCapable = true;

                return isCapable;
            }
        }
    }
}