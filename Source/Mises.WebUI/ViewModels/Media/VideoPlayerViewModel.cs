﻿#region

using System;
using System.Web;
using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    ///<summary>
    ///</summary>
    public class VideoPlayerViewModel : BaseViewModel
    {
        private String _guid = String.Empty;

        public VideoPlayerViewModel(HttpBrowserCapabilitiesBase browser, VideoDTO video)
        {
            Browser = browser;

            Mapper.Map(video, this);
        }

        public String Guid
        {
            get
            {
                if (String.IsNullOrEmpty(_guid))
                    _guid = System.Guid.NewGuid().ToString();

                return _guid;
            }
        }

        public HttpBrowserCapabilitiesBase Browser { get; set; }

        public Int32 Id { get; set; }
        public String MimeType { get; set; }
        public String Url { get; set; }
        public String PosterUrl { get; set; } /*{ return String.Concat("/media/poster/", this.Id); }*/

        public Boolean IsYoutubeClip
        {
            get { return (MimeType == VideoTypes.Youtube); }
        }

        //TODO revise these when more info about browser/version support for which containers/codecs etc
        public Boolean CanPlayNatively
        {
            get
            {
                //ASSUMPTION: All mobile devices support html5 (better chance of this than supporting silverlight!).
                if (Browser.IsMobileDevice && (MimeType != VideoTypes.Wmv))
                    return true;

                Boolean isCapable = false;

                /*TODO is it worth doing opera (0.38% market at time of writing)?
                 *http://diveintohtml5.org/video.html
                CODECS/CONTAINER	IE		FIREFOX	SAFARI	CHROME	OPERA	IPHONE	ANDROID
                Theora+Vorbis+Ogg	·			3.5+			†			5.0+		10.5+		·				·
                H.264+AAC+MP4			9.0+	·				3.0+			·				·			3.0+		2.0+
                WebM							9.0+*	4.0+			†			6.0+		10.6+		·				2.3‡
                */

                switch (Browser.Browser)
                {
                    case "Chrome":
                        if ((Browser.MajorVersion >= 5) && (MimeType == VideoTypes.Ogg))
                            isCapable = true;
                        else if ((Browser.MajorVersion >= 6) && (MimeType == VideoTypes.Webm))
                            isCapable = true;
                        else if (MimeType == VideoTypes.Mp4)
                            //TODO Added support for mp4 in v[when?] then withdrew in v[when - not yet done at time of writing?] (H.264 licensing issue)
                            isCapable = true;
                        break;
                    case "Firefox":
                        if ((Browser.MajorVersion >= 3) && (Browser.MinorVersion >= 5) && (MimeType == VideoTypes.Ogg))
                            isCapable = true;
                        else if ((Browser.MajorVersion >= 4) &&
                                 ((MimeType == VideoTypes.Ogg) || (MimeType == VideoTypes.Webm)))
                            isCapable = true;
                        break;
                    case "Opera":
                        if ((Browser.MajorVersion >= 10) && ((MimeType == VideoTypes.Ogg) || (MimeType == VideoTypes.Webm)))
                            isCapable = true;
                        break;
                    case "IE":
                        if ((Browser.MajorVersion >= 9) && ((MimeType == VideoTypes.Mp4) || (MimeType == VideoTypes.Webm)))
                            isCapable = true;
                        break;
                    case "Safari":
                        if ((Browser.MajorVersion >= 3) && (MimeType == VideoTypes.Mp4))
                            isCapable = true;
                        break;
                }

                return isCapable;
            }
        }

        public Boolean CanPlayInFlash
        {
            get
            {
                Boolean isCapable = false;

                if ((MimeType == VideoTypes.Swf) || (MimeType == VideoTypes.Flv))
                    //|| (this.VideoType == VideoTypes.Mp4))
                    isCapable = true;

                return isCapable;
            }
        }

        public Boolean CanPlayInSilverlight
        {
            get
            {
                Boolean isCapable = false;

                if ((MimeType == VideoTypes.Wmv) || (MimeType == VideoTypes.Stream) || (MimeType == VideoTypes.Mp4))
                    isCapable = true;

                return isCapable;
            }
        }
    }
}