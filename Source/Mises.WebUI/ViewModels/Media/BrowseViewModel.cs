﻿#region

using System;
using System.Collections.Generic;
using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    /// <summary>
    ///   Summary description for BrowseViewModel
    /// </summary>
    public class BrowseViewModel : BrowseViewModel<MisesDTO>
    {
        public BrowseViewModel()
        {
        }

        public BrowseViewModel(PaginableResult<MisesDTO> documents)
        {
            Mapper.Map(documents, this);
        }

        public BrowseViewModel(PaginableResult<DocumentDTO> documents)
        {
            Mapper.Map(documents, this);
        }

        public MediaPreferenceTypes Preference { get; set; }
        public MisesDTO Featured { get; set; }

        public String GetMediaSummary()
        {
            String result = String.Empty;

            if (TotalItems > 0 && (Counts as IDictionary<string, object>).ContainsKey("Video"))
            {
                var videocount = Counts.Video;
                var audiocount = Counts.Audio;
                //var litcount = this.Counts.Literature;
                var contribdetails = String.Empty;

                if (videocount > 0)
                    result += String.Format("{0} video", videocount);

                if (audiocount > 0)
                    result += String.Format("{0}{1} audio", (videocount > 0) ? ", " : String.Empty, audiocount);

                //if (litcount > 0)
                //result += String.Format("{0}{1} literature", (videocount + audiocount > 0) ? ", " : String.Empty, litcount);
            }

            return result;
        }
    }
}