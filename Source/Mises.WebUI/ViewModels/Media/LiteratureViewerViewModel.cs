﻿#region

using System;
using System.Web;
using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    ///<summary>
    ///</summary>
    public class LiteratureViewerViewModel : BaseViewModel
    {
        public LiteratureViewerViewModel(HttpBrowserCapabilitiesBase browser, LiteratureDTO literature)
        {
            Browser = browser;

            Mapper.Map(literature, this);
        }

        public HttpBrowserCapabilitiesBase Browser { get; set; }

        public Int32 Id { get; set; }
        public String MimeType { get; set; }
        public String Url { get; set; }

        //TODO revise these when more info about browser/version support for which containers/codecs etc
        public Boolean CanViewNatively
        {
            get { return true; }
        }
    }
}