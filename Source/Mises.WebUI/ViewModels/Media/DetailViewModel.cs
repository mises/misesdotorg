﻿#region

using System;
using System.Collections.Generic;
using AutoMapper;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    /// <summary>
    ///   Summary description for BrowseViewModel
    /// </summary>
    public class DetailViewModel : BaseViewModel
    {
        public DetailViewModel()
        {
        }

        public DetailViewModel(DocumentDTO document) : this(MediaPreferenceTypes.Video, document)
        {
        }

        public DetailViewModel(MediaPreferenceTypes preference, DocumentDTO document)
        {
            Preference = preference;

            Mapper.Map(document, this);
        }

        public MediaPreferenceTypes Preference { get; set; }

        // Pretty much one to one with DocumentDTO...
        public Int32 Id { get; set; }
        public Guid Identifier { get; set; }
        public String PublishInfoHtml { get; set; }
        public String DescriptionHtml { get; set; }
        public DateTime CreatedDate { get; set; }
        public CategorySummaryDTO Category { get; set; }
        public IList<AuthorSummaryDTO> Authors { get; set; }
        public IList<MediaDTO> Media { get; set; }

        // The 'more' lists...
        public PaginableResult<DocumentDTO> AuthorDocuments { get; set; }
    }
}