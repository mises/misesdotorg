﻿#region

using System;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    /// <summary>
    ///   Summary description for BrowseViewModel
    /// </summary>
    public class SearchViewModel : BrowseViewModel<MisesDTO>
    {
        public String Term { get; set; }
        public Boolean Errored { get; set; }
    }
}