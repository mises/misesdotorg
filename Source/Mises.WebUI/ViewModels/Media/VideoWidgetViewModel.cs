﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mises.Domain.Contracts;

#endregion

namespace Mises.Web.ViewModels.Media
{
    ///<summary>
    ///</summary>
    public class VideoWidgetViewModel : DetailViewModel
    {
/*
        private String _guid = String.Empty;
*/
        private VideoDTO _video;

/*
        public String Guid
        {
            get
            {
                if (String.IsNullOrEmpty(_guid))
                    _guid = System.Guid.NewGuid().ToString();

                return _guid;
            }
        }
*/

        public HttpBrowserCapabilities Browser { get; set; }

        public VideoDTO Video
        {
            get { return _video ?? (_video = Media.OfType<VideoDTO>().GetBestFormat()); }
        }

        public Boolean IsYoutubeClip
        {
            get { return (Video.MimeType == VideoTypes.Youtube); }
        }

        //TODO revise these when more info about browser/version support for which containers/codecs etc
        public Boolean CanPlayNatively
        {
            get
            {
                Boolean isCapable = false;

                /*TODO is it worth doing opera (0.38% market at time of writing)? and mobile should be being handled elsewhere...
				CODECS/CONTAINER	IE		FIREFOX	SAFARI	CHROME	OPERA	IPHONE	ANDROID
				Theora+Vorbis+Ogg	·			3.5+			†			5.0+		10.5+		·				·
				H.264+AAC+MP4			9.0+	·				3.0+			·				·			3.0+		2.0+
				WebM							9.0+*	4.0+			†			6.0+		10.6+		·				2.3‡
				*/

                switch (Browser.Browser)
                {
                    case "Chrome":
                        if ((Browser.MajorVersion >= 5) && (Video.MimeType == VideoTypes.Ogg))
                            isCapable = true;
                        else if ((Browser.MajorVersion >= 6) && (Video.MimeType == VideoTypes.Webm))
                            isCapable = true;
                        else if (Video.MimeType == VideoTypes.Mp4)
                            //TODO Added support for mp4 in v[when?] then withdrew in v[when - not yet done at time of writing?] (H.264 licensing issue)
                            isCapable = true;
                        break;
                    case "Firefox":
                        if ((Browser.MajorVersion >= 3) && (Browser.MinorVersion >= 5) && (Video.MimeType == VideoTypes.Ogg))
                            isCapable = true;
                        else if ((Browser.MajorVersion >= 4) &&
                                 ((Video.MimeType == VideoTypes.Ogg) || (Video.MimeType == VideoTypes.Webm)))
                            isCapable = true;
                        break;
                    case "IE":
                        if ((Browser.MajorVersion >= 9) &&
                            ((Video.MimeType == VideoTypes.Mp4) || (Video.MimeType == VideoTypes.Webm)))
                            isCapable = true;
                        break;
                    case "Safari":
                        //TODO tested latest safari and wouldn't play mp4 so for now...
                        //if ((this.Browser.MajorVersion >= 3) && (this.Video.MimeType == VideoTypes.Mp4))
                        //isCapable = true;
                        break;
                }

                return isCapable;
            }
        }

        public Boolean CanPlayInFlash
        {
            get
            {
                bool isCapable = (Video.MimeType == VideoTypes.Swf) || (Video.MimeType == VideoTypes.Flv);

                return isCapable;
            }
        }

        public Boolean CanPlayInSilverlight
        {
            get
            {
                bool isCapable = (Video.MimeType == VideoTypes.Wmv) || (Video.MimeType == VideoTypes.Mp4);

                return isCapable;
            }
        }
    }
}