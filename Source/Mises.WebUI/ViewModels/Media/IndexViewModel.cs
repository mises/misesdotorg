﻿#region

using System.Collections.Generic;
using Mises.Domain.Contracts;
using System.Web.Mvc.Html;
using System.Linq;
using System.Web.Mvc;
using System;

#endregion

namespace Mises.Web.ViewModels.Media
{
    public class IndexViewModel : BaseViewModel
    {
		public IndexViewModel()
		{
			SubjectId = CategoryId = AuthorId = -1;
		}

        public IEnumerable<AuthorSummaryDTO> Authors { get; set; }

        public IEnumerable<CategorySummaryDTO> Categories { get; set; }

        public BrowseViewModel<CategorySummaryDTO> CategoryBreadcrumb { get; set; }

        public BrowseViewModel<CategorySummaryDTO> CategoryChildren { get; set; }

        public BrowseViewModel Documents { get; set; }

        public bool Errored { get; set; }

        public string FeedAppend { get; set; }

        public string FilterPrepend { get; set; }

        public string FilterText { get; set; }

        public IEnumerable<SubjectSummaryDTO> Subjects { get; set; }

		public int SubjectId { get; set; }

		public int CategoryId { get; set; }

		public int AuthorId { get; set; }

		public bool AdvancedOptionSelected
		{
			get
			{
				return AuthorId >= 0 || SubjectId >= 0 || CategoryId >= 0;
			}
		}
    }
}