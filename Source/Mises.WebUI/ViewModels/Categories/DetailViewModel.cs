﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AutoMapper;

using Mises.Domain.Contracts;

namespace Mises.Web.ViewModels.Categories
{
	/// <summary>
	/// Summary description for IndexViewModel
	/// </summary>
	public class DetailViewModel : BaseViewModel
	{
		public BrowseViewModel<CategorySummaryDTO> Breadcrumb { get; set; }
		public BrowseViewModel<CategorySummaryDTO> Children { get; set; }
		public Media.BrowseViewModel Documents { get; set; }

		public DetailViewModel()
		{
		}
	}
}