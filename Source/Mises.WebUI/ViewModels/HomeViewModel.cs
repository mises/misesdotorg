﻿#region

using System.Collections.Generic;
using System.Data;
using System.Linq;
using BookCatalog.Models;
using Mises.Data;

#endregion

namespace Mises.Web.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        public IEnumerable<DailyArticlesGetTodaysSummaryResult> FeaturedArticle { get; set; }

        public IEnumerable<DailyArticlesGetTodaysSummaryResult> TodaysArticles { get; set; }

        public List<StoreGetBestsellersResult> StoreItems { get; set; }

        public List<AcademyCourse> Courses { get; set; }

        public List<CalendarGetUpcomingEventsResult> Events { get; set; }

        public List<MediaGetLatestAudioResult> Audio { get; set; }

        public DataTable BlogDataset { get; set; }

        public List<LiteratureViewModel> Books { get; set; }

        public DataTable BastiatDataset { get; set; }
    }
}