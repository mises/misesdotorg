﻿using System.Collections.Generic;
using System.Linq;
using Mises.Data;

namespace Mises.Web.ViewModels
{
    public class EventsViewModel
    {
        public IQueryable<Mises.Data.Calendar> UpcomingEvents { get; set; }

        public List<Mises.Data.Calendar> PastEvents { get; set; }
    }
}