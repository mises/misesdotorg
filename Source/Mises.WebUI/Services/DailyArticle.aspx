<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ OutputCache Duration="180" VaryByParam="*" %>
<%@ Import Namespace="Mises.Domain.Articles" %>
<script runat="server" visible="true">


    private void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        IEnumerable<DailyArticle> dailies = Mises.Domain.Articles.DailyArticle.GetTodaysArticles();
        var daily = dailies.First();
        lblAuthor.Text = daily.AuthorName;
        lblDate.Text = DateTime.Today.ToLongDateString();
        lblDescription.Text = daily.Description;
        linkStoryTitle.NavigateUrl = daily.URL;
        linkStoryTitle.Text = daily.Title;

        DailyArticle.Width = Request.QueryString["size"] != ""
                                 ? Request.QueryString["size"]
                                 : Convert.ToString(350);
    }


</script>
<table width="200" border="1" cellspacing="0" cellpadding="1" id="DailyArticle" runat="server"
       style="border-color: #CCCCCC">
    <tr>
        <th style="font-family: 'Verdana', 'Arial', 'Helvetica', 'sans-serif'; font-size: 8pt;"
            class="DailyHeader">
            DAILY ARTICLE |
            <asp:label id="lblDate" runat="server"></asp:label>
        </th>
    </tr>
    <tr>
        <td class="DailyBody">
            <strong>
                <asp:hyperlink id="linkStoryTitle" target="_blank" runat="server"></asp:hyperlink>
            </strong>
            <br />
            <em>
                <asp:label id="lblAuthor" runat="server"></asp:label>
            </em>
            <p>
                <asp:label id="lblDescription" runat="server"></asp:label>
            </p>
        </td>
    </tr>
</table>