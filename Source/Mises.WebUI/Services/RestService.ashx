<%@ WebHandler Language="C#" Class="RestService" %>

#region

using System.Web;

#endregion

public class RestService : IHttpHandler

{
    #region IHttpHandler Members

    public void ProcessRequest(HttpContext context)
    {
        context.Response.Redirect("http://mises.freecapitalists.org//MisesShop/RandomBook");
    }

    public bool IsReusable
    {
        get { return true; }
    }

    #endregion
}