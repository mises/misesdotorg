﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Documents;
using Mises.Domain.Media;

#endregion

partial class Services_MediaEmbed : Page
{
    public string PreviewURL;
    public string URL;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.EnableViewState = false;
        var mediaId = (int) Conversion.Val(Request.QueryString["MediaId"]);
        ContentDocument media = (mediaId == 0)
                                    ? new ContentDocument((int) Conversion.Val(Request.QueryString["Id"]))
                                    : ContentDocument.GetDocumentByFileId(mediaId);

        Page.Title = media.Title;
        if (media.DocumentFileList.Count == 0) return;

        DocumentFile mediaFile = mediaId > 0
                                  ? media.DocumentFileList.FindLast(p => p.FileId == mediaId)
                                  : media.DocumentFileList[0];
        URL = mediaFile.URL;
        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;
        PreviewURL = string.Format("{0}/media/poster/{1}", baseUrl, media.Id);

        if (mediaFile.URL.EndsWith(".mp3"))
        {
            var lit = new Literal
                          {
                              Text = string.Format(
                                  @"<script type=""text/JavaScript"" src=""{0}/Controls/Media/JW/swfobject.js""></script>
<embed allowfullscreen=""true"" allowscriptaccess=""always"" flashvars=""&file={1}&height=20&width=320&autostart=true"" height=""20"" src=""{2}/Controls/Media/JW/mediaplayer.swf"" width=""320"">
</embed>",
                                  baseUrl, mediaFile.URL, baseUrl),
                              EnableViewState = false
                          };
            Page.Form.Controls.Add(lit);

            mediaspace.Visible = false;
        }
        else if (mediaFile.URL.EndsWith(".pdf"))
        {
            var lit = new Literal
                          {
                              Text = string.Format(
                                  @"<iframe src=""{0}"" width=""640"" style=""height:480px"" align=""left""
                    >[Your browser does <em>not</em> support <code>iframe</code>,
                    or has been configured not to display inline frames.
                    You can access <a href=""{0}"">the document</a> via a link though.]</iframe>",
                                  mediaFile.URL)
                          };
            Page.Form.Controls.Add(lit);

            mediaspace.Visible = false;
        }
    }
}