<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update TMS</title>
</head>
<body>
   
<?php

ini_set('display_errors',1);
error_reporting(E_ALL|E_STRICT);

  function do_post_request($url, $data, $optional_headers = null)
  {
			//echo ("calling $url?$data \n");
     $params = array('http' => array(
                  'method' => 'POST',
                  'content' => $data
               ));
     if ($optional_headers !== null) {
        $params['http']['header'] = $optional_headers;
     }
     $ctx = stream_context_create($params);
     $fp = @fopen($url, 'rb', false, $ctx);
     if (!$fp) {
        throw new Exception("Problem with $url, $php_errormsg");
     }
     $response = @stream_get_contents($fp);
     if ($response === false) {
        throw new Exception("Problem reading data from $url, $php_errormsg");
     }
     return $response;
  }

set_time_limit(295);

//Connect to DB
$db = mysql_connect("data.mises.org", "graphs","lvmigraphman") or die(mysql_error()); 
mysql_select_db("graphs",$db) or die(mysql_error());

//Array of units
$units  = array(
	"lin",
	"chg",
	"ch1",
	"pch",
	"pc1",
	"pca",
	"cch",
	"cca",
	"log"
);

//Array of series to update
$series = array(
	"MZMNS",
	"M1NS",
	"M2NS",
	//"M3NS"
	"CURRNS",
	"DDDFCBNS",
	"DDDFOINS",
	"SAVINGNS",
	"TCDNS",
	"USGVDDNS"
);

//Cycle through each series
foreach ($series as $thisseries) {
	//Build URL
	$url = 'http://api.stlouisfed.org/fred/series/observations';
	$SQLstm = "SELECT MAX(datemonth) from series_". strtolower($thisseries);
	$result = mysql_query($SQLstm);
	if (!$result)
	{
		$date = '1776-07-04';
	} else
	{
		$row = mysql_fetch_row($result);
		$date = $row[0]; //get datemonth;
	}
	

	foreach ($units as $thisunit)
	{
		

		//Build query
		$data = array(
			      'units'		=>$thisunit,
			      'observation_start'	=>$date,
			      'observation_end'	=>'9999-12-31',
			      'series_id' => $thisseries,
				  'api_key' => 'fd4fbed97026835b3fa89d14eab02736'
			      );

		//Download latest data
		$feddata = do_post_request($url, http_build_query($data));

		$xml = new SimpleXMLElement($feddata);
		//sample data looks like this:
		
		//<observations realtime_start="2010-01-18" realtime_end="2010-01-18"
			//observation_start="1776-07-04" observation_end="9999-12-31"
			//units="lin" output_type="1" file_type="xml"
			//order_by="observation_date" sort_order="asc" count="612"
			//offset="0" limit="100000">
		//<observation realtime_start="2010-01-18" realtime_end="2010-01-18" date="1959-01-01" value="142.2"/>
	
		#lower case series name becuase the mysql tables are case sensitive.
		$thisseries = strtolower($thisseries);	


		#for all values in observation set
		foreach ($xml->observation as $value)
		{
			if ($value['value'] == '.'){ /*echo("skipping $value[value]");*/ continue; }
			#create SQL statment to insert the value appropriately.
			if ($thisunit == 'lin')
			{
				$SQLstm = "INSERT INTO series_$thisseries (datemonth, lin) VALUES ('$value[date]','$value[value]')";
			} else
			{
				$SQLstm = "UPDATE series_$thisseries SET $thisunit = '$value[value]' WHERE datemonth LIKE '$value[date]'";
			}
			#call statment and print error if it fails.
			$result = mysql_query($SQLstm);
			if (!$result)
			{
				echo('Invalid query: ' . mysql_error() . '<br>');
			} else
			{
				//echo("inserted data $value[date]/$value[value] in $thisseries/$thisunit\n");
			}
		}
	
	}
}

echo "If this is the only thing you see, it worked. <br />";

?>

<br />
   <img src="http://mises.org/content/nofed/makegraph.php?tms=true&range=max&bars=true&size=med&unit=lin">


</body>
</html>