#region

using System;
using System.Configuration;
using System.Web.UI;

#endregion

partial class Chart : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btn_make.Click += btn_make_Click;
        btn_download.Click += btn_download_Click;

        if (! IsPostBack)
        {
            string series = Request.QueryString["series"];
            if (! (string.IsNullOrEmpty(series)))
            {
                cb_tms.Checked = false;
                cb_mzm.Checked = false;
                cb_m1.Checked = false;
                cb_m2.Checked = false;
                cb_m3.Checked = false;

                switch (series.ToLower())
                {
                    case "tms":
                        cb_tms.Checked = true;
                        break;
                    case "mzm":
                        cb_mzm.Checked = true;
                        break;
                    case "m1":
                        cb_m1.Checked = true;
                        break;
                    case "m2":
                        cb_m2.Checked = true;
                        break;
                    case "m3":
                        cb_m3.Checked = true;
                        break;
                }
            }

            if (cb_tms.Checked)
            {
                lbl_desc.Text =
                    "The True Money Supply (TMS) was formulated by Murray Rothbard and represents the amount of money in the economy that is available for immediate use in exchange. It has been referred to in the past as the Austrian Money Supply, the Rothbard Money Supply and the True Money Supply. The benefits of TMS over conventional measures calculated by the Federal Reserve are that it counts only immediately available money for exchange and does not double count.  MMMF shares are excluded from TMS precisely because they represent equity shares in a portfolio of highly liquid, short-term investments which must be sold in exchange for money before such shares can be redeemed.  For a detailed description and explanation of the TMS aggregate, see <a href=\"http://mises.freecapitalists.org//journals/aen/aen6_4_1.pdf\">Salerno (1987)</a> and <a href=\"http://mises.freecapitalists.org//journals/qjae/pdf/qjae3_4_3.pdf\">Shostak (2000)</a>.  The TMS consists of the following: Currency Component of M1, Total Checkable Deposits, Savings Deposits, U.S. Government Demand Deposits and Note Balances, Demand Deposits Due to Foreign Commercial Banks, and Demand Deposits Due to Foreign Official Institutions.";
            }


            img_chart.ImageUrl = GetImageUrl();
        }
        lbl_heading.Text = FormatSeriesTitle();
    }

    private string FormatSeriesTitle()
    {
        string str = "";
        if (cb_tms.Checked)
        {
            str += ", " + ConfigurationManager.AppSettings.Get("Title.TMS");
        }
        if (cb_mzm.Checked)
        {
            str += ", " + ConfigurationManager.AppSettings.Get("Title.MZM");
        }
        if (cb_m1.Checked)
        {
            str += ", " + ConfigurationManager.AppSettings.Get("Title.M1");
        }
        if (cb_m2.Checked)
        {
            str += ", " + ConfigurationManager.AppSettings.Get("Title.M2");
        }
        if (cb_m3.Checked)
        {
            str += ", " + ConfigurationManager.AppSettings.Get("Title.M3");
        }
        if (! (string.IsNullOrEmpty(str)))
        {
            str = "Series:  " + str.Substring(1);
        }
        return str;
    }

    protected void btn_make_Click(object sender, EventArgs e)
    {
        img_chart.ImageUrl = GetImageUrl();
    }

    protected void btn_download_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetDataUrl());
    }

    private string GetImageUrl()
    {
        //'http://www.smises.org/content/nofed/makegraph.php?tms=true&mzm=true&m1=true&m2=true&m3=true&unit=lin&range=5&bars=false&size=med&Make+Graph=make

        string qs = "";
        if (cb_tms.Checked)
        {
            qs += "&tms=true";
        }
        if (cb_mzm.Checked)
        {
            qs += "&mzm=true";
        }
        if (cb_m1.Checked)
        {
            qs += "&m1=true";
        }
        if (cb_m2.Checked)
        {
            qs += "&m2=true";
        }
        if (cb_m3.Checked)
        {
            qs += "&m3=true";
        }
        if (! (string.IsNullOrEmpty(qs)))
        {
            qs = qs.Substring(1);
        }
        else if (! (string.IsNullOrEmpty(Request.QueryString["series"])))
        {
            qs = Request.QueryString["series"] + "=true";
        }
        else
        {
            lbl_error.Visible = true;
            return null;
        }
        lbl_error.Visible = false;
        qs += "&unit=" + ddl_units.SelectedValue;
        qs += "&range=" + rbl_range.SelectedValue;
        qs += "&bars=" + rbl_bars.SelectedValue;
        qs += "&size=" + rbl_size.SelectedValue;
        qs += "&start=" + tb_startdate.Text;
        qs += "&end=" + tb_enddate.Text;
        //qs += "&Make+Graph=make";
        return ConfigurationManager.AppSettings["MakeGraphURL"] + "?" + qs;
    }

    private string GetDataUrl()
    {
        string qs = "";
        if (cb_tms.Checked)
        {
            qs += "&tms=true";
        }
        if (cb_mzm.Checked)
        {
            qs += "&mzm=true";
        }
        if (cb_m1.Checked)
        {
            qs += "&m1=true";
        }
        if (cb_m2.Checked)
        {
            qs += "&m2=true";
        }
        if (cb_m3.Checked)
        {
            qs += "&m3=true";
        }
        if (! (string.IsNullOrEmpty(qs)))
        {
            qs = qs.Substring(1);
        }
        else if (! (string.IsNullOrEmpty(Request.QueryString["series"])))
        {
            qs = Request.QueryString["series"] + "=true";
        }
        else
        {
            lbl_error.Visible = true;
            return null;
        }
        lbl_error.Visible = false;
        qs += "&bars=" + rbl_bars.SelectedValue;
        qs += "&size=" + rbl_size.SelectedValue;
        qs += "&unit=" + ddl_units.SelectedValue;
        qs += "&range=" + rbl_range.SelectedValue;
        qs += "&start=" + tb_startdate.Text;
        qs += "&end=" + tb_enddate.Text;
        //qs += "&Make+Graph=make";
        return ConfigurationManager.AppSettings["DownloadURL"] + "?" + qs;
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        //INSTANT C# NOTE: Converted event handler wireups:
        Load += Page_Load;
    }

    protected void rbl_range_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbl_range.SelectedIndex == 3)
        {
            tb_enddate.Enabled = true;
            tb_startdate.Enabled = true;
        }
        else
        {
            tb_enddate.Enabled = false;
            tb_startdate.Enabled = false;
        }
    }

    protected void rbl_bars_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
}