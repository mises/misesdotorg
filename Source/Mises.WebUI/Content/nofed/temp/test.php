<?php
  function do_post_request($url, $data, $optional_headers = null)
  {
     $params = array('http' => array(
                  'method' => 'POST',
                  'content' => $data
               ));
     if ($optional_headers !== null) {
        $params['http']['header'] = $optional_headers;
     }
     $ctx = stream_context_create($params);
     $fp = @fopen($url, 'rb', false, $ctx);
     if (!$fp) {
        throw new Exception("Problem with $url, $php_errormsg");
     }
     $response = @stream_get_contents($fp);
     if ($response === false) {
        throw new Exception("Problem reading data from $url, $php_errormsg");
     }
     return $response;
  }

set_time_limit(295);

//Connect to DB
$db = mysql_connect("localhost", "graphs","lvmigraphman") or die(mysql_error()); 
mysql_select_db("graphs",$db) or die(mysql_error());

//Array of units
$units  = array(
	"lin",
	"chg",
	"ch1",
	"pch",
	"pc1",
	"pca",
	"cch",
	"cca",
	"log"
);

//Array of series to update
$series = array(
	"MZMNS",
	"M1NS",
	"M2NS",
	//"M3NS"
	"CURRNS",
	"DDDFCBNS",
	"DDDFOINS",
	"SAVINGNS",
	"TCDNS",
	"USGVDDNS"
);

//Cycle through each series
foreach ($series as $thisseries) {
	//Build URL
	$url = "http://research.stlouisfed.org/fred2/series/$thisseries/downloaddata";
	$thisseries = strtolower($thisseries);	

	foreach ($units as $thisunit) {
		//Build query
		$data = array('_qf__mainform'	=>'',
			      'units'		=>$thisunit,
			      'obs_start_date'	=>'2008-03-01',
			      'obs_end_date'	=>'2008-04-01',
			      'file_format'	=>'csv',
			      'download_data'	=>'Download Data');

		//Download latest data

echo do_post_request($url, http_build_query($data));
echo "<br>\n";

/*		$feddata = explode("\n",do_post_request($url, http_build_query($data)));
//		print_r ($feddata);
	
		//Parse data
		foreach ($feddata as $value) {
			$thisline=explode(",",$value);
			if ($thisline[0]=="DATE"||empty($thisline[0])||$thisline[1]==".") continue;
			$thisline[0]=trim($thisline[0]);
			$thisline[1]=trim($thisline[1]);
			if ($thisline[1]==".") continue;
			//Update DB
			if ($thisunit=="lin") {
				echo "INSERT INTO series_$thisseries (datemonth, lin) VALUES ('$thisline[0]', '$thisline[1]')<br>\n";
				$result = mysql_query("INSERT INTO series_$thisseries (datemonth, lin) VALUES ('$thisline[0]', '$thisline[1]')");
				if (!$result) {
				    echo('Invalid query: ' . mysql_error() . '<br>');
				}
			} else {
				echo "UPDATE series_$thisseries SET $thisunit = '$thisline[1]' WHERE datemonth LIKE '$thisline[0]'<br>\n";
				$result = mysql_query("UPDATE series_$thisseries SET $thisunit = '$thisline[1]' WHERE datemonth LIKE '$thisline[0]'");
				if (!$result) {
				    echo('Invalid query: ' . mysql_error() . '<br>');
				}
			}
		}*/
	}
}

?>
