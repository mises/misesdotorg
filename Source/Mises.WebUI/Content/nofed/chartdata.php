<?php

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Transfer-Encoding: binary");
    header("Content-Type: " . "application/csv");
//    header("Content-Length: " . $filesize);
    header("Content-Disposition: attachment; filename=\"misesdata.csv\";" ); 

// Grab data from DB
$db = mysql_connect("data.mises.org", "graphs","lvmigraphman") or die(mysql_error());
 
mysql_select_db("graphs",$db) or die(mysql_error());

// Pick unit, use case instead of passing potentially dangerous variables
switch ($_REQUEST["unit"]) {
case "lin":
	$thisunit = "lin";
	$unit="Billions of Dollars";
	break;
case "chg":
	$thisunit = "chg";
	$unit="Change, Billions of Dollars";
	break;
case "ch1":
	$thisunit = "ch1";
	$unit="Change from Year Ago, Billions of Dollars";
	break;
case "pch":
	$thisunit = "pch";
	$unit="Percent Change";
	break;
case "pc1":
	$thisunit = "pc1";
	$unit="Percent Change from Year Ago";
	break;
case "pca":
	$thisunit = "pca";
	$unit="Compounded Annual Rate of Change";
	break;
case "cch":
	$thisunit = "cch";
	$unit="Continuously Compounded Rate of Change";
	break;
case "cca":
	$thisunit = "cca";
	$unit="Continuously Compounded Annual Rate of Change";
	break;
case "log":
	$thisunit = "log";
	$unit="Natural Log of Billions of Dollars";
	break;
default:
	die("Unit not understood.");
}

$SQLstm = "SELECT * FROM cat_agg_". $thisunit . "_view";
// Select proper range
switch ($_REQUEST["range"]) {
case "max": //don't need to add anything but a valid choice
	break;
case 10:
	$SQLstm .= "WHERE datemonth>=NOW() - INTERVAL 10 YEAR";
	break;
case 5:
	$SQLstm .= "WHERE datemonth>=NOW() - INTERVAL 5 YEAR";
	break;
case "cust":
	  //validate the start/end dates
  if (!isset($_REQUEST['start']) || !isset($_REQUEST['end']) )
  {
    die ("custom range reqires start and end dates set");
  }

  list($m, $d, $y) = explode("/", $_REQUEST['start']);
  if ( $m<1 || 12<$m || $d<1 || 31<$d || $y < 1900 )
  {
    die("illegal date: $m-$d-$y");
  }
  $startD = "$y-$m-$d";
  $startEpoch = mktime(0,0,0,$m,$d,$y);
  list($m, $d, $y) = explode("/", $_REQUEST['end']);
  if ( $m<1 || 12<$m || $d<1 || 31<$d || $y < 1900 )
  {
    die("illegal date: $m-$d-$y");
  }
  $endD = "$y-$m-$d";
  $endEpoch = mktime(0,0,0,$m,$d,$y);
  if ($startEpoch > $endEpoch)
  {
    die("Custom range end must be AFTER begin");
  }
  $SQLstm .= " WHERE '$startD'<datemonth AND datemonth<'$endD' ";
  break;
default:
	die("Error: Range not understood");
}
$SQLstm .= " order by datemonth asc";
$query = mysql_query($SQLstm) or die (mysql_error());

$firstline="DATE";
$plotcount=0;
// Create the linear plots
if (isset($_REQUEST["tms"])&&$_REQUEST["tms"]=="true") {
	$firstline.=",TMS";
	if ($plotcount==0) {
		$graphtitle="True Money Supply (TMS)";
	} else {
		$graphtitle.=" vs. True Money Supply (TMS)";
	}
	$plotcount++;
}
if (isset($_REQUEST["mzm"])&&$_REQUEST["mzm"]=="true") {
	$firstline.=",MZM";
	if ($plotcount==0) {
		$graphtitle="MZM Money Stock";
	} else {
		$graphtitle.=" vs. MZM Money Stock";
	}
	$plotcount++;
}
if (isset($_REQUEST["m1"])&&$_REQUEST["m1"]=="true") {
	$firstline.=",M1";
	if ($plotcount==0) {
		$graphtitle="M1 Money Stock";
	} else {
		$graphtitle.=" vs. M1 Money Stock";
	}
	$plotcount++;
}
if (isset($_REQUEST["m2"])&&$_REQUEST["m2"]=="true") {
	$firstline.=",M2";
	if ($plotcount==0) {
		$graphtitle="M2 Money Stock";
	} else {
		$graphtitle.=" vs. M2 Money Stock";
	}
	$plotcount++;
}
if (isset($_REQUEST["m3"])&&$_REQUEST["m3"]=="true") {
	$firstline.=",M3";
	if ($plotcount==0) {
		$graphtitle="M3 Money Stock";
	} else {
		$graphtitle.=" vs. M3 Money Stock";
	}
	$plotcount++;
}
	$firstline.="\n";

echo "Title:,$graphtitle\n";
echo "Source:,Ludwig von Mises Institute\n";
echo "Seasonal Adjustment:,Not Seasonally Adjusted\n";
echo "Frequency,Monthly\n";
echo "Units:,$unit\n";
echo "\n";
echo "\n";
echo $firstline;

while($row = mysql_fetch_array($query)) {
	  				echo "$row[0]";
	//  $data_currns[] 	= $row[1];
	//  $data_dddfcbns[] 	= $row[2];
	//  $data_dddfoins[] 	= $row[3];
	//  $data_savingns[] 	= $row[4];
	//  $data_tcdns[] 	= $row[5];
	//  $data_usgvddns[] 	= $row[6];
	if (isset($_REQUEST["m1"]))  	echo ",$row[7]";
	if (isset($_REQUEST["m2"]))  	echo ",$row[8]";
	if (isset($_REQUEST["m3"]))  	echo ",$row[9]";
	if (isset($_REQUEST["mzm"]))  	echo ",$row[10]";
	if (isset($_REQUEST["tms"]))  	echo ",$row[11]";
	echo "\n";
}



?>
