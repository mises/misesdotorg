<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.Master" AutoEventWireup="false"
    Title="Mises.org: Economic Data" Inherits="Chart" CodeBehind="Chart.aspx.cs" %>
<%@ Response.Redirect("http://mises.freecapitalists.org/", true); %>
<% Response.Redirect("http://mises.freecapitalists.org/", true); %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" cellpadding="1" cellspacing="1" class="MainTable">
        <tbody>
            <tr>
                <td>
                    <br />
                    <br />
                    <div>
                        <asp:Label runat="server" ID="lbl_heading" CssClass="CommonTextBig" />
                        <br />
                        <br />
                        <p>
                            <asp:Label runat="server" ID="lbl_desc" CssClass="CommonTextBig" />
                        </p>
                        <asp:Label runat="server" ID="lbl_error" CssClass="error" Text="Please select a series." />
                        <br />
                        <asp:Image runat="server" ID="img_chart" Style="z-index: 2;" />
                        <div id="toolbar" class="ui-corner-all">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Aggregates:
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="cb_tms" Text="TMS" Checked="true" />
                                            <asp:CheckBox runat="server" ID="cb_mzm" Text="MZM" />
                                            <asp:CheckBox runat="server" ID="cb_m1" Text="M1" />
                                            <asp:CheckBox runat="server" ID="cb_m2" Text="M2" />
                                            <asp:CheckBox runat="server" ID="cb_m3" Text="M3" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Unit:
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddl_units">
                                                <asp:ListItem Value="lin" Text="Billions of Dollars" Selected="true" />
                                                <asp:ListItem Value="chg" Text="Change, Billions of Dollars" />
                                                <asp:ListItem Value="ch1" Text="Change from Year Ago, Billions of Dollars" />
                                                <%--
									<asp:ListItem Value="pch" Text="Percent Change" visible=false />
									<asp:ListItem Value="pc1" Text="Percent Change from Year Ago" visible=false />
									<asp:ListItem Value="pca" Text="Compounded Annual Rate of Change" visible=false />
									<asp:ListItem Value="cch" Text="Continuously Compounded Rate of Change" visible=false />
									<asp:ListItem Value="cca" Text="Continuously Compounded Annual Rate of Change" visible=false />
									<asp:ListItem Value="log" Text="Natural Log of Billions of Dollars" visible=false />
                                                --%><asp:ListItem Value="pch">Percent Change</asp:ListItem>
                                                <asp:ListItem Value="pc1">Percent Change from Year Ago</asp:ListItem>
                                                <asp:ListItem Value="pca">Compunded Annual Rate of Change</asp:ListItem>
                                                <asp:ListItem Value="cch">Continuously Compunded Rate of Change</asp:ListItem>
                                                <asp:ListItem Value="cca">Continuously Compounded Annual Rate of Change</asp:ListItem>
                                                <asp:ListItem Value="log">Natural Log of Billions of Dollars</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 40px">
                                            Range:
                                        </td>
                                        <td style="height: 40px">
                                            <asp:RadioButtonList runat="server" ID="rbl_range" RepeatDirection="horizontal" AutoPostBack="True"
                                                Height="27px" OnSelectedIndexChanged="rbl_range_SelectedIndexChanged">
                                                <asp:ListItem Value="5" Text="5 Years" />
                                                <asp:ListItem Value="10" Text="10 Years" />
                                                <asp:ListItem Value="max" Text="Max" Selected="true" />
                                                <asp:ListItem Value="cust">Custom</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 16px">
                                            Custom Dates:
                                        </td>
                                        <td style="height: 16px">
                                            Start:
                                            <asp:TextBox ID="tb_startdate" runat="server" Enabled="False" Width="95px">MM/DD/YYYY</asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End:
                                            <asp:TextBox ID="tb_enddate" runat="server" Enabled="False" Width="95px">MM/DD/YYYY</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Recession Bars:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList runat="server" ID="rbl_bars" RepeatDirection="horizontal" OnSelectedIndexChanged="rbl_bars_SelectedIndexChanged">
                                                <asp:ListItem Value="true" Text="On" Selected="true" />
                                                <asp:ListItem Value="false" Text="Off" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Size:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList runat="server" ID="rbl_size" RepeatDirection="horizontal">
                                                <asp:ListItem Value="med" Text="Medium" Selected="true" />
                                                <asp:ListItem Value="large" Text="Large" />
                                                <asp:ListItem Value="xlarge" Text="X-Large" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <asp:Button runat="server" ID="btn_make" CssClass="ui-state-default ui-corner-all ui-button"
                                Text="Make Graph" />
                            <asp:Button runat="server" ID="btn_download" CssClass="ui-state-default ui-corner-all ui-button"
                                Text="Download Data" Enabled="True" />
                        </div>
                    </div>
            </tr>
        </tbody>
    </table>
</asp:Content>
