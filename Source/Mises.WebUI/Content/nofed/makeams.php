<?php
include ("jpgraph-3.0.2/src/jpgraph.php");
include ("jpgraph-3.0.2/src/jpgraph_line.php");

// Grab data from DB
$db = mysql_connect("data.mises.org", "graphs","lvmigraphman") or die(mysql_error());
 
mysql_select_db("graphs",$db) or die(mysql_error());
$query = mysql_query("SELECT LEFT(datamonth,4), ams2 FROM graphdata ORDER BY datamonth") or die(mysql_error());
 
while($row = mysql_fetch_array($query)) {
  $ydata[] = $row[1];
  $leg[] = $row[0];
}

// Create the graph. These two calls are always required
$graph = new Graph(630,378,"auto");	
$graph->SetScale("textlin");

// Appearence stuff
$graph->img->SetMargin(70, 30, 50, 50);
$graph->SetMarginColor('lightblue'); 
$graph->title->Set ("Austrian Money Stock (AMS)");
$graph->subtitle->Set ("Source: Ludwig von Mises Institute");
//$graph->xaxis-> title->Set("Shaded areas indicate US recessions as determined by the NBER." );
$graph->xaxis->SetTitle("Shaded areas indicate US recessions as determined by the NBER.",'middle'); 
$graph->xaxis->SetTitlemargin(15);
$graph->yaxis-> title->Set("(Billions of Dollars)" );
$graph->yaxis->SetTitlemargin(50);
$graph->yaxis->SetLabelFormatCallback("number_format");

// Years to show on x-axis
//$xyears = array(1950, 1960, 1970, 1980, 1990, 2000, 2010);
//$graph->xaxis-> SetTickLabels($xyears);
$graph->xaxis->SetTextTickInterval(60); 
$graph->xaxis->SetTickLabels($leg);
//$graph->xaxis->SetTextLabelInterval(120); 

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot->SetColor("blue");

// Add the plot to the graph
$graph->Add($lineplot);

// Display the graph
$graph->Stroke();
?>
