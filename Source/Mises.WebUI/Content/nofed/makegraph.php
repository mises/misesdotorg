<?php

include ("jpgraph-3.0.2/src/jpgraph.php");
include ("jpgraph-3.0.2/src/jpgraph_line.php");
include ("jpgraph-3.0.2/src/jpgraph_bar.php");
include ("jpgraph-3.0.2/src/jpgraph_log.php");

function kill_nulls(&$item1)
{
	if (is_null($item1)) $item1="x";
}

function justyear(&$thisdate)
{
	$thisdate=substr($thisdate,0,4);
}

function is_recession($testdate)
{
	$thisdate=str_replace("-","",$testdate);

	if (($thisdate>=18570601)&&($thisdate<=18581201)) {
		return TRUE;
	} elseif (($thisdate>=18601001)&&($thisdate<=18610601)) {
		return TRUE;
	} elseif (($thisdate>=18650401)&&($thisdate<=18671201)) {
		return TRUE;
	} elseif (($thisdate>=18690601)&&($thisdate<=18701201)) {
		return TRUE;
	} elseif (($thisdate>=18731001)&&($thisdate<=18790301)) {
		return TRUE;
	} elseif (($thisdate>=18820301)&&($thisdate<=18850501)) {
		return TRUE;
	} elseif (($thisdate>=18870301)&&($thisdate<=18880401)) {
		return TRUE;
	} elseif (($thisdate>=18900701)&&($thisdate<=18910501)) {
		return TRUE;
	} elseif (($thisdate>=18930101)&&($thisdate<=18940601)) {
		return TRUE;
	} elseif (($thisdate>=18951201)&&($thisdate<=18970601)) {
		return TRUE;
	} elseif (($thisdate>=18990601)&&($thisdate<=19001201)) {
		return TRUE;
	} elseif (($thisdate>=19020901)&&($thisdate<=19040801)) {
		return TRUE;
	} elseif (($thisdate>=19070501)&&($thisdate<=19080601)) {
		return TRUE;
	} elseif (($thisdate>=19100101)&&($thisdate<=19120101)) {
		return TRUE;
	} elseif (($thisdate>=19130101)&&($thisdate<=19141201)) {
		return TRUE;
	} elseif (($thisdate>=19180801)&&($thisdate<=19190301)) {
		return TRUE;
	} elseif (($thisdate>=19200101)&&($thisdate<=19210701)) {
		return TRUE;
	} elseif (($thisdate>=19230501)&&($thisdate<=19240701)) {
		return TRUE;
	} elseif (($thisdate>=19261001)&&($thisdate<=19271101)) {
		return TRUE;
	} elseif (($thisdate>=19290801)&&($thisdate<=19330301)) {
		return TRUE;
	} elseif (($thisdate>=19370501)&&($thisdate<=19380601)) {
		return TRUE;
	} elseif (($thisdate>=19450201)&&($thisdate<=19451001)) {
		return TRUE;
	} elseif (($thisdate>=19481101)&&($thisdate<=19491001)) {
		return TRUE;
	} elseif (($thisdate>=19530701)&&($thisdate<=19540501)) {
		return TRUE;
	} elseif (($thisdate>=19570801)&&($thisdate<=19580401)) {
		return TRUE;
	} elseif (($thisdate>=19600401)&&($thisdate<=19610201)) {
		return TRUE;
	} elseif (($thisdate>=19691201)&&($thisdate<=19701101)) {
		return TRUE;
	} elseif (($thisdate>=19731101)&&($thisdate<=19750301)) {
		return TRUE;
	} elseif (($thisdate>=19800101)&&($thisdate<=19800701)) {
		return TRUE;
	} elseif (($thisdate>=19810701)&&($thisdate<=19821101)) {
		return TRUE;
	} elseif (($thisdate>=19900701)&&($thisdate<=19910301)) {
		return TRUE;
	} elseif (($thisdate>=20010301)&&($thisdate<=20011101)) {
		return TRUE;
	} elseif (($thisdate>=20071201) ) {
		return TRUE;
	} else {
		return FALSE;
	}
}
	




//Define colors
$colors = array(
	0 => "blue",
	1 => "red",
	2 => "green",
	3 => "purple",
	4 => "brown"
);

// Grab data from DB
$db = mysql_connect("data.mises.org", "graphs","lvmigraphman") or die(mysql_error());
 
mysql_select_db("graphs",$db) or die(mysql_error());

// Pick unit, use case instead of passing potentially dangerous variables
switch ($_REQUEST["unit"]) {
case "lin":
	$thisunit = "lin";
	$ytitle="(Billions of Dollars)";
	break;
case "chg":
	$thisunit = "chg";
	$ytitle="(Change, Billions of Dollars)";
	break;
case "ch1":
	$thisunit = "ch1";
	$ytitle="(Change from Year Ago, Billions of Dollars)";
	break;
case "pch":
	$thisunit = "pch";
	$ytitle="(Percent Change)";
	break;
case "pc1":
	$thisunit = "pc1";
	$ytitle="(Percent Change from Year Ago)";
	break;
case "pca":
	$thisunit = "pca";
	$ytitle="(Compounded Annual Rate of Change)";
	break;
case "cch":
	$thisunit = "cch";
	$ytitle="(Continuously Compounded Rate of Change)";
	break;
case "cca":
	$thisunit = "cca";
	$ytitle="(Continuously Compounded Annual Rate of Change)";
	break;
case "log":
	$thisunit = "log";
	$ytitle="(Natural Log of Billions of Dollars)";
	break;
default:
	die("Unit not understood.");
}

// Select proper range
$SQLstm = "SELECT * FROM cat_agg_"."$thisunit"."_view";

switch ($_REQUEST["range"]) {
case "max":
	$tickinterval=5;
	break;
case 10:
	$SQLstm .= " WHERE datemonth>=NOW() - INTERVAL 10 YEAR";
	$tickinterval=1;
	break;
case 5:
	$SQLstm .= " WHERE datemonth>=NOW() - INTERVAL 5 YEAR";
	$tickinterval=1;
	break;
case "cust":
	//validate the start/end dates
	if (!isset($_REQUEST['start']) || !isset($_REQUEST['end']) )
	{
		die ("custom range reqires start and end dates set");
	}

	list($m, $d, $y) = explode("/", $_REQUEST['start']);
	if ( $m<1 || 12<$m || $d<1 || 31<$d || $y < 1900 )
	{
		die("illegal date: $m-$d-$y");
	} 
	$startD = "$y-$m-$d";
	$startEpoch = mktime(0,0,0,$m,$d,$y);
	list($m, $d, $y) = explode("/", $_REQUEST['end']);
  if ( $m<1 || 12<$m || $d<1 || 31<$d || $y < 1900 )
  {
    die("illegal date: $m-$d-$y");
  }
	$endD = "$y-$m-$d";
	$endEpoch = mktime(0,0,0,$m,$d,$y);
	if ($startEpoch > $endEpoch)
	{
		die("Custom range end must be AFTER begin");
	}
	$SQLstm .= " WHERE '$startD'<datemonth AND datemonth<'$endD' ";
	$tickinterval=1;
	break;
default:
	die("Error: Range not understood");
}

$SQLstm .= " order by datemonth asc";
$query = mysql_query($SQLstm) or die (mysql_error());
 
$maxval=0;
while($row = mysql_fetch_array($query)) {
	  				$leg[]	 		= $row[0]; //substr($row[0],0,4)
	//  $data_currns[] 	= $row[1];
	//  $data_dddfcbns[] 	= $row[2];
	//  $data_dddfoins[] 	= $row[3];
	//  $data_savingns[] 	= $row[4];
	//  $data_tcdns[] 	= $row[5];
	//  $data_usgvddns[] 	= $row[6];
	if (isset($_REQUEST["m1"]))  	$data_m1ns[] 		= $row[7];
	if (isset($_REQUEST["m2"]))  	$data_m2ns[] 		= $row[8];
	if (isset($_REQUEST["m3"]))  	$data_m3ns[]	 	= $row[9];
	if (isset($_REQUEST["mzm"]))  	$data_mzmns[]		= $row[10];
	if (isset($_REQUEST["tms"]))  $data_asms[] 		= $row[11];
}

//Check max val, populate $data_recession

/*
if (!isset($data_m1ns)) 	$data_m1ns[] = 0;
if (!isset($data_m2ns)) 	$data_m2ns[] = 0;
if (!isset($data_m3ns)) 	$data_m3ns[] = 0;
if (!isset($data_mzmns)) 	$data_mzmns[] = 0;
if (!isset($data_asms)) 	$data_asms[] = 0;
*/

//$maxval=max(array(max($data_m1ns),max($data_m2ns),max($data_m3ns),max($data_mzmns),max($data_asms)));

if (isset($data_m1ns)) 	$maximums[] = max($data_m1ns);
if (isset($data_m2ns)) 	$maximums[] = max($data_m2ns);
if (isset($data_m3ns)) 	$maximums[] = max($data_m3ns);
if (isset($data_mzmns))	$maximums[] = max($data_mzmns);
if (isset($data_asms)) 	$maximums[] = max($data_asms);
$maxval = max($maximums);

if (isset($data_m1ns)) {
	array_walk($data_m1ns, 'kill_nulls');
} 
if (isset($data_m2ns)) {
	array_walk($data_m2ns, 'kill_nulls');
} 
if (isset($data_m3ns)) {
	array_walk($data_m3ns, 'kill_nulls');
} 
if (isset($data_mzmns)) {
	array_walk($data_mzmns, 'kill_nulls');
} 
if (isset($data_asms)) {
	array_walk($data_asms, 'kill_nulls');
} 

//$minval=min(array(min($data_m1ns),min($data_m2ns),min($data_m3ns),min($data_mzmns),min($data_asms)));

if (isset($data_m1ns)) 	$minimums[] = min($data_m1ns);
if (isset($data_m2ns)) 	$minimums[] = min($data_m2ns);
if (isset($data_m3ns)) 	$minimums[] = min($data_m3ns);
if (isset($data_mzmns))	$minimums[] = min($data_mzmns);
if (isset($data_asms)) 	$minimums[] = min($data_asms);
$minval = min($minimums);

if ($maxval>0) {
	foreach ($leg as $tempvalue) {
		//echo "$leg  $tempvalue  ".is_recession($tempvalue)."<br>";
		if (is_recession($tempvalue)) {
			$data_recession[] = $maxval;
		} else {
			$data_recession[] = "";
		}
	}
}
if ($minval<0) {
	foreach ($leg as $tempvalue) {
		//echo "$leg  $tempvalue  ".is_recession($tempvalue)."<br>";
		if (is_recession($tempvalue)) {
			$data_recession2[] = $minval;
		} else {
			$data_recession2[] = "";
		}
	}
}

// Create the graph. These two calls are always required
switch ($_REQUEST["size"]) {
case "med":
	$graph = new Graph(630,378,"auto");
	break;
case "large":
	$graph = new Graph(800,480,"auto");
	break;
case "xlarge":
	$graph = new Graph(1000,600,"auto");
	break;
default:
	die ("Size not understood.");
}

$scale = 'textlin';

if (isset($_REQUEST["scale"]))
{
	$scale = $_REQUEST["scale"];
}
	
$graph->SetScale($scale,$minval,$maxval);

// Appearence stuff
$graph->SetMarginColor('lightblue'); 
$graph->subtitle->Set ("Source: Ludwig von Mises Institute");
$graph->xaxis->SetTitlemargin(25);
$graph->yaxis-> title->Set($ytitle);
$graph->yaxis->SetTitlemargin(50);
$graph->yaxis->SetLabelFormatCallback("number_format");
$graph->xaxis->SetPos("min");

// Recession bars
if ($_REQUEST["bars"]=="true") {
	if ($maxval>0) {
		$bplot = new BarPlot($data_recession);
		$bplot->SetFillColor('gray'); 
		$bplot->SetColor('gray'); 
		$bplot->SetWidth(1);
		$graph->Add($bplot); 
	}
	if ($minval<0) {
		$bplot2 = new BarPlot($data_recession2);
		$bplot2->SetFillColor('gray');
		$bplot2->SetColor('gray'); 
		$bplot2->SetWidth(1);
		$graph->Add($bplot2);
	}
	$graph->xaxis->SetTitle("Shaded areas indicate US recessions as determined by the NBER.",'middle');
}

// Show only years
array_walk($leg, 'justyear');


// Years to show on x-axis
$graph->xaxis->SetLabelAngle(90);
$graph->xaxis->SetTextTickInterval(12); //show a tick each year
$graph->xaxis->SetTextLabelInterval($tickinterval); //label less than a year?
$graph->xaxis->SetTickLabels($leg);
//$graph->xaxis->SetLabelFormatCallback("justyear");


$plotcount=0;
// Create the linear plots
if (isset($_REQUEST["tms"])&&$_REQUEST["tms"]=="true") {
	if ($plotcount==0) {
		$graphtitle="True Money Supply (TMS)";
	} else {
		$graphtitle.="\nvs. True Money Supply (TMS)";
	}
	$lineasms=new LinePlot($data_asms);
	$lineasms->SetColor($colors[$plotcount]);
	$lineasms->SetLegend("TMS");
	$graph->Add($lineasms);
	$plotcount++;
}
if (isset($_REQUEST["mzm"])&&$_REQUEST["mzm"]=="true") {
	if ($plotcount==0) {
		$graphtitle="MZM Money Stock";
	} else {
		$graphtitle.="\nvs. MZM Money Stock";
	}
	$linemzmns=new LinePlot($data_mzmns);
	$linemzmns->SetColor($colors[$plotcount]);
	$linemzmns->SetLegend("MZM");
	$graph->Add($linemzmns);
	$plotcount++;
}
if (isset($_REQUEST["m1"])&&$_REQUEST["m1"]=="true") {
	if ($plotcount==0) {
		$graphtitle="M1 Money Stock";
	} else {
		$graphtitle.="\nvs. M1 Money Stock";
	}
	$linem1ns=new LinePlot($data_m1ns);
	$linem1ns->SetColor($colors[$plotcount]);
	$linem1ns->SetLegend("M1");
	$graph->Add($linem1ns);
	$plotcount++;
}
if (isset($_REQUEST["m2"])&&$_REQUEST["m2"]=="true") {
	if ($plotcount==0) {
		$graphtitle="M2 Money Stock";
	} else {
		$graphtitle.="\nvs. M2 Money Stock";
	}
	$linem2ns=new LinePlot($data_m2ns);
	$linem2ns->SetColor($colors[$plotcount]);
	$linem2ns->SetLegend("M2");
	$graph->Add($linem2ns);
	$plotcount++;
}
if (isset($_REQUEST["m3"])&&$_REQUEST["m3"]=="true") {
	if ($plotcount==0) {
		$graphtitle="M3 Money Stock";
	} else {
		$graphtitle.="\nvs. M3 Money Stock";
	}
	$linem3ns=new LinePlot($data_m3ns);
	$linem3ns->SetColor($colors[$plotcount]);
	$linem3ns->SetLegend("M3");
	$graph->Add($linem3ns);
	$plotcount++;
}

//Set title
$graph->title->Set ($graphtitle);

//Set margins
$graph->legend->Pos(0.01,0.5,"right","middle"); 

$graph->img->SetMargin(70,75, 50, 75);

// Display the graph
$graph->Stroke();
//phpinfo();


?>
