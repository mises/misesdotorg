﻿#region

using System;
using System.Web.UI;
using Mises.Domain.Utility;

#endregion

/// <summary>
///   Summary description for Error
/// </summary>
partial class Error : Page
{
    private void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                return;
            }

            Response.StatusCode = 500;
            Response.Status = "500 Internal Server Error";

            Exception objErr = Server.GetLastError();

            var err = "\n" + "Error in: " + Server.HtmlEncode(Request.Url.PathAndQuery.PadRight(150).Substring(0, 150));

            if (objErr != null)
            {
                err += objErr.Message + Environment.NewLine;

                Response.StatusDescription = objErr.Message;

                if (objErr.InnerException != null)
                {
                    err += objErr.InnerException.Message + Environment.NewLine;
                }
            }

            Page.Title = err;
            txtDescription.Text = err;
            lblResult.Text = err;

        }
        catch (Exception ex)
        {
            lblResult.Text = ex.ToString();
        }
    }

    //Page_Load


    protected void Send_Click(object sender, EventArgs e)
    {
        if (txtSpamValidation.Text != "Mises")
        {
            lblResult.Text = "Enter Mises in the textbox";
            return;
        }

        string message = "Error at " + Request.QueryString["aspxerrorpath"] + "\n" + txtDescription.Text +
                         Environment.NewLine + txtEmail.Text;
        EmailHelper.SendMail("errors@freecapitalists.org", "heroic@gmail.com",
                             "Mises.org Error " + Request.QueryString["aspxerrorpath"], message);
        lblFeedback.Visible = false;
        lblResult.Text = "E-mail has been sent. Thank you.";
    }

    //Send_Click
}