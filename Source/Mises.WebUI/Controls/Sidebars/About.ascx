<%@ Control Language="C#" AutoEventWireup="True" Inherits="Controls_Sidebars_About" Codebehind="About.ascx.cs" %>
<%@ Register Src="~/Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>
<uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="AboutSidebar" />
<div class="box greateconomists">
    <h2>
        Great Austrian Economists</h2>
    <div class="cont">
        <ul class="economists">
            <%--<li><a href="#">Benjamin Anderson <span>(1886 - 1949)</span></a></li>--%>
            <asp:Repeater ID="dlBiographies" runat="server" 
                          DataSourceID="sqlDocumentsGetBiographies">
                <ItemTemplate>
                    <li>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%#Eval("DocumentId", "/about/{0}")%>'
                                       Text='<%#Eval("Title").ToString().Replace("Biography of ", "")%>'></asp:HyperLink></li>
                </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource ID="sqlDocumentsGetBiographies" runat="server" 
                               ConnectionString="<%$ ConnectionStrings:Public %>" 
                               SelectCommand="DocumentsGetBiographies" 
                               SelectCommandType="StoredProcedure" EnableCaching="True"></asp:SqlDataSource>
        </ul>
        <!-- / .cont -->
    </div>
    <!-- / .box -->
</div>