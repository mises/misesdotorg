<%@ Control Language="C#" AutoEventWireup="false" Inherits="Controls_Sidebars_Media" Codebehind="Media.ascx.cs" %>
<%@ Register Src="../../Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>

<%@ OutputCache Duration="180" VaryByParam="*" Shared="false" %>
<%@ Import Namespace="Mises.Data" %>

<script type="text/javascript">
    //<![CDATA[
    function submitenter(myfield, e) {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;
        else return true;

        if (keycode == 13) {
            return Search();
        } else
            return true;
    }

    function Search() {
        var text = $('#searchText').val();
        var site = $('#txtSearchSite').val();
        window.location = "http://search.mises.org/search?q=" + text + "&site=" + site;
        return false;
    }
    //]]>
</script>

<style type="text/css">
    table.gsc-search-box td.gsc-input
    {
        padding-right: 2px;
    }

    table.gsc-search-box td
    {
        vertical-align: middle;
    }

    td.gsc-search-button
    {
        width: 1%;
    }

    input.gsc-input
    {
        border: 1px solid #BCCDF0;
        padding-left: 2px;
        width: 99%;
    }

    input.gsc-search-button
    {
        margin-left: 2px;
    }
</style>

<p><uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="MediaSidebar" /></p>

<div class="box activitywrapper">
    <h2>
        Media Search</h2>

    <script type="text/javascript">
        /* <![CDATA[ */

        function search_media_but() {
            window.location = "/media/search/1?q=" + encodeURIComponent(escape(document.getElementById('search_media_text').value));
            return false;
        }
        /* ]]> */
    </script>

    <fieldset id="MediaSearch">
        <input type="text" class="text" name="search_media_text" id="search_media_text" value="" />

        <input type="image" class="buttonimg" name="search_media_but" id="search_media_but"
               alt="Search Site" src="//mises.freecapitalists.org/images/Theme/images/but-search.jpg" onclick=" return search_media_but(); " />
    </fieldset>
</div>

<br />
<div class="outerbox">
    <div class="box popular-content">
        <h2>
            Popular Content</h2>
        <div class="cont">
            <ul class="popular-content">
                <asp:Repeater runat="server" ID="Repeater3" DataSourceID="SqlDataSource3">
                    <ItemTemplate>
                        <li>
                            <a title="<%#Eval("Count")%> tags" href="<%#String.Format("/media/{0}/{1}?d=true", Eval("DocumentId"),
                                           DataFormat.GenerateSlug(Eval("Title")))%>"><%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%></a>
                            <em> by <a href="<%#String.Format("/media/authors/{0}/{1}", Eval("Author1"),
                                           DataFormat.GenerateSlug(Eval("AuthorName")))%>"><%#Eval("AuthorName")%></a> | <span class="time"><%#Eval("CreateDate")%></span></em>
                        </li>
                    </ItemTemplate>
                    <SeparatorTemplate>
                    </SeparatorTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" CacheDuration="180" ConnectionString="<%$ ConnectionStrings:Public %>"
                                   SelectCommand="MediaGetTopTagged" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </ul>
            <%--TODO: <a class="more" href="#">more?/a>--%>
            <!-- / .cont -->
        </div>
        <!-- / .box -->
    </div>
</div>

<div class="outerbox">
    <div class="box activitywrapper">
        <h2>
            Authors</h2>
        <div class="cont">
            <ul class="authors">
                <asp:Repeater runat="server" ID="Repeater1" DataSourceID="SqlDataSource1">
                    <ItemTemplate>
                        <li>
                            <a href="<%#String.Format("/media/authors/{0}/{1}", Eval("AuthorId"),
                                                  DataFormat.GenerateSlug(Eval("AuthorLast")))%>"><%#Eval("AuthorLast")%></a>
                        </li>
                    </ItemTemplate>
                    <SeparatorTemplate>
                    </SeparatorTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" CacheDuration="180" ConnectionString="<%$ ConnectionStrings:Public %>"
                                   SelectCommand="MediaTopAuthors" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                <li class="last"><a style="color: #d80e0e;" href="/literature/authors">All Authors</a>
                </li>
            </ul>
            <!-- / .cont -->
        </div>
        <!-- / .box -->
    </div>
</div>
<div class="outerbox">
    <div class="box activitywrapper">
        <h2>
            Topics</h2>
        <div class="cont">
            <ul class="topics">
                <li>
                <asp:Repeater runat="server" ID="Repeater2" DataSourceID="SqlDataSource2">
                    <ItemTemplate>
                        <li>
                            <a href="<%#String.Format("/media/subjects/{0}/{1}", Eval("SubjectId"),
                                                  DataFormat.GenerateSlug(Eval("Subject")))%>"><%#Eval("Subject")%></a>
                        </li>
                    </ItemTemplate>
                    <SeparatorTemplate>
                    </SeparatorTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" CacheDuration="180" ConnectionString="<%$ ConnectionStrings:Public %>"
                                   SelectCommand="MediaTopFullSubjects" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                <li class="last"><a style="color: #d80e0e;" href="/media.aspx?action=subject">more</a>
                </li>
            </ul>
            <!-- / .cont -->
        </div>
        <!-- / .box -->
    </div>
</div>
<%--<div class="outerbox">
    <div class="box activitywrapper">
        <h2>
            New Media</h2>
        <div class="cont">
            <ul class="members">
                <asp:ObjectDataSource runat="server" SelectMethod="GetNewMedia" TypeName="Mises.Domain.Media.MediaCatalog"
                    ID="objNewMedia" EnableCaching="True" CacheDuration="360"></asp:ObjectDataSource>
                <asp:Repeater ID="rptNewMedia" runat="server" DataSourceID="objNewMedia">
                    <ItemTemplate>
                        <li><a class="image" href="<%#Eval("URL")%>">
                            <img src="/media/poster/<%#Eval("DocumentId")%>" width="40"
                                alt="" style="display: <%#Eval("Display")%>" /></a>
                            <div class="memberdetails">
                                <h4>
                                    <a href='/media/<%#DataBinder.Eval(Container, "DataItem.MediaId")%>'>
                                        <%#Eval("Title")%></a></h4>
                                <dl>
                                    <dt>by: </dt>
                                    <dd>
                                        <a href="Media/Authors&amp;ID=<%#Eval("AuthorId")%>">
                                            <%#Eval("Author")%></a></dd>
                                    <dt>added: </dt>
                                    <dd>
                                        <%#Eval("CreateDate")%></dd>
                                </dl>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
            <!-- / .cont -->
        </div>
        <!-- / .box -->
    </div>
</div>--%>