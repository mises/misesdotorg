<%@ Control Language="C#" AutoEventWireup="True"
            Inherits="Controls.Sidebars.DailyArticleSidebar" Codebehind="DailyArticle.ascx.cs" %>
<%@ Register Src="~/Controls/Theme/AuthorBio.ascx" TagName="AuthorBio" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Tagging/RelatedDocuments.ascx" TagName="RelatedDocuments" TagPrefix="uc2" %>

<%@ OutputCache Duration="180" VaryByParam="*" Shared="false" %>
	
<uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="ArticlesSidebar" />
<uc1:AuthorBio ID="AuthorBio1" runat="server" />

<script src="//www.intensedebate.com/widgets/acctComment/150587/5" defer="defer" type="text/javascript"> </script>

<uc2:RelatedDocuments ID="RelatedDocuments1" runat="server" />