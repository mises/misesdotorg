<%@ Control Language="C#" AutoEventWireup="True" Inherits="Controls_Sidebars_Literature"
    CodeBehind="Literature.ascx.cs" %>
<%@ Register Src="~/Controls/PageContentControl.ascx" TagName="PageContentControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Tagging/RelatedDocuments.ascx" TagName="RelatedDocuments"
    TagPrefix="uc2" %>
<%@ Register Namespace="VRK.Controls" TagPrefix="vrk" Assembly="VRK.Controls" %>
<uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="LiteratureSidebar" />
<uc2:RelatedDocuments ID="RelatedDocuments1" runat="server" />
<div class="box">
    <h2>
        <a href="/clouds.aspx">User-Contributed Tags</a></h2>
    <div class="item">
        <asp:SqlDataSource ID="sqlUserTags" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            EnableCaching="True" SelectCommand="TagGetTopTags" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>        
        <vrk:Cloud ID="UserContributedCloud" runat="server" CssClass="littagcloud" DataHrefField="tag" DataHrefFormatString="/tag/{0}"
            DataSourceID="sqlUserTags" DataTextField="Tag" DataTitleField="Count" DataTitleFormatString="{0} documents"
            DataWeightField="Count" ItemCssClassPrefix="CommonTag">
        </vrk:Cloud>
    </div>
</div>
