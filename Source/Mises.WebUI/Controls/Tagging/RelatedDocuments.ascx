<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Sidebars_RelatedDocuments" Codebehind="RelatedDocuments.ascx.cs" %>
<%@ OutputCache Duration="3600" VaryByParam="Id" Shared="false" %>
<div class="box greateconomists">
    <asp:Repeater ID="dlRelatedDocuments" runat="server" DataSourceID="dsRelatedTags">
        <HeaderTemplate>
            <h2>
                Related</h2>
            <div class="cont">
            <ul class="economists">
        </HeaderTemplate>
        <ItemTemplate>
            <li><a href="<%#Eval("URL")%>">
                    <%#Eval("DocumentTypeString")%>:
                    <%#Eval("Title")%></a> </li>
        </ItemTemplate>
        <FooterTemplate>
        </ul>
            <!-- / .cont -->
        </div>
            <!-- / .box -->
        </FooterTemplate>
    </asp:Repeater>
    <asp:ObjectDataSource ID="dsRelatedTags" runat="server" SelectMethod="GetRelatedDocuments"
                          TypeName="Mises.Domain.Tags.Tagging" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="ArticleId" QueryStringField="Id" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>