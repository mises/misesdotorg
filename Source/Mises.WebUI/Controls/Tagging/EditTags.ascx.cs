#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Tags;
using Mises.Domain.Utility;

#endregion

partial class Controls_Tagging_EditTags : UserControl
{
    #region Properties

    public Guid Identifier { get; set; }

    #endregion

    public Controls_Tagging_EditTags()
    {
        Load += Page_Load;
    }

    private void Page_Load(object sender, EventArgs e)
    {
        gvTaggedPages.RowDeleting += gvTags_RowDeleting;
    }

    public void GetUserTags()
    {
        if (! Context.User.Identity.IsAuthenticated)
        {
            return;
        }
        var tagging = new Tagging();
        //Dim tags As New System.Collections.Generic.List(Of MisesWeb.TagItem)
        gvTaggedPages.DataSource = tagging.GetUserTags(Context.User.Identity.Name, Request.GetOriginalHostAddress());
        gvTaggedPages.DataBind();
        TagCloud.DataSource = tagging.TagItemList;
        TagCloud.DataBind();
    }


    protected void gvTags_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // Delete Selected Tag
        var Ident = new Guid(gvTaggedPages.DataKeys[e.RowIndex].Values[0].ToString());
        string tag = gvTaggedPages.DataKeys[e.RowIndex].Values[1].ToString();
        Tagging.DeleteUserDocumentTag(Context.User.Identity.Name, Ident, tag);
        GetUserTags();
    }
}