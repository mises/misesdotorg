<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Tagging_TaggingStats" Codebehind="TaggingStats.ascx.cs" %>
<a id="stats" name="stats">
    <h2>
        Tag Totals:</h2>
    <asp:GridView ID="gvTagStats" runat="server" AutoGenerateColumns="False" AllowSorting="False">
        <Columns>
            <asp:BoundField DataField="UniqueTags" HeaderText="Unique Tags" ReadOnly="True" SortExpression="UniqueTags" />
            <asp:BoundField DataField="BotTags" HeaderText="Bot Tags" ReadOnly="True" />
            <asp:BoundField DataField="TotalTaggedDocs" HeaderText="Total Tagged Docs" ReadOnly="True" />
            <asp:BoundField DataField="BotTaggedDocs" HeaderText="Bot Tagged Docs" ReadOnly="True" />
            <asp:BoundField DataField="UserTaggedDocs" HeaderText="User Tagged Docs" ReadOnly="True" />
            <asp:BoundField DataField="TodaysTagging" HeaderText="Todays Tagging Activity" ReadOnly="True" />
            <asp:BoundField DataField="TodaysTags" HeaderText="Today's New Tags" ReadOnly="True" />
        </Columns>
    </asp:GridView>
    <h2>
        Top taggers in the last 24 hours:</h2>
    <asp:GridView ID="gvTopUsersToday" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="TaggedBy" DataNavigateUrlFormatString="/tags.aspx?user={0}"
                                DataTextField="TaggedBy" HeaderText="Tagged By"></asp:HyperLinkField>
            <asp:BoundField DataField="Count" HeaderText="Count"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <h2>
        All-time top taggers:</h2>
    <asp:GridView ID="gvTopUsersAllTime" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="TaggedBy" DataNavigateUrlFormatString="/tags.aspx?user={0}"
                                DataTextField="TaggedBy" HeaderText="Tagged By"></asp:HyperLinkField>
            <asp:BoundField DataField="Count" HeaderText="Count"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <h2>
        Today's top tags:</h2>
    <asp:GridView ID="gvTagCount" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="Tag" DataNavigateUrlFormatString="/tag/{0}"
                                DataTextField="Tag" HeaderText="Tag" SortExpression="Tag" />
            <asp:BoundField DataField="Count" HeaderText="Count" ReadOnly="True" SortExpression="Count" />
        </Columns>
    </asp:GridView>
</a>