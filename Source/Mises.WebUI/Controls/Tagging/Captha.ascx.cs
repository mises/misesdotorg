#region

using System;
using System.Web.UI;

#endregion

partial class Controls_Tagging_Captha : UserControl
{
    public bool IsValid
    {
        get
        {
            if (Visible == false)
            {
                return true;
            }

            if (txtCaptcha.Text.Trim() == "5" || txtCaptcha.Text.Trim() == "five")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}