#region

using System;
using System.Data;
using System.Web.UI;
using Mises.Data;

#endregion

partial class Controls_Tagging_TaggingStats : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                              "dbo.TagGetStats");

        gvTagStats.DataSource = ds.Tables[0];
        gvTopUsersToday.DataSource = ds.Tables[1];
        gvTopUsersAllTime.DataSource = ds.Tables[2];
        gvTagCount.DataSource = ds.Tables[3];

        DataBind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}