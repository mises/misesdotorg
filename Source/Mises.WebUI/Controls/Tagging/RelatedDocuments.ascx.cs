#region

using System;
using System.Web.UI;
using Mises.Data;
using Mises.Domain.Tags;

#endregion

partial class Controls_Sidebars_RelatedDocuments : UserControl
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        try
        {
            if (DataFormat.IsGUID(Request.QueryString["Id"]))
            {
                var DocumentId = new Guid(Request.QueryString["Id"]);
                dlRelatedDocuments.DataSource = Tagging.GetRelatedDocuments(DocumentId);
                dlRelatedDocuments.DataSourceID = null;
            }
            dlRelatedDocuments.DataBind();
            if (dlRelatedDocuments.Items.Count == 0)
            {
                Visible = false;
            }
        }
        catch
        {
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        PreRender += Page_PreRender;
    }
}