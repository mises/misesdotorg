<%@ Control Language="C#" AutoEventWireup="false" Inherits="Controls_Tagging_TagCloud" Codebehind="TagCloud.ascx.cs" %>
<%@ Register Namespace="VRK.Controls" TagPrefix="vrk" Assembly="VRK.Controls" %>
<%@ Register Src="Captha.ascx" TagName="Captha" TagPrefix="uc1" %>
<%--<%@ OutputCache Duration="160" Shared="false" VaryByParam="*" VaryByCustom="userName" %>--%>

<div class="tagcloud">
    <h5>
        <a href="/clouds.aspx">User-Contributed Tags:</a>
        <asp:Label ID="lblTagResult" runat="server"></asp:Label></h5>
    <vrk:Cloud ID="TagCloud" runat="server" CssClass="tagcloud" DataHrefField="URL" DataTextField="Name"
               DataTitleField="Weight" DataTitleFormatString="{0} documents" DataWeightField="Weight"
               ItemCssClassPrefix="CommonTag">
    </vrk:Cloud>
    <uc1:Captha ID="Captha1" runat="server" Visible="false" />
    <asp:Panel ID="pnlLoggedIn" runat="server" CssClass="addtag" Visible="false">
        <fieldset>
            <asp:Label runat="server" ID="txtLabel" AssociatedControlID="txtTag" Text="Add a Tag" />
            <asp:TextBox ID="txtTag" runat="server">
            </asp:TextBox><asp:ImageButton  CssClass="ui-state-default ui-corner-all ui-button"  ID="btnTagDocument" runat="server"
                                           ImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/but-addtag.jpg" />
            <span>(Ex: Capitalism, Inflation)</span>
        </fieldset>
    </asp:Panel>
    <div class="addtag">
        <fieldset>
            <label for="txtTag" id="txtLabel">
                Add a Tag</label>
            <input name="txtTag" type="text" id="txtTag" /><input type="image" name="btnTagDocument"
                                                                  id="btnTagDocument" class="buttonimg" src="//mises.freecapitalists.org/images/Theme/images/buttons/but-addtag.jpg"
                                                                  onclick=" javascript:AddTag('<%=DocumentIdentifier%>');return false;" style="border-width: 0px;" />
            <span>(Ex: Human Action, Inflation)</span>
        </fieldset>
    </div>
</div>