<%@ Control Language="C#" AutoEventWireup="false" Inherits="Controls_Tagging_EditTags" Codebehind="EditTags.ascx.cs" %>
<h4>
    My Tags</h4>
<%@ Register Namespace="VRK.Controls" TagPrefix="vrk" Assembly="VRK.Controls" %>
<asp:GridView ID="gvTaggedPages" runat="server" AutoGenerateColumns="False" DataKeyNames="Identifier, Description">
    <Columns>
        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
        <asp:HyperLinkField DataNavigateUrlFields="URL" DataTextField="Title" HeaderText="Title"
                            SortExpression="Title" />
        <asp:CommandField ShowDeleteButton="True" />
    </Columns>
</asp:GridView>
<h4>
    My Tag Cloud</h4>
<div class="tagcloud">
    <vrk:Cloud ID="TagCloud" runat="server" CssClass="tagcloud" DataHrefField="URL"
               DataTextField="Name" DataTitleField="Weight" DataTitleFormatString="{0} documents"
               DataWeightField="Weight">
    </vrk:Cloud>
</div>