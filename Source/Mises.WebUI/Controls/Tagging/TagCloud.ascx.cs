#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Mises.Domain.Tags;
using Mises.Domain.Utility;

#endregion

partial class Controls_Tagging_TagCloud : UserControl
{
    private Guid mIdentifier;


    public Guid DocumentIdentifier
    {
        get
        {
            if (mIdentifier == Guid.Empty && (ViewState["mIdentifier"] != null || Request.QueryString["Id"] != null))
            {
                if (ViewState["mIdentifier"] != null)
                {
                    mIdentifier = (Guid) (ViewState["mIdentifier"]);
                }
                else
                {
                    if (Request.QueryString["Id"] != null && Request.QueryString["Id"].Length == 32)

                        ViewState["mIdentifier"] = new Guid(Request.QueryString["Id"]);
                }
            }

            if (mIdentifier == Guid.Empty && Context.Items["GUID"] != null)
            {
                mIdentifier = (Guid) Context.Items["GUID"];
            }

            return mIdentifier;
        }
        set
        {
            if (ViewState["mIdentifier"] != null)
            {
                return;
            }
            mIdentifier = value;
            ViewState["mIdentifier"] = value;
            Visible = true;
            Bind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnTagDocument.Click += btnTagDocument_Click;
        //btnTagDocument_Click.PostBackUrl
        btnTagDocument.PostBackUrl = Request.RawUrl;

        if (Page.IsPostBack)
        {
            return;
        }
        if (Context.User != null)
            Captha1.Visible = !Context.User.Identity.IsAuthenticated && !Request.Url.ToString().Contains("/manager");
    }

    private void Bind()
    {
        if (mIdentifier == Guid.Empty)
            Visible = false;


        Tagging.GetGoogleKeywords(DocumentIdentifier, Context);

        var list = Tagging.GetDocumentTagCloud(DocumentIdentifier);
        if (list.Any())
        {
            TagCloud.DataSource = list;
            TagCloud.DataBind();
            TagCloud.Visible = true;
        }
        else
        {
            TagCloud.Visible = false;
            lblTagResult.Text = " Tag this document!";
        }
    }

    protected void btnTagDocument_Click(object sender, EventArgs e)
    {
        if (!Captha1.IsValid)
        {
            lblTagResult.Text = "Invalid CAPTCHA text";
            return;
        }

        string user = Context.User.Identity.IsAuthenticated
                          ? Context.User.Identity.Name
                          : Request.GetOriginalHostAddress();

        Tagging.TagDocument(txtTag.Text, DocumentIdentifier, user);
        Bind();
    }
}