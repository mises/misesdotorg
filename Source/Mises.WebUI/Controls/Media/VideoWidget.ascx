﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="MisesWeb.Controls_Media_VideoWidget" Codebehind="VideoWidget.ascx.cs" %>

<%@ Register Src="~/Controls/Media/NoJsForVideo.ascx" TagName="NoJsForVideo" TagPrefix="mises" %>

<div class="widget">
    <div class="player">
        <% if (ViewModel.IsYoutubeClip)
           {%>
        <iframe title="YouTube video player" src="//www.youtube.com/embed/<%=ViewModel.Video.Url%>" frameborder="0" allowfullscreen></iframe>
        <% }
           else if (ViewModel.CanPlayNatively)
           {%>
        <video preload="none" controls="controls" poster="<%=ViewModel.Video.PosterUrl%>" src="<%=ViewModel.Video.Url%>" type="<%=ViewModel.Video.MimeType%>">
            <%--put in tag instead as IE9 wouldn't play <source src="<%=ViewModel.Video.Url%>" type="<%=ViewModel.Video.MimeType%>" />--%>
        </video>
        <% }
           else if (ViewModel.CanPlayInFlash)
           {%>
        <mises:NoJsForVideo id="NoJsForVideo1" runat="server" />
        <script type="text/javascript" src="/controls/media/jw/jwplayer.js"> </script>
        <div id="jwplayer" src="<%=ViewModel.Video.Url%>">
        </div>
        <script type="text/javascript">
			jwplayer("jwplayer").setup({ "flashplayer": "/controls/media/jw/player.swf" });
		</script>
        <% }
           else if (ViewModel.CanPlayInSilverlight)
           {%>
        <mises:NoJsForVideo id="NoJsForVideo2" runat="server" />
        <div id="slplayer" class="slplayer" data-file="<%=ViewModel.Video.Url%>" data-image="<%=ViewModel.Video.PosterUrl%>"></div>
        <% }
           else
           {%>
        <div class="message-info ui-corner-all">
            <span class="icon"></span>
            <p>We're sorry but a suitable playback mechanism for this video hasn't been detected for your browser but you can <a href="<%=ViewModel.Video.Url%>">download here</a> and watch in your own player.</p>
        </div>
        <% }%>
    </div>
    <div class="info">
        <h3><a href="/media/<%=ViewModel.Video.Id%>/<%=ViewModel.Title.ToSlug()%>"><%=ViewModel.Title%></a></h3>
        <h4><a href="/media/categories/<%=ViewModel.Category.Id%>/<%=ViewModel.Category.Name.ToSlug()%>"><%=ViewModel.Category.Name%></a></h4>
        <p><%=ViewModel.CreatedDate.ToString("D")%> by <a href="/authors/<%=ViewModel.Authors.First().Id%>/<%=ViewModel.Authors.First().FullName.ToSlug()%>"><%=ViewModel.Authors.First().FullName%></a></p>
        <p class="omega"><%=MvcHtmlString.Create(ViewModel.DescriptionHtml)%></p>
    </div>
</div>