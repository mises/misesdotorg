﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Media_NoJsForVideo" Codebehind="NoJsForVideo.ascx.cs" %>

<!-- NOTE: only expected to appear once on any one page... -->
<div id="<%=ClientID%>" class="message-info ui-corner-all">
    <span class="icon"></span>
    <p>This video couldn't be played because your browser does not natively support this type of video and you have javascript disabled.</p>
    <p>Your options:</p>
    <ul>
        <li>turn on javascript</li>
        <li>start using one of <a href="http://www.google.com/chrome">Chrome</a>, <a href="http://www.mozilla.com">Firefox</a>, <a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie">IE</a> or <a href="http://www.apple.com/safari/download/">Safari</a></li>
        <li>watch in your own player (<a href="<%=ViewModel.Video.Url%>">download available here</a>)</li>
    </ul>
</div>
<script type="text/javascript">
	;
	(function() {
	    var el = document.getElementById("<%=ClientID%>");
	    var p = el.parentNode;
	    p.removeChild(el);
	    el = null;
	    p = null;
	})();
</script>