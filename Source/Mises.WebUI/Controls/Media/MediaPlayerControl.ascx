<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Media_MediaPlayer" Codebehind="MediaPlayerControl.ascx.cs" %>
<asp:Literal ID="litPlayMp3" runat="server"></asp:Literal>
<asp:Panel ID="pnlVideo" runat="server" Visible="False">
    <div style="text-align: center">
        <object id='mediaPlayer' width="640" height="480" classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95'
                codebase='http://activex.microsoft.com/activex/controls/ mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
                standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
            <param name='fileName' value="<%
                                              Response.Write(VideoURL);
%>">
            <param name='animationatStart' value='1'>
            <param name='transparentatStart' value='1'>
            <param name='autoStart' value='1'>
            <param name='ShowControls' value='1'>
            <param name='ShowDisplay' value='0'>
            <param name='ShowStatusBar' value='0'>
            <param name='loop' value='0'>
            <embed type='application/x-mplayer2' pluginspage='http://microsoft.com/windows/mediaplayer/ en/download/'
                   id='mediaPlayer' name='mediaPlayer' displaysize='4' autosize='1' bgcolor='darkblue'
                   showcontrols='1' showtracker='1' autostart='1' showdisplay='0' showstatusbar='0'
                   videoborder3d='0' width="640" height="480"  src="<%
                                              Response.Write(VideoURL);
%>" autostart='1' designtimesp='5311' loop='0'>
            </embed>
        </object>
    </div>
</asp:Panel>