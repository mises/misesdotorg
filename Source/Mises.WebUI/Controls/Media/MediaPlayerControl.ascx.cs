#region

using System;
using System.Web.UI;

#endregion

public partial class Controls_Media_MediaPlayer : UserControl
{
    public string VideoURL;

    public string MediaURL
    {
        get { return ""; }
        set
        {
            if (value.Contains(".mp3"))
            {
                string baseURL = Request.Url.Scheme + "://" + Request.Url.Authority;
                litPlayMp3.Text =
                    string.Format(
                        "<script type=\"text/javascript\" src=\"{0}/Controls/Media/JW/swfobject.js\"></script><embed allowfullscreen=\"true\" allowscriptaccess=\"always\" flashvars=\"&file={1}&height=20&width=320&autostart=true\" height=\"20\" src=\"{2}/Controls/Media/JW/mediaplayer.swf\" width=\"320\"></embed>",
                        baseURL, value, baseURL);
            }
            else
            {
                VideoURL = value;
                pnlVideo.Visible = true;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}