﻿#region

using System;
using System.Web;
using System.Web.UI;
using Mises.Web.ViewModels.Media;

#endregion

namespace MisesWeb
{
    public partial class Controls_Media_VideoWidget : UserControl
    {
        private VideoWidgetViewModel _viewModel;

        public VideoWidgetViewModel ViewModel
        {
            get { return _viewModel; }
            set
            {
                _viewModel = value;
                _viewModel.Browser = HttpContext.Current.Request.Browser;

                NoJsForVideo1.ViewModel = _viewModel;
                NoJsForVideo2.ViewModel = _viewModel;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}