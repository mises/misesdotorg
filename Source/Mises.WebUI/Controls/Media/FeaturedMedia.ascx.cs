#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Data;
using Mises.Domain.Media;

#endregion

public partial class Controls_Media_FeaturedMedia : UserControl
{
    private List<DocumentFile> DocumentFiles;

    public Controls_Media_FeaturedMedia()
    {
        Load += Page_Load;
    }

    public string mediaURL { get; set; }
    public string thumbnailURL { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var featuredMedia = MediaCatalog.GetFeaturedMedia();
        var db = DataHelper.EntityDataModel;

        int documentId = featuredMedia.First().DocumentId;

        DocumentFiles = db.DocumentFiles.Include("DocumentMediaType").Where(df => df.DocumentId == documentId).ToList();
        
        rptFeaturedMedia.ItemDataBound += FeaturedMediaDataBinding;
        rptFeaturedMedia.DataSource = featuredMedia;
        rptFeaturedMedia.DataBind();
    }

    private void FeaturedMediaDataBinding(object sender, RepeaterItemEventArgs repeaterItemEventArgs)
    {
        if (DocumentFiles.Count == 0) return;

        int DocumentId = Convert.ToInt32(((MediaGetFeaturedResult)repeaterItemEventArgs.Item.DataItem).DocumentId);
        //Convert.ToInt32(((Literal)repeaterItemEventArgs.Item.FindControl("litDocumentId")).Text);

        var documentFiles = DocumentFiles.Where(p => p.DocumentId == DocumentId);

        // var player = (MediaPlayer)(repeaterItemEventArgs.Item.FindControl("MediaPlayerControl"));
        //player.Sk = 


        if (null != (documentFiles.ToList().FindLast(p => p.MediaTypeId == 16)))
        {
            // Streaming link found
            ((Literal) repeaterItemEventArgs.Item.FindControl("litMediaPlayer")).Text =
                MediaCatalog.GetYouTubeEmbedCode(documentFiles.ToList().FindLast(p => p.MediaTypeId == 16).URL);
            repeaterItemEventArgs.Item.FindControl("player").Visible = false;
            //player.Visible = false;
            repeaterItemEventArgs.Item.FindControl("lnkTitle").Visible = false;
            repeaterItemEventArgs.Item.FindControl("lnkAuthor").Visible = false;
            mediaURL = documentFiles.ToList().FindLast(p => p.MediaTypeId == 16).URL;
        }
        else if (null != (documentFiles.ToList().FindLast(p => p.MediaTypeId == 12)))
        {
            // Streaming link found
            mediaURL = documentFiles.ToList().FindLast(p => p.MediaTypeId == 12).URL;
        }
        else if (null != (documentFiles.ToList().FindLast(p => p.MediaTypeId == 2)))
        {
            // WMV found
            mediaURL = documentFiles.ToList().FindLast(p => p.MediaTypeId == 2).URL;
        }
        else if (null != (documentFiles.ToList().FindLast(p => p.MediaTypeId == 14)))
        {
            // MP4 found
            mediaURL = documentFiles.ToList().FindLast(p => p.MediaTypeId == 14).URL;
        }
        else if (null != (documentFiles.ToList().FindLast(p => p.MediaTypeId == 1)))
        {
            // MP3 found
            mediaURL = documentFiles.ToList().FindLast(p => p.MediaTypeId == 1).URL;
        }
        else
        {
            // Streaming file not found
            // todo: add image link
            return;
        }
        //player.MediaSource = mediaURL;

        var fileControl = ((Repeater) repeaterItemEventArgs.Item.FindControl("repDownloadFile"));

        //documentFiles.ToList().ForEach(p => p.MediaIconPath = p.Description);
        //documentFiles.ToList().ForEach(p => p.Description = p.URL.Substring(p.URL.LastIndexOf(".") + 1).ToUpper());

        fileControl.DataSource = documentFiles.Where(p => p.MediaTypeId != 12);
        fileControl.DataBind();


        //((Literal)repeaterItemEventArgs.Item.FindControl("litMediaUrl")).Text = mediaURL;
        //((Literal)repeaterItemEventArgs.Item.FindControl("litPreviewUrl")).Text =
        //    string.Format("/media/poster/{0}", DocumentId);


        if (Request.UserAgent != null && (Request.UserAgent.Contains("Mobile"))) // iPhone/iPad
        {
            var file = documentFiles.FirstOrDefault(p => p.MediaTypeId == 14);
            if (file != null)
            {
                mediaURL = file.URL;

                if (mediaURL == null)
                {
                    // No supported iPad type found
                    ((Literal) repeaterItemEventArgs.Item.FindControl("litMediaPlayer")).Text =
                        "No HTML5 compatible video type found.";
                }
                // Use HTML for video
                ((Literal) repeaterItemEventArgs.Item.FindControl("litMediaPlayer")).Text =
                    string.Format(
                        "<video class=\"video\" style=\"width:345px;height:285px\" autoplay=\"autoplay\" controls=\"controls\" preload=\"preload\"><source src=\"{0}\"></video>",
                        mediaURL);

                //var ctr = (repeaterItemEventArgs.Item.FindControl("MediaPlayerControl"));

                //if (ctr != null) ctr.Visible = false;
            }
        }
    }
}