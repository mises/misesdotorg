<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Media_FeaturedMedia" Codebehind="FeaturedMedia.ascx.cs" %>
<%@ Import Namespace="Mises.Data" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.SilverlightControls" Assembly="Mises.Web" %>
<%--<%@ OutputCache Duration="180" VaryByParam="none" %>--%>
<%@ Register Assembly="System.Web.Silverlight" Namespace="System.Web.UI.SilverlightControls"
             TagPrefix="asp" %>
<div class="featured-media">
    <asp:Repeater runat="server" ID="rptFeaturedMedia">
        <ItemTemplate>
            <asp:Literal runat="server" ID="litMediaPlayer"></asp:Literal>
            <%--<asp:MediaPlayer ID="MediaPlayerControl" runat="server" Width="345px" Height="285px"
                SkinID="" AutoPlay="True">
            </asp:MediaPlayer>--%>

            <asp:Silverlight ID="player" 
                             runat="server" 
                             Source="~/ClientBin/VideoPlayer.xap" 
                             MinimumVersion="2.0.31005" 
                             Width="348" Height="245" InitParameters="m=<%#mediaURL%>" />


            <h3 class="media-title">
                <asp:HyperLink ID="lnkTitle" runat="server" NavigateUrl='<%#Eval("URL")%>' Text='<%#Eval("Title")%>'>
                </asp:HyperLink>
            </h3>
            <p class="byline">
                <asp:HyperLink ID="lnkAuthor" runat="server" Text='<%#Eval("Author")%>' NavigateUrl='<%#string.Format("/media/author/{0}/{1}", Eval("Author1"),
                                           DataFormat.GenerateSlug(Eval("Author")))%>'>'&gt;</asp:HyperLink>
            </p>
            <p class="description">
                <%--<span class="event">'<%#Eval("PublicationInformation")%></span> | --%>
                <asp:Literal ID="litDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Literal>
            </p>
            <p class="date">
                <%#Convert.ToDateTime(Eval("CreateDate")).ToLongDateString()%>
            </p>
            <p class="summary">
            </p>
            <p class="download">
            Download:
            <asp:Repeater ID="repDownloadFile" runat="server">
                <ItemTemplate>
                    <a href="<%#Eval("URL")%>">
                        <%#Eval("DocumentMediaType.Description")%></a>
                </ItemTemplate>
                <SeparatorTemplate>
                    |
                </SeparatorTemplate>
            </asp:Repeater>
            <%--<a title="mp3" href="">MP3</a> | <a title="wmv" href="">WMV</a> | <a title="mp4"
                    href="">MP4</a> | <a title="torrent" href="">TORRENT</a></p>--%>
            <%--<ul>
                    <li><a href="#" onclick="window.open('/MyMises/EmailPage.aspx?url=' + escape(self.location) + '&amp;title=' + escape(document.title),'mywin',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;" title="Share">Share</a></li>
                    <li><a href="#" onclick="window.open('/MyMises/EditFavorite.aspx?url=' + escape(self.location) + '&amp;title=' + escape(document.title),'mywin',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;" title="Add to MyMises">Add to
                        MyMises</a></li>
                </ul>--%>
            <br clear="all" />
        </ItemTemplate>
    </asp:Repeater>
</div>