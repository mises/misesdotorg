<%@ WebHandler Language="C#" Class="DocumentImage" %>

#region

using System;
using System.Web;
using Mises.Domain.Documents;

#endregion

public class DocumentImage : IHttpHandler
{
    #region IHttpHandler Members

    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        if (string.IsNullOrEmpty(context.Request.QueryString["Id"]) || context.Request.QueryString["Id"] == "{0}")
            return;

        byte[] image = ContentDocument.GetMetaImage((Convert.ToInt32(context.Request.QueryString["Id"])));
        context.Response.ContentType = "Image/JPEG";

        context.Response.Cache.SetExpires(DateTime.Now.AddYears(1));
        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.Cache.VaryByParams["Id"] = true;

        if (image != null)
        {
            context.Response.BinaryWrite(image);
        }
        else
        {
            var imageArray = new byte[2];
            imageArray[0] = 0;
            context.Response.BinaryWrite(imageArray);
        }
    }

    #endregion
}