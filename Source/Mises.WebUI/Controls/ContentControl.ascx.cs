#region

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Documents;
using Mises.Domain.Media;
using Mises.Domain.Utility;
using Document = Mises.Domain.Documents.Document;
using d = Mises.Domain.Documents;

#endregion

public partial class Controls_ContentControl : UserControl
{
    #region Public Properties

    public int DefaultId { get; set; }

    public string MissingDefaultRedirectURL { get; set; }

    public string ISBN { get; set; }

    #endregion

	/// <summary>
	///		Enumerates columns of <see cref="gvOtherFormats"/>; use for accessing columns programmatically by index.
	/// </summary>
	private enum OtherFormatsGridColumns : int
	{
		MediaIconPath = 0,
		FileFormat,
		VolumeOrdinal,
		FileSize,
		CreateDate
	}

    private int _EditId;
    private Guid _Identifier;

 

    private void BindPage(Document doc)
    {
        litPageTitle.Text = string.Format("{0}", doc.Title);
        litPageTitle.Visible = true;

        if (doc.Title.StartsWith("Duplicate: ["))
        {
            string id = doc.Title.Substring(12, doc.Title.IndexOf("]") - 12);
            Response.RedirectPermanent("/document/" + id + "/" + doc.Title.ToSlug());
        }

        string author = string.Empty;

        if (doc is ContentDocument)
        {
            var contentDoc = (ContentDocument)doc;

            litPublicationInformation.Text = contentDoc.PublicationInformation;
            if ((contentDoc).StoreDescription != null)
                litPublicationInformation.Text = (contentDoc).StoreDescription;
            if ((contentDoc).MetaImage != null)
            {
                imgCover.Visible = true;
                imgCover.Src = string.Format("/media/poster/{0}", doc.Id);
            }
            else
            {
                imgCover.Visible = false;
            }

            if (!string.IsNullOrEmpty((contentDoc).CoverImage))
            {
                imgCover.Visible = true;
                imgCover.Src = (contentDoc).CoverImage;
            }

            if (imgCover.Visible)
            {
                var metaImg = new HtmlMeta { Content = Mises.Data.DataFormat.GetAbsoluteURL(imgCover.Src) };
                metaImg.Attributes.Add("property", "og:image");
                Page.Header.Controls.Add(metaImg);
            }

            author = contentDoc.AuthorName;
            if (!string.IsNullOrEmpty(contentDoc.CoAuthorName.Trim()))
            {
                author += " and " + contentDoc.CoAuthorName;
            }

            var meta = new HtmlMeta() { Content = doc.Title };
            meta.Attributes.Add("property", "og:title");
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta { Content = "book" };
            meta.Attributes.Add("property", "og:type");
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta { Content = string.Format("http://mises.freecapitalists.org//document/{0}/{1}",doc.Id, doc.Title.ToSlug()) };
            meta.Attributes.Add("property", "og:url");
            Page.Header.Controls.Add(meta);

            meta = new HtmlMeta { Content = "216712848362908" };
            meta.Attributes.Add("property", "app_id");
            Page.Header.Controls.Add(meta);
        }


        switch (doc.DocumentType)
        {
            case DocumentType.Page:
                if (doc.Contents.StartsWith("<h1"))
                {
                    litPageTitle.Visible = false;
                }

                if (doc.Contents.Trim().StartsWith("<p") || doc.Contents.Trim().StartsWith("<h"))
                {
                    litContent.Text = doc.Contents;
                }
                else
                {
                    litContent.Text = "<p>" + doc.Contents + "</p>";
                }

                if (string.IsNullOrEmpty(doc.Contents))
                {
                    litContent.Text = doc.Description;
                }

                litContent.Text = doc.Contents;
                break;
            case DocumentType.Media:
                litContent.Text = doc.Description;

                if (!(doc is ContentDocument))
                {
                    if (doc.Contents.StartsWith("http:"))
                        Response.RedirectPermanent(doc.Contents);
                    else
                        litContent.Text = doc.Contents;

                    return;
                }

                var media = (ContentDocument)doc;

                if (media.ProductId > 0)
                {
                    media.DocumentFileList.Insert(0, new DocumentFile
                                                      {
                                                          VolumeComment = "Mises Store Link",
                                                          URL =
                                                              string.Format(
                                                                  "http://store.mises.org/Product.aspx?ProductId={0}&utm_source=Resources",
                                                                  media.ProductId),
                                                          MediaTypeId = 7,
                                                          DocumentMediaType = new DocumentMediaType() { MediaIconPath = "/images/icons/store.png", Description = "Mises Store Link", },
                                                          CreateDate = media.CreateDate
                                                      });
                }

                if (!string.IsNullOrEmpty(media.ISBN))
                {
                    ISBN = media.ISBN;
                    pnlGoogleBooks.Visible = true;
                }

                //if (media.FullText)
                //{
                //    media.DocumentFileList.Insert(0, new DocumentFile
                //                                      {
                //                                          Description = "HTML Version",
                //                                          URL =
                //                                              string.Format(
                //                                                  "http://mises.freecapitalists.org//resources.aspx?Id={0}&html=1",
                //                                                  media.Id),
                //                                          MediaTypeId = 1,
                //                                          MediaIconPath = "/images/Icons/html.png",
                //                                          CreateDate = media.CreateDate
                //                                      });
                //}

                if (media.DocumentFileList.Count > 0)
                {
                    // If there is only a media file, redirect to media
                    if (media.DocumentFileList.Count == 1 && media.DocumentFileList[0].DocumentMediaType.IsMedia)
                        Response.RedirectPermanent(string.Format("/media/{0}/{1}", media.DocumentFileList[0].FileId,media.Title.ToSlug() ));

                    //media.DocumentFileList.Remove(media.DocumentFileList.FindLast(p => p.MediaId == mediaId));
                    media.DocumentFileList.ForEach(m => MyPage.ConvertToMobileUrl(m.URL));

					// if all formats are single folume hide volume number column
					bool multiVolume = media.DocumentFileList.Exists(m => m.VolumeOrdinal > 1);
					gvOtherFormats.Columns[(int)OtherFormatsGridColumns.VolumeOrdinal].Visible = multiVolume;

                    gvOtherFormats.DataSource = media.DocumentFileList;
                    gvOtherFormats.DataBind();
                    pnlResources.Visible = true;
                }
                else
                    // Show PDF URL
                    litContent.Text = doc.Contents;
                break;
            default:
                if (doc.Contents != null && doc.Contents.Length < 500 &&
                    (doc.Contents.EndsWith(".asp") || doc.Contents.EndsWith(".htm") || doc.Contents.EndsWith(".html") ||
                     doc.Contents.EndsWith(".php") || doc.Contents.EndsWith(".aspx") || doc.Contents.EndsWith(".pdf") ||
                     doc.Contents.EndsWith(".epub")))
                {
                    Response.RedirectPermanent(doc.Contents);
                }
                else
                {
                    // This document is html content only
                    litContent.Text = doc.Contents;
                }

                break;
        }

        if (!string.IsNullOrEmpty(author))
        {
            var hm = new HtmlMeta { Name = "Author", Content = author };
            Page.Header.Controls.Add(hm);
        }

        if (!string.IsNullOrEmpty(doc.Keywords))
        {
            var hm = new HtmlMeta { Name = "Keywords", Content = doc.Keywords };
            Page.Header.Controls.Add(hm);
        }

        if (!string.IsNullOrEmpty(doc.Description))
        {
            var hm = new HtmlMeta { Name = "Description", Content = DataFormat.StripHTML(doc.Description) };
            Page.Header.Controls.Add(hm);
        }

        Page.Title = string.Format("{0} - {1} - Mises Institute", doc.Title, author);
        _Identifier = doc.Identifier;
        litAuthor.Text = author;

        //string url = Request.Url.AbsoluteUri;
        //if (!string.IsNullOrEmpty(doc.CustomUrl))
        //{
        //    url = Request.Url.Host + "/" + doc.CustomUrl;
        //}

        Comments1.DocumentId = doc.Identifier.ToString();
        Bookmark1.Bind(Request.RawUrl, doc.Title, doc.Description);
        if (TagCloud != null)
        {
            TagCloud.DocumentIdentifier = doc.Identifier;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _Identifier = Guid.Empty;

        string tempId = Request.QueryString["Id"];
        if (string.IsNullOrEmpty(tempId))
        {
            tempId = Request.QueryString["Id"];
        }
        if (Information.IsNumeric(tempId))
        {
            _EditId = Convert.ToInt32(Conversion.Val(tempId));
        }
        else if (DataFormat.IsGUID(tempId))
        {
            _Identifier = new Guid(tempId);
        }
        else
        {
            if (!string.IsNullOrWhiteSpace(tempId)) // invalid ID passed - error out
                Response.RedirectPermanent("/Error/Http404/?e=invalid+page+id");
        }

        if (Request.QueryString["p"] != null) // requesting an old-format html page
        {
            string url = Request.QueryString["p"];
            string aspPage = url.Substring(url.LastIndexOf("/") + 1);
            string path = url.IndexOf("?p=") == -1
                              ? url.Replace(aspPage, "")
                              : url.Substring(url.IndexOf("?p=") + 3).Replace(aspPage, "");

            _Identifier = (Document.GetPageGUID(aspPage, path));

            if (Guid.Empty == _Identifier)
            {
                //url = url.Replace("resources.aspx?p=", "");
                Response.RedirectPermanent(string.Format("/Error/Http404/?requestedUrl={0}", Server.UrlEncode(url)));
            }
            else
            {
                //Server.Transfer(string.Format("resources.aspx?Id={0}", _Identifier), false);
            }
        }


        // Check for default parameters
        if (_EditId == 0 && _Identifier == Guid.Empty && DefaultId > 0)
        {
            _EditId = DefaultId;
        }
        if (_EditId == 0 && _Identifier == Guid.Empty && MissingDefaultRedirectURL.Length > 1)
        {
            Response.RedirectPermanent(MissingDefaultRedirectURL);
        }

        if (_EditId > 0) // Requested Document Id
        {
            var document = new ContentDocument(_EditId);
            //if (document.FullText)
            //{
            //    if (document.DocumentFileList.Count == 0 || Request.QueryString["html"] != null)
            //    {
            //        document.DocumentType = DocumentType.Content;
            //    }
            //    else
            //    {
            //        document.DocumentType = DocumentType.Media;
            //    }
            //    BindPage(document);
            //}
            //else
            //{
                if (document.DocumentFileList.Count > 0) // show media list
                {
                    //Response.RedirectPermanent(document.DocumentFileList[0].URL);
                    document.DocumentType = DocumentType.Media;
                    BindPage(document);
                }
            //}
        }
        else if (_Identifier.ToString() != "") // Get Contents by Identifier
        {
            var doc = new Document(_Identifier);
            if (doc.Id == 0)
            {
                litContent.Text = "<h2>Content not found!</h2>";
                return;
            }
            BindPage(doc);
        }
        else if (!string.IsNullOrEmpty(MissingDefaultRedirectURL))
        {
            Server.Transfer(MissingDefaultRedirectURL);
        }
        else
        {
            throw new Exception("Missing identifier and no default URL specified");
        }
    }
}