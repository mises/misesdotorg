<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="Controls_ContentControl"
    CodeBehind="ContentControl.ascx.cs" %>
<%@ Register Src="Media/MediaPlayerControl.ascx" TagName="MediaPlayer" TagPrefix="uc3" %>
<%@ Register Src="Navigation/Bookmark.ascx" TagName="Bookmark" TagPrefix="uc2" %>
<%@ Register Src="Tagging/TagCloud.ascx" TagName="TagCloud" TagPrefix="uc1" %>
<%@ Register Src="Theme/Comments.ascx" TagName="Comments" TagPrefix="uc4" %>
<asp:Panel ID="pnlNextPrev" runat="server" Visible="false">
    <asp:HyperLink ID="lnkPreviousPage" Visible="false" runat="server">&lt;&lt; PreviousPage</asp:HyperLink>
    <asp:HyperLink ID="lnkNextPage" Visible="false" runat="server">Next Page &gt;&gt;</asp:HyperLink>
</asp:Panel>
<h1 rel="title">
    <asp:Literal ID="litPageTitle" runat="server"></asp:Literal></h1>
<div id="ISBN" style="display: none">
    <%=ISBN%></div>
<h2 rel="author">
    <asp:Literal ID="litAuthor" runat="server"></asp:Literal></h2>
<asp:Panel ID="pnlResources" runat="server" Visible="false">
    <asp:GridView ID="gvOtherFormats" runat="server" AutoGenerateColumns="False">
        <Columns>
          <%--  <asp:ImageField DataImageUrlField="DocumentMediaType.MediaIconPath" HeaderText="">
            </asp:ImageField>--%>
            <asp:TemplateField HeaderText="File Format:">
                <ItemTemplate>
                    <a href="<%#Eval("URL")%>" onclick="_gaq.push(['_trackEvent', 'Resource', 'Download', '<%#Eval("URL")%>']);">
                        <%#Eval("DocumentMediaType.Description")%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="VolumeOrdinal" HeaderText="Volume" />
            <asp:BoundField DataField="FileSize" DataFormatString="{0: #,#}" HeaderText="Size (bytes)" />
            <asp:BoundField DataField="CreateDate" HeaderText="Created" DataFormatString="{0:d}" />
            <%--<asp:BoundField DataField="Duration" HeaderText="Duration" />--%>
        </Columns>
    </asp:GridView>
</asp:Panel>
<asp:Panel ID="pnlGoogleBooks" runat="server" Visible='false'>
    <script type="text/javascript" src="http://books.google.com/books/previewlib.js"> </script>
    <script type="text/javascript">
        GBS_insertPreviewButtonPopup('ISBN:<%=ISBN%>');
    </script>
</asp:Panel>
<img id="imgCover" runat="server" src="" alt="cover image" visible="false" class="alignright" />
<p>
    <asp:Literal ID="litContent" runat="server"></asp:Literal>
</p>
<p>
    <asp:Literal ID="litPublicationInformation" runat="server"></asp:Literal>
</p>
<uc3:MediaPlayer ID="MediaPlayer1" runat="server" Visible="False" />
<uc4:Comments ID="Comments1" runat="server" />
<uc1:TagCloud ID="TagCloud" runat="server" />
<uc2:Bookmark ID="Bookmark1" runat="server" />
