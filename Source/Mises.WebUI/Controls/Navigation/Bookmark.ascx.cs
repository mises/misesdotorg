#region

using System;
using System.Web.UI;

#endregion

partial class Controls_Navigation_Bookmark : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (lnkStubleUpon.HRef == string.Empty)
        //{
        //    Visible = false;
        //}
    }


    public void Bind(string URL, string Title)
    {
        Bind(URL, Title, "");
    }

    public void Bind(string URL, string Title, string Description)
    {
        if (string.IsNullOrEmpty(Title))
        {
            return;
        }
        //Title = Server.HtmlEncode(DataFormat.StripHTML(Title));
        //Description = Server.HtmlEncode(DataFormat.StripHTML(Description));
        //URL = Server.UrlEncode((DataFormat.GetAbsoluteURL(URL)));

        //lnkDigg.HRef = string.Format("http://digg.com/submit?phase=2&amp;bodytext={2}&amp;title={0}&amp;url={1}", Title,
        //                             URL, Description);
        //lnkDelicious.HRef = string.Format("http://del.icio.us/post?v=2&amp;url={1}&amp;title={0}", Title, URL);
        //lnkNewswine.HRef = string.Format("http://www.newsvine.com/_tools/seed?popoff=0&amp;u={1}&amp;h={0}", Title, URL);
        //// lnkReddit.HRef = string.Format("http://reddit.com/submit?url={1}&amp;title={0}", Title, URL);
        //lnkFacebook.HRef = string.Format("http://www.facebook.com/share.php?u={0}", URL);
        //lnkStubleUpon.HRef = string.Format("http://www.stumbleupon.com/submit?url={1}&amp;title={0}", Title, URL);
        Visible = true;
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}