﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Controls.Navigation
{
    public partial class LetterSelector : System.Web.UI.UserControl
    {
        public event EventHandler<LetterSelectedEventArgs> OnLetterSelected;

        private Dictionary<string, LinkButton> letterLinks;

        public string SelectedLetter
        {
            get { return ViewState["SelectedLetter"] == null ? string.Empty : ViewState["SelectedLetter"].ToString(); }
            set { ViewState["SelectedLetter"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void RenderLinks()
        {
            letterLinks = new Dictionary<string, LinkButton>();

            var codeForA = Convert.ToInt32('A');
            var codeForZ = Convert.ToInt32('Z');

            for (var code = codeForA; code <= codeForZ; code++)
            {
                if(code > codeForA)
                {
                    var literal = new Literal {Text = " | "};
                    placeHolder.Controls.Add(literal);
                }

                var text = Convert.ToChar(code).ToString();

                var linkButton = new LinkButton
                                     {
                                         Text = text,
                                         CssClass = "letterLink",
                                         CommandArgument = text,
                                         EnableViewState = false
                                     };
                //linkButton.Font.Bold = false;

                linkButton.Click += linkButton_Click;

                placeHolder.Controls.Add(linkButton);

                letterLinks.Add(text, linkButton);
            }
        }

        void linkButton_Click(object sender, EventArgs e)
        {
            var linkButton = (LinkButton) sender;
            SelectedLetter = linkButton.CommandArgument;
            linkButton.Font.Bold = true;

            InvokeLetterSelected(SelectedLetter);
        }

        protected override void OnPreRender(EventArgs e)
        {
            
        }

        protected override void OnInit(EventArgs e)
        {
            RenderLinks();
        }

        public void InvokeLetterSelected(string letter)
        {
            EventHandler<LetterSelectedEventArgs> handler = OnLetterSelected;
            if (handler != null) handler(this, new LetterSelectedEventArgs(letter));
        }
    }

    /// <summary>
    /// LetterSelectedEventArgs is the class containing letter selected event data.
    /// </summary>
    public class LetterSelectedEventArgs : EventArgs
    {
        public string Letter { get; private set; }

        public LetterSelectedEventArgs(string letter)
        {
            Letter = letter;
        }
    }
}