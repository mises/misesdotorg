<%@ Control Language="C#" AutoEventWireup="false" Inherits="Controls_Theme_EventsList" Codebehind="EventsList.ascx.cs" %>
<div class="box upcomingevents">
    <h2>
        Upcoming Events</h2>
    <a class="rss" href="/events.xml">rss</a>
    <div class="cont">
        <ul class="eventsbig">
            <asp:Repeater ID="dlUpcomingEvents" runat="server" DataSourceID="sqlGetUpcomingEvents">
                <ItemTemplate>
                    <li>
                        <%--<h3>
							<a href="/events/<%#DataBinder.Eval(Container, "DataItem.EventId")%>">
								<%#DataBinder.Eval(Container, "DataItem.Location")%></a> <span>
									<%#DataBinder.Eval(Container, "DataItem.EventDate")%></span></h3>--%>
                        <a href="/events/<%#DataBinder.Eval(Container, "DataItem.EventId")%>">
                            <img src="<%#DataBinder.Eval(Container, "DataItem.EventImage")%>" alt="Event Image"
                                 width="257" height="87" style="width: 257; height: 87;" /></a>
                        <%--<h4>
							<%#DataBinder.Eval(Container, "DataItem.Title")%></h4>--%>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource ID="sqlGetUpcomingEvents" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                               EnableCaching="True" CacheDuration="10" SelectCommand="CalendarGetUpcomingEvents"
                               SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter DefaultValue="4" Name="limit" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </ul>
        <a class="more" href="/events/">view all&hellip;</a>
        <!-- / .cont -->
    </div>
    <!-- / .box -->
</div>