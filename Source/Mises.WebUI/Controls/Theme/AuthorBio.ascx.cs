#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Domain.Documents;

#endregion

partial class Controls_Theme_AuthorBio : UserControl
{
    private int AuthorId;

    public Controls_Theme_AuthorBio()
    {
        Load += Page_Load;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            rptAuthorBio.ItemDataBound += rptAuthorBio_ItemDataBound;

            int ArticleId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
            AuthorId = Convert.ToInt32(Conversion.Val(Request.QueryString["AuthorId"]));

            if (ArticleId > 0)
            {
                ShowArticleAuthorInfo(ArticleId);
            }
            else if (AuthorId > 0)
            {
                ShowAuthorInfo(AuthorId);
            }
            else
            {
                Visible = false;
            }
        }
        catch
        {
            Visible = false;
        }
    }

    public void ShowArticleAuthorInfo(int ArticleId)
    {
        rptAuthorBio.DataSource = DocumentAuthors.GetAuthorBioByArticleId(ArticleId);
        rptAuthorBio.DataBind();
    }

    public void ShowAuthorInfo(int AuthorId)
    {
        if (AuthorId == 0)
        {
            Visible = false;
            return;
        }

        rptAuthorBio.DataSource = DocumentAuthors.GetAuthorBioByAuthorId(AuthorId);
        rptAuthorBio.DataBind();

        if (rptAuthorBio.Items.Count == 0)
        {
            Visible = false;
        }
    }

    protected void rptAuthorBio_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var imgAuthorThumbs = (Image)(e.Item.FindControl("imgAuthorThumbs"));
        if (imgAuthorThumbs.ImageUrl == string.Empty)
        {
            imgAuthorThumbs.Visible = false;
        }
    }
}