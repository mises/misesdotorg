<%@ Control Language="C#" AutoEventWireup="false"
            EnableViewState="false" Inherits="Controls.Theme.Controls_Theme_CommunityTabs" Codebehind="CommunityTabs.ascx.cs" %>
<%--<%@ OutputCache Duration="180" VaryByParam="none" Shared="true" %>--%>
<div class="outerbox">
    <div id="tabs-community">
        <ul>
            <li><a href="#tab-blog">Mises Blog</a></li>
            <li><a href="#tab-blogs">User Blogs</a></li>
            <li><a href="#tab-forums">Forums</a></li>
        </ul>
        <div class="box communitytabbed">
            <div id="tab-blog">
                <h2>
                    Mises Blog</h2>
                <div class="cont">
                    <ul class="posts">
                        <asp:Repeater ID="dlMisesBlog" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("link")%>' Text='<%#Eval("title")%>'>
                                    </asp:HyperLink>
                                    <em>
                                        <%#Eval("PubDate")%>
                                        by <a href="<%#Eval("user_url")%>">
                                               <%#Eval("author")%></a></em> </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <a class="more" href="http://blog.mises.org/">view all&hellip;</a> <a class="rss"
                                                                                          href="http://feeds.mises.org/MisesBlog">mises blog rss</a>
                    <!-- / .cont -->
                </div>
            </div>
            <div id="tab-blogs">
                <h2>
                    Community Blogs</h2>
                <div class="cont">
                    <ul class="posts">
                        <asp:Repeater ID="dlAustrianNetworkBlogs" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("link")%>' Text='<%#Eval("title")%>'>
                                    </asp:HyperLink>
                                    <em>
                                        <%#Eval("PubDate")%>
                                        by <a href="/Community/user/Profile.aspx?UserID=<%#Eval("UserId")%>">
                                               <%#Eval("author")%></a></em> </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <a class="more" href="/Community/blogs/">view all&hellip;</a> <a class="rss" href="/Community/blogs/MainFeed.aspx">
                                                                                      user blogs rss</a>
                    <!-- / .cont -->
                </div>
            </div>
            <div id="tab-forums">
                <h2>
                    Mises Forums</h2>
                <div class="cont">
                    <ul class="posts">
                        <asp:Repeater ID="dlAustrianNetworkForums" runat="server">
                            <ItemTemplate>
                                <li><a href="/Community/forums/t/<%#Eval("ThreadId")%>.aspx">
                                        <%#Eval("Subject")%></a> <em>
                                                                     <%#Eval("PostDate")%>
                                                                     by <a href="/Community/user/Profile.aspx?UserID=<%#Eval("UserId")%>">
                                                                            <%#Eval("PostAuthor")%></a></em> </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <a class="more" href="/Community/forums/">view all&hellip;</a> <a class="rss" href="/Community/forums/aggregaterss.aspx">
                                                                                       forums rss</a>
                    <!-- / .cont -->
                </div>
            </div>
            <!-- / .box -->
        </div>
    </div>
</div>