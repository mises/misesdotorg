<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Theme_LiteratureList" Codebehind="LiteratureList.ascx.cs" %>
<%@ Import Namespace="Mises.Data" %>
<div class="outerbox">
    <div class="box literature">
        <h2 style="margin-bottom: 7px;"><i>Literature</i></h2>
        <a class="rss" href="/literature.xml">rss</a>
        <div class="cont">
            <ul class="literature">
                <asp:Repeater ID="dlLiterature" runat="server" DataSourceID="sqlNewLiterature">
                    <ItemTemplate>
                        <li>
                            <asp:HyperLink runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Title")%>'
                                           NavigateUrl='<%#Eval("URL").ToString().Contains("mises.org")
                                 ? string.Format("/document/{0}/{1}", Eval("DocumentId"),
                                                 DataFormat.GenerateSlug(Eval("Title")))
                                 : Eval("URL")%>'>
                            </asp:HyperLink>
                            <em>by
                                <%#Eval("Author")%></em></li>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="sqlNewLiterature" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                                   SelectCommand="DocumentGetNewLiterature" SelectCommandType="StoredProcedure"
                                   EnableCaching="True" CacheDuration="160"></asp:SqlDataSource>
            </ul>
            <a class="more" href="/Literature">view all&hellip;</a>
            <!-- / .cont -->
        </div>
        <!-- / .box -->
    </div>
</div>