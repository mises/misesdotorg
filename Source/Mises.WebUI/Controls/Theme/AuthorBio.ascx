<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Theme_AuthorBio" Codebehind="AuthorBio.ascx.cs" %>
<%@ OutputCache VaryByParam="*" Shared="True" Duration="160" %>
<%@ Import Namespace="Mises.Data" %>
<div class="box aboutauthor">
    <h2>
        About the Author</h2>
    <asp:Repeater runat="server" ID="rptAuthorBio">
        <ItemTemplate>
            <div class="cont">
                <asp:Image ID="imgAuthorThumbs" runat="server" Height="150" ImageUrl='<%#DataBinder.Eval(Container.DataItem, "Photo")%>'
                           AlternateText='<%#"Photo of " + Eval("Author")%>' />
                <div class="name">
                    <asp:Literal ID="litAuthorName" runat="server" Text='<%#Eval("Author")%>'>
                    
				
                    </asp:Literal>
                </div>
				
                <asp:HyperLink ID="lnkViewProfile" runat="server" NavigateUrl='<%#"../../daily/author/" + Eval("AuthorId") + "/" +
                             DataFormat.GenerateSlug(Eval("Author"))%>'>View Author Archive</asp:HyperLink>
                <br />
                <p>
				
                    <asp:Literal ID="litAuthorBioText" runat="server" Text='<%#Eval("BioText")%>'></asp:Literal>
				
                </p>
				
                <!-- / .cont -->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- / .box -->
</div>