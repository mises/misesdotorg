﻿#region

using System;
using System.Web.UI;
using Mises.Data;
using Mises.Domain.Services;

#endregion

public partial class Controls_Theme_Footer : UserControl
{
    private QuoteGetRandomResult quote;

    public QuoteGetRandomResult Quote
    {
        get { return quote ?? (quote = quotes.RandomQuote()); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}