﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Theme_Footer"
    CodeBehind="Footer.ascx.cs" EnableViewState="False" %>
<%@ OutputCache Duration="90" VaryByParam="none" Shared="true" %>
<%@ Import Namespace="Mises.Data" %>
<asp:ObjectDataSource runat="server" SelectMethod="GetNewAudio" TypeName="Mises.Domain.Media.MediaCatalog"
    ID="objNewAudio" CacheDuration="360" EnableCaching="True"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" SelectMethod="GetNewVideo" TypeName="Mises.Domain.Media.MediaCatalog"
    ID="objNewVideo" CacheDuration="360" EnableCaching="True"></asp:ObjectDataSource>
<div id="footer">
    <div class="container ludwig">
        <div class="bf_column">
            <a href="/literature.xml" class="bf_rss" title="Subscribe to the RSS Feed"><b>RSS Feed
                for Literature</b></a><h2 class="hdrflit">
                    <a href="/Literature"><b>Literature</b></a></h2>
            <div class="clearer">
            </div>
            <ul>
                <asp:Repeater ID="dlLiterature" runat="server" DataSourceID="sqlNewLiterature">
                    <ItemTemplate>
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Title")%>'
                                NavigateUrl='<%#Eval("URL").ToString().Contains("mises.org")
                                 ? string.Format("/document/{0}/{1}", Eval("DocumentId"),
                                                 DataFormat.GenerateSlug(Eval("Title")))
                                 : Eval("URL")%>'>
                            </asp:HyperLink>
                            <em>by
                                <%#Eval("Author")%></em></li>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:SqlDataSource ID="sqlNewLiterature" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                    SelectCommand="DocumentGetNewLiterature" SelectCommandType="StoredProcedure"
                    EnableCaching="True" CacheDuration="160"></asp:SqlDataSource>
            </ul>
        </div>
        <!-- bf_column -->
        <div class="bf_column">
            <a href="/media.xml" class="bf_rss" title="Subscribe to the RSS Feed"><b>RSS Feed for
                Audio</b></a><a href="/media"><h2 class="hdrfaudio">
                    <b>Audio</b></h2>
                </a>
            <div class="clearer">
            </div>
            <ul>
                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="objNewAudio">
                    <ItemTemplate>
                        <li><a href="<%#String.Format("/media/{0}/{1}", Eval("FileId"), DataFormat.GenerateSlug(Eval("Title")))%>">
                            <%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%></a> <em>by <a href="<%#String.Format("/media/authors/{0}/{1}", Eval("Author1"),
                                                  DataFormat.GenerateSlug(Eval("Author")))%>">
                                <%#Eval("Author")%></a></em> </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- bf_column -->
        <div class="bf_column">
            <a href="/media.xml" class="bf_rss" title="Subscribe to the RSS Feed"><b>RSS Feed for
                Video</b></a><a href="/media"><h2 class="hdrfvideo">
                    <b>Video</b></h2>
                </a>
            <div class="clearer">
            </div>
            <ul>
                <asp:Repeater ID="rptVdieo" runat="server" DataSourceID="objNewVideo">
                    <ItemTemplate>
                        <li><a href="<%#String.Format("/media/{0}/{1}", Eval("FileId"), DataFormat.GenerateSlug(Eval("Title")))%>">
                            <%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%></a> <em>by <a href="<%#String.Format("/media/authors/{0}/{1}", Eval("Author1"),
                                                  DataFormat.GenerateSlug(Eval("Author")))%>">
                                <%#Eval("Author")%></a></em> </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- bf_column -->
        <div class="clearer">
        </div>
        <% if (Quote != null)
           { %>
        <div id="lvmi_quote">
            <p>
                "<%=Quote.Quote%>"</p>
            <p class="lq_tag">
                &#151;
                <%=Quote.Author%>, in <a href="http://mises.freecapitalists.org//quotes.aspx?action=source&Source=<%=Quote.Subject%>">
                    <%=Quote.Source%></a></p>
        </div>
        <% } %>
        <!-- lvmi_quote -->
        <div id="bf_address">
            <img src="//mises.freecapitalists.org/images/Theme/images/bf_shield.png" alt="LvMI" class="fl" style="margin-right: 5px;" />
            <img src="//mises.freecapitalists.org/images/Theme/images/bf_lvmi.png" alt="Ludwig von Mises Institute" />
            <p>
                518 West Magnolia Avenue &#149; Auburn, Alabama 36832-4501 &#149; Phone: 334.321.2100
                &#149; Fax: 334.321.2119<br />
                <a href="/contact.aspx">Contact Us</a>
                <a title="Comments and Questions about the Mises Institute" href="mailto:contact@freecapitalists.org?subject=Comment+or+Question+about+the Mises+Institute">
                    contact@freecapitalists.org</a> &#149; <a title="Comments about the Mises.org web site" href="mailto:webmaster@freecapitalists.org?subject=Comments+about+the+Mises.org+web+site">
                        webmaster@freecapitalists.org</a> &#149; <a href="#" onclick=" window.open('/MyMises/EditFavorite.aspx?url=' + escape(self.location) + '&amp;title=' + escape(document.title), 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); ">
                            Save to MyMises</a> &#149; <a href="/sitemap.aspx" rel="directory">Site Map</a>
                &#149; <a href="http://groups.google.com/group/misesdev">Open Source</a> &#149;
                <a href="http://mises.freecapitalists.org//mobile/">Mobile</a></p>
            <p>
                <a href="http://creativecommons.org/licenses/by/3.0/">
                    <img alt="creativecommons.org" src="//mises.freecapitalists.org/images/cc-a.png" /></a></p>
        </div>
        <!-- bf_address -->
    </div>
    <!-- container / watermark -->
</div>
