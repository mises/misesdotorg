#region

using System;
using System.Web.UI;

#endregion

partial class Controls_Theme_MiniSearch : UserControl
{
    public Controls_Theme_MiniSearch()
    {
        Load += Page_Load;
    }

    public string searchSite
    {
        get
        {
            string _searchSite = "default_collection";

            string path = Request.FilePath;

            if (path.IndexOf("/articles.aspx") >= 0)
                _searchSite = "Mises_Daily";
            else if (path.IndexOf("/literature.aspx") >= 0)
                _searchSite = "Literature";
            else if (path.IndexOf("/literature") >= 0)
                _searchSite = "Literature";
            else if (path.IndexOf("/media") >= 0)
                _searchSite = "Media";
            else if (path.IndexOf("http://store.mises.org") >= 0)
                _searchSite = "Store";
            else if (path.IndexOf("/blog/") >= 0)
                _searchSite = "Blog";
            else if (path.IndexOf("/books/") >= 0)
                _searchSite = "Stacks";

            return _searchSite;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //txtSearchSite.Value = searchSite;
    }
}