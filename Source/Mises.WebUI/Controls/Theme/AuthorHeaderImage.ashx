<%@ WebHandler Language="C#" Class="AuthorHeaderImage" %>

#region

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using Mises.Domain.Utility;

#endregion

public class AuthorHeaderImage : IHttpHandler
{
    private Graphics backgroundGraphics;
    private Bitmap backgroundImage;

    #region IHttpHandler Members

    public void ProcessRequest(HttpContext context)
    {
        GenerateHeaderImage(context);
    }

    public bool IsReusable
    {
        get { return false; }
    }

    #endregion

    private void GenerateHeaderImage(HttpContext context)
    {
        context.Response.ContentType = "image/jpeg";

        context.Response.Cache.SetExpires(DateTime.Now.AddYears(1));
        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.Cache.VaryByParams["url"] = true;
        context.Response.Cache.VaryByParams["name"] = true;

        string url = context.Request.QueryString["url"];
        string authorname = context.Request.QueryString["name"];

        if (string.IsNullOrEmpty(authorname))
        {
            return;
        }

        backgroundImage =
            (Bitmap)
            Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + @"\images\Theme\images\LiteratureHeader.jpg");
        backgroundGraphics = Graphics.FromImage(backgroundImage);


        // Gradient
        //Dim rect As Rectangle = New Rectangle(0, 0, bkGroundImg.PhysicalDimension.Width, CType(bkGroundImg.PhysicalDimension.Height + 1, Integer))
        //'Dim brush As Brush = New LinearGradientBrush(rect, Color.DarkGoldenrod, Color.Yellow, 90)
        //Dim p As New Pen(Color.Red, 10)
        //bkImgGraphics.DrawRectangle(p, rect)
        //'      bkImgGraphics.FillRectangle(brush, rect)

        // p = new Pen(Color.Green, 10);
        // bkImgGraphics.DrawLine(p, 0, 35, (int)userImg.PhysicalDimension.Width,35);

        //makeTransparent(CType(Double.Parse(trans), Single))


        if (authorname.Length > 0)
        {
            var font = new Font("Perpetua Titling MT", 24F, FontStyle.Regular);
            if (font.FontFamily.Name != "Perpetua Titling MT")
            {
                font = new Font("Times New Roman", 24F, FontStyle.Regular);
            }

            backgroundGraphics.DrawString(authorname.ToUpper(), font, new SolidBrush(Color.FromArgb(100, 0, 0, 0)), 10,
                                          5);
        }

        if (!string.IsNullOrEmpty(url))
        {
            url = url.Replace("images2", "images").Replace("images3", "images").Replace("images4", "images");
            
            using (Bitmap authorImage = Imaging.GetImageFromURL(url))
            {
                using (var smallAuthorImage = (Bitmap) Imaging.resizeImage(authorImage, new Size(130, 100)))
                {
                    var rect = new Rectangle(499, 0, (int) (smallAuthorImage.PhysicalDimension.Width + 2),
                                             Convert.ToInt32(smallAuthorImage.PhysicalDimension.Height + 2));
                    var p = new Pen(Color.Black, 2F);
                    backgroundGraphics.DrawRectangle(p, rect);
                    backgroundGraphics.DrawImage(smallAuthorImage, 500, 0);
                }
            }
        }
        backgroundImage.Save(context.Response.OutputStream, ImageFormat.Jpeg);
    }
}