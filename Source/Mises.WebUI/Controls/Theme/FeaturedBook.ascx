<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Theme_FeaturedBook" ClientIDMode="Static" Codebehind="FeaturedBook.ascx.cs" %>
<script type="text/javascript">

    $(document).ready(function() {
        GetRandomProduct();
        setInterval(GetRandomProduct, 300000);

    });
</script>
<div class="outerbox" id="featuredProduct">
</div>
<script id="featuredProductTemplate" type="text/x-jquery-tmpl">   

    <div class="box featured">
    <h2>Featured in our Store</h2>
    <div class="clearer" style="height: 5px;"></div>
    <div class="item">
         
        <div class="bord-se">
            <div class="bord-ne">
                <div class="bord-s"> 
                    <a href="http://store.mises.org/Product.aspx?ProductId=${ProductId}&utm_source=Homepage&utm_medium=FeaturedProd&utm_term=Widget&utm_campaign=Featured_Widget" id="A1"><img class="FeaturedProduct" src="${ThumbnailUrl}" alt="featured" style="border-width: 0px;" /></a>
                </div><!-- bord-s -->
            </div><!-- bord-ne -->
        </div><!-- bord-se -->
            
    </div><!-- item -->
    <h4><a href="http://store.mises.org/Product.aspx?ProductId=${ProductId}&amp;utm_source=Homepage&amp;utm_medium=FeaturedProd&amp;utm_term=Widget&amp;utm_campaign=Featured_Widget">${Name}</a></h4>
    <p><span>Author: </span><a href="/store/search.aspx?m=${AuthorId}">${Author}</a><br /> 
        <span class="Price">$${Price}</span><br />
        ${Summary}				
        <a class="more" href="/store/New-Products-C52.aspx">view all&hellip;</a> 
    </p>
    <!-- / .box --> 
        
    <div class="clearer"></div>

</script>