<%@ Control Language="C#" AutoEventWireup="false" Inherits="Controls_Theme_Sidebar" Codebehind="Sidebar.ascx.cs" %>
<%@ Import Namespace="Mises.Domain.Services" %>
<%--<%@ OutputCache Duration="180" VaryByParam="*" Shared="false" VaryByControl="plcCustomControl.Controls"  %>--%>
<%@ Register Src="MiniSearch.ascx" TagName="MiniSearch" TagPrefix="uc2" %>
<%@ Register Src="FeaturedBook.ascx" TagName="FeaturedBook" TagPrefix="uc3" %>
<%@ Register Src="EventsList.ascx" TagName="EventsList" TagPrefix="uc1" %>
<%@ Register Src="LiteratureList.ascx" TagName="LiteratureList" TagPrefix="uc4" %>
<%--<%@ Register Src="CommunityTabs.ascx" TagName="CommunityTabs" TagPrefix="uc5" %>--%>
<%@ Register Src="~/Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>

<uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="GlobalSidebar" />

<uc2:MiniSearch ID="MiniSearch1" runat="server" />
<asp:PlaceHolder ID="plcCustomControl" runat="server"></asp:PlaceHolder>
<uc3:FeaturedBook ID="FeaturedBook1" runat="server" />
<%--<uc5:CommunityTabs ID="CommunityTabs1" runat="server" />--%>
<uc4:LiteratureList ID="LiteratureList1" runat="server" />
<div class="outerbox">
    <uc1:EventsList ID="EventsList1" runat="server" />
</div>
<div class="outerbox" id="litQuote" runat="server">
    <div class="widget-header">
        <h2>
            <asp:HyperLink runat="server" NavigateUrl="~/quotes.aspx">The Quotable Mises</asp:HyperLink></h2>
    </div>
    <div class="cont quote">        
        <%= quotes.GetRandomQuote(null) %>
    </div>
</div>