#region

using System;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class Controls_Theme_Sidebar : UserControl
{
    public string VirtualPathToControl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Context.Items.Contains("ShowSidebar"))
        {
            if ((bool)Context.Items["ShowSidebar"] == false)
                Visible = false;
        }

        if (!string.IsNullOrEmpty(VirtualPathToControl))
        {
            LoadUserControl(VirtualPathToControl);
        }

        string URL = Request.Url.ToString();

        if (URL.Contains("404")) return;

        if (URL.Contains("event"))
        {
            //if (EventsList1 != null)
            //{
            //    EventsList1.Visible = false;
            //}
            LoadUserControl("../../Controls/Sidebars/Events.ascx");
        }
        else if (URL.Contains("default.aspx"))
        {
            LiteratureList1.Visible = false;
        }
        else if (Request.IsSecureConnection && URL.Contains("donate") || URL.Contains("form"))
        // Donate and Registration pages
        {
            LoadUserControl("../../Controls/Sidebars/Donate.ascx");
            // LoadUserControl("../../Controls/Sidebars/About.ascx");

            if (EventsList1 != null)
            {
                EventsList1.Visible = false;
            }
            LiteratureList1.Visible = false;
            FeaturedBook1.Visible = false;
            MiniSearch1.Visible = false;
            litQuote.Visible = false;
            //if (CommunityTabs1 != null)
            //{
            //    CommunityTabs1.Visible = false;
            //}
        }
        else if (URL.Contains("periodical") || URL.Contains("detail") || URL.Contains("publications"))
        {
            LoadUserControl("../../Controls/Sidebars/Publications.ascx");
        }
        else if (URL.Contains("about"))
        {
            LoadUserControl("../../Controls/Sidebars/About.ascx");
            LoadUserControl("../../Controls/Sidebars/Literature.ascx");
        }
        else if (URL.Contains("literature") || URL.Contains("resources") || URL.Contains("/web/"))
        {
            LoadUserControl("../../Controls/Sidebars/Literature.ascx");
        }
        else if (URL.Contains("media"))
        {
            LoadUserControl("../../Controls/Sidebars/Media.ascx");
            LiteratureList1.Visible = false;
            //FeaturedBook1.Visible = false;
            //MiniSearch1.Visible = false;
            EventsList1.Visible = false;
            litQuote.Visible = false;
            //if (CommunityTabs1 != null)
            //{
            //    CommunityTabs1.Visible = false;
            //}
        }
        else if (URL.Contains("article"))
        {
            LoadUserControl("../../Controls/Sidebars/DailyArticle.ascx");
        }
    }

    private void LoadUserControl(string path)
    {
        Control control = LoadControl(path);
        plcCustomControl.Controls.Add(control);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }
}