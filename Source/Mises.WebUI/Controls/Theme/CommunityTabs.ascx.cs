#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Web.UI;
using Mises.Data;
using Mises.Domain.Utility;

#endregion

namespace Controls.Theme
{
    public partial class Controls_Theme_CommunityTabs : UserControl
    {
        private const string blogCacheKey = "blogKey2";
        private const string communityCacheKey = "communityKey";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    DataSet ds;

                    if (Cache.Get(communityCacheKey) == null)
                    {
                        ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                      "dbo.CommunityGetFeed");
                        Cache.Add(communityCacheKey, ds, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero,
                                  CacheItemPriority.Normal, null);
                    }
                    else
                    {
                        ds = Cache.Get(communityCacheKey) as DataSet;
                    }

                    if (ds != null)
                    {
                        dlAustrianNetworkForums.DataSource = ds.Tables[0];
                        dlAustrianNetworkForums.DataBind();


                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            if (dr.RowState == DataRowState.Deleted) continue;
                            DateTime postDate = DateTime.MinValue;
                            DateTime.TryParse(Convert.ToString(dr["PubDate"]), out postDate);
                            string applicationKey = dr["ApplicationKey"].ToString();
                            string baseURL = Request.Url.Scheme + "://" + Request.Url.Authority;

                            try
                            {
                                dr["link"] = string.Format(baseURL + "/Community/blogs/{0}/archive/{1}/{2}/{3}.aspx",
                                                       applicationKey,
                                                       postDate.Year, postDate.Month.ToString("00"),
                                                       postDate.Day.ToString("00"));
                            }
                            catch
                            {

                                dr["link"] = string.Format(baseURL + "/Community/blogs/{0}/",applicationKey);
                            }
                            
                        }

                        try
                        {
                            dlAustrianNetworkBlogs.DataSource = ds.Tables[1];
                            dlAustrianNetworkBlogs.DataBind();
                        }
                        catch
                        {
                            //ExceptionLogging.LogException(Context, ex);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    ExceptionLogging.LogException(Context, ex);
                }
               
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(Context, ex);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Load += Page_Load;
        }
    }
}