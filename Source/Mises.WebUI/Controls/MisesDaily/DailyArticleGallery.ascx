<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_MisesDaily_DailyArticleGallery" Codebehind="DailyArticleGallery.ascx.cs" %>
<%@ OutputCache Duration="180" VaryByParam="*" %>
<%@ Import Namespace="Mises.Data" %>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                   SelectCommand="DailyArticlesGetImageGallery" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="AuthorId" QueryStringField="AuthorId"
                                  Type="Int32" />
        <asp:QueryStringParameter DefaultValue="0" Name="Limit" QueryStringField="Limit"
                                  Type="Int32" />
        <%--<asp:QueryStringParameter Name="SearchQuery" QueryStringField="q" Type="String" DefaultValue="" ConvertEmptyStringToNull=true />--%>
    </SelectParameters>
</asp:SqlDataSource>
<asp:ListView ID="DailyArticleGallery" runat="server" GroupItemCount="5" DataSourceID="SqlDataSource1"
              DataKeyNames="Month">
    <ItemTemplate>
        <td>
            <div class="Article" id="<%#Eval("ArticleId")%>">
                <a href="<%#Server.HtmlEncode(Convert.ToString(Eval("NavigateUrl"))) + "/" +
                             DataFormat.GenerateSlug(Eval("Title"))%> "
                   title="&quot;<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>&quot; by <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>">
                    <img src="<%#Server.HtmlEncode(Convert.ToString(Eval("ImageURL")))%>"
                         alt="" title="&quot;<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>&quot; by <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>" />
                </a><a class="ArticleTitle" href="<%#Server.HtmlEncode(Convert.ToString(Eval("NavigateUrl"))) + "/" +
                                    DataFormat.GenerateSlug(Eval("Title"))%>"
                       title="&quot;<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>&quot; by <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>">
                        <%#Eval("Title")%></a>
                <br />
                <span class="ArticleDate">
                    <%#DateTime.Parse(Eval("DatePosted").ToString()).ToString("MMM dd yyyy")%></span>
                by <span class="ArticleAuthor"><a href="/daily/author/<%#Eval("AuthorId")%>/<%#DataFormat.GenerateSlug(Eval("Author"))%>"
                                                  title="Mises Daily Archive for <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>">
                                                   <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>
                                               </a></span>
            </div>
        </td>
    </ItemTemplate>
    <GroupTemplate>
        <div id="itemPlaceholderContainer" runat="server" style="">
            <span id="itemPlaceholder" runat="server" />
        </div>
    </GroupTemplate>
    <GroupSeparatorTemplate>
    </tr>
    </GroupSeparatorTemplate>
    <EmptyDataTemplate>
        <span>No data was returned.</span>
    </EmptyDataTemplate>
    <LayoutTemplate>
        <div style="" class="Pager">
            <asp:DataPager ID="DataPager2" runat="server" PageSize="50">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                    <asp:NumericPagerField ButtonCount="20" />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
        </div>
        <table id="DailyArticleGallery" cellpadding="0" cellspacing="0">
            <div id="groupPlaceholderContainer" runat="server" style="">
                <span id="groupPlaceholder" runat="server" />
            </div>
        </table>
        <div style="" class="Pager">
            <asp:DataPager ID="DataPager3" runat="server" PageSize="100">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                    <asp:NumericPagerField ButtonCount="20" />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
        </div>
    </LayoutTemplate>
</asp:ListView>