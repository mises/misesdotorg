﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

public partial class Controls_MisesDaily_DailyArticleList : UserControl
{
    //private int _authorId;
    private string _month;

    public GridView ArchivesGrid
    {
        get { return gvArchives; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }


    protected void gvArchives_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        // Add Monthly Breaks
        if ((e.Row.RowType != DataControlRowType.DataRow) || Request.QueryString["action"] != "")
            return;
        if (!(gvArchives.SortExpression == "" || gvArchives.SortExpression.Contains("DatePosted")))
        {
            return;
        }

        string currentMonth = gvArchives.DataKeys[e.Row.RowIndex].Values[0].ToString();
        if (_month == currentMonth) return;
        _month = currentMonth;
        var lit = new Literal();

        var link = (HyperLink) (e.Row.Cells[1].FindControl("HyperLink1"));

        string archives = "</td></tr>" + Environment.NewLine +
                          "<tr class=\"aspGridView_RowStyle\"><th colspan=\"5\">" + Environment.NewLine;
        archives += "<h4 >" + currentMonth + " " + Convert.ToDateTime(e.Row.Cells[2].Text).Year + "</h4>" +
                    Environment.NewLine;
        archives += "</th></tr>" + Environment.NewLine + "<tr class=\"aspGridView_RowStyle\"><td>" +
                    Environment.NewLine;
        lit.Text = archives;
        e.Row.Cells[0].Controls.Add(lit);
        e.Row.Cells[0].Controls.Add(link);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Load += Page_Load;
        gvArchives.RowDataBound += gvArchives_RowDataBound;
    }
}