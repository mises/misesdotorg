﻿<%@ Control Language="C#" AutoEventWireup="true"
            Inherits="Controls_MisesDaily_DailyArticleList" Codebehind="DailyArticleList.ascx.cs" %>
<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<cc1:GridView ID="gvArchives" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
              DataKeyNames="Month, AuthorName,CoAuthorId, CoAuthorName, Photo , CoPhoto" AllowPaging="True"
              DataSourceID="sqlDailyArticles" PageSize="80" PagerSettings-PageButtonCount="20"
              EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
              SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
    <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
    <Columns>
        <asp:TemplateField HeaderText="Title" SortExpression="Title">
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#Convert.ToString(Eval("Title"))%>'
                               NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.ArticleId", "~/daily/{0}")%>'> </asp:HyperLink>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="AuthorId" DataNavigateUrlFormatString="/daily/author/{0}"
                            DataTextField="AuthorName" HeaderText="Author" SortExpression="AuthorName">
            <ItemStyle HorizontalAlign="Left"></ItemStyle>
        </asp:HyperLinkField>
        <asp:BoundField DataField="DatePosted" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"
                        SortExpression="DatePosted">
            <ItemStyle HorizontalAlign="Left"></ItemStyle>
        </asp:BoundField>
        <asp:TemplateField HeaderText="Feed" SortExpression="AuthorId">
            <ItemTemplate>
                <asp:HyperLink runat="server" ImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/rss.gif" ID="lnk"
                               NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.AuthorId", "~/Feeds/articles.ashx?AuthorId={0}")%>'
                               ToolTip="Author RSS link" Text="Author RSS link"></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</cc1:GridView>
<asp:SqlDataSource ID="sqlDailyArticles" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                   SelectCommand="DailyArticlesGetArchive" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False"
                   CacheDuration="160" EnableCaching="True">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="AuthorId" QueryStringField="AuthorId"
                                  Type="Int32" />
        <asp:QueryStringParameter Name="SearchQuery" QueryStringField="q" Type="String" DefaultValue="" />
    </SelectParameters>
</asp:SqlDataSource>