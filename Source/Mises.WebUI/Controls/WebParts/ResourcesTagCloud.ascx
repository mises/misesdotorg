<%@ Control Language="C#" AutoEventWireup="false" CodeFile="ResourcesTagCloud.ascx.cs"
	Inherits="Controls_ResourcesTagCloud" EnableViewState="false" %>
<%@ Register Namespace="VRK.Controls" TagPrefix="vrk" Assembly="VRK.Controls" %>
<h3>
	Literature Subjects</h3>
<div id="Div1" style="width: 200; text-align: center;">
</div>
<asp:ObjectDataSource ID="sourceGetDocumentSubjects" runat="server" SelectMethod="GetDocumentSubjects"
	TypeName="Mises.Domain.Tags.CloudSource"></asp:ObjectDataSource>
<div id="CommonTagCloud">
	<vrk:Cloud ID="Cloud3" runat="server" CssClass="tagcloud" DataHrefField="URL" DataSourceID="sourceGetDocumentSubjects"
		DataTextField="Name" DataTitleField="Weight" DataTitleFormatString="{0} documents"
		DataWeightField="Weight">
	</vrk:Cloud>
</div>