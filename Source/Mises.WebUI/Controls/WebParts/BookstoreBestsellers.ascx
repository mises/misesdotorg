<%@ Control Language="C#" AutoEventWireup="false" CodeFile="BookstoreBestsellers.ascx.cs" Inherits="Controls_BookstoreBestsellers" %>
<%--<%@ OutputCache Duration="1200" VaryByParam="none" Shared="true" %>--%>
<div class="CommonSidebarArea">
	<div class="CommonSidebarRoundTop">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
	<div class="CommonSidebarInnerArea">
		<h4 class="CommonSidebarHeader">
			<a href="/store/newprods.aspx">Bestsellers</a></h4>
		<div class="CommonSidebarContent">
			<div class="CommonListArea">


<asp:GridView ID="gvBooksList" runat="server" AutoGenerateColumns="False" CellPadding="4"
	ForeColor="#333333" GridLines="None" UseAccessibleHeader="False" ShowFooter="false" ShowHeader="false">    
	<Columns>
		<asp:TemplateField>           
			<ItemTemplate>
				<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("DisplayPage", "/store/{0}")%>'
					Text='<%#Eval("Name")%>'></asp:HyperLink>                
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
	<RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
	<EditRowStyle BackColor="#999999" />
	<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
	<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />    
	<AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>

 </div>
		</div>
	</div>
	<div class="CommonSidebarRoundBottom">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
</div>