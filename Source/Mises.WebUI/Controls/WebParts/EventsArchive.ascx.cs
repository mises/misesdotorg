#region

using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

#endregion

partial class Controls_EventsArchive : UserControl, IWebPart
{
    #region WebParts

    public string CatalogIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Description
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Subtitle
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
    }

    public string Title
    {
        get { return "Upcoming Events"; }
        set { }
    }

    public string TitleIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string TitleUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    #endregion

    public void Bind(DataTable dt)
    {
        gvUpcomingEvents.DataSource = dt;
        gvUpcomingEvents.DataBind();
    }

    //Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
    //    If Page.IsPostBack Then Return
    //    Me.gvUpcomingEvents.DataSource = SqlHelper.ExecuteReader(MisesWeb.BusinessBase.ConnectionString, CommandType.Text, "select EventId,Title,EventDate,EndDate,Location,Display,CreateDate from calendar where display = 1 and EndDate > getdate() -1 ORDER BY EndDate ASC")
    //    Me.gvUpcomingEvents.DataBind()

    //End Sub
}