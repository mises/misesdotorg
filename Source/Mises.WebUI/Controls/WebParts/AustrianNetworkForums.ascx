<%@ Control Language="C#" AutoEventWireup="false" CodeFile="AustrianNetworkForums.ascx.cs"
	Inherits="WebParts_NetworkForumPosts" %>

<div class="CommonSidebarArea">
	<div class="CommonSidebarRoundTop">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
	<div class="CommonSidebarInnerArea">
		<h4 class="CommonSidebarHeader">
			<a href="/Community/forums/">Forum Posts</a> <a href="/Community/forums/aggregaterss.aspx">
				<img alt="RSS Feed" style="border: none" src="/Theme/images/buttons/rss.gif" /></a></h4>
		<div class="CommonSidebarContent">
			<div class="CommonListArea">
				<asp:DataList ID="dlAustrianNetworkForums" runat="server" CellPadding="4"
					ForeColor="#333333" GridLines="None" UseAccessibleHeader="false" ShowFooter="false"
					ShowHeader="false">
					<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
					<SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
					<ItemTemplate>

						<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("link")%>' Text='<%#Eval("title")%>'></asp:HyperLink>                      <%--  by <%#Eval("dc.creator")%>--%>
					</ItemTemplate>
					<AlternatingItemStyle BackColor="White" ForeColor="#284775" />
					<ItemStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Justify" />
					<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
				</asp:DataList>
			</div>
		</div>
	</div>
	<div class="CommonSidebarRoundBottom">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
</div>