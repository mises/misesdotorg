<%@ Control Language="C#" AutoEventWireup="false" CodeFile="EventsArchive.ascx.cs" Inherits="Controls_EventsArchive" %>
<%--<%@OutputCache Duration="120" VaryByParam="none" Shared="true" %>--%>
<div class="CommonSidebarArea">
	<div class="CommonSidebarRoundTop">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
	<div class="CommonSidebarInnerArea">
		<h4 class="CommonSidebarHeader">
			<a href="events.aspx">Upcoming Events</a> <a href="/events.xml"><img alt="RSS Feed" style="border:none" src="/Theme/images/buttons/rss.gif" /></a></h4>
		<div class="CommonSidebarContent">
			<div class="CommonListArea">


<asp:GridView ID="gvUpcomingEvents" runat="server" AutoGenerateColumns="False" CellPadding="4"
	ForeColor="#333333" GridLines="None" UseAccessibleHeader="False" ShowFooter="false" ShowHeader="false">    
	<Columns>
		<asp:TemplateField>           
			<ItemTemplate>
				<em>
					<%#DataBinder.Eval(Container, "DataItem.EventDate")%>
				</em>-  <a href="/events/<%#DataBinder.Eval(Container, "DataItem.EventId")%>">
					<%#DataBinder.Eval(Container, "DataItem.Title")%>
				</a> in 
				<%#DataBinder.Eval(Container, "DataItem.Location")%>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
	<RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Left" />
	<EditRowStyle BackColor="#999999" />
	<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
	<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />    
	<AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>

 </div>
		</div>
	</div>
	<div class="CommonSidebarRoundBottom">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
</div>