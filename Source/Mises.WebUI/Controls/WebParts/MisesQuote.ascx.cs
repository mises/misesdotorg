#region

using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using Mises.Data;
using MvcMiniProfiler.Data;

#endregion

public partial class Controls_MisesQuote : UserControl, IWebPart
{
    public Controls_MisesQuote()
    {
        Load += Page_Load;
    }

    #region IWebPart Members

    public string CatalogIconImageUrl { get; set; }
    public string Description { get; set; }
    public string Subtitle { get; set; }

    public string Title
    {
        get { return "Random Mises Quote"; }
        set { }
    }

    public string TitleIconImageUrl { get; set; }
    public string TitleUrl { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        // Random Quote
        var db = new MisesDBDataContext(ProfiledDbConnection.Get(new System.Data.SqlClient.SqlConnection(BusinessBase.ConnectionString)));
        QuoteGetRandomResult quote = db.QuoteGetRandom().First();
        litQuote.Text = "<a target=\"_top\" href=\"/quotes.aspx?action=subject&subject=" +
                        quote.Subject + "\"> Ludwig von Mises:</a> " + "\"" + quote.Quote +
                        "\" - <a target=\"_top\" href=\"/quotes.aspx?action=source&Source=" +
                        quote.Source + "\">" + quote.Source + "</a>";
    }
}