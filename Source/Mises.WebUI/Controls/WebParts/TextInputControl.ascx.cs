﻿#region

using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

#endregion

partial class TextInputControl : UserControl, IWebPart
{
    private string _subTitle = "[0]";

    public TextInputControl()
    {
        Title = "Super cool user control ";
        Description = string.Empty;
        CatalogIconImageUrl = string.Empty;
    }

    #region IWebPart Members

    public string CatalogIconImageUrl { get; set; }

    public string Description { get; set; }

    public string Subtitle
    {
        get { return string.Empty; }
    }

    public string Title { get; set; }

    public string TitleIconImageUrl
    {
        get { return string.Empty; }
        set { }
    }

    public string TitleUrl
    {
        get { return string.Empty; }
        set { }
    }

    #endregion
}