#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using RssToolkit.Web.WebControls;

#endregion

partial class CustomControls_MisesBlogArchive : UserControl, IWebPart
{
    public CustomControls_MisesBlogArchive()
    {
        Load += Page_Load;
    }

    #region IWebPart Members

    public string CatalogIconImageUrl { get; set; }
    public string Description { get; set; }
    public string Subtitle { get; set; }

    public string Title
    {
        get { return "Mises Blog Posts"; }
        set { }
    }

    public string TitleIconImageUrl { get; set; }
    public string TitleUrl { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }
        try
        {
            //TODO: FIX to relative URL
            var source = new RssDataSource
                             {
                                 Url = "http://blog.mises.org/index.xml"
                             };
            dlMisesBlog.DataSource = source;
            dlMisesBlog.DataBind();
        }
        catch
        {
        }
    }
}