#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using RssToolkit.Web.WebControls;

#endregion

partial class WebParts_NetworkForumPosts : UserControl, IWebPart
{
    #region IWebPart Members

    public string CatalogIconImageUrl { get; set; }
    public string Description { get; set; }
    public string Subtitle { get; set; }
    public string TitleIconImageUrl { get; set; }
    public string TitleUrl { get; set; }

    public string Title
    {
        get { return "Austrian Network Forum Posts"; }
        set { }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                return;
            }
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;
            var source = new RssDataSource
                             {
                                 Url = baseUrl + "/Community/forums/aggregaterss.aspx"
                             };
            dlAustrianNetworkForums.DataSource = source;
            dlAustrianNetworkForums.DataBind();
        }
        catch
        {
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Load += Page_Load;
    }
}