<%@ Control Language="C#" AutoEventWireup="false" CodeFile="NewResources.ascx.cs" Inherits="CustomControls_NewResources" %>
<div class="CommonSidebarArea">
	<div class="CommonSidebarRoundTop">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
	<div class="CommonSidebarInnerArea">
		<h4 class="CommonSidebarHeader">
			<a href="resources.aspx">New Literature</a> <a href="/guide.xml"><img alt="RSS Feed" style="border:none" src="/Theme/images/buttons/rss.gif" /></a></h4>
		<div class="CommonSidebarContent">
			<div class="CommonListArea">
<asp:GridView ID="gvRecentMedia" runat="server" AutoGenerateColumns="False" CellPadding="4"
	ForeColor="#333333" GridLines="None" UseAccessibleHeader="False" ShowFooter="false" ShowHeader="false">        
	<Columns>
		<asp:TemplateField>            
			<ItemTemplate>
				<table align="left" width="100%">
					<tr align="left" valign="top">
						<td width="10" style="padding: 4px 4px 8px 0;">
							<asp:Image ID="Image1" runat="server" AlternateText='<%#Eval("MediaIconPath")%>'
								ImageUrl='<%#Eval("MediaIconPath","/{0}")%>' /></td>
						<td style="padding: 4px 0 8px 0;">
							<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("URL")%>' Text='<%#Eval("Title")%>'></asp:HyperLink>
							&middot;
							<asp:Label ID="Label1" runat="server" Text='<%#Eval("Author")%>'></asp:Label>
						</td>
					</tr>
				</table>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
	<RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
	<EditRowStyle BackColor="#999999" />
	<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
	<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
	<HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
	<AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>

 </div>
		</div>
	</div>
	<div class="CommonSidebarRoundBottom">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
</div>