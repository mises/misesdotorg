#region

using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

#endregion

partial class CustomControls_MediaArchive : UserControl, IWebPart
{
    #region WebParts

    public string CatalogIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Description
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Subtitle
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
    }

    public string Title
    {
        get { return "New Media"; }
        set { }
    }

    public string TitleIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string TitleUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    #endregion

    public void Bind(DataTable dt)
    {
        gvStudyGuide.DataSource = dt;
        gvStudyGuide.DataBind();
    }
}