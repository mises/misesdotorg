#region

using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using Mises.Data;

#endregion

partial class Controls_BookstoreBestsellers : UserControl, IWebPart
{
    #region WebParts

    public string CatalogIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Description
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Subtitle
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
    }

    public string Title
    {
        get { return "Bookstore Bestsellers"; }
        set { }
    }

    public string TitleIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string TitleUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    #endregion

    public Controls_BookstoreBestsellers()
    {
        Load += Page_Load;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //  If Page.IsPostBack Then Return
        gvBooksList.DataSource =
            SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["MisesShop"].ConnectionString,
                                    CommandType.StoredProcedure, "dbo.spGetBestSellers");
        gvBooksList.DataBind();
    }
}