#region

using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

#endregion

partial class CustomControls_QuickLinks : UserControl, IWebPart
{
    #region WebParts

    public string CatalogIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Description
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string Subtitle
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
    }

    public string Title
    {
        get { return "Quick Links"; }
        set { }
    }

    public string TitleIconImageUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    public string TitleUrl
    {
        get
        {
            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }
        set { }
    }

    #endregion

    //Public Sub Bind(ByVal dt As DataTable)
    //    ' Me.litContents.Text = dt.Rows(0)("content")
    //End Sub
}