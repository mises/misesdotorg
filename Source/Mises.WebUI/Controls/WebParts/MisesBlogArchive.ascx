<%@ Control Language="C#" AutoEventWireup="false" CodeFile="MisesBlogArchive.ascx.cs"
	Inherits="CustomControls_MisesBlogArchive" %>

<div class="CommonSidebarArea">
	<div class="CommonSidebarRoundTop">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
	<div class="CommonSidebarInnerArea">
		<h4 class="CommonSidebarHeader">
		    <!-- TODO: FIX to relative URL -->
			<a href="http://blog.mises.org/">Mises Blog Posts</a> <a href="http://feeds.mises.org/MisesBlog?format=xml">
				<img alt="RSS Feed" style="border: none" src="/Theme/images/buttons/rss.gif" /></a></h4>
		<div class="CommonSidebarContent">
			<div class="CommonListArea">

				<asp:DataList ID="dlMisesBlog" runat="server">                                        
					<ItemTemplate>
						<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("link")%>' Text='<%#Eval("title")%>'></asp:HyperLink>
						by <%#Eval("author")%>
					</ItemTemplate>                    
				</asp:DataList>

			</div>
		</div>
	</div>
	<div class="CommonSidebarRoundBottom">
		<div class="r1">
		</div>
		<div class="r2">
		</div>
		<div class="r3">
		</div>
		<div class="r4">
		</div>
	</div>
</div>