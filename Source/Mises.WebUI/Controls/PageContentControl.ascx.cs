#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Documents;
using d = Mises.Domain.Documents;

#endregion

partial class Controls_PageContentControl : UserControl
{
    private string _contentName;

    public string ContentName
    {
        get { return _contentName; }
        set { _contentName = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cache[_contentName] == null || Request.Url.Host.StartsWith("beta"))
        {
            string contentText = Document.GetPageContentByName(_contentName);
            if (contentText != null)
            {
                Cache.Insert(_contentName, contentText, null, DateTime.Now.AddMinutes(10), TimeSpan.Zero);
            }
        }

        //Response.Write(Cache[_contentName].ToString());
        Controls.Add(new Literal() { Text = Cache[_contentName].ToString() });
        //litContentText.Text = Cache[_contentName].ToString();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}