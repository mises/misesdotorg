<%@ Control Language="C#" AutoEventWireup="false" ClientIDMode="Static" Inherits="Controls_ContactInfo"
    CodeBehind="ContactInfo.ascx.cs" %>
<table id="tblNameOnly" border="0" cellpadding="2" cellspacing="1" runat="server"
    visible="false">
    <tr>
        <td style="height: 25px">
            Name
        </td>
        <td style="height: 25px">
            <asp:TextBox ID="txtName" runat="server" AutoCompleteType="DisplayName" Columns="40"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                ErrorMessage="You must enter a name.">
                *</asp:RequiredFieldValidator>
        </td>
        <td align="center" colspan="1" rowspan="100" style="height: 23px" valign="top" runat="server"
            id="tdImage">
            <img id="img1" runat="server" alt="Mises Coat of Arms" src="\images\coatofarms3.gif" />
        </td>
    </tr>
    <tr>
        <td style="height: 25px">
            Email
        </td>
        <td style="height: 25px" rowspan="2">
            <asp:TextBox ID="txtEmail2" runat="server" AutoCompleteType="Email" Columns="40"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ControlToValidate="txtEmail2"
                ErrorMessage="You must enter a valid email.">
                *</asp:RequiredFieldValidator>
           
        </td>
    </tr>
</table>
<table><tr>
 <td align="center" colspan="1" rowspan="100" style="height: 23px" valign="top" runat="server"
            id="tdImg" Visible="False">
            <img id="imgLogo" runat="server" alt="Mises Coat of Arms" src="\images\coatofarms3.gif" />
        </td></tr></table>
<table id="tblContactInfo" border="0" cellpadding="2" cellspacing="1" runat="server">
    <tr>
        <td style="height: 25px" colspan="1" rowspan="1">
            
            First</td>
        <td colspan="1" rowspan="1" class="style1">
            <asp:TextBox ID="txtFirstName" runat="server" AutoCompleteType="FirstName" Columns="30"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirst12" runat="server" ControlToValidate="txtFirstName"
                ErrorMessage="You must enter a first name.">
                *</asp:RequiredFieldValidator>
        </td>
        <td>Last</td>
        <td>
            <asp:TextBox ID="txtLastName" runat="server" AutoCompleteType="LastName" Columns="30"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLast2" runat="server" ControlToValidate="txtLastName"
                ErrorMessage="You must enter a last name.">
                *</asp:RequiredFieldValidator>
        </td>
        <td align="center" colspan="1" rowspan="96" style="height: 23px" valign="top" runat="server"
            id="td1">
            &nbsp;</td>
    </tr>
    <tr style="font-weight: bold">
        <td style="height: 25px">
            Email</td>
        <td class="style1">
            <asp:TextBox ID="txtEmail" runat="server" AutoCompleteType="Email" Columns="30"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                ErrorMessage="You must enter a valid email.">
                *</asp:RequiredFieldValidator>   
        </td>
        <td>
            Phone</td>
        <td>
            <asp:TextBox ID="txtPhone" runat="server" AutoCompleteType="HomePhone" Columns="15"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPhone"
                ErrorMessage="You must enter a valid phone number.">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr style="color: #000000">
        <td valign="top">
            Address
        </td>
        <td valign="top" class="style2">
            <asp:TextBox ID="txtAddress" runat="server" AutoCompleteType="HomeStreetAddress"
                Columns="30" Rows="3" TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtAddress"
                ErrorMessage="You must provide your address.">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr style="font-weight: bold; color: #000000">
        <td>
            City
        </td>
        <td class="style2">
            <asp:TextBox ID="txtCity" runat="server" AutoCompleteType="HomeCity" Columns="30"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCity"
                ErrorMessage="You must enter a city.">*</asp:RequiredFieldValidator>
        </td>
        <td style="font-weight: normal;">
            State</td>
        <td style="font-weight: normal;">
            <asp:TextBox ID="txtState" runat="server" AutoCompleteType="HomeState" Columns="15"></asp:TextBox>
            <em>&nbsp;"N/A" for none.</em>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtState"
                ErrorMessage="You must enter a state or 'N/A'.">*</asp:RequiredFieldValidator>
            </td>
    </tr>
    <tr>
        <td>
            Postal Code
        </td>
        <td class="style2">
            <asp:TextBox ID="txtPostalCode" runat="server" AutoCompleteType="HomeZipCode" Columns="15"></asp:TextBox>
            <em>&nbsp;"N/A" for none.</em>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtPostalCode"
                ErrorMessage="You must enter a postal code or 'N/A'.">*</asp:RequiredFieldValidator>
        </td>
        <td>
            Country</td>
        <td>
            <asp:DropDownList ID="ddCountries" runat="server" DataSourceID="sqlGetCountries"
                DataTextField="Name" DataValueField="Name" Height="16px">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sqlGetCountries" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                EnableCaching="True" SelectCommand="CountriesGetList" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>
        </td>
    </tr>
    </table>