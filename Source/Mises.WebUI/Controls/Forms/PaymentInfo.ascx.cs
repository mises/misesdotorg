#region

using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class Controls_PaymentInfo : UserControl
{
    public Controls_PaymentInfo()
    {
        Load += Page_Load;
    }

    public double Amount
    {
        get
        {
            if (lblGetCost.Text != "")
            {
                return Convert.ToDouble(lblGetCost.Text.Replace("$", ""));
            }
            else
            {
                return 0D;
            }
        }
        set { lblGetCost.Text = value.ToString("c"); }
    }

    public string CardName
    {
        get { return txtCardName.Text; }
        set { txtCardName.Text = value; }
    }

    public string AccountNumber
    {
        get { return txtCardNum.Text; }
        set { txtCardNum.Text = value; }
    }

    public string CardType
    {
        get { return ddCreditCardType.SelectedValue; }
        set { ddCreditCardType.SelectedValue = value; }
    }

    public string CardVerification
    {
        get { return txtCardVerification.Value; }
        set { txtCardVerification.Value = value; }
    }

    public int MonthExpiration
    {
        get { return Convert.ToInt32(ddMonthExpiration.SelectedValue); }
        set { ddMonthExpiration.SelectedValue = value.ToString(); }
    }

    public int YearExpiration
    {
        get { return Convert.ToInt32(ddYearExpiration.SelectedValue); }
        set { ddYearExpiration.SelectedValue = value.ToString(); }
    }

    public string PaymentComments
    {
        get { return txtComments.Value; }
        set { txtComments.Value = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize form
        string[] Months = Regex.Split("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec", " ");
        for (int i = 0; i <= 11; i++)
        {
            var month = new ListItem {Value = (i + 1).ToString(), Text = Months[i]};
            ddMonthExpiration.Items.Add(month);
        }
        for (int i = 0; i <= 10; i++)
        {
            var year = new ListItem
                           {
                               Value = (DateTime.Now.Year + i).ToString(),
                               Text = (DateTime.Now.Year + i).ToString()
                           };
            ddYearExpiration.Items.Add(year);
        }
    }
}