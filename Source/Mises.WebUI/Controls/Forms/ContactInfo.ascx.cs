#region

using System.Web.UI;

#endregion

partial class Controls_ContactInfo : UserControl
{
    //public string Title
    //{
    //    get { return lblBillingInformation.Text; }
    //    set { lblBillingInformation.Text = value; }
    //}

    public string ImageURL
    {
        get { return imgLogo.Src; }
        set
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                imgLogo.Src = value;
                img1.Src = value;
                tdImage.Visible = true;
            }
            else
            {
                tdImage.Visible = false;
                tdImg.Visible = false;
            }
        }
    }

    public string Email
    {
        get { return txtEmail.Text + txtEmail2.Text; }
        set
        {
            txtEmail.Text = value;

            txtEmail2.Text = value;

        }
    }

    public string Phone
    {
        get { return txtPhone.Text; }
        set { txtPhone.Text = value; }
    }

    public string FirstName
    {
        get {
            return  txtFirstName.Text + txtName.Text;
        }
        set
        {
            
                txtFirstName.Text = value;
            
                txtName.Text = value;
            
        }
    }

    public string LastName
    {
        get { return txtLastName.Text; }
        set { txtLastName.Text = value; }
    }

    public string Address
    {
        get { return txtAddress.Text; }
        set { txtAddress.Text = value; }
    }

    public string City
    {
        get { return txtCity.Text; }
        set { txtCity.Text = value; }
    }

    public string State
    {
        get { return txtState.Text; }
        set { txtState.Text = value; }
    }

    public string PostalCode
    {
        get { return txtPostalCode.Text; }
        set { txtPostalCode.Text = value; }
    }

    public string Country
    {
        get { return ddCountries.SelectedValue; }
        set { ddCountries.SelectedValue = value; }
    }

    public void GetNameOnly()
    {
        tblContactInfo.Visible = false;
        tblNameOnly.Visible = true;
    }
}