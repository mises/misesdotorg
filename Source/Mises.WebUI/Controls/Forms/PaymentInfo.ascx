<%@ Control Language="C#" AutoEventWireup="false" Inherits="Controls_PaymentInfo"
    CodeBehind="PaymentInfo.ascx.cs" %>
<table id="tblPaymentInformation" runat="server" border="0" cellpadding="2" cellspacing="1" style="width:640px;" visible="true">
    <tr>
        <th align="center" colspan="2" valign="middle">
            Payment Information
        </th>
    </tr>
    <tr>
        <td style="width:160px;">
            Amount to be Charged:
        </td>
        <td>
            <asp:Label ID="lblGetCost" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Name On Card:
        </td>
        <td>
            <asp:TextBox ID="txtCardName" runat="server" AutoCompleteType="DisplayName"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtCardName"
                ErrorMessage="You must enter a credit card name.">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Credit Card Type:
        </td>
        <td>
            &nbsp;
            <asp:DropDownList ID="ddCreditCardType" runat="server">
                <asp:ListItem Value="Visa">Visa</asp:ListItem>
                <asp:ListItem Value="MasterCard">MasterCard</asp:ListItem>
                <asp:ListItem Value="AMEX">American Express</asp:ListItem>
                <asp:ListItem Value="Discover">Discover</asp:ListItem>
                <asp:ListItem Value="Diners Club">Diners Club</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Number:
        </td>
        <td>
            <asp:TextBox ID="txtCardNum" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtCardNum"
                ErrorMessage="You must enter a credit card number.">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCardNum"
                ErrorMessage="Please enter a valid credit card number." ValidationExpression="\d{15,20}">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Verification Code:
        </td>
        <td>
            <input id="txtCardVerification" runat="server" maxlength="10" name="CardNumber" size="3"
                type="text" />
            (Three # code on back.)
        </td>
    </tr>
    <tr>
        <td>
            Expiration Date:
        </td>
        <td>
            <asp:DropDownList ID="ddMonthExpiration" runat="server">
            </asp:DropDownList>
            /
            <asp:DropDownList ID="ddYearExpiration" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr valign="top">
        <td>
            Comments:
        </td>
        <td>
            <textarea id="txtComments" runat="server" cols="23" name="Comments" rows="4" style="width:450px; height:85px;"></textarea>
        </td>
    </tr>
</table>
