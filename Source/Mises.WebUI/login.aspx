<%@ Page Language="c#" MasterPageFile="~/MasterPages/Content.master" Title="Mises Login" ClientIDMode="Static"
    AutoEventWireup="true" %>

<script runat="server" language="c#">


    private void OnLoginSuccess(object sender, EventArgs e)
    {
        string url = Request["ReturnUrl"];
        Response.Redirect(!string.IsNullOrEmpty(url) ? url : "/manager/");
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div align="center">
        <h3 class="CommonMessageTitle">
            Sign in to Mises.org</h3>
        <asp:Login ID="Login1" runat="server" RememberMeSet="True" Width="100%" OnLoggedIn="OnLoginSuccess">
            <LayoutTemplate>
                <table cellspacing="1" border="0" cellpadding="5" width="500">
                    <tr>
                        <td align="right" class="CommonFormFieldName">
                            <label for="UserName">
                                Sign in name</label>
                        </td>
                        <td class="CommonFormField">
                            <asp:TextBox ID="UserName" runat="server" Font-Size="0.8em" MaxLength="64" Columns="30"
                                CssClass="CommonTextBig">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="CommonFormFieldName">
                            <label for="Password">
                                Password</label>
                        </td>
                        <td class="CommonFormField">
                            <asp:TextBox ID="Password" runat="server" Font-Size="0.8em" class="CommonTextBig"
                                TextMode="Password" MaxLength="64" Columns="11">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                            <span class="txt4">(<a href="/Community/user/EmailForgottenPassword.aspx">I forgot my
                                password</a>)</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left" class="CommonFormField" nowrap="nowrap">
                            <span type="checkbox">
                                <asp:CheckBox ID="RememberMe" runat="server" Checked="True" Text="Remember me next time." /></span>
                            <label>
                                Next time automatically sign me in</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left" class="CommonFormField" nowrap="nowrap">
                            <asp:Button ID="LoginButton" runat="server" BackColor="White" BorderColor="#507CD1"
                                BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Verdana"
                                Font-Size="0.8em" ForeColor="#284E98" Text="Log In" CssClass="CommonTextButton Big"
                                ValidationGroup="Login1" />
                            <div align="center">
                                <div class="CommonMessageArea">
                                    <div class="CommonMessageContent error field-validation-error ui-state-error-text">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
        </asp:Login>
        <h4>
            New user? <a href="/Community/user/CreateUser.aspx?ReturnUrl=">Please sign up at Mises.com</a>
        </h4>
        
        <h4><a href="https://mises.freecapitalists.org/Manager/user.aspx">Manage My Account</a></h4>

    </div>
</asp:Content>
