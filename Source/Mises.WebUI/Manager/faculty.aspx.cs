﻿#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Mises.Data;

#endregion

partial class faculty : Page
{
    protected void btnAddPerson_Click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text,
                                  "INSERT INTO Faculty (name, groups, display) VALUES  (@name,@group,1)",
                                  new SqlParameter("@name", txtName.Text),
                                  new SqlParameter("@group", ddlGroup.SelectedValue));
        DataBind();
    }
}