#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class Manager_periodicals : Page
{
    protected void btnAddPeriodical_Click(object sender, EventArgs e)
    {
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        btnAddPeriodical.Click += btnAddPeriodical_Click;
        Load += Page_Load;
    }
}