﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" CodeFile="wardlibrary.aspx.cs" Inherits="Manager_periodicals_wardlibrary"     MaintainScrollPositionOnPostback="true"  ValidateRequest="false" %>
<%@ Register Src="../ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<h3>
		<asp:Label ID="lblTitle" runat="server">Edit Ward and Massey Libraries</asp:Label><br />
	</h3>
	<asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
		CssClass="submitbutton" onclick="btnSave_Click"></asp:Button>
	<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="submitbutton" OnClick="btnCancel_Click"></asp:Button>
	<asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this article?');" runat="server" Text="Delete" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<asp:Button ID="btnGoBack" OnClientClick="location.href='journalarchives.aspx?id=8'; return false;" runat="server" Text="Go Back" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<br />

<table style="margin-right: auto;margin-left: auto"  border="0" width="800">
    <tr><td>ID:</td><td><asp:Label ID="lblControl" runat="server" /></td></tr>
    <tr><td>Title</td><td nowrap="nowrap"><asp:TextBox ID="txttitle" Columns="80" runat="server" /> 
        <asp:RequiredFieldValidator ID="valTitleRequired" runat="server" 
            ErrorMessage="Title is required." Text="*" ControlToValidate="txttitle"></asp:RequiredFieldValidator></td>
    </tr>
    <tr><td></td><td>(plus subtitle; put "The" or "A" at the end; Virtue of Wealth, The)</td></tr>
    <tr>
        <td nowrap="nowrap">Author or Editor</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast" runat="server" />, <asp:TextBox ID="txtauthorfirst" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr><td></td><td>or institution; add ed. when applicable</td></tr>
    <tr>
        <td nowrap="nowrap">Co-Author</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast2" runat="server" />, <asp:TextBox ID="txtauthorfirst2" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap">Co-Author</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast3" runat="server" />, <asp:TextBox ID="txtauthorfirst3" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr><td>Display</td><td><asp:CheckBox ID="chkdisplay" runat="server" /></td></tr>
    <tr><td>Create Date</td>
        <td>
				<ew:CalendarPopup ID="calCreateDate" runat="server">
					<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="White"></WeekdayStyle>
					<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
					<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
						BackColor="AntiqueWhite"></OffMonthStyle>
					<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></GoToTodayStyle>
					<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGoldenrodYellow"></TodayDayStyle>
					<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
					<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGray"></WeekendStyle>
					<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
					<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></ClearDateStyle>					
				</ew:CalendarPopup>
        </td>
    </tr>
    <tr><td>Publisher Info</td><td><asp:TextBox ID="txtpublisher_info" runat="server" TextMode="MultiLine" Rows="5" Columns="80" /></td></tr>
    <tr><td></td><td>(City, State, Publisher, Edition, Date)</td></tr>
    <tr><td>Comments</td><td><asp:TextBox ID="txtcomments" runat="server" TextMode="MultiLine" Rows="5" Columns="80" /></td></tr>
    <tr><td></td><td>(edited by, series vol #s, special collection, translator, original publication date, etc.)</td></tr>
    <tr><td>Donated By</td><td><asp:TextBox ID="txtdonatedBy" runat="server" /></td></tr>
    <tr><td>Checked Out By</td><td><asp:TextBox ID="txtchecked_out_by" runat="server" /></td></tr>
    <tr><td>Donated Date</td><td>
    <ew:CalendarPopup ID="calDonatedDate" runat="server">
					<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="White"></WeekdayStyle>
					<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
					<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
						BackColor="AntiqueWhite"></OffMonthStyle>
					<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></GoToTodayStyle>
					<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGoldenrodYellow"></TodayDayStyle>
					<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
					<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGray"></WeekendStyle>
					<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
					<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></ClearDateStyle>					
				</ew:CalendarPopup>
    </td></tr>
    <tr><td>Coauthors</td><td><asp:TextBox ID="txtcoauthors" runat="server" /></td></tr>
    <tr><td>Subject</td><td><asp:TextBox ID="txtsubject" TextMode="MultiLine" Rows="5" Columns="80" runat="server" /></td></tr>
    <tr><td></td><td>(i.e. "inflation, economics, monetary"... etc)</td></tr>
    <tr><td colspan="2"><asp:ValidationSummary ID="ValidationSummary1" runat="server" /></td></tr>
		<tr>
			<td valign="top" class="detailheader">
				Tags
			</td>
			<td class="detailtext">
				<uc1:DocumentTags ID="DocumentTags1" runat="server" />
			</td>
		</tr>
</table>
</asp:Content>

