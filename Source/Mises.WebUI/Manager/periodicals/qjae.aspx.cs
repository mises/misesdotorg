#region

using System;
using System.Data.Entity.Core.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Data;

#endregion

public partial class QJAEpage : Page
{
    private void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        int ArticleId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

        if (ArticleId == 0)
        {
            lblTitle.Text = "Add New QJAE";
            lblStoryId.Text = "0";
        }
        else
        {
            lblStoryId.Text = ArticleId.ToString();

            using (MisesModel model = DataHelper.EntityDataModel)
            {
                QJAEdb article = model.QJAEdb.First(p => p.control == ArticleId);
                lblTitle.Text = "Editing: " + article.title;
                Page.Title = lblTitle.Text;
                txtTitle.Text = article.title;

                txtVolume.Text = Convert.ToString(article.volume);
                txtNumber.Text = Convert.ToString(article.number);
                txtArticleNumber.Text = Convert.ToString(article.articleNum);


                txt1First.Text = article.authorFirst1;
                txt1Last.Text = article.authorLast1;

                txt2First.Text = article.authorFirst2;
                txt2Last.Text = article.authorLast2;


                if (article.Author1 != null) AuthorSelect1.Author1 = (int) article.Author1;
                if (article.Author2 != null) AuthorSelect1.Author2 = (int) article.Author2;

                lnkGUID.Text = article.GUID.ToString();
                lnkGUID.NavigateUrl = string.Format("/resources/{0}", article.GUID);

                DocumentTags1.DocumentIdentifier = article.GUID;
            }
        }
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(Conversion.Val(lblStoryId.Text));

        using (MisesModel model = DataHelper.EntityDataModel)
        {
            QJAEdb article = id > 0 ? model.QJAEdb.First(p => p.control == id) : new QJAEdb();

            article.title = txtTitle.Text;
            article.articleNum = int.Parse(txtArticleNumber.Text);
            article.volume = int.Parse(txtVolume.Text);
            article.number = int.Parse(txtNumber.Text);
            article.Author1 = 0;
            article.Author2 = 0;
            article.authorFirst1 = txt1First.Text;
            article.authorLast1 = txt1Last.Text;
            article.authorFirst2 = txt2First.Text;
            article.authorLast2 = txt2Last.Text;
            article.display = "Yes";

            article.Author1 = AuthorSelect1.Author1;
            article.Author2 = AuthorSelect1.Author2;

            article.GUID = Guid.NewGuid();
            article.CreateDate = DateTime.Now;

            if (article.control == 0)
                model.AddToQJAEdb(article);

            model.SaveChanges();


            lblStoryId.Text = article.control.ToString();
        }

        if (fileUploadFile.HasFile) btnUploadFile_Click(null, null);

        Response.Redirect("/manager/periodicals/JournalArchives.aspx?Id=4");
    }


    private void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/manager/periodicals/JournalArchives.aspx?Id=4");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int articleId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

        using (MisesModel model = DataHelper.EntityDataModel)
        {
            QJAEdb article = model.QJAEdb.First(p => p.control == articleId);
            model.DeleteObject(article);
            model.SaveChanges();
        }
        Response.Redirect("/manager/periodicals/JournalArchives.aspx?Id=4");
    }

    protected void btnUploadFile_Click(object sender, EventArgs e)
    {
        int articleId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        if (articleId == 0)
            btnSave_Click(null, null);

        if (fileUploadFile.HasFile)
        {
            if (File.Exists(Server.MapPath(GetSavePath())))
            {
                File.Delete(Server.MapPath(GetSavePath()));
            }

            fileUploadFile.SaveAs(Server.MapPath(GetSavePath()));
            SetFeedbackMessage("File Uploaded to " + GetSavePath());
        }
        else
        {
            SetFeedbackMessage("Error: No file selected!");
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Load += Page_Load;
        btnSave.Click += btnSave_Click;
        Cancel.Click += Cancel_Click;
        btnDelete.Click += btnDelete_Click;
    }

    #region FileUpload

    private string GetSavePath()
    {
        return string.Format("../../journals/qjae/pdf/qjae{0}_{1}_{2}.pdf", txtVolume.Text, txtNumber.Text,
                             txtArticleNumber.Text);
    }


    private void SetFeedbackMessage(string message)
    {
        lblErrorMessage.Text += message;
    }

    #endregion FileUpload
}