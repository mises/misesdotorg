<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="Mises Review Editor"
    MaintainScrollPositionOnPostback="true" ValidateRequest="false" Inherits="MisesReview"
    CodeBehind="misesreview.aspx.cs" %>

<%@ Register Src="../ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Label ID="lblTitle" runat="server">Edit Mises Review</asp:Label><br />
    </h2>
    <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
        <asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
            CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
        <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="ui-state-default ui-corner-all ui-button">
        </asp:Button>
        <asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this Story?');"
            runat="server" Text="Delete" CssClass="ui-state-default ui-corner-all ui-button">
        </asp:Button>
        <asp:Button ID="btnTidyHTML" runat="server" Text="Tidy HTML" CssClass="ui-state-default ui-corner-all ui-button">
        </asp:Button>
    </div>
    
    <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" Font-Bold="True"
        ForeColor="Red"></asp:Label>
    <table style="margin-right: auto; margin-left: auto" border="0" width="800">
        <tr>
            <td valign="top" class="detailheader">
                ID:
                <asp:Label ID="lblStoryId" runat="server" Font-Bold="True">0</asp:Label>
            </td>
            <td class="detailtext">
                Issue:
                <asp:DropDownList ID="ddlIssueSeason" runat="server">
                    <asp:ListItem Value="1">Spring</asp:ListItem>
                    <asp:ListItem Value="2">Summer</asp:ListItem>
                    <asp:ListItem Value="3">Fall</asp:ListItem>
                    <asp:ListItem Value="4">Winter</asp:ListItem>
                </asp:DropDownList>
                Year:
                <asp:TextBox ID="txtIssueYear" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Date
            </td>
            <td class="detailtext">
                <ew:CalendarPopup ID="calStoryDate" runat="server">
                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                        BackColor="White"></WeekdayStyle>
                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                        BackColor="AntiqueWhite"></OffMonthStyle>
                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                        BackColor="LightGray"></WeekendStyle>
                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                </ew:CalendarPopup>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Title
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtTitle" runat="server" Columns="80"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" class="detailheader">
                First Author
            </td>
            <td align="left" class="detailtext">
                First:
                <asp:TextBox ID="txt1First" runat="server"></asp:TextBox>
                &nbsp;Last:
                <asp:TextBox ID="txt1Last" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" class="detailheader">
                Second Author
            </td>
            <td align="left" class="detailtext">
                First:
                <asp:TextBox ID="txt2First" runat="server"></asp:TextBox>
                &nbsp;Last:
                <asp:TextBox ID="txt2Last" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" class="detailheader">
                Third Author
            </td>
            <td align="left" class="detailtext">
                First:
                <asp:TextBox ID="txt3First" runat="server"></asp:TextBox>
                &nbsp;Last:
                <asp:TextBox ID="txt3Last" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                GUID
            </td>
            <td align="left" class="detailtext">
                <asp:HyperLink ID="lnkGUID" runat="server"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2" class="detailheader">
                <div id="DailyStory">
                    <asp:TextBox ID="txtFullText" runat="server" Height="500px" Width="100%" TextMode="MultiLine"
                        CssClass="ckeditor"></asp:TextBox>
                </div>
            </td>
        </tr>
        <tr>
            <td class="detailheader" style="font-weight: bold" valign="top">
                Edited:
            </td>
            <td class="detailtext">
                <asp:Label runat="server" ID="lblEditedBy"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Tags
            </td>
            <td class="detailtext">
                <uc1:DocumentTags ID="DocumentTags1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
