﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" CodeFile="scholar.aspx.cs" Inherits="Manager_periodicals_scholar"     MaintainScrollPositionOnPostback="true"  ValidateRequest="false" %>
<%@ Register Src="../ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<h3>
		<asp:Label ID="lblTitle" runat="server">Edit Scholar Article</asp:Label><br />
	</h3>
	<asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
		CssClass="submitbutton" onclick="btnSave_Click"></asp:Button>
	<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="submitbutton" OnClick="btnCancel_Click"></asp:Button>
	<asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this article?');" runat="server" Text="Delete" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<asp:Button ID="btnGoBack" OnClientClick="location.href='journalarchives.aspx?id=7'; return false;" runat="server" Text="Go Back" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<br />

<table style="margin-right: auto;margin-left: auto"  border="0" width="800">
    <tr><td>ID:</td><td><asp:Label ID="lblControl" runat="server" /></td></tr>
    <tr><td>Author Name</td><td><asp:TextBox ID="txtauthor_name" runat="server" /></td></tr>
    <tr><td>Paper Name</td><td><asp:TextBox ID="txtpaper_name" runat="server" /></td></tr>
    <tr><td>When It...?</td><td><asp:TextBox ID="txtwhenit" runat="server" /></td></tr>
    <tr><td>Create Date</td><td><ew:CalendarPopup ID="calCreateDate" runat="server">
					<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="White"></WeekdayStyle>
					<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
					<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
						BackColor="AntiqueWhite"></OffMonthStyle>
					<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></GoToTodayStyle>
					<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGoldenrodYellow"></TodayDayStyle>
					<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
					<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGray"></WeekendStyle>
					<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
					<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></ClearDateStyle>					
				</ew:CalendarPopup></td></tr>
    <tr><td>File Name</td><td><asp:TextBox ID="txtfile_name" runat="server" /></td></tr>
    <tr><td>Display</td><td><asp:CheckBox ID="chkdisplay" runat="server" /></td></tr>
    <tr><td>Notation</td><td><asp:TextBox ID="txtnotation" runat="server" /></td></tr>
    <tr><td>List Order</td><td><asp:TextBox ID="txtlistorder" runat="server" /></td></tr>
    <tr><td colspan="2"><asp:ValidationSummary ID="ValidationSummary1" runat="server" /></td></tr>
</table>
</asp:Content>

