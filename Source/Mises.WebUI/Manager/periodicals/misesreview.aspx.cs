#region

using System;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Domain.Periodicals;
using Mises.Domain.Utility;

#endregion

partial class MisesReview : Page
{
    // private string PeriodicalId;

    private void Page_Load(object sender, EventArgs e)
    {
        if (! Page.IsPostBack)
        {
            int ReviewId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

            if (ReviewId == 0)
            {
                lblTitle.Text = "Add New Mises Review";
                lblStoryId.Text = "0";
            }
            else
            {
                lblStoryId.Text = ReviewId.ToString();
                PeriodicalStory story = PeriodicalStory.GetPeriodicalDetails(2, ReviewId);

                lblTitle.Text = "Editing: " + story.Title;
                Page.Title = lblTitle.Text;
                txtTitle.Text = story.Title;

                txtIssueYear.Text = Convert.ToString(story.IssueYear);

                try
                {
                    ddlIssueSeason.SelectedValue = Convert.ToString(story.IssueSeason);
                }
                catch
                {
                }


                txt1First.Text = story.AuthorFirst;
                txt1Last.Text = story.AuthorLast;

                txt2First.Text = story.Author2First;
                txt2Last.Text = story.Author2Last;

                txt3First.Text = story.Author3First;
                txt3Last.Text = story.Author3Last;

                lnkGUID.Text = story.GUID.ToString();
                lnkGUID.NavigateUrl = string.Format("/resources/{0}", story.GUID);
                // bool display = false;

                txtFullText.Text = story.Content;

                DocumentTags1.DocumentIdentifier = story.GUID;

                lblEditedBy.Text = story.EditedBy;
            }
        }
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
        // Save Storys

        var Story = new PeriodicalStory();
        Story.Id = Convert.ToInt32(Conversion.Val(lblStoryId.Text));
        Story.PeriodicalId = 2;
        Story.Title = txtTitle.Text.Trim();
        Story.Content = txtFullText.Text;


        Story.AuthorFirst = txt1First.Text;
        Story.AuthorLast = txt1Last.Text;

        Story.Author2First = txt2First.Text;
        Story.Author2Last = txt2Last.Text;

        Story.Author3First = txt3First.Text;
        Story.Author3Last = txt3Last.Text;

        string Editor = null;
        if (Context.User.Identity.IsAuthenticated)
        {
            Editor = Context.User.Identity.Name;
        }
        else
        {
            Editor = Request.GetOriginalHostAddress();
        }

        Story.IssueYear = Convert.ToInt32(Conversion.Val(txtIssueYear.Text));
        Story.IssueSeason = Convert.ToInt32(ddlIssueSeason.SelectedValue);

        Story.EditedBy = Editor;

        lblStoryId.Text = Story.UpdatePeriodical().ToString();

        lblTitle.Text = "Story Saved.<br /><a href=Storys.aspx>Back to Index</a>";
    }


    private void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Storys.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int StoryId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        PeriodicalStory.Delete(new Guid(lnkGUID.Text));
        Response.Redirect("Storys.aspx");
    }

    protected void btnTidyHTML_Click(object sender, EventArgs e)
    {
        txtFullText.Text = HTMLtidy.TidyHTML(txtFullText.Text);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        btnSave.Click += btnSave_Click;
        Cancel.Click += Cancel_Click;
        btnDelete.Click += btnDelete_Click;
        btnTidyHTML.Click += btnTidyHTML_Click;
    }
}