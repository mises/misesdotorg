﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" CodeFile="wardlibrarydb.aspx.cs" Inherits="Manager.periodicals.wardlibrarydb" %>
<%@ Register src="../../Controls/Navigation/LetterSelector.ascx" tagname="LetterSelector" tagprefix="uc1" %>
<%@ Register assembly="Mises.WebControls" namespace="Mises.WebControls" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="search">
            <a href="wardlibrary.aspx">Add New Book</a>
        </div>
        <div class="search">
            <uc1:LetterSelector ID="LetterSelector1" runat="server" OnOnLetterSelected="LetterSelector1_OnOnLetterSelected" />
        </div>
        <div class="search">
            <strong>Search the Ward Library by:</strong>
            <asp:RadioButtonList ID="rblSearchBy" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem>Title</asp:ListItem>
                <asp:ListItem>Authors</asp:ListItem>
                <asp:ListItem>Donated By</asp:ListItem>
                <asp:ListItem>Subject</asp:ListItem>
                <asp:ListItem Selected="True">All</asp:ListItem>
            </asp:RadioButtonList>
            <asp:TextBox ID="txtSearchTerm" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" Text="Search" 
                onclick="btnSearch_Click" />
        </div>
        <div class="search">
            <strong>Search by Control ID:</strong><br />
            <asp:TextBox ID="txtSearchByControlId" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearchByControlId" runat="server" Text="Search" 
                onclick="btnSearchByControlId_Click" />
        </div>
        <div class="search">
            <strong>Search by Control ID Range:</strong><br />
            From: <asp:TextBox ID="txtSearchRange1" runat="server"></asp:TextBox> 
            To: <asp:TextBox ID="txtSearchRange2" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearchRange" runat="server" Text="Search" onclick="btnSearchRange_Click" />
        </div>
        <div class="search">
            View the <asp:LinkButton ID="lnkLastFifty" runat="server" onclick="lnkLastFifty_Click">last 50 books</asp:LinkButton> entered in the database<br />
            <asp:LinkButton ID="lnkCheckedOut" runat="server" onclick="lnkCheckedOut_Click">View all checked out books</asp:LinkButton><br />
            <a href="/library.aspx">Go to Live Ward Library</a><br />
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate><img src="/images/loading.gif" /></ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:GridView ID="gvSearchResults" runat="server">
        <Columns><asp:HyperLinkField DataNavigateUrlFields="control" DataNavigateUrlFormatString="wardlibrary.aspx?Id={0}" DataTextField="control" HeaderText="Edit" /></Columns>
        <EmptyDataTemplate>No results.</EmptyDataTemplate>
        </cc1:GridView>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

