﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" CodeFile="jls.aspx.cs" Inherits="Manager_periodicals_jls" 
    MaintainScrollPositionOnPostback="true"  ValidateRequest="false" %>
<%@ Register Src="../ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<h3>
		<asp:Label ID="lblTitle" runat="server">Edit Journal of Libertarian Studies</asp:Label><br />
	</h3>
	<asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
		CssClass="submitbutton" onclick="btnSave_Click"></asp:Button>
	<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="submitbutton" OnClick="btnCancel_Click"></asp:Button>
	<asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this article?');" runat="server" Text="Delete" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<asp:Button ID="btnGoBack" OnClientClick="location.href='journalarchives.aspx?id=3'; return false;" runat="server" Text="Go Back" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<br />

<table style="margin-right: auto;margin-left: auto"  border="0" width="800">
    <tr><td>ID:</td><td><asp:Label ID="lblControl" runat="server" /></td></tr>
    <tr><td>Title</td><td><asp:TextBox ID="txttitle" Columns="80" runat="server" /> 
        <asp:RequiredFieldValidator ID="valTitleRequired" runat="server" 
            ErrorMessage="Title is required." Text="*" ControlToValidate="txttitle" /></td></tr>
    <tr>
        <td nowrap="nowrap">First Author</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast" runat="server" />, <asp:TextBox ID="txtauthorfirst" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap">Second Author</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast2" runat="server" />, <asp:TextBox ID="txtauthorfirst2" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr><td>Display</td><td><asp:CheckBox ID="chkdisplay" runat="server" /></td></tr>
    <tr><td>Volume</td><td><asp:TextBox ID="txtvolume" runat="server" />
        <asp:RequiredFieldValidator ID="valVolumeRequired" runat="server" ErrorMessage="Volume is required." Text="*" ControlToValidate="txtvolume" />
        <asp:RangeValidator ID="valVolumeNumeric" runat="server" ErrorMessage="Volume has to be numeric value." Type="Integer" ControlToValidate="txtvolume" MinimumValue="0" MaximumValue="1000000" /></td></tr>
    <tr><td>Number</td><td><asp:TextBox ID="txtnumber" runat="server" />
        <asp:RequiredFieldValidator ID="valNumberRequired" runat="server" ErrorMessage="Number is required." Text="*" ControlToValidate="txtnumber" />
        <asp:RangeValidator ID="valNumberNumeric" runat="server" ErrorMessage="Number has to be numeric value." Type="Integer" ControlToValidate="txtnumber" MinimumValue="0" MaximumValue="1000000" /></td></tr>
    <tr><td>Article Number</td><td><asp:TextBox ID="txtarticleNum" runat="server" />
        <asp:RequiredFieldValidator ID="valArticleNumRequired" runat="server" ErrorMessage="Article Number is required." Text="*" ControlToValidate="txtarticlenum" />
        <asp:RangeValidator ID="valArticleNumNumeric" runat="server" ErrorMessage="Article Number has to be numeric value." Type="Integer" ControlToValidate="txtarticlenum" MinimumValue="0" MaximumValue="1000000" /></td></tr>
    <tr><td>link</td><td><asp:TextBox ID="txtlink" runat="server" /></td></tr>

    <tr><td>GUID</td><td><asp:HyperLink ID="lnkGUID" runat="server"></asp:HyperLink></td></tr>
    <tr><td colspan="2"><asp:ValidationSummary ID="ValidationSummary1" runat="server" /></td></tr>
</table>
</asp:Content>

