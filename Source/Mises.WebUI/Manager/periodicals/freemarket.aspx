﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" CodeFile="freemarket.aspx.cs" Inherits="Manager_periodicals_freemarket" 
    MaintainScrollPositionOnPostback="true"  ValidateRequest="false" %>
<%@ Register Src="../ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<h3>
		<asp:Label ID="lblTitle" runat="server">Edit Mises Review</asp:Label><br />
	</h3>
	<asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
		CssClass="submitbutton" onclick="btnSave_Click"></asp:Button>
	<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="submitbutton" OnClick="btnCancel_Click"></asp:Button>
	<asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this article?');" runat="server" Text="Delete" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<asp:Button ID="btnGoBack" OnClientClick="location.href='journalarchives.aspx?id=1'; return false;" runat="server" Text="Go Back" CssClass="submitbutton" onclick="btnDelete_Click"></asp:Button>
	<br />

<table style="margin-right: auto;margin-left: auto"  border="0" width="800">
    <tr><td>ID:</td><td><asp:Label ID="lblControl" runat="server" /></td></tr>
    <tr><td>Title</td><td><asp:TextBox ID="txttitle" Columns="80" runat="server" /> 
        <asp:RequiredFieldValidator ID="valTitleRequired" runat="server" 
            ErrorMessage="Title is required." Text="*" ControlToValidate="txttitle"></asp:RequiredFieldValidator></td></tr>
    <tr>
        <td nowrap="nowrap">First Author</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast" runat="server" />, <asp:TextBox ID="txtauthorfirst" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap">Second Author</td>
        <td style="vertical-align: bottom">
            <asp:TextBox ID="txtauthorlast2" runat="server" />, <asp:TextBox ID="txtauthorfirst2" runat="server" />
             (Last name, First name)
        </td>
    </tr>
    <tr><td>Subject 1</td><td><asp:TextBox ID="txtsubject1" Columns="80" runat="server" /></td></tr>
    <tr><td>Subject 2</td><td><asp:TextBox ID="txtsubject2" Columns="80" runat="server" /></td></tr>
    <tr><td>Subject 3</td><td><asp:TextBox ID="txtsubject3" Columns="80" runat="server" /></td></tr>
    <tr><td>GUID</td><td><asp:HyperLink ID="lnkGUID" runat="server"></asp:HyperLink></td></tr>
    <tr><td colspan="2"><asp:ValidationSummary ID="ValidationSummary1" runat="server" /></td></tr>
    <tr><td colspan="2"><asp:TextBox ID="txtbody" runat="server" Height="500px" Width="100%" TextMode="MultiLine" CssClass="ckeditor" /></td></tr>
    <tr><td>Article Date</td>
        <td>
				<ew:CalendarPopup ID="calArticleDate" runat="server">
					<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="White"></WeekdayStyle>
					<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
					<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
						BackColor="AntiqueWhite"></OffMonthStyle>
					<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></GoToTodayStyle>
					<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGoldenrodYellow"></TodayDayStyle>
					<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
					<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
						BackColor="LightGray"></WeekendStyle>
					<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
					<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
						ForeColor="Black" BackColor="White"></ClearDateStyle>					
				</ew:CalendarPopup>
        </td>
    </tr>
    <tr><td>PDFurl</td><td><asp:TextBox ID="txtPDFurl" runat="server" /></td></tr>
		<tr>
			<td valign="top" class="detailheader">
				Tags
			</td>
			<td class="detailtext">
				<uc1:DocumentTags ID="DocumentTags1" runat="server" />
			</td>
		</tr>
</table>
</asp:Content>

