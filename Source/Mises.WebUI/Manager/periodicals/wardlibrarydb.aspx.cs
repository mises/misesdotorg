﻿using System;
using System.Linq;
using Controls.Navigation;
using Mises.Data;

namespace Manager.periodicals
{
    public partial class wardlibrarydb : System.Web.UI.Page
    {
        private static readonly MisesModel model = new MisesModel();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            switch (rblSearchBy.SelectedValue)
            {
                case "All":
                    SearchAll(txtSearchTerm.Text);
                    break;

                case "Title":
                    SearchByTitle(txtSearchTerm.Text);
                    break;

                case "Authors":
                    SearchByAuthors(txtSearchTerm.Text);
                    break;

                case "Donated By":
                    SearchByDonator(txtSearchTerm.Text);
                    break;

                case "Subject":
                    SearchBySubject(txtSearchTerm.Text);
                    break;
            }
        }

        private void SearchBySubject(string text)
        {
            var query = from book
                            in model.WardLibrary
                        where book.subject.Contains(text)
                        select book;

            BindSearchResults(query);
        }

        private void SearchByDonator(string text)
        {
            var query = from book
                            in model.WardLibrary
                        where book.donatedBy.Contains(text)
                        select book;

            BindSearchResults(query);
        }

        private void SearchByAuthors(string text)
        {
            var query = from book
                            in model.WardLibrary
                        where
                            book.authorfirst.Contains(text) ||
                            book.authorlast.Contains(text) ||
                            book.authorfirst2.Contains(text) ||
                            book.authorlast2.Contains(text) ||
                            book.authorfirst3.Contains(text) ||
                            book.authorlast3.Contains(text)
                        select book;

            BindSearchResults(query);
        }

        private void SearchByTitle(string text)
        {
            var query = from book
                            in model.WardLibrary
                        where book.title.Contains(text)
                        select book;

            BindSearchResults(query);
        }

        private void SearchAll(string text)
        {
            var query = from book
                            in model.WardLibrary
                        where
                            book.title.Contains(text) ||
                            book.authorfirst.Contains(text) ||
                            book.authorlast.Contains(text) ||
                            book.authorfirst2.Contains(text) ||
                            book.authorlast2.Contains(text) ||
                            book.authorfirst3.Contains(text) ||
                            book.authorlast3.Contains(text) ||
                            book.donatedBy.Contains(text) ||
                            book.subject.Contains(text)
                        select book;

            BindSearchResults(query);
        }

        protected void btnSearchByControlId_Click(object sender, EventArgs e)
        {
            int id;
            Int32.TryParse(txtSearchByControlId.Text, out id);

            var query = from book
                            in model.WardLibrary
                            where book.control == id
                        select book;

            BindSearchResults(query);
        }

        protected void btnSearchRange_Click(object sender, EventArgs e)
        {
            int id1;
            int id2;
            Int32.TryParse(txtSearchRange1.Text, out id1);
            Int32.TryParse(txtSearchRange2.Text, out id2);

            var query = from book
                            in model.WardLibrary
                            where book.control >= id1 && book.control <= id2
                        select book;

            BindSearchResults(query);
        }

        protected void LetterSelector1_OnOnLetterSelected(object sender, LetterSelectedEventArgs e)
        {
            var query = from book
                            in model.WardLibrary
                            where book.title.StartsWith(e.Letter)
                            orderby book.title
                        select book;

            BindSearchResults(query);
        }

        protected void lnkLastFifty_Click(object sender, EventArgs e)
        {
            var query = from book
                in model.WardLibrary
                        orderby book.CreateDate descending 
                        select book;

            BindSearchResults(query.Take(50));
        }

        protected void lnkCheckedOut_Click(object sender, EventArgs e)
        {
            var query = from book
                in model.WardLibrary
                        where !String.IsNullOrEmpty(book.checked_out_by)
                        orderby book.title
                        select book;

            BindSearchResults(query);
        }

        private void BindSearchResults(IQueryable<WardLibrary> query)
        {
            gvSearchResults.DataSource = query;
            gvSearchResults.DataBind();
        }
}
}