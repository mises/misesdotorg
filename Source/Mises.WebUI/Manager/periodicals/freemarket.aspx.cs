﻿using System;
using System.Linq;
using System.Web.UI;
using Mises.Data;

public partial class Manager_periodicals_freemarket : Page
{
    #region Constants

    private const string PAGE_NOT_FOUND_URL = "~/404.aspx";
    private const string ARTICLE_URL_TEMPLATE = "/resources/{0}";
    private const string CURRENT_PAGE_TEMPLATE = "freemarket.aspx?id={0}";
    private const string JOURNAL_ARCHIVES_URL = "/manager/periodicals/JournalArchives.aspx?Id=2";
    private const string ERROR_PAGE_URL = "~/Error.aspx";

    #endregion

    #region Member variables

    private static readonly MisesModel model = new MisesModel();

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        string redirectUrl = null;

        try
        {
            if (IsPostBack) return;

            var id = GetQueryStringId();

            if (id == 0)
            {
                SetDefaultValues();
            }
            else
            {
                var article = GetArticle(id);

                if (article == null)
                {
                    redirectUrl = PAGE_NOT_FOUND_URL;
                }
                else
                {
                    Display(article);
                }
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        if (redirectUrl != null)
        {
            Response.Redirect(redirectUrl, true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            if (!IsValid) return;

            FreeMarket article = IsNewArticle() ? Insert() : Update();

            redirectUrl = article == null ? PAGE_NOT_FOUND_URL : GetArticleUrl(article.control);
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            var article = GetArticle(GetFormId());

            if (article == null)
            {
                redirectUrl = PAGE_NOT_FOUND_URL;
            }
            else
            {
                model.DeleteObject(article);
                model.SaveChanges();

                redirectUrl = JOURNAL_ARCHIVES_URL;
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetArticleUrl(GetFormId()));
    }

    #endregion

    #region Private methods

    private bool IsNewArticle()
    {
        return GetFormId() == 0;
    }

    private int GetQueryStringId()
    {
        int id;
        return Int32.TryParse(Request.QueryString["Id"], out id) ? id : 0;
    }

    private int GetFormId()
    {
        int id;
        return Int32.TryParse(lblControl.Text, out id) ? id : 0;
    }

    private FreeMarket CreateAndLoad()
    {
        var journal = new FreeMarket {GUID = Guid.NewGuid()};

        return LoadValues(journal);
    }

    private FreeMarket LoadValues(FreeMarket article)
    {
        article.title = txttitle.Text;
        article.authorfirst = txtauthorfirst.Text;
        article.authorlast = txtauthorlast.Text;
        article.authorfirst2 = txtauthorfirst2.Text;
        article.authorlast2 = txtauthorlast2.Text;
        article.subject1 = txtsubject1.Text;
        article.subject2 = txtsubject2.Text;
        article.subject3 = txtsubject3.Text;
        article.body = txtbody.Text;
        article.articledate = calArticleDate.SelectedValue;
        article.PDFurl = txtPDFurl.Text;

        return article;
    }

    private void Display(FreeMarket article)
    {
        lblControl.Text = article.control.ToString();
        txttitle.Text = article.title;
        lnkGUID.Text = article.GUID.ToString();
        lnkGUID.NavigateUrl = string.Format(ARTICLE_URL_TEMPLATE, article.GUID);
        txtauthorfirst.Text = article.authorfirst;
        txtauthorlast.Text = article.authorlast;
        txtauthorfirst2.Text = article.authorfirst2;
        txtauthorlast2.Text = article.authorlast2;
        txtsubject1.Text = article.subject1;
        txtsubject2.Text = article.subject2;
        txtsubject3.Text = article.subject3;
        txtbody.Text = article.body;
        calArticleDate.SelectedValue = article.articledate;
        txtPDFurl.Text = article.PDFurl;
    }

    private FreeMarket GetArticle(int id)
    {
        return model.FreeMarket.FirstOrDefault(fm => fm.control == id);
    }

    private FreeMarket Insert()
    {
        var article = CreateAndLoad();
        model.FreeMarket.AddObject(article);
        model.SaveChanges();

        return article;
    }

    private FreeMarket Update()
    {
        var article = GetArticle(GetFormId());

        if (article != null)
        {
            LoadValues(article);
            model.SaveChanges();
        }

        return article;
    }

    private static string GetArticleUrl(int articleId)
    {
        return string.Format(CURRENT_PAGE_TEMPLATE, articleId);
    }

    private void SetDefaultValues()
    {
        lblControl.Text = "[new]";
        calArticleDate.SelectedValue = DateTime.Today;
    }

    #endregion
}