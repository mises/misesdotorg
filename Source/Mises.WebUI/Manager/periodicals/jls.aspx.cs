﻿using System;
using System.Linq;
using Mises.Data;

public partial class Manager_periodicals_jls : System.Web.UI.Page
{
    #region Constants

    private const string PAGE_NOT_FOUND_URL = "~/404.aspx";
    private const string ARTICLE_URL_TEMPLATE = "/resources/{0}";
    private const string CURRENT_PAGE_TEMPLATE = "JLS.aspx?id={0}";
    private const string JOURNAL_ARCHIVES_URL = "/manager/periodicals/JournalArchives.aspx?Id=3";
    private const string ERROR_PAGE_URL = "~/Error.aspx";

    #endregion

    #region Member variables

    private static readonly MisesModel model = new MisesModel();

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        string redirectUrl = null;

        try
        {
            if (IsPostBack) return;

            var id = GetQueryStringId();

            if (id == 0)
            {
                SetDefaultValues();
            }
            else
            {
                var article = GetArticle(id);

                if (article == null)
                {
                    redirectUrl = PAGE_NOT_FOUND_URL;
                }
                else
                {
                    Display(article);
                }
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        if (redirectUrl != null)
        {
            Response.Redirect(redirectUrl, true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            if (!IsValid) return;

            JLS article = IsNewArticle() ? Insert() : Update();

            redirectUrl = article == null ? PAGE_NOT_FOUND_URL : GetArticleUrl(article.control);
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            var article = GetArticle(GetFormId());

            if (article == null)
            {
                redirectUrl = PAGE_NOT_FOUND_URL;
            }
            else
            {
                model.DeleteObject(article);
                model.SaveChanges();

                redirectUrl = JOURNAL_ARCHIVES_URL;
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetArticleUrl(GetFormId()));
    }

    #endregion

    #region Private methods

    private bool IsNewArticle()
    {
        return GetFormId() == 0;
    }

    private int GetQueryStringId()
    {
        int id;
        return Int32.TryParse(Request.QueryString["Id"], out id) ? id : 0;
    }

    private int GetFormId()
    {
        int id;
        return Int32.TryParse(lblControl.Text, out id) ? id : 0;
    }

    private JLS CreateAndLoad()
    {
        var journal = new JLS { GUID = Guid.NewGuid() };

        return LoadValues(journal);
    }

    private JLS LoadValues(JLS article)
    {
        article.display = chkdisplay.Checked ? "Yes" : "No";
        article.volume = Convert.ToInt32(txtvolume.Text);
        article.number = Convert.ToInt32(txtnumber.Text);
        article.articleNum = Convert.ToInt32(txtarticleNum.Text);
        article.title = txttitle.Text;
        article.authorFirst1 = txtauthorfirst.Text;
        article.authorLast1 = txtauthorlast.Text;
        article.authorFirst2 = txtauthorfirst2.Text;
        article.authorLast2 = txtauthorlast2.Text;
        article.link = txtlink.Text;

        return article;
    }

    private void Display(JLS article)
    {
        lblControl.Text = article.control.ToString();
        txttitle.Text = article.title;
        lnkGUID.Text = article.GUID.ToString();
        lnkGUID.NavigateUrl = string.Format(ARTICLE_URL_TEMPLATE, article.GUID);
        chkdisplay.Checked = article.display.ToLower().Equals("yes");
        txtvolume.Text = article.volume.ToString();
        txtnumber.Text = article.number.ToString();
        txtarticleNum.Text = article.articleNum.ToString();
        txttitle.Text = article.title;
        txtauthorfirst.Text = article.authorFirst1;
        txtauthorlast.Text = article.authorLast1;
        txtauthorfirst2.Text = article.authorFirst2;
        txtauthorlast2.Text = article.authorLast2;
        txtlink.Text = article.link; 
    }

    private static JLS GetArticle(int id)
    {
        return model.JLS.FirstOrDefault(fm => fm.control == id);
    }

    private JLS Insert()
    {
        var article = CreateAndLoad();
        model.JLS.AddObject(article);
        model.SaveChanges();

        return article;
    }

    private JLS Update()
    {
        var article = GetArticle(GetFormId());

        if (article != null)
        {
            LoadValues(article);
            model.SaveChanges();
        }

        return article;
    }

    private static string GetArticleUrl(int articleId)
    {
        return string.Format(CURRENT_PAGE_TEMPLATE, articleId);
    }

    private void SetDefaultValues()
    {
        lblControl.Text = "[new]";
    }

    #endregion
}