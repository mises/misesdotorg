﻿using System;
using System.Linq;
using Mises.Data;

public partial class Manager_periodicals_scholar : System.Web.UI.Page
{
    #region Constants

    private const string PAGE_NOT_FOUND_URL = "~/404.aspx";
    private const string CURRENT_PAGE_TEMPLATE = "scholar.aspx?id={0}";
    private const string JOURNAL_ARCHIVES_URL = "/manager/periodicals/JournalArchives.aspx?Id=7";
    private const string ERROR_PAGE_URL = "~/Error.aspx";

    #endregion

    #region Member variables

    private static readonly MisesModel model = new MisesModel();

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        string redirectUrl = null;

        try
        {
            if (IsPostBack) return;

            var id = GetQueryStringId();

            if (id == 0)
            {
                SetDefaultValues();
            }
            else
            {
                var article = GetArticle(id);

                if (article == null)
                {
                    redirectUrl = PAGE_NOT_FOUND_URL;
                }
                else
                {
                    Display(article);
                }
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        if (redirectUrl != null)
        {
            Response.Redirect(redirectUrl, true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            if (!IsValid) return;

            scholar article = IsNewArticle() ? Insert() : Update();

            redirectUrl = article == null ? PAGE_NOT_FOUND_URL : GetArticleUrl(article.control);
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            var article = GetArticle(GetFormId());

            if (article == null)
            {
                redirectUrl = PAGE_NOT_FOUND_URL;
            }
            else
            {
                model.DeleteObject(article);
                model.SaveChanges();

                redirectUrl = JOURNAL_ARCHIVES_URL;
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetArticleUrl(GetFormId()));
    }

    #endregion

    #region Private methods

    private bool IsNewArticle()
    {
        return GetFormId() == 0;
    }

    private int GetQueryStringId()
    {
        int id;
        return Int32.TryParse(Request.QueryString["Id"], out id) ? id : 0;
    }

    private int GetFormId()
    {
        int id;
        return Int32.TryParse(lblControl.Text, out id) ? id : 0;
    }

    private scholar CreateAndLoad()
    {
        var journal = new scholar();

        return LoadValues(journal);
    }

    private scholar LoadValues(scholar article)
    {
        article.display = chkdisplay.Checked ? "yes" : "no";
        article.author_name = txtauthor_name.Text;
        article.paper_name = txtpaper_name.Text;
        article.whenit = txtwhenit.Text;
        article.CreateDate = calCreateDate.SelectedDate;
        article.file_name = txtfile_name.Text;
        article.notation = txtnotation.Text;
        article.listorder = txtlistorder.Text;
        return article;
    }

    private void Display(scholar article)
    {
        lblControl.Text = article.control.ToString();

        chkdisplay.Checked = article.display.ToLower().Equals("yes");
        txtauthor_name.Text = article.author_name;
        txtpaper_name.Text = article.paper_name;
        txtwhenit.Text = article.whenit;
        calCreateDate.SelectedDate = article.CreateDate;
        txtfile_name.Text = article.file_name;
        txtnotation.Text = article.notation;
        txtlistorder.Text = article.listorder;
    }

    private static scholar GetArticle(int id)
    {
        return model.scholar.FirstOrDefault(fm => fm.control == id);
    }

    private scholar Insert()
    {
        var article = CreateAndLoad();
        model.scholar.AddObject(article);
        model.SaveChanges();

        return article;
    }

    private scholar Update()
    {
        var article = GetArticle(GetFormId());

        if (article != null)
        {
            LoadValues(article);
            model.SaveChanges();
        }

        return article;
    }

    private static string GetArticleUrl(int articleId)
    {
        return string.Format(CURRENT_PAGE_TEMPLATE, articleId);
    }

    private void SetDefaultValues()
    {
        lblControl.Text = "[new]";
        calCreateDate.SelectedValue = DateTime.Today;
    }

    #endregion
}