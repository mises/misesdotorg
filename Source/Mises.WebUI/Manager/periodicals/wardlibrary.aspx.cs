﻿using System;
using System.Linq;
using System.Web.UI;
using Mises.Data;

public partial class Manager_periodicals_wardlibrary : Page
{
    #region Constants

    private const string PAGE_NOT_FOUND_URL = "~/404.aspx";
    private const string CURRENT_PAGE_TEMPLATE = "WardLibrary.aspx?id={0}";
    private const string JOURNAL_ARCHIVES_URL = "/manager/periodicals/JournalArchives.aspx?Id=8";
    private const string ERROR_PAGE_URL = "~/Error.aspx";

    #endregion

    #region Member variables

    private static readonly MisesModel model = new MisesModel();

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        string redirectUrl = null;

        try
        {
            if (IsPostBack) return;

            var id = GetQueryStringId();

            if (id == 0)
            {
                SetDefaultValues();
            }
            else
            {
                var article = GetArticle(id);

                if (article == null)
                {
                    redirectUrl = PAGE_NOT_FOUND_URL;
                }
                else
                {
                    Display(article);
                }
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        if (redirectUrl != null)
        {
            Response.Redirect(redirectUrl, true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            if (!IsValid) return;

            WardLibrary article = IsNewArticle() ? Insert() : Update();

            redirectUrl = article == null ? PAGE_NOT_FOUND_URL : GetArticleUrl(article.control);
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        string redirectUrl;

        try
        {
            var article = GetArticle(GetFormId());

            if (article == null)
            {
                redirectUrl = PAGE_NOT_FOUND_URL;
            }
            else
            {
                model.DeleteObject(article);
                model.SaveChanges();

                redirectUrl = JOURNAL_ARCHIVES_URL;
            }
        }
        catch (Exception)
        {
            redirectUrl = ERROR_PAGE_URL;
        }

        Response.Redirect(redirectUrl, true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetArticleUrl(GetFormId()));
    }

    #endregion

    #region Private methods

    private bool IsNewArticle()
    {
        return GetFormId() == 0;
    }

    private int GetQueryStringId()
    {
        int id;
        return Int32.TryParse(Request.QueryString["Id"], out id) ? id : 0;
    }

    private int GetFormId()
    {
        int id;
        return Int32.TryParse(lblControl.Text, out id) ? id : 0;
    }

    private WardLibrary CreateAndLoad()
    {
        var journal = new WardLibrary();

        return LoadValues(journal);
    }

    private WardLibrary LoadValues(WardLibrary article)
    {
        article.title = txttitle.Text;
        article.authorfirst = txtauthorfirst.Text;
        article.authorlast = txtauthorlast.Text;
        article.authorfirst2 = txtauthorfirst2.Text;
        article.authorlast2 = txtauthorlast2.Text;
        article.authorfirst3 = txtauthorfirst3.Text;
        article.authorlast3 = txtauthorlast3.Text;

        article.display = chkdisplay.Checked ? "Yes" : "No";
        article.checked_out_by = txtchecked_out_by.Text;
        article.publisher_info = txtpublisher_info.Text;
        article.comments = txtcomments.Text;
        article.donatedBy = txtdonatedBy.Text;
        article.donatedDate = calDonatedDate.SelectedDate;
        article.coauthors = txtcoauthors.Text;
        article.subject = txtsubject.Text;
        article.CreateDate = calCreateDate.SelectedDate;
        return article;
    }

    private void Display(WardLibrary article)
    {
        lblControl.Text = article.control.ToString();
        txttitle.Text = article.title;
        txtauthorfirst.Text = article.authorfirst;
        txtauthorlast.Text = article.authorlast;
        txtauthorfirst2.Text = article.authorfirst2;
        txtauthorlast2.Text = article.authorlast2;

        txtchecked_out_by.Text = article.checked_out_by;
        txtpublisher_info.Text = article.publisher_info;
        txtcomments.Text = article.comments;
        txtdonatedBy.Text = article.donatedBy;
        
        txtcoauthors.Text = article.coauthors;
        txtsubject.Text = article.subject;

        if (article.donatedDate.HasValue) calDonatedDate.SelectedDate = article.donatedDate.Value;
        if (article.CreateDate.HasValue) calCreateDate.SelectedDate = article.CreateDate.Value;
    }

    private WardLibrary GetArticle(int id)
    {
        return model.WardLibrary.FirstOrDefault(fm => fm.control == id);
    }

    private WardLibrary Insert()
    {
        var article = CreateAndLoad();
        model.WardLibrary.AddObject(article);
        model.SaveChanges();

        return article;
    }

    private WardLibrary Update()
    {
        var article = GetArticle(GetFormId());

        if (article != null)
        {
            LoadValues(article);
            model.SaveChanges();
        }

        return article;
    }

    private static string GetArticleUrl(int articleId)
    {
        return string.Format(CURRENT_PAGE_TEMPLATE, articleId);
    }

    private void SetDefaultValues()
    {
        lblControl.Text = "[new]";
        calCreateDate.SelectedValue = DateTime.Today;
    }

    #endregion
}