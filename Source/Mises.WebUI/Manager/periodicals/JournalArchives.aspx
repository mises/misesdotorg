<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false" Inherits="JournalArchivesManager" Title="Periodical Archives Manager" Codebehind="JournalArchives.aspx.cs" %>

<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <h1><asp:Literal runat="server" ID="litHeader"></asp:Literal></h1>
        <a href="#" runat="server" id="lnkImg">
            <img id="imgLogo" runat="server" class="leftx" src="" alt="" />
        </a>
        <%--<asp:Literal ID="lblDescription" runat="server"></asp:Literal>--%>
        <ul style="float: right">
            <li><a href="/manager/periodicals/misesreview.aspx">Add New Review</a></li>
            <li><a href="/manager/periodicals/qjae.aspx">Add New QJAE Article</a></li>
            <li><a href="/manager/periodicals/freemarket.aspx">Add New Free Market Article</a></li>
            <li><a href="/manager/periodicals/jls.aspx">Add New Journal of Libertarian Studies</a></li>
            <li><a href="/manager/periodicals/rae.aspx">Add New Review of Austrian Economics Article</a></li>
            <li><a href="/manager/periodicals/aen.aspx">Add New Austrian Economics Newsletter</a></li>
            <li><a href="/manager/periodicals/scholar.aspx">Add New Scholar Article</a></li>
            <li><a href="/manager/periodicals/wardlibrary.aspx">Add Book to Ward and Massey Library</a></li>
        </ul>
    </div>
    <div style="clear: both">
        <hr />
        <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
            <h4><span class="search">Search: </span></h4>
            <asp:Label ID="Label1" runat="server" AssociatedControlID="txtTitle" Text="Title"></asp:Label>
            <asp:TextBox ID="txtTitle" type="text" size="20" runat="server"></asp:TextBox>
            <asp:Label ID="Label2" runat="server" AssociatedControlID="txtAuthor" Text="Author"></asp:Label>
            <asp:TextBox ID="txtAuthor" type="text" size="20" runat="server"></asp:TextBox>
            <asp:Label ID="lblSubject" runat="server" AssociatedControlID="txtsubject" Text="Subject"
                       Visible="False"></asp:Label>
            <asp:TextBox ID="txtSubject" type="text" size="20" runat="server" Visible="False"></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" Text="Search Journal" AccessKey="s" EnableViewState="False" CssClass="ui-state-default ui-corner-all ui-button"
                        ToolTip="Search the Periodicals" Width="146px"></asp:Button>
        </div>
    </div>
    <h3>
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </h3>
    <br />
    <cc1:GridView PagerStyle-CssClass="pager" ID="gvMonthlyArchives" runat="server" PageSize="100"
                  DataKeyNames="Month" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False"
                  Width="100%" EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
                  SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title (Edit)" SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.title")%>'
                                   NavigateUrl='<%#String.Format("{0}.aspx?Id={1}", GetPageName(), Eval("Id"))%>'> </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="AuthorLast">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%#DataBinder.Eval(Container, "DataItem.PeriodicalId")%>&amp;search=<%#DataBinder.Eval(Container, "DataItem.AuthorLast")%>">
                        <%#DataBinder.Eval(Container, "DataItem.AuthorName")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DatePosted" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"
                            SortExpression="DatePosted">
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
        </Columns>
        <PagerStyle CssClass="pager"></PagerStyle>
    </cc1:GridView>
    <cc1:GridView PagerStyle-CssClass="pager" ID="gvSeasonalArchives" runat="server"
                  DataKeyNames="Season" PageSize="100" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False"
                  Width="100%" EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
                  SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.title")%>'
                                   NavigateUrl='<%#String.Format("{0}.aspx?Id={1}", GetPageName(), Eval("Id"))%>'> </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="AuthorLast">
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%#DataBinder.Eval(Container, "DataItem.PeriodicalId")%>&amp;author=<%#DataBinder.Eval(Container, "DataItem.AuthorLast")%>">
                        <%#DataBinder.Eval(Container, "DataItem.AuthorName")%>
                    </a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Volume" SortExpression="Volume">
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%#DataBinder.Eval(Container, "DataItem.PeriodicalId")%>&amp;volume=<%#DataBinder.Eval(Container, "DataItem.Volume")%>">
                        <%#DataBinder.Eval(Container, "DataItem.Volume")%>
                    </a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
        </Columns>
    </cc1:GridView>
    <cc1:GridView PagerStyle-CssClass="pager" ID="gvLibrary" runat="server" PageSize="100"
                  AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
                  EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
                  SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <a href="wardlibrary.aspx?Id=<%#DataBinder.Eval(Container, "DataItem.Id")%>">
                    <%#DataBinder.Eval(Container, "DataItem.Title")%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="AuthorLast">
                <ItemTemplate>
                    <a href="periodical.aspx?Id=<%#DataBinder.Eval(Container, "DataItem.PeriodicalId")%>&amp;search=<%#DataBinder.Eval(Container, "DataItem.AuthorName")%>">
                        <%#DataBinder.Eval(Container, "DataItem.AuthorName")%>
                    </a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:BoundField DataField="DatePosted" ReadOnly="True" HeaderText="Date Added" SortExpression="DatePosted"
                            HtmlEncode="False" DataFormatString="{0:d}">
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
        </Columns>
    </cc1:GridView>
</asp:Content>