<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="QJAE Editor"
         MaintainScrollPositionOnPostback="true"  ValidateRequest="false" Inherits="QJAEpage" Codebehind="qjae.aspx.cs" %>

<%@ Register Src="../ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register src="../ManagerControls/AuthorSelect.ascx" tagname="AuthorSelect" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3>
        <asp:Label ID="lblTitle" runat="server">Edit QJAE</asp:Label><br />
    </h3>
    <asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
                CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
    <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
    <asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this Story?');"
                runat="server" Text="Delete" CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
    <br />
    <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" Font-Bold="True"
               ForeColor="Red"></asp:Label>
    <table style="margin-right: auto; margin-left: auto"  border="0" width="800">
        <tr>
            <td valign="top" class="detailheader">
                ID:
                <asp:Label ID="lblStoryId" runat="server" Font-Bold="True">0</asp:Label>
            </td>
            <td class="detailtext">
                Volume:
                <asp:TextBox ID="txtVolume" runat="server"></asp:TextBox>
                Number:
                <asp:TextBox ID="txtNumber" runat="server"></asp:TextBox>
                &nbsp;Article Number:<asp:TextBox ID="txtArticleNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Date
            </td>
            <td class="detailtext">
                <ew:CalendarPopup ID="calStoryDate" runat="server">
                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                  BackColor="White"></WeekdayStyle>
                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                      ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                   BackColor="AntiqueWhite"></OffMonthStyle>
                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                   BackColor="LightGoldenrodYellow"></TodayDayStyle>
                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                  BackColor="LightGray"></WeekendStyle>
                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                       ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
					
                </ew:CalendarPopup>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Title
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtTitle" runat="server" Columns="80"></asp:TextBox>
            </td>
        </tr>
		
        <tr>
            <td valign="top" class="detailheader">
                Authors
            </td>
            <td class="detailtext">
                <uc2:AuthorSelect ID="AuthorSelect1" runat="server" />
            </td>
        </tr>
		
        <tr>
            <td valign="top" align="left" class="detailheader">
                (Old Field) First Author
            </td>
            <td align="left" class="detailtext">
                First:
                <asp:TextBox ID="txt1First" runat="server"></asp:TextBox>
                &nbsp;Last:
                <asp:TextBox ID="txt1Last" runat="server"></asp:TextBox>
			    
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" class="detailheader">
                (Old Field) Second Author
            </td>
            <td align="left" class="detailtext">
                First:
                <asp:TextBox ID="txt2First" runat="server"></asp:TextBox>
                &nbsp;Last:
                <asp:TextBox ID="txt2Last" runat="server"></asp:TextBox>			    
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                Upload PDF</td>
            <td align="left" class="detailtext">
                <asp:FileUpload ID="fileUploadFile" runat="server" 
                                CssClass="ui-state-default ui-corner-all ui-button" />
                <asp:Button ID="btnUploadFile" runat="server" CssClass="ui-state-default ui-corner-all ui-button" 
                            Text="Upload Article" onclick="btnUploadFile_Click" />
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                GUID
            </td>
            <td align="left" class="detailtext">
                <asp:HyperLink ID="lnkGUID" runat="server"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2" class="detailheader">
                <div id="DailyStory">
                    <!-- TODO: FIX to relative URL -->
                </div>
            </td>
        </tr>
        <tr>
            <td class="detailheader" style="font-weight: bold" valign="top">
                Edited:
            </td>
            <td class="detailtext">
                <asp:Label runat="server" ID="lblEditedBy"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Tags
            </td>
            <td class="detailtext">
                <uc1:DocumentTags ID="DocumentTags1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>