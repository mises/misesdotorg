<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="Untitled Page" Inherits="menu" Codebehind="~/Manager/menu.aspx.cs" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    &nbsp;<h2>
              Mises Main Menu</h2>
    <br />
    <br />
    <h4>
        Menu Items:</h4><br />
    <br />
    <strong><span style="font-size: 10pt; color: #000000">Title</span><asp:TextBox
                                                                          ID="txtTitle" runat="server"></asp:TextBox>URL:
        <asp:TextBox ID="txtURL" runat="server"></asp:TextBox>Section:<asp:TextBox ID="txtSection"
                                                                                   runat="server"></asp:TextBox><asp:Button ID="btnNewMenu" runat="server" OnClick="btnNewMenu_Click"
                                                                                                                            Text="Add Item" /></strong>
    <p>

    </p>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="menuId" DataSourceID="MainMenu"
                  ForeColor="#333333" GridLines="None">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="menuId" HeaderText="menuId" InsertVisible="False" ReadOnly="True"
                            SortExpression="menuId" Visible="False" />
            <asp:BoundField DataField="menuTitle" HeaderText="menuTitle" SortExpression="menuTitle" />
            <asp:BoundField DataField="menuURL" HeaderText="menuURL" SortExpression="menuURL" />
            <asp:TemplateField HeaderText="menuSection" SortExpression="menuSection">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("menuSection")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SectionName" DataTextField="SectionName"
                                      DataValueField="SectionId" SelectedValue='<%#Bind("menuSection")%>'>
                    </asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
        </Columns>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="MainMenu" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [MainMenu] WHERE [menuId] = @original_menuId" InsertCommand="INSERT INTO [MainMenu] ([menuTitle], [menuURL], [menuSection]) VALUES (@menuTitle, @menuURL, @menuSection)"
                       SelectCommand="SELECT [menuTitle], [menuId], [menuURL], [menuSection] FROM [MainMenu] ORDER BY [menuSection], [menuTitle]"
                       UpdateCommand="UPDATE [MainMenu] SET [menuTitle] = @menuTitle, [menuURL] = @menuURL, [menuSection] = @menuSection WHERE [menuId] = @original_menuId">
        <DeleteParameters>
            <asp:Parameter Name="original_menuId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="menuTitle" Type="String" />
            <asp:Parameter Name="menuURL" Type="String" />
            <asp:Parameter Name="menuSection" Type="String" />
            <asp:Parameter Name="original_menuId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="menuTitle" Type="String" />
            <asp:Parameter Name="menuURL" Type="String" />
            <asp:Parameter Name="menuSection" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />
    <h4>
        Menu Sections:</h4>
    &nbsp;&nbsp;
    <asp:GridView ID="GridView2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                  CellPadding="4" DataKeyNames="SectionId" DataSourceID="SectionName" ForeColor="#333333"
                  GridLines="None">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="SectionId" HeaderText="SectionId" ReadOnly="True" SortExpression="SectionId" />
            <asp:BoundField DataField="SectionName" HeaderText="SectionName" SortExpression="SectionName" />
            <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
        </Columns>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="SectionName" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [MenuSections] WHERE [SectionId] = @original_SectionId"
                       InsertCommand="INSERT INTO [MenuSections] ([SectionName]) VALUES (@SectionName)"
                       SelectCommand="SELECT [SectionId], [SectionName] FROM [MenuSections]" UpdateCommand="UPDATE [MenuSections] SET [SectionName] = @SectionName WHERE [SectionId] = @original_SectionId">
        <DeleteParameters>
            <asp:Parameter Name="original_SectionId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="SectionName" Type="String" />
            <asp:Parameter Name="original_SectionId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="SectionName" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    &nbsp;
</asp:Content>