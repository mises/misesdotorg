﻿#region

using System;
using System.Data;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Domain.Old;

#endregion

partial class review : Page
{
    private void Page_Load(object sender, EventArgs e)
    {
        if (! Page.IsPostBack)
        {
            int reviewId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

            if (reviewId == 0)
            {
                lblTitle.Text = "Add New Daily review";
                lblReviewId.Text = "0";
                return;
            }

            lblReviewId.Text = reviewId.ToString();
            var review = new BookReview();
            DataTable ds = review.GetReview(reviewId);

            foreach (DataRow row in ds.Rows)
            {
                lblTitle.Text = "Editing: " + Convert.ToString(row["title"]) + " Note: Cannot save changes yet!";
                txtTitle.Text = Convert.ToString(row["title"]);
                if (! (Convert.IsDBNull(row["Review"])))
                {
                    txtReview.Text = Convert.ToString(row["Review"]);
                }
                if (! (Convert.IsDBNull(row["ScreenName"])))
                {
                    txtScreenName.Text = Convert.ToString(row["ScreenName"]);
                }
                if (! (Convert.IsDBNull(row["Location"])))
                {
                    txtLocation.Text = Convert.ToString(row["Location"]);
                }
                try
                {
                    calreviewDate.SelectedDate = row.Field<DateTime>("ReviewDate");
                }
                catch (Exception)
                {
                    calreviewDate.SelectedDate = DateTime.Now;
                }
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // Save reviews      
        //BookReview review = new BookReview();
        //            .ReviewId = CInt(Val(lblReviewId.Text))
        //           .title = txtTitle.Text.Trim
        //            .Review = txtReview.Text
        //           .First = txtScreenName.Text.Trim
        //            .Last = txtLocation.Text.Trim
        //            .display = chkVisible.Checked
        //            .type = ddDisplayType.SelectedValue
        //            .DatePosted = calreviewDate.SelectedDate
        //            Me.lblReviewId.Text = .Savereview().ToString

        lblTitle.Text = "review Saved.<br /><a href=default.aspx>Back to Index</a>";
    }

    public void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("default.aspx");
    }
}