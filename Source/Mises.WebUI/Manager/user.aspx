﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="true"
    CodeBehind="user.aspx.cs" Inherits="Mises.Web.Manager.user" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        My Status:
        <asp:LoginName ID="LoginName2" runat="server" />
        <asp:LoginStatus ID="LoginStatus2" runat="server" />
    </div>
    <div>
    Create Account:
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server">
        <WizardSteps>
            <asp:CreateUserWizardStep runat="server" />
            <asp:CompleteWizardStep runat="server" />
        </WizardSteps>
    </asp:CreateUserWizard>

    </div>
    <div>
    Change Password:
    <asp:ChangePassword ID="ChangePassword1" runat="server">
    </asp:ChangePassword>
    </div>
    <div>
    Password recovery:
    <asp:PasswordRecovery ID="PasswordRecovery1" runat="server">
        <MailDefinition From="webmaster@freecapitalists.org">
        </MailDefinition>
    </asp:PasswordRecovery>
    </div>
</asp:Content>
