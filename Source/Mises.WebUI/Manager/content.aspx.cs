#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Documents;

#endregion

partial class ContentManagerPage : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (! Page.IsPostBack)
        {
            Bind();
        }
    }

    public void Bind()
    {
        string action = Request.QueryString["action"];
        string source = Request.QueryString["source"];
        int id = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        Bind(action, id, source);
    }

    public void Bind(string action, int Id, string Source)
    {
        if (txtSearchQuery.Text != "")
        {
            action = "search";
            Source = txtSearchQuery.Text;
        }
        DataTable dt = null;
        string title = "";
        if (! string.IsNullOrEmpty(action))
        {
            action = action.ToLower();
        }

        if (Id > 0 || !string.IsNullOrEmpty(Source))
        {
            if (string.IsNullOrEmpty(action))
            {
                dt =
                    SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                             "dbo.DocumentAdminLinks").Tables[0];
            }
            else
                switch (action)
                {
                    case "title":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.DocumentAdminLinks").Tables[0];
                        title = "Browse by title";
                        break;
                    case "subject":
                        {
                            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                                  CommandType.StoredProcedure,
                                                                  "dbo.DocumentAdminLinks",
                                                                  new SqlParameter("@SubjectId", Id));
                            dt = ds.Tables[0];
                            title = "Browse by subject: " + ds.Tables[1].Rows[0][0];
                        }
                        break;
                    case "author":
                        {
                            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                                  CommandType.StoredProcedure,
                                                                  "dbo.DocumentAdminLinks",
                                                                  new SqlParameter("@AuthorId", Id));
                            dt = ds.Tables[0];
                            title = "Browse by author: " + ds.Tables[1].Rows[0][0];
                        }
                        break;
                    case "source":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.DocumentAdminLinks", new SqlParameter("@Source", Source)).
                                Tables[0];
                        title = "Browse by source: " + Source;
                        break;
                    case "mediatype":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.DocumentAdminLinks", new SqlParameter("@MediaType", Id)).
                                Tables[0];
                        title = "Browse by Media Type";
                        break;
                    case "search":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.DocumentAdminLinks", new SqlParameter("@SearchQuery", Source))
                                .Tables[
                                    0];
                        title = dt.Rows.Count + " matches found.";
                        break;
                }
            if (ViewState["SortExpression"] != null && ViewState["SortExpression"].ToString() != "")
            {
                dt.DefaultView.Sort = ViewState["SortExpression"] + ViewState["SortDirection"].ToString();
            }
            gvLiterature.DataSource = dt;
            gvLiterature.DataBind();
        }
        else if (! string.IsNullOrEmpty(action)) // No particular Id
        {
            DataTable dt2 = null;
            switch (action)
            {
                case "subject":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentSubjectsGetList").Tables[0];
                    gvSubjects.DataSource = dt2;
                    gvSubjects.DataBind();
                    title = "Browse by subject";
                    break;
                case "austrian":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentSubjectsGetList", new SqlParameter("@CategoryId", 1)).
                            Tables[0
                            ];
                    gvSubjects.DataSource = dt2;
                    gvSubjects.DataBind();
                    title = "Austrian Economics";
                    break;
                case "libertarian":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentSubjectsGetList", new SqlParameter("@CategoryId", 4)).
                            Tables[0
                            ];
                    gvSubjects.DataSource = dt2;
                    gvSubjects.DataBind();
                    title = "Libertarian Studies";
                    break;
                case "jel":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentSubjectsGetList SortOrder",
                                                 new SqlParameter("@CategoryId", 3))
                            .Tables[0];
                    gvSubjects.DataSource = dt2;
                    gvSubjects.DataBind();
                    title = "Journal of Economic Literature";
                    break;
                case "author":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentAuthorGetList",
                                                 new SqlParameter("@ContentType", "Literature")).Tables[0];
                    gvAuthors.DataSource = dt2;
                    gvAuthors.DataBind();
                    title = "Browse by Literature Author";
                    break;
                case "source":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentGetSourceJournalList").Tables[0];
                    gvSource.DataSource = dt2;
                    gvSource.DataBind();
                    title = "Browse by source";
                    break;
                case "type":
                    dt2 =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentMediaTypeGetList").Tables[0];
                    gvMediaType.DataSource = dt2;
                    gvMediaType.DataBind();
                    title = "Browse by media type";
                    break;
                default:
                    gvLiterature.DataSource =
                        SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                 "dbo.DocumentAdminLinks").Tables[0];
                    gvLiterature.DataBind();
                    title = "Browse by title";
                    break;
            }
        }
        else
        {
            dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                         "dbo.DocumentAdminLinks").Tables[0];
            if (ViewState["SortExpression"] != null && ViewState["SortExpression"].ToString() != "")
            {
                dt.DefaultView.Sort = ViewState["SortExpression"] + ViewState["SortDirection"].ToString();
            }
            gvLiterature.DataSource = dt.DefaultView;
            gvLiterature.DataBind();
        }
        lblTitle.Text = title;
    }

    protected void btnAskTheGuide_Click(object sender, EventArgs e)
    {
        // Search the guide
        ViewState["SortExpression"] = "";
        ViewState["SortDirection"] = "";
        gvLiterature.PageIndex = 0;
        Bind("search", 0, txtSearchQuery.Text.Trim());
    }


    protected void dgStudyGuide_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvLiterature.PageIndex = e.NewPageIndex;
        Bind();
    }

    protected void gvLiterature_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        // Set URL
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //if (! (Convert.IsDBNull(((DataRowView) e.Row.DataItem).Row["FullText"])))
            //{
            //    if (Convert.ToString(((DataRowView) e.Row.DataItem).Row["FullText"]) == "True")
            //    {
            //        ((HyperLink) (e.Row.Cells[2].FindControl("link"))).NavigateUrl =
            //            Request.Url.Scheme + "://" + Request.Url.Authority + "/web/" +
            //            Convert.ToInt32(((DataRowView) e.Row.DataItem).Row["DocumentId"]);
            //    }
            //}
        }
    }

    protected void gvLiterature_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["SortExpression"] = e.SortExpression;
        if (e.SortDirection == SortDirection.Ascending)
        {
            ViewState["SortDirection"] = " ASC";
        }
        else
        {
            ViewState["SortDirection"] = " DESC";
        }
        Bind();
    }

    private void HideTables()
    {
        gvLiterature.Visible = false;
        gvSubjectEdit.Visible = false;
        gvCategoryEdit.Visible = false;
        gvAuthorEdit.Visible = false;
    }

    protected void btnEditAuthors_Click(object sender, EventArgs e)
    {
        HideTables();
        gvAuthorEdit.Visible = true;
        pnlNewAuthor.Visible = true;
    }

    protected void btnEditSubjects_Click(object sender, EventArgs e)
    {
        HideTables();
        gvSubjectEdit.Visible = true;
    }

    protected void btnEditCategories_Click(object sender, EventArgs e)
    {
        HideTables();
        gvCategoryEdit.Visible = true;
    }

    protected void btnAddAuthor_Click(object sender, EventArgs e)
    {
        if (DocumentAuthors.AddNewAuthor(txtFirstName.Text, txtMiddleName.Text, txtLastName.Text))
        {
            btnAddAuthor.Text = "Done!";
            Bind();
            gvAuthors.Visible = true;
        }
        else
        {
            btnAddAuthor.Text = "Error adding author!";
        }
    }

    protected void btnDeleteAuthor_Click(object sender, EventArgs e)
    {
        int rowsChanged = DocumentAuthors.ReplaceAuthor(Convert.ToInt32(txtOldId.Text), Convert.ToInt32(txtNewId.Text));
        btnDeleteAuthor.Text = rowsChanged + " rows changed";
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        btnAskTheGuide.Click += btnAskTheGuide_Click;
        gvLiterature.PageIndexChanging += dgStudyGuide_PageIndexChanging;
        gvLiterature.RowDataBound += gvLiterature_RowDataBound;
        gvLiterature.Sorting += gvLiterature_Sorting;
        btnEditAuthors.Click += btnEditAuthors_Click;
        btnEditSubjects.Click += btnEditSubjects_Click;
        btnEditCategories.Click += btnEditCategories_Click;
        btnAddAuthor.Click += btnAddAuthor_Click;
        btnDeleteAuthor.Click += btnDeleteAuthor_Click;
    }
}