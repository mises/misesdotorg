<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="Media Category Editor" 
         ValidateRequest="false" MaintainScrollPositionOnPostback="true"
         Inherits="Manager.MediaCategory" Codebehind="~/Manager/MediaCategory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>Media Categories</h2>
    &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                             DeleteCommand="DELETE FROM [MediaCategory] WHERE [CategoryId] = @CategoryId"
                             InsertCommand="INSERT INTO [MediaCategory] ([ParentCategory], [Category], [Description], [CategoryImage], [iTunesCategoryCode], [CreateDate], [SortOrder]) VALUES (@ParentCategory, @Category, @Description, @CategoryImage, @iTunesCategoryCode, @CreateDate, @SortOrder)"
                             SelectCommand="SELECT * FROM [MediaCategory]"
		
                             UpdateCommand="UPDATE [MediaCategory] SET [ParentCategory] = @ParentCategory, [Category] = @Category, [Description] = @Description, [CategoryImage] = @CategoryImage, [iTunesCategoryCode] = @iTunesCategoryCode, [CreateDate] = @CreateDate, [SortOrder] = @SortOrder WHERE [CategoryId] = @CategoryId">
              <DeleteParameters>
                  <asp:Parameter Name="CategoryId" Type="Int32" />
              </DeleteParameters>
              <UpdateParameters>
                  <asp:Parameter Name="ParentCategory" Type="Int32" />
                  <asp:Parameter Name="Category" Type="String" />
                  <asp:Parameter Name="Description" Type="String" />
                  <asp:Parameter Name="CategoryImage" Type="String" />
                  <asp:Parameter Name="iTunesCategoryCode" Type="String" />
                  <asp:Parameter Name="CreateDate" Type="DateTime" />
                  <asp:Parameter Name="SortOrder" Type="Int32" />
                  <asp:Parameter Name="CategoryId" Type="Int32" />
              </UpdateParameters>
              <InsertParameters>
                  <asp:Parameter Name="ParentCategory" Type="Int32" />
                  <asp:Parameter Name="Category" Type="String" />
                  <asp:Parameter Name="Description" Type="String" />
                  <asp:Parameter Name="CategoryImage" Type="String" />
                  <asp:Parameter Name="iTunesCategoryCode" Type="String" />
                  <asp:Parameter Name="CreateDate" Type="DateTime" />
                  <asp:Parameter Name="SortOrder" Type="Int32" />
              </InsertParameters>
          </asp:SqlDataSource>
    <asp:DetailsView ID="DetailsView1" runat="server" CellPadding="4" ForeColor="#333333"
                     GridLines="None" Height="50px" Width="125px" AutoGenerateRows="False" DataKeyNames="CategoryId" DataSourceID="SqlDataSource1" OnItemInserted="DetailsView1_ItemInserted" AllowPaging="True">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <RowStyle BackColor="#EFF3FB" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <EmptyDataTemplate>
            <asp:Button ID="btnNewCategory" runat="server" OnClick="btnNewCategory_Click" Text="Add New Category" />
        </EmptyDataTemplate>
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
        <Fields>
            <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
            <asp:BoundField DataField="ParentCategory" HeaderText="Parent Category" SortExpression="ParentCategory" />
            <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" InsertVisible="False"
                            ReadOnly="True" SortExpression="CategoryID" />
            <asp:CommandField ButtonType="Button" ShowDeleteButton="True" ShowEditButton="True"
                              ShowInsertButton="True" HeaderText="Edit">
                <ControlStyle Font-Bold="True" />
            </asp:CommandField>
            <asp:BoundField DataField="CategoryImage" HeaderText="CategoryImage" 
                            SortExpression="CategoryImage" />
            <asp:BoundField DataField="iTunesCategoryCode" HeaderText="iTunesCategoryCode" 
                            SortExpression="iTunesCategoryCode" />
            <asp:BoundField DataField="SortOrder" HeaderText="SortOrder" 
                            SortExpression="SortOrder" />
        </Fields>
    </asp:DetailsView>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                  AutoGenerateColumns="False" DataKeyNames="CategoryId" DataSourceID="sqlCategory" 
                  UseAccessibleHeader="False" Width="683px" CellPadding="4" 
                  ForeColor="#333333" GridLines="None" PageSize="250">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                              HeaderText="Edit" ButtonType="Button" >
                <ItemStyle Font-Bold="True" Font-Size="Larger" />
            </asp:CommandField>
            <asp:BoundField DataField="CategoryID" HeaderText="ID#" InsertVisible="False"
                            SortExpression="CategoryID" ReadOnly="True" />
            <asp:TemplateField HeaderText="Category" SortExpression="Category">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("Category")%>' Width="304px"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("Category")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Rows="8" Text='<%#Bind("Description")%>'
                                 TextMode="MultiLine" Width="409px"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Description")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="iTunesCategoryCode" HeaderText="iTunesCategoryCode" 
                            SortExpression="iTunesCategoryCode" />
            <asp:BoundField DataField="SortOrder" HeaderText="SortOrder" 
                            SortExpression="SortOrder" />
            <asp:BoundField DataField="CategoryImage" HeaderText="CategoryImage" 
                            SortExpression="CategoryImage" />
            <asp:BoundField DataField="ParentCategory" HeaderText="Parent Category" SortExpression="ParentCategory" />
        </Columns>
        <PagerSettings Position="TopAndBottom" PageButtonCount="35" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="sqlCategory" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [MediaCategory] WHERE [CategoryId] = @CategoryId"
                       InsertCommand="INSERT INTO [MediaCategory] ([ParentCategory], [Category], [Description], [CategoryImage], [iTunesCategoryCode], [CreateDate], [SortOrder]) VALUES (@ParentCategory, @Category, @Description, @CategoryImage, @iTunesCategoryCode, @CreateDate, @SortOrder)"
                       SelectCommand="SELECT * FROM [MediaCategory]"
		
                       UpdateCommand="UPDATE [MediaCategory] SET [ParentCategory] = @ParentCategory, [Category] = @Category, [Description] = @Description, [CategoryImage] = @CategoryImage, [iTunesCategoryCode] = @iTunesCategoryCode, [CreateDate] = @CreateDate, [SortOrder] = @SortOrder WHERE [CategoryId] = @CategoryId">
        <DeleteParameters>
            <asp:Parameter Name="CategoryId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ParentCategory" Type="Int32" />
            <asp:Parameter Name="Category" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="CategoryImage" Type="String" />
            <asp:Parameter Name="iTunesCategoryCode" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
            <asp:Parameter Name="CategoryId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="ParentCategory" Type="Int32" />
            <asp:Parameter Name="Category" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="CategoryImage" Type="String" />
            <asp:Parameter Name="iTunesCategoryCode" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
    &nbsp;&nbsp;&nbsp;
</asp:Content>