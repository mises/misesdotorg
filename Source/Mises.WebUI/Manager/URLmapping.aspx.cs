#region

using System;
using System.Web.UI;

#endregion

partial class Manager_URLmapping : Page
{
    protected void btnAddRedirect_Click(object sender, EventArgs e)
    {
        sqlRedirectedURL.InsertParameters["RedirectURL"].DefaultValue = txtRedirectURL.Text;
        sqlRedirectedURL.InsertParameters["RequestedURL"].DefaultValue = txtRequestedURL.Text;
        sqlRedirectedURL.InsertParameters["Priority"].DefaultValue = 0.ToString();
        sqlRedirectedURL.InsertParameters["ExactMatchOnly"].DefaultValue = false.ToString();
        sqlRedirectedURL.Insert();

        txtRedirectURL.Text = string.Empty;
        txtRequestedURL.Text = string.Empty;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        btnAddRedirect.Click += btnAddRedirect_Click;
    }
}