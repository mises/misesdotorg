﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

public partial class Manager_book : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (IsPostBack) return;

        if (Request.QueryString["Id"] == null)
        {
            DetailsView1.ChangeMode(DetailsViewMode.Insert);
        }

        DetailsView1.ItemInserted += DetailsView1_ItemInserted;
        DetailsView1.ItemInserting += DetailsView1_ItemInserting;
    }

    private void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
    }

    private void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        Response.Redirect("library.aspx");
    }
}