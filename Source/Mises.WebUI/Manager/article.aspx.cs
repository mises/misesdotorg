#region

using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Utility;

using DailyArticle = Mises.Domain.Articles.DailyArticle;
using Document = Mises.Domain.Documents.Document;
using d = Mises.Domain.Documents;

#endregion

public partial class article : Page
{
    private void Page_Load(object sender, EventArgs e)
    {
        btnSave.Click += btnSave_Click;
        Cancel.Click += Cancel_Click;
        btnDelete.Click += btnDelete_Click;
        btnTidyHTML.Click += btnTidyHTML_Click;
        RevisionHistory1.ShowRevisionEvent += ShowRevision;

        if (Page.IsPostBack) return;

        int ArticleId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

        if (ArticleId == 0)
        {
            litTitle.Text = "New Mises Daily Article";
            lnkArticleId.Text = "0";
            calArticleDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
        }
        else
        {
            imgPhoto.ImageUrl = string.Format("/images/DailyArticleBigImages/{0}.jpg", ArticleId);
            imgThumbnail.ImageUrl = string.Format("/images/DailyArticleImages/{0}.jpg", ArticleId);


            var Article = new DailyArticle(ArticleId);

            lnkArticleId.Text = ArticleId.ToString();
            lnkArticleId.NavigateUrl = string.Format("/preview/{0}/{1}", ArticleId, Article.Title.ToSlug());

            litTitle.Text = "Editing: <em>" + Article.Title + "</em>";
            Page.Title = litTitle.Text;
            txtTitle.Text = Article.Title;

            txtHeadlineText.Text = Article.HeadlineText;

            txtIntro.Text = Article.Description;

            chkFeatured.Checked = Article.Featured;
            chkHeadline.Checked = Article.Headline;

            if (Article.AuthorId > 0)
            {
                AuthorSelect1.Author1 = Article.AuthorId;
            }
            if (Article.CoAuthorId > 0)
            {
                AuthorSelect1.Author2 = Article.CoAuthorId;
            }

            //lnkGUID.Text = Article.Identifier.ToString();
            //lnkGUID.NavigateUrl = string.Format("/about/{0}", Article.Identifier);

            lnkURL.NavigateUrl = string.Format("/daily/{0}/{1}", ArticleId, Article.Title.ToSlug());
            lnkURL.Text = lnkURL.NavigateUrl;

            bool display = Article.ShowArticle;
            txtFullText.Text = Article.Contents;
            chkVisible.Checked = display;
            txtPhotoHeight.Text = Article.PhotoHeight.ToString();

            txtPhotoUrl.Text = Article.PhotoURL;
            DocumentTags1.DocumentIdentifier = Article.Identifier;

            RevisionHistory1.DocumentGUID = Article.Identifier;
            lblEditedBy.Text = Article.EditedBy;

            try
            {
                calArticleDate.Text = Article.DatePosted.ToString("MM/dd/yyyy");
            }
            catch (Exception)
            {
                calArticleDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            }
        }
    }

    private void ShowRevision(RevisionGetByRevisionIdResult save)
    {
        txtFullText.Text = save.DocumentText;
        txtFullText.ReadOnly = true;
        lblErrorMessage.Text = String.Format("Viewing revision by {0} saved on {1}", save.Editor, save.EditDate);
        Page.Title = litTitle.Text;
    }

    public void btnSave_Click(object sender, EventArgs e)
    {
        // Save Articles

        //If txtIntro.Text.Contains("<img") Then
        //    Throw New Exception("Introduction text cannot contain image urls - use the Photo URL field!")
        //End If

        var article = new DailyArticle
                          {
                              ArticleId = Convert.ToInt32(Conversion.Val(lnkArticleId.Text)),
                              Title = txtTitle.Text.Trim(),
                              HeadlineText = txtHeadlineText.Text,
                              Description = txtIntro.Text,
                              Contents = txtFullText.Text,
                              ShowArticle = chkVisible.Checked,
                              DatePosted = DateTime.Parse(calArticleDate.Text),
                              AuthorId = AuthorSelect1.Author1,
                              CoAuthorId = AuthorSelect1.Author2,
                              Featured = chkFeatured.Checked,
                              Headline = chkHeadline.Checked,
                              PhotoURL = txtPhotoUrl.Text,
                              ThumbnailURL = txtThumbnailUrl.Text,
                              PhotoHeight = Convert.ToInt32(Conversion.Val(txtPhotoHeight.Text))
                          };


        //SaveArticleHeaderImages(article.ArticleId, article.PhotoURL, article.ThumbnailURL, article.PhotoHeight);

        string Editor = null;
        Editor = Context.User.Identity.IsAuthenticated ? Context.User.Identity.Name : Request.GetOriginalHostAddress();


        lnkArticleId.Text = article.SaveArticle(Editor).ToString();

        lnkURL.NavigateUrl = string.Format("/daily/{0}/{1}", article.ArticleId, article.Title.ToSlug());
        lnkURL.Text = lnkURL.NavigateUrl;

        UploadDailyPhotos(article.ArticleId);

        lnkArticleId.NavigateUrl = string.Format("/preview/{0}/{1}", lnkArticleId.Text, article.Title.ToSlug());

        litTitle.Text = "Editing: <em>" + article.Title + "</em>";
        lblErrorMessage.Text = "Article Saved.<br /><a href=articles.aspx>Back to Index</a>";
    }

    private void UploadDailyPhotos(int articleId)
    {
        try
        {
            if (articleId == 0) return;

            if (uploadPhotoUrl.HasFile)
            {
                txtPhotoUrl.Text = uploadPhotoUrl.FileName;

                string filename = Server.MapPath(string.Format(@"\images\DailyArticleBigImages\{0}.jpg", articleId));
                if (File.Exists(filename))
                    File.Delete(filename);
                uploadPhotoUrl.SaveAs(filename);
            }

            if (uploadThumbnail.HasFile)
            {
                txtThumbnailUrl.Text = uploadThumbnail.FileName;

                string filename = Server.MapPath(string.Format(@"\images\DailyArticleImages\{0}.jpg", articleId));
                if (File.Exists(filename))
                    File.Delete(filename);
                uploadThumbnail.SaveAs(filename);
            }
            else
            {
                // Create thumbnail if it is missing
                if (uploadPhotoUrl.HasFile)
                {
                    string thumbPath = Server.MapPath(string.Format(@"\images\DailyArticleImages\{0}.jpg", articleId));
                    //if (!System.IO.File.Exists(filename))
                    //{

                    var image = new Bitmap(uploadPhotoUrl.PostedFile.InputStream);
                    ;

                    //    int photoHeight = int.Parse(txtPhotoHeight.Text);
                    Imaging.CreateProportionalImageThumbnail(image, thumbPath);
                    // }
                }
            }

            if (uploadPhotoUrl.HasFile)
            {
                // update name in db

                var db =
                    new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
                var article = from a in db.DailyArticleTBs where a.ArticleId == articleId select a;
                article.First().PhotoURL = uploadPhotoUrl.FileName;
                db.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text += "Error saving photo: " + ex.Message;
            ExceptionLogging.LogException(Context,ex);
        }
    }


    /// <summary>
    ///   Gets the thumbnail.  Generates if it doesn't exist.
    /// </summary>
    /// <param name = "ArticleId">The article id.</param>
    /// <param name = "ImageURL">The image URL.</param>
    private void SaveArticleHeaderImages(int ArticleId, string photoUrl, string thumbnailUrl, int Height)
    {
        if (string.IsNullOrEmpty(photoUrl) && string.IsNullOrEmpty(thumbnailUrl))
        {
            return;
        }
        Imaging.CreateProportionalImageThumbnail(thumbnailUrl, "/images/DailyArticleImages/" + ArticleId + ".jpg", 0,
                                                 Height, Context);

        Imaging.SaveImageFromUrl(photoUrl, "/images/DailyArticleBigImages/" + ArticleId + ".jpg");
        //Imaging.CreateProportionalImageThumbnail(ImageURL, "/images/thumbnails/" + ArticleId + ".jpg", 155, 116,
        //                                         Context, false);
    }

    //Protected Sub btnMakeFeatured_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMakeFeatured.Click
    //    MisesWeb.DailyArticle.UpdateFeaturedArticle(CInt(Val(lblArticleId.Text)))
    //End Sub

    private void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("articles.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int ArticleId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        Mises.Domain.Articles.DailyArticle.DeleteArticle(ArticleId);
        Response.Redirect("articles.aspx");
    }

    protected void btnTidyHTML_Click(object sender, EventArgs e)
    {
        txtFullText.Text = HTMLtidy.TidyHTML(txtFullText.Text);
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }

    protected void btnCopyImages_Click(object sender, EventArgs e)
    {
        // Make a hard copy:

        if (lnkArticleId.Text == "0")
        {
            lblErrorMessage.Text = "Save the article first.";
            return;
        }

        var ArticleId = txtCopyFromId.Text.Trim();

        string sourcePath = Server.MapPath("/Images/DailyArticleImages/" + ArticleId + ".jpg");
        string newLinkPath = Server.MapPath("/Images/DailyArticleImages/" + lnkArticleId.Text + ".jpg");

        CreateCopy(newLinkPath, sourcePath);

        //CreateHardLink(newLinkPath, sourcePath, IntPtr.Zero);

        sourcePath = Server.MapPath("/Images/DailyArticleBigImages/" + ArticleId + ".jpg");
        newLinkPath = Server.MapPath("/Images/DailyArticleBigImages/" + lnkArticleId.Text + ".jpg");
        //CreateHardLink(newLinkPath, sourcePath, IntPtr.Zero);

        CreateCopy(newLinkPath, sourcePath);

        imgPhoto.ImageUrl = string.Format("http://mises.freecapitalists.org//images/DailyArticleBigImages/{0}.jpg", ArticleId);
        imgThumbnail.ImageUrl = string.Format("http://mises.freecapitalists.org//images/DailyArticleImages/{0}.jpg", ArticleId);

        lblErrorMessage.Text = "Image copied.";
    }

    private static void CreateCopy(string newLinkPath, string sourcePath)
    {
        File.Copy(sourcePath, newLinkPath);
    }


    [DllImport("Kernel32.dll", CharSet = CharSet.Unicode)]
    private static extern bool CreateHardLink(
        string lpFileName,
        string lpExistingFileName,
        IntPtr lpSecurityAttributes
        );
}