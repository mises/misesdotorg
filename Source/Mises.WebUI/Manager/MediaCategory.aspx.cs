﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Manager
{
    partial class MediaCategory : Page
    {
        protected void btnNewCategory_Click(object sender, EventArgs e)
        {
            DetailsView1.ChangeMode(DetailsViewMode.Insert);
        }

        protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            DataBind();
        }
    }
}