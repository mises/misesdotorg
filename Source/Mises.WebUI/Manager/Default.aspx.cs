﻿#region

using System;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.UI;
using Mises.Data;


#endregion

/// <summary>
///   Summary description for Default
/// </summary>
partial class Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      //  if (Page.IsPostBack) return;

        var cookie = new HttpCookie("IsAdminLoggedIn", "1") {Expires = DateTime.Now.AddMonths(3)};
        Response.Cookies.Add(cookie);


        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));

        IMultipleResults results = db.GlobalGetStatisticsMultipleRS(true);

        gvGlobalStats.DataSource = results.GetResult<GlobalGetStatisticsResult>();
        gvGlobalStats.DataBind();

        if (Request.QueryString["update"] != null)
            UpdateSite(Request.QueryString["update"]);
    }

    private void UpdateSite(string environment)
    {
        Response.BufferOutput = false;

        string fileName = @"I:\web\UpdateMisesBetaFromSVN.bat";

        if (environment == "LIVE")
        {
            fileName = @"I:\web\UpdateMisesLiveFromSVN.bat";
        }

        // http://meyerweb.com/eric/tools/dencoder/
        if (Request.QueryString["file"] != null)
        {
            fileName = Request.QueryString["file"];
        }

        if (File.Exists(fileName))
        {
            // Create the ProcessInfo object
            ProcessStartInfo psi = new ProcessStartInfo("cmd.exe")
                                       {
                                           UseShellExecute = false,
                                           RedirectStandardOutput = true,
                                           RedirectStandardInput = true,
                                           RedirectStandardError = true,
                                           WorkingDirectory = "c:\\temp\\"
                                       };

            // Start the process
            Process proc = Process.Start(psi);

            // Open the batch file for reading
            StreamReader strm = File.OpenText(fileName);

            // Attach the output for reading
            StreamReader sOut = proc.StandardOutput;
            
            // Attach the in for writing
            StreamWriter sIn = proc.StandardInput;

            // Write each line of the batch file to standard input
            while (strm.Peek() != -1)
            {
                sIn.WriteLine(strm.ReadLine());
            }
            
            strm.Close();

            // Exit CMD.EXE
            const string stEchoFmt = "# {0} run successfully. Exiting";

            sIn.WriteLine(String.Format(stEchoFmt, fileName));
            sIn.WriteLine("EXIT");

            // Close the process
            proc.Close();

            // Read the sOut to a string.
            string results = sOut.ReadToEnd().Trim();
            
            // Close the io Streams;
            sIn.Close();
            sOut.Close();

            // Write out the results.
            const string fmtStdOut = "<font face=courier size=0>{0}</font>";
            Response.Write(String.Format(fmtStdOut, results.Replace(Environment.NewLine, "<br>")));
        }

        //StreamReader myStreamReader = proc.StandardOutput;
        //string myString = myStreamReader.ReadLine();
        //Debug.WriteLine(myString);
        //Response.Write("<h1>" + myString + "</h1>");

        //StreamReader myStreamReader2 = proc.StandardError;
        //string myString2 = myStreamReader2.ReadLine();

        //if (!string.IsNullOrWhiteSpace(myString2))
        //{
        //    Response.Write(@"<h1 style=""color:red"">" + myString2 + "</h1>");
        //}

        //proc.Close();
        Response.Write("<h3>Finished</h3>");

        Response.End();
    }
}