<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" Title="Mises Content Management System - Authorized Users Only"
    EnableViewState="False" Inherits="Default" CodeBehind="~Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="tabs">
        <ul>
            <li><a href="#tab-cms">
                <img src="../images/Theme/images/mobile/book.png" />Content Management</a></li>
            <li><a href="#tab-offsite">
                <img src="../images/Theme/images/mobile/search.png" />Third Party</a></li>
            <li><a href="#tab-uptime">
                <img src="../images/Theme/images/mobile/edu_science.png" />Stats &amp; Uptime</a></li>
        </ul>
        <div id="tab-cms">
            <div class="leftColumn">
                <div class="adminSection">
                    <h3>
                        Publications/Documents Editors</h3>
                    <ul>
                        <li><a href="/Manager/articles.aspx">
                            <img src="../images/Theme/images/ico/ico-post.gif" />Daily Article Editor</a>
                        <li><a href="/Manager/content.aspx?format=literature">
                            <img src="../images/Theme/images/ico/ico-post2.gif" />Documents Editor</a>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        Audio/Visual Media</h3>
                    <ul>
                        <li><a href="/Manager/content.aspx?format=media">
                            <img src="../images/Theme/images/ico/ico-audio.gif" />Media Manager<img src="../images/Theme/images/ico/ico-video.gif" />
                        </a>
                        <li><a href="/Manager/MediaCategory.aspx">
                            <img src="../images/Theme/images/ico/ico-mymises.gif" />Media Categories</a></li>
                        <li><a href="/Manager/MediaTypes.aspx">
                            <img src="../images/Theme/images/ico/sub_youtube.png" />Edit Media Types</a></li>
                        <li><a href="https://sitemanager.itunes.apple.com/WebObjects/DZStudio.woa/wa/SignIn/">
                            iTunes U Admin</a></li>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        Mises Institute Faculty & Staff</h3>
                    <ul>
                        <li><a href="/Manager/fellows.aspx">
                            <img src="../images/Theme/images/ico/person.png" />Faculty Biographies</a></li>
                        <li><a href="/Manager/faculty.aspx">
                            <img src="../images/Theme/images/emailthis.jpg" />Faculty and Staff Directory</a></li>
                        <li>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        Site Settings/Misc</h3>
                    <ul>
                        <li><a href="/Manager/quizinfo.aspx">Quiz Manager</a></li>
                        <li><a href="/Manager/quotes.aspx">Mises.Org Quotes Editor</a></li><li><a href="/Manager/pageContent.aspx">
                            Page Content Editor (Sidebars, etc)</a></li><li><a href="/Manager/tags.aspx">
                                <img src="../images/icons/icon_category.gif" />Tag Manager</a></li>
                        <li><a href="/Manager/URLmapping.aspx">URL Rewrite Mappings</a></li><li><a href="/Manager/errors.aspx">
                            Mises.org Error Log</a></li>
                        <li><a href="/Manager/library.aspx">Ward Library</a></li>
                        <li><a href="/Manager/AcademyCourse.aspx">
                            <img src="../images/Theme/images/ico/academy.png" />Academy/Featured Product Widget</a></li>
                    </ul>
                </div>
            </div>
            <div class="rightColumn">
                <div class="adminSection">
                    <h3>
                        Create New:</h3>
                    <ul>
                        <li><a href="/Manager/article.aspx">
                            <img src="../images/icons/doc.png" />Daily Article</a></li>
                        <li><a href="/Manager/page.aspx">
                            <img src="../images/icons/html.png" />HTML Web Page</a></li>
                        <li><a href="/Manager/document.aspx?format=literature">
                            <img src="../images/icons/book.png" />Book/Document</a></li>
                        <li><a href="/Manager/document.aspx?format=media">
                            <img src="../images/icons/mpeg4.png" />Audio/Video Document</a></li>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        Forms, Events, &amp; Donations</h3>
                    <ul>
                        <li><a href="/Manager/events.aspx">
                            <img src="../images/Theme/images/ico/ico-events.jpg" />
                            Event Calendar </a></li>
                        <li><a href="/Manager/formManager.aspx">
                            <img src="../images/Theme/images/ico/ico-bestseller.gif" />Registration/Donate Custom
                            Forms Manager</a></li>
                        <li><a href="/Manager/donations.aspx">
                            <img src="/images/icons/store.png" />Donation Records</a></li>
                        
                        <li><a href="/Manager/registrations.aspx">
                            <img src="/images/icons/store.png" />Registration (Forms) Records</a></li>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        Journals</h3>
                    <ul>
                        <li><strong><a href="/Manager/periodicals/periodicals.aspx">
                            <img src="../images/Theme/images/mobile/journal.png" height="32" />Periodicals Manager</a></strong></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=1">Free Market Manager</a> <a class="addNewLink"
                            href="periodicals/freemarket.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=2">Mises Review Manager</a> <a class="addNewLink"
                            href="periodicals/misesreview.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=3">JLS Manager</a> <a class="addNewLink"
                            href="periodicals/jls.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=4">QJAE Article Manager</a> <a class="addNewLink"
                            href="periodicals/qjae.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=5">RAE Article Manager</a> <a class="addNewLink"
                            href="periodicals/rae.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=6">AEN Article Manager</a> <a class="addNewLink"
                            href="periodicals/aen.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=7">Scholar Article Manager</a> <a
                            class="addNewLink" href="periodicals/scholar.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/JournalArchives.aspx?Id=8">Ward Library Database</a> <a
                            class="addNewLink" href="periodicals/wardlibrary.aspx">Add New</a></li>
                        <li><a href="/Manager/periodicals/wardlibrarydb.aspx">Ward Library Search UI</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="tab-offsite">
            <div class="leftColumn">
                <div class="adminSection">
                    <h3>
                        Analytics</h3>
                    <ul>
                        <%--<li><a href="?update=BETA">Update BETA</a></li><li><a href="?update=LIVE">Update LIVE</a></li>--%>
                        <li><a href="http://blog.mises.org/wp-admin">Blog Admin</a></li><li><a href="http://google.com/analytics/">
                            Google Analytics</a>
                        <li>
                            <%--<a href="http://tcr141.tynt.com/dashboard/#mi$e$">Tynt Dashboard</a><br/>--%>
                        <li><a href="http://www.crazyegg.com/login/?username=misesinstitute@gmail.com">Crazzyegg</a>
                            <%--<a href="http://code.mises.com:8080/">Jenkins</a></li><li>--%>
                            <%--<a href="http://code.mises.com:8080/job/Mises_Live/build?token=PUSH_LIVE">PUSH BETA TO LIVE</a><br/>--%>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        Mises Universe</h3>
                    <ul>
                        <li><a href="https://www.google.com/a/cpanel/mises.com/Dashboard">Mises.com Google Dashboard</a></li>
                        <li><a href="http://academy.mises.org/admin/">Moodle Admin</a>
                        <li><a href="http://intensedebate.com/moderate/150587">comment moderation</a>
                    </ul>
                </div>
            </div>
            <div class="rightColumn">
                <div class="adminSection">
                    <h3>
                        Collaboration Tools</h3>
                    <ul>
                        <li><a href="http://projects.mises.org/">Projects.Mises.org</a> </li>
                        <%--<li><a href="/Community/groups/mises/default.aspx">Community: Mises Internal Group</a></li>
                        <li><a href="/Community/groups/mises/wiki/upcoming-articles.aspx">Wiki: Upcoming Articles</a></li>--%>
                        <li><a href="http://code.mises.com/svn/listing.php?repname=MisesWeb">Web SVN</a></li>
                    </ul>
                </div>
                <div class="adminSection">
                    <h3>
                        E-commerce</h3>
                    <ul>
                        <li><a href="https://store.mises.org/Admin/Website/Scriptlets/EditScriptlet.aspx?s=Home%20Page&t=Content&tid=Glass_MidNight">
                            Edit Store Home Page</a></li><li><a href="/MisesShop/admin">Able Mises Shop Admin</a></li>
                        <li><a href="http://store.mises.org/photoupload.aspx">Product Photo Upload</a></li>
                        
                        <li><a href="https://docs.google.com/spreadsheet/ccc?key=0ApD5VqGCXfIEcFU5RXR3RGdGRnUtWTVXeV83VWY0VFE&pli=1#gid=0">All ISBNs - Google Docs</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="tab-uptime">
            <div id="StatsTable">
                <h2>
                    Site Stats:</h2>
                <div style="width: 650px; overflow: auto;">
                    <asp:GridView ID="gvGlobalStats" runat="server" Width="650">
                    </asp:GridView>
                </div>
            </div>
            <div id="Uptime1">
                <a href="http://site24x7.com/login/status.do?execute=StatusReport&u=true&p=WYJxFr%2BQXfWL%2FYOcA1Axh9A6bT6dDJCR4DPibVPpQ2PTmQXWaoMxCA%3D%3D"
                    style="text-decoration: none; cursor: pointer; color: #000000;"><span style="border: 1px solid #FF9933;
                        background-color: #FF6600; text-transform: 0px; text-align: center; display: inline-block;
                        line-height: 11px; text-indent: 1px; font-family: Verdana; font-size: 9px;"><b style="display: inline-block;
                            color: #FFFFFF;">Website<li>
                            Uptime </b><b style="background-color: #FFFFFF; display: inline-block;">Site24X7
                                <li>
                                    <script type="text/javascript" src="http://ext1.site24x7.com/website-uptime.html?v=WYJxFr%2BQXfU3ef3W2DkbzJpJwodla%2BdX"> </script>
                            </b></span></a>
            </div>
            <div class="clearer">
            </div>
            <hr />
            <div class="footer">
                <iframe src='https://www.site24x7.com/login/status.do?execute=portalIntegration&id=BjJ9ujZCJI3FUPynqDbtIKFqyBCNdw%2FoizS1vBIT8MuXCmkqQZ%2BZQVWSh%2BlKdkYF'
                    scrolling='yes' align='center' height='400' width='1000' border='0' frameborder='0'>
                </iframe>
                <a href="http://www.pingdom.com">
                    <img src="http://share.pingdom.com/banners/d4c15ab9" alt="Uptime for Mises Blog: Last 30 days "
                        title=" Uptime for Mises Blog: Last 30
    days" width="300" height="165" /></a>
                <!-- IPv6-test.com button BEGIN -->
                <a href='http://ipv6-test.com/validate.php?url=referer'>
                    <img src='http://ipv6-test.com/button-ipv6-small.png' alt='ipv6 ready' title='ipv6
    ready' border='0' /></a>
                <!-- IPv6-test.com button END -->
            </div>
        </div>
    </div>
</asp:Content>
