<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" AutoEventWireup="true"
         Title="Mises Form Manager" Inherits="formManager" Codebehind="~/Manager/formManager.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        Mises Forms Manager:</h2>
    <asp:SqlDataSource ID="sqlFormsList" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="SELECT [FormId], [FormTitle], [CreateDate], [FormAuthor] FROM [RegistrationForms] ORDER BY [CreateDate] DESC"
                       DeleteCommand="DELETE FROM [RegistrationForms] WHERE [FormId] = @FormId" InsertCommand="INSERT INTO [RegistrationForms] ([FormTitle], [CreateDate], [FormAuthor]) VALUES (@FormTitle, @CreateDate, @FormAuthor)"
                       UpdateCommand="UPDATE [RegistrationForms] SET [FormTitle] = @FormTitle, [CreateDate] = @CreateDate, [FormAuthor] = @FormAuthor WHERE [FormId] = @FormId"
                       OnDeleting="sqlFormsList_Deleting">
        <DeleteParameters>
            <asp:Parameter Name="FormId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="FormTitle" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="FormAuthor" Type="String" />
            <asp:Parameter Name="FormId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="FormTitle" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="FormAuthor" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    <h4>
        <a href="formEditor.aspx">New Registration Form</a></h4>
    <div class="box">
        <asp:GridView ID="gvFormsList" runat="server" AutoGenerateColumns="False" DataKeyNames="FormId"
                      DataSourceID="sqlFormsList" AllowPaging="True" AllowSorting="True" Width="100%"
                      OnRowDeleted="gvFormsList_RowDeleted" PageSize="50">
            <Columns>
                <asp:BoundField DataField="FormId" HeaderText="FormId" InsertVisible="False" ReadOnly="True"
                                SortExpression="FormId" Visible="False" />
                <asp:HyperLinkField DataNavigateUrlFields="FormId" DataNavigateUrlFormatString="/form.aspx?Id={0}"
                                    DataTextField="FormTitle" HeaderText="Title (View)" SortExpression="FormTitle" />
                <asp:HyperLinkField DataNavigateUrlFields="FormId" DataNavigateUrlFormatString="formEditor.aspx?formId={0}"
                                    HeaderText="Edit" SortExpression="FormId" Text="Edit" />
                <asp:BoundField DataField="FormAuthor" HeaderText="From" SortExpression="FormAuthor" />
                <asp:BoundField DataField="CreateDate" HeaderText="Created" SortExpression="CreateDate"
                                DataFormatString="{0:d}" />
            </Columns>
        </asp:GridView>
    </div>
    <asp:Panel ID="Panel1" runat="server" BackColor="WhiteSmoke">
        <h4>
            Create a quick registration link:</h4>
        <br />
        Title:<br />
        <asp:TextBox ID="txtTitle" runat="server" Columns="100" TextMode="MultiLine" CssClass="ckeditor" Width="628px"></asp:TextBox><br />
        Description:<br />
        <asp:TextBox ID="txtDescription" runat="server" Columns="100" TextMode="MultiLine" CssClass="ckeditor"
                     Width="628px"></asp:TextBox><br />
        <strong>
            <br />
            Optional Fields:<br />
        </strong>
        <br />
        Image URL:<br />
        <asp:TextBox ID="txtImageURL" runat="server" Width="622px"></asp:TextBox><br />
        <br />
        Cost:<asp:TextBox ID="txtCost" runat="server"></asp:TextBox>
        Cost for guests:<asp:TextBox ID="txtGuestCost" runat="server"></asp:TextBox>
        <asp:CheckBox ID="chkProcessPayment" runat="server" Text="Process Payment" Checked="True" /><br />
        Thank you message:<br />
        <asp:TextBox ID="txtThankYou" runat="server" Columns="100" TextMode="MultiLine" CssClass="ckeditor" Width="628px"></asp:TextBox><br />
        <br />
        <asp:Button ID="btnCreateLink" runat="server" OnClick="btnCreateLink_Click" Text="Create Link!" /></asp:Panel>
</asp:Content>