﻿<%@ Page Title="Donation Detail View" Language="C#" MasterPageFile="~/MasterPages/Manager.master"
         AutoEventWireup="true" Inherits="Manager_donation" Codebehind="donation.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3>
        <a href="donations.aspx">Back to list</a>
    </h3>
    <h2>
        <a href="donations.aspx">Donation Detail:</a></h2>
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CellPadding="4"
                     DataKeyNames="DonationID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None"
                     Height="50px" Width="100%">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:BoundField DataField="DonationID" HeaderText="DonationID" InsertVisible="False"
                            ReadOnly="True" SortExpression="DonationID" />
            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
            <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip" />
            <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:CheckBoxField DataField="MisesMember" HeaderText="MisesMember" SortExpression="MisesMember" />
            <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
            <asp:BoundField DataField="OtherAmount" HeaderText="OtherAmount" SortExpression="OtherAmount" />
            <asp:BoundField DataField="Designation" HeaderText="Designation" SortExpression="Designation" />
            <asp:BoundField DataField="OtherDesignation" HeaderText="OtherDesignation" SortExpression="OtherDesignation" />
            <asp:CheckBoxField DataField="Recurring" HeaderText="Recurring" SortExpression="Recurring" />
            <asp:BoundField DataField="RecurringInterval" HeaderText="RecurringInterval" SortExpression="RecurringInterval" />
            <asp:CheckBoxField DataField="GiftEstate" HeaderText="GiftEstate" SortExpression="GiftEstate" />
            <asp:CheckBoxField DataField="GiftIncome" HeaderText="GiftIncome" SortExpression="GiftIncome" />
            <asp:CheckBoxField DataField="Anonymous" HeaderText="Anonymous" SortExpression="Anonymous" />
            <asp:BoundField DataField="CardName" HeaderText="CardName" SortExpression="CardName" />
            <asp:BoundField DataField="CardType" HeaderText="CardType" SortExpression="CardType" />
            <asp:BoundField DataField="CardNumber" HeaderText="CardNumber" SortExpression="CardNumber" />
            <asp:BoundField DataField="CardExpMonth" HeaderText="CardExpMonth" SortExpression="CardExpMonth" />
            <asp:BoundField DataField="CardExpYear" HeaderText="CardExpYear" SortExpression="CardExpYear" />
            <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="Comments" />
            <asp:BoundField DataField="IPAddress" HeaderText="IP Address" SortExpression="IPAddress" />
            <asp:BoundField DataField="ApprovalCode" HeaderText="ApprovalCode" SortExpression="ApprovalCode" />
            <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" SortExpression="TransactionId" />
            <asp:BoundField DataField="CreateTime" HeaderText="CreateTime" SortExpression="CreateTime" />
            <asp:BoundField DataField="CreateUserID" HeaderText="CreateUserID" SortExpression="CreateUserID" />
            <asp:BoundField DataField="ModifyTime" HeaderText="ModifyTime" SortExpression="ModifyTime" />
            <asp:BoundField DataField="ModifyUserID" HeaderText="ModifyUserID" SortExpression="ModifyUserID" />
            <asp:CheckBoxField DataField="IsDeleted" HeaderText="IsDeleted" SortExpression="IsDeleted" />
            <asp:BoundField DataField="IPaddress" HeaderText="IP Address" />
            <asp:BoundField DataField="TestMode" HeaderText="Test Mode" />
            <asp:BoundField DataField="ResponseCodeText" HeaderText="ResponseCodeText" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ButtonType="Button" />
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [Donations] WHERE [DonationID] = @DonationID" InsertCommand="INSERT INTO [Donations] ([FirstName], [LastName], [Phone], [Address], [City], [State], [Zip], [Country], [Email], [MisesMember], [Amount], [OtherAmount], [Designation], [OtherDesignation], [Recurring], [RecurringInterval], [GiftEstate], [GiftIncome],Anonymous, [CardName], [CardType], [CardNumber], [CardExpMonth], [CardExpYear], [Comments], [ApprovalCode], [TransactionId], [CreateTime], [CreateUserID], [ModifyTime], [ModifyUserID], [IsDeleted]) VALUES (@FirstName, @LastName, @Phone, @Address, @City, @State, @Zip, @Country, @Email, @MisesMember, @Amount, @OtherAmount, @Designation, @OtherDesignation, @Recurring, @RecurringInterval, @GiftEstate, @GiftIncome,@Anonymous, @CardName, @CardType, @CardNumber, @CardExpMonth, @CardExpYear, @Comments, @ApprovalCode, @TransactionId, @CreateTime, @CreateUserID, @ModifyTime, @ModifyUserID, @IsDeleted)"
                       SelectCommand="SELECT * FROM [Donations] WHERE ([DonationID] = @DonationID)"
                       UpdateCommand="UPDATE [Donations] SET [FirstName] = @FirstName, [LastName] = @LastName, [Phone] = @Phone, [Address] = @Address, [City] = @City, [State] = @State, [Zip] = @Zip, [Country] = @Country, [Email] = @Email, [MisesMember] = @MisesMember, [Amount] = @Amount, [OtherAmount] = @OtherAmount, [Designation] = @Designation, [OtherDesignation] = @OtherDesignation, [Recurring] = @Recurring, [RecurringInterval] = @RecurringInterval, [GiftEstate] = @GiftEstate, [GiftIncome] = @GiftIncome,[Anonymous] = @Anonymous, [CardName] = @CardName, [CardType] = @CardType, [CardNumber] = @CardNumber, [CardExpMonth] = @CardExpMonth, [CardExpYear] = @CardExpYear, [Comments] = @Comments, [ApprovalCode] = @ApprovalCode, [TransactionId] = @TransactionId, [CreateTime] = @CreateTime, [CreateUserID] = @CreateUserID, [ModifyTime] = @ModifyTime, [ModifyUserID] = @ModifyUserID, [IsDeleted] = @IsDeleted WHERE [DonationID] = @DonationID">
        <DeleteParameters>
            <asp:Parameter Name="DonationID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="City" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Zip" Type="String" />
            <asp:Parameter Name="Country" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="MisesMember" Type="Boolean" />
            <asp:Parameter Name="Amount" Type="Decimal" />
            <asp:Parameter Name="OtherAmount" Type="Decimal" />
            <asp:Parameter Name="Designation" Type="String" />
            <asp:Parameter Name="OtherDesignation" Type="String" />
            <asp:Parameter Name="Recurring" Type="Boolean" />
            <asp:Parameter Name="RecurringInterval" Type="Int32" />
            <asp:Parameter Name="GiftEstate" Type="Boolean" />
            <asp:Parameter Name="GiftIncome" Type="Boolean" />
            <asp:Parameter Name="Anonymous" Type="Boolean" />
            <asp:Parameter Name="CardName" Type="String" />
            <asp:Parameter Name="CardType" Type="String" />
            <asp:Parameter Name="CardNumber" Type="String" />
            <asp:Parameter Name="CardExpMonth" Type="Int32" />
            <asp:Parameter Name="CardExpYear" Type="Int32" />
            <asp:Parameter Name="Comments" Type="String" />
            <asp:Parameter Name="ApprovalCode" Type="Int32" />
            <asp:Parameter Name="TransactionId" Type="String" />
            <asp:Parameter Name="CreateTime" Type="DateTime" />
            <asp:Parameter Name="CreateUserID" Type="Int32" />
            <asp:Parameter Name="ModifyTime" Type="DateTime" />
            <asp:Parameter Name="ModifyUserID" Type="Int32" />
            <asp:Parameter Name="IsDeleted" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="" Name="DonationID" QueryStringField="Id"
                                      Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="City" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Zip" Type="String" />
            <asp:Parameter Name="Country" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="MisesMember" Type="Boolean" />
            <asp:Parameter Name="Amount" Type="Decimal" />
            <asp:Parameter Name="OtherAmount" Type="Decimal" />
            <asp:Parameter Name="Designation" Type="String" />
            <asp:Parameter Name="OtherDesignation" Type="String" />
            <asp:Parameter Name="Recurring" Type="Boolean" />
            <asp:Parameter Name="RecurringInterval" Type="Int32" />
            <asp:Parameter Name="GiftEstate" Type="Boolean" />
            <asp:Parameter Name="GiftIncome" Type="Boolean" />
            <asp:Parameter Name="Anonymous" Type="Boolean" />
            <asp:Parameter Name="CardName" Type="String" />
            <asp:Parameter Name="CardType" Type="String" />
            <asp:Parameter Name="CardNumber" Type="String" />
            <asp:Parameter Name="CardExpMonth" Type="Int32" />
            <asp:Parameter Name="CardExpYear" Type="Int32" />
            <asp:Parameter Name="Comments" Type="String" />
            <asp:Parameter Name="ApprovalCode" Type="Int32" />
            <asp:Parameter Name="TransactionId" Type="String" />
            <asp:Parameter Name="CreateTime" Type="DateTime" />
            <asp:Parameter Name="CreateUserID" Type="Int32" />
            <asp:Parameter Name="ModifyTime" Type="DateTime" />
            <asp:Parameter Name="ModifyUserID" Type="Int32" />
            <asp:Parameter Name="IsDeleted" Type="Boolean" />
            <asp:Parameter Name="DonationID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>