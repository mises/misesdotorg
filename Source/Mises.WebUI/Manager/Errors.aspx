<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true"
    Title="Error Log" Inherits="Manager_Errors" CodeBehind="Errors.aspx.cs" %>

<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="text-align: left; margin-left:0px;">
        <h1>
            Mises Error Log</h1>
        <div id="chart">
            <img src="<%=((Manager_Errors) Page).ChartUrl%>" alt="Exception Frequency Chart" />
        </div>
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CellPadding="4"
            DataSourceID="EntityDataSource2" ForeColor="#333333" GridLines="None" Height="50px"
            Width="125px">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <RowStyle BackColor="#EFF3FB" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <Fields>
                <asp:BoundField DataField="ExceptionID" HeaderText="Exception ID" InsertVisible="False"
                    SortExpression="ExceptionID" />
                <asp:BoundField DataField="ExceptionHash" HeaderText="Exception Hash" SortExpression="ExceptionHash" />
                <asp:BoundField DataField="Exception1" HeaderText="Exception" SortExpression="Exception1"
                    ItemStyle-Width="500px" />
                <asp:BoundField DataField="ExceptionMessage" HeaderText="Stack Trace" SortExpression="ExceptionMessage" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" SortExpression="IPAddress" />
                <asp:BoundField DataField="ServerName" HeaderText="Server" SortExpression="ServerName" />
                <asp:BoundField DataField="UserAgent" HeaderText="User Agent" SortExpression="UserAgent" />
                <asp:BoundField DataField="HttpReferrer" HeaderText="Referrer" SortExpression="HttpReferrer" />
                <asp:BoundField DataField="HttpVerb" HeaderText="Http Verb" SortExpression="HttpVerb" />
                <asp:BoundField DataField="PathAndQuery" HeaderText="Path" SortExpression="PathAndQuery"
                    ItemStyle-Width="250px" />
                <asp:BoundField DataField="DateCreated" HeaderText="Date Created" SortExpression="DateCreated" />
                <asp:BoundField DataField="DateLastOccurred" HeaderText="Last Occurred" SortExpression="DateLastOccurred" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" SortExpression="Frequency" />
            </Fields>
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
        <cc1:GridView ID="GridView1" runat="server" AllowPaging="True" Width="100%" AllowSorting="True"
            AutoGenerateColumns="False" DataSourceID="EntityDataSource1" PagerSettings-PageButtonCount="20"
            EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
            SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif" CellPadding="4"
            DataKeyNames="ExceptionID" EnableSelection="False" ForeColor="#333333" GridLines="None"
            SortDirectionAlt="Ascending" SortExpressionAlt="" PageSize="80" SortColumnHeaderIsBold="False">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="Exception1" HeaderText="Exception" SortExpression="Exception1"
                    ItemStyle-Width="50">
                    <ItemStyle Width="50px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="HttpVerb" HeaderText="Verb" SortExpression="HttpVerb" />
                <asp:TemplateField HeaderText="Path" SortExpression="PathAndQuery">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("PathAndQuery").ToString().Length > 70
                                 ? Eval("PathAndQuery").ToString().Substring(0, 70)
                                 : Eval("PathAndQuery")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DateLastOccurred" HeaderText="Last Occurred" SortExpression="DateLastOccurred" />
                <asp:BoundField DataField="Frequency" HeaderText="Frequency" SortExpression="Frequency" />
            </Columns>
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerSettings PageButtonCount="20"></PagerSettings>
            <RowStyle BackColor="#EFF3FB" />
        </cc1:GridView>
        <asp:Button ID="btnClearExceptionLog" runat="server" OnClick="btnClearExceptionLog_Click"
            Text="DELETE ALL" />
        <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=MisesModel"
            DefaultContainerName="MisesModel" EnableFlattening="False" EntitySetName="ExceptionLogs"
            EntityTypeFilter="ExceptionLog" OrderBy="it.[DateLastOccurred] desc" 
            Select="it.[ExceptionID], it.[ExceptionHash], it.[Exception1], it.[ExceptionMessage], it.[IPAddress], it.[ServerName], it.[UserAgent], it.[HttpReferrer], it.[HttpVerb], it.[PathAndQuery], it.[DateCreated], it.[DateLastOccurred], it.[Frequency]">
        </asp:EntityDataSource>
        <asp:EntityDataSource ID="EntityDataSource2" runat="server" AutoGenerateWhereClause="true"
            ConnectionString="name=MisesModel" DefaultContainerName="MisesModel" EnableFlattening="False"
            EntitySetName="ExceptionLogs" EntityTypeFilter="ExceptionLog">
            <WhereParameters>
                <asp:ControlParameter ControlID="GridView1" DefaultValue="0" Name="ExceptionID" PropertyName="SelectedValue"
                    Type="Int32" />
            </WhereParameters>
        </asp:EntityDataSource>
    </div>
</asp:Content>
