﻿#region

using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using Mises.Data;

#endregion

public partial class Manager_community : Page
{
    private readonly string conn = ConfigurationManager.ConnectionStrings["Membership"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack) return;

        GridView1.DataSource = SqlHelper.ExecuteDataset(conn, CommandType.Text,
                                                        @"SELECT TOP 1000 
				UserID,
				UserName,
				Email,
				ForceLogin,
				UserAccountStatus,				
				LastActivity,								
				IsApproved,
				IsAnonymous,
				CreateDate FROM dbo.cs_Users ORDER BY dbo.cs_Users.CreateDate DESC
");
        GridView1.DataBind();
    }

    protected void btnClean_Click(object sender, EventArgs e)
    {
        object result = SqlHelper.ExecuteScalar(conn, "_CleanupSpam");
        Response.Write("Done: " + result);
    }
}