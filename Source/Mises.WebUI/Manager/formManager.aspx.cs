﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;

#endregion

/// <summary>
///   Summary description for formManager
/// </summary>
partial class formManager : Page
{
    protected void btnCreateLink_Click(object sender, EventArgs e)
    {
        string conferenceName = txtTitle.Text;
        string description = txtDescription.Text;
        double guestCost = Conversion.Val(txtGuestCost.Text);
        string imgName = txtImageURL.Text;
        double cost = Conversion.Val(txtCost.Text);
        bool process = chkProcessPayment.Checked;

        //TODO: FIX to relative URL
        string url = "https://mises.freecapitalists.org/register.aspx?title=" + conferenceName;
        url += "&cost=" + cost + "&guests=" + guestCost + "&image=" + imgName + "&thankyou=" + txtThankYou.Text +
               "&description=" + description;
        if (process == false)
        {
            url += "&process=no";
        }
        Response.Redirect(url);
    }

    protected void gvFormsList_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        throw new Exception(e.Keys[0].ToString());
    }

    protected void sqlFormsList_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Cancel = true;
    }
}