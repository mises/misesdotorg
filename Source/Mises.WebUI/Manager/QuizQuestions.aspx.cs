#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class Manager_QuizQuestionsPage : Page
{
    protected void btnAddNewItem_Click(object sender, EventArgs e)
    {
        dvDetailEdit.ChangeMode(DetailsViewMode.Insert);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        btnAddNewItem.Click += btnAddNewItem_Click;
    }


    protected void gvQuizContentListRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Add")
        {
            btnAddNewItem_Click(null, null);
        }

        var questionIdBox = (TextBox) dvDetailEdit.FindControl("txtQuestionId");

        if (questionIdBox != null)
        {
            int dataItemindex = int.Parse(e.CommandArgument.ToString());

            string QuestionId = gvQuizContentList.DataKeys[dataItemindex]["QuestionID"].ToString();
            questionIdBox.Text = QuestionId;

            //string question = gvQuizContentList.DataKeys[dataItemindex]["Question"].ToString();
            //((Label)dvDetailEdit.FindControl("lblQuestion")).Text = question;
        }
    }
}