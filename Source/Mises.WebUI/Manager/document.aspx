<%@ Page ValidateRequest="false" Language="C#" MasterPageFile="~/MasterPages/Manager.Master"
    AutoEventWireup="false" Inherits="document" Title="Editing Content Document"
    MaintainScrollPositionOnPostback="true" CodeBehind="document.aspx.cs" %>

<%@ Register Src="../Controls/Tagging/TagCloud.ascx" TagName="TagCloud" TagPrefix="uc3" %>
<%@ Register Src="ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc4" %>
<%@ Register Src="ManagerControls/AuthorSelect.ascx" TagName="AuthorSelect" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function confirm_delete() {
            if (confirm("Are you sure you want to delete this item?") == true)
                return true;
            else
                return false;
        }

    </script>





    <h3 class="adminSection">
        <a href="content.aspx">Back to Document List</a>
    </h3>
    <h1>
        <asp:Literal Text="Edit Document" ID="litTitle" runat="server" /></h1>
    <asp:SqlDataSource ID="sqlMediaTypes" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        SelectCommand="SELECT [MediaTypeID], [MediaType] FROM [DocumentMediaType] ORDER BY [MediaType]">
    </asp:SqlDataSource>
    <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" Font-Bold="True"
        Font-Size="14pt" ForeColor="Red"></asp:Label>
    <table style="height: 81px" width="650">
        <tr>
            <td class="detailheader" valign="top">
                <label for="<%=chkDisplayItem.ClientID%>">
                    #<asp:Label ID="lblDocumentId" runat="server"></asp:Label></label>
            </td>
            <td class="detailtext">
                <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
                    <asp:Button ID="btnSaveItem" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Save" AccessKey="S" />
                    <asp:CheckBox ID="chkDisplayItem" runat="server" Checked="True" Text="Visible" />
                    <asp:CheckBox ID="chkFeatured" runat="server" Checked="False" Text="Featured" Visible="false" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Delete" />
                    <asp:Button ID="btnMergeWith" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Replace with: " OnClick="btnMergeWith_Click" />
                    <asp:TextBox ID="txtMergeWithNumber" runat="server" Columns="4"></asp:TextBox>
                    <input value="Cancel" class="ui-state-default ui-corner-all ui-button" type="button"
                        onclick=" window.location = 'content.aspx'; " id="btnCancel" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">
                <label>
                    URL</label>
            </td>
            <td class="detailtext">
                <asp:HyperLink NavigateUrl="" runat="server" ID="lnkToDocument" Target="_New" CssClass="ui-icon-link ui-state-default" />
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">
                <label for="<%=txtTitle.ClientID%>">
                    Title</label>
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtTitle" runat="server" Columns="90" CssClass="textfield"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                    Display="Dynamic" ErrorMessage="Title is required">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr runat="server" id="trMediaGP">
            <td class="detailheader" valign="top">
                <label for="<%=ddMediaCategory.ClientID%>">
                    <a href="MediaCategory.aspx">Grandparent Category</a></label>
            </td>
            <td class="detailtext">&nbsp;<asp:DropDownList ID="ddMediaCategoryGrandparent" runat="server"
                DataTextField="Category"
                DataValueField="CategoryId" Width="600px">
            </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="trMediaParent">
            <td class="detailheader" valign="top">
                <label for="<%=ddMediaCategory.ClientID%>">
                    <a href="MediaCategory.aspx">Parent Category</a></label>
            </td>
            <td class="detailtext">&nbsp;<asp:DropDownList ID="ddMediaCategoryParent" runat="server"
                DataTextField="Category"
                DataValueField="CategoryId" Width="600px">
            </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="trMediaCat">
            <td class="detailheader" valign="top">
                <label for="<%=ddMediaCategory.ClientID%>">
                    <a href="MediaCategory.aspx">Category</a></label>
            </td>
            <td class="detailtext">&nbsp;<asp:DropDownList ID="ddMediaCategory" runat="server"
                DataTextField="Category"
                DataValueField="CategoryId" Width="600px">
            </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">
                <label for="<%=AuthorSelect.ClientID%>">
                    Authors</label>
            </td>
            <td class="detailtext">
                <uc1:AuthorSelect ID="AuthorSelect" runat="server" />
                &nbsp;
                <asp:CheckBox ID="chkInternal" runat="server" Text="Mises.org" />
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">
                <asp:Literal ID="litDescription" runat="server" Text="Description" />
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtDescription" runat="server" Height="200px" Width="100%" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">
                <label for="<%=txtPublicationInformation.ClientID%>">
                    Publication Information</label>
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtPublicationInformation" runat="server" Columns="80" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">Keywords
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtMetaKeywords" runat="server" Columns="80" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="trUrlType">
            <td class="detailheader" style="height: 31px" valign="top">Product Links
            </td>
            <td class="detailtext" style="height: 31px">Product ID:<asp:TextBox ID="txtProductId"
                runat="server" Width="108px"></asp:TextBox>
                &nbsp;or SKU:<asp:TextBox ID="txtSKU" runat="server" AutoPostBack="True" OnTextChanged="txtSKU_TextChanged"
                    Width="108px"></asp:TextBox>
                <asp:Button ID="btnUpdateStoreFiles" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                    Text="Update Store" OnClick="btnUpdateStoreFiles_Click" />
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">
                <label for="<%=fileUploadFile.ClientID%>">
                    File Attachments</label>
            </td>
            <td class="detailtext">

                <div id="UploadDiv">

                    <asp:FileUpload ID="filUpload" runat="server" CssClass="ui-state-default ui-corner-all ui-button" />



                    <asp:Panel ID="uploadPanel" runat="server">

                        <span>Vol#<asp:TextBox ID="txtVolume" runat="server" AutoCompleteType="Disabled"
                            Rows="2" Width="40px">1</asp:TextBox>
                            Comment:
                <asp:TextBox ID="txtVolumeComment" runat="server"></asp:TextBox>
                        </span>

                    </asp:Panel>

                    <asp:Button ID="btnUpload" runat="server" Text="Upload File" CssClass="ui-state-default ui-corner-all ui-button"
                        OnClick="newUploadButton_Click" />

                </div>


                <asp:Panel ID="pnlNewSource" runat="server" BackColor="#E0E0E0" Visible="False">
                    <asp:FileUpload ID="fileUploadFile" runat="server" CssClass="ui-state-default ui-corner-all ui-button" />

                    <asp:Button ID="btnUploadFile" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Upload Document/Media" />
                </asp:Panel>
                <div>
                    or:
                </div>
                <asp:Panel ID="Panel1" runat="server" BackColor="#E0E0E0" Visible="True">
                    URL:<asp:TextBox ID="txtNewURL" Columns="70" runat="server"></asp:TextBox>
                    <asp:Button ID="btnSaveSource" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Add URL" />
                </asp:Panel>
                &nbsp; &nbsp;<br />

                <asp:GridView ID="gvMediaFiles" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    ForeColor="#333333" GridLines="None" UseAccessibleHeader="False" DataKeyNames="FileId"
                    Width="650px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Type">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddMediaType" runat="server" DataSourceID="sqlMediaTypes" DataTextField="MediaType"
                                    DataValueField="MediaTypeID" SelectedValue='<%#Eval("MediaTypeId")%>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("MediaType")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="URL">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Columns="80" Text='<%#Eval("URL")%>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("URL")%>' Text='<%#Eval("URL")%>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VolumeOrdinal" HeaderText="Volume" />
                        <asp:BoundField DataField="VolumeComment" HeaderText="Comment" />
                        <asp:CheckBoxField DataField="Display" HeaderText="Display" />
                        <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
                    </Columns>
                    <RowStyle BackColor="#EFF3FB" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr runat="server" id="trTextContent">
            <td class="detailheader" valign="top">
                <label>
                    Text Content</label>
            </td>
            <td class="detailtext">Add new HTML content <a href="/manager/page.aspx">here,</a> then
                add it as an HTML
                document type with the link /page/#<br />
            </td>
        </tr>
        <tr runat="server" id="trSubjects">
            <td class="detailheader" valign="top">
                <label for="<%=chkSubjects.ClientID%>">
                    Subjects</label>
            </td>
            <td class="detailtext">
                <asp:CheckBoxList ID="chkSubjects" runat="server" DataSourceID="sqlSubjects" DataTextField="Subject"
                    DataValueField="SubjectId">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">Source
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtSource" runat="server" Columns="90"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">GUID
            </td>
            <td class="detailtext">
                <asp:HyperLink ID="lnkGUID" runat="server">00000000-0000-0000-0000-000000000000</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top">Tags
            </td>
            <td class="detailtext">&nbsp;<uc4:DocumentTags ID="DocumentTags1" runat="server" />
                <br />
                <uc3:TagCloud ID="TagCloud" runat="server" />
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sqlSubjects" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        SelectCommand="SELECT [SubjectId], [Subject] FROM [DocumentSubjects] WHERE VISIBLE=1 ORDER BY [SortOrder], ShortSubject">
    </asp:SqlDataSource>
</asp:Content>
