﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mises.Web.Manager
{
    public partial class donors : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["p"] != null)
            {
                Session.Add("p", Request.QueryString["p"]);
            }

            if (Session["p"] == null || Session["p"].ToString() != "Mises123")
            {
             throw new Exception("Not authorized");   
            }
        }
    }
}