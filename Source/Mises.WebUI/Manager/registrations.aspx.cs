﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using GoogleChartSharp;
using Mises.Data;

#endregion

public partial class Manager_Registrations : Page
{
    private MisesDBDataContext db;

    public string ChartUrl
    {
        get
        {
            var RegistrationsList = db.RegistrationGetReport().ToList();
                //db.RegistrationsGetReport().ToList();

            var chart = new LineChart(700, 300, LineChartType.SingleDataSet);

            var labelsY = new[] {"$10K", "$20K", "$30K", "$40K", "$50K", "$60K", "$70K", "$80K", "$90K", "$100K"};
            var axisY = new ChartAxis(ChartAxisType.Left, labelsY);
            var max = (decimal) RegistrationsList.Max(t => t.Total);
            axisY.AddLabel(new ChartAxisLabel("Monthly Total"));
            //axisY.SetRange(0, (int)max);

            chart.AddAxis(axisY);
            IEnumerable<string> labelsX = RegistrationsList.Where(t => t.Month == 1 || t.Month == 6).Select(t => t.Date);
            var axisX = new ChartAxis(ChartAxisType.Bottom, labelsX.ToArray());
            axisX.AddLabel(new ChartAxisLabel("Date"));
            chart.AddAxis(axisX);


            List<int> data = RegistrationsList.Select(t => (int) t.Total.Value).ToList();
            chart.SetData(data.ToArray());

            chart.SetTitle(string.Format("Registrations from {0} to {1}", RegistrationsList.First().Date,
                                         RegistrationsList.Last().Date));

            string url = chart.GetUrl();


            string sData = "&chd=t:";
            data.ForEach(t => sData += (int) ((t/max)*100) + ",");
            sData = sData.TrimEnd(Char.Parse(","));
            url += sData;

            return url;

            //var client = new WebClient();

            //return client.UploadString(chart.GetUrl(), "POST", "");
            //return "";

            //rangeList = string.Format(rangeList, 0, RegistrationsList.Max(p => p.Total));

            //const string chartformat =
            //    "http://chart.apis.google.com/chart?chs=500x125&cht=ls&chco=0077CC&chtt={0}&chd=t:{1}&chxt=x,y&chxl=0:|{2}|1:{3}";

            //string title = Server.UrlEncode(string.Format("Exceptions in the last {0} hours", RegistrationsList.Count));

            //var url = string.Format(chartformat, title, countList, hoursList, rangeList);
            //return url;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        db = DataHelper.MisesDataContext;

        //if (Page.IsPostBack) return;
       // litTotal.Text = string.Format("{0:c}", db.DonationGetTotal().FirstOrDefault().Column1);
    }
}