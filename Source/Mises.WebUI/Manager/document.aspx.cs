#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookCatalog.Models;
using Krystalware.SlickUpload;
using Krystalware.SlickUpload.Web.Controls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Documents;
using Mises.Domain.Utility;
using Mises.MetaParser.Media;

#endregion

partial class document : Page
{
    private DocumentRepository documentRepository;

    #region Properties

    public int DocumentId
    {
        get { return Convert.ToInt32(Conversion.Val(lblDocumentId.Text)); }
        set { lblDocumentId.Text = Convert.ToString(value); }
    }

    #endregion Properties

    #region File Upload

    protected void slickUpload_UploadComplete(object sender, UploadSessionEventArgs e)
    {
       // uploadResultPanel.Visible = true;
        uploadPanel.Visible = false;

        //if (e.UploadSession != null && e.UploadSession.State == UploadState.Complete)
        //{
        //    if (e.UploadSession.UploadedFiles.Count > 0)
        //    {
        //        resultsRepeater.DataSource = e.UploadSession.UploadedFiles;
        //        resultsRepeater.DataBind();
        //    }
        //    else
        //    {
        //        SetFeedbackMessage("Upload complete but no files found.");
        //    }
        //}
        //else
        //{
        //    if (e.UploadSession.State == UploadState.Error)
        //    {
        //        throw new Exception(e.UploadSession.State.ToString() + e.UploadSession.ErrorSummary.ToString() + e.UploadSession.ErrorType.ToString());    
        //    }
            

        //    SetFeedbackMessage("Unknown upload error.");

        //}

        bool successAny = false;

        if (IsValidFile(filUpload.PostedFile))
        {
            if (ProcessUpload(filUpload.PostedFile))
                successAny = true;
        }

        //foreach (var uploadedFile in e.UploadedFiles)
        //{
        //    if (IsValidFile(uploadedFile))
        //    {
        //        if (ProcessUpload(uploadedFile))
        //            successAny = true;
        //    }
        //}

        if (successAny)
        {
            ShowItem();
        }
        else
        {
            SetFeedbackMessage("No files uploaded.");
        }
    }

    protected void newUploadButton_Click(object sender, EventArgs e)
    {
       // uploadResultPanel.Visible = false;

        slickUpload_UploadComplete(null,null);
        uploadPanel.Visible = true;
    }

    #endregion File Upload

    #region Page Event Handling

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        chkSubjects.DataBind();
        DocumentId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

        documentRepository = new DocumentRepository(DataHelper.EntityDataModel);

        if (Request.QueryString["MediaId"] != null)
        {
            var mediaId = (int) Conversion.Val(Request.QueryString["MediaId"]);

            ContentDocument media = ContentDocument.GetDocumentByFileId(mediaId);

            DocumentId = media.Id;
        }

        ShowItem();
    }

    ///// <summary>
    /////   Upload File and add it as a media file
    ///// </summary>
    ///// <param name="sender"> The source of the event. </param>
    ///// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    //protected void btnUploadFile_Click(object sender, EventArgs e)
    //{
    //    HttpPostedFile uploadedFile = fileUploadFile.PostedFile;

    //    if (IsValidFile(uploadedFile))
    //    {
    //        if (ProcessUpload(uploadedFile))
    //            ShowItem();

    //    }
    //}

    protected void btnSaveItem_Click(object sender, EventArgs e)
    {
        SaveItem();
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (DocumentId == 0)
        {
            SetFeedbackMessage("Missing guide Id! Did you save this item?");
            return;
        }
        ContentDocument.DeleteDocument(DocumentId);
        Response.RedirectPermanent("content.aspx");
    }


    //protected void btnPrintOnDemandItem_Click(object sender, EventArgs e)
    //{
    //    if (DocumentId == 0)
    //    {
    //        SaveItem();
    //    }
    //    var media = new DocumentFile { DocumentId = DocumentId, MediaTypeId = 7, URL = "http://stores.lulu.com/mises/" };
    //    media.AddNewSource();
    //    ShowItem();
    //}

    //protected void btnAddMisesShopItem_Click(object sender, EventArgs e)
    //{
    //    if (DocumentId == 0)
    //    {
    //        SaveItem();
    //    }
    //    var media = new DocumentFile { DocumentId = DocumentId, MediaTypeId = 5, URL = "http://stores.lulu.com/mises/" };
    //    media.AddNewSource();

    //    ShowItem();
    //}

    protected void btnSaveSource_Click(object sender, EventArgs e)
    {
        if (DocumentId == 0)
        {
            SaveItem();
        }

        AddFileRecordToDB(0, txtNewURL.Text, GetMediaTypeFromFileName(txtNewURL.Text), txtVolumeComment.Text);

        ShowItem();
    }

    protected void gvMediaFiles_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvMediaFiles.EditIndex = -1;
        ShowItem();
    }

    protected void gvMediaFiles_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // DocumentFile.DeleteSource(Convert.ToInt32(gvMediaFiles.DataKeys[e.RowIndex].Value));

        int fileId = Convert.ToInt32(gvMediaFiles.DataKeys[e.RowIndex].Value);


        documentRepository = new DocumentRepository(DataHelper.EntityDataModel);
        documentRepository.DeleteFile(fileId);


        ShowItem();
    }

    protected void gvMediaFiles_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvMediaFiles.EditIndex = e.NewEditIndex;
        ShowItem();
    }

    protected void gvMediaFiles_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileId = Convert.ToInt32(gvMediaFiles.DataKeys[e.RowIndex].Value);

        var db = DataHelper.EntityDataModel;
        var file = db.DocumentFiles.Single(f => f.FileId == fileId);

        file.MediaTypeId =
            Convert.ToInt32(((DropDownList) (gvMediaFiles.Rows[e.RowIndex].Cells[0].Controls[1])).SelectedValue);

        if (!string.IsNullOrWhiteSpace(e.NewValues[0].ToString()))
        {
            file.VolumeOrdinal = byte.Parse(e.NewValues[0].ToString());
        }

        if (e.NewValues[1] != null)
        {
            file.VolumeComment = e.NewValues[1].ToString();
        }
        if (e.NewValues[2] != null)
        {
            file.Display = (bool) e.NewValues[2];
        }

        file.URL = ((TextBox) (gvMediaFiles.Rows[e.RowIndex].Cells[1].Controls[1])).Text;


        int changed = db.SaveChanges();

        lblErrorMessage.Text += string.Format("{0} rows updated", changed);


        gvMediaFiles.EditIndex = -1;
        ShowItem();
    }

    #endregion Page events

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        LoadComplete += Page_LoadComplete;
        //btnUploadFile.Click += btnUploadFile_Click;
        btnSaveItem.Click += btnSaveItem_Click;
        btnDelete.Click += btnDelete_Click;

        //btnAddMisesShopItem.Click += btnAddMisesShopItem_Click;
        btnSaveSource.Click += btnSaveSource_Click;
        gvMediaFiles.RowCancelingEdit += gvMediaFiles_RowCancelingEdit;
        gvMediaFiles.RowDeleting += gvMediaFiles_RowDeleting;
        gvMediaFiles.RowEditing += gvMediaFiles_RowEditing;
        gvMediaFiles.RowUpdating += gvMediaFiles_RowUpdating;
    }

    public void ShowItem()
    {
        if (Request.QueryString["format"] == "media" ||
            (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Contains("media")))
        {
            litTitle.Text = "Edit Media";

            //  trPublishInfo.Visible = false;
            trSubjects.Visible = false;
            trTextContent.Visible = false;
            trUrlType.Visible = false;
        }
        else if (Request.QueryString["format"] == "document" || Request.QueryString["format"] == "literature" ||
                 (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Contains("literature")))
        {
            litTitle.Text = "Edit Document";

            // hide media fields
            trMediaGP.Visible = false;
            trMediaParent.Visible = false;
            trMediaCat.Visible = false;
        }


        btnDelete.Attributes.Add("onclick", "return confirm_delete();");
        lnkGUID.Text = Guid.NewGuid().ToString();

        ContentDocument document = DocumentId > 0 ? new ContentDocument(DocumentId) : new ContentDocument();

        ddMediaCategory.DataSource = document.GetMediaCategoryList();
        ddMediaCategory.DataBind();

        ddMediaCategoryParent.DataSource = document.GetParentMediaCategoryList();
        ddMediaCategoryParent.DataBind();

        ddMediaCategoryGrandparent.DataSource = document.GetParentMediaCategoryList();
        ddMediaCategoryGrandparent.DataBind();

        txtPublicationInformation.Text = document.PublicationInformation;
        txtSource.Text = document.Source;
        txtTitle.Text = document.Title;
        Page.Title = document.Title + " (#" + document.Id + ")";
        chkFeatured.Checked = document.Featured;


        txtProductId.Text = document.ProductId.ToString();

        txtDescription.Text = document.Description;
        txtMetaKeywords.Text = document.Keywords;

        if (document.ProductId > 0)
        {
            //    txtDescription.ReadOnly = true;
            //    txtDescription.Style.Add("color", "grey");
            litDescription.Text = "Description <br/>(Will also update store text)";
        }

        lnkToDocument.NavigateUrl = string.Format("http://{0}/document/{1}/{2}", Request.Url.Host, document.Id,
                                                  document.Title.ToSlug());
        lnkToDocument.Text = lnkToDocument.NavigateUrl;

        chkDisplayItem.Checked = document.Display;
        if (document.Author1 > 0)
        {
            AuthorSelect.Author1 = document.Author1;
        }
        if (document.Author1 == 2)
        {
            chkInternal.Checked = true;
        }
        if (document.Author2 > 0)
        {
            AuthorSelect.Author2 = document.Author2;
        }

        lnkGUID.Text = document.Identifier.ToString();
        lnkGUID.NavigateUrl = string.Format("/resources/{0}", document.Identifier);
        ddMediaCategory.Items.Insert(0, new ListItem("None", "0"));
        ddMediaCategoryParent.Items.Insert(0, new ListItem("None", "0"));
        ddMediaCategoryGrandparent.Items.Insert(0, new ListItem("None", "0"));

        try
        {
            if (document.CategoryId > 0)
            {
                ddMediaCategory.SelectedValue = Convert.ToString(document.CategoryId);
            }

            if (document.CategoryId > 0)
            {
                ddMediaCategoryParent.SelectedValue = Convert.ToString(document.ParentCategoryId);
            }

            if (document.CategoryId > 0)
            {
                ddMediaCategoryGrandparent.SelectedValue = Convert.ToString(document.GrandParentCategoryId);
            }

            //MisesModel.MediaCategory.


            foreach (ListItem subject in from ListItem subject in chkSubjects.Items
                                         from subjectId in
                                             document.SelectedSubjects.Where(
                                                 subjectId => Convert.ToInt32(subject.Value) == subjectId)
                                         select subject)
            {
                subject.Selected = true;
            }
        }
        catch
        {
        }


        if (DocumentId > 0)
        {
            // Get Media Files
            gvMediaFiles.DataSource = document.dtMediaFiles;
            gvMediaFiles.DataBind();

            DocumentTags1.DocumentIdentifier = document.Identifier;
            if (TagCloud != null)
            {
                TagCloud.DocumentIdentifier = document.Identifier;
            }
        }
    }

    public int SaveItem()
    {
        var document = new ContentDocument
            {
                Id = DocumentId,
                Author1 = chkInternal.Checked ? 2 : AuthorSelect.Author1,
                Author2 = AuthorSelect.Author2,
                ProductId = (int) Conversion.Val(txtProductId.Text),
                Display = chkDisplayItem.Checked,
                Featured = chkFeatured.Checked,
                Source = txtSource.Text,
                Title = txtTitle.Text,
                PublicationInformation = txtPublicationInformation.Text,
                Description = txtDescription.Text,
                Keywords = txtMetaKeywords.Text,
                CategoryId = Convert.ToInt32(Conversion.Val(ddMediaCategory.SelectedValue)),
                ParentCategoryId = Convert.ToInt32(Conversion.Val(ddMediaCategoryParent.SelectedValue)),
                GrandParentCategoryId =
                    Convert.ToInt32(Conversion.Val(ddMediaCategoryGrandparent.SelectedValue))
            };


        if (document.CategoryId == 0 && (document.ParentCategoryId > 0 || document.GrandParentCategoryId > 0))
        {
            SetFeedbackMessage("Cannot set parent category when category is not set.");
            return document.Id;
        }

        //chkSubjects.Items.ForEach(p => document.SelectedSubjects.Add(Convert.ToInt32(p.Value)));

        foreach (ListItem selectedSubject in
            chkSubjects.Items.Cast<ListItem>().Where(selectedSubject => selectedSubject.Selected))
        {
            document.SelectedSubjects.Add(Convert.ToInt32(selectedSubject.Value));
        }

        document.SaveDocument();
        DocumentId = document.Id;
        Master.Page.Title = document.Title + " (#" + document.Id + ")";
        lnkGUID.Text = document.Identifier.ToString();
        lnkGUID.NavigateUrl = string.Format("/resources/{0}", document.Identifier);

        SetFeedbackMessage("Item saved.");
        return DocumentId;
    }

    private void SetFeedbackMessage(string message)
    {
        lblErrorMessage.Text += message;
    }

    // Get ProductID from the SKU
    protected void txtSKU_TextChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtProductId.Text) && !string.IsNullOrWhiteSpace(txtSKU.Text))
        {
            var db =
                new AbleCommerceDBDataContext(DataHelper.StoreConnectionString);
            txtProductId.Text = db.ProductIdFromSKU(txtSKU.Text).FirstOrDefault().ProductId.ToString();
        }
    }

    protected void btnMergeWith_Click(object sender, EventArgs e)
    {
        int replacementId = int.Parse(txtMergeWithNumber.Text);
        var document = new ContentDocument(DocumentId);
        document.Title = "Duplicate: [" + replacementId + "] " + document.Title;
        document.Display = false;
        txtTitle.Text = document.Title;
        document.SaveDocument();
        lblErrorMessage.Text = string.Format("Redirect to #{0} saved.", replacementId);
    }

    #region File Upload Handling

    protected int Volume
    {
        get { return string.IsNullOrWhiteSpace(txtVolume.Text) ? 1 : int.Parse(txtVolume.Text); }
    }

    private int GetMediaTypeFromFileName(string fileName)
    {
        string fileExtension = Path.GetExtension(fileName);
        int selectedValue = 0;

        switch (fileExtension)
        {
            case ".mp3":
            case ".wav":
            case ".wma":
            case ".ogg":
            case ".au":
            case ".asf":
            case ".ogv":
                selectedValue = 1; // Audio
                break;
            case ".mpg":
            case ".avi":

            case ".mpeg":


            case ".divx":
                selectedValue = 2; // Video
                break;

            case ".mp4":
                selectedValue = 14; // Video
                break;
            case ".zip":
                selectedValue = 18; // Video
                break;

            case ".pdf":
                selectedValue = 3; // PDF
                break;
            case ".doc":
            case ".rtf":
                selectedValue = 4; // Document
                break;
            case ".html":
            case ".htm":
                selectedValue = 5; // HTML Pages
                break;
            case ".lit":
            case ".pbd":
            case ".txt":
                selectedValue = 6; // Offline Files
                break;
            case ".epub":
                selectedValue = 9;
                break;

            default:

                selectedValue = 4; // generic
                SetFeedbackMessage(
                    "Warning: This file type was not recognized!  Please ask the webmaster to fix this");
                break;
        }

        return selectedValue;
    }

    private bool IsValidFile(HttpPostedFile file)
    {
        bool bValid = file.FileName != string.Empty;
        if (file.ContentLength == 0)
        {
            SetFeedbackMessage("Error: No file selected!");
            bValid = false;
        }


        return bValid;
    }

    private bool IsDuplicate(string location)
    {
        bool bDuplicate = false;
        // Read file into a data stream

        if (File.Exists(location))
        {
            var info = new FileInfo(location);
            string backupFolder = DataHelper.DocumentFolder + @"\DocumentBackups\";

            if (!Directory.Exists(backupFolder))
            {
                Directory.CreateDirectory(backupFolder);
            }

            string backup = backupFolder + info.Name;

            try
            {
                if (File.Exists(backup))
                {
                    File.Delete(backup);
                }

                File.Move(location, backup);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionLogging.LogException(Context, ex);

                Thread.Sleep(1000);
                File.Delete(location);

                lblErrorMessage.Text += "  Unable to backup previous version of this file.";
            }


            bDuplicate = true;
        }
        return bDuplicate;
    }


    private string GetSavePath(string fileName, out int mediaTypeId)
    {
        string title = DataFormat.SanitizePathElement(txtTitle.Text);

        title = DataFormat.SanitizeFileName(title);

        if (Volume > 1)
        {
            title += "_Vol_" + Volume.ToString();
        }

        string folder;

        int mediaType = GetMediaTypeFromFileName(fileName);
        mediaTypeId = mediaType;

        if (mediaTypeId == 0)
        {
            // unrecognized file type, use original extension
        }

        var format = NavigationModels.Formats().FirstOrDefault(t => t.MediaTypeID == mediaType);
        if (format == null || !format.IsMedia)
        {
            // book
            int authorId = AuthorSelect.Author1;

            var db = DataHelper.EntityDataModel;
            var author = db.DocumentAuthors.FirstOrDefault(a => a.AuthorId == authorId);

            if (author != null)
            {
                string authorName = (author.AuthorFirst +
                                     (string.IsNullOrWhiteSpace(author.AuthorMiddle) ? "" : " " + author.AuthorMiddle)
                                     + " " + author.AuthorLast).Trim();

                folder = @"\books\" + DataFormat.SanitizePathElement(authorName);
            }
            else
            {
                SetFeedbackMessage("select an author first.");
                return null;
            }
        }
        else
        {
            // media

            if (ddMediaCategory.SelectedItem.Value == "0")
            {
                SetFeedbackMessage("error: select a media category");
                return null;
            }

            folder = @"\media\" + DataFormat.SanitizePathElement(ddMediaCategory.SelectedItem.Text);
        }

        folder = DataFormat.SanitizePath(folder);

        folder = folder + "\\" + title + Path.GetExtension(fileName);

        return folder;
    }


    private bool ProcessUpload(HttpPostedFile file)
    {
        int mediaTypeId = 0;

        string relativePath = GetSavePath(file.FileName, out mediaTypeId);

        if (relativePath == null) return false;

        relativePath = DataFormat.SanitizePath(relativePath);

        string absolutePath = DataHelper.DocumentFolder + relativePath;
        

        bool isNewVersionOfExistingFile = (IsDuplicate(absolutePath));

        var fileInfo = new FileInfo(absolutePath);
        
        if (!fileInfo.Directory.Exists)
            fileInfo.Directory.Create();

       // File.Move(file.ServerLocation, absolutePath);

        file.SaveAs(absolutePath);

        if (DocumentId == 0) SaveItem();

        string newFileUrl = "http://library.mises.org/" + relativePath.Replace("\\", "/");
        if (!isNewVersionOfExistingFile)
        {
            // new file

            SetFeedbackMessage("Added file to <a target='_blank' href=\"" + newFileUrl + "\">" + newFileUrl +
                               "</a> and the DB.");

            int fileId = AddFileRecordToDB(file.ContentLength, newFileUrl, mediaTypeId, txtVolumeComment.Text);
            //AddNewFileForDocument(file, mediaTypeId, newFileUrl);


            // update meta data
            var metaFile = new MediaFile {FileId = fileId, AbsolutePathToFile = absolutePath, DocumentId = DocumentId};

            metaFile.UpdateProperties();

            if (absolutePath.EndsWith(".epub", StringComparison.InvariantCultureIgnoreCase))
            {
                UpdateStoreFileVersion();
            }
        }
        else
        {
            // replacing old file 
            SetFeedbackMessage("Updated <a target='_blank' href=\"" + newFileUrl + "\">" + newFileUrl +
                               "</a> and backed up the previous version");

            // hack in case unspported file type is uploaded
            try
            {
                //AddNewFileForDocument(file, mediaTypeId, newFileUrl);
                int fileId = AddFileRecordToDB(file.ContentLength, newFileUrl, mediaTypeId, txtVolumeComment.Text);
            }
            catch (SqlException ex)
            {
                ExceptionLogging.LogException(Context, ex);
                lblErrorMessage.Text += "Uploaded, but error adding to DB: " + ex;
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(Context, ex);
                lblErrorMessage.Text += ex;
            }
        }


        return true;
    }


    private int AddFileRecordToDB(long size, string fileUrl, int mediaTypeId, string volumeComment)
    {
        int fileId = 0;

        var media = new DocumentFile
            {
                DocumentId = DocumentId,
                MediaTypeId = mediaTypeId,
                URL = fileUrl,
                fileSize = (int) size,
                VolumeOrdinal = (byte) Volume,
                VolumeComment = volumeComment,
                Display = true
            };
        try
        {
            documentRepository = new DocumentRepository(DataHelper.EntityDataModel);
            fileId = documentRepository.AddFile(media);
        }
        catch (SqlException exception)
        {
            ExceptionLogging.LogException(Request.RequestContext.HttpContext, exception);

            media = new DocumentFile
                {
                    DocumentId = DocumentId,
                    MediaTypeId = mediaTypeId,
                    URL = fileUrl,
                    fileSize = (int) size,
                    VolumeOrdinal = (byte) (Volume + 1)
                };
            try
            {
                fileId = documentRepository.AddFile(media);
                fileId = media.FileId;
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(Context, ex);

                SetFeedbackMessage(ex.Message);

                //                SetFeedbackMessage("Warning: Unsupported file type " + ex.Message);
            }
        }
        catch (Exception exception)
        {
            ExceptionLogging.LogException(Request.RequestContext.HttpContext, exception);
            SetFeedbackMessage("Error updating DB: " + exception);
        }

        return fileId;
    }

    #endregion File Upload Handling

    #region Update Store Books

    protected void btnUpdateStoreFiles_Click(object sender, EventArgs e)
    {
        txtSKU_TextChanged(null, null);
        UpdateStoreFileVersion();
    }

    private void UpdateStoreFileVersion()
    {
        try
        {
            string productId = txtProductId.Text;

            if (string.IsNullOrWhiteSpace(productId) || productId == "0") return;


            const string query = @"
SELECT  TOP 1 
df.URL  + ';' + ServerFileName 
FROM
	ac_Products prod1
	JOIN ac_ProductDigitalGoods
		ON ac_ProductDigitalGoods.ProductId = prod1.ProductId
	JOIN ac_DigitalGoods dg
		ON dg.DigitalGoodId = ac_ProductDigitalGoods.DigitalGoodId
	JOIN ac_Products prod2
		ON prod2.sku = REPLACE(prod1.Sku, 'EBOK', '')
	JOIN Mises..Documents AS docs
		ON prod2.ProductId = docs.ProductId
	JOIN Mises..DocumentFiles df
		ON docs.DocumentId = df.DocumentId AND df.MediaTypeId = 9
		
WHERE ServerFileName LIKE '%.epub'		
and docs.ProductId = @ProductId
";

            var temp = SqlHelper.ExecuteScalar(DataHelper.StoreConnectionString, CommandType.Text, query,
                                               new SqlParameter("@ProductId", productId));

            string[] books = new string[] {};
            if (temp != null)
                books = temp.ToString().Split(';');

            var model = DataHelper.EntityDataModel;

            var file = model.DocumentFiles.FirstOrDefault(d => d.URL.EndsWith(".epub") && d.DocumentId == DocumentId);

            string litName = books[0];

            string ableName = "";

            string ABLE_PATH = DataHelper.StoreFolder + @"\App_Data\DigitalGoods\";
            string BACKUP_PATH = DataHelper.StoreFolder + @"App_Data\DigitalGoods\Backups\";


            if (books.Length > 1)
                ableName = ABLE_PATH + books[1];

            string filePath;
            bool hasFile = DataFormat.GetPathFromURL(DataHelper.DocumentFolder, file.URL, out filePath);

            if (!hasFile)
            {
                lblErrorMessage.Text += (file.URL + " not found!");
                return;
            }

            litName = filePath;

            string backup = BACKUP_PATH + books[1];

            if (File.Exists(litName))
            {
                if (new FileInfo(ableName).Length != new FileInfo(litName).Length)
                {
                    if (File.Exists(backup))
                    {
                        File.Delete(backup);
                    }

                    lblErrorMessage.Text += ("Replacing: " + ableName + " with " + litName);

                    if (File.Exists(ableName))
                    {
                        File.Move(ableName, backup);
                    }
                    File.Copy(litName, ableName, true);

                    File.Replace(litName, ableName, backup);
                }
                else
                {
                    lblErrorMessage.Text += ("Identical: " + ableName + " with " + litName);
                }
            }
            else
            {
                File.Copy(ableName, litName);
                lblErrorMessage.Text += ("Missing file: " + litName);
            }
        }
        catch (Exception ex)
        {
            ExceptionLogging.LogException(Context, ex);

            lblErrorMessage.Text += ex;
        }
    }

    #endregion Update Store Books
}