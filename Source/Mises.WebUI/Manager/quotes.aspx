<%@ Page Title="Quote Manager" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" Inherits="Manager_quotes" Codebehind="quotes.aspx.cs" %>

<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
             Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <h1>
        Mises.org Quote Table Editor</h1>
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                  AutoGenerateColumns="False" DataKeyNames="QuoteId" DataSourceID="QuotesDataSource"
                  PageSize="500">
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="35" />
        <Columns>
            <asp:BoundField DataField="QuoteId" HeaderText="QuoteId" ReadOnly="True" SortExpression="QuoteId"
                            Visible="False" />
            <asp:TemplateField HeaderText="Quote" SortExpression="Quote">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="10" Text='<%#Bind("Quote")%>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Quote")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AuthorId" HeaderText="AuthorId" SortExpression="AuthorId" />
            <asp:BoundField DataField="Source" HeaderText="Source" SortExpression="Source" />
            <asp:BoundField DataField="Page" HeaderText="Page" SortExpression="Page" />
            <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />
            <asp:CommandField ButtonType="Button" ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
    </asp:GridView>
    <asp:EntityDataSource ID="QuotesDataSource" runat="server" ConnectionString="name=MisesEntities"
                          DefaultContainerName="MisesModel" EnableDelete="True" EnableInsert="True" EnableUpdate="True"
                          EntitySetName="Quotes" EntityTypeFilter="Quotes">
    </asp:EntityDataSource>
</asp:Content>