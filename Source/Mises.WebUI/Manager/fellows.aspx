﻿<%@ Page Title="Fellows Bio Manager" Language="C#" MasterPageFile="~/MasterPages/Manager.master"
         MaintainScrollPositionOnPostback="true" AutoEventWireup="true" ValidateRequest="false" Inherits="Manager_fellows" Codebehind="fellows.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="header">
        <h2 class="adminSection">
            Fellows Bio Manager
        </h2>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="control"
                  DataSourceID="sqlFellows">
        <Columns>
            <asp:CommandField ShowEditButton="True" ButtonType="Button" HeaderText="Edit" />
            <asp:BoundField DataField="control" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                            Visible="false" SortExpression="control" />
            <asp:TemplateField HeaderText="Name" SortExpression="name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("name")%>' Columns="100"
                                 Width="100%"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("name")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="HTML" SortExpression="bio" ControlStyle-Width="500">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("bio")%>' TextMode="MultiLine"
                                 CssClass="ckeditor" Width="100%" Rows="30"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("bio")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="image" HeaderText="Photo" SortExpression="image" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlFellows" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [fellows] WHERE [control] = @control" InsertCommand="INSERT INTO [fellows] ([name], [bio], [image]) VALUES (@name, @bio, @image)"
                       SelectCommand="SELECT [control], [name], [bio], [image] FROM [fellows]" UpdateCommand="UPDATE [fellows] SET [name] = @name, [bio] = @bio, [image] = @image WHERE [control] = @control">
        <DeleteParameters>
            <asp:Parameter Name="control" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="bio" Type="String" />
            <asp:Parameter Name="image" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="bio" Type="String" />
            <asp:Parameter Name="image" Type="String" />
            <asp:Parameter Name="control" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>