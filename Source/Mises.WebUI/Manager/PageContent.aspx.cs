#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Utility;

#endregion

partial class Manager_PageContent : Page
{
    protected void btnAddNewItem_Click(object sender, EventArgs e)
    {
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }

    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        try
        {
            gvPageContentList.DataBind();
            Cache.Remove(e.Values[1].ToString());
        }
        catch (Exception ex)
        {
            ExceptionLogging.LogException(this.Context, ex);
        }
    }

    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        try
        {
            gvPageContentList.DataBind();
            Cache.Remove(e.NewValues[1].ToString());
        }
        catch (Exception ex)
        {
            ExceptionLogging.LogException(this.Context, ex);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        btnAddNewItem.Click += btnAddNewItem_Click;
        DetailsView1.ItemInserted += DetailsView1_ItemInserted;
        DetailsView1.ItemUpdated += DetailsView1_ItemUpdated;
    }
}