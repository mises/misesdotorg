﻿<%@ Page Title="Donations Records Database for Mises.org" Language="C#" MasterPageFile="~/MasterPages/Manager.master"
         AutoEventWireup="true" Inherits="Manager_donations" Codebehind="donations.aspx.cs" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
             Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Donations Admin
        <asp:Literal ID="litTotal" runat="server"></asp:Literal></h1>
    <div id="chart">
        <img src="<%=((Manager_donations) Page).ChartUrl%>" alt="Exception Frequency Chart"
             height="350" width="700" />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                      AutoGenerateColumns="False" DataKeyNames="DonationID" DataSourceID="sqlDonationsList"
                      EnableSortingAndPagingCallbacks="True" PageSize="100" Width="100%">
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="DonationId" HeaderText="View" DataTextField="DonationId"
                                    DataNavigateUrlFormatString="donation.aspx?Id={0}" Text="View" />
                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Amount" DataFormatString="{0:c}" HeaderText="Amount" ReadOnly="True"
                                SortExpression="Amount" />
                <asp:CheckBoxField DataField="Recurring" HeaderText="Recurring" SortExpression="Recurring" />
                <asp:BoundField DataField="ResponseCode" HeaderText="Approved" SortExpression="ResponseCode" />
                <asp:CheckBoxField DataField="TestMode" HeaderText="Test Mode" SortExpression="TestMode" />
                <asp:BoundField DataField="CardType" HeaderText="CardType" SortExpression="CardType" />
                <asp:BoundField DataField="CreateTime" DataFormatString="{0:d}" HeaderText="Date"
                                SortExpression="CreateTime" />
            </Columns>
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="50" Position="TopAndBottom" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqlDonationsList" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                           SelectCommand="DonationGetList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </div>
    <%--    <asp:Chart ID="Chart1" runat="server" DataSourceID="sqlDonationReport" 
    Width="1000px">
    <series>
        <asp:Series Name="Series1" ChartType="Line" XValueMember="Date" 
            YValueMembers="Total" Legend="Legend1">
        </asp:Series>
    </series>
    <chartareas>
        <asp:ChartArea Name="ChartArea1">
        </asp:ChartArea>
    </chartareas>
</asp:Chart>

<asp:SqlDataSource ID="sqlDonationReport" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        SelectCommand="DonationGetReport" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="sqlDonationReport" AllowSorting="True" 
        EnableSortingAndPagingCallbacks="True">
        <Columns>
            <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" SortExpression="Date" />
            <asp:BoundField DataField="Total" DataFormatString="{0:c}" HeaderText="Total" ReadOnly="True"
                SortExpression="Total" />
        </Columns>
    </asp:GridView>--%>
</asp:Content>