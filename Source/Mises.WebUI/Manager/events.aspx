<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="Event Manager"
         ValidateRequest="false" Inherits="eventsManager" Codebehind="~/Manager/events.aspx.cs" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <a href="events.aspx">Mises Event Editor</a></h2>
    
    <asp:DetailsView ID="DetailsView2" runat="server" AutoGenerateRows="False" DataKeyNames="EventId"
                     DataSourceID="sqlEventDetailsForm" Height="50px" Width="100%" 
                     AllowPaging="True" OnItemInserted="DetailsView2_ItemInserted" 
                     DefaultMode="Edit" CellPadding="4" ForeColor="#333333" GridLines="None">
        <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
        <Fields>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                              ShowInsertButton="True" ButtonType="Button" />
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("Title")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Columns="80" Text='<%#Bind("Title")%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Columns="80" Text='<%#Bind("Title")%>'></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="Display" HeaderText="Display" SortExpression="Display" />
            <asp:TemplateField HeaderText="StartDate" SortExpression="StartDate">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%#Bind("StartDate")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    &nbsp;<ew:CalendarPopup ID="CalendarPopup2" runat="server" AllowArbitraryText="False"  DisableTextboxEntry="False" 
                                            CellPadding="2px" CellSpacing="0px" Culture="English (United States)" JavascriptOnChangeFunction=""
                                            LowerBoundDate="" SelectedDate='<%#Bind("StartDate")%>' UpperBoundDate="12/31/9999 23:59:59">
                              <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                 Font-Size="XX-Small" ForeColor="Black" />
                              <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                              <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                        
                            
                              <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Gray" />
                              <WeekEndStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                              <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Black" />
                          </ew:CalendarPopup>
                </EditItemTemplate>
                <InsertItemTemplate>
                    &nbsp;<ew:CalendarPopup ID="CalendarPopup3" runat="server" AllowArbitraryText="False"  DisableTextboxEntry="False" 
                                            CellPadding="2px" CellSpacing="0px" Culture="English (United States)" JavascriptOnChangeFunction=""
                                            LowerBoundDate="" SelectedDate='<%#Bind("StartDate")%>' UpperBoundDate="12/31/9999 23:59:59">
                              <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                 Font-Size="XX-Small" ForeColor="Black" />
                              <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                              <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                        
                            
                              <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Gray" />
                              <WeekEndStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                              <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Black" />
                          </ew:CalendarPopup>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EndDate" SortExpression="EndDate">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%#Bind("EndDate")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    &nbsp;<ew:CalendarPopup ID="CalendarPopup4" runat="server" AllowArbitraryText="False"  DisableTextboxEntry="False" 
                                            CellPadding="2px" CellSpacing="0px" Culture="English (United States)" JavascriptOnChangeFunction=""
                                            LowerBoundDate="" SelectedDate='<%#Bind("EndDate")%>' UpperBoundDate="12/31/9999 23:59:59">
                              <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                 Font-Size="XX-Small" ForeColor="Black" />
                              <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                              <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                        
                            
                              <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Gray" />
                              <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                              <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Black" />
                          </ew:CalendarPopup>
                </EditItemTemplate>
                <InsertItemTemplate>
                    &nbsp;<ew:CalendarPopup ID="CalendarPopup5" runat="server" AllowArbitraryText="False"  DisableTextboxEntry="False" 
                                            CellPadding="2px" CellSpacing="0px" Culture="English (United States)" JavascriptOnChangeFunction=""
                                            LowerBoundDate="" SelectedDate='<%#Bind("EndDate")%>' UpperBoundDate="12/31/9999 23:59:59">
                              <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                 Font-Size="XX-Small" ForeColor="Black" />
                              <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                              <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                        
                            
                              <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Gray" />
                              <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                              <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Black" />
                          </ew:CalendarPopup>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location" SortExpression="Location">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%#Bind("Location")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Columns="80" Text='<%#Bind("Location")%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Columns="80" Text='<%#Bind("Location")%>'></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>           
            <asp:TemplateField HeaderText="Image URL" SortExpression="EventImage">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%#Bind("EventImage")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Columns="80" Text='<%#Bind("EventImage")%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Columns="80" Text='<%#Bind("EventImage")%>'></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IntroText" SortExpression="IntroText">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%#Bind("IntroText")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Columns="80" Text='<%#Bind("IntroText")%>'
                                 TextMode="MultiLine" CssClass="ckeditor"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Columns="80" Text='<%#Bind("IntroText")%>'
                                 TextMode="MultiLine" CssClass="ckeditor"></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%#Bind("Description")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtEventDetails" runat="server" Height="500px" Width="100%" Text='<%#Bind("Description")%>'
                                 TextMode="MultiLine" CssClass="ckeditor"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtEventDetails" runat="server" Height="500px" Width="100%" Text='<%#Bind("Description")%>'
                                 TextMode="MultiLine" CssClass="ckeditor"></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
        </Fields>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerSettings PageButtonCount="50" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add New Event" />
        </EmptyDataTemplate>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    </asp:DetailsView>
  
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="EventId" DataSourceID="SqlDataSource1"
                  Width="100%" ForeColor="#333333" GridLines="None" PageSize="20" UseAccessibleHeader="False">
        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="EventId" 
                                DataNavigateUrlFormatString="events.aspx?Id={0}" HeaderText="Edit" 
                                Text="Edit" />
            <asp:HyperLinkField DataNavigateUrlFields="EventId" DataNavigateUrlFormatString="/events/{0}"
                                DataTextField="Title" HeaderText="Title" SortExpression="Title" />
            <asp:TemplateField HeaderText="Start Date" SortExpression="StartDate">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("StartDate")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    &nbsp;<ew:CalendarPopup ID="CalendarPopup6" runat="server" AllowArbitraryText="False"  DisableTextboxEntry="False" 
                                            CellPadding="2px" CellSpacing="0px" Culture="English (United States)" JavascriptOnChangeFunction=""
                                            LowerBoundDate="" SelectedDate='<%#Bind("StartDate")%>' UpperBoundDate="12/31/9999 23:59:59"
                                            VisibleDate="2005-06-12">
                              <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                 Font-Size="XX-Small" ForeColor="Black" />
                              <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                              <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                        
                            
                              <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Gray" />
                              <WeekEndStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                              <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Black" />
                          </ew:CalendarPopup>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date" SortExpression="EndDate">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#Bind("EndDate")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    &nbsp;<ew:CalendarPopup ID="CalendarPopup7" runat="server" AllowArbitraryText="False"  DisableTextboxEntry="False" 
                                            CellPadding="2px" CellSpacing="0px" Culture="English (United States)" JavascriptOnChangeFunction=""
                                            LowerBoundDate="" SelectedDate='<%#Bind("EndDate")%>' UpperBoundDate="12/31/9999 23:59:59"
                                            VisibleDate="2005-06-12">
                              <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                 Font-Size="XX-Small" ForeColor="Black" />
                              <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                              <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                        
                            
                              <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Gray" />
                              <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                              <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                              ForeColor="Black" />
                              <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                             Font-Size="XX-Small" ForeColor="Black" />
                          </ew:CalendarPopup>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Location" HeaderText="Location" 
                            SortExpression="Location" />
            <asp:CheckBoxField DataField="Display" HeaderText="Display" SortExpression="Display" />
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return confirm('Are you sure you want to delete this event?');"
                                    CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerSettings PageButtonCount="30" Position="TopAndBottom" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>    
    <asp:SqlDataSource ID="sqlEventDetailsForm" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="SELECT * FROM [Calendar] WHERE ([EventId] = @EventId)" DeleteCommand="DELETE FROM [Calendar] WHERE [EventId] = @EventId"
                       InsertCommand="INSERT INTO [Calendar] ( [Title], [EventDate], [StartDate], [EndDate], [IntroText], [EventImage], [Description], [Location], [Display]) VALUES (@Title, @EventDate, @StartDate, @EndDate, @IntroText, @EventImage, @Description, @Location, @Display)"


                       UpdateCommand="UPDATE [Calendar] SET [Title] = @Title, [EventDate] = @EventDate, [StartDate] = @StartDate, [EndDate] = @EndDate, [IntroText] = @IntroText, [EventImage] = @EventImage, [Description] = @Description, [Location] = @Location, [Display] = @Display WHERE [EventId] = @EventId">
        <DeleteParameters>
            <asp:Parameter Name="EventId" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="EventId" PropertyName="SelectedValue"
                                  Type="Int32" DefaultValue="" />
        </SelectParameters>
        <UpdateParameters>            
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="EventDate" Type="String" />
            <asp:Parameter Name="StartDate" Type="DateTime" />
            <asp:Parameter Name="EndDate" Type="DateTime" />
            <asp:Parameter Name="IntroText" Type="String" />
            <asp:Parameter Name="EventImage" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Location" Type="String" />
            <asp:Parameter Name="Display" Type="Boolean" />            
            <asp:Parameter Name="EventId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>            
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="EventDate" Type="String" />
            <asp:Parameter Name="StartDate" Type="DateTime" />
            <asp:Parameter Name="EndDate" Type="DateTime" />
            <asp:Parameter Name="IntroText" Type="String" />
            <asp:Parameter Name="EventImage" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Location" Type="String" />
            <asp:Parameter Name="Display" Type="Boolean" />            
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [Calendar] WHERE [EventId] = @EventId" InsertCommand="INSERT INTO [Calendar] ([Title], [EventDate], [EndDate], [IntroText], [EventImage], [Location], [Display]) VALUES (@Title, @EventDate, @EndDate, @IntroText, @EventImage, @Location, @Display)"
                       SelectCommand="SELECT EventId, Title, EventDate, EndDate, IntroText, EventImage, Location, Display, StartDate FROM Calendar ORDER BY EndDate DESC, CreateDate DESC"
        
                       UpdateCommand="UPDATE [Calendar] SET [Title] = @Title, [EventDate] = @EventDate, [EndDate] = @EndDate, [IntroText] = @IntroText, [EventImage] = @EventImage, [Location] = @Location, [Display] = @Display WHERE [EventId] = @EventId">
        <DeleteParameters>
            <asp:Parameter Name="EventId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="EventDate" Type="String" />
            <asp:Parameter Name="EndDate" Type="DateTime" />
            <asp:Parameter Name="IntroText" Type="String" />
            <asp:Parameter Name="EventImage" Type="String" />
            <asp:Parameter Name="Location" Type="String" />
            <asp:Parameter Name="Display" Type="Boolean" />
            <asp:Parameter Name="EventId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="EventDate" Type="String" />
            <asp:Parameter Name="EndDate" Type="DateTime" />
            <asp:Parameter Name="IntroText" Type="String" />
            <asp:Parameter Name="EventImage" Type="String" />
            <asp:Parameter Name="Location" Type="String" />
            <asp:Parameter Name="Display" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>