﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true"
    Inherits="Manager_library" CodeBehind="library.aspx.cs" %>

<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <a href="library.aspx">Ward and Massey Library Admin</a></h2>
        <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
            <a href="book.aspx" class="ui-button">Add a Book</a>
        </div>
        <cc1:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" DataKeyNames="control" DataSourceID="sqlViewBooks"
            PageSize="50">
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="control" HeaderText="View" DataTextField="control"
                    SortExpression="control" InsertVisible="False" DataNavigateUrlFormatString="book.aspx?Id={0}"
                    Text="View" />
                <asp:BoundField DataField="title" HeaderText="Book Title" SortExpression="title" />
                <asp:BoundField DataField="authorfirst" HeaderText="First Name" SortExpression="authorfirst" />
                <asp:BoundField DataField="authorlast" HeaderText="Last Name" SortExpression="authorlast" ControlStyle-Width="50px" />
                <asp:BoundField DataField="editorfirst" HeaderText="Editor First Name" SortExpression="editorfirst" />
                <asp:BoundField DataField="editorlast" HeaderText="Editor Last Name" SortExpression="editorlast" />
                <asp:BoundField DataField="CreateDate" HeaderText="Added" SortExpression="CreateDate"
                    DataFormatString="{0:d}" />
            </Columns>
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="30" Position="TopAndBottom" />
        </cc1:GridView>
        <asp:SqlDataSource ID="sqlViewBooks" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            SelectCommand="SELECT [control], [title], [authorfirst], [authorlast], [editorfirst], [editorlast], [CreateDate] FROM [WardLibrary] ORDER BY authorlast ASC, authorfirst ASC, editorlast, editorfirst, title">
        </asp:SqlDataSource>
</asp:Content>
