#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class Manager_MediaTypes : Page
{
    protected void btnNewMediaType_Click(object sender, EventArgs e)
    {
        dvMediaTypeEdit.ChangeMode(DetailsViewMode.Insert);
    }

    protected void dvMediaTypeEdit_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        gvMediaTypeList.DataBind();
    }

    protected void dvMediaTypeEdit_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        gvMediaTypeList.DataBind();
    }

    protected void dvMediaTypeEdit_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        gvMediaTypeList.DataBind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        btnNewMediaType.Click += btnNewMediaType_Click;
        dvMediaTypeEdit.ItemDeleted += dvMediaTypeEdit_ItemDeleted;
        dvMediaTypeEdit.ItemInserted += dvMediaTypeEdit_ItemInserted;
        dvMediaTypeEdit.ItemUpdated += dvMediaTypeEdit_ItemUpdated;
    }
}