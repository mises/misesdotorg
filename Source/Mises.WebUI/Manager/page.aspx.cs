#region

using System;
using System.Linq;
using System.Web.UI;
using Mises.Data;
using Mises.Domain.Documents;
using Mises.Domain.Utility;
using d = Mises.Domain.Documents;

#endregion

public partial class Manager_page : Page
{
    private void OpenPage(int id)
    {
        var model = DataHelper.EntityDataModel;
        var myPage = model.PageTableSet.SingleOrDefault(p => p.PageId == id) ?? new PageTable();

        ViewState["PageId"] = myPage.PageId;

        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;

        lnkGUID.Text = myPage.GUID.ToString();
        txtPageTitle.Text = myPage.pageTitle;
        txtPageContent.Text = myPage.content;
        txtPageKeywords.Text = myPage.metaKeywords;
        txtPageDescription.Text = myPage.metaDescription;
        litTitle.Text = "Editing " + myPage.pageTitle;
        Page.Title = litTitle.Text;

        lnkGUID.Text = myPage.GUID.ToString();
        lnkGUID.NavigateUrl = baseUrl + "/resources/" + myPage.GUID;

        lnkURL.NavigateUrl = string.Format("/page/{0}/{1}", myPage.PageId, myPage.pageTitle.ToSlug());
        lnkURL.Text = lnkURL.NavigateUrl;

    }

    private void SavePage()
    {
        int pageId = 0;

        if (ViewState["PageId"] != null)
            pageId = (int)ViewState["PageId"];


        var model = DataHelper.EntityDataModel;
        var page = model.PageTableSet.SingleOrDefault(p => p.PageId == pageId) ?? new PageTable();

        if (page.PageId == 0)
        {
            page.GUID = Guid.NewGuid();
        }

        page.content = txtPageContent.Text;
        page.metaKeywords = txtPageKeywords.Text;
        page.metaDescription = txtPageDescription.Text;
        page.pageTitle = txtPageTitle.Text;
        page.CreateDate = DateTime.Now;
        page.EditDate = DateTime.Now;

        page.Visible = true;


        if (page.PageId == 0)
        {
            model.PageTableSet.AddObject(page);
        }

        model.SaveChanges();

        pageId = page.PageId;
        ViewState["PageId"] = pageId;

        litTitle.Text = "Updated " + Title;
        lnkURL.NavigateUrl = string.Format("/page/{0}/{1}", pageId, page.pageTitle.ToSlug());
        lnkURL.Text = lnkURL.NavigateUrl;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["PageId"] != null)
            {
                OpenPage(int.Parse(Request.QueryString["PageId"]));
            }

            btnDelete.Attributes.Add("onclick", "return confirm_delete();");
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        SavePage();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        OpenPage(d.Document.GetNextId((int)ViewState["PageId"]));
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int pageId = (int)ViewState["PageId"];
        var model = DataHelper.EntityDataModel;
        var page = model.PageTableSet.SingleOrDefault(p => p.PageId == pageId) ?? new PageTable();
        model.PageTableSet.DeleteObject(page);

    }

    protected void btnTidy_Click(object sender, EventArgs e)
    {
        txtPageContent.Text = HTMLtidy.TidyHTML(txtPageContent.Text);
    }
}