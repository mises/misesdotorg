<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" AutoEventWireup="false" Inherits="Manager_MediaTypes" title="Media Type Manager" Codebehind="MediaTypes.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>
        Media Types/Document Formats</h2>
    <br />
    <asp:GridView ID="gvMediaTypeList" runat="server" AllowPaging="True" AllowSorting="True"
                  AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MediaTypeId" DataSourceID="sqlMediaTypeList"
                  ForeColor="#333333" GridLines="None" UseAccessibleHeader="False">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:ImageField DataAlternateTextField="MediaIconPath" DataImageUrlField="MediaIconPath">
            </asp:ImageField>
            <asp:BoundField DataField="MediaTypeId" HeaderText="MediaTypeId" InsertVisible="False"
                            ReadOnly="True" SortExpression="MediaTypeId" />
            <asp:BoundField DataField="MediaType" HeaderText="MediaType" SortExpression="MediaType" />
            <asp:CommandField HeaderText="Edit" ShowSelectButton="True" />
        </Columns>
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="sqlMediaTypeList" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="select MediaTypeId, MediaType, MediaIconPath from DocumentMediaType">
    </asp:SqlDataSource>
    <br />
    <h4>
        Edit Media Type:</h4>
    &nbsp;<asp:DetailsView ID="dvMediaTypeEdit" runat="server" AllowPaging="True"
                           AutoGenerateRows="False" CellPadding="4" DataKeyNames="MediaTypeID" DataSourceID="sqlEditMediaType"
                           ForeColor="#333333" GridLines="None" Height="50px" Width="125px" DefaultMode="Edit">
              <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
              <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
              <EditRowStyle BackColor="#2461BF" />
              <RowStyle BackColor="#EFF3FB" />
              <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
              <Fields>
                  <asp:BoundField DataField="MediaTypeID" HeaderText="MediaTypeID" InsertVisible="False"
                                  ReadOnly="True" SortExpression="MediaTypeID" />
                  <asp:BoundField DataField="MediaType" HeaderText="MediaType" SortExpression="MediaType" />
                  <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                  <asp:BoundField DataField="MediaIconPath" HeaderText="Media Icon Path" SortExpression="MediaIconPath" />
                  <asp:BoundField DataField="Extensions" HeaderText="Extensions" SortExpression="Extensions" />
                  <asp:BoundField DataField="UploadPath" HeaderText="Upload Folder" SortExpression="UploadPath" />
                  <asp:BoundField DataField="CreateTime" HeaderText="Created" SortExpression="CreateTime" />
                  <asp:BoundField DataField="MIMEtype" HeaderText="MIME type" SortExpression="MIMEtype" />
                  <asp:CheckBoxField DataField="IsDeleted" HeaderText="IsDeleted" SortExpression="IsDeleted" />
                  <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
              </Fields>
              <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
              <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
              <AlternatingRowStyle BackColor="White" />
          </asp:DetailsView>
    <asp:Button ID="btnNewMediaType" runat="server" Text="New Media Type" />
    <asp:SqlDataSource ID="sqlEditMediaType" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [DocumentMediaType] WHERE [MediaTypeID] = @MediaTypeID"
                       InsertCommand="INSERT INTO [DocumentMediaType] ([MediaType], [MediaIconPath], [Extensions], [UploadPath], [Description], [CreateTime], [MIMEtype], [IsDeleted]) VALUES (@MediaType, @MediaIconPath, @Extensions, @UploadPath, @Description, getdate(), @MIMEtype, @IsDeleted)"
                       SelectCommand="SELECT * FROM [DocumentMediaType] WHERE ([MediaTypeID] = @MediaTypeID)"
                       UpdateCommand="UPDATE [DocumentMediaType] SET [MediaType] = @MediaType, [MediaIconPath] = @MediaIconPath, [Extensions] = @Extensions, [UploadPath] = @UploadPath, [Description] = @Description, [CreateTime] = @CreateTime, [MIMEtype] = @MIMEtype, [IsDeleted] = @IsDeleted WHERE [MediaTypeID] = @MediaTypeID">
        <DeleteParameters>
            <asp:Parameter Name="MediaTypeID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="MediaType" Type="String" />
            <asp:Parameter Name="MediaIconPath" Type="String" />
            <asp:Parameter Name="Extensions" Type="String" />
            <asp:Parameter Name="UploadPath" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="CreateTime" Type="DateTime" />
            <asp:Parameter Name="MIMEtype" Type="String" />
            <asp:Parameter Name="IsDeleted" Type="Boolean" />
            <asp:Parameter Name="MediaTypeID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="gvMediaTypeList" DefaultValue="0" Name="MediaTypeID"
                                  PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="MediaType" Type="String" />
            <asp:Parameter Name="MediaIconPath" Type="String" />
            <asp:Parameter Name="Extensions" Type="String" />
            <asp:Parameter Name="UploadPath" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="CreateTime" Type="DateTime" />
            <asp:Parameter Name="MIMEtype" Type="String" />
            <asp:Parameter Name="IsDeleted" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>