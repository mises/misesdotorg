<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="Article Editor" ClientIDMode="Static"
    MaintainScrollPositionOnPostback="true" ValidateRequest="false" Inherits="article"
    CodeBehind="article.aspx.cs" %>

<%@ Register Src="ManagerControls/DocumentTags.ascx" TagName="DocumentTags" TagPrefix="uc1" %>

<%@ Register Src="ManagerControls/RevisionHistory.ascx" TagName="RevisionHistory"
    TagPrefix="uc3" %>
<%@ Register Src="ManagerControls/AuthorSelect.ascx" TagName="AuthorSelect" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Literal ID="litTitle" runat="server">New Article</asp:Literal><br />
    </h2>
    <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" Font-Bold="True" Font-Size="14pt"
        ForeColor="Red" CssClass="ui-state-error-text"></asp:Label>
    <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
        <asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
            CssClass="ui-state-default ui-corner-all ui-button ui-button-icon-primary"></asp:Button>
        <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="ui-state-default ui-corner-all ui-button">
        </asp:Button>
        <asp:Button ID="btnDelete" OnClientClick="return confirm('Are you sure you want to delete this article?');"
            runat="server" Text="Delete" CssClass="ui-state-default ui-corner-all ui-button">
        </asp:Button>
        <asp:Button ID="btnTidyHTML" runat="server" Text="Tidy HTML" CssClass="ui-state-default ui-corner-all ui-button"
            Visible="False"></asp:Button>
    </div>
    <table style="margin-right: auto; margin-left: auto" border="0" width="800">
        <tr>
            <td align="left" class="detailheader" valign="top">
                URL
            </td>
            <td align="left" class="detailtext">
                <asp:HyperLink CssClass="ui-icon-link ui-state-default" ID="lnkURL" runat="server"
                    Target="_New"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                ID:
                <asp:HyperLink ID="lnkArticleId" runat="server" Font-Bold="True">0</asp:HyperLink>
            </td>
            <td class="detailtext">
                <asp:CheckBox ID="chkVisible" runat="server" Text="Published"></asp:CheckBox>
                <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured on Homepage"></asp:CheckBox>
                <asp:CheckBox ID="chkHeadline" runat="server" Text="#1 Feature"></asp:CheckBox>
                &nbsp;
                <label for="calArticleDate">
                    Date</label>
                <asp:TextBox runat="server" ID="calArticleDate" ClientIDMode="Static" CssClass="datepicker"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Title
            </td>
            <td class="detailtext">
                <asp:TextBox CssClass="textfield" ID="txtTitle" runat="server" Columns="80"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Headline Text
            </td>
            <td class="detailtext">
                <asp:TextBox CssClass="textfield" ID="txtHeadlineText" runat="server" Columns="80"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" class="detailheader">
                Author
            </td>
            <td align="left" class="detailtext">
                &nbsp;<uc2:AuthorSelect ID="AuthorSelect1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Intro Text
            </td>
            <td class="detailtext">
                <asp:TextBox CssClass="textfield" ID="txtIntro" runat="server" Columns="80" TextMode="MultiLine"
                    Rows="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader" style="vertical-align: top">
                <strong>Article Text</strong>
                <br />
                Valid Tags:
                <pre style="font-style: italic">
[product:0]
[product:#]
[course:0]
[course:#]
[image]
[thumb]
[AuthorName]
[AuthorName2]
[AuthorArchive]
[AuthorArchive2]
[RSSfeed]
[RSSfeed2]
[comment]
[Embed:#]
[bio]
[LatestArticle]
[AuthorPhoto]
[AuthorPhoto]
[bio2]
[LatestArticle2]
[AuthorPhoto2]
</pre>
            </td>
            <td valign="top" class="detailheader">
                <asp:TextBox ID="txtFullText" runat="server" Height="500px" Width="100%" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" style="font-weight: bold">
                Photo
            </td>
            <td class="detailtext">
                <asp:Image ID="imgPhoto" runat="server" Height="100" />
                <asp:FileUpload ID="uploadPhotoUrl" runat="server" />
                (Use [image] to render the image tag)<br />
                <asp:Button ID="btnCopyImages" runat="server" OnClick="btnCopyImages_Click" Text="Or copy from #:" />
                <asp:TextBox ID="txtCopyFromId" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" style="font-weight: bold">
                Thumbnail
            </td>
            <td class="detailtext">
                <asp:Image ID="imgThumbnail" runat="server" Height="50" />
                <asp:FileUpload ID="uploadThumbnail" runat="server" />
                <br />
                (Optional - autogenerated from photo url)
            </td>
        </tr>
        <tr>
            <td class="detailheader" style="font-weight: bold" valign="top">
                Thumbnail<br />
                Height:
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtPhotoHeight" runat="server">130</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" style="font-weight: bold" valign="top">
                Edited:
            </td>
            <td class="detailtext">
                <asp:Label runat="server" ID="lblEditedBy"></asp:Label>
                <br />
                <uc3:RevisionHistory ID="RevisionHistory1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                Tags
            </td>
            <td class="detailtext">
                <uc1:DocumentTags ID="DocumentTags1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader">
                File Names
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtPhotoUrl" runat="server" Columns="80" ReadOnly="True"></asp:TextBox>
                <asp:TextBox ID="txtThumbnailUrl" runat="server" Columns="80" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
