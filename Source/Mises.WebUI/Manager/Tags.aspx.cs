#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Tags;

#endregion

partial class Manager_Tags : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void UpdateStats(int rows)
    {
        lblStatus.Text = string.Format("{0} rows updated.", rows);
    }


    protected void btnDeleteTag_Click(object sender, EventArgs e)
    {
        UpdateStats(Tagging.DeleteTag(txtTag.Text));
    }

    protected void btnDeleteUserTags_Click(object sender, EventArgs e)
    {
        UpdateStats(Tagging.DeleteUserTags(txtUser.Text));
    }

    protected void btnReplaceTag_Click(object sender, EventArgs e)
    {
        UpdateStats(Tagging.UpdateTag(txtOldTag.Text, txtNewTag.Text));
    }


    protected void gvAnonymousTaggers_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void gvAnonymousTaggers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "DeleteTag":
                UpdateStats(Tagging.DeleteTag(e.CommandArgument.ToString()));
                gvAnonymousTaggers.DataBind();
                break;
            case "DeleteUser":
                if (e.CommandArgument.ToString() == "SearchEngineBot")
                {
                    throw new Exception("You don't want to delete ALL search engine tags!");
                }
                UpdateStats(Tagging.DeleteUserTags(e.CommandArgument.ToString()));
                gvAnonymousTaggers.DataBind();
                break;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        btnDeleteTag.Click += btnDeleteTag_Click;
        btnDeleteUserTags.Click += btnDeleteUserTags_Click;
        btnReplaceTag.Click += btnReplaceTag_Click;
        gvAnonymousTaggers.SelectedIndexChanged += gvAnonymousTaggers_SelectedIndexChanged;
        gvAnonymousTaggers.RowCommand += gvAnonymousTaggers_RowCommand;
    }
}