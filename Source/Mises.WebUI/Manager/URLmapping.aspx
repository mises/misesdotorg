<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false"
    Inherits="Manager_URLmapping" Title="URL Redirect Mappings" CodeBehind="URLmapping.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        404 Redirect Mappings</h2>
    <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
        From:
        <asp:textbox id="txtRequestedURL" runat="server"></asp:textbox>
        To:
        <asp:textbox id="txtRedirectURL" runat="server"></asp:textbox>
        <asp:button id="btnAddRedirect" runat="server" text="Add Redirect" CssClass="ui-state-default ui-corner-all ui-button ui-button-icon-primary" />
    </div>
    <asp:gridview id="gvURLs" runat="server" allowpaging="True" allowsorting="True" autogeneratecolumns="False"
        datakeynames="RedirectId" datasourceid="sqlRedirectedURL" pagesize="250">
		<PagerSettings Mode="NextPreviousFirstLast" Position="TopAndBottom" />
		<Columns>
			<asp:BoundField DataField="RequestedURL" HeaderText="Requested URL" SortExpression="RequestedURL" />
			<asp:BoundField DataField="RedirectURL" HeaderText="Redirect URL" SortExpression="RedirectURL" />
			<asp:CheckBoxField DataField="ExactMatchOnly" HeaderText="Exact Match" SortExpression="ExactMatchOnly" />
			<asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority" />
			<asp:CommandField HeaderText="Edit" ShowDeleteButton="True" ShowEditButton="True" />
		</Columns>
	</asp:gridview>
    <asp:sqldatasource id="sqlRedirectedURL" runat="server" connectionstring="<%$ ConnectionStrings:Public %>"
        deletecommand="DELETE FROM [RedirectedURL] WHERE [RedirectId] = @RedirectId"
        insertcommand="INSERT INTO [RedirectedURL] ([RequestedURL], [RedirectURL], [Priority], [ExactMatchOnly]) VALUES (@RequestedURL, @RedirectURL, @Priority, @ExactMatchOnly)"
        selectcommand="SELECT [RedirectId], [RequestedURL], [RedirectURL], [Priority], [ExactMatchOnly] FROM [RedirectedURL] ORDER BY RedirectID DESC"
        updatecommand="UPDATE [RedirectedURL] SET [RequestedURL] = @RequestedURL, [RedirectURL] = @RedirectURL, [Priority] = @Priority, [ExactMatchOnly] = @ExactMatchOnly WHERE [RedirectId] = @RedirectId">
		<DeleteParameters>
			<asp:Parameter Name="RedirectId" Type="Int32" />
		</DeleteParameters>
		<UpdateParameters>
			<asp:Parameter Name="RequestedURL" Type="String" />
			<asp:Parameter Name="RedirectURL" Type="String" />
			<asp:Parameter Name="Priority" Type="Int32" />
			<asp:Parameter Name="ExactMatchOnly" Type="Boolean" />
			<asp:Parameter Name="RedirectId" Type="Int32" />
		</UpdateParameters>
		<InsertParameters>
			<asp:Parameter Name="RequestedURL" Type="String" />
			<asp:Parameter Name="RedirectURL" Type="String" />
			<asp:Parameter Name="Priority" Type="Int32" />
			<asp:Parameter Name="ExactMatchOnly" Type="Boolean" />
		</InsertParameters>
	</asp:sqldatasource>
</asp:Content>
