﻿#region

using System;
using System.Data;
using System.Web.UI;
using Mises.Data;

#endregion

partial class menu : Page
{
    protected void btnNewMenu_Click(object sender, EventArgs e)
    {
        string SQL = "INSERT INTO MainMenu (menuTitle,menuURL,menuSection) VALUES ('" +
                     txtTitle.Text + "','" + txtURL.Text + "','" + txtSection.Text + "')";
        SqlHelper.ExecuteNonQuery(DataHelper.ConnectionString, CommandType.Text, SQL);
        DataBind();
    }
}