﻿<%@ Page Title="Mises Razors Edge Database" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" CodeBehind="donors.aspx.cs" Inherits="Mises.Web.Manager.donors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2>Mises.org Donor Database</h2>
    

    <div class="ui-widget ui-corner-all" style="border: 1px black solid;background-color:lightgrey;">
        <strong>Search By:</strong>
        <ul>
            <li>Name 
                <input type="type" name="name" value=" " /></li>
            <li>Email 
                <input type="type" name="name" value=" " /></li>
            <li>Phone 
                <input type="type" name="name" value=" " /></li>
            <li>Address 
                <input type="type" name="name" value=" " /></li>
            <li>Gift Range
                
                from 
                <input type="type" name="name" value="$" />
                to
                <input type="type" name="name" value="$" />
            </li>
        </ul>
        

        <asp:Button Text="Find Records" runat="server" CssClass="ui-button-text" />

    </div>    


    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="100"
        AllowSorting="True" AutoGenerateColumns="False" 
        DataKeyNames="DonorId" DataSourceID="sqlDonorTable">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="DonorId" 
                DataNavigateUrlFormatString="donor.aspx?DonorId={0}" HeaderText="Select" Text="Select" />
            <asp:BoundField DataField="DonorId" HeaderText="DonorId" InsertVisible="False" Visible="False" 
                ReadOnly="True" SortExpression="DonorId" />
            <asp:BoundField DataField="Email1" HeaderText="Email" 
                SortExpression="Email1" ReadOnly="True" />
            <asp:BoundField DataField="Email2" HeaderText="Phone" 
                SortExpression="Email2" ReadOnly="True" />
            <asp:BoundField DataField="First_Name" HeaderText="First" 
                SortExpression="First_Name" ReadOnly="True" />
            <asp:BoundField DataField="Last_Name" HeaderText="Last" 
                SortExpression="Last_Name" ReadOnly="True" />
            <asp:BoundField DataField="Middle_Name" HeaderText="Middle" 
                SortExpression="Middle_Name" ReadOnly="True" />
            <asp:BoundField DataField="DateAdded" HeaderText="Added" 
                SortExpression="DateAdded" ReadOnly="True" />
            <asp:BoundField DataField="Total_nmbr_of_Gifts" 
                HeaderText="Gifts" SortExpression="Total_nmbr_of_Gifts" ReadOnly="True" />
            <asp:BoundField DataField="Total_Gift_amount" HeaderText="Gift $" 
                SortExpression="Total_Gift_amount" DataFormatString="{0:c}" 
                ReadOnly="True" />
        </Columns>
        <PagerSettings PageButtonCount="100" 
            Position="TopAndBottom" />
    </asp:GridView>
    <asp:SqlDataSource ID="sqlDonorTable" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Public %>" 
        SelectCommand="SELECT DonorId, Email1,Email2, First_Name, Last_Name, Middle_Name,DateAdded,Total_nmbr_of_Gifts,Total_Gift_amount 
        FROM [DonorList] Order By DonorId desc"></asp:SqlDataSource>
</asp:Content>
