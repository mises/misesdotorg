#region

using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;


#endregion

namespace MisesWeb.Manager
{
    partial class DailyArticlesArchiveManager : Page
    {
        //private string Letter;
        private string Month;

        private void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }

            var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));

            gvRecentEdits.DataSource = db.RevisionGetRecent();
            gvRecentEdits.DataBind();

            if (Request.QueryString["action"] == "search")
            {
                if (gvArchives.Rows.Count > 0)
                {
                    lblTitle.Text = "Daily Articles Search: " + gvArchives.Rows.Count + " results";
                }
                else
                {
                    lblTitle.Text = "Sorry, no results for \"" + Request.QueryString["q"];
                }
            }

            if (Request.QueryString["AuthorId"] != null && gvArchives.Rows.Count > 0)
            {
                // Show particular author
                lblTitle.Text = gvArchives.DataKeys[0]["AuthorName"].ToString();

                var hm = new HtmlMeta {Name = "Description", Content = "Daily Article archive for " + lblTitle.Text};
                Page.Header.Controls.Add(hm);
                Page.Title = hm.Content;

                var link = new HtmlLink();
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;
                link.Attributes.Add("type", "application/rss+xml");
                link.Attributes.Add("rel", "alternate");
                link.Attributes.Add("title", "Daily Article feed for " + lblTitle.Text);
                link.Attributes.Add("href",
                                    string.Format("{0}/Feeds/articles.ashx?AuthorId={1}", baseUrl,
                                                  Convert.ToInt32(Conversion.Val(Request.QueryString["AuthorId"]))));
                Page.Header.Controls.AddAt(1, link);

                //If dsAuthors.Tables[0].Rows(0).Item("Photo").ToString <> "" Then
                //    Me.lblTitle.Text &= "<img src=""" & dsAuthors.Tables[0].Rows(0).Item("Photo").ToString & """ alt=""" & dsAuthors.Tables[0].Rows(0).Item("AuthorName").ToString & """ align=""center""><br/>"

                //End If
            }
        }

        protected void gvArchives_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // Add Monthly Breaks
            if (Request.QueryString["action"] == "" && Request.QueryString["AuthorId"] == "" &&
                (e.Row.RowType == DataControlRowType.DataRow)) // if title view
            {
                if (! (gvArchives.SortExpression == "" || gvArchives.SortExpression.Contains("DatePosted")))
                {
                    return;
                }

                string currentMonth = gvArchives.DataKeys[e.Row.RowIndex].Values[0].ToString();
                if (Month != currentMonth)
                {
                    Month = currentMonth;
                    var link = new HyperLink();
                    var lit = new Literal();

                    link = (HyperLink) (e.Row.Cells[1].FindControl("HyperLink1"));
                    string archives = "</td></tr>" + Environment.NewLine + "<tr><th colspan=\"5\">" +
                                      Environment.NewLine;
                    archives += "<h4 >" + currentMonth + " " + Convert.ToDateTime(e.Row.Cells[2].Text).Year + "</h4>" +
                                Environment.NewLine;
                    archives += "</th></tr>" + Environment.NewLine + "<tr><td>" + Environment.NewLine;
                    lit.Text = archives;
                    e.Row.Cells[0].Controls.Add(lit);
                    e.Row.Cells[0].Controls.Add(link);
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Load += Page_Load;
            gvArchives.RowDataBound += gvArchives_RowDataBound;
        }
    }
}