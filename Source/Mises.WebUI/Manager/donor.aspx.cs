﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mises.Web.Manager
{
    public partial class Donor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["p"] == null || (string) Session["p"] != "Mises123")
            {
                throw new Exception("Not authorized");
            }
        }
    }
}