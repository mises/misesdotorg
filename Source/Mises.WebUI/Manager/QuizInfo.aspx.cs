#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

partial class Manager_QuizInfoPage : Page
{
    protected void btnAddNewItem_Click(object sender, EventArgs e)
    {
        dvDetailEdit.ChangeMode(DetailsViewMode.Insert);
    }

    protected void dvDetailEdit_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        Cache.Remove(e.Values[1].ToString());
        gvQuizContentList.DataBind();
    }

    protected void dvDetailEdit_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        Cache.Remove(e.NewValues[1].ToString());
        gvQuizContentList.DataBind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        btnAddNewItem.Click += btnAddNewItem_Click;
        dvDetailEdit.ItemInserted += dvDetailEdit_ItemInserted;
        dvDetailEdit.ItemUpdated += dvDetailEdit_ItemUpdated;
    }
}