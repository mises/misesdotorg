<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" AutoEventWireup="false"
    ValidateRequest="false" MaintainScrollPositionOnPostback="true" Inherits="ContentManagerPage"
    Title="Mises.org Content Manager" CodeBehind="content.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <h1 style="text-align: center;">
            <a href="content.aspx" title="The Austrian Economics Content Manager">Documents</a>
        </h1>
        <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">
            <a class="ui-state-default ui-corner-all ui-button" href="MediaCategory.aspx">Edit Categories</a>
            <label class="search" for="txtSearchQuery" style="padding: 10px;">
                Search:</label>
            <asp:TextBox ID="txtSearchQuery" ClientIDMode="Static" runat="server" CssClass="ui-widget ui-widget-content ui-corner-left"></asp:TextBox>
            <asp:Button ID="btnAskTheGuide" runat="server" Text="Go" CssClass="ui-button ui-widget ui-state-default ui-corner-right ui-button-text-only" />
            <span class="filter">
                <input value="Add New Document" class="ui-state-default ui-corner-all ui-button"
                    type="button" onclick=" window.location = 'document.aspx?format=literature'; " />
                <input value="Add New Audio/Video" class="ui-state-default ui-corner-all ui-button"
                    type="button" onclick=" window.location = 'document.aspx?format=media'; " />
                <input value="Add New Web Page" class="ui-state-default ui-corner-all ui-button"
                    type="button" onclick=" window.location = 'page.aspx'; " />
                <asp:Button ID="btnEditAuthors" runat="server" Text="Edit Authors" CssClass="ui-state-default ui-corner-all ui-button" />
                <asp:Button ID="btnEditSubjects" runat="server" Text="Edit Subjects" CssClass="ui-state-default ui-corner-all ui-button" />
                <asp:Button ID="btnEditCategories" runat="server" Text="Edit Categories" CssClass="ui-state-default ui-corner-all ui-button" /><br />
            </span>
        </div>
        <div class="filter ui-state-default">
            Browse By: <a href="content.aspx?action=title">Title</a> | <a href="content.aspx?action=subject">
                Subject</a> | <a href="content.aspx?action=author">Author</a> | <a href="content.aspx?action=source">
                    Source</a> | <a href="content.aspx?action=type">Type</a>
        </div>
        <asp:Panel ID="pnlNewAuthor" runat="server" BackColor="#E0E0E0" Height="28px" Visible="False"
            Width="646px">
            F:<asp:TextBox ID="txtFirstName" runat="server">
            </asp:TextBox>
            M:<asp:TextBox ID="txtMiddleName" runat="server">
            </asp:TextBox>
            L:<asp:TextBox ID="txtLastName" runat="server">
            </asp:TextBox>
            <asp:Button ID="btnAddAuthor" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                Text="Add Author" />
        </asp:Panel>
        <asp:Panel ID="pnlReplaceAuthor" runat="server" BackColor="#E0E0E0" Height="36px"
            Width="529px">
            Old Id:<asp:TextBox ID="txtOldId" runat="server">
            </asp:TextBox>
            New Id:
            <asp:TextBox ID="txtNewId" runat="server">
            </asp:TextBox>
            <asp:Button ID="btnDeleteAuthor" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                Text="Replace Author" />
        </asp:Panel>
        <asp:GridView ID="gvAuthorEdit" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="sqlAuthorEdit" Visible="False" AllowPaging="True" PageSize="100"
            DataKeyNames="AuthorId">
            <PagerSettings Position="TopAndBottom" PageButtonCount="30" />
            <Columns>
                <asp:BoundField DataField="AuthorId" HeaderText="Id#" InsertVisible="False" ReadOnly="True"
                    SortExpression="AuthorId"></asp:BoundField>
                <asp:BoundField DataField="AuthorFirst" HeaderText="First" SortExpression="AuthorFirst">
                </asp:BoundField>
                <asp:BoundField DataField="AuthorMiddle" HeaderText="Middle" SortExpression="AuthorMiddle">
                </asp:BoundField>
                <asp:BoundField DataField="AuthorLast" HeaderText="Last" SortExpression="AuthorLast">
                </asp:BoundField>
                <asp:BoundField DataField="Photo" HeaderText="Photo" SortExpression="Photo"></asp:BoundField>
                <asp:TemplateField HeaderText="Bio" SortExpression="BioText">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#Bind("BioText")%>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("BioText")%>' TextMode="MultiLine"
                            Rows="10" Columns="50"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Born" HeaderText="Born" SortExpression="Born"></asp:BoundField>
                <asp:BoundField DataField="Died" HeaderText="Died" SortExpression="Died"></asp:BoundField>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ButtonType="Button"
                    ControlStyle-CssClass="button ui-button"></asp:CommandField>
            </Columns>
        </asp:GridView>
        <asp:GridView ID="gvSubjectEdit" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataKeyNames="SubjectId" DataSourceID="sqlSubjectEdit" Visible="False">
            <Columns>
                <asp:BoundField DataField="SubjectId" HeaderText="SubjectId" InsertVisible="False"
                    ReadOnly="True" SortExpression="SubjectId" Visible="False" />
                <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />
                <asp:BoundField DataField="SortOrder" HeaderText="SortOrder" SortExpression="SortOrder" />
                <asp:BoundField DataField="CategoryId" HeaderText="CategoryId" SortExpression="CategoryId" />
                <asp:BoundField DataField="Photo" HeaderText="Photo" SortExpression="Photo" />
                <asp:CommandField ShowEditButton="True" HeaderText="Edit" ShowInsertButton="True" />
                <asp:CommandField ShowDeleteButton="True" HeaderText="Delete" />
            </Columns>
            <HeaderStyle CssClass="gridheader" />
        </asp:GridView>
        <asp:GridView ID="gvCategoryEdit" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataKeyNames="CategoryId" DataSourceID="sqlCategoryEdit" Visible="False">
            <Columns>
                <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
                <asp:TemplateField HeaderText="Description" SortExpression="Description">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#Bind("Description")%>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("Description")%>' TextMode="MultiLine"
                            CssClass="ckeditor" Rows="10" Columns="50"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ParentCategory" HeaderText="ParentCategory" SortExpression="ParentCategory" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ButtonType="Button" />
            </Columns>
            <HeaderStyle CssClass="gridheader" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqlCategoryEdit" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            DeleteCommand="DELETE FROM [MediaCategory] WHERE [CategoryId] = @CategoryId"
            InsertCommand="INSERT INTO [MediaCategory] ([CategoryId], [ParentCategory], [Category], [Description]) VALUES (@CategoryId, @ParentCategory, @Category, @Description)"
            SelectCommand="SELECT [CategoryId], [ParentCategory], [Category], [Description] FROM [MediaCategory]"
            UpdateCommand="UPDATE [MediaCategory] SET [ParentCategory] = @ParentCategory, [Category] = @Category, [Description] = @Description WHERE [CategoryId] = @CategoryId">
            <DeleteParameters>
                <asp:Parameter Name="CategoryId" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="ParentCategory" Type="Int32" />
                <asp:Parameter Name="Category" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="CategoryId" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="CategoryId" Type="Int32" />
                <asp:Parameter Name="ParentCategory" Type="Int32" />
                <asp:Parameter Name="Category" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqlSubjectEdit" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            DeleteCommand="DELETE FROM [DocumentSubjects] WHERE [SubjectId] = @original_SubjectId"
            InsertCommand="INSERT INTO [DocumentSubjects] ([Subject], [SortOrder], [CategoryId], Photo) VALUES (@Subject, @SortOrder, @CategoryId, @Photo)"
            SelectCommand="SELECT [SubjectId], [Subject], [SortOrder], [CategoryId],Photo FROM [DocumentSubjects] ORDER BY [Subject]"
            UpdateCommand="UPDATE [DocumentSubjects] SET [Subject] = @Subject, [SortOrder] = @SortOrder, [CategoryId] = @CategoryId, Photo=@Photo WHERE [SubjectId] = @original_SubjectId">
            <DeleteParameters>
                <asp:Parameter Name="original_SubjectId" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Subject" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Int32" />
                <asp:Parameter Name="CategoryId" Type="Int32" />
                <asp:Parameter Name="Photo" />
                <asp:Parameter Name="original_SubjectId" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Subject" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Int32" />
                <asp:Parameter Name="CategoryId" Type="Int32" />
                <asp:Parameter Name="Photo" />
            </InsertParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqlAuthorEdit" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            DeleteCommand="DELETE FROM [DocumentAuthors] WHERE [AuthorId] = @AuthorId" InsertCommand="INSERT INTO [DocumentAuthors] ([AuthorFirst], [AuthorMiddle], [AuthorLast], [Photo], [Born], [Died], [BioText]) VALUES (@AuthorFirst, @AuthorMiddle, @AuthorLast, @Photo, @Born, @Died, @BioText)"
            SelectCommand="SELECT [AuthorId], [AuthorFirst], [AuthorMiddle], [AuthorLast], [Photo], [Born], [Died], [BioText] FROM [DocumentAuthors] ORDER BY [AuthorLast]"
            UpdateCommand="UPDATE [DocumentAuthors] SET [AuthorFirst] = @AuthorFirst, [AuthorMiddle] = @AuthorMiddle, [AuthorLast] = @AuthorLast, [Photo] = @Photo, [Born] = @Born, [Died] = @Died, [BioText] = @BioText WHERE [AuthorId] = @AuthorId">
            <DeleteParameters>
                <asp:Parameter Name="AuthorId" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="AuthorFirst" Type="String" />
                <asp:Parameter Name="AuthorMiddle" Type="String" />
                <asp:Parameter Name="AuthorLast" Type="String" />
                <asp:Parameter Name="Photo" Type="String" />
                <asp:Parameter Name="Born" Type="String" />
                <asp:Parameter Name="Died" Type="String" />
                <asp:Parameter Name="BioText" Type="String" />
                <asp:Parameter Name="AuthorId" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="AuthorFirst" Type="String" />
                <asp:Parameter Name="AuthorMiddle" Type="String" />
                <asp:Parameter Name="AuthorLast" Type="String" />
                <asp:Parameter Name="Photo" Type="String" />
                <asp:Parameter Name="Born" Type="String" />
                <asp:Parameter Name="Died" Type="String" />
                <asp:Parameter Name="BioText" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        <div style="background-color: #f4f4f4; font-weight: bold; width: 100%; font-size: 12pt">
            Top&gt;
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <asp:GridView ID="gvLiterature" runat="server" AutoGenerateColumns="False"
            AllowPaging="True" AllowSorting="True" PageSize="50">
            <HeaderStyle CssClass="gridheader" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="DocumentId" DataNavigateUrlFormatString="/web/{0}"
                    DataTextField="DocumentId" HeaderText="#" SortExpression="DocumentId" />
                <asp:TemplateField HeaderText="Title" SortExpression="Title">
                    <ItemTemplate>
                        <asp:HyperLink Font-Bold="true" ID="link" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Title")%>'
                            NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.URL")%>'>
                        </asp:HyperLink>
                        <br />
                        <%#DataBinder.Eval(Container, "DataItem.PublicationInformation")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("Title")%>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField HeaderText="Author" DataNavigateUrlFields="Author1" DataNavigateUrlFormatString="content.aspx?action=author&amp;Id={0}"
                    DataTextField="Author" SortExpression="Author" />
                <asp:HyperLinkField HeaderText="Source" DataNavigateUrlFields="Source" DataNavigateUrlFormatString="content.aspx?action=source&amp;source={0}"
                    DataTextField="Source" SortExpression="Source" />
                <asp:ImageField DataAlternateTextField="MediaType" DataImageUrlField="MediaIconPath"
                    HeaderText="Type" SortExpression="MediaType" ReadOnly="True">
                </asp:ImageField>
                <%-- <asp:CheckBoxField DataField="FullText" HeaderText="HTML" ReadOnly="True" SortExpression="FullText" />--%>
                <asp:HyperLinkField DataNavigateUrlFields="DocumentId" DataNavigateUrlFormatString="document.aspx?Id={0}"
                    HeaderText="Edit" Text="Edit" SortExpression="DocumentId" />
            </Columns>
            <PagerSettings Position="TopAndBottom" PageButtonCount="30" Mode="NumericFirstLast" />
        </asp:GridView>
        <asp:GridView ID="gvAuthors" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="AuthorId" HeaderText="Id" SortExpression="AuthorId" />
                <asp:TemplateField HeaderText="Author">
                    <ItemTemplate>
                        <img src="/images/icons/icon_category.gif" alt="Folder" />
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("AuthorId", "content.aspx?action=author&amp;Id={0}")%>'
                            Text='<%#Eval("AuthorName")%>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="Subject">
                    <ItemTemplate>
                        <img src="/images/icons/icon_category.gif" alt="Folder" />
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("SubjectId", "content.aspx?action=subject&amp;Id={0}")%>'
                            Text='<%#Eval("Subject")%>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:GridView ID="gvSource" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="Source">
                    <ItemTemplate>
                        <img src="/images/icons/icon_category.gif" alt="Folder" />
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("Source", "content.aspx?action=source&Source={0}")%>'
                            Text='<%#Eval("Source")%>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:GridView ID="gvMediaType" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:ImageField DataAlternateTextField="MediaType" DataImageUrlField="MediaIconPath"
                    HeaderText="Type" SortExpression="MediaType">
                </asp:ImageField>
                <asp:HyperLinkField DataNavigateUrlFields="MediaTypeId" DataNavigateUrlFormatString="content.aspx?action=MediaType&amp;Id={0}"
                    DataTextField="MediaType" HeaderText="MediaType" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
