﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="Manager_FeaturedWidget" Codebehind="AcademyCourse.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Featured Products</h1>
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                  DataKeyNames="WidgetId" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
        <Columns>
            <asp:BoundField DataField="WidgetId" HeaderText="WidgetId" ReadOnly="True" SortExpression="WidgetId"
                            Visible="false" InsertVisible="False" />
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
            <asp:BoundField DataField="Author" HeaderText="Author" SortExpression="Author" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("Description")%>' TextMode="MultiLine"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("Description")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductUrl" HeaderText="ProductUrl" SortExpression="ProductUrl" />
            <asp:TemplateField HeaderText="ImageUrl" SortExpression="ImageUrl">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("ImageUrl")%>' Columns="80"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("ImageUrl")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CreateDate" DataFormatString="{0:d}" HeaderText="CreateDate"
                            Visible="false" SortExpression="CreateDate" />
            <asp:CommandField HeaderText="Edit" ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [AcademyCourses] WHERE [WidgetId] = @WidgetId" InsertCommand="INSERT INTO [AcademyCourses] ([Title], [Author], [Price], [Description], [ProductUrl], [ImageUrl], [CreateDate]) VALUES (@Title, @Author, @Price, @Description, @ProductUrl, @ImageUrl, @CreateDate)"
                       ProviderName="<%$ ConnectionStrings:Public.ProviderName %>" SelectCommand="SELECT [WidgetId], [Title], [Author], [Price], [Description], [ProductUrl], [ImageUrl], [CreateDate] FROM [AcademyCourses]"
                       UpdateCommand="UPDATE [AcademyCourses] SET [Title] = @Title, [Author] = @Author, [Price] = @Price, [Description] = @Description, [ProductUrl] = @ProductUrl, [ImageUrl] = @ImageUrl, [CreateDate] = @CreateDate WHERE [WidgetId] = @WidgetId">
        <DeleteParameters>
            <asp:Parameter Name="WidgetId" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Author" Type="String" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="ProductUrl" Type="String" />
            <asp:Parameter Name="ImageUrl" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Author" Type="String" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="ProductUrl" Type="String" />
            <asp:Parameter Name="ImageUrl" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="WidgetId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="WidgetId" DataSourceID="sqlAddWidget"
                  DefaultMode="Insert" CellPadding="4" ForeColor="#333333">
        <EditItemTemplate>
            WidgetId:
            <asp:Label ID="WidgetIdLabel1" runat="server" Text='<%#Eval("WidgetId")%>' />
            <br />
            Title:
            <asp:TextBox ID="TitleTextBox" runat="server" Text='<%#Bind("Title")%>' />
            <br />
            Author:
            <asp:TextBox ID="AuthorTextBox" runat="server" Text='<%#Bind("Author")%>' />
            <br />
            Price:
            <asp:TextBox ID="PriceTextBox" runat="server" Text='<%#Bind("Price")%>' />
            <br />
            Description:
            <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%#Bind("Description")%>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                            Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                                  CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <InsertItemTemplate>
            Title:
            <asp:TextBox ID="TitleTextBox" runat="server" Columns="100" Text='<%#Bind("Title")%>' />
            <br />
            Author:
            <asp:TextBox ID="AuthorTextBox" runat="server" Columns="100" Text='<%#Bind("Author")%>' />
            <br />
            Price:
            <asp:TextBox ID="PriceTextBox" runat="server" Text='<%#Bind("Price")%>' />
            <br />
            Product URL:
            <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("ProductUrl")%>' Columns="100" />
            <br />
            Image URL:
            <asp:TextBox ID="TextBox2" runat="server" Columns="100" Text='<%#Bind("ImageUrl")%>' />
            <br />
            Description:
            <asp:TextBox ID="DescriptionTextBox" runat="server" Columns="100" Rows="5" Text='<%#Bind("Description")%>'
                         TextMode="MultiLine" />
            <br />
            <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                        Text="Insert" />
            &nbsp;<asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False"
                              CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            WidgetId:
            <asp:Label ID="WidgetIdLabel" runat="server" Text='<%#Eval("WidgetId")%>' />
            <br />
            Title:
            <asp:Label ID="TitleLabel" runat="server" Text='<%#Bind("Title")%>' />
            <br />
            Author:
            <asp:Label ID="AuthorLabel" runat="server" Text='<%#Bind("Author")%>' />
            <br />
            Price:
            <asp:Label ID="PriceLabel" runat="server" Text='<%#Bind("Price")%>' />
            <br />
            Description:
            <asp:Label ID="DescriptionLabel" runat="server" Text='<%#Bind("Description")%>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit"
                            Text="Edit" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                                  Text="Delete" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                                  Text="New" />
        </ItemTemplate>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
    </asp:FormView>
    <asp:SqlDataSource ID="sqlAddWidget" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [AcademyCourses] WHERE [WidgetId] = @WidgetId" InsertCommand="INSERT INTO [AcademyCourses] ([Title], [Author], [Price], [Description], [ProductUrl], [ImageUrl], [CreateDate]) VALUES (@Title, @Author, @Price, @Description, @ProductUrl, @ImageUrl, @CreateDate)"
                       SelectCommand="SELECT * FROM [AcademyCourses]" UpdateCommand="UPDATE [AcademyCourses] SET [Title] = @Title, [Author] = @Author, [Price] = @Price, [Description] = @Description, [ProductUrl] = @ProductUrl, [ImageUrl] = @ImageUrl, [CreateDate] = @CreateDate WHERE [WidgetId] = @WidgetId">
        <DeleteParameters>
            <asp:Parameter Name="WidgetId" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Author" Type="String" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="ProductUrl" Type="String" />
            <asp:Parameter Name="ImageUrl" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Author" Type="String" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="ProductUrl" Type="String" />
            <asp:Parameter Name="ImageUrl" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="WidgetId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>