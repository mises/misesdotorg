<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Manager.Master" Title="Edit Reviews" Inherits="review" Codebehind="~/Manager/review.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table style="margin-right: auto; margin-left: auto" class="MainTable" id="Table1" cellspacing="1" cellpadding="0" width="650"  border="0">
        <tr>
            <td valign="top">
            </td>
            <td>
                <h3 style="text-align: center" >
                    <asp:Label ID="lblTitle" runat="server">Edit Review</asp:Label><br />
                </h3>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <strong>ID:</strong>
                <asp:Label ID="lblReviewId" runat="server" Font-Bold="True">0</asp:Label></td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]" OnClick="btnSave_Click"
                            CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
                <asp:Button ID="Cancel" OnClick="Cancel_Click" runat="server" Text="Cancel" CssClass="ui-state-default ui-corner-all ui-button"></asp:Button></td>
        </tr>
        <tr>
            <td valign="top">
                <strong>Date</strong></td>
            <td>
                <ew:CalendarPopup ID="calreviewDate" runat="server">
                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                  BackColor="White"></WeekdayStyle>
                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                      ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                   BackColor="AntiqueWhite"></OffMonthStyle>
                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                   BackColor="LightGoldenrodYellow"></TodayDayStyle>
                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                  BackColor="LightGray"></WeekendStyle>
                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                       ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                </ew:CalendarPopup>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <strong>Title</strong></td>
            <td>
                <asp:TextBox ID="txtTitle" runat="server" Columns="80"></asp:TextBox></td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <strong>Author</strong></td>
            <td align="center">
                First:
                <asp:TextBox ID="txtScreenName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <strong>Location</strong>
            </td>
            <td>
                <asp:TextBox ID="txtLocation" runat="server" Columns="80"></asp:TextBox></td>
        </tr>
        <tr>
            <td valign="top">
                <strong>Intro Text</strong></td>
            <td>
                <asp:TextBox ID="txtReview" runat="server" Columns="80" TextMode="MultiLine" CssClass="ckeditor" Rows="20"></asp:TextBox></td>
        </tr>
    </table>
</asp:Content>