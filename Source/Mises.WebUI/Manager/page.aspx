<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" AutoEventWireup="true"
    Strict="true" ValidateRequest="false" Inherits="Manager_page" Title="Page Editor"
    CodeBehind="page.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function confirm_delete() {
            if (confirm("Are you sure you want to delete this item?") == true)
                return true;
            else
                return false;
        }

    </script>
    <table style="margin-right: auto; margin-left: auto" class="MainTable" id="Identifier"
        cellspacing="1" cellpadding="0" width="780" border="0">
        <tr>
            <td valign="top" class="detailheader">
            </td>
            <td class="detailtext">
                <h2 class="adminSection">
                    <asp:Literal ID="litTitle" runat="server">New Page</asp:Literal>
                </h2>
                <div class="ui-buttonset ui-state-default">
                    <asp:Button ID="btnSave" runat="server" Text="Save Changes" AccessKey="s" title="Save your changes [alt-s]"
                        CssClass="ui-state-default ui-corner-all ui-button" OnClick="btnSave_Click">
                    </asp:Button>
                    <asp:Button ID="btnDelete" runat="server" AccessKey="s" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Delete" title="Save your changes [alt-s]" OnClick="btnDelete_Click" />
                    <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="ui-state-default ui-corner-all ui-button">
                    </asp:Button>
                    <asp:Button ID="btnNext" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        OnClick="btnNext_Click" Text="Next" />
                    <asp:Button ID="btnTidy" runat="server" CssClass="ui-state-default ui-corner-all ui-button"
                        Text="Tidy HTML" OnClick="btnTidy_Click" /><br />
                    <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" Font-Bold="True"
                        ForeColor="Red"></asp:Label>
                </div>
            </td>
        </tr>
        <!--
        <tr>
            <td align="left" class="detailheader" valign="top">
                <strong>GUID</strong></td>
            <td align="left" class="detailtext">
                <asp:HyperLink ID="lnkGUID" runat="server" Target="_blank" CssClass="ui-icon-link ui-state-default"></asp:HyperLink></td>
        </tr>
        -->
        <tr>
            <td align="left" class="detailheader" valign="top">
                <strong>URL</strong>
            </td>
            <td align="left" class="detailtext">
                <asp:HyperLink ID="lnkURL" runat="server" Target="_blank" CssClass="ui-icon-link ui-state-default"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td valign="top" class="detailheader" style="height: 24px">
                <strong>Title</strong>
            </td>
            <td class="detailtext" style="height: 24px">
                <asp:TextBox CssClass="textfield" ID="txtPageTitle" runat="server" Columns="80"></asp:TextBox>
            </td>
        </tr>
        <%--<tr>
            <td valign="top" align="left" class="detailheader" style="height: 19px">
                <strong>Author</strong></td>
            <td align="left" class="detailtext" style="height: 19px">
                TODO</td>
        </tr>--%>
        <tr>
            <td valign="top" class="detailheader">
                <strong>Meta Description</strong>
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtPageDescription" runat="server" Columns="80" TextMode="MultiLine"
                    Rows="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" style="font-weight: bold">
                Meta Keywods
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtPageKeywords" runat="server" Columns="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                HTML Page Content:
            </td>
            <td valign="top" colspan="2" class="detailheader">
                <asp:TextBox ID="txtPageContent" runat="server" Height="500px" Width="100%" TextMode="MultiLine"
                    CssClass="ckeditor"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
