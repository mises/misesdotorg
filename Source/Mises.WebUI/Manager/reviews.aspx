<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" Title="Edit Reviews" Inherits="reviews" Codebehind="~/Manager/reviews.aspx.cs" %>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        Review Manager</h2>
    <strong>Search The Review Archives: </strong>
    <br />
    <input id="txtQuery" type="text" size="20" name="find_spec" runat="server" />
    &nbsp;
    <asp:Button ID="btnSearch" AccessKey="s" runat="server" ToolTip="Search the Daily review"
                EnableViewState="False" Text="GO" CssClass="ui-state-default ui-corner-all ui-button"></asp:Button>
    <br />
    <i>Sort review By:</i> <a href="review.aspx?action=authors">Author</a> | <a href="review.aspx?action=title">
                                                                                 Title</a> | <a href="review.aspx">Date Posted</a>
    <asp:Label ID="lblMonthlyArchives" runat="server"></asp:Label>
    <asp:DataGrid ID="dgArchives" runat="server" Width="800px" AutoGenerateColumns="False"
                  AllowPaging="True" AllowSorting="True" PageSize="100" HorizontalAlign="Center"
                  CellPadding="4" ForeColor="#333333" GridLines="None">
        <FooterStyle ForeColor="White" BackColor="#507CD1" Font-Bold="True"></FooterStyle>
        <SelectedItemStyle Font-Bold="True" ForeColor="#333333" BackColor="#D1DDF1"></SelectedItemStyle>
        <ItemStyle BackColor="#EFF3FB"></ItemStyle>
        <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#507CD1"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="False" DataField="ReviewId" ReadOnly="True" HeaderText="ReviewId">
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Title">
                <itemtemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.title")%>'
                                   NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.ReviewId", "review.aspx?Id={0}")%>'> </asp:HyperLink>
                </itemtemplate>
            </asp:TemplateColumn>
            <asp:HyperLinkColumn DataNavigateUrlField="BookId" DataNavigateUrlFormatString="/store/product2.aspx?Product_ID={0}"
                                 DataTextField="BookId" HeaderText="View"></asp:HyperLinkColumn>
            <asp:BoundColumn DataField="ReviewDate" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}">
                <itemstyle horizontalalign="Right"></itemstyle>
            </asp:BoundColumn>
            <asp:ButtonColumn Text="Delete" ButtonType="PushButton" DataTextField="ReviewId"
                              HeaderText="Delete" CommandName="Delete"></asp:ButtonColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Center" ForeColor="White" Position="TopAndBottom" BackColor="#2461BF">
        </PagerStyle>
        <EditItemStyle BackColor="#2461BF" />
        <AlternatingItemStyle BackColor="White" />
    </asp:DataGrid>
</asp:Content>