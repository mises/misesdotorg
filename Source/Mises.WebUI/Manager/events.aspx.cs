﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

/// <summary>
///   Summary description for events
/// </summary>
partial class eventsManager : Page
{
    protected void Button1_Click(object sender, EventArgs e)
    {
        DetailsView2.ChangeMode(DetailsViewMode.Insert);
    }

    protected void DetailsView2_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        if (Request.QueryString["Id"] != "")
        {
            sqlEventDetailsForm.SelectParameters.Clear();
            sqlEventDetailsForm.SelectParameters.Add("EventId", Request.QueryString["Id"]);
        }
    }
}