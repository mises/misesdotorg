﻿#region

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Data;
using Mises.Domain.Utility;

using Document = Mises.Domain.Documents.Document;

#endregion

public partial class Manager_ManagerControls_RevisionHistory : UserControl
{
    #region Delegates

    public delegate void ShowRevisionDelegate(RevisionGetByRevisionIdResult revision);

    #endregion

    public Guid DocumentGUID { get; set; }

    public string CurrentText { get; set; }
    public event ShowRevisionDelegate ShowRevisionEvent;

    protected void Page_Init(object sender, EventArgs e)
    {
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack) return;
        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
        ISingleResult<RevisionGetListByGUIDResult> revisions = db.RevisionGetListByGUID(DocumentGUID);

        ViewState["DocumentGUID"] = DocumentGUID;

        gvRevisions.DataSource = revisions;
        gvRevisions.DataBind();
    }

    protected void ShowRevision(object sender, CommandEventArgs e)
    {
        int revisionId = int.Parse(e.CommandArgument.ToString());

        RevisionGetByRevisionIdResult revision = GetRevision(revisionId);
        if (ShowRevisionEvent != null)
            ShowRevisionEvent(revision);
    }

    private RevisionGetByRevisionIdResult GetRevision(int revisionId)
    {
        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
        ISingleResult<RevisionGetByRevisionIdResult> revision = db.RevisionGetByRevisionId(revisionId);
        IEnumerator<RevisionGetByRevisionIdResult> revEnum = revision.GetEnumerator();
        if (revEnum.MoveNext())
        {
            RevisionGetByRevisionIdResult save = revEnum.Current;
            return save;
        }
        return null;
    }

    protected void btnCompare_Click(object sender, EventArgs e)
    {
        // Get text of selected revision
        string oldText = "";

        DocumentGUID = (Guid) ViewState["DocumentGUID"];

        var doc = new Document(DocumentGUID);
        oldText = doc.Contents;

        string newText = (from GridViewRow row in gvRevisions.Rows
                          let checkbox = (RadioButton) row.Cells[0].Controls[1]
                          where checkbox.Checked
                          select (int) gvRevisions.DataKeys[row.DataItemIndex].Value
                          into revisionId select GetRevision(revisionId)
                          into revision select revision.DocumentText).FirstOrDefault();

        var diffHelper = new HtmlDiff(oldText, newText);

        litDiffText.Text = diffHelper.Build();
        pnlDiff.Visible = true;
    }
}