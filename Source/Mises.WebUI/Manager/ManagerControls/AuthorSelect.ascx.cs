#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Documents;

#endregion

partial class Manager_ManagerControls_AuthorSelect : UserControl
{
    #region Properties

    public int Author1
    {
        get { return Convert.ToInt32(ddAuthor1.SelectedValue); }
        set { ddAuthor1.SelectedValue = Convert.ToString(value); }
    }

    public int? Author2
    {
        get { return Convert.ToInt32(ddAuthor2.SelectedValue); }
        set { ddAuthor2.SelectedValue = Convert.ToString(value); }
    }

    #endregion

    public Manager_ManagerControls_AuthorSelect()
    {
        Load += Page_Load;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorAdd.AuthorAdded += AuthorAdd_AuthorAdded;
        if (Page.IsPostBack)
        {
            return;
        }
        BindPage();
    }

    protected void AuthorAdd_AuthorAdded(object sender, EventArgs e)
    {
        BindPage();
    }

    private void BindPage()
    {
        try
        {
            ddAuthor1.DataSource = DocumentAuthors.GetAuthorsLastNameFirst();
            ddAuthor1.DataBind();
        }
        catch (ArgumentOutOfRangeException) // Invalid author selection
        {
            ddAuthor1.Items.Insert(0, "Invalid Author Selected: Save To Reset");
        }

        try
        {
            ddAuthor2.DataSource = DocumentAuthors.GetAuthorsLastNameFirst();
            ddAuthor2.DataBind();
        }
        catch (ArgumentOutOfRangeException) // Invalid author selection
        {
            ddAuthor2.Items.Insert(0, "Invalid Author Selected: Save To Reset");
        }


        var item = new ListItem("Select one:", "0") {Selected = false};
        ddAuthor1.Items.Insert(0, item);
        ddAuthor2.Items.Insert(0, item);
    }

    public string  AuthorName { get { return ddAuthor1.SelectedItem.Text; }}
}