#region

using System;
using System.Web.UI;
using Mises.Domain.Documents;

#endregion

public partial class Manager_ManagerControls_Author : UserControl
{
    #region Delegates

    public delegate void AuthorAddedEventHandler(object sender, EventArgs e);

    #endregion

    public Manager_ManagerControls_Author()
    {
        Load += Page_Load;
    }

    private void Page_Load(object sender, EventArgs e)
    {
        btnAddAuthor.Click += btnAddAuthor_Click;
        btnNewAuthor.Click += btnNewAuthor_Click;
    }

    public event AuthorAddedEventHandler AuthorAdded;

    protected void btnAddAuthor_Click(object sender, EventArgs e)
    {
        if (DocumentAuthors.AddNewAuthor(txtFirstName.Text, txtMiddleName.Text, txtLastName.Text))
        {
            lblStatus.Text = "Added author.";
            pnlNewAuthor.Visible = false;
            if (AuthorAdded != null)
                AuthorAdded(null, null);
        }
        else
        {
            lblStatus.Text = "Error adding author.";
        }
    }

    protected void btnNewAuthor_Click(object sender, EventArgs e)
    {
        pnlNewAuthor.Visible = true;
    }
}