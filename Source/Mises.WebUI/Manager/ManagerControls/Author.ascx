<%@ Control Language="C#" AutoEventWireup="false" Inherits="Manager_ManagerControls_Author" Codebehind="Author.ascx.cs" %>
<asp:LinkButton ID="btnNewAuthor" runat="server">New Author</asp:LinkButton>
<asp:Panel ID="pnlNewAuthor" runat="server" BackColor="#E0E0E0" Visible="False" Width="646px">
    F:<asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>M:<asp:TextBox ID="txtMiddleName"
                                                                                 runat="server"></asp:TextBox>L:<asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
    <asp:Button ID="btnAddAuthor" runat="server" CssClass="ui-state-default ui-corner-all ui-button" Text="Add Author" />
    <asp:Label ID="lblStatus" runat="server" EnableViewState="False"></asp:Label>
</asp:Panel>