#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Tags;

#endregion

partial class Manager_ManagerControls_DocumentTags : UserControl
{
    private Guid mIdentifier;

    public Guid DocumentIdentifier
    {
        get
        {
            if (mIdentifier == Guid.Empty && (ViewState["mIdentifier"] != null || Request.QueryString["Id"] != null))
            {
                if (ViewState["mIdentifier"] != null)
                {
                    mIdentifier = (Guid) (ViewState["mIdentifier"]);
                }
                else
                {
                    ViewState["mIdentifier"] = new Guid(Request.QueryString["Id"]);
                }
            }
            return mIdentifier;
        }
        set
        {
            if (ViewState["mIdentifier"] != null)
            {
                return;
            }
            mIdentifier = value;
            ViewState["mIdentifier"] = value;
            Visible = true;
            GetDocumentTags();
        }
    }

    private void GetDocumentTags()
    {
        gvDocumentTags.DataSource = Tagging.GetDocumentTags(DocumentIdentifier);
        gvDocumentTags.DataBind();
    }


    protected void gvDocumentTags_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // Delete Selected Tag
        string tag = gvDocumentTags.DataKeys[e.RowIndex].Values[0].ToString();
        Tagging.DeleteDocumentTag(tag, DocumentIdentifier);
        GetDocumentTags();
    }

    protected void btnDeleteAllTags_Click(object sender, EventArgs e)
    {
        Tagging.DeleteAllDocumentTags(DocumentIdentifier);
        GetDocumentTags();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        gvDocumentTags.RowDeleting += gvDocumentTags_RowDeleting;
        //btnDeleteAllTags.Click += btnDeleteAllTags_Click;
    }
}