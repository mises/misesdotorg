﻿<%@ Control Language="C#" AutoEventWireup="true"
            Inherits="Manager_ManagerControls_RevisionHistory" Codebehind="RevisionHistory.ascx.cs" %>
<asp:GridView ID="gvRevisions" runat="server" AllowSorting="True" AutoGenerateColumns="False"
              DataKeyNames="RevisionId">
    <Columns>
        <asp:TemplateField>
            <EditItemTemplate>
                <asp:RadioButton ID="rdoCompare" runat="server" />
            </EditItemTemplate>
            <ItemTemplate>
                <asp:RadioButton ID="rdoCompare" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Revision" ShowHeader="False" SortExpression="Editor">
            <ItemTemplate>
                <asp:LinkButton ID="btnShowRevision" runat="server" Text='<%#Eval("Editor")%>' CommandArgument='<%#Eval("RevisionId")%>'
                                CommandName="Select" OnCommand="ShowRevision"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="EditDate" HeaderText="Date" SortExpression="EditDate" />
        <asp:BoundField DataField="Size" HeaderText="Size" SortExpression="Size" />
    </Columns>
</asp:GridView>

<asp:Button ID="btnCompare" runat="server" Text="Compare Revisions" 
            onclick="btnCompare_Click" />

<asp:Panel runat="server" ID="pnlDiff" Visible="false">
    <hr />
    <h2>
        Html Diff Visualisation</h2>
    <hr />
    <asp:Literal runat="server" ID="litDiffText"></asp:Literal>
</asp:Panel>