<%@ Control Language="C#" AutoEventWireup="false" Inherits="Manager_ManagerControls_DocumentTags" Codebehind="DocumentTags.ascx.cs" %>
<asp:GridView ID="gvDocumentTags" runat="server" AutoGenerateColumns="False" DataKeyNames="TagText">
    <Columns>
        <asp:HyperLinkField DataNavigateUrlFields="TagText" 
                            DataNavigateUrlFormatString="/tag/{0}" DataTextField="TagText" 
                            HeaderText="Tag" />
        <asp:CommandField ShowDeleteButton="True" HeaderText="Delete" />
    </Columns>
</asp:GridView>
<asp:Button ID="btnDeleteAllTags" runat="server" 
            Text="Delete All Tags for this Document" onclick="btnDeleteAllTags_Click" />
<p>
    <a href="/manager/Tags.aspx">Tag Central</a></p>