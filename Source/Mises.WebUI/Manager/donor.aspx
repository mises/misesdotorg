﻿<%@ Page Title="Mises.org Records for Donor" Language="C#" MasterPageFile="~/MasterPages/Manager.master"
    AutoEventWireup="true"
    CodeBehind="donor.aspx.cs" Inherits="Mises.Web.Manager.Donor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <h2>Mises.org Records for Donor</h2>

    <div><a href="donors.aspx">Back to List</a></div>

    <div class="ui-widget ui-corner-all" style="border: 1px black solid; background-color: lightgrey;">
        <table>
            <tr>
                <th>Source</th>
                <th>Count</th>
            </tr>
            <tr>
                <td>Store Purchases
                </td>
                <td>
                    <a href="javascript:alert('todo!')">#</a>
                </td>
            </tr>
            <tr>
                <td>Academy Student Records
                </td>
                <td>
                    <a href="javascript:alert('todo!')">#</a>
                </td>
            </tr>
            <tr>
                <td>Online Donations
                </td>
                <td>
                    <a href="javascript:alert('todo!')">#</a>
                </td>
            </tr>
            <tr>
                <td>Event Registrations
                </td>
                <td>
                    <a href="javascript:alert('todo!')">#</a>
                </td>
            </tr>
            <tr>
                <td>Mises Community Activity
                </td>
                <td>
                    <a href="javascript:alert('todo!')">#</a>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False"
        CellPadding="4" DataKeyNames="DonorId" DataSourceID="sqlDonorInfo"
        ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:BoundField DataField="DonorId" HeaderText="DonorId" InsertVisible="False"
                ReadOnly="True" SortExpression="DonorId" />
            <asp:BoundField DataField="Email1" HeaderText="Email1"
                SortExpression="Email1" />
            <asp:BoundField DataField="Email2" HeaderText="Email2"
                SortExpression="Email2" />
            <asp:BoundField DataField="Email3" HeaderText="Email3"
                SortExpression="Email3" />
            <asp:BoundField DataField="Email4" HeaderText="Email4"
                SortExpression="Email4" />
            <asp:BoundField DataField="Email5" HeaderText="Email5"
                SortExpression="Email5" />
            <asp:BoundField DataField="First_Name" HeaderText="First_Name"
                SortExpression="First_Name" />
            <asp:BoundField DataField="Last_Name" HeaderText="Last_Name"
                SortExpression="Last_Name" />
            <asp:BoundField DataField="Middle_Name" HeaderText="Middle_Name"
                SortExpression="Middle_Name" />
            <asp:BoundField DataField="Address" HeaderText="Address"
                SortExpression="Address" />
            <asp:BoundField DataField="DateAdded" HeaderText="DateAdded"
                SortExpression="DateAdded" />
            <asp:BoundField DataField="Total_nmbr_of_Gifts"
                HeaderText="Total_nmbr_of_Gifts" SortExpression="Total_nmbr_of_Gifts" />
            <asp:BoundField DataField="Total_Gift_amount" HeaderText="Total_Gift_amount"
                SortExpression="Total_Gift_amount" />
            <asp:BoundField DataField="Total_Addresses" HeaderText="Total_Addresses"
                SortExpression="Total_Addresses" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="sqlDonorInfo" runat="server"
        ConnectionString="<%$ ConnectionStrings:Public %>"
        DeleteCommand="DELETE FROM [DonorList] WHERE [DonorId] = @DonorId"
        InsertCommand="INSERT INTO [DonorList] ([Email1], [Email2], [Email3], [Email4], [Email5], [First_Name], [Last_Name], [Middle_Name], [Address], [DateAdded], [Total_nmbr_of_Gifts], [Total_Gift_amount], [Total_Addresses]) VALUES (@Email1, @Email2, @Email3, @Email4, @Email5, @First_Name, @Last_Name, @Middle_Name, @Address, @DateAdded, @Total_nmbr_of_Gifts, @Total_Gift_amount, @Total_Addresses)"
        SelectCommand="SELECT * FROM [DonorList] WHERE ([DonorId] = @DonorId)"
        UpdateCommand="UPDATE [DonorList] SET [Email1] = @Email1, [Email2] = @Email2, [Email3] = @Email3, [Email4] = @Email4, [Email5] = @Email5, [First_Name] = @First_Name, [Last_Name] = @Last_Name, [Middle_Name] = @Middle_Name, [Address] = @Address, [DateAdded] = @DateAdded, [Total_nmbr_of_Gifts] = @Total_nmbr_of_Gifts, [Total_Gift_amount] = @Total_Gift_amount, [Total_Addresses] = @Total_Addresses WHERE [DonorId] = @DonorId">
        <DeleteParameters>
            <asp:Parameter Name="DonorId" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Email1" Type="String" />
            <asp:Parameter Name="Email2" Type="String" />
            <asp:Parameter Name="Email3" Type="String" />
            <asp:Parameter Name="Email4" Type="String" />
            <asp:Parameter Name="Email5" Type="String" />
            <asp:Parameter Name="First_Name" Type="String" />
            <asp:Parameter Name="Last_Name" Type="String" />
            <asp:Parameter Name="Middle_Name" Type="String" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="DateAdded" Type="DateTime" />
            <asp:Parameter Name="Total_nmbr_of_Gifts" Type="Double" />
            <asp:Parameter Name="Total_Gift_amount" Type="Double" />
            <asp:Parameter Name="Total_Addresses" Type="Double" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="DonorId"
                QueryStringField="DonorId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Email1" Type="String" />
            <asp:Parameter Name="Email2" Type="String" />
            <asp:Parameter Name="Email3" Type="String" />
            <asp:Parameter Name="Email4" Type="String" />
            <asp:Parameter Name="Email5" Type="String" />
            <asp:Parameter Name="First_Name" Type="String" />
            <asp:Parameter Name="Last_Name" Type="String" />
            <asp:Parameter Name="Middle_Name" Type="String" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="DateAdded" Type="DateTime" />
            <asp:Parameter Name="Total_nmbr_of_Gifts" Type="Double" />
            <asp:Parameter Name="Total_Gift_amount" Type="Double" />
            <asp:Parameter Name="Total_Addresses" Type="Double" />
            <asp:Parameter Name="DonorId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
