#region

using System;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mises.Domain.Old;

#endregion

partial class reviews : Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        //INSTANT C# TODO TASK: Insert the following converted event handler wireups at the end of the 'InitializeComponent' method for forms, 'Page_Init' for web pages, or into a constructor for other classes:
        Load += Page_Load;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dgArchives.PageIndexChanged += dgArchives_PageIndexChanged;
        dgArchives.SortCommand += dgArchives_SortCommand;
        dgArchives.ItemDataBound += dgArchives_ItemDataBound;
        btnSearch.Click += btnSearch_Click;
        dgArchives.ItemCommand += dgArchives_ItemCommand;

        if (! Page.IsPostBack)
        {
            Bindreview();
        }
    }


    private void Bindreview()
    {
        Bindreview("");
    }

//INSTANT C# NOTE: C# does not support optional parameters. Overloaded method(s) are created above.
//ORIGINAL LINE: Sub Bindreview(Optional ByVal sortField As String = "")
    private void Bindreview(string sortField)
    {
        var review = new BookReview();
        dgArchives.DataSource = review.GetReviews(sortField);
        dgArchives.DataBind();
    }

    protected void dgArchives_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        dgArchives.CurrentPageIndex = e.NewPageIndex;
        Bindreview();
    }

    protected void dgArchives_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        string sortField = e.SortExpression;
        Bindreview(sortField);
    }

    private void dgArchives_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // Add Monthly Breaks
        if (Request.QueryString["action"] == "" &&
            (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)) // if title view
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.SelectedItem)
            {
                e.Item.Cells[3].Attributes.Add("OnClientClick",
                                               "return confirm('Are you sure you want to delete this review?');");
            }
        }
    }


    private void btnSearch_Click(object sender, EventArgs e)
    {
        dgArchives.CurrentPageIndex = 0;
        // Search review
        string qry = Server.HtmlEncode(txtQuery.Value.Trim());
        var review = new BookReview();
        dgArchives.DataSource = review.Search(qry);

        if (review.results > 0)
        {
            lblMonthlyArchives.Text = "<div align=center><h3>Review Search: " + review.results + " results.</h3></div>";
            dgArchives.DataBind();
        }
        else
        {
            lblMonthlyArchives.Text = "<div align=center><h3>Sorry, no results for \"" + qry + "\"</h3><br /></div>";
            dgArchives.Visible = false;
        }
    }

    private void dgArchives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            // Delete
            var review = new BookReview();
            review.DeleteReview(Convert.ToInt32(e.Item.Cells[0].Text));
            Bindreview();
        }
    }
}


internal class Editreview
{
    //This call is required by the Web Form Designer.

//    protected HyperLink lnlNew;

    [DebuggerStepThrough]
    private void InitializeComponent()
    {
    }

    private void Page_Init(object sender, EventArgs e) //Handles MyBase.Init
    {
        //CODEGEN: This method call is required by the Web Form Designer
        //Do not modify it using the code editor.
        InitializeComponent();
    }
}