<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false"
    MaintainScrollPositionOnPostback="true" Title="Mises Institute :: Faculty Members"
    Inherits="faculty" CodeBehind="~/Manager/faculty.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3 class="adminSection">
        <a href="fellows.aspx">Edit Faculty/Fellow Biographies</a>
    </h3>
    <h2 class="adminSection">Senior Fellows, Associated Scholars, Other Scholars, and Staff
    </h2>
    <p>
        New Person:<asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        &nbsp;<asp:DropDownList ID="ddlGroup" runat="server">
            <asp:ListItem>Senior Faculty</asp:ListItem>
            <asp:ListItem>Associated Scholars</asp:ListItem>
            <asp:ListItem>Other Scholars Working in the Austrian Tradition</asp:ListItem>
            <asp:ListItem>Staff</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnAddPerson" runat="server" Text="Add" OnClick="btnAddPerson_Click" />
    </p>
    <div id="SeniorFaculty" class="cont">
        <h2 class="adminSection">Senior Faculty</h2>
        <asp:GridView ID="gvSeniorFaculty" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="sqlSenior" DataKeyNames="control">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name"></asp:BoundField>
                <asp:BoundField DataField="school" HeaderText="Affiliation" SortExpression="school">
                </asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email"></asp:BoundField>
                <asp:BoundField DataField="web" HeaderText="Web" SortExpression="web"></asp:BoundField>
                <asp:BoundField DataField="jobtitle" HeaderText="Job Title" SortExpression="jobtitle">
                </asp:BoundField>
                <asp:BoundField DataField="orders" HeaderText="Order" SortExpression="orders"></asp:BoundField>
                <asp:CheckBoxField DataField="display" HeaderText="Visible" SortExpression="display">
                </asp:CheckBoxField>
                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True"></asp:CommandField>
                <asp:CommandField HeaderText="Edit" ShowEditButton="True"></asp:CommandField>
            </Columns>
        </asp:GridView>
    </div>
    <div id="Adjunct" class="cont">
        <asp:SqlDataSource ID="sqlSenior" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            SelectCommand="SELECT * FROM [Faculty] WHERE ([groups] = @groups)" DeleteCommand="DELETE FROM [Faculty] WHERE [control] = @control"
            InsertCommand="INSERT INTO [Faculty] ([name], [school], [email], [web], [groups], [orders], [jobtitle], [display], [CreateDate]) VALUES (@name, @school, @email, @web, @groups, @orders, @jobtitle, @display, @CreateDate)"
            UpdateCommand="UPDATE [Faculty] SET [name] = @name, [school] = @school, [email] = @email, [web] = @web, [orders] = @orders, [jobtitle] = @jobtitle, [display] = @display WHERE [control] = @control">
            <SelectParameters>
                <asp:Parameter DefaultValue="Senior Faculty" Name="groups" Type="String" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="control" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
                <asp:Parameter Name="control" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
            </InsertParameters>
        </asp:SqlDataSource>
        <h2 class="adminSection">Other Schoolars</h2>
        <asp:GridView ID="gvFaculty" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="sqlAdjunct" DataKeyNames="control">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name"></asp:BoundField>
                <asp:BoundField DataField="school" HeaderText="Affiliation" SortExpression="school">
                </asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email"></asp:BoundField>
                <asp:BoundField DataField="web" HeaderText="Web" SortExpression="web"></asp:BoundField>
                <asp:BoundField DataField="jobtitle" HeaderText="Job Title" SortExpression="jobtitle">
                </asp:BoundField>
                <asp:BoundField DataField="orders" HeaderText="Order" SortExpression="orders"></asp:BoundField>
                <asp:CheckBoxField DataField="display" HeaderText="Visible" SortExpression="display">
                </asp:CheckBoxField>
                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True"></asp:CommandField>
                <asp:CommandField HeaderText="Edit" ShowEditButton="True"></asp:CommandField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="sqlAdjunct" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            SelectCommand="SELECT * FROM [Faculty] WHERE ([groups] = @groups)" DeleteCommand="DELETE FROM [Faculty] WHERE [control] = @control"
            InsertCommand="INSERT INTO [Faculty] ([name], [school], [email], [web], [groups], [orders], [jobtitle], [display], [CreateDate]) VALUES (@name, @school, @email, @web, @groups, @orders, @jobtitle, @display, @CreateDate)"
            UpdateCommand="UPDATE [Faculty] SET [name] = @name, [school] = @school, [email] = @email, [web] = @web, [orders] = @orders, [jobtitle] = @jobtitle, [display] = @display WHERE [control] = @control">
            <SelectParameters>
                <asp:Parameter DefaultValue="Other Scholars Working in the Austrian Tradition" Name="groups"
                    Type="String" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="control" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
                <asp:Parameter Name="control" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
            </InsertParameters>
        </asp:SqlDataSource>
    </div>

    <div id="Associated" class="cont">
        <asp:SqlDataSource ID="sqlAssociated" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            SelectCommand="SELECT * FROM [Faculty] WHERE ([groups] = @groups)" DeleteCommand="DELETE FROM [Faculty] WHERE [control] = @control"
            InsertCommand="INSERT INTO [Faculty] ([name], [school], [email], [web], [groups], [orders], [jobtitle], [display], [CreateDate]) VALUES (@name, @school, @email, @web, @groups, @orders, @jobtitle, @display, @CreateDate)"
            UpdateCommand="UPDATE [Faculty] SET [name] = @name, [school] = @school, [email] = @email, [web] = @web, [orders] = @orders, [jobtitle] = @jobtitle, [display] = @display WHERE [control] = @control">
            <SelectParameters>
                <asp:Parameter DefaultValue="Associated Scholars" Name="groups" Type="String" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="control" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
                <asp:Parameter Name="control" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
            </InsertParameters>
        </asp:SqlDataSource>
        <h2 class="adminSection">Associated Scholars</h2>
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="sqlAssociated" DataKeyNames="control">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name"></asp:BoundField>
                <asp:BoundField DataField="school" HeaderText="Affiliation" SortExpression="school">
                </asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email"></asp:BoundField>
                <asp:BoundField DataField="web" HeaderText="Web" SortExpression="web"></asp:BoundField>
                <asp:BoundField DataField="jobtitle" HeaderText="Job Title" SortExpression="jobtitle">
                </asp:BoundField>
                <asp:BoundField DataField="orders" HeaderText="Order" SortExpression="orders"></asp:BoundField>
                <asp:CheckBoxField DataField="display" HeaderText="Visible" SortExpression="display">
                </asp:CheckBoxField>
                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True"></asp:CommandField>
                <asp:CommandField HeaderText="Edit" ShowEditButton="True"></asp:CommandField>
            </Columns>
        </asp:GridView>

    </div>

    <div id="Staff" class="cont">
        <h2 class="adminSection">Staff</h2>
        <asp:GridView ID="gvStaff" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="sqlStaff2" DataKeyNames="control">
            <Columns>
                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name"></asp:BoundField>
                <asp:BoundField DataField="school" HeaderText="Affiliation" SortExpression="school">
                </asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email"></asp:BoundField>
                <asp:BoundField DataField="web" HeaderText="Web" SortExpression="web"></asp:BoundField>
                <asp:BoundField DataField="photo" HeaderText="Photo" SortExpression="photo" />
                <asp:BoundField DataField="jobtitle" HeaderText="Job Title" SortExpression="jobtitle">
                </asp:BoundField>
                <asp:BoundField DataField="orders" HeaderText="Order" SortExpression="orders"></asp:BoundField>
                <asp:CheckBoxField DataField="display" HeaderText="Visible" SortExpression="display">
                </asp:CheckBoxField>
                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True"></asp:CommandField>
                <asp:CommandField HeaderText="Edit" ShowEditButton="True"></asp:CommandField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="sqlStaff2" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            SelectCommand="SELECT * FROM [Faculty] WHERE ([groups] = @groups)" DeleteCommand="DELETE FROM [Faculty] WHERE [control] = @control"
            InsertCommand="INSERT INTO [Faculty] ([name], [school], [email], [web],[photo], [groups], [orders], [jobtitle], [display], [CreateDate]) VALUES (@name, @school, @email, @web,@photo, @groups, @orders, @jobtitle, @display, @CreateDate)"
            UpdateCommand="UPDATE [Faculty] SET [name] = @name, [school] = @school, [email] = @email, [web] = @web,[photo] = @photo, [orders] = @orders, [jobtitle] = @jobtitle, [display] = @display WHERE [control] = @control">
            <SelectParameters>
                <asp:Parameter DefaultValue="Staff" Name="groups" Type="String" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="control" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="photo" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
                <asp:Parameter Name="control" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="school" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web" Type="String" />
                <asp:Parameter Name="photo" Type="String" />
                <asp:Parameter Name="groups" Type="String" />
                <asp:Parameter Name="orders" Type="Int32" />
                <asp:Parameter Name="jobtitle" Type="String" />
                <asp:Parameter Name="display" Type="Boolean" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
            </InsertParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
