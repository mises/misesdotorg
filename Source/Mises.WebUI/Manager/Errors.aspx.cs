#region

using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using Mises.Data;


#endregion

public partial class Manager_Errors : Page
{
    public string ChartUrl
    {
        get
        {
            var db = DataHelper.MisesDataContext;
            var exceptions = db.ExceptionsGetReport();

            var exceptionList = exceptions.Take(100).ToList();

            //string countList = string.Empty;
            //exceptionList.ForEach(e => countList += e.TotalCount + ",");
            //countList = countList.TrimEnd(Char.Parse(","));

            //string hoursList = string.Empty;
            //exceptionList.ForEach(e => hoursList += e.Hour + "|");
            //hoursList = hoursList.TrimEnd(Char.Parse("|"));

            var countList = String.Join("|", exceptionList.Take(100).Select(e => e.TotalCount));
            var hoursList = String.Join("|", exceptionList.Take(100).Select(e => e.Hour));

            string rangeList = "||{0}||{1}";

            //exceptionList.ForEach(e => hoursList += e.Hour + "|");
            rangeList = string.Format(rangeList, 0, exceptionList.Max(p => p.TotalCount));

            const string chartformat =
                "http://chart.apis.google.com/chart?chs=650x125&cht=ls&chco=0077CC&chtt={0}&chd=t:{1}&chxt=x,y&chxl=0:|{2}|1:{3}";

            string title = Server.UrlEncode(string.Format("Exceptions in the last {0} hours", exceptionList.Count));

            string url = string.Format(chartformat, title, countList, hoursList, rangeList);
            return url;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnClearExceptionLog_Click(object sender, EventArgs e)
    {
        DataHelper.ExecuteNonQuery("TRUNCATE TABLE dbo.Exceptions");
        Response.Redirect("Errors.aspx");
    }
}