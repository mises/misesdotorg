#region

using System;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Domain.Payments;

#endregion

partial class Manager_formEditor : Page
{
    private int mFormId = -1;

    private int FormId
    {
        get
        {
            if (mFormId == -1)
            {
                mFormId = Convert.ToInt32(Conversion.Val(Request.QueryString["FormId"]));
            }
            return mFormId;
        }
        set { mFormId = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }

        btnDelete.Attributes.Add("onclick", "return confirm_delete();");

        if (FormId == 0)
        {
            lblTitle.Text = "Create New Form";
            dateExpirationDateTextBox.SelectedDate = DateTime.Today.AddYears(50);
        }
        else
        {
            pnlAddProduct.Visible = true;
            pnlAddQuestion.Visible = true;
            // Load Form
            using (var form = new RegistrationForm(FormId))
            {
                lblTitle.Text = "Editing " + form.Title;
                lblFormIdLabel1.Text = form.FormId.ToString();
                lblCreateDate.Text = "Created<br />" + form.CreateDate.ToString("d");
                txtFormTitleTextBox.Text = form.Title;
                txtFormAuthorTextBox.Text = form.Author;
                dateExpirationDateTextBox.SelectedDate = form.Expiration;
                txtImageTextBox.Text = form.Image;
                txtIntroductionTextBox.Text = form.Introduction;
                txtFooter.Text = form.FooterText;
                txtPrimaryProductIdTextBox.Text = form.PrimaryProductId.ToString();
                txtThankYouMessageTextBox.Text = form.ThankYouMessage;
                txtEmailMessage.Text = form.EmailConfirmation;
                txtEmailAddress.Text = form.EmailAddress;
                chkGetAddressInfoCheckBox.Checked = form.GetAddressInfo;
                chkTakeCreditCardCheckBox.Checked = form.TakeCreditCard;
                chkProcessPaymentCheckBox.Checked = form.ProcessPayment;
                chkShowSidebar.Checked = form.ShowSidebar;
                chkPromptforCustomAmount.Checked = form.EnterAmountPrompt;
                lblGUID.Text = form.Identifier.ToString();
                chkPromptforRecurringDonation.Checked = form.RecurringPrompt;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // Save Form
        var form = new RegistrationForm
                       {
                           FormId = FormId,
                           Title = txtFormTitleTextBox.Text,
                           Author = txtFormAuthorTextBox.Text,
                           Expiration = dateExpirationDateTextBox.SelectedDate,
                           Image = txtImageTextBox.Text,
                           Introduction = txtIntroductionTextBox.Text,
                           FooterText = txtFooter.Text,
                           PrimaryProductId = Convert.ToInt32(Conversion.Val(txtPrimaryProductIdTextBox.Text)),
                           ThankYouMessage = txtThankYouMessageTextBox.Text,
                           EmailConfirmation = txtEmailMessage.Text,
                           EmailAddress = txtEmailAddress.Text,
                           GetAddressInfo = chkGetAddressInfoCheckBox.Checked,
                           TakeCreditCard = chkTakeCreditCardCheckBox.Checked,
                           ProcessPayment = chkProcessPaymentCheckBox.Checked,
                           EnterAmountPrompt = chkPromptforCustomAmount.Checked,
                           ShowSidebar = chkShowSidebar.Checked,
                           RecurringPrompt = chkPromptforRecurringDonation.Checked
                       };
        FormId = form.SaveForm();
        if (FormId > 0 && sender != null)
        {
            Response.Redirect("formEditor.aspx?formId=" + FormId);
        }
    }

    //Protected Sub frmEditForms_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertedEventArgs) Handles frmEditForms.ItemInserted
    //    Me.pnlAddProduct.Visible = True
    //    Me.pnlAddQuestion.Visible = True
    //End Sub

    protected void btnAddProduct_Click(object sender, EventArgs e)
    {
        // Save New Product
        var Product = new Product
                          {
                              Description = txtDescription.Text,
                              Price = Conversion.Val(txtPrice.Text),
                              ProductName = txtName.Text,
                              SelectQuantity = chkQuantity.Checked,
                              FormId = FormId
                          };
        Product.Save();
        gvProducts.DataBind();

        txtDescription.Text = "";
    }


    protected void btnAddQuestion_Click(object sender, EventArgs e)
    {
        // Save New Question
        var question = new Question
                           {
                               FormId = FormId,
                               QuestionText = txtQuestion.Text,
                               QuestionType = Convert.ToInt32(Conversion.Val(ddQuestionType.SelectedValue)),
                               Required = chkRequired.Checked
                           };
        question.Save();
        gvQuestions.DataBind();
        txtQuestion.Text = string.Empty;
    }


    protected void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("formManager.aspx");
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {
        btnSave_Click(null, null);
        Response.Redirect("/forms/" + FormId);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        var form = new RegistrationForm();
        form.DeleteForm(FormId);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        btnSave.Click += btnSave_Click;
        btnAddProduct.Click += btnAddProduct_Click;
        btnAddQuestion.Click += btnAddQuestion_Click;
        Cancel.Click += Cancel_Click;
        btnPreview.Click += btnPreview_Click;
        btnDelete.Click += btnDelete_Click;
    }
}