<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.Master" AutoEventWireup="false"
    Inherits="Manager_formEditor" Title="Custom Form Editor" ValidateRequest="false"
    CodeBehind="formEditor.aspx.cs" %>

<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function confirm_delete() {
            if (confirm("Are you sure you want to delete this item?") == true)
                return true;
            else
                return false;
        }

    </script>
    <asp:SqlDataSource ID="sqlProducts" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        DeleteCommand="DELETE FROM [RegistrationProducts] WHERE [ProductId] = @ProductId"
        InsertCommand="INSERT INTO [RegistrationProducts] ([ProductName], [Description], [SelectQuantity], [Price], ProductOrder) VALUES (@ProductName, @Description, @SelectQuantity, @Price,@ProductOrder)"
        SelectCommand="SELECT ProductId, ProductName, Description, SelectQuantity, Price, ProductOrder FROM dbo.RegistrationProducts WHERE (FormId = @FormId)"
        UpdateCommand="UPDATE dbo.RegistrationProducts SET ProductName = @ProductName, Description = @Description, SelectQuantity = @SelectQuantity, Price = @Price, ProductOrder = @ProductOrder WHERE (ProductId = @ProductId)">
        <DeleteParameters>
            <asp:Parameter Name="ProductId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ProductName" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="SelectQuantity" Type="Boolean" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="ProductOrder" />
            <asp:Parameter Name="ProductId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="FormId" QueryStringField="FormId"
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="ProductName" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="SelectQuantity" Type="Boolean" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="ProductOrder" />
        </InsertParameters>
    </asp:SqlDataSource>
    &nbsp;
    <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" Font-Bold="True"
        ForeColor="Red"></asp:Label>
    <table style="margin-right: auto; margin-left: auto" id="EditTable" border="0" cellpadding="0"
        cellspacing="1" width="780">
        <tr>
            <td class="detailheader" valign="top" align="left">
                <asp:Label ID="lblCreateDate" runat="server" Text='<%#Bind("FormId")%>'></asp:Label><br />
            </td>
            <td class="detailtext">
                <h3>
                    <asp:Label ID="lblTitle" runat="server">Edit Form</asp:Label><br />
                </h3>
                <div id="toolbar" class="ui-buttonset ui-corner-all ui-state-default">

                <asp:Button ID="btnSave" runat="server" AccessKey="s" CssClass="ui-state-default ui-corner-all ui-button" Text="Save Changes"
                    title="Save your changes [alt-s]" />
                <asp:Button ID="btnPreview" runat="server" CssClass="ui-state-default ui-corner-all ui-button" Text="Save & View" />
                <asp:Button ID="btnDelete" runat="server" CssClass="ui-state-default ui-corner-all ui-button" Text="Delete" />
                <asp:Button ID="Cancel" runat="server" CssClass="ui-state-default ui-corner-all ui-button" Text="Cancel" />
                <asp:Label ID="lblFormIdLabel1" runat="server" Text='<%#Bind("FormId")%>'></asp:Label>
                </div>

            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" align="left">
                Title
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtFormTitleTextBox" runat="server" Columns="100" Rows="2" Text='<%#Bind("FormTitle")%>'
                    TextMode="MultiLine" Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                Expiration Date
            </td>
            <td class="detailtext">
                <ew:CalendarPopup ID="dateExpirationDateTextBox" runat="server" AllowArbitraryText="False"
                    SelectedDate="2056-02-15" ShowGoToToday="True" VisibleDate="2056-02-15">
                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        Font-Size="XX-Small" ForeColor="Black" />
                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                        ForeColor="Black" />
                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                        ForeColor="Black" />
                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                        ForeColor="Black" />
                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        Font-Size="XX-Small" ForeColor="Black" />
                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                        ForeColor="Black" />
                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        Font-Size="XX-Small" ForeColor="Gray" />
                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                        ForeColor="Black" />
                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                        Font-Size="XX-Small" ForeColor="Black" />
                </ew:CalendarPopup>
                (The form will not accept submissions after this date.)
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" align="left">
                Email Submissions to
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtEmailAddress" runat="server" Columns="80" Width="179px"></asp:TextBox>
                (If blank, payments@freecapitalists.org will be used.)
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                Email From
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtFormAuthorTextBox" runat="server" Columns="80" Text='Ludwig von Mises Institute'></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" align="left">
                Default Product Id
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtPrimaryProductIdTextBox" runat="server" Text='<%#Bind("PrimaryProductId")%>'></asp:TextBox>
                (This product will selected and required.)
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                Image
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtImageTextBox" runat="server" Columns="80" Text='<%#Bind("Image")%>'></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                GUID
            </td>
            <td class="detailtext">
                <asp:Label ID="lblGUID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" align="left">
                Introduction
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtIntroductionTextBox" runat="server" Height="500px" Width="100%"
                    TextMode="MultiLine" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                Footer:
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtFooter" runat="server" Height="500px" Width="100%" TextMode="MultiLine"
                    ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="detailheader" valign="top">
                Email Message:
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtEmailMessage" runat="server" Columns="100" Rows="10" Text='<%#Bind("ThankYouMessage")%>'
                    TextMode="MultiLine"  Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" align="left">
                Thank You<br />
                Message:
            </td>
            <td class="detailtext">
                <asp:TextBox ID="txtThankYouMessageTextBox" runat="server" Columns="100" Rows="10"
                    Text='<%#Bind("ThankYouMessage")%>' TextMode="MultiLine" 
                    Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="detailheader" valign="top" align="left">
                <strong></strong>
            </td>
            <td class="detailtext">
            </td>
        </tr>
        <tr>
            <td class="detailheader" colspan="2" valign="top" align="left">
                <asp:CheckBox ID="chkGetAddressInfoCheckBox" runat="server" Checked='<%#Bind("GetAddressInfo")%>'
                    Text="Get Address Info" /><br />
                <asp:CheckBox ID="chkShowSidebar" runat="server" Checked='<%#Bind("ShowSidebar")%>' Text="Show Sidebar" />
                <br />
                <asp:CheckBox ID="chkTakeCreditCardCheckBox" runat="server" Checked='<%#Bind("TakeCreditCard")%>'
                    Text="Take Credit Card Info (includes address info)" /><br />
                <asp:CheckBox ID="chkProcessPaymentCheckBox" runat="server" Checked='<%#Bind("ProcessPayment")%>'
                    Text="Process credit card payment" />
                <br />
                <asp:CheckBox ID="chkPromptforCustomAmount" runat="server" Checked='<%#Bind("ProcessPayment")%>'
                    Text="Prompt to enter custom amount" />
                <br />
                <asp:CheckBox ID="chkPromptforRecurringDonation" runat="server" Checked='<%#Bind("ProcessPayment")%>'
                    Text="Prompt to make donation recurring" />
            </td>
        </tr>
    </table>
    <br />
    <strong>Products:&nbsp;
        <br />
    </strong>
    <asp:Panel ID="pnlAddProduct" runat="server" BackColor="#E0E0E0" Visible="False">
        <strong>Add Product:</strong><br />
        Name:<br />
        <asp:TextBox ID="txtName" runat="server" Columns="80"></asp:TextBox><br />
        Description:<br />
        <asp:TextBox ID="txtDescription" runat="server" Columns="70" Rows="3" TextMode="MultiLine"
            ></asp:TextBox>
        <br />
        Price:<br />
        <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
        <br />
        <asp:CheckBox ID="chkQuantity" runat="server" Text="Quantity" /><br />
        (If checked, the form will prompt for a quantity, such as the number of guests.)<br />
        <asp:Button ID="btnAddProduct" runat="server" Text="Add Product" /></asp:Panel>
    <br />
    <asp:GridView ID="gvProducts" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ProductId" DataSourceID="sqlProducts"
        ForeColor="#333333" GridLines="None" UseAccessibleHeader="False">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="ProductId" HeaderText="ID #" InsertVisible="False" ReadOnly="True"
                SortExpression="ProductId" />
            <asp:BoundField DataField="ProductName" HeaderText="Name" SortExpression="ProductName" />
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Columns="70" Rows="3" Text='<%#Bind("Description")%>'
                        TextMode="MultiLine" ></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Description")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="{0:c}" />
            <asp:CheckBoxField DataField="SelectQuantity" HeaderText="Qty." SortExpression="SelectQuantity" />
            <asp:BoundField DataField="ProductOrder" HeaderText="Order" SortExpression="ProductOrder" />
            <asp:CommandField HeaderText="Edit" ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <br />
    <strong>Questions:</strong><br />
    <br />
    <asp:Panel ID="pnlAddQuestion" runat="server" BackColor="#E0E0E0" Visible="False">
        <strong>Add Question:</strong><br />
        <asp:TextBox ID="txtQuestion" runat="server" Columns="70" Rows="3" TextMode="MultiLine"
            ></asp:TextBox><br />
        Type:<asp:DropDownList ID="ddQuestionType" runat="server">
            <asp:ListItem Value="1">Textbox</asp:ListItem>
            <asp:ListItem Value="2">Text Area</asp:ListItem>
            <asp:ListItem Value="3">Yes/No</asp:ListItem>
            <asp:ListItem Value="4">Number</asp:ListItem>
             <asp:ListItem Value="5">Donation Amount</asp:ListItem>
        </asp:DropDownList>
        <asp:CheckBox ID="chkRequired" runat="server" Text="Required" />
        <asp:Button ID="btnAddQuestion" runat="server" Text="Add Question" /></asp:Panel>
    <asp:SqlDataSource ID="sqlQuestions" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        DeleteCommand="DELETE FROM [RegistrationQuestions] WHERE [QuestionId] = @QuestionId"
        InsertCommand="INSERT INTO dbo.RegistrationQuestions(FormId, Question, QuestionType, QuestionOrder, Required) VALUES (@FormId, @Question, @QuestionType, @QuestionOrder,@Required)"
        SelectCommand="SELECT QuestionId, FormId, Question, QuestionType, QuestionOrder, Required FROM dbo.RegistrationQuestions WHERE (FormId = @FormId)"
        UpdateCommand="UPDATE dbo.RegistrationQuestions SET Question = @Question, QuestionType = @QuestionType, QuestionOrder = @QuestionOrder, Required = @Required WHERE (QuestionId = @QuestionId)">
        <DeleteParameters>
            <asp:Parameter Name="QuestionId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Question" Type="String" />
            <asp:Parameter Name="QuestionType" Type="Byte" />
            <asp:Parameter Name="QuestionOrder" />
            <asp:Parameter Name="Required" />
            <asp:Parameter Name="QuestionId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="FormId" QueryStringField="FormId"
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="FormId" Type="Int32" />
            <asp:Parameter Name="Question" Type="String" />
            <asp:Parameter Name="QuestionType" Type="Byte" />
            <asp:Parameter Name="QuestionOrder" />
            <asp:Parameter Name="Required" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />
    <asp:GridView ID="gvQuestions" runat="server" AllowPaging="True" AllowSorting="True"
        DataKeyNames="QuestionId" AutoGenerateColumns="False" CellPadding="4" DataSourceID="sqlQuestions"
        ForeColor="#333333" GridLines="None" UseAccessibleHeader="False" Width="594px">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="QuestionId" HeaderText="ID #" InsertVisible="False" ReadOnly="True"
                SortExpression="QuestionId" Visible="False" />
            <asp:BoundField DataField="FormId" HeaderText="FormId" SortExpression="FormId" ReadOnly="True"
                Visible="False" />
            <asp:TemplateField HeaderText="Question" SortExpression="Question">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Columns="50" Rows="3" Text='<%#Bind("Question")%>'
                        TextMode="MultiLine" ></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Question")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="QuestionType" HeaderText="Type" SortExpression="QuestionType" />
            <asp:BoundField DataField="Required" HeaderText="Required" SortExpression="Required" />
            <asp:BoundField DataField="QuestionOrder" HeaderText="Order" SortExpression="QuestionOrder" />
            <asp:CommandField HeaderText="Edit" ShowEditButton="True" ShowDeleteButton="True" />
        </Columns>
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
