<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false"
         MaintainScrollPositionOnPostback="true" ValidateRequest="false"
         Inherits="Manager_PageContent" Title="Mises.org Page Content Editor" Codebehind="PageContent.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        Mises.org Page Content Editor</h2>
    <p>
        Use this editor to create and edit content parts, such as page headers. The name
        field must match the name on the page.</p>
    <asp:GridView ID="gvPageContentList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                  DataKeyNames="ContentID" DataSourceID="sqlContentList">
        <Columns>
            <asp:CommandField ShowSelectButton="True" HeaderText="Edit"></asp:CommandField>
            <asp:BoundField DataField="ContentID" HeaderText="ContentID" InsertVisible="False"
                            ReadOnly="True" SortExpression="ContentID" Visible="False"></asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
            <asp:BoundField DataField="Title" HeaderText="Description" SortExpression="Title">
            </asp:BoundField>
            <asp:BoundField DataField="LastModified" HeaderText="LastModified" SortExpression="LastModified"
                            DataFormatString="{0:d}"></asp:BoundField>
            <asp:BoundField DataField="SortOrder" HeaderText="SortOrder" SortExpression="SortOrder">
            </asp:BoundField>
            <asp:CheckBoxField DataField="Hidden" HeaderText="Hidden" SortExpression="Hidden">
            </asp:CheckBoxField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlContentList" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [PageContent] WHERE [ContentID] = @ContentID" InsertCommand="INSERT INTO [PageContent] ([Name], [Title], [Body], [LastModified], [SortOrder], [Hidden]) VALUES (@Name, @Title, @Body, @LastModified, @SortOrder, @Hidden)"
                       SelectCommand="SELECT * FROM [PageContent] ORDER BY [SortOrder], [Name]" UpdateCommand="UPDATE [PageContent] SET [Name] = @Name, [Title] = @Title, [Body] = @Body, [LastModified] = @LastModified, [SortOrder] = @SortOrder, [Hidden] = @Hidden WHERE [ContentID] = @ContentID">
        <DeleteParameters>
            <asp:Parameter Name="ContentID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Body" Type="String" />
            <asp:Parameter Name="LastModified" Type="DateTime" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
            <asp:Parameter Name="Hidden" Type="Boolean" />
            <asp:Parameter Name="ContentID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Body" Type="String" />
            <asp:Parameter Name="LastModified" Type="DateTime" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
            <asp:Parameter Name="Hidden" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="ContentID"
                     DataSourceID="sqlContentForm" Height="50px" Width="100%" CellPadding="4" ForeColor="#333333"
                     GridLines="None" DefaultMode="Edit">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <Fields>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True"
                              ButtonType="Button" HeaderText="Edit" UpdateText="Save"></asp:CommandField>
            <asp:BoundField DataField="ContentID" HeaderText="ContentID" InsertVisible="False"
                            ReadOnly="True" SortExpression="ContentID"></asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Name (Key)" ReadOnly="True">
                <ControlStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Description (Optional)" SortExpression="Title">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#Bind("Title")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("Title")%>' TextMode="MultiLine"
                                 Columns="100"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("Title")%>'></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Body" SortExpression="Body">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Body")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtFullText" runat="server" Text='<%#Bind("Body")%>' TextMode="MultiLine"
                                 Columns="120" Rows="35" Width="100%"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtFullText" runat="server" Text='<%#Bind("Body")%>' 
                                 Columns="100" Rows="30" TextMode="MultiLine"></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SortOrder" HeaderText="SortOrder (Admin)" SortExpression="SortOrder"
                            ConvertEmptyStringToNull="False" InsertVisible="False" NullDisplayText="0"></asp:BoundField>
            <asp:CheckBoxField DataField="Hidden" HeaderText="Hidden (Unused)" SortExpression="Hidden">
            </asp:CheckBoxField>
        </Fields>
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="sqlContentForm" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [PageContent] WHERE [ContentID] = @ContentID" InsertCommand="INSERT INTO [PageContent] ([Name], [Title], [Body], [SortOrder], [Hidden]) VALUES (@Name, @Title, @Body, @SortOrder, @Hidden)"
                       SelectCommand="SELECT [ContentID], [Name], [Title], [Body], [SortOrder], [Hidden] FROM [PageContent] WHERE ([ContentID] = @ContentID) ORDER BY Name, SortOrder"
                       UpdateCommand="UPDATE [PageContent] SET [Title] = @Title, [Body] = @Body, [SortOrder] = @SortOrder, [Hidden] = @Hidden WHERE [ContentID] = @ContentID">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvPageContentList" DefaultValue="" Name="ContentID"
                                  PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ContentID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Body" Type="String" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
            <asp:Parameter Name="Hidden" Type="Boolean" />
            <asp:Parameter Name="ContentID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Body" Type="String" />
            <asp:Parameter Name="SortOrder" Type="Int32" DefaultValue="0" ConvertEmptyStringToNull="False" />
            <asp:Parameter Name="Hidden" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:Button ID="btnAddNewItem" runat="server" Text="New Content Item" />
</asp:Content>