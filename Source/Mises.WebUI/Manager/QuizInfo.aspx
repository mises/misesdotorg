<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false"
         ValidateRequest="false" Inherits="Manager_QuizInfoPage"
         Title="Quiz Info Editor" Codebehind="QuizInfo.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        input
        {
            width: 100%;
        }
    </style>
    <h2 class="adminSection">
        Quiz Info Editor</h2>
    <p>
        TODO: Some instructions...</p>
    <asp:GridView ID="gvQuizContentList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                  DataKeyNames="QuizID" DataSourceID="sqlContentList">
        <Columns>
            <asp:CommandField ShowSelectButton="True"></asp:CommandField>
            <asp:BoundField DataField="QuizID" HeaderText="QuizID" ReadOnly="True" SortExpression="QuizID">
            </asp:BoundField>
            <asp:BoundField DataField="QuizTitle" HeaderText="QuizTitle" SortExpression="QuizTitle">
            </asp:BoundField>
            <asp:BoundField DataField="Display" HeaderText="Display" SortExpression="Display">
            </asp:BoundField>
            <asp:BoundField DataField="NumTaken" HeaderText="NumTaken" SortExpression="NumTaken">
            </asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFields="QuizId" DataNavigateUrlFormatString="~/Manager/QuizQuestions.aspx?QuizId={0}"
                                HeaderText="Edit Questions" Text="Edit" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlContentList" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="SELECT [QuizID], [QuizTitle], [Display], [NumTaken] FROM [QuizType]">
    </asp:SqlDataSource>
    <asp:DetailsView ID="dvDetailEdit" runat="server" AutoGenerateRows="False" DataKeyNames="QuizID"
                     DataSourceID="sqlContentForm" Height="50px" Width="650px" CellPadding="4" ForeColor="#333333"
                     GridLines="None" DefaultMode="Edit">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <RowStyle BackColor="#EFF3FB" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <Fields>
            <asp:CommandField ShowEditButton="True" ShowInsertButton="True"></asp:CommandField>
            <asp:BoundField DataField="QuizID" HeaderText="QuizID" ReadOnly="True" SortExpression="QuizID">
            </asp:BoundField>
            <asp:BoundField DataField="Language" HeaderText="Language" SortExpression="Language">
            </asp:BoundField>
            <asp:BoundField DataField="NumOfAnswers" HeaderText="NumOfAnswers" SortExpression="NumOfAnswers">
            </asp:BoundField>
            <asp:TemplateField HeaderText="Title" SortExpression="QuizTitle">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("QuizTitle")%>' TextMode="MultiLine"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("QuizTitle")%>' TextMode="MultiLine"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("QuizTitle")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="QuizDescription">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("QuizDescription")%>' TextMode="MultiLine"
                                 CssClass="ckeditor" Rows="10"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("QuizDescription")%>' TextMode="MultiLine"
                                 CssClass="ckeditor" Rows="5"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("QuizDescription")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="QuizPostInfo" SortExpression="QuizPostInfo">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%#Bind("QuizPostInfo")%>' TextMode="MultiLine"
                                 CssClass="ckeditor" Rows="3"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%#Bind("QuizPostInfo")%>' TextMode="MultiLine"
                                 CssClass="ckeditor" Rows="3"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#Bind("QuizPostInfo")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="Display" HeaderText="Display" SortExpression="Display" />
            <asp:BoundField DataField="RecipientEmail" HeaderText="RecipientEmail" SortExpression="RecipientEmail">
            </asp:BoundField>
            <asp:BoundField DataField="NumTaken" HeaderText="NumTaken" SortExpression="NumTaken">
            </asp:BoundField>
            <asp:BoundField DataField="CorrectAnswerText" HeaderText="CorrectAnswerText" SortExpression="CorrectAnswerText">
            </asp:BoundField>
            <asp:BoundField DataField="LabelError" HeaderText="LabelError" SortExpression="LabelError">
            </asp:BoundField>
            <asp:BoundField DataField="LabelEnterEmail" HeaderText="LabelEnterEmail" SortExpression="LabelEnterEmail">
            </asp:BoundField>
            <asp:BoundField DataField="LabelYourAnswer" HeaderText="LabelYourAnswer" SortExpression="LabelYourAnswer">
            </asp:BoundField>
            <asp:BoundField DataField="LabelBestAnswer" HeaderText="LabelBestAnswer" SortExpression="LabelBestAnswer">
            </asp:BoundField>
            <asp:BoundField DataField="LabelYourScore" HeaderText="LabelYourScore" SortExpression="LabelYourScore">
            </asp:BoundField>
            <asp:BoundField DataField="LabelThankYou" HeaderText="LabelThankYou" SortExpression="LabelThankYou">
            </asp:BoundField>
            <asp:BoundField DataField="QuizType" HeaderText="QuizType" SortExpression="QuizType">
            </asp:BoundField>
        </Fields>
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="sqlContentForm" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [QuizType] WHERE [QuizID] = @QuizID" InsertCommand="INSERT INTO [QuizType] ([QuizID], [QuizType], [Language], [NumOfAnswers], [QuizTitle], [QuizDescription], [QuizPostInfo], [Display], [RecipientEmail], [NumTaken], [CorrectAnswerText], [LabelError], [LabelEnterEmail], [LabelYourAnswer], [LabelBestAnswer], [LabelYourScore], [LabelThankYou]) VALUES (@QuizID, @QuizType, @Language, @NumOfAnswers, @QuizTitle, @QuizDescription, @QuizPostInfo, @Display, @RecipientEmail, @NumTaken, @CorrectAnswerText, @LabelError, @LabelEnterEmail, @LabelYourAnswer, @LabelBestAnswer, @LabelYourScore, @LabelThankYou)"
                       SelectCommand="SELECT * FROM [QuizType] WHERE ([QuizID] = @QuizID)" UpdateCommand="UPDATE [QuizType] SET [QuizType] = @QuizType, [Language] = @Language, [NumOfAnswers] = @NumOfAnswers, [QuizTitle] = @QuizTitle, [QuizDescription] = @QuizDescription, [QuizPostInfo] = @QuizPostInfo, [Display] = @Display, [RecipientEmail] = @RecipientEmail, [NumTaken] = @NumTaken, [CorrectAnswerText] = @CorrectAnswerText, [LabelError] = @LabelError, [LabelEnterEmail] = @LabelEnterEmail, [LabelYourAnswer] = @LabelYourAnswer, [LabelBestAnswer] = @LabelBestAnswer, [LabelYourScore] = @LabelYourScore, [LabelThankYou] = @LabelThankYou WHERE [QuizID] = @QuizID">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvQuizContentList" DefaultValue="0" Name="QuizID"
                                  PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="QuizID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="QuizType" Type="String" />
            <asp:Parameter Name="Language" Type="String" />
            <asp:Parameter Name="NumOfAnswers" Type="Int32" />
            <asp:Parameter Name="QuizTitle" Type="String" />
            <asp:Parameter Name="QuizDescription" Type="String" />
            <asp:Parameter Name="QuizPostInfo" Type="String" />
            <asp:Parameter Name="Display" Type="String" />
            <asp:Parameter Name="RecipientEmail" Type="String" />
            <asp:Parameter Name="NumTaken" Type="Int32" />
            <asp:Parameter Name="CorrectAnswerText" Type="String" />
            <asp:Parameter Name="LabelError" Type="String" />
            <asp:Parameter Name="LabelEnterEmail" Type="String" />
            <asp:Parameter Name="LabelYourAnswer" Type="String" />
            <asp:Parameter Name="LabelBestAnswer" Type="String" />
            <asp:Parameter Name="LabelYourScore" Type="String" />
            <asp:Parameter Name="LabelThankYou" Type="String" />
            <asp:Parameter Name="QuizID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="QuizID" Type="Int32" />
            <asp:Parameter Name="QuizType" Type="String" />
            <asp:Parameter Name="Language" Type="String" />
            <asp:Parameter Name="NumOfAnswers" Type="Int32" />
            <asp:Parameter Name="QuizTitle" Type="String" />
            <asp:Parameter Name="QuizDescription" Type="String" />
            <asp:Parameter Name="QuizPostInfo" Type="String" />
            <asp:Parameter Name="Display" Type="String" />
            <asp:Parameter Name="RecipientEmail" Type="String" />
            <asp:Parameter Name="NumTaken" Type="Int32" />
            <asp:Parameter Name="CorrectAnswerText" Type="String" />
            <asp:Parameter Name="LabelError" Type="String" />
            <asp:Parameter Name="LabelEnterEmail" Type="String" />
            <asp:Parameter Name="LabelYourAnswer" Type="String" />
            <asp:Parameter Name="LabelBestAnswer" Type="String" />
            <asp:Parameter Name="LabelYourScore" Type="String" />
            <asp:Parameter Name="LabelThankYou" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:Button ID="btnAddNewItem" runat="server" Text="Add New Quiz" />
</asp:Content>