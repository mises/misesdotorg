<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" Title="Mises Daily Manager" Inherits="MisesWeb.Manager.DailyArticlesArchiveManager" MaintainScrollPositionOnPostback="True" Codebehind="articles.aspx.cs" %>

<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="text-align:center;">
        Mises Daily Manager</h1>
    <h3 class="button" style="margin: 20px;">        
        <a href="article.aspx">Add New Daily Article</a>
    </h3>
    
    <div class="adminSection">

    <h2>
        <asp:Image runat="server" ID="imgAuthorHeader" Visible="false" />
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h2>    
    <cc1:GridView ID="gvArchives" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                  DataKeyNames="Month, AuthorName,CoAuthorId, CoAuthorName, Photo , CoPhoto" AllowPaging="True"
                  DataSourceID="sqlDailyArticles" PageSize="80" PagerSettings-PageButtonCount="20"
                  EnableNextPrevNumericPager="True" ShowSortDirection="True" SortAscImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/up.gif"
                  SortColumnHeaderIsBold="True" SortDescImageUrl="//mises.freecapitalists.org/images/Theme/images/buttons/down.gif">
        <PagerSettings PageButtonCount="20" Position="TopAndBottom"></PagerSettings>
        <Columns>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#Convert.ToString(Eval("Title"))%>'
                                   NavigateUrl='<%#DataBinder.Eval(Container, "DataItem.ArticleId", "article.aspx?Id={0}")%>'> </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="AuthorId" DataNavigateUrlFormatString="articles.aspx?AuthorId={0}"
                                DataTextField="AuthorName" HeaderText="Author" SortExpression="AuthorName">
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:HyperLinkField>
            <asp:BoundField DataField="DatePosted" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"
                            SortExpression="DatePosted">
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundField>
        </Columns>
    </cc1:GridView>
    
    </div>
    <div class="adminSection">
    <h2>Recent Edits:</h2>

    <asp:SqlDataSource ID="sqlDailyArticles" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       SelectCommand="DailyArticlesGetArchive" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False"
                       CacheDuration="160" EnableCaching="False">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="AuthorId" QueryStringField="AuthorId"
                                      Type="Int32" />
            <asp:QueryStringParameter Name="SearchQuery" QueryStringField="q" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="gvRecentEdits" runat="server" AutoGenerateColumns="false" EnableSortingAndPagingCallbacks="true"
                  AllowSorting="true">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="ArticleId" DataNavigateUrlFormatString="article.aspx?Id={0}"
                                DataTextField="Title" HeaderText="Recent Daily Edits" />
            <asp:BoundField HeaderText="Editor" DataField="EditBy" />
            <asp:BoundField HeaderText="EditDate" DataField="EditDate" />
        </Columns>
    </asp:GridView>
    
    </div>
</asp:Content>