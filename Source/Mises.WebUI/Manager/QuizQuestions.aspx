<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false"
    MaintainScrollPositionOnPostback="true" ValidateRequest="false" Inherits="Manager_QuizQuestionsPage"
    Title="Quiz Info Editor" CodeBehind="QuizQuestions.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        Quiz Question Editor</h2>
    <p>
        Some instructions...</p>
    <asp:GridView ID="gvQuizContentList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
        OnRowCommand="gvQuizContentListRowCommand" DataKeyNames="QuestionID,Question"
        DataSourceID="sqlContentList" AllowPaging="True">
        <PagerSettings Mode="NumericFirstLast" />
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Select"
                        Text="Select" CommandArgument="<%#Container.DisplayIndex%>" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="Add"
                        CommandArgument="<%#Container.DisplayIndex%>" Text="Add" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="QuestionID" HeaderText="QuestionID" ReadOnly="True" SortExpression="QuestionID"
                InsertVisible="False"></asp:BoundField>
            <asp:BoundField DataField="QuizID" HeaderText="QuizID" SortExpression="QuizID" Visible="False">
            </asp:BoundField>
            <asp:TemplateField HeaderText="Question" SortExpression="Question">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("Question")%>' TextMode="MultiLine"
                        Rows="3"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Question")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SortOrder" HeaderText="SortOrder" SortExpression="SortOrder">
            </asp:BoundField>
            <asp:BoundField DataField="CorrectAnswer" HeaderText="CorrectAnswer" SortExpression="CorrectAnswer" />
            <asp:CommandField ShowEditButton="True" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlContentList" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        SelectCommand="SELECT * FROM [QuizQuestion] WHERE ([QuizID] = @QuizID)" DeleteCommand="DELETE FROM [QuizQuestion] WHERE [QuestionID] = @QuestionID"
        InsertCommand="INSERT INTO [QuizQuestion] ([QuizID], [Question], [SortOrder], [CorrectAnswer]) VALUES (@QuizID, @Question, @SortOrder, @CorrectAnswer)"
        UpdateCommand="UPDATE [QuizQuestion] SET [QuizID] = @QuizID, [Question] = @Question, [SortOrder] = @SortOrder, [CorrectAnswer] = @CorrectAnswer WHERE [QuestionID] = @QuestionID">
        <SelectParameters>
            <asp:QueryStringParameter Name="QuizID" QueryStringField="QuizId" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="QuestionID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="QuizID" Type="Int32" />
            <asp:Parameter Name="Question" Type="String" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
            <asp:Parameter Name="CorrectAnswer" Type="Byte" />
            <asp:Parameter Name="QuestionID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="QuizID" Type="Int32" />
            <asp:Parameter Name="Question" Type="String" />
            <asp:Parameter Name="SortOrder" Type="Int32" />
            <asp:Parameter Name="CorrectAnswer" Type="Byte" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:DetailsView ID="dvDetailEdit" runat="server" AutoGenerateRows="False" DataKeyNames="AnswerID"
        DataSourceID="sqlContentForm" Height="50px" Width="650px" CellPadding="4" ForeColor="#333333"
        GridLines="None" DefaultMode="Edit" AllowPaging="True">
        <PagerSettings Position="TopAndBottom" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <RowStyle BackColor="#EFF3FB" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <PagerStyle BackColor="#2461BF" Font-Bold="True" Font-Size="16pt" ForeColor="#99FF99"
            HorizontalAlign="Center" />
        <Fields>
            <asp:BoundField DataField="AnswerID" HeaderText="AnswerID" ReadOnly="True" SortExpression="AnswerID"
                InsertVisible="False"></asp:BoundField>
            <asp:TemplateField HeaderText="QuestionID" SortExpression="QuestionID">
                <EditItemTemplate>
                    <asp:TextBox ID="txtQuestionId" runat="server" Text='<%#Bind("QuestionID")%>'></asp:TextBox>
                    <asp:Label ID="lblQuestion" runat="server"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtQuestionId" runat="server" Text='<%#Bind("QuestionID")%>'></asp:TextBox>
                    <asp:Label ID="lblQuestion" runat="server"></asp:Label>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#Bind("QuestionID")%>'></asp:Label>
                    <asp:Label ID="lblQuestion" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Answer" SortExpression="Answer">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" Columns="100" Rows="10" runat="server" Text='<%#Bind("Answer")%>'
                        TextMode="MultiLine"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" Columns="100" Rows="10" runat="server" Text='<%#Bind("Answer")%>'
                        TextMode="MultiLine"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Answer")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AnswerValue" HeaderText="AnswerValue" SortExpression="AnswerValue">
            </asp:BoundField>
            <asp:TemplateField HeaderText="AnswerExplanation" SortExpression="AnswerExplanation">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("AnswerExplanation")%>' Columns="100"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("AnswerExplanation")%>' Columns="100"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("AnswerExplanation")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" ButtonType="Button" />
            <asp:CommandField ShowEditButton="True" ButtonType="Button" />
            <asp:CommandField ShowDeleteButton="True" ButtonType="Button" />
        </Fields>
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="sqlContentForm" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        DeleteCommand="DELETE FROM [QuizAnswer] WHERE [AnswerID] = @AnswerID" InsertCommand="INSERT INTO [QuizAnswer] ([QuestionID], [Answer], [AnswerValue], [AnswerExplanation]) VALUES (@QuestionID, @Answer, @AnswerValue, @AnswerExplanation)"
        SelectCommand="SELECT * FROM [QuizAnswer] WHERE ([QuestionID] = @QuestionID)"
        UpdateCommand="UPDATE [QuizAnswer] SET [QuestionID] = @QuestionID, [Answer] = @Answer, [AnswerValue] = @AnswerValue, [AnswerExplanation] = @AnswerExplanation WHERE [AnswerID] = @AnswerID">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvQuizContentList" Name="QuestionID" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="AnswerID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="QuestionID" Type="Int32" />
            <asp:Parameter Name="Answer" Type="String" />
            <asp:Parameter Name="AnswerValue" Type="Int32" />
            <asp:Parameter Name="AnswerExplanation" Type="String" />
            <asp:Parameter Name="AnswerID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="QuestionID" Type="Int32" />
            <asp:Parameter Name="Answer" Type="String" />
            <asp:Parameter Name="AnswerValue" Type="Int32" />
            <asp:Parameter Name="AnswerExplanation" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:Button ID="btnAddNewItem" runat="server" Text="New Answer" />
</asp:Content>
