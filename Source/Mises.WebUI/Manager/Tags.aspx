<%@ Page Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="false"
         MaintainScrollPositionOnPostback="true" Inherits="Manager_Tags"
         Title="Manage Tags" Codebehind="Tags.aspx.cs" %>

<%@ Register Src="../Controls/Tagging/TaggingStats.ascx" TagName="TaggingStats" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Tag Central</h1>
    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
    <h4>
        Delete tag:</h4>
    <asp:TextBox ID="txtTag" runat="server"></asp:TextBox>
    <asp:Button ID="btnDeleteTag" runat="server" Text="Delete Tag" /><br />
    <h4>
        Delete <strong>all</strong> tags by user:</h4>
    <asp:TextBox ID="txtUser" runat="server"></asp:TextBox><asp:Button ID="btnDeleteUserTags"
                                                                       runat="server" Text="Delete Users Tags" /><br />
    <h4>
        Update Tag:</h4>
    From:
    <asp:TextBox ID="txtOldTag" runat="server"></asp:TextBox>
    To:
    <asp:TextBox ID="txtNewTag" runat="server"></asp:TextBox>
    <asp:Button ID="btnReplaceTag" runat="server" Text="Update" />
    <br />
    <br />
    <h4>
        Recent Anonymous Taggers:</h4>
    <strong>&quot;Delete All&quot; deletes all tags by that user.</strong>
    <br />
    <asp:GridView ID="gvAnonymousTaggers" runat="server" DataKeyNames="TaggedBy,Tag"
                  AutoGenerateColumns="False" AllowPaging="True" DataSourceID="objTagging" PageSize="100"
                  PagerSettings-Position="TopAndBottom" AllowSorting="True">
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="40" Position="TopAndBottom" />
        <Columns>
            <asp:HyperLinkField HeaderText="Tag" SortExpression="Tag" 
                                DataNavigateUrlFields="Tag" DataNavigateUrlFormatString="/tag/{0}" 
                                DataTextField="Tag" />
            <asp:HyperLinkField DataNavigateUrlFields="TaggedBy" 
                                DataNavigateUrlFormatString="/tags.aspx?user={0}" DataTextField="TaggedBy" 
                                HeaderText="Tagged By"></asp:HyperLinkField>
            <asp:BoundField DataField="TaggedDate" DataFormatString="{0:d}" HeaderText="Date"
                            SortExpression="TaggedDate" />
            <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                <itemtemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CommandArgument='<%#Eval("Tag")%>'
                                    CommandName="DeleteTag" Text="Delete"></asp:LinkButton>
                </itemtemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete All" ShowHeader="False">
                <itemtemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("TaggedBy")%>'
                                    CommandName="DeleteUser" Text="Delete All"></asp:LinkButton>
                </itemtemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="objTagging" runat="server" SelectMethod="GetAnonymousTaggers"
                          TypeName="Mises.Domain.Tags.Tagging"></asp:ObjectDataSource>
    <br />
    <br />
    <a href="/Community/controlpanel/tools/ManageCensorships.aspx">
        Edit Banned Words</a><br />
    <uc1:TaggingStats ID="TaggingStats1" runat="server" />
</asp:Content>