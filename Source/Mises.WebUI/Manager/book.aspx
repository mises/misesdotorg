﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Manager.master" AutoEventWireup="true" Inherits="Manager_book" Codebehind="book.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="header">
        <h1>
            <a href="library.aspx">Ward and Massey Library Admin</a></h1>
    </div>
    <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="100%" AutoGenerateRows="False"
                     DefaultMode="Edit" CellPadding="4" DataKeyNames="control" DataSourceID="sqlBook"
                     ForeColor="#333333" GridLines="None" AllowPaging="True">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" Width="600px" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:BoundField DataField="control" HeaderText="control" ReadOnly="True" SortExpression="control"
                            InsertVisible="False" Visible="False" />
            <asp:CommandField ButtonType="Button" ShowDeleteButton="True" ShowEditButton="True"
                              ShowInsertButton="True" />
            <asp:TemplateField HeaderText="Book Title" SortExpression="title">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Rows="5" Text='<%#Bind("title")%>' TextMode="MultiLine"
                                 Width="100%"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Bind("title")%>' Rows="5" TextMode="MultiLine"
                                 Width="100%"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("title")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Edition" HeaderText="Edition:" SortExpression="Edition" />
            <asp:BoundField DataField="ISBN" HeaderText="ISBN Number:" SortExpression="ISBN" />
            <asp:BoundField DataField="Pages" HeaderText="Pages:" SortExpression="Pages" />
            <asp:BoundField DataField="Binding" HeaderText="Binding:" SortExpression="Binding" />
       <%-- <asp:BoundField DataField="checked_out_by" HeaderText="Checked Out By:" SortExpression="checked_out_by" /> --%>
            <asp:BoundField DataField="donatedBy" HeaderText="Donated By" SortExpression="donatedBy" />
            <asp:BoundField DataField="Quantity" HeaderText="Number of Copies:" SortExpression="Quantity" />
            <asp:BoundField DataField="Location" HeaderText="Location (Ward or Massey):" SortExpression="Location" />

            <asp:BoundField DataField="donatedDate" DataFormatString="{0:d}" HeaderText="Donated Date:"
                            SortExpression="donatedDate" />
            <asp:BoundField DataField="SpecialCollection" HeaderText="Special Collection:" SortExpression="SpecialCollection" />
            <asp:BoundField DataField="authorfirst" HeaderText="Author First Name:" SortExpression="authorfirst" />
            <asp:BoundField DataField="authorlast" HeaderText="Author Last Name:" SortExpression="authorlast" />
            <asp:BoundField DataField="authorfirst2" HeaderText="Author First Name2:" SortExpression="authorfirst2" />
            <asp:BoundField DataField="authorlast2" HeaderText="Author Last Name2:" SortExpression="authorlast2" />
            <asp:BoundField DataField="authorfirst3" HeaderText="Author First Name3:" SortExpression="authorfirst3" />
            <asp:BoundField DataField="authorlast3" HeaderText="Author Last Name3:" SortExpression="authorlast3" />
            <asp:BoundField DataField="EditorFirst" HeaderText="Editor First Name:" SortExpression="EditorFirst" />
            <asp:BoundField DataField="EditorLast" HeaderText="Editor Last Name:" SortExpression="EditorLast" />
            <asp:BoundField DataField="PublishedDate" HeaderText="Original Publish Date:" SortExpression="PublishedDate" />
            <asp:BoundField DataField="PublisherName" HeaderText="Publisher Name:" SortExpression="PublisherName" />
            <asp:BoundField DataField="PublisherURL" HeaderText="Publisher Website:" SortExpression="PublisherURL" />
            <asp:BoundField DataField="PublisherPhone" HeaderText="Publisher Phone:" SortExpression="PublisherPhone" />
            <asp:TemplateField HeaderText="Publisher Additional Info:" SortExpression="publisher_info">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" Rows="5" TextMode="MultiLine" Width="100%" runat="server"
                                 Text='<%#Bind("publisher_info")%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" Rows="5" TextMode="MultiLine" Width="100%" runat="server"
                                 Text='<%#Bind("publisher_info")%>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#Bind("publisher_info")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Additional Info:" SortExpression="comments">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" Rows="5" TextMode="MultiLine" Width="100%" runat="server"
                                 Text='<%#Bind("comments")%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    (i.e. &quot;inflation,economics,monetary&quot;..etc)
                    <asp:TextBox ID="TextBox5" Rows="5" TextMode="MultiLine" Width="100%" runat="server"
                                 Text='<%#Bind("comments")%>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#Bind("comments")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="subject" HeaderText="Subject:" SortExpression="subject" />
         <%--   <asp:BoundField DataField="coauthors" HeaderText="Coauthors" SortExpression="coauthors" /> --%>
            <asp:BoundField DataField="CreateDate" HeaderText="Date Added to Library:" InsertVisible="False" SortExpression="CreateDate" />
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="sqlBook" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                       DeleteCommand="DELETE FROM [WardLibrary] WHERE [control] = @control" InsertCommand="INSERT INTO [WardLibrary] ([title], [checked_out_by], [authorfirst], [authorlast], [authorfirst2], [authorlast2], [authorfirst3], [authorlast3], [publisher_info], [comments], [donatedBy], [donatedDate], [coauthors], [Edition], [ISBN], [Pages], [Binding], [Quantity], [Location], [SpecialCollection], [EditorFirst], [EditorLast], [PublisherURL], [PublisherPhone], [PublisherName], [subject]) VALUES (@title, @checked_out_by, @authorfirst, @authorlast, @authorfirst2, @authorlast2, @authorfirst3, @authorlast3, @publisher_info, @comments, @donatedBy, @donatedDate, @coauthors, @Edition, @ISBN, @Pages, @Binding, @Quantity, @Location, @SpecialCollection, @EditorFirst, @EditorLast, @PublisherURL, @PublisherPhone, @PublisherName, @subject)"
                       SelectCommand="SELECT * FROM [WardLibrary] WHERE ([control] = @control)" UpdateCommand="UPDATE [WardLibrary] SET [title] = @title, [checked_out_by] = @checked_out_by, [authorfirst] = @authorfirst, [authorlast] = @authorlast, [authorfirst2] = @authorfirst2, [authorlast2] = @authorlast2, [authorfirst3] = @authorfirst3, [authorlast3] = @authorlast3, [publisher_info] = @publisher_info, [comments] = @comments, [donatedBy] = @donatedBy, [donatedDate] = @donatedDate, [coauthors] = @coauthors, [Edition] = @Edition, [ISBN] = @ISBN, [Pages] = @Pages, [Binding] = @Binding, [Quantity] = @Quantity, [Location] = @Location, [SpecialCollection] = @SpecialCollection, [EditorFirst] = @EditorFirst, [EditorLast] = @EditorLast, [PublisherURL] = @PublisherURL, [PublisherPhone] = @PublisherPhone, [PublisherName] = @PublisherName, [subject] = @subject WHERE [control] = @control">
        <DeleteParameters>
            <asp:Parameter Name="control" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>            
            <asp:Parameter Name="title" Type="String" />
            <asp:Parameter Name="checked_out_by" Type="String" />
            <asp:Parameter Name="authorfirst" Type="String" />
            <asp:Parameter Name="authorlast" Type="String" />
            <asp:Parameter Name="authorfirst2" Type="String" />
            <asp:Parameter Name="authorlast2" Type="String" />
            <asp:Parameter Name="authorfirst3" Type="String" />
            <asp:Parameter Name="authorlast3" Type="String" />
            <asp:Parameter Name="publisher_info" Type="String" />
            <asp:Parameter Name="comments" Type="String" />
            <asp:Parameter Name="donatedBy" Type="String" />
            <asp:Parameter Name="donatedDate" Type="DateTime" />
            <asp:Parameter Name="coauthors" Type="String" />
            <asp:Parameter Name="Edition" Type="String" />  
            <asp:Parameter Name="ISBN" Type="String" />  
            <asp:Parameter Name="Pages" Type="String" />  
            <asp:Parameter Name="Binding" Type="String" />  
            <asp:Parameter Name="Quantity" Type="String" />  
            <asp:Parameter Name="Location" Type="String" />  
            <asp:Parameter Name="SpecialCollection" Type="String" />  
            <asp:Parameter Name="EditorFirst" Type="String" />  
            <asp:Parameter Name="EditorLast" Type="String" /> 
            <asp:Parameter Name="PublisherURL" Type="String" />  
            <asp:Parameter Name="PublisherPhone" Type="String" />  
            <asp:Parameter Name="PublisherName" Type="String" />   
            <asp:Parameter Name="subject" Type="String" />            
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="control" QueryStringField="Id" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>            
            <asp:Parameter Name="title" Type="String" />
            <asp:Parameter Name="checked_out_by" Type="String" />
            <asp:Parameter Name="authorfirst" Type="String" />
            <asp:Parameter Name="authorlast" Type="String" />
            <asp:Parameter Name="authorfirst2" Type="String" />
            <asp:Parameter Name="authorlast2" Type="String" />
            <asp:Parameter Name="authorfirst3" Type="String" />
            <asp:Parameter Name="authorlast3" Type="String" />
            <asp:Parameter Name="publisher_info" Type="String" />
            <asp:Parameter Name="comments" Type="String" />
            <asp:Parameter Name="donatedBy" Type="String" />
            <asp:Parameter Name="donatedDate" Type="DateTime" />
            <asp:Parameter Name="coauthors" Type="String" />
             <asp:Parameter Name="Edition" Type="String" />  
            <asp:Parameter Name="ISBN" Type="String" />  
            <asp:Parameter Name="Pages" Type="String" />  
            <asp:Parameter Name="Binding" Type="String" />  
            <asp:Parameter Name="Quantity" Type="String" />  
            <asp:Parameter Name="Location" Type="String" />  
            <asp:Parameter Name="SpecialCollection" Type="String" />  
            <asp:Parameter Name="EditorFirst" Type="String" />  
            <asp:Parameter Name="EditorLast" Type="String" /> 
            <asp:Parameter Name="PublisherURL" Type="String" />  
            <asp:Parameter Name="PublisherPhone" Type="String" />  
            <asp:Parameter Name="PublisherName" Type="String" />
            <asp:Parameter Name="subject" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="control" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>