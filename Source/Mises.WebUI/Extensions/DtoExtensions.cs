﻿#region

using System;

#endregion

namespace Mises.Domain.Contracts
{
    public static class DtoExtensions
    {
        //TODO something better than these but for now...

        public static Boolean IsDownloadable(this VideoDTO videoDTO)
        {
            return (videoDTO.MimeType == VideoTypes.Mp4) || (videoDTO.MimeType == VideoTypes.Wmv);
        }

        public static Boolean IsDownloadable(this AudioDTO audioDTO)
        {
            return ((audioDTO.MimeType == AudioTypes.Mp3) || (audioDTO.MimeType == AudioTypes.Mp4a));
        }

        public static Boolean IsDownloadable(this LiteratureDTO literatureDTO)
        {
            return ((literatureDTO.MimeType == LiteratureTypes.Pdf) ||
                    (literatureDTO.MimeType == LiteratureTypes.Document) ||
                    (literatureDTO.MimeType == LiteratureTypes.Mobi) ||
                    (literatureDTO.MimeType == LiteratureTypes.Ebook));
        }

        public static String GetShortDescription(this MediaDTO mediaDTO)
        {
            String result = VideoTypes.Undefined;

            switch (mediaDTO.MimeType)
            {
                case VideoTypes.Mp4:
                    result = "Mp4";
                    break;
                case VideoTypes.Wmv:
                    result = "Wmv";
                    break;
                case VideoTypes.Youtube:
                    result = "Youtube";
                    break;
                case VideoTypes.Stream:
                    result = "Stream";
                    break;
                case AudioTypes.Mp3:
                    result = "Mp3";
                    break;
                case AudioTypes.Mp4a:
                    result = "Mp4a";
                    break;
                case LiteratureTypes.Pdf:
                    result = "Pdf";
                    break;
                case LiteratureTypes.Document:
                    result = "Doc";
                    break;
                case LiteratureTypes.Ebook:
                    result = "Epub";
                    break;
                case LiteratureTypes.Mobi:
                    result = "Amazon Mobi";
                    break;
            }

            return result;
        }
    }
}