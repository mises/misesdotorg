﻿#region

using System;
using System.Web.Mvc;

#endregion

namespace Mises.Web.Controllers
{
    public static class ControllerExtensions
    {
        public static ViewResult RazorView(this Controller controller)
        {
            return RazorView(controller, null, null);
        }

        public static ViewResult RazorView(this Controller controller, Object model)
        {
            return RazorView(controller, null, model);
        }

        public static ViewResult RazorView(this Controller controller, String viewName)
        {
            return RazorView(controller, viewName, null);
        }

        public static ViewResult RazorView(this Controller controller, String viewName, Object model)
        {
            if (model != null)
                controller.ViewData.Model = model;

            controller.ViewBag._ViewName = GetViewName(controller, viewName);

            const string titlePrepend = ""; //TODO worth putting in config?
            const string titlePostPend = " - Ludwig von Mises Institute";

            if (controller.ViewBag.Title == null)
            {
                controller.ViewBag.Title = "Ludwig von Mises Institute";

                var prop = model.GetType().GetProperty("Title");
                if (prop != null)
                {
                    var propValue = prop.GetValue(model, null);
                    if (propValue != null)
                    {
                        controller.ViewBag.Title = String.Concat(titlePrepend, propValue.ToString(), titlePostPend);
                    }
                }
            }
            else
            {
                controller.ViewBag.Title = String.Concat(titlePrepend, controller.ViewBag.Title);
            }

            return new ViewResult
                       {
                           ViewName = "RazorView",
                           ViewData = controller.ViewData,
                           TempData = controller.TempData
                       };
        }

        private static string GetViewName(Controller controller, String viewName)
        {
            return !String.IsNullOrEmpty(viewName)
                       ? viewName
                       : controller.RouteData.GetRequiredString("action");
        }
    }
}