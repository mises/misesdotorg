﻿#region

using System;
using System.Configuration;
using AutoMapper;
using Mises.Domain.Contracts;
using Mises.Web.ViewModels;
using Mises.Web.ViewModels.Media;
using BrowseViewModel = Mises.Web.ViewModels.Authors.BrowseViewModel;
using DetailViewModel = Mises.Web.ViewModels.Authors.DetailViewModel;

#endregion

// For now just have the defaults as a static rather than extensions...

namespace System.Web
{
    public static class HttpApplicationExtensions
    {
        internal static void CreateMaps(this HttpApplication httpApplication)
        {
            /*Mapper.CreateMap<SubjectDTO, Mises.Web.ViewModels.Subjects.DetailViewModel>();
			Mapper.CreateMap<PaginableResult<SubjectSummaryDTO>, Mises.Web.ViewModels.Subjects.IndexViewModel>();*/
            Mapper.CreateMap<PaginableResult<CategorySummaryDTO>, BrowseViewModel<CategorySummaryDTO>>();
            Mapper.CreateMap<PaginableResult<DocumentDTO>, BrowseViewModel<DocumentDTO>>();

            Mapper.CreateMap<DocumentDTO, VideoPodViewModel>();

            Mapper.CreateMap<AuthorDTO, DetailViewModel>();
            Mapper.CreateMap<PaginableResult<AuthorDTO>, BrowseViewModel>();

            Mapper.CreateMap<PaginableResult<ArticleSummaryDTO>, Mises.Web.ViewModels.Articles.BrowseViewModel>();

            Mapper.CreateMap<VideoDTO, VideoPlayerViewModel>();
            Mapper.CreateMap<AudioDTO, AudioPlayerViewModel>();
            Mapper.CreateMap<LiteratureDTO, LiteratureViewerViewModel>();
            Mapper.CreateMap<DocumentDTO, Mises.Web.ViewModels.Media.DetailViewModel>();
            Mapper.CreateMap<PaginableResult<MisesDTO>, SearchViewModel>();
            Mapper.CreateMap<PaginableResult<DocumentDTO>, Mises.Web.ViewModels.Media.BrowseViewModel>();
            Mapper.CreateMap<PaginableResult<MisesDTO>, Mises.Web.ViewModels.Media.BrowseViewModel>();

            Mapper.CreateMap<Mises.Data.DocumentDTO, VideoPodViewModel>();
        }
    }
}

namespace Mises.Web
{
    public static class ApplicationDefaults
    {
        private static Int32? _summaryPageSize;

        private static Int32? _pageSize;

        private static Int32? _listSize;

        public static Int32 SummaryPageSize
        {
            get
            {
                if (!_summaryPageSize.HasValue)
                    setFromConfig(9, "DefaultSummaryResultsPageSize", out _summaryPageSize);

                return _summaryPageSize.Value;
            }
        }

        public static Int32 PageSize
        {
            get
            {
                if (!_pageSize.HasValue)
                    setFromConfig(5, "DefaultResultsPageSize", out _pageSize);

                return _pageSize.Value;
            }
        }

        public static Int32 ListSize
        {
            get
            {
                if (!_listSize.HasValue)
                    setFromConfig(10, "DefaultPaginationListSize", out _listSize);

                return _listSize.Value;
            }
        }

        private static void setFromConfig(Int32 fallbackValue, String settingName, out Int32? value)
        {
            Int32 result = fallbackValue;
            Int32 attempt;

            if (Int32.TryParse(ConfigurationManager.AppSettings[settingName], out attempt) && (attempt > 0))
                result = attempt;

            value = result;
        }
    }
}