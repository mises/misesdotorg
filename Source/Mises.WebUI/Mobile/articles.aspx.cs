#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Mises.Data;
using Mises.Domain.Documents;

#endregion

namespace MisesWeb.Mobile
{
    partial class DailyArticlesArchive : Page
    {
        private int _authorId;

        private void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }

            Int32.TryParse(Request.QueryString["AuthorId"], out _authorId);

            #region Lookup AuthorId by Author Last Name

            string author = DataFormat.StripHTML(Request.QueryString["Author"]);
            if (!String.IsNullOrEmpty(author))
            {
                // Show particular author

                object selectedAuthor = SqlHelper.ExecuteScalar(DataHelper.ConnectionString,
                                                                CommandType.Text,
                                                                "SELECT AuthorId FROM DocumentAuthors WHERE AuthorLast = @AuthorLast",
                                                                new SqlParameter("@AuthorLast", author));
                if (selectedAuthor != null && int.TryParse(selectedAuthor.ToString(), out _authorId))
                {
                    Response.Redirect("/articles.aspx?AuthorId=" + _authorId);
                }
            }

            #endregion Lookup AuthorId by Author Last Name

            if (Request.QueryString["action"] == "search")
            {
                DailyArticleList1.Visible = true;
                DailyArticleGallery1.Visible = false;

                if (DailyArticleList1.ArchivesGrid.Rows.Count > 0)
                {
                    lblTitle.Text = "Daily Articles Search: " + DailyArticleList1.ArchivesGrid.Rows.Count + " results";
                }
                else
                {
                    lblTitle.Text = "Sorry, no results for \"" + DataFormat.StripHTML(Request.QueryString["q"]);
                }

                return;
            }

            if (Request.QueryString["action"] == "list") // show list of all articles
            {
                DailyArticleGallery1.Visible = false;
                DailyArticleList1.Visible = true;
                return;
            }

            try
            {
                if (_authorId == -1)
                {
                    // Show Author List:
                    Title = "Mises Daily Authors";
                    lblTitle.Text = Title;
                    gvAuthors.DataSource = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                                    CommandType.StoredProcedure,
                                                                    "dbo.DocumentAuthorGetList",
                                                                    new SqlParameter("@ContentType", "DailyArticles"));
                    gvAuthors.DataBind();
                    DailyArticleGallery1.Visible = false;
                    return;
                }
                if (_authorId > 0)
                {
                    // Show particular author's articles:
                    var dsAuthor = DocumentAuthors.GetAuthorBioByAuthorId(_authorId);

                    var rowAuthor = dsAuthor.Tables[0].Rows[0];

                    string authorName = Convert.ToString(rowAuthor["Author"]);
                    //string coAuthorName = Convert.ToString(DailyArticleGallery1.ArchivesGrid.DataKeys[0]["CoAuthorName"]);
                    //int coAuthorId = int.Parse(DailyArticleGallery1.ArchivesGrid.DataKeys[0]["CoAuthorId"].ToString());

                    string photoURL = rowAuthor["Photo"].ToString();
                    //if (_authorId == coAuthorId)
                    //{
                    //    authorName = coAuthorName;
                    //    photoURL = DailyArticleGallery1.ArchivesGrid.DataKeys[0]["CoPhoto"].ToString();
                    //}
                    lblTitle.Text = authorName;

                    var hm = new HtmlMeta
                                 {
                                     Name = "Description",
                                     Content = "Mises Daily archive for " + authorName
                                 };
                    Page.Header.Controls.Add(hm);
                    Page.Title = hm.Content;

                    var link = new HtmlLink();
                    string baseURL = Request.Url.Scheme + "://" + Request.Url.Authority;
                    link.Attributes.Add("type", "application/rss+xml");
                    link.Attributes.Add("rel", "alternate");
                    link.Attributes.Add("title", "Mises Daily feed for " + authorName);
                    link.Attributes.Add("href",
                                        string.Format(baseURL + "/Feeds/articles.ashx?AuthorId={0}", _authorId));
                    Page.Header.Controls.AddAt(1, link);

                    if (!string.IsNullOrEmpty(photoURL))
                    {
                        imgAuthorHeader.Visible = true;
                        imgAuthorHeader.ImageUrl =
                            string.Format("/Controls/Theme/AuthorHeaderImage.ashx?url={0}&name={1}", photoURL, authorName);
                        imgAuthorHeader.AlternateText = authorName;
                    }
                }
            }
            catch (FormatException)
            {
                // do nothing
            }
        }
    }
}