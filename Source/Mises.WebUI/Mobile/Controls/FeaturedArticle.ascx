<%@ Control Language="C#" AutoEventWireup="false"
            EnableViewState="false" Inherits="Mobile.Controls.Controls_Theme_FeaturedArticle" Codebehind="FeaturedArticle.ascx.cs" %>
<script type="text/javascript">

    var mydate = new Date();
    mydate.setDate(mydate.getDate() - 1);
    var today = new Date();
    var currentDay = new Date();

    $(document).ready(function() {
        $("#Previous").click(function() {

            $("#Next").css("display", "inherit");

            GetDaysArticles((new Date(currentDay).setDate(new Date(currentDay).getDate() - 1)), false);
        });

        $("#Next").click(function() {
            GetDaysArticles((new Date(currentDay).setDate(new Date(currentDay).getDate() + 1)), true);
        });

    });


</script>
<div id="arrows">
    <img id="Previous" alt="Previous day's articles" src="//mises.freecapitalists.org/images/Theme/images/PreviousDay.png"
         width="163" height="25" style="position: relative; cursor: pointer" />
    <img id="Next" alt="Next day's articles" src="//mises.freecapitalists.org/images/Theme/images/NextDay.png" width="143"
         height="25" style="float: right; cursor: pointer; display: none;" />
</div>
<div id="FeaturedArticles">
    <asp:Repeater runat="server" ID="rptFeaturedArticle">
        <ItemTemplate>
            <div class="box blogteaser">
                <div class="cont">                    
                    <h2>
                        <asp:HyperLink ID="lnkTitle" runat="server" NavigateUrl='<%#string.Format("/mobile/daily.aspx?Id={0}", Eval("ArticleId"))%>'
                                       Text='<%#Eval("Title")%>'>
                        </asp:HyperLink>
                    </h2>
                    <%--<asp:HyperLink runat="server" NavigateUrl="~/dailyarticles.xml" CssClass="rss" title="Subscribe to Mises Institute Daily Articles">
					rss</asp:HyperLink>--%>
                    <p class="author">
                        Mises Daily:
                        <asp:Literal runat="server" ID="litArticleDate" Text='<%#Convert.ToDateTime(Eval("DatePosted")).ToLongDateString()%>'>
                        </asp:Literal>
                        by
                        <asp:HyperLink ID="lnkAuthor" runat="server" Text='<%#Eval("Author")%>' NavigateUrl='<%#string.Format("/mobile/articles.aspx?AuthorId={0}", Eval("AuthorId"))%>'>'&gt;</asp:HyperLink>
                        <asp:Literal runat="server" ID="litAnd" Visible='<%#Convert.ToInt32(Eval("CoAuthorId")) > 0%>'> and </asp:Literal>
                        <asp:HyperLink ID="lnkCoAuthor" runat="server" Visible='<%#Convert.ToInt32(Eval("CoAuthorId")) > 0%>'
                                       Text='<%#Eval("CoAuthor")%>' NavigateUrl='<%#string.Format("/mobile/articles.aspx?AuthorId={0}", Eval("CoAuthorId"))%>'>' &gt;</asp:HyperLink>
                    </p>
                    <div class="bord-se">
                        <div class="bord-ne">
                            <div class="bord-s">
                                <a href='<%#string.Format("/mobile/daily.aspx?Id={0}", Eval("ArticleId"))%>' runat="server"
                                   id="lnkImage" visible='<%#Eval("PhotoURL").ToString() != ""%>'>
                                    <img runat="server" id="imgStory" visible='<%#Eval("PhotoURL").ToString() != ""%>'
                                         style='margin: 5px 5px;' alt="Thumbnail" height='75'
                                         width="100" src='<%#string.Format("http://s3.amazonaws.com/veksler-backup/DailyArticleImages/{0}.jpg", Eval("ArticleId"))%>' />
                                </a>
                            </div>
                        </div>
                    </div>
                    <p>
                        <asp:Literal ID="litMainStory" runat="server" Text='<%#Eval("Description")%>'></asp:Literal>
                        <asp:HyperLink ID="lnkMore" runat="server" CssClass="more" NavigateUrl='<%#string.Format("/mobile/daily.aspx?Id={0}", Eval("ArticleId"))%>'>read more&hellip;</asp:HyperLink>
                    </p>
                    <!-- / .cont -->
                </div>
                <!-- / .box -->
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<textarea id="dailyTemplate" style="display: none"> 
  
    <div class="cont"><h2>Mises Daily's for {$T.DatePosted}:</h2><br /></div>
  
    {#foreach $T as article}

    <div class="box blogteaser"> 
        <div class="cont"> 
            <h2> 
                <a href="/mobile/daily.aspx?Id={$T.article.ArticleId}">{$T.article.Title}</a> 
            </h2>                 
            <p class="author"> 
                Mises Daily:
                {$T.article.DatePosted}
                by
                <a href="/mobile/articles.aspx?AuthorId={$T.article.AuthorId}">{$T.article.AuthorName}</a>
            </p> 
            <div class="bord-se"> 
                <div class="bord-ne"> 
                    <div class="bord-s"> 
                        <a href="/mobile/daily.aspx?Id={$T.article.ArticleId}"> 
                            <img src="//s3.amazonaws.com/veksler-backup/DailyArticleImages/{$T.article.ArticleId}.jpg" style="margin: 5px 5px;" alt="Thumbnail" width="100" height="75" /> 
                        </a> 
                    </div> 
                </div> 
            </div> 
            <p> 
                {$T.article.Description}
                <a class="more" href="/mobile/daily.aspx?Id={$T.article.ArticleId}">read more&hellip;</a> 
            </p> 
            <!-- / .cont --> 
        </div> 
        <!-- / .box --> 
    </div>         
        
    {#/for}        
</textarea>