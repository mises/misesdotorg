#region

using System;
using System.Web.UI;
using Mises.Data;

#endregion

namespace Mobile.Controls
{
    partial class Controls_Theme_FeaturedArticle : UserControl
    {
        public Controls_Theme_FeaturedArticle()
        {
            Load += Page_Load;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            rptFeaturedArticle.DataSource =
                DataHelper.MisesDataContext.DailyArticlesGetTodaysSummary();
            rptFeaturedArticle.DataBind();
        }
    }
}