<%@ Control Language="C#" AutoEventWireup="false"
            Inherits="Controls_Theme_DailyArticleGallery" Codebehind="DailyArticleGallery.ascx.cs" %>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
                   SelectCommand="DailyArticlesGetImageGallery" SelectCommandType="StoredProcedure">    
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="AuthorId" QueryStringField="AuthorId"
                                  Type="Int32" />
        <%--<asp:QueryStringParameter Name="SearchQuery" QueryStringField="q" Type="String" DefaultValue="" ConvertEmptyStringToNull=true />--%>
    </SelectParameters>
</asp:SqlDataSource>
<asp:ListView ID="DailyArticleGallery" runat="server" GroupItemCount="3" DataSourceID="SqlDataSource1" DataKeyNames="Month">
    <ItemTemplate>
        <td>
            <div class="Article">
                <a href="/mobile/daily.aspx?Id=<%#Eval("ArticleId")%>" title="&quot;<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>&quot; by <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>">
                    <img width="120" src="<%#Server.HtmlEncode(Convert.ToString(Eval("ImageURL")))%>"
                         alt="" title="&quot;<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>&quot; by <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>" />
                </a>
                <a class="ArticleTitle" href="/mobile/daily.aspx?Id=<%#Eval("ArticleId")%>" title="&quot;<%#Server.HtmlEncode(Convert.ToString(Eval("Title")))%>&quot; by <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>">
                    <%#Eval("Title")%></a>
                <br />
                <span class="ArticleDate"><%#DateTime.Parse(Eval("DatePosted").ToString()).ToString("MMM dd yyyy")%></span> by
                <span class="ArticleAuthor"><a href="articles.aspx?AuthorId=<%#Eval("AuthorId")%>" title="Mises Daily Archive for <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>">
                                                <%#Server.HtmlEncode(Convert.ToString(Eval("Author")))%>
                                            </a>
                </span>
            </div>
        </td>
    </ItemTemplate>
    <GroupTemplate>
        <div id="itemPlaceholderContainer" runat="server" style="">
            <span id="itemPlaceholder" runat="server" />
        </div>
    </GroupTemplate>
    <GroupSeparatorTemplate>
    </tr>
    </GroupSeparatorTemplate>
    <EmptyDataTemplate>
        <span>No data was returned.</span>
    </EmptyDataTemplate>
    <LayoutTemplate>
        <div style="" class="Pager">
            <asp:DataPager ID="DataPager2" runat="server" PageSize="100">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                    <asp:NumericPagerField ButtonCount="10" />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
        </div>
        <table id="DailyArticleGallery" cellpadding="0" cellspacing="0">
            <div id="groupPlaceholderContainer" runat="server" style="">
                <span id="groupPlaceholder" runat="server" />
            </div>
        </table>
        <div style="" class="Pager">
            <asp:DataPager ID="DataPager3" runat="server" PageSize="100">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                    <asp:NumericPagerField ButtonCount="10" />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                                ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
        </div>
    </LayoutTemplate>
</asp:ListView>