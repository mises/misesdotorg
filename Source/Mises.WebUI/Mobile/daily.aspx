<%@ Page Language="C#" MasterPageFile="~/MasterPages/Mobile.master" MaintainScrollPositionOnPostback="true"
         EnableViewState="false" Title="Mises Institute Daily Articles"
         Inherits="MisesWeb.Mobile.DailyArticlePage" Codebehind="daily.aspx.cs" %>

<%@ Register Src="~/Controls/Navigation/Bookmark.ascx" TagName="Bookmark" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/Tagging/TagCloud.ascx" TagName="TagCloud" TagPrefix="uc2" %>
<%--<%@ Register src="Controls/Theme/Comments.ascx" tagname="Comments" tagprefix="uc1" %>--%>

<%--<%@ OutputCache Duration="180" VaryByParam="*" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">    
    <h1>
        <asp:Literal runat="server" ID="litHeaderText"></asp:Literal></h1>
    <p class="meta">
        <strong>Mises Daily:</strong>
        <asp:Literal ID="lblDatePosted" runat="server"></asp:Literal>
        by
        <asp:HyperLink ID="lnkAuthor" runat="server"></asp:HyperLink>
        <asp:Literal ID="litAnd" runat="server" Visible="false"> and </asp:Literal>
        <asp:HyperLink ID="lnkCoAuthor" runat="server" Visible="false"></asp:HyperLink>
    </p>
    <div id="DailyArticle">
        <asp:Literal ID="litStoryText" runat="server" EnableViewState="false"></asp:Literal>
    </div>
    <%--<uc1:Comments ID="Comments1" runat="server" />--%>
    <div class="subscribe">
        You can receive the Mises Dailies in your inbox.
        <asp:HyperLink runat="server" NavigateUrl="~/content/elist.asp">
            Go here to subscribe or unsubscribe</asp:HyperLink>.<br />
        <uc2:TagCloud ID="TagCloud" runat="server" />
    </div>
    <div class="rel">
        <ul class="nav">
            <li class="first">
                <asp:HyperLink ID="lnkPreviousPage2" runat="server">&laquo; Previous</asp:HyperLink></li>
            <li><a href="/articles.aspx" rel="directory">Mises Daily Index</a></li>
            <li>
                <asp:HyperLink ID="lnkNextPage2" runat="server">Next &raquo;</asp:HyperLink></li>
        </ul>
        <ul class="share">
            <li class="s-prin"><a href="#" onclick=" window.print();return false; " title="Print">
                                   Print</a></li>
            <li class="s-shar"><a href="#" onclick=" window.open('/MyMises/EmailPage.aspx?url=' + escape(self.location) + '&amp;title=' + escape(document.title),'mywin',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;" title="Share">Share</a></li>
            <li class="s-mymi"><a href="#" onclick=" window.open('/MyMises/EditFavorite.aspx?url=' + escape(self.location) + '&amp;title=' + escape(document.title),'mywin',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');return false;" title="Add to MyMises">
                                   Add to MyMises</a></li>
            <li class="s-subs">
                <asp:HyperLink runat="server" NavigateUrl="~/content/elist.asp" title="Subscribe">Subscribe</asp:HyperLink></li>
        </ul>
        <!-- /rel -->
    </div>
    <uc3:Bookmark ID="Bookmark" runat="server" />
</asp:Content>