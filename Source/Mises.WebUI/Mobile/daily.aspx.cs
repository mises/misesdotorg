#region

using System;
using System.Web.UI.HtmlControls;
using Mises.Data;
using Mises.Domain.Utility;
using DailyArticle = Mises.Domain.Articles.DailyArticle;

#endregion

namespace MisesWeb.Mobile
{
    partial class DailyArticlePage : MyPage
    {
        private int _articleId;

        public DailyArticlePage()
        {
            Load += Page_Load;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            //        if (Page.IsPostBack) return;

            if (!(Int32.TryParse(Request.QueryString["control"], out _articleId)) &&
                !(Int32.TryParse(Request.QueryString["Id"], out _articleId)) &&
                !(Int32.TryParse(Request.QueryString["record"], out _articleId)) &&
                !(GetIdFromTitle()))
            {
                Response.Redirect("articles.aspx");
            }

            if (Request.Browser.Crawler)
            {
                Response.Redirect("/daily/" + _articleId);
            }

            DailyArticle article;
            try
            {
                article = new DailyArticle(_articleId);
            }
            catch (ArgumentOutOfRangeException)
            {
                litStoryText.Text = "Article not found. Did you enter the Id # correctly?";
                return;
            }

            if (article.ShowArticle == false && !(Request.Url.ToString().Contains("preview")) && !(IsAdmin()))
            {
                litStoryText.Text = "You must sign in to view this article.";
                return;
            }
            Master.Page.Title = string.Format("{0} - {1} - Mises Daily", DataFormat.StripHTML(article.Title),
                                              article.AuthorName);

            //ViewState("DocumentId") = article.Identifier

            litHeaderText.Text = article.Title;
            litStoryText.Text = article.ReplaceText();

            lnkAuthor.Text = article.AuthorName;
            lnkAuthor.NavigateUrl = "/articles.aspx?AuthorId=" + article.AuthorId;
            lnkAuthor.Attributes.Add("rel", "author");

            if (!String.IsNullOrEmpty(article.CoAuthorName))
            {
                litAnd.Visible = true;
                lnkCoAuthor.Visible = true;
                lnkCoAuthor.Text = article.CoAuthorName;
                lnkCoAuthor.NavigateUrl = "/articles.aspx?AuthorId=" + article.CoAuthorId;
                lnkCoAuthor.Attributes.Add("rel", "coauthor");
            }

            lblDatePosted.Text = article.DatePosted.Date.ToLongDateString();

            //Comments1.DocumentId = article.Identifier.ToString();

            if (TagCloud != null)
            {
                TagCloud.DocumentIdentifier = article.Identifier;
            }
            Bookmark.Bind(article.URL, article.Title, article.Description);

            var hm = new HtmlMeta
                         {
                             Name = "Description",
                             Content = DataFormat.StripHTML(article.Description)
                         };
            Page.Header.Controls.Add(hm);

            hm = new HtmlMeta
                     {
                         Name = "author",
                         Content = article.AuthorName
                     };
            Page.Header.Controls.Add(hm);

            var link = new HtmlLink();
            string baseURL = Request.Url.Scheme + "://" + Request.Url.Authority;
            link.Attributes.Add("type", "application/rss+xml");
            link.Attributes.Add("rel", "alternate");
            link.Attributes.Add("title", "Mises Daily feed for " + article.AuthorName);
            link.Attributes.Add("href",
                                string.Format(baseURL + "/Feeds/articles.ashx?AuthorId={0}", article.AuthorId));
            Page.Header.Controls.AddAt(1, link);


            if (article.NextURL != "")
            {
                lnkNextPage2.NavigateUrl = article.NextURL;
                lnkNextPage2.Attributes.Add("rel", "next");
            }
            else
            {
                lnkNextPage2.Visible = false;
            }
            if (article.PreviousURL != "")
            {
                lnkPreviousPage2.NavigateUrl = article.PreviousURL;
                lnkPreviousPage2.Attributes.Add("rel", "previous");
            }
            else
            {
                lnkPreviousPage2.Visible = false;
            }
        }


        public bool IsAdmin()
        {
            const string cookieName = "ABLECOMMERCE1";
            return ((Request.Cookies[cookieName] != null &&
                     Request.Cookies[cookieName]["IsAdmin"] != null &&
                     Request.Cookies[cookieName]["IsAdmin"] == "1"
                    ))
                   ||
                   ((Request.Cookies["IsAdminLoggedIn"] != null &&
                     Request.Cookies["IsAdminLoggedIn"].Value == "1"
                    ));
        }

        private bool GetIdFromTitle()
        {
            if (Request.QueryString["title"] != null)
            {
                _articleId = DailyArticle.SearchByTitle(Request.QueryString["title"]);
                if (_articleId > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}