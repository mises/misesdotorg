﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Mobile.master" AutoEventWireup="false"
         EnableViewState="false" Inherits="Mobile_Default" Title="Ludwig von Mises Institute - Homepage" Codebehind="default.aspx.cs" %>

<%@ Register Src="~/Mobile/Controls/FeaturedArticle.ascx" TagName="FeaturedArticle"
             TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Theme/LiteratureList.ascx" TagName="LiteratureList" TagPrefix="uc4" %>

<%@ OutputCache Duration="90" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <uc1:FeaturedArticle ID="FeaturedArticle1" runat="server" />
    <div class="twocol">
        <div class="col1">
            <uc4:LiteratureList ID="LiteratureList1" runat="server" />
            <!-- / .col1 -->
        </div>
        <div class="col2">
            <!-- / .col2 -->
        </div>
        <!-- / .twocol -->
    </div>
</asp:Content>