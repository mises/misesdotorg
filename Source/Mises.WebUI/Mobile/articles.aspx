<%@ Page Language="C#" MasterPageFile="~/MasterPages/Mobile.master" Title="Mises Daily Archives - Mises Institute" Inherits="MisesWeb.Mobile.DailyArticlesArchive" AutoEventWireup="true" Codebehind="~/articles.aspx.cs" %>

<%@ Register Src="~/Controls/PageContentControl.ascx" TagName="PageContentControl"
             TagPrefix="uc1" %>
<%@ Register Src="./Controls/DailyArticleGallery.ascx" TagName="DailyArticleGallery"
             TagPrefix="uc2" %>
<%@ Register Assembly="Mises.WebControls" Namespace="Mises.WebControls" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/MisesDaily/DailyArticleList.ascx" TagName="DailyArticleList"
             TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        Mises Daily</h1>
    <uc1:PageContentControl ID="PageContentControl1" runat="server" ContentName="ArticlesHeader" />
    <h2>
        <asp:Image runat="server" ID="imgAuthorHeader" Visible="false" />
        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
    </h2>
    <div class="box">
        <uc3:DailyArticleList ID="DailyArticleList1" runat="server" Visible="false" />
        <uc2:DailyArticleGallery ID="DailyArticleGallery1" runat="server" />
        <cc1:GridView ID="gvAuthors" runat="server" AutoGenerateColumns="False" Width="650px"
                      CssClass="tlibrary tliteraturel">
            <Columns>
                <asp:TemplateField HeaderText="Name" SortExpression="AuthorName">
                    <ItemTemplate>
                        <img src="/images/icons/icon_category.gif" alt="Folder" />
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("AuthorId", "articles.aspx?AuthorId={0}")%>'
                                       Text='<%#Server.HtmlEncode(Convert.ToString(Eval("AuthorName")))%>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>              
            </Columns>
        </cc1:GridView>
    </div>
</asp:Content>