<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false" Inherits="search" Title="Mises.org Search" EnableViewState="false" Codebehind="search.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        Mises.org Site Search
    </h2>
    <h5>
        (Advanced search coming soon!)
    </h5>
    <br />
    <h4>
        <asp:TextBox ID="txtSearchField" runat="server"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search Mises.org" />
    </h4>
    <br />
    <p>
        <asp:PlaceHolder ID="plcContents" runat="server"></asp:PlaceHolder>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </p>
    <asp:GridView ID="emptyGrid" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <p>
        <!-- TODO: FIX to relative URL -->
        <a href="#" onclick=" window.external.AddSearchProvider('http://mises.freecapitalists.org//opensearch.xml'); ">
            Add Mises.org Search to your browser</a>
    </p>
</asp:Content>