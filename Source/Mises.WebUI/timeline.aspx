<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/MasterPages/Content.master"
         Inherits="timeline" Title="Mises Timeline" Codebehind="timeline.aspx.cs" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="body">
        <h1>
            Mises Timeline</h1>
        <div id="tl" class="timeline-default" style="height: 600px;">
        </div>
        <div style="width: 100%">
            <table style="text-align: center; width: 100%">
                <tr>
                    <td>
                        <a href="javascript:centerTimeline(1);">1 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(250);">250 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(500);">500 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(750);">750 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1000);">1000 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1250);">1250 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1500);">1500 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1750);">1750 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1750);">1800 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1750);">1850 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1750);">1900 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(1750);">1950 AD</a></td>
                    <td>
                        <a href="javascript:centerTimeline(2000);">2000 AD</a></td>
                </tr>
            </table>
        </div>
        <div class="controls" id="controls">
        </div>
    </div>
</asp:Content>