<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" EnableViewState="false" Inherits="misesreview_detail" Codebehind="~/misesreview_detail.aspx.cs" %>


<%@ Register Src="Controls/Navigation/Bookmark.ascx" TagName="Bookmark" TagPrefix="uc1" %>
<%@ Register Src="Controls/Tagging/TagCloud.ascx" TagName="TagCloud" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" EnableViewState="false"
             runat="Server">
    <div class="hreview">
        <a href="/periodical.aspx?Id=2" rel="directory">
            <img style="border-style: solid; border-width: 0px; float: left" alt="The Mises Review" src="/images/themisesreview.jpg" 
                /></a><br />
        <p>
            Edited and written by <asp:HyperLink runat="server" CssClass="reviewer" NavigateUrl="~/fellow.aspx?Id=5">David
                                      Gordon</asp:HyperLink>, senior fellow of the Mises Institute and author of <asp:HyperLink runat="server" NavigateUrl="/store/Search.aspx?m=6">
                                                                                                                     four books</asp:HyperLink> and thousands of essays.</p>
        <br />
        <h1 class="item">
            <asp:Literal ID="lblTitle" runat="server" Text="Literal"></asp:Literal></h1>
        <h2 class="reviewer">
            <asp:Literal ID="lblAuthor" runat="server" Text="Literal"></asp:Literal></h2>
        <h3>
            <asp:Literal ID="lblSeason" runat="server" Text="Literal"></asp:Literal><br />
            <asp:Literal ID="lblIssue" runat="server" Text="Literal"></asp:Literal>
        </h3>
        <br />
        <div class="description">
            <asp:Literal ID="lblBody" runat="server"></asp:Literal>
        </div>
        <abbr class="dtreviewed" style="display: none;">
            <asp:Literal ID="litDateReviewed" runat="server" Text=""></asp:Literal></abbr>
        <span class="type" style="display: none;">product</span> <span class="version" style="display: none;">
                                                                     0.3</span>
    </div>
    <a href="#" onclick=" history.go(-1); ">Back</a>
    <uc2:TagCloud ID="TagCloud" runat="server" />
    <uc1:Bookmark ID="Bookmark1" runat="server" />
</asp:Content>