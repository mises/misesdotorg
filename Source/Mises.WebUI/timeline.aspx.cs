#region

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

#endregion

partial class timeline : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlLink cssLink = new HtmlLink();
        cssLink.Href = "http://simile.mit.edu/timeline/api/bundle.css";
        cssLink.Attributes.Add("rel", "stylesheet");
        cssLink.Attributes.Add("type", "text/css");
        Header.Controls.Add(cssLink);

        Page.Form.Attributes.Add("onLoad", "onLoad();");
        Page.Form.Attributes.Add("onresize", "onResize();");
        //'onload="onLoad();" onresize="onResize();"

        //Page.RegisterStartupScript("onLoad", "onLoad();")
        //Page.RegisterStartupScript("onresize", "onResize();")

        //Dim script As New HtmlLink
        //script.Href = "http://simile.mit.edu/timeline/api/timeline-api.js"
        //script.Attributes.Add("type", "text/javascript")
        //Header.Controls.Add(script)

        //script = New HtmlLink
        //script.Href = "http://simile.mit.edu/timeline/examples/examples.js"
        //script.Attributes.Add("type", "text/javascript")
        //Header.Controls.Add(script)

        Literal lit = new Literal();
        lit.Text =
            "<script src=\"http://simile.mit.edu/timeline/api/timeline-api.js\" type=\"text/javascript\"></script>";
        lit.Text +=
            "<script src=\"http://simile.mit.edu/timeline/examples/examples.js\" type=\"text/javascript\"></script>";
        lit.Text += "<script src=\"timeline.js\" type=\"text/javascript\"></script>";

        lit.Text += "</head><body onload=\"onLoad();\" onresize=\"onResize();\">";
        Header.Controls.Add(lit);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
    }
}