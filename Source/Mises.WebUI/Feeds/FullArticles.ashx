<%@ WebHandler Language="C#" Class="ArticleFeed" %>

#region

using System;
using System.Collections.Generic;
using Mises.Data;
using Mises.Domain.Articles;
using Mises.Domain.Feeds;
using RssToolkit.Rss;

#endregion

public class ArticleFeed : ArticlesHttpHandlerBase
{
    //TODO: FIX to relative URL
    protected override void PopulateRss(string channelName, string userName)
    {
        //Int32 AuthorId = 0;
        //Int32.TryParse(Context.Request.QueryString["AuthorId"], out AuthorId);

        string userAgent = Context.Request.UserAgent;
        if (Context.Request.Url.Host != "localhost" && !String.IsNullOrEmpty(userAgent) &&
            !userAgent.StartsWith("FeedBurner"))
        {
            Context.Response.Redirect("http://feeds.mises.org/MisesFullTextArticles");
        }

        ArticlesItem item;
        ArticlesGuid ident;
        Rss.Version = "2.0";
        Rss.Channel = new ArticlesChannel();
        Rss.Channel.Items = new List<ArticlesItem>();


        Rss.Channel.Title = "Mises Institute Daily Articles";
        Rss.Channel.Description = "Daily Articles from The Mises Institute on Austrian Economics and Libertarianism";
        Rss.Channel.Link = "http://mises.org/articles.aspx";
        Rss.Channel.PubDate = DateTime.Now.ToString("r");

        var img = new ArticlesImage();
        img.Link = "http://mises.org/articles.aspx";
        img.Url = "http://mises.org/images3/DailyArticles.gif";
        img.Title = "Daily Articles";
        Rss.Channel.Image = img;
        Rss.Channel.Ttl = "10";
        Rss.Channel.Copyright = Configuration.Copyright;

        foreach (DailyArticlesGetFullTextFeedResult article in DailyArticle.GetRSSFullTextFeed())
        {
            item = new ArticlesItem();
            item.Title = DataFormat.StripHTML(article.Title);
            item.Description = article.description;
            item.Link = String.Format("http://mises.org/daily/{0}", article.ArticleId);
            ident = new ArticlesGuid();
            ident.IsPermaLink = "false";
            ident.Text = article.ArticleText;
            item.Guid = ident;
            item.Author = article.AuthorName;
            item.PubDate = RssXmlHelper.ToRfc822(article.dateposted);

            Rss.Channel.Items.Add(item);
        }

        //if (AuthorId > 0 && Rss.Channel.Items.Count > 0)
        //{
        //    Rss.Channel.Title = "Daily Article archives for " + Rss.Channel.Items[0].Author;
        //}
    }
}