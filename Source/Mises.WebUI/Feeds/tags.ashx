<%@ WebHandler Language="C#" Class="ArticleFeed" %>

#region

using System;
using System.Collections.Generic;
using System.Web;
using Mises.Data;
using Mises.Domain.Feeds;
using Mises.Domain.Tags;
using d = Mises.Domain.Documents;

#endregion

public class ArticleFeed : ArticlesHttpHandlerBase
{
    //TODO: FIX to relative URL
    protected override void PopulateRss(string channelName, string userName)
    {
        Context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(90));
        Context.Response.Cache.SetCacheability(HttpCacheability.Public);
        
        string tag = "";
        tag = Context.Request.QueryString["tag"];

        ArticlesItem item;
        ArticlesGuid ident;
        Rss.Version = "2.0";
        Rss.Channel = new ArticlesChannel();
        Rss.Channel.Items = new List<ArticlesItem>();


        Rss.Channel.Title = "Mises Institute Tagged Documents";
        Rss.Channel.Description = "Tagged Documents from The Mises Institute on Austrian Economics and Libertarianism";
        Rss.Channel.Link = "http://mises.freecapitalists.org//tags.ashx";
        Rss.Channel.PubDate = DateTime.Now.ToString("r");

        var img = new ArticlesImage();
        img.Link = "http://mises.freecapitalists.org//clouds.aspx";
        img.Url = "http://mises.freecapitalists.org//Theme/images/ico/tags.png";
        img.Title = "Tagged Documents";
        Rss.Channel.Image = img;
        Rss.Channel.Ttl = "10";
        Rss.Channel.Copyright = Configuration.Copyright;

        foreach (var doc in Tagging.GetDocumentsWithTag(tag))
        {
            item = new ArticlesItem();
            item.Title = doc.Title;
            item.Description = doc.DocumentType.ToString();
            item.Link = DataFormat.GetAbsoluteURL(doc.URL);
            ident = new ArticlesGuid();
            ident.IsPermaLink = "false";
            ident.Text = doc.Identifier.ToString();
            item.Guid = ident;
            //item.Tag = tag;
            //item.PubDate = RssXmlHelper.ToRfc822(DateTime.Parse(row["datePosted"].ToString()));

            Rss.Channel.Items.Add(item);
        }


        Rss.Channel.Title = "Documents tagged \"" + tag + "\"";
    }
}