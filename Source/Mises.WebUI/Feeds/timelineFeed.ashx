<%@ WebHandler Language="C#" Class="timelineFeed" %>

#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Mises.Data;
using Mises.Domain.Timeline;

#endregion

internal class Author
{
    public string AuthorLast;
    public string authorName;
    public string born;
    public string desc;
    public string died;
}


public class timelineFeed : IHttpHandler
{
    #region IHttpHandler Members

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/xml";

        var authors = new List<Author>();

        using (
            SafeDataReader reader = SqlHelper.ExecuteReader(BusinessBase.ConnectionString, CommandType.Text,
                                                            "SELECT [AuthorFirst] ,[Born],[Died],[BioText],[BioGUID] FROM [Biography] WHERE Born <> '' ORDER BY [Born] DESC")
            )
        {
//        Using reader As SqlDataReader = SqlHelper.ExecuteReader(BusinessBase.ConnectionString, CommandType.Text, "SELECT [AuthorFirst] + ' ' + AuthorMiddle + + ' ' + AuthorLast As AuthorFirst,[Born],[Died],[BioText],[BioGUID] FROM [DocumentAuthors] WHERE Born <> '' ORDER BY [Born] DESC")
            while (reader.Read())
            {
                var author = new Author();
                author.authorName = Convert.ToString(reader["AuthorFirst"]);
                author.born = Convert.ToString(reader["Born"]);
                author.died = Convert.ToString(reader["Died"]);
                author.desc = Convert.ToString(reader["BioText"]);
                authors.Add(author);
            }
        }


        var TimeLineList = new List<TimeLineEvent>();
        foreach (Author author in authors)
        {
            var tle = new TimeLineEvent();
            tle.Text = author.desc;
            tle.Born = author.born;
            tle.Died = author.died;
            //DateTime.TryParse(author.desc, tle.End))
            tle.Title = author.authorName;
            TimeLineList.Add(tle);
        }

        context.Response.Write(TimeLineEvent.GetXML(TimeLineList));
    }

    public bool IsReusable
    {
        get { return false; }
    }

    #endregion
}