<%@ WebHandler Language="C#" Class="ArticleFeed" %>

#region

using System;
using System.Collections.Generic;
using Mises.Data;
using Mises.Domain.Feeds;
using RssToolkit.Rss;
using System.Web;
using DailyArticle = Mises.Domain.Articles.DailyArticle;

#endregion

public class ArticleFeed : ArticlesHttpHandlerBase
{
    //TODO: FIX to relative URLs 
    protected override void PopulateRss(string channelName, string userName)
    {
        Context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(120));
        Context.Response.Cache.SetCacheability(HttpCacheability.Public);
        
        Int32 AuthorId = 0;
        Int32.TryParse(Context.Request.QueryString["AuthorId"], out AuthorId);

        string userAgent = Context.Request.UserAgent;
        if (AuthorId == 0 && !String.IsNullOrEmpty(userAgent) && !userAgent.StartsWith("FeedBurner") &&
            Context.Request.UserHostName != "127.0.0.1")
        {
            Context.Response.Redirect("http://feeds.mises.org/MisesDailyArticles");
        }

        ArticlesItem item;
        ArticlesGuid ident;
        Rss.Version = "2.0";
        Rss.Channel = new ArticlesChannel();
        Rss.Channel.Items = new List<ArticlesItem>();


        Rss.Channel.Title = "Mises Institute Daily Articles";
        Rss.Channel.Description = "Daily Articles from The Mises Institute on Austrian Economics and Libertarianism";
        Rss.Channel.Link = "http://mises.freecapitalists.org//daily/";
        Rss.Channel.PubDate = DateTime.Now.ToString("r");

        var img = new ArticlesImage();
        img.Link = "http://mises.freecapitalists.org//daily/";
        img.Url = "http://mises.freecapitalists.org//images/DailyArticles.gif";
        img.Title = "Daily Articles";
        Rss.Channel.Image = img;
        Rss.Channel.Ttl = "10";
        Rss.Channel.Copyright = Configuration.Copyright;

        //var articlesRs = DailyArticle.GetRSSFullTextFeed();

        //List<DailyArticle> articles = new List<DailyArticle>();

        //articlesRs.ToList().ForEach(article => articles.Add(
        //    new DailyArticle().ArticleFromLinqResult(article)
        //                                           ));

        foreach (DailyArticlesGetFeedResult article in DailyArticle.GetRSSFeed(AuthorId))
        {
            item = new ArticlesItem();
            item.Title = DataFormat.StripHTML(article.Title);
            item.Description = article.description;
            item.Link = String.Format("http://mises.freecapitalists.org//daily/{0}/{1}", article.ArticleId, article.Title.ToSlug());
            ident = new ArticlesGuid();
            ident.IsPermaLink = "false";
            //ident.Text = article.description;
            item.Guid = ident;
            item.Author = article.AuthorName;
            item.PubDate = RssXmlHelper.ToRfc822(article.dateposted);

            Rss.Channel.Items.Add(item);
        }

        if (AuthorId > 0 && Rss.Channel.Items.Count > 0)
        {
            Rss.Channel.Title = "Daily Article archives for " + Rss.Channel.Items[0].Author;
        }
    }
}