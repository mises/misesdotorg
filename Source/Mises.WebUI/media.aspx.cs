﻿#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Media;


#endregion

public partial class MediaPage : Page
{
    private string CurrentCategory;

    #region Properies

    private string SearchField
    {
        get { return ViewState["search"] != null ? Convert.ToString(ViewState["search"]) : ""; }
        set { ViewState["search"] = value; }
    }

    #endregion

    private void BindMedia()
    {
        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));

        MediaCatalog media = new MediaCatalog();
        string action = Request.QueryString["action"];
        int ActionId = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));

        if (!string.IsNullOrEmpty(action))
        {
            FeaturedMedia1.Visible = false;
        }


        // No Longer Used - remove after 1/1/2009
        if (action == "CreateDate" || action == "LastName" || action == "Title")
        {
            gvMediaItems.SortExpressionAlt = action;
        }

        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority;
        switch (action)
        {
            case "category":
                {
                    //lblAction.Text = "Browse by Category";

                    media.GetCategoryContents(ActionId);

                    if (ActionId == 210)
                    {
                        lblAction.Text = "Recent Uploads";
                        var link = new HtmlLink();
                        link.Attributes.Add("type", "application/rss+xml");
                        link.Attributes.Add("rel", "alternate");
                        link.Attributes.Add("title", "Recent Media Uploads");
                        link.Attributes.Add("href", "http://feeds.mises.org/MisesMedia");
                        Page.Header.Controls.AddAt(1, link);
                    }


                    if (media.dtCategoryInfo.Rows.Count > 0)
                    {
                        //lblAction.Text = "Browsing " + media.dtCategoryList.Rows[0].Field<string>("Category"]);
                        CurrentCategory = media.dtCategoryInfo.Rows[0].Field<string>("Category");

                        if (media.dtCategoryInfo.Rows[0].Field<int?>("ParentCategoryID") != 0)
                        {
                            lblAction.Text = string.Format("<a href=\"/media/category/{0}/{1}\">{2}</a> > ", media.dtCategoryInfo.Rows[0]["ParentCategoryId"], media.dtCategoryInfo.Rows[0]["ParentCategory"].ToString().ToSlug(), media.dtCategoryInfo.Rows[0]["ParentCategoryName"]);
                        }

                        lblAction.Text +=
                            string.Format(@"<a href=""/media/category/{0}/{1}"">{2}</a> ", media.dtCategoryInfo.Rows[0].Field<int?>("CategoryId"), media.dtCategoryInfo.Rows[0].Field<string>("Category").ToSlug(), media.dtCategoryInfo.Rows[0].Field<string>("Category"));

                        string feedURL = string.Format("{0}/Feed/Media/?CategoryId={1}", baseUrl,media.dtCategoryInfo.Rows[0].Field<int?>("CategoryId"));

                        var link = new HtmlLink();
                        link.Attributes.Add("type", "application/rss+xml");
                        link.Attributes.Add("rel", "alternate");
                        link.Attributes.Add("title", "Media feed for " + CurrentCategory);
                        link.Attributes.Add("href", feedURL);
                        Page.Header.Controls.AddAt(1, link);

                        PageContentControl1.Controls.Add(new LiteralControl(string.Format(
                            @"<p><strong><a class=""podcast"" href=""{0}"">Media Feed for {1}</a></strong></p><br />",
                            feedURL, CurrentCategory
                                                                                )));


                        litHeader.Text = media.dtCategoryInfo.Rows[0].Field<string>("Category");

                        link = new HtmlLink();
                        link.Attributes.Add("type", "application/rss+xml");
                        link.Attributes.Add("rel", "alternate");
                        link.Attributes.Add("title", "Media feed for " + lblAction.Text);
                        link.Attributes.Add("href",
                                            string.Format("{0}/Feed/Media/?CategoryId={1}", baseUrl, ActionId));
                        Page.Header.Controls.AddAt(1, link);
                    }
                    break;
                }
            case "subject":
                {
                    switch (ActionId)
                    {
                        case 0:
                            {
                                gvSubjects.DataSource = db.MediaTopSubjects();
                                gvSubjects.DataBind();
                                lblAction.Text = "Browse by subject";
                                return;
                            }
                        default:
                            media.GetSubjectMatches(ActionId);

                            var subjectsObj = from subjects in db.DocumentSubjectTBs
                                              where subjects.SubjectId == ActionId
                                              select subjects;

                            if (subjectsObj.Any())
                                lblAction.Text = subjectsObj.First().Subject;

                            break;
                    }

                    break;
                }
            case "showname":
            case "author": // Show Files
                {
                    litHeader.Text = "Mises Media Authors";
                    Page.Title = litHeader.Text;

                    if (ActionId == 0)
                    {
                        var mises = DataHelper.EntityDataModel;
                        var authors = mises.MediaGetAuthorsAlpha().ToList();
                        authors = authors.OrderBy(author => author.AuthorLast).ToList();

                        gvAuthors.DataSource = authors;
                        gvAuthors.RowDataBound += gvAuthors_RowDataBound;
                        gvAuthors.DataBind();

                        var letters = from ch in "ABCDEFGHIJKLMNOPQRSTUVWXYZ" select ch;
                        string lit = "";
                        letters.ToList().ForEach(l => { lit += string.Format(" <a href=\"#{0}\">{0}</a>", l); });
                        litAuthorLetters.Text = string.Format("{0}", lit);
                        alpha.Visible = true;

                        return;
                    }
                    media.GetContributorFiles(ActionId);
                    var bio = db.DocumentAuthorsGetDetail(null, ActionId).FirstOrDefault();
                    if (bio != null)
                    {
                        lblAction.Text = bio.Author + " Author Archives";
                        CurrentCategory = lblAction.Text;

                        string feedURL = string.Format("{0}/Feed/Media/?AuthorId={1}", baseUrl, ActionId);
                        var link = new HtmlLink();
                        link.Attributes.Add("type", "application/rss+xml");
                        link.Attributes.Add("rel", "alternate");
                        link.Attributes.Add("title", "Media Feed for " + bio.Author);
                        link.Attributes.Add("href", feedURL);
                        Page.Header.Controls.AddAt(1, link);

                        // Get Author Bio
                        pHeaderText.InnerHtml = bio.BioText;

                        if (!string.IsNullOrEmpty(bio.Photo))
                        {
                            imgAuthorHeader.Visible = true;
                            imgAuthorHeader.ImageUrl =
                                string.Format(Request.Url.Scheme + "://mises.freecapitalists.org/Controls/Theme/AuthorHeaderImage.ashx?url={0}&name={1}",
                                              bio.Photo,
                                              bio.Author);
                            imgAuthorHeader.AlternateText = bio.Author;
                        }
                        pHeaderText.InnerHtml += string.Format(
                            @"<p><strong><a class=""podcast"" href=""{0}"">Media Feed for {1}</a></strong></p><br />",
                            feedURL, bio.Author
                            );


                        // Author Archive Links:
                        divAuthorArchive.Visible = true;

                        lnkAuthorDailyArchiveLink.Text = string.Format("{0} Daily Article Archives", bio.Author);
                        lnkAuthorLiterature.Text = string.Format("{0} Literature Archives", bio.Author);

                        lnkAuthorDailyArchiveLink.NavigateUrl = String.Format("literature.aspx?AuthorId={0}", ActionId);
                        lnkAuthorLiterature.NavigateUrl = String.Format("literature.aspx?action=author&ID={0}", ActionId);
                    }


                    break;
                }
            case "search":
                {
                    if (string.IsNullOrEmpty(Request.QueryString["q"]))
                    {
                        return;
                    }
                    gvMediaItems.SortExpressionAlt = "";
                    SearchField = DataFormat.StripHTML(Request.QueryString["q"]).Trim();
                    lblAction.Text = "Media Search";
                    media.MediaSearch(SearchField);

                    if (media.results == 0)
                    {
                        var words = SearchField.Split(Convert.ToChar(" "));
                        if (words.Length > 0)
                            media.MediaSearch(words[words.Length - 1]);
                    }
                    lblAction.Text = media.results == 0
                                         ? "Sorry, no matches found."
                                         : string.Format("{0} matches for {1}", media.results, SearchField);
                    gvMediaItems.DataBind();
                    break;
                }
            case "MediaType":
                {
                    switch (ActionId)
                    {
                        case 0:
                            {
                                gvMediaType.DataSource = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                                                  CommandType.StoredProcedure,
                                                                                  "MediaTypeGetList");
                                gvMediaType.DataBind();
                                lblAction.Text = "Browse by Media Type";
                                return;
                            }
                        default:

                            gvMediaItems.DataSource = media.GetMediaByType(ActionId);
                            gvMediaItems.DataBind();


                            var mediaType =
                                db.DocumentMediaTypeTBs.Where(mediatype => mediatype.MediaTypeID == ActionId).Select(
                                    mediatype => mediatype.Description);
                            if (mediaType.Any())
                                lblAction.Text = "Browse by " + mediaType.First();
                            Title = lblAction.Text;

                            return;
                    }
                }
            default: // Other sort
                {
                    if (string.IsNullOrEmpty(action))
                    {
                        media.GetCategoryContents(0);
                    }
                    else
                    {
                        media.GetCategoryContents(-1);
                    }
                    break;
                }
        }

        if (action == "category" || string.IsNullOrEmpty(action))
        {
            gvCategories.DataSource = media.dtCategoryList;
            gvCategories.DataBind();
        }
        else
        {
            gvCategories.Visible = false;
        }

        if (gvMediaItems.SortExpressionAlt != "")
        {
            media.dtMediafile.DefaultView.Sort = gvMediaItems.SortExpressionAlt +
                                                 ((gvMediaItems.SortDirectionAlt == SortDirection.Descending)
                                                      ? " DESC"
                                                      : "");
        }
        else
        {
            media.dtMediafile.DefaultView.Sort = "EditDate DESC";
        }

        gvMediaItems.DataSource = media.dtMediafile.DefaultView;
        gvMediaItems.DataBind();

        if (!string.IsNullOrEmpty(lblAction.Text))
            Page.Title = CurrentCategory + " - " + Page.Title;
    }

    protected override void OnInit(EventArgs e)
    {
        Page.Init += delegate
                         {
                             if (ScriptManager.GetCurrent(Page) == null)
                             {
                                 ScriptManager sMgr = new ScriptManager();
                                 Page.Form.Controls.AddAt(0, sMgr);
                             }
                         };
        base.OnInit(e);
        Load += Page_Load;
    }

    #region Events

    private void Page_Load(object sender, EventArgs e)
    {
        gvMediaItems.PageIndexChanging += gvMediaItems_PageIndexChanging;
        gvMediaItems.Sorting += gvMediaItems_Sorting;
        gvCategories.RowDataBound += gvCategories_RowDataBound;
        if (Page.IsPostBack)
        {
            return;
        }
        BindMedia();
    }

    protected void gvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string catImageURL = Convert.ToString(((DataRowView)e.Row.DataItem).Row["CategoryImage"]);
            if (!string.IsNullOrEmpty(catImageURL))
            {
                var imgCategory = (Image) (e.Row.FindControl("imgCategory"));
                if (catImageURL.Contains("DocumentImage.ashx"))
                {
                    imgCategory.Height = 125;
                }
                imgCategory.ImageUrl = catImageURL;
                imgCategory.Visible = true;
                imgCategory.AlternateText = Convert.ToString(((DataRowView) e.Row.DataItem).Row["Category"]);
            }
        }
    }

    #region Handling Sorting and Paging

    protected void gvMediaItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMediaItems.PageIndex = e.NewPageIndex;
        BindMedia();
    }

    protected void gvMediaItems_Sorting(object sender, GridViewSortEventArgs e)
    {
        gvMediaItems.PageIndex = 0;
        BindMedia();
    }

    #endregion

    private char last;
    private void gvAuthors_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow) return;

        var author = ((MediaGetAuthorsAlpha_Result)e.Row.DataItem);
        string name = author.AuthorLast;

        if (!string.IsNullOrWhiteSpace(name) && last != name[0])
        {
            last = name[0];
            string row = string.Format("<h2><a href=\"#{0}\" id=\"{0}\">{0}</a></h2>", last);
            e.Row.Cells[0].Controls.AddAt(0, new LiteralControl { Text = row });
        }
    }

    #endregion
}