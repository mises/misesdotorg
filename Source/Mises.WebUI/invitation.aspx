<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" Title="Invite a friend!" Inherits="invitation" Codebehind="~/invitation.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <a href="/daily">
        <img style="border-style: solid; border-width: 0px" src="/images/DailyArticles.gif" alt="Daily Articles" 
             height="91" width="250" /></a>
    <br />
    <a href="/elist.asp">Subscribe</a>
    <br />
    <h2 style="text-align: center">
        <asp:Label ID="lblTitle" runat="server" Text="Invite a friend to join the Mises Daily article list:"></asp:Label></h2>
    <br />
    Message:<br />
    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Width="411px" Height="75px" Enabled="False">You will enjoy this Daily Article from the Mises Institute, an economic education in your inbox.  

        Sign up at http://mises.freecapitalists.org//content/elist.asp</asp:TextBox><br />
    <br />
    Your name: &nbsp; &nbsp;
    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="emailName" runat="server" ControlToValidate="txtName"
                                ErrorMessage="Please enter your name."></asp:RequiredFieldValidator><br />
    <br />
    Friend's emails:
    <br />
    <asp:TextBox ID="txtEmail1" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="emailValidator" runat="server" ControlToValidate="txtEmail1"
                                    ErrorMessage="Please enter your friend's email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><br />
    <asp:TextBox ID="txtEmail2" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail2"
                                    ErrorMessage="Please enter your friend's email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><br />
    <asp:TextBox ID="txtEmail3" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail3"
                                    ErrorMessage="Please enter your friend's email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><br />
    <asp:TextBox ID="txtEmail4" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail4"
                                    ErrorMessage="Please enter your friend's email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><br />
    <asp:TextBox ID="txtEmail5" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtEmail5"
                                    ErrorMessage="Please enter your friend's email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><br />
    <br />
    <asp:Button ID="Send" runat="server" OnClick="Send_Click" Text="Send Invitation"
                CssClass="ui-state-default ui-corner-all ui-button" />
</asp:Content>