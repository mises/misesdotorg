var email = new LiveValidation('email');
email.add(Validate.Presence);
email.add(Validate.Email);

var first_name = new LiveValidation('first_name');
first_name.add(Validate.Presence);
first_name.add(Validate.Length, { minimum: 2 });

var street_address = new LiveValidation('street_address');
street_address.add(Validate.Presence);

var zip = new LiveValidation('zip');
zip.add(Validate.Presence);

var last_name = new LiveValidation('last_name');
last_name.add(Validate.Presence);

var city = new LiveValidation('city');
city.add(Validate.Presence);
city.add(Validate.Length, { minimum: 2 });