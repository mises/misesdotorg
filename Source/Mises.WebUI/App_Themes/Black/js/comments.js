var username = new LiveValidation('name');
username.add(Validate.Presence, { failureMessage: "Supply a name!" });
username.add(Validate.Length, { minimum: 4, maximum: 20 });

var mail = new LiveValidation('mail');
mail.add(Validate.Presence, { failureMessage: "Supply an e-mail address!" });
mail.add(Validate.Email);