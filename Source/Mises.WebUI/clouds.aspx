<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" Title="Mises Tag Clouds" EnableViewState="false" %>

<%@ Register Src="Controls/Tagging/TaggingStats.ascx" TagName="TaggingStats" TagPrefix="uc1" %>
<%@ Register Namespace="VRK.Controls" TagPrefix="vrk" Assembly="VRK.Controls" %>
<%@ OutputCache Duration="360" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <asp:Image runat="server" ImageUrl="//mises.freecapitalists.org/images/Theme/images/ico/tags.png" alt="Tags" />
        Mises Tag Clouds</h1>
    <h2>User-Contributed Tags (<a href="#stats" title="Tag Statistics">Stats</a>)</h2>
    <asp:SqlDataSource ID="sqlUserTags" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        EnableCaching="True" SelectCommand="TagGetTopTags" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <vrk:Cloud ID="UserContributedCloud" runat="server" CssClass="tagcloud" DataHrefField="tag" DataHrefFormatString="/tag/{0}"
        DataSourceID="sqlUserTags" DataTextField="Tag" DataTitleField="Count" DataTitleFormatString="{0} documents"
        DataWeightField="Count" ItemCssClassPrefix="CommonTag">
    </vrk:Cloud>
    <%--<h2>
        <asp:HyperLink runat="server" NavigateUrl="~/Community/tags/default.aspx">Community Tag Clouds</asp:HyperLink></h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="~/Community/tags/default.aspx">View Community Tag Clouds</asp:HyperLink></p>
    <h2>--%>
    <h2>Literature Authors</h2>
    <asp:ObjectDataSource ID="ObjectDataSourceGuide" runat="server" SelectMethod="GetDocumentAuthors"
        TypeName="Mises.Domain.Tags.CloudSource" />
    <vrk:Cloud ID="Cloud1" runat="server" DataSourceID="ObjectDataSourceGuide" DataTextField="Name"
        DataTitleField="Weight" DataTitleFormatString="{0} documents" DataWeightField="Weight"
        DataHrefField="URL" CssClass="tagcloud" ItemCssClassPrefix="CommonTag">
    </vrk:Cloud>
    <h2>Literature Subjects</h2>
    <div id="Div1" style="width: 200px; text-align: center;">
    </div>
    <asp:ObjectDataSource ID="sourceGetDocumentSubjects" runat="server" SelectMethod="GetDocumentSubjects"
        TypeName="Mises.Domain.Tags.CloudSource"></asp:ObjectDataSource>
    <div class="tagcloud">
        <vrk:Cloud ID="Cloud3" runat="server" CssClass="tagcloud" DataHrefField="URL" DataSourceID="sourceGetDocumentSubjects"
            DataTextField="Name" DataTitleField="Weight" DataTitleFormatString="{0} documents"
            DataWeightField="Weight" ItemCssClassPrefix="CommonTag">
        </vrk:Cloud>
    </div>
    <h2>Media Authors</h2>
    <asp:ObjectDataSource ID="sourceMedia" runat="server" SelectMethod="GetMediaAuthors"
        TypeName="Mises.Domain.Tags.CloudSource" />
    <vrk:Cloud ID="c1" runat="server" DataSourceID="sourceMedia" DataTextField="Name"
        ItemCssClassPrefix="CommonTag" DataTitleField="Weight" DataTitleFormatString="{0} media items"
        DataWeightField="Weight" DataHrefField="URL" CssClass="tagcloud">
    </vrk:Cloud>
    <h2>Daily Article Authors</h2>
    <asp:ObjectDataSource ID="sourceDaily" runat="server" SelectMethod="GetDailyAuthors"
        TypeName="Mises.Domain.Tags.CloudSource" />
    <vrk:Cloud ID="Cloud2" runat="server" DataSourceID="sourceDaily" DataTextField="Name"
        ItemCssClassPrefix="CommonTag" DataTitleField="Weight" DataTitleFormatString="{0} media items"
        DataWeightField="Weight" DataHrefField="URL" CssClass="tagcloud">
    </vrk:Cloud>

    <h2>Top Search Terms</h2>
    <div id="queries"></div>
    <script src="//www.google.com/cse/query_renderer.js"> </script>
    <script src="//www.google.com/cse/api/008192686144554149800/cse/lssgwvjfvx4/queries/js?callback=(new+PopularQueryRenderer(document.getElementById(%22queries%22))).render"> </script>




    <uc1:TaggingStats ID="TaggingStats1" runat="server" />


    <div>
        <hr />

        <h2>Most Favorited Content</h2>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
            EnableCaching="True" SelectCommand="FavoritesGetMostPopular" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

        <asp:GridView ID="gvMostFavorited" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:TemplateField HeaderText="Link">
                    <ItemTemplate>
                        <a href="<%#Eval("url")%>">
                            <%#Eval("title")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="count" HeaderText="Count" />

            </Columns>
        </asp:GridView>
    </div>



</asp:Content>
