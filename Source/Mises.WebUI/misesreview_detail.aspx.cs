#region

using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using Microsoft.VisualBasic;
using Mises.Data;


#endregion

public partial class misesreview_detail : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int control = 0;
        try
        {
            control = Convert.ToInt32(Conversion.Val(Request.QueryString["control"]));
        }
        catch (Exception)
        {
            Response.Redirect("/");
        }

        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));

        var review = db.MisesReviewGetReview(control).SingleOrDefault();

        if (review == null)
            return;

        string AuthorName = review.authorFirst + " " + review.authorLast;
        if (!string.IsNullOrWhiteSpace(review.authorFirst2))
        {
            AuthorName += " and " + review.authorFirst2 + " " + review.authorlast2;
        }
        if (!string.IsNullOrWhiteSpace(review.authorlast3))
        {
            AuthorName += " and " + review.authorFirst3 + " " + review.authorlast3;
        }

        lblAuthor.Text = AuthorName;

        lblSeason.Text = string.Format("{0} {1}", review.issue_Season, review.issue_Year);
        lblIssue.Text = string.Format("Volume {0}, Number {1}", review.issue_Year - 1994, review.issue_Season);

        lblBody.Text = Strings.Replace(review.body, "<a href=", "<a class=\"fn url\" href=", 1, 1, CompareMethod.Binary);

        Page.Title = "The Mises Review: " + review.title + " by " + AuthorName;
        lblTitle.Text = review.title;

//        litDateReviewed.Text = review.Created.ToString();

        Bookmark1.Bind(Request.Url.ToString(), Page.Title, Page.Title);
        if (TagCloud != null)
        {
            TagCloud.DocumentIdentifier = review.GUID;
        }
    }
}