#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Mises.Data;
using Mises.Domain.Services;

#endregion

partial class studyguide : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (! Page.IsPostBack)
        {
            Bind();
            lblRandomQuote.Text = quotes.GetRandomQuote("");
        }
    }

    public void Bind()
    {
        string action = Request.QueryString["action"];
        int id = Convert.ToInt32(Conversion.Val(Request.QueryString["Id"]));
        string source = Request.QueryString["source"];
        string subject = Request.QueryString["subject"];
        Bind(action, id, source, subject);
    }

    public void Bind(string action, int id, string source, string subject)
    {
        if (txtSearchQuery.Text != "")
        {
            action = "search";
            source = txtSearchQuery.Text;
        }
        DataTable dt = null;
        string title = "";
        if (! string.IsNullOrEmpty(action))
        {
            action = action.ToLower();
        }

        if (id > 0 || ! string.IsNullOrEmpty(source) || ! string.IsNullOrEmpty(subject))
        {
            if (string.IsNullOrEmpty(action))
            {
                dt =
                    SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                             "dbo.spGetQuotes").Tables[0];
            }
            else
                switch (action)
                {
                    case "title":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.spGetQuotes").Tables[0];
                        title = "Browse by title";
                        break;
                    case "subject":
                        {
                            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                                  CommandType.StoredProcedure,
                                                                  "dbo.spGetQuotes",
                                                                  new SqlParameter("@subject", subject));
                            dt = ds.Tables[0];
                            title = "Browse by subject: " + subject;
                        }
                        break;
                    case "author":
                        {
                            DataSet ds = SqlHelper.ExecuteDataset(DataHelper.ConnectionString,
                                                                  CommandType.StoredProcedure,
                                                                  "dbo.spGetQuotes", new SqlParameter("@AuthorId", id));
                            dt = ds.Tables[0];
                            //title = "Browse by author: " & ds.Tables(1).Rows(0).Item(0).ToString
                        }
                        break;
                    case "source":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.spGetQuotes", new SqlParameter("@Source", source)).Tables[0];
                        title = "Browse by source: " + source;
                        break;
                    case "search":
                        dt =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.spGetQuotes", new SqlParameter("@SearchQuery", source)).Tables
                                [0];
                        title = dt.Rows.Count + " matches found.";
                        break;
                }
            if (ViewState["SortExpression"] != null && ViewState["SortExpression"].ToString() != "")
            {
                dt.DefaultView.Sort = ViewState["SortExpression"] + ViewState["SortDirection"].ToString();
            }
            gvQuotes.DataSource = dt;
            gvQuotes.DataBind();
        }
        else if (! string.IsNullOrEmpty(action)) // No particular id
        {
            DataTable dt2;
            if (action == "subject" && string.IsNullOrEmpty(subject))
            {
                dt2 = quotes.GetSubjectList();
                gvSubjects.DataSource = dt2;
                gvSubjects.DataBind();
                title = "Browse by quote subject";
            }
            else
                switch (action)
                {
                    case "author":
                        dt2 = quotes.GetAuthorList();
                        gvAuthors.DataSource = dt2;
                        gvAuthors.DataBind();
                        title = "Browse by author";
                        break;
                    case "source":
                        dt2 =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.Text,
                                                     "SELECT DISTINCT SOurce FROM QUOTES ORDER BY SOURCE").Tables[0];
                        gvSource.DataSource = dt2;
                        gvSource.DataBind();
                        title = "Browse by source";
                        break;
                    default:
                        gvQuotes.DataSource =
                            SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure,
                                                     "dbo.spGetQuotes").Tables[0];
                        gvQuotes.DataBind();
                        title = "Browse by title";
                        break;
                }
        }
        else
        {
            dt =
                SqlHelper.ExecuteDataset(DataHelper.ConnectionString, CommandType.StoredProcedure, "dbo.spGetQuotes").
                    Tables[0];
            if (ViewState["SortExpression"] != null && ViewState["SortExpression"].ToString() != "")
            {
                dt.DefaultView.Sort = ViewState["SortExpression"] + ViewState["SortDirection"].ToString();
            }
            gvQuotes.DataSource = dt.DefaultView;
            gvQuotes.DataBind();
        }
        lblTitle.Text = title;
    }

    protected void btnAskTheGuide_Click(object sender, EventArgs e)
    {
        // Search the guide
        ViewState["SortExpression"] = "";
        ViewState["SortDirection"] = "";
        gvQuotes.PageIndex = 0;
        Bind("search", 0, txtSearchQuery.Text.Trim(), "");
    }


    protected void dgStudyGuide_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvQuotes.PageIndex = e.NewPageIndex;
        Bind();
    }

    protected void gvQuotes_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["SortExpression"] = e.SortExpression;
        if (e.SortDirection == SortDirection.Ascending)
        {
            ViewState["SortDirection"] = " ASC";
        }
        else
        {
            ViewState["SortDirection"] = " DESC";
        }
        Bind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        Load += Page_Load;
        btnAskTheGuide.Click += btnAskTheGuide_Click;
        gvQuotes.PageIndexChanging += dgStudyGuide_PageIndexChanging;
        gvQuotes.Sorting += gvQuotes_Sorting;
    }
}