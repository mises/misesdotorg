<%@ Page Language="C#" MasterPageFile="~/MasterPages/Content.master" AutoEventWireup="false"
    Inherits="RegistrationPage" EnableEventValidation="true" CodeBehind="form.aspx.cs" %>

<%@ Register Src="Controls/Forms/PaymentInfo.ascx" TagName="PaymentInfo" TagPrefix="uc1" %>
<%@ Register Src="Controls/Forms/ContactInfo.ascx" TagName="ContactInfo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h3>
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
        <br />
    </h3>
    <div id="divError" runat="server" visible="False" enableviewstate="False">
        <p class="error ErrorMessage" style="color: red">
            There was a problem with your payment information. You may modify your payment information
            and try again, call us at 1-800-OF-MISES or email <a href="mailto:contact@freecapitalists.org?subject=<%= RegistrationForm.Title %>">
                contact@freecapitalists.org</a>.
        </p>
    </div>
    <div id="pnlResults" runat="server" visible="False">
        <h4>
            <asp:Literal ID="lblDonationThankYou" runat="server"></asp:Literal>
        </h4>
        <p style="font-style: italic;">
            <asp:Literal ID="lblPaymentMessage" runat="server"></asp:Literal>
        </p>
    </div>
    <div runat="server" id="theForm">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <div id="FormDescription">
            <asp:Literal ID="litDescription" runat="server"></asp:Literal>
        </div>
        <asp:Panel ID="pnlAttendance" runat="server" Visible="False">
            <strong>Additional guests attending: </strong>
            <asp:TextBox ID="txtNumAttending" runat="server"></asp:TextBox>
            <br />
            <asp:Literal ID="lblCostPerGuest" runat="server"></asp:Literal>
            <br />
        </asp:Panel>
        <uc2:ContactInfo ID="ContactInfo" runat="server" />

        <asp:Panel ID="pnlFormDetail" runat="server">
            <br />
            <br />
            <asp:DataList ID="dlProducts" runat="server" DataKeyField="ProductId"
                OnItemDataBound="dlProductsDataBound" DataSourceID="sqlProducts" AlternatingItemStyle-BorderColor="Silver"
                BackColor="#99CCFF">
                <ItemTemplate>
                    <asp:Literal ID="ProductId" runat="server" Text='<%#Eval("ProductId")%>' Visible="False"></asp:Literal>
                    </div> <strong>
                        <asp:Literal ID="lblProductName" runat="server" Text='<%#Eval("ProductName")%>'></asp:Literal></strong>
                    </div>
                    <div>
                        <%#Eval("Description")%>
                    </div>
                    <div>
                        <asp:Literal ID="lblQuantity" runat="server" Text="Quantity:"></asp:Literal>
                    </div>
                    <div>
                        <asp:TextBox ID="txtQuantity" runat="server" AutoPostBack="True" OnTextChanged="UpdateTotal"
                            CausesValidation="False" Text="0"></asp:TextBox>
                    </div>
                    <asp:Literal ID="SelectQuantity" runat="server" Text='<%#Eval("SelectQuantity")%>'
                        Visible="False"></asp:Literal>
                    <asp:CheckBox ID="chkPrice" runat="server" Text='<%#Eval("Price", "{0:C}")%>'
                        AutoPostBack="True" OnCheckedChanged="UpdateTotal" />
                </ItemTemplate>
                <ItemStyle />
            </asp:DataList>
            <br />
            <asp:Panel ID="pnlCustomAmount" runat="server" Visible="False">
                <div style="font-weight: bold" runat="server" id="divAmountPicker" visible="False">
                    <asp:Literal Text="" ID="lblDonationPrompt" runat="server" />
                    <asp:DropDownList ID="ddDonationAmount" runat="server" AutoPostBack="true" OnTextChanged="UpdateTotal">
                        <asp:ListItem Value="0">Select:</asp:ListItem>
                        <asp:ListItem Value="15">$15</asp:ListItem>
                        <asp:ListItem Value="35">$35</asp:ListItem>
                        <asp:ListItem Value="50">$50</asp:ListItem>
                        <asp:ListItem Value="100">$100</asp:ListItem>
                        <asp:ListItem Value="250">$250</asp:ListItem>
                        <asp:ListItem Value="500">$500</asp:ListItem>
                        <asp:ListItem Value="1000">$1000</asp:ListItem>
                        <asp:ListItem Value="5000">$5000</asp:ListItem>
                        <asp:ListItem Value="10000">$10000</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <hr />
                <strong>
                    <asp:Literal Text="Or enter a custom amount:" ID="lblEnterAmount" runat="server" />
                </strong>
                <asp:TextBox ID="txtCustomAmount" runat="server" AutoPostBack="True" OnTextChanged="UpdateTotal"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="reqCustomAmt" runat="server" ControlToValidate="txtCustomAmount"
            ErrorMessage="Please enter an amount.">*</asp:RequiredFieldValidator>--%>
            </asp:Panel>
            <asp:CheckBox ID="chkRecurring" runat="server"
                Text=" Repeat my gift monthly" Visible="False" BackColor="#FFFF66" />
            <br />

            <asp:DataList ID="dlQuestions" runat="server" DataKeyField="QuestionId"
                DataSourceID="sqlQuestions">
                <ItemTemplate>
                    <asp:Literal ID="QuestionLiteral" runat="server" Text='<%#Eval("Question")%>'></asp:Literal>
                    <br />
                    <asp:TextBox ID="txtAnswer" runat="server"></asp:TextBox>
                    <br />
                    <asp:Literal ID="lblQuestionType" runat="server" Text='<%#Eval("QuestionType")%>'
                        Visible="False"></asp:Literal>
                    <asp:Literal ID="QuestionIdLiteral" runat="server" Text='<%#Eval("QuestionId")%>'
                        Visible="False"></asp:Literal>
                </ItemTemplate>
                <ItemStyle />
            </asp:DataList>
            <asp:Button ID="btnGetTotal" runat="server" CausesValidation="False" Text="Calculate Total"
                CssClass="button"
                Visible="false" />
            <asp:Literal ID="lblFooterText" runat="server"></asp:Literal>
        </asp:Panel>
        <uc1:PaymentInfo ID="PaymentInfo" runat="server" />
        <br />
        <asp:Button ID="btnSubmitRegistration" runat="server" Text="Submit" CssClass="ui-state-default ui-corner-all ui-button" />
    </div>
    <asp:SqlDataSource ID="sqlProducts" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        SelectCommand="SELECT [ProductName], [ProductId], [Price], [Description], [SelectQuantity] FROM [RegistrationProducts] WHERE ([FormId] = @FormId) ORDER BY [ProductOrder]">
        <SelectParameters>
            <asp:QueryStringParameter Name="FormId" QueryStringField="Id" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlQuestions" runat="server" ConnectionString="<%$ ConnectionStrings:Public %>"
        SelectCommand="SELECT [Question], [QuestionType], [QuestionId], [QuestionOrder], [Required] FROM [RegistrationQuestions] WHERE ([FormId] = @FormId) ORDER BY [QuestionOrder]">
        <SelectParameters>
            <asp:QueryStringParameter Name="FormId" QueryStringField="Id" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
