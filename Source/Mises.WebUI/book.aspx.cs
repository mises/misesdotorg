#region

using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Mises.Data;


#endregion

public partial class book : Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (IsPostBack) return;

        var db = new MisesDBDataContext((new SqlConnection(DataHelper.ConnectionString)));
        if (Request.QueryString["Id"] == null) Response.RedirectPermanent("/library/");
        dvBook.DataSource = db.WardLibraryGetBook(int.Parse(Request.QueryString["Id"]));
        dvBook.DataBind();

        if (dvBook.Rows.Count > 0)
            Title = ((Literal) dvBook.Rows[0].FindControl("litTitle")).Text + Title;


        var meta = new HtmlMeta {Content = "book"};
        meta.Attributes.Add("property", "og:type");
        Page.Header.Controls.Add(meta);

        
    }
}