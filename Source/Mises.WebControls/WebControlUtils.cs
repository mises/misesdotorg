#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

#endregion

namespace MisesWeb
{
    public class WebControlUtils
    {
        public static Control FindControl(Control container, string id)
        {
            if ((container == null) || string.IsNullOrEmpty(id))
            {
                return null;
            }
            if (! (container is INamingContainer) && (container.NamingContainer != null))
            {
                container = container.NamingContainer;
            }
            Control control = FindControlDown(container, id, null);
            if (control == null)
            {
                var searchedContainers = new Dictionary<string, bool>();
                searchedContainers[container.ClientID] = true;
                container = container.NamingContainer;
                while ((container != null) && (control == null))
                {
                    control = FindControlDown(container, id, searchedContainers);
                    searchedContainers[container.ClientID] = true;
                    container = container.NamingContainer;
                }
            }
            return control;
        }

        private static Control FindControlDown(Control container, string id,
                                               IDictionary<string, bool> searchedContainers)
        {
            if (container.ID == id)
            {
                return container;
            }
            Control control = container.FindControl(id);
            if ((control != null) && (control.ID == id))
            {
                return control;
            }
            foreach (Control control2 in
                container.Controls.Cast<Control>().Where(
                    control2 => (searchedContainers == null) || !searchedContainers.ContainsKey(control2.ClientID)))
            {
                control = FindControlDown(control2, id, searchedContainers);
                if (control != null)
                {
                    return control;
                }
            }
            return null;
        }
    }
}