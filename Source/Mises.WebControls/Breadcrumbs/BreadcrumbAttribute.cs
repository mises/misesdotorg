﻿#region

using System;
using System.Web.Mvc;
using Mises.WebControls.Breadcrumbs;

#endregion

namespace Mises.WebControls.Web.Breadcrumbs
{
    public class BreadcrumbAttribute : ActionFilterAttribute
    {
        private string breadcrumbItemTitle;

        public BreadcrumbAttribute(string title, int level)
        {
            Title = title;
            Level = level;
        }

        public string Title { get; set; }
        public int Level { get; set; }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var controller = (Controller) filterContext.Controller;

            string breadcrumbText = controller.GetBreadcrumbText();

            if (!String.IsNullOrEmpty(Title))
            {
                breadcrumbItemTitle = String.Format(Title, breadcrumbText);
            }

            Breadcrumb[] breadcrumbs = GetBreadcrumbManager(filterContext)
                .PushBreadcrumb(
                    filterContext.HttpContext.Request.RawUrl,
                    breadcrumbItemTitle,
                    Level);

            controller.ViewData.Add("breadcrumbs", breadcrumbs);
            controller.ViewBag.Title = breadcrumbItemTitle;
            base.OnResultExecuting(filterContext);
        }

        private static IBreadcrumbManager GetBreadcrumbManager(ResultExecutingContext filterContext)
        {
            var bcm = filterContext.HttpContext.Session["breadcrumbManager"] as IBreadcrumbManager;
            if (bcm == null)
            {
                bcm = new BreadcrumbManager();
                filterContext.HttpContext.Session["breadcrumbManager"] = bcm;
            }
            return bcm;
        }
    }
}