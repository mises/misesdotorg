﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Mises.WebControls.Breadcrumbs;

#endregion

namespace Mises.WebControls.Web.Breadcrumbs
{
    public class BreadcrumbManager : IBreadcrumbManager
    {
        private readonly Stack<Breadcrumb> stack;

        public BreadcrumbManager()
        {
            stack = new Stack<Breadcrumb>();
        }

        #region IBreadcrumbManager Members

        public Breadcrumb[] PushBreadcrumb(string link, string text, int level)
        {
            if (String.IsNullOrEmpty(text))
            {
                text = link;
            }

            var bc = new Breadcrumb {Link = link, Text = text, Level = level};

            while (stack.Any() && stack.Peek().Level >= level)
            {
                stack.Pop();
            }

            stack.Push(bc);
            return stack.ToArray();
        }

        #endregion
    }
}