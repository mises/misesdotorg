﻿#region

using Mises.WebControls.Breadcrumbs;

#endregion

namespace Mises.WebControls.Web.Breadcrumbs
{
    public interface IBreadcrumbManager
    {
        Breadcrumb[] PushBreadcrumb(string link, string text, int level);
    }
}