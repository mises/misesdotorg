﻿#region

using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Mises.WebControls
{
    /// <summary>
    ///   Enhanced version of the GridView control. 
    ///   Supports: Column sort direction images, row selecting/highlighting, prev/numeric/next paging.
    ///   From http://www.codeproject.com/KB/webforms/NextPrevNumericPager.aspx
    /// </summary>
    [Themeable(true)]
    [DefaultProperty("SelectedValue")]
    [ToolboxData("<{0}:GridView runat=server></{0}:GridView>")]
    public class GridView : System.Web.UI.WebControls.GridView
    {
        private const string UrlEditor = "System.Web.UI.Design.UrlEditor";
        private const string MiscCategory = "Misc";
        private const string StyleBackgroundColorFormat = "this.style.backgroundColor = '{0}';";

        /// <summary>
        ///   Color of the row being hovered over.
        /// </summary>
        public string HighlightColor
        {
            get
            {
                if (ViewState[Keys.HighlightColor] == null)
                {
                    ViewState[Keys.HighlightColor] = false;
                }
                return (string) ViewState[Keys.HighlightColor];
            }

            set { ViewState[Keys.HighlightColor] = value; }
        }

        /// <summary>
        ///   Turns the selection highlighting on or off.
        /// </summary>
        public bool EnableSelection
        {
            get
            {
                if (ViewState[Keys.EnableSelection] == null)
                {
                    ViewState[Keys.EnableSelection] = false;
                }
                return (bool) ViewState[Keys.EnableSelection];
            }

            set { ViewState[Keys.EnableSelection] = value; }
        }

        /// <summary>
        ///   Turns on JustinsWeb sanctioned pager.
        /// </summary>
        public bool EnableNextPrevNumericPager
        {
            get
            {
                if (ViewState[Keys.EnableJustinWebPager] == null)
                {
                    ViewState[Keys.EnableJustinWebPager] = false;
                }
                return (bool) ViewState[Keys.EnableJustinWebPager];
            }

            set
            {
                AllowPaging = value;
                ViewState[Keys.EnableJustinWebPager] = value;
            }
        }

        /// <summary>
        ///   Get/Set alternative to the inherited SortDirection field that is always accurate.
        /// </summary>
        public SortDirection SortDirectionAlt
        {
            get
            {
                if (ViewState[Keys.SortDirection] == null)
                {
                    ViewState[Keys.SortDirection] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState[Keys.SortDirection];
            }

            set { ViewState[Keys.SortDirection] = value; }
        }

        /// <summary>
        ///   Get/Set alternative to the inherited SortExpression field that is always accurate.
        /// </summary>
        public string SortExpressionAlt
        {
            get
            {
                if (ViewState[Keys.SortExpressionAlt] == null)
                {
                    ViewState[Keys.SortExpressionAlt] = string.Empty;
                }
                return (string) ViewState[Keys.SortExpressionAlt];
            }

            set { ViewState[Keys.SortExpressionAlt] = value; }
        }

        /// <summary>
        ///   Get or Set Image location to be used to display Ascending Sort order.
        /// </summary>
        [
            Description("Image to display for Ascending Sort"),
            Category(MiscCategory),
            Editor(UrlEditor, typeof (UITypeEditor)),
            DefaultValue(""),
        ]
        public string SortAscImageUrl
        {
            get
            {
                object o = ViewState[Keys.SortImageAsc];
                return (o != null ? o.ToString() : string.Empty);
            }
            set { ViewState[Keys.SortImageAsc] = value; }
        }


        /// <summary>
        ///   Get or Set Image location to be used to display Ascending Sort order.
        /// </summary>
        [
            Description("Sets whether or not we show the sort arrows in the GridView."),
            Category(MiscCategory),
            Editor(UrlEditor, typeof (UITypeEditor)),
            DefaultValue(false),
        ]
        public bool ShowSortDirection
        {
            get
            {
                object o = ViewState[Keys.ShowSortDirection];
                return (o != null ? Convert.ToBoolean(o) : false);
            }
            set { ViewState[Keys.ShowSortDirection] = value; }
        }

        /// <summary>
        ///   Gets or Sets whether we show the sort arrow in the GridView after the header text or before.
        /// </summary>
        [
            Description("Sets whether we show the sort arrow in the GridView after the header text or before."),
            Category(MiscCategory),
            Editor(UrlEditor, typeof (UITypeEditor)),
            DefaultValue(false),
        ]
        public bool ShowSortImageBeforeHeaderText
        {
            get
            {
                object o = ViewState[Keys.ShowSortImageBeforeHeaderText];
                return (o != null ? Convert.ToBoolean(o) : false);
            }
            set { ViewState[Keys.ShowSortImageBeforeHeaderText] = value; }
        }

        /// <summary>
        ///   Get or Set Image location to be used to display Ascending Sort order.
        /// </summary>
        [
            Description("Image to display for Descending Sort"),
            Category(MiscCategory),
            Editor(UrlEditor, typeof (UITypeEditor)),
            DefaultValue(""),
        ]
        public string SortDescImageUrl
        {
            get
            {
                object o = ViewState[Keys.SortImageDesc];
                return (o != null ? o.ToString() : string.Empty);
            }
            set { ViewState[Keys.SortImageDesc] = value; }
        }


        /// <summary>
        ///   Make the sort column header bold
        /// </summary>
        [
            Description("Make the sort column header bold"),
            Category(MiscCategory),
            Editor(UrlEditor, typeof (UITypeEditor)),
            DefaultValue(true),
        ]
        public bool SortColumnHeaderIsBold
        {
            get
            {
                if (ViewState[Keys.SortColumnHeader] == null)
                {
                    ViewState[Keys.SortColumnHeader] = false;
                }
                return (bool) ViewState[Keys.SortColumnHeader];
            }

            set { ViewState[Keys.SortColumnHeader] = value; }
        }


        /// <summary>
        ///   When using GridView in certain ways the SortDirection and SortExpression
        ///   properties are sometimes left blank or never changed. When using this control,
        ///   the Alt properties remedy this situation.
        /// </summary>
        /// <param name = "e"></param>
        protected override void OnSorting(GridViewSortEventArgs e)
        {
            base.PageIndex = 0;

            //Handle setting up of sorting info 
            if (!String.IsNullOrEmpty(SortExpression))
            {
                SortExpressionAlt = e.SortExpression;
                SortDirectionAlt = e.SortDirection;
            }
            else
            {
                if (SortExpressionAlt == e.SortExpression)
                {
                    SortDirectionAlt = SortDirectionAlt == SortDirection.Ascending
                                           ? SortDirection.Descending
                                           : SortDirection.Ascending;
                }
                else
                {
                    SortDirectionAlt = SortDirection.Ascending;
                }

                SortExpressionAlt = e.SortExpression;
            }

            base.OnSorting(e);
        }


        protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
        {
            if (EnableNextPrevNumericPager)
            {
                base.PagerTemplate = new NextPrevNumericPagerTemplate(PageIndex, PageCount);
            }
            base.InitializePager(row, columnSpan, pagedDataSource);
        }

        /// <summary>
        ///   Adds custom effects to the GridView at runtime.
        /// </summary>
        /// <param name = "e"></param>
        protected override void OnRowCreated(GridViewRowEventArgs e)
        {
            base.OnRowCreated(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //If row selection is enabled then add mouse over scripts to enable on client.
                if (EnableSelection)
                {
                    EnableEnhancedSelectForRow(e);
                }
            }
            else if (e.Row.RowType == DataControlRowType.Header && ShowSortDirection)
            {
                //this.SortDirection
                foreach (TableCell headerCell in
                    e.Row.Cells.Cast<TableCell>().Where(headerCell => headerCell.HasControls()))
                {
                    AddSortImageToHeaderCell(headerCell);
                }
            }
        }

        private void AddSortImageToHeaderCell(TableCell headerCell)
        {
            // search for the header link
            var lnkBtn = (WebControl) headerCell.Controls[0];

            // initialize a new image
            var img = new Image
                          {
                              ImageUrl =
                                  (SortDirectionAlt == SortDirection.Ascending
                                       ? SortAscImageUrl
                                       : SortDescImageUrl)
                          }; // setting the dynamically URL of the image

            // checking if the header link is the user's choice
            if ((lnkBtn is LinkButton && SortExpressionAlt == ((LinkButton) lnkBtn).CommandArgument)
                ||
                (lnkBtn is ImageButton && SortExpressionAlt == ((ImageButton) lnkBtn).CommandArgument)
                )
            {
                // adding a space and the image to the header link
                headerCell.Controls.Add(new LiteralControl(" "));
                headerCell.Controls.Add(img);

                // Make the sort column header bold
                if (SortColumnHeaderIsBold)
                {
                    lnkBtn.Style.Add("font-weight", "bolder");
                }
            }
        }

        private void EnableEnhancedSelectForRow(GridViewRowEventArgs e)
        {
            e.Row.Attributes.Add("onmouseover", RowOnMouseOverScript());
            e.Row.Attributes.Add("onmouseout", RowOnMouseOutScript());


            // if no link are presen the cell,
            // we add the functionnality to select the row on the cell with a click
            foreach (TableCell cell in e.Row.Cells.Cast<TableCell>().Where(cell => !Recurser.ContainsLink(cell)))
            {
                AddPostBackEventToCell(e, cell);
            }
        }

        private void AddPostBackEventToCell(GridViewRowEventArgs e, TableCell cell)
        {
            // here we add the command to postback when the user click somewhere in the cell
            cell.Attributes.Add("onclick",
                                Page.ClientScript.GetPostBackEventReference(this,
                                                                            "Select$" + e.Row.RowIndex));
            cell.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
            cell.Attributes.Add("title", "Select");
        }

        /// <summary>
        ///   Highlights the background of the row that the mouse is currently hovering over.
        /// </summary>
        /// <returns></returns>
        protected string RowOnMouseOverScript()
        {
            return string.Format(StyleBackgroundColorFormat, HighlightColor);
        }

        /// <summary>
        ///   Removes highlighting created by RowOnMouseOverScript.
        /// </summary>
        /// <returns></returns>
        protected string RowOnMouseOutScript()
        {
            return string.Format(StyleBackgroundColorFormat, string.Empty);
        }

        #region Nested type: Keys

        private static class Keys
        {
            public const string HighlightColor = "highlightColor";
            public const string EnableSelection = "enableSelection";
            public const string EnableJustinWebPager = "enableJustinsWebPager";
            public const string SortDirection = "sortDirection";
            public const string SortExpressionAlt = "sortExpressionAlt";
            public const string SortColumnHeader = "sortColumnHeaderIsBold";
            public const string SortImageAsc = "SortImageAsc";
            public const string ShowSortDirection = "ShowSortDirection";
            public const string ShowSortImageBeforeHeaderText = "ShowSortImageBeforeHeaderText";
            public const string SortImageDesc = "SortImageDesc";
        }

        #endregion
    }
}