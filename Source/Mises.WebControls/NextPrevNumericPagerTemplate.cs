#region

using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Mises.WebControls
{
    public class NextPrevNumericPagerTemplate : ITemplate
    {
        private const string PageCommandName = "Page";
        private const string LastCommandArgument = "Last";
        private const string PagerResultsSummary = "PagerResultsSummary";
        private readonly int _pageCount;
        private readonly int _pageIndex;

        public NextPrevNumericPagerTemplate(int pageIndex, int pageCount)
        {
            _pageIndex = pageIndex;
            _pageCount = pageCount;
        }

        private int _pageNumber
        {
            get { return _pageIndex + 1; }
            //set { _pageIndex = value - 1; }
        }

        #region ITemplate Members

        public void InstantiateIn(Control container)
        {
            int pagerStartIndex = startPageIndex(_pageIndex, _pageCount);
            int pagerEndIndex = endPageIndex(_pageIndex, _pageCount);


            if (_pageNumber > 1)
            {
                createPrevButton(container);
                createSpacer(container);
                createPrevEllipsisIfNeeded(container, pagerStartIndex);
            }

            var resultsSummary = new Label {CssClass = PagerResultsSummary, Text = " page "};
            container.Controls.Add(resultsSummary);

            createCorrectPageButtons(container, pagerStartIndex, pagerEndIndex);
            createNextEllipsisIfNeeded(container, pagerEndIndex);

            createLastPageButton(container);

            if (_pageNumber < _pageCount)
            {
                createNextButton(container);
            }
        }

        #endregion

        private void createNextEllipsisIfNeeded(Control container, int pagerEndIndex)
        {
            if (pageNumber(pagerEndIndex) < _pageCount)
            {
                createEllipsisButton(container, pagerEndIndex + 1);
            }
        }

        private static void createPrevEllipsisIfNeeded(Control container, int pagerStartIndex)
        {
            if (pageNumber(pagerStartIndex) > 1)
            {
                createEllipsisButton(container, pagerStartIndex - 1);
            }
        }

        private void createCorrectPageButtons(Control container, int pagerStartIndex, int pagerEndIndex)
        {
            for (int i = pagerStartIndex; i <= pagerEndIndex; i++)
            {
                createCorrectPageButton(container, i);
            }
        }

        private void createLastPageButton(Control container)
        {
            var lastPage = new Label {CssClass = PagerResultsSummary, Text = " of "};
            container.Controls.Add(lastPage);

            var nextButton = new LinkButton
                                 {
                                     CommandName = PageCommandName,
                                     CommandArgument = LastCommandArgument,
                                     CssClass = PagerResultsSummary,
                                     Text = _pageCount.ToString()
                                 };
            container.Controls.Add(nextButton);

            lastPage = new Label {CssClass = PagerResultsSummary, Text = " "};
            container.Controls.Add(lastPage);
        }

        private static void createNextButton(Control container)
        {
            var nextButton = new LinkButton {CommandName = PageCommandName, CommandArgument = "Next", Text = "next >"};
            container.Controls.Add(nextButton);
        }

        private static void createPrevButton(Control container)
        {
            var prevButton = new LinkButton
                                 {
                                     CommandName = PageCommandName,
                                     CommandArgument = "Prev",
                                     Text = "< previous"
                                 };
            container.Controls.Add(prevButton);
        }


        private void createResultsSummary(Control container)
        {
            var resultsSummary = new Label
                                     {
                                         CssClass = PagerResultsSummary,
                                         Text = "Page " + _pageNumber + " of " + _pageCount
                                     };

            container.Controls.Add(resultsSummary);
        }

        private void createCorrectPageButton(Control container, int pageIndexOnButton)
        {
            if (_pageIndex == pageIndexOnButton)
            {
                createNumericPageLabel(container, pageIndexOnButton);
            }
            else
            {
                createNumericPageButton(container, pageIndexOnButton);
            }

            createSpacer(container);
        }

        private static void createNumericPageButton(Control container, int pageIndex)
        {
            var pageButton = new LinkButton
                                 {
                                     Text = pageNumber(pageIndex).ToString(),
                                     CommandName = PageCommandName,
                                     CommandArgument = pageNumber(pageIndex).ToString()
                                 };
            container.Controls.Add(pageButton);
        }

        private static void createNumericPageLabel(Control container, int pageIndex)
        {
            var currentPageLabel = new Label
                                       {
                                           CssClass = "SelectedPageButton",
                                           Text = pageNumber(pageIndex).ToString()
                                       };
            container.Controls.Add(currentPageLabel);
        }


        private static void createEllipsisButton(Control container, int goToIndex)
        {
            var pageButton = new LinkButton
                                 {
                                     Text = "...",
                                     CommandName = PageCommandName,
                                     CommandArgument = pageNumber(goToIndex).ToString()
                                 };
            container.Controls.Add(pageButton);

            createSpacer(container);
        }

        private static void createSpacer(Control container)
        {
            var spacer = new Literal {Text = "&nbsp;"};
            container.Controls.Add(spacer);
        }


        private static int startPageIndex(int currentPageIndex, int totalPageCount)
        {
            int startingPageToDisplay = currentPageIndex - 4;

            if ((pageIndex(totalPageCount) - currentPageIndex) < 5)
            {
                return pageIndex(totalPageCount) - 9 < 0 ? 0 : pageIndex(totalPageCount) - 9;
            }
            if (startingPageToDisplay < 0)
            {
                return 0;
            }
            return startingPageToDisplay;
        }

        private static int endPageIndex(int currentPageIndex, int totalPageCount)
        {
            int endingPageToDisplay = currentPageIndex + 5;
            int maxEndingPageIndex = (9 > pageIndex(totalPageCount)) ? pageIndex(totalPageCount) : 9;

            if (endingPageToDisplay > totalPageCount - 1)
            {
                endingPageToDisplay = totalPageCount - 1;
            }
            else if (currentPageIndex < 5)
            {
                endingPageToDisplay = maxEndingPageIndex;
            }
            return endingPageToDisplay;
        }

        /// <summary>
        ///   Converts a given page index to a page number.
        /// </summary>
        /// <param name = "pageIndex"></param>
        /// <returns></returns>
        private static int pageNumber(int pageIndex)
        {
            return pageIndex + 1;
        }

        /// <summary>
        ///   Converts a given page number to a page index.
        /// </summary>
        /// <param name = "pageNumber"></param>
        /// <returns></returns>
        private static int pageIndex(int pageNumber)
        {
            return pageNumber - 1;
        }
    }
}