﻿#region

using System.Web.Routing;
using Mises.Domain;

#endregion

namespace System.Web.Mvc
{
    public static class ManagerHelper
    {
        public static string DocumentId(this HtmlHelper helper)
        {
            if (helper.ViewData.ContainsKey("DocumentId"))
                return helper.ViewData["DocumentId"].ToString();

            if (helper.ViewData.ContainsKey("Id"))
                return helper.ViewData["Id"].ToString();

            string id = "";
            if (helper.ViewContext.RequestContext.HttpContext.Request["Id"] != null)
            {
                id = helper.ViewContext.RequestContext.HttpContext.Request["Id"];
            }
            if (helper.ViewContext.RequestContext.HttpContext.Request["MediaId"] != null)
            {
                id = helper.ViewContext.RequestContext.HttpContext.Request["Id"];
            }
            if (helper.ViewContext.RequestContext.HttpContext.Request["ccontrol"] != null)
            {
                id = helper.ViewContext.RequestContext.HttpContext.Request["Id"];
            }

            var values = helper.ViewContext.RequestContext.RouteData.Values;

            if (values.ContainsKey("id"))
            {
                id = values["id"].ToString();
            }

            return id;
        }

        public static string EditURL(this HtmlHelper helper)
        {
            return AdminHelper.GetEditURL(helper.DocumentId(),
                                          helper.ViewContext.RequestContext.HttpContext.Request.Url.ToString(),
                                          helper.ViewContext.RequestContext.HttpContext.Request.QueryString);
        }

        public static bool IsAdmin(this HtmlHelper helper)
        {
            return AdminHelper.IsAdmin(helper.ViewContext.RequestContext.HttpContext.Request.Cookies);
        }
    }
}