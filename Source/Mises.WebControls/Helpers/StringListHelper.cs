#region

using System.Collections.Generic;

#endregion

namespace System.Web.Mvc
{
    public static class StringListHelper
    {
        public static MvcHtmlString StringList(this HtmlHelper helper, IEnumerable<string> items)
        {
            return StringList(helper, items, null);
        }


        public static MvcHtmlString StringList(this HtmlHelper helper, IEnumerable<string> items,
                                               IDictionary<string, object> htmlAttributes)
        {
            var ul = new TagBuilder("ul");

            foreach (var item in items)
            {
                var li = new TagBuilder("li");

                var span = new TagBuilder("label");
                span.SetInnerText(item);
                if (htmlAttributes != null)
                    span.MergeAttributes(htmlAttributes, true);

                li.InnerHtml = span.ToString();

                ul.InnerHtml += li.ToString();
            }

            return MvcHtmlString.Create(ul.ToString());
        }
    }
}