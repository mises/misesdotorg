﻿#region

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Mises.Domain.Utility;

#endregion

namespace Mises.WebControls.Helpers
{
    /// <summary>
    ///   http://blogs.msdn.com/miah/archive/2008/11/13/extending-mvc-returning-an-image-from-a-controller-action.aspx
    /// </summary>
    public static class ControllerExtensions
    {
        public static ImageResult Image(this Controller controller, Stream imageStream, string contentType)
        {
            return new ImageResult(imageStream, contentType);
        }

        public static ImageResult Image(this Controller controller, byte[] imageBytes, string contentType)
        {
            return new ImageResult(new MemoryStream(imageBytes), contentType);
        }

        public static ImageResult Image(this Controller controller, byte[] imageBytes, string contentType,
                                        string fileName)
        {
            return new ImageResult(new MemoryStream(imageBytes), contentType, fileName);
        }

        public static string ConvertToString(this string[] anyString)
        {
            if (anyString != null && anyString.Length > 0)
            {
                StringBuilder result = new StringBuilder();
                foreach (var t in anyString)
                {
                    result.Append(string.Format("{0} ", t));
                }
                return result.ToString().Trim();
            }
            else
            {
                return string.Empty;
            }
        }
    }

    public class ImageResult : ActionResult
    {
        public ImageResult(Stream imageStream, string contentType)
        {
            if (imageStream == null)
                throw new ArgumentNullException("imageStream");
            if (contentType == null)
                throw new ArgumentNullException("contentType");

            ImageStream = imageStream;
            ContentType = contentType;
            FileName = "";
        }

        public ImageResult(Stream imageStream, string contentType, string fileName, int maxDimension = 0)
        {
            if (imageStream == null)
                throw new ArgumentNullException("imageStream");
            if (contentType == null)
                throw new ArgumentNullException("contentType");

            ImageStream = imageStream;
            ContentType = contentType;
            FileName = fileName;
            MaxDimension = maxDimension;
        }

        public Stream ImageStream { get; private set; }
        public string ContentType { get; private set; }
        public string FileName { get; private set; }
        public readonly int MaxDimension;

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = ContentType;

            if (!string.IsNullOrWhiteSpace(FileName))
                response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);

            response.Cache.SetCacheability(HttpCacheability.Public);

            if (MaxDimension > 0 && ImageStream != null)
            {
                var image = Image.FromStream(ImageStream);

                if (image.Width > MaxDimension || image.Height > MaxDimension)
                {
                    int height = MaxDimension;
                    int width = MaxDimension;
                    //find the greater proportion
                    decimal ratio = image.Width / (decimal)image.Height;

                    if (ratio > 1) // too wide
                        height = (int)(MaxDimension / ratio);
                    else
                        width = (int)(MaxDimension * ratio);

                    image = Imaging.CreateReducedImage(image, new Size(width, height));
                    ImageStream = image.ToStream(ImageFormat.Jpeg);
                }
                else
                {
                    ImageStream = image.ToStream(ImageFormat.Jpeg);
                }
            }


            byte[] buffer = new byte[4096];
            while (true)
            {
                int read = ImageStream.Read(buffer, 0, buffer.Length);
                if (read == 0)
                    break;

                response.OutputStream.Write(buffer, 0, read);
            }

            response.End();
        }
    }

    
}