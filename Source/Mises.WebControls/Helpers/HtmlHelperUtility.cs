﻿#region

using System.Web.Mvc;

#endregion

namespace Mises.WebControls.Web.Html
{
    public static class HtmlHelperUtility
    {
        public static T GetValueFromProviderValue<T>(this HtmlHelper helper, string key)
        {
            var valueProvider = helper.ViewContext.Controller.ValueProvider;
            return (T) valueProvider.GetValue(key).ConvertTo(typeof (T));
        }

        public static string GetValueFromProviderValue(this HtmlHelper helper, string key)
        {
            var valueProvider = helper.ViewContext.Controller.ValueProvider;
            return valueProvider.GetValue(key).AttemptedValue;
        }
    }
}