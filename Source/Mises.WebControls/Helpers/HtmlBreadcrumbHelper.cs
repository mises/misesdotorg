﻿#region

using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Mises.WebControls.Breadcrumbs;

#endregion

namespace Mises.WebControls.Web.UI.Helpers
{
    public static class HtmlBreadcrumbHelper
    {
        public static MvcHtmlString Breadcrumb(this HtmlHelper helper)
        {
            Breadcrumb[] breadcrumb = helper.ViewData["breadcrumbs"] as Breadcrumb[];
            return breadcrumb != null ? Breadcrumb(helper, breadcrumb) : new MvcHtmlString(String.Empty);
        }

        public static MvcHtmlString Breadcrumb(this HtmlHelper helper, Breadcrumb[] breadcrumbs)
        {
            const string arrow = "<span class='breadcrumbs-arrow'> » </span>";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='breadcrumbs'>");
            foreach (var breadcrumb in breadcrumbs.Reverse())
            {
                sb.Append(arrow)
                    .Append("<span class='breadcrumb'><a href='")
                    .Append(breadcrumb.Link)
                    .Append("'>")
                    .Append(breadcrumb.Text)
                    .Append("</a></span>");
            }
            sb.Append("</div>");
            sb.Replace("class='breadcrumbs'>" + arrow, "class='breadcrumbs'>");
            return new MvcHtmlString(sb.ToString());
        }
    }
}