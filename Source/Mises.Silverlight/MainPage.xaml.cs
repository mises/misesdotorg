﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using ControlLibrary;
using Mises.Silverlight.FeadturedDocumentFeed;

namespace Mises.Silverlight {
    public partial class MainPage : UserControl {
        private readonly ObservableCollection<DocumentDTO> _documents = new ObservableCollection<DocumentDTO>();
        private int _selectedIndex = -1;
        public MainPage() {

            InitializeComponent();
            

            this.flowControl.ItemsSource = _documents;
            this.flowControl.SelectedItemChanged += flowControl_SelectedItemChanged;
            KeyDown += flowControl.OnKeyDown;
            MouseWheel += MainPage_MouseWheel;
            this.Loaded += MainPage_Loaded;
        }

        void MainPage_MouseWheel(object sender, MouseWheelEventArgs e) {
            int i = -(e.Delta / 120);
            int index = flowControl.SelectedIndex + i;
            if (index < 0) {
                flowControl.First();
            }
            else if (index > flowControl.Items.Count - 1) { 
                flowControl.Last(); 
            }
            else {
                flowControl.SelectedIndex = index;
            }
        }


        void flowControl_SelectedItemChanged(ControlLibrary.CoverFlowEventArgs e)
        { 
            if (e.Index == this._selectedIndex) {
                var doc = _documents[e.Index];
                var uri = new Uri(string.Format("/mediaplayer.aspx?MediaId={0}&path={1}",doc.MediaId, doc.URL),UriKind.Relative);
                HtmlPage.Window.Navigate(uri);
                
            }
            else {
                this._selectedIndex = e.Index;
            }
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e) {
            var client = new VideoFeedClient();
            client.GetFeaturedVideosAsync();
            client.GetFeaturedVideosCompleted += client_GetFeaturedVideosCompleted;
        }

        private void client_GetFeaturedVideosCompleted(object sender, GetFeaturedVideosCompletedEventArgs e) {
            if (e.Error != null) {
                HtmlPage.Window.Alert(e.Error.Message);
            }
            else {
                foreach (var dto in e.Result)
                {
                    var dc = new DocumentClient(dto);
                    var img = new BitmapImage();
                    if (dto.ImageBinary != null)
                    {
                        img.SetSource(new MemoryStream(dto.ImageBinary.Bytes));
                    }
                    else
                    {
                        var scheme = Application.Current.Host.Source.Scheme;

                        
                        var host = Application.Current.Host.Source.Host;
                        var port = Application.Current.Host.Source.Port;

                        var url = string.Format("{0}://{1}:{2}{3}", scheme, host, port, "/images/media_general.png");
                        var uri = new Uri(url);
                        img.UriSource = uri;
                    }

                    dc.Image = img;
                    _documents.Add(dc);
                }
            }
        }

        private void mainImage_MouseEnter(object sender, MouseEventArgs e)
        {
            Image image = sender as Image;
            var sb = new Storyboard();
            var grid = image.Parent as Grid;

            grid.Resources.Clear();

            if(image!=null) {
                // find the Title Textblock
                TextBlock tb = null;
                UIElementCollection children = grid.Children;
                foreach (UIElement child in children) {
                    var t = child as TextBlock;
                    if(t!=null && t.Name == "TitleText") {
                        tb = t;
                        break;
                    }

                }

                if(tb!=null) {
                    var textAnimation = new DoubleAnimation { From = 0, To = 1 };
                    Storyboard.SetTargetProperty(textAnimation, new PropertyPath("Opacity"));
                    Storyboard.SetTarget(textAnimation, tb);
                    sb.Children.Add(textAnimation);
                }

               

               

                var dbl = new DoubleAnimation { From = 1, To = .3 };

                Storyboard.SetTargetProperty(dbl, new PropertyPath("Opacity"));
                Storyboard.SetTarget(dbl, image);
                sb.Children.Add(dbl);
                sb.Begin();

                grid.Resources.Add("OON", sb);
            }
           

        }

        private void mainImage_MouseLeave(object sender, MouseEventArgs e)
        {
            Image image = sender as Image;
            var sb = new Storyboard();
            var grid = image.Parent as Grid;

            grid.Resources.Clear();

            if (image != null)
            {
                // find the Title Textblock
                TextBlock tb = null;
                UIElementCollection children = grid.Children;
                foreach (UIElement child in children)
                {
                    var t = child as TextBlock;
                    if (t != null && t.Name == "TitleText")
                    {
                        tb = t;
                        break;
                    }

                }

                if (tb != null)
                {
                    var textAnimation = new DoubleAnimation { From = 1, To = 0 };
                    Storyboard.SetTargetProperty(textAnimation, new PropertyPath("Opacity"));
                    Storyboard.SetTarget(textAnimation, tb);
                    sb.Children.Add(textAnimation);
                }





                var dbl = new DoubleAnimation { From = .3, To = 1 };

                Storyboard.SetTargetProperty(dbl, new PropertyPath("Opacity"));
                Storyboard.SetTarget(dbl, image);
                sb.Children.Add(dbl);
                sb.Begin();

                grid.Resources.Add("OOFF", sb);
            }

            

        }
    }
    public class DocumentClient:DocumentDTO {
        public DocumentClient(DocumentDTO dto) {
            this.Title = dto.Title;
            this.Author1Name = dto.Author1Name;
            this.MetaDescription = dto.MetaDescription;
            this.CreateDate = dto.CreateDate;
            this.AuthorLinkPage = string.Concat("/articles.aspx?AuthorId=", dto.Author1);
            this.URL = dto.URL;
            this.MediaTypeId = dto.MediaTypeId;
            this.DocumentId = dto.DocumentId;
            this.MediaId = dto.MediaId;
        }

        public BitmapImage Image { get; set; }
        public string AuthorLinkPage { get; set; }
    }
    public class ImageBrushConverter : IValueConverter
    {
        public ImageBrushConverter() {
        }

        // This converts the value object to the string to display.
        // This will work with most simple types.
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return "";

            var bytes = value as Binary;
            return new MemoryStream(bytes.Bytes);
        }

        // No need to implement converting back on a one-way binding
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class DateConverter : IValueConverter
    {

        #region IValueConverter Members

        //Called when binding from an object property to a control property
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || (DateTime)value == DateTime.MinValue) return null;
            DateTime dt = (DateTime)value;
            return dt.ToString((string)parameter, culture);
        }

        //Called with two-way data binding as value is pulled out of control and put back into the property
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string val = (string)value;
            DateTime outDate;
            if (DateTime.TryParse(val, culture, DateTimeStyles.None, out outDate))
            {
                return outDate;
            }
            return DependencyProperty.UnsetValue;

        }

        #endregion
    }

}