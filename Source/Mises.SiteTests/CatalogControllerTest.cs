﻿#region

using System.Linq;
using System.Web;
using System.Web.Mvc;
using LiteratureCatalog.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.SiteTests;
using Mises.Web.Areas.Catalog.Controllers;
using Moq;

#endregion

namespace LiteratureCatalog.Tests
{
    ///<summary>
    ///  This is a test class for CatalogControllerTest and is intended
    ///  to contain all CatalogControllerTest Unit Tests
    ///</summary>
    [TestClass]
    public class CatalogControllerTest
    {
        ///<summary>
        ///  Gets or sets the test context which provides
        ///  information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        ///<summary>
        ///  A test for Index
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        // // [HostType("ASP.NET")]
        // [AspNetDevelopmentServerHost("F:\\web\\MisesWeb\\LiteratureCatalog", "/")]
        // [UrlToTest("http://localhost:31396/")]
        public void IndexTest()
        {
            CatalogController target = new CatalogController(); // TODO: Initialize to an appropriate value
            //  ActionResult expected = null; // TODO: Initialize to an appropriate value
            ActionResult actual;
            actual = target.Index();
            Assert.IsInstanceOfType(actual, typeof (FeedResult));
            //Assert.AreEqual(expected, actual);
        }

        ///<summary>
        ///  A test for Titles
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //// [HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("F:\\web\\MisesWeb\\LiteratureCatalog", "/")]
        //[UrlToTest("http://localhost:31396/")]
        public void TitlesTest()
        {
            CatalogController target = new CatalogController(); // TODO: Initialize to an appropriate value
            //  ActionResult expected = null; // TODO: Initialize to an appropriate value
            ActionResult actual;
            actual = target.Books();
            Assert.IsInstanceOfType(actual, typeof (FeedResult));
            //Assert.AreEqual(expected, actual);
        }

        ///<summary>
        ///  A test for TopAuthors
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        // [HostType("ASP.NET")]
        // [UrlToTest("http://localhost/Catalog")] 
        public void TopAuthorsTest()
        {
            CatalogController target = new CatalogController(); // TODO: Initialize to an appropriate value
            FeedResult actual;
            actual = target.TopAuthors();
            Assert.IsTrue(actual.Feed.Feed.Items.Any());
        }

        ///<summary>
        ///  A test for Author
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        // [HostType("ASP.NET")]
        // [UrlToTest("http://localhost/Catalog")] 
        public void AuthorTest()
        {
            CatalogController target = new CatalogController(); // TODO: Initialize to an appropriate value
            target.SetFakeControllerContext();
            target.Request.SetupRequestUrl("~/Catalog/catalog");

            const int authorId = 299; // TODO: Initialize to an appropriate value
            FeedResult actual;
            actual = target.Author(authorId);
            Assert.IsTrue(actual.Feed.Feed.Items.Any());
        }

        ///<summary>
        ///  A test for Authors
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        // [HostType("ASP.NET")]
        // [UrlToTest("http://localhost/Catalog")] 
        public void AuthorsTest()
        {
            CatalogController target = new CatalogController(); // TODO: Initialize to an appropriate value
            FeedResult actual;
            actual = target.Authors();
            Assert.IsTrue(actual.Feed.Feed.Items.Any());
        }
    }
}