﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Collections.Specialized;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;
using Moq;

namespace Mises.SiteTests
{
    public static class MvcMockHelpers
    {

        public static ControllerContext GetControllerContext(string httpmethod, NameValueCollection querystring = null, NameValueCollection form = null)
        {
            var user = new Mock<IPrincipal>();
            user.Setup(u => u.Identity.Name).Returns("testid");
            var request = new Mock<HttpRequestBase>();
            request.Setup(r => r.HttpMethod).Returns(httpmethod);
            request.Setup(r => r.PathInfo).Returns(string.Empty);
            var mockHttpContext = new Mock<HttpContextBase>();
            mockHttpContext.Setup(c => c.Request).Returns(request.Object);
            mockHttpContext.Setup(c => c.User).Returns(user.Object);
            mockHttpContext.Setup(c => c.Request.QueryString).Returns(querystring);
            mockHttpContext.Setup(c => c.Request.Form).Returns(form);
            return new ControllerContext(mockHttpContext.Object, new RouteData(), new Mock<ControllerBase>().Object);
        }

        public static HttpContextBase FakeHttpContext()
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>();

            var requestContext = new Mock<RequestContext>();


            context.Expect(h => h.Cache).Returns(HttpRuntime.Cache);

            context.Expect(ctx => ctx.Request).Returns(request.Object);
            context.Expect(ctx => ctx.Response).Returns(response.Object);
            context.Expect(ctx => ctx.Session).Returns(session.Object);
            context.Expect(ctx => ctx.Server).Returns(server.Object);
            context.Expect(ctx => ctx.Request.RequestContext).Returns(requestContext.Object);
            context.Expect(ctx => ctx.Request.RequestContext.HttpContext).Returns(context.Object);

            //var params = new Mock<NameValueCollection>();
            //context.Expect(ctx => ctx.Request.Params).Returns(params.Object);

            return context.Object;
        }

        public static HttpContextBase FakeHttpContext(string url)
        {
            HttpContextBase context = FakeHttpContext();
            context.Request.SetupRequestUrl(url);
            return context;
        }

        public static void SetFakeControllerContext(this Controller controller)
        {
            var httpContext = FakeHttpContext();
            ControllerContext context = new ControllerContext(new RequestContext(httpContext, new RouteData()), controller);
            controller.ControllerContext = context;
        }

        static string GetUrlFileName(string url)
        {
            if (url.Contains("?"))
                return url.Substring(0, url.IndexOf("?"));
            else
                return url;
        }

        static NameValueCollection GetQueryStringParameters(string url)
        {
            if (url.Contains("?"))
            {
                NameValueCollection parameters = new NameValueCollection();

                string[] parts = url.Split("?".ToCharArray());
                string[] keys = parts[1].Split("&".ToCharArray());

                foreach (string key in keys)
                {
                    string[] part = key.Split("=".ToCharArray());
                    parameters.Add(part[0], part[1]);
                }

                return parameters;
            }
            else
            {
                return null;
            }
        }

        public static void SetHttpMethodResult(this HttpRequestBase request, string httpMethod)
        {
            Mock.Get(request)
                .Expect(req => req.HttpMethod)
                .Returns(httpMethod);
        }

        public static void SetupRequestUrl(this HttpRequestBase request, string url)
        {
            if (url == null)
                throw new ArgumentNullException("url");

            if (!url.StartsWith("~/"))
                throw new ArgumentException("Sorry, we expect a virtual url starting with \"~/\".");

            var mock = Mock.Get(request);

            mock.Expect(req => req.QueryString)
                .Returns(GetQueryStringParameters(url));
            mock.Expect(req => req.AppRelativeCurrentExecutionFilePath)
                .Returns(GetUrlFileName(url));
            mock.Expect(req => req.PathInfo)
                .Returns(string.Empty);
            mock.Expect(req => req.Url)
                .Returns(new Uri(url.Replace("~", "http://localhost/")));

        }
    }
}