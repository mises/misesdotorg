﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BookCatalog.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.Data;

namespace Mises.SiteTests
{
    [TestClass]
    public class DailyTests
    {
        [TestMethod]
        public void GetArchivesReturnsArticles()
        {
            var document = new Mises.Web.Controllers.DailyController();
            document.SetFakeControllerContext();

            var view = document.AlbumView();

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((List<DailyArticlesGetImageGalleryResult>)view.Model);
            Assert.IsTrue(model.Count > 0, "No daily articles");


            Assert.IsTrue(view.ViewBag.PageSize > 0);
            Assert.IsTrue(view.ViewBag.CurrentPage == 0);
            Assert.IsTrue(view.ViewBag.Total > 0);
        }

        [TestMethod]
        public void Get_AuthorArchives_Returns_AuthorList()
        {
            var document = new Mises.Web.Controllers.DailyController();
            document.SetFakeControllerContext();

            ViewResult view = document.AuthorList() as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            Assert.IsInstanceOfType(view.Model, typeof(List<DocumentAuthorsGetListResult>));
            
            var model = ((List<DocumentAuthorsGetListResult>)view.Model);
            Assert.IsTrue(model.Count > 0, "No authors");
        }

        [TestMethod]
        public void GetDailyReturnsArticle()
        {
            var document = new Mises.Web.Controllers.DailyController();
            document.SetFakeControllerContext();

            const int articleId = 5861;
            ViewResult view = document.Detail(articleId,null,null) as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((Mises.Domain.Articles.DailyArticle)view.Model);

            Assert.IsInstanceOfType(model, typeof(Mises.Domain.Articles.DailyArticle));

            Assert.IsTrue(articleId == model.ArticleId);

            Assert.IsTrue(model.Title == "Defending Kim Kardashian");

            Assert.IsFalse(string.IsNullOrWhiteSpace(view.ViewBag.Meta));


        }
    }
}
