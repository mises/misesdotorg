﻿using Mises.Data;
using Mises.Domain;
using Mises.Services;
using Mises.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Mises.Domain.Contracts;
using System.Web.Mvc;
using Mises.Web.ViewModels;
using Moq;

namespace Mises.SiteTests
{
    [TestClass()]
    public class HomeControllerTest
    {
        
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://local.mises.org")]
        public void Index_Returns_HomeViewModel()
        {
            IServiceLayer services = null; 
            HomeController target = new HomeController(services);
            target.SetFakeControllerContext();
            
            ViewResult result= target.Index() as ViewResult;
            Assert.IsInstanceOfType(result.Model, typeof(HomeViewModel));
            
        }
      
        [TestMethod]
        public void Home_GetRandomVideo_ReturnsVideo()
        {
            MediaTests.GetMappings();
            IServiceLayer services = new ServiceLayer(new InProcessMediaService(new MediaRepository(Data.DataHelper.ConnectionString),null,null)); 
            HomeController target = new HomeController(services);
            target.SetFakeControllerContext();

            VideoPodViewModel result = target.RandomVideo();

            Assert.IsTrue(result.Id > 0,"Failed to get random video");
        }

        [TestMethod]
        public void Home_GetRandomVideo_WithMoqReturnsVideo()
        {
            MediaTests.GetMappings();

            var mockRepository = new Mock<IMediaRepository>();
            mockRepository.Setup(m => m.GetRandomVideo()).Returns(new Document() {DocumentId = 66, Title = "Test"});
            
            IServiceLayer services = new ServiceLayer(new InProcessMediaService(mockRepository.Object, null, null));
            HomeController target = new HomeController(services);
            target.SetFakeControllerContext();

            VideoPodViewModel result = target.RandomVideo();

            Assert.AreEqual("Test", result.Title);
        }

    }
}
