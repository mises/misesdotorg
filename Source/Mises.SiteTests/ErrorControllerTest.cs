﻿using Mises.Web.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Mises.Domain.Contracts;
using System.Web.Mvc;
using Mises.Web.ViewModels;

namespace Mises.SiteTests
{
    [TestClass()]
    public class ErrorControllerTest
    {
       
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://local.mises.org")]
        public void Http404Test()
        {
            // /TRADCYCL/READINGS.ASP

            ErrorController target = new ErrorController();
            string aspxerrorpath = "";
            string requestedURL = "~/TRADCYCL/READINGS.ASP"; 
            ActionResult expected = null; 
            ActionResult actual;

            target.SetFakeControllerContext();
            target.HttpContext.Request.SetupRequestUrl(requestedURL);
            actual = target.Http404(aspxerrorpath, requestedURL);


            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
