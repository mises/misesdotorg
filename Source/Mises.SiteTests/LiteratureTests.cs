﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BookCatalog.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mises.SiteTests
{
    [TestClass]
    public  class LiteratureTests
    {

        [TestMethod]
        public void GetLiteratureReturnsMultipleDocuments()
        {
            var document = new Mises.Web.Controllers.LiteratureController();
            document.SetFakeControllerContext();

            var view = document.Index() as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((List<LiteratureViewModel>)view.Model);
            Assert.IsTrue(model.Count > 0, "No documents");

            Assert.IsTrue(view.ViewBag.PageSize > 0);
            Assert.IsTrue(view.ViewBag.CurrentPage== 0);
            Assert.IsTrue(view.ViewBag.Total > 0);
        }

        [TestMethod]
        public void Literature_Search_ReturnsMultipleDocuments()
        {
            var document = new Mises.Web.Controllers.LiteratureController();
            document.SetFakeControllerContext();

            var view = document.Search("apple") as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((List<LiteratureViewModel>)view.Model);
            Assert.IsTrue(model.Count > 0, "No documents");

            Assert.IsTrue(view.ViewBag.PageSize > 0);
            Assert.IsTrue(view.ViewBag.CurrentPage == 0);
            Assert.IsTrue(view.ViewBag.Total > 0);
        }


        [TestMethod]
        public void GetAuthorReturnsMultipleAuthors()
        {
            var document = new Mises.Web.Controllers.LiteratureController();
            document.SetFakeControllerContext();

            var view = document.Authors(0,0) as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((List<AuthorViewModel>)view.Model);
            Assert.IsTrue(model.Count > 0, "No authors");
        }

        [TestMethod]
        public void GetAuthorAlpha_Returns_Sorted_Documents()
        {
            var document = new Mises.Web.Controllers.LiteratureController();
            document.SetFakeControllerContext();

            var view = document.GetDocumentList(0,AuthorId:331,titleSort:true) as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((List<LiteratureViewModel>)view.Model);
            Assert.IsTrue(model.Count > 0, "No authors");

            Assert.IsTrue(model.First().Title[0] < model.Last().Title[0]);
        }

    }
}
