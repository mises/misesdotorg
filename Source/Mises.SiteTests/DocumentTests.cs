﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BookCatalog.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Mises.SiteTests
{
    [TestClass]
    public class DocumentTests
    {
      //  private readonly Mock<System.Web.HttpRequestBase> _request = new Mock<System.Web.HttpRequestBase>();

        //public static HttpContextBase GetMockHttpContext()
        //{
        //    var requestCookies = new Mock<HttpCookieCollection>();
        //    var request = new Mock<HttpRequestBase>();
        //    request.Setup(r => r.Cookies).Returns(requestCookies.Object);
        //    request.Setup(r => r.Url).Returns(new Uri("http://example.org"));
        //    var responseCookies = new Mock<HttpCookieCollection>();
        //    var response = new Mock<HttpResponseBase>();
        //    response.Setup(r => r.Cookies).Returns(responseCookies.Object);
        //    var context = new Mock<HttpContextBase>();
        //    context.Setup(ctx => ctx.Request).Returns(request.Object);
        //    context.Setup(ctx => ctx.Response).Returns(response.Object);
        //    context.Setup(ctx => ctx.Session).Returns(new Mock<HttpSessionStateBase>().Object);
        //    context.Setup(ctx => ctx.Server).Returns(new Mock<HttpServerUtilityBase>().Object);
        //    context.Setup(ctx => ctx.User).Returns(GetMockMembershipUser());
        //    context.Setup(ctx => ctx.User.Identity).Returns(context.Object.User.Identity);
        //    context.Setup(ctx => ctx.Response.Output).Returns(new StringWriter());
        //    return context.Object;
        //}

        //private static IPrincipal GetMockMembershipUser()
        //{
        //    return null;
        //}

        [TestMethod]
        public void GetDocumentReturnsDocument()
        {
            var document = new Mises.Web.Controllers.DocumentController();

            const int documentId = 6860; // Classical Liberalism and the Austrian School
            
            document.SetFakeControllerContext();
            document.HttpContext.Request.SetupRequestUrl("~/document/" + documentId.ToString());

            var view = document.View(documentId) as ViewResult;

            Assert.IsNotNull(view.Model,"The model is null");

            var model = ((LiteratureViewModel) view.Model);

            Assert.IsTrue(model.DocumentId == documentId);

            Assert.IsTrue(model.Title == "Classical Liberalism and the Austrian School");

            Assert.IsTrue(model.Formats.Count >0,"No formats");

        }
       

        [TestMethod]
        public void GetDocumentByAdminReturnsHiddenFiles()
        {
            //TODO: mock admin cookies
            //http://blog.paulhadfield.net/2010/09/mocking-httpcookiecollection-in.html

            Assert.Inconclusive("TODO");
        }

    }
}
