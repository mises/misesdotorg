﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.Domain.Payments;
using Mises.Domain.Utility;

namespace Mises.SiteTests
{
    [TestClass]
    public class DonationsTests
    {
        [TestMethod]
        public void RegistrationSuccessfull()
        {
            const int formId = 51;

            RegistrationForm myform = new RegistrationForm(formId) {EmailAddress = "heroic@gmail.com"};

            RegistrationPage regPage = new RegistrationPage
                {ContactInfo = new Controls_ContactInfo(), RegistrationForm = myform};


            string email = regPage.GenerateEmail();

            Debug.WriteLine(email);

            string subject = "TEST " + DateTime.Now.ToString();

            EmailHelper.SendMail("payments@freecapitalists.org", "heroic@gmail.com", subject,email);

            //int recordId =myform.SaveForm();
        }
    }
}
