﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BookCatalog.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.Data;
using Mises.Web.ViewModels;

namespace Mises.SiteTests
{
    [TestClass]
    public class FacultyControllerTests
    {
        [TestMethod]
        public void Get_Faculty_Returns_Model()
        {
            var document = new Mises.Web.Controllers.FacultyController();
            document.SetFakeControllerContext();
            document.Request.SetupRequestUrl("~/default.aspx");

            var view = document.Index() as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            Assert.IsInstanceOfType(view.Model, typeof(FacultyModel));
        }

   
    }
}
