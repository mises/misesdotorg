﻿using System;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.Domain.Feeds;
using Moq;

namespace Mises.SiteTests
{
    [TestClass]
    public class FeedTests
    {
        [TestMethod]
        public void MediaFeedReturnsRecords()
        {
            var document = new Mises.Web.Controllers.FeedController();
            document.SetFakeControllerContext();
            document.Request.SetupRequestUrl("~/Feed/media");
            RssActionResult view = document.Media() as RssActionResult;

            Assert.IsNotNull(view.Feed, "The model is null");

            var feed = (view.Feed);

            Assert.IsTrue(feed.Title.Text == "Mises Institute Media");

            Assert.AreEqual(feed.Items.Count(),50);

            Assert.IsInstanceOfType(feed, typeof(SyndicationFeed));

            Assert.IsTrue(feed.Items.ToList().Any());


        }

        [TestMethod]
        public void MediaFeedForCategoryReturnsRecords()
        {
            var document = new Mises.Web.Controllers.FeedController();
            document.SetFakeControllerContext();
            document.Request.SetupRequestUrl("~/Feed/media");
            RssActionResult view = document.Media(CategoryId: 247) as RssActionResult;

            Assert.IsNotNull(view.Feed, "The model is null");

            var feed = (view.Feed);

            Assert.AreEqual(feed.Title.Text, "Mises Media: Man, Economy, and State, with Power and Market");

            Assert.AreNotEqual(feed.Items.Count(), 50);

            Assert.IsInstanceOfType(feed, typeof(SyndicationFeed));

            Assert.IsTrue(feed.Items.ToList().Any());


        }

        [TestMethod]
        public void MediaFeedForAuthorReturnsRecords()
        {
            var document = new Mises.Web.Controllers.FeedController();
            document.SetFakeControllerContext();
            document.Request.SetupRequestUrl("~/Feed/media");
            RssActionResult view = document.Media(AuthorId: 299) as RssActionResult;

            Assert.IsNotNull(view.Feed, "The model is null");

            var feed = (view.Feed);

            //Assert.AreEqual(feed.Title.Text, "Mises Media: Man, Economy, and State, with Power and Market");

            Assert.IsInstanceOfType(feed, typeof(SyndicationFeed));

            Assert.IsTrue(feed.Items.ToList().Any());

            Assert.AreNotEqual(feed.Items.Count(), 50);


        }

        [TestMethod]
        public void MediaFeedIsOrderedByDescendingDate()
        {
            var document = new Mises.Web.Controllers.FeedController();
            document.SetFakeControllerContext();
            document.Request.SetupRequestUrl("~/Feed/media");
            RssActionResult view = document.Media() as RssActionResult;

            Assert.IsNotNull(view.Feed, "The model is null");

            var feed = (view.Feed);

            var first = feed.Items.First();
            var last = feed.Items.Last();

            Assert.IsTrue(first.PublishDate > last.PublishDate);


        }

        [TestMethod]
        public void NewProductsFeed_ReturnsBooks()
        {
            var document = new Mises.Web.Controllers.FeedController();
            document.SetFakeControllerContext();
            document.Request.SetupRequestUrl("~/Feed/newProducts");
            RssActionResult view = document.NewProducts() as RssActionResult;

            Assert.IsNotNull(view.Feed, "The model is null");

            var feed = (view.Feed);

            Assert.IsTrue(feed.Items.ToList().Any());

        }
    }
}
