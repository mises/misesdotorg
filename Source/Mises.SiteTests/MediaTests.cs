﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mises.Domain;
using Mises.Domain.Contracts;
using Mises.Services;
using Mises.Web.ViewModels;
using Mises.Web.ViewModels.Media;
using Moq;
using BrowseViewModel = Mises.Web.ViewModels.Authors.BrowseViewModel;

namespace Mises.SiteTests
{
    public class MediaTestsBase
    {
        // todo: this is redundant: use site version
        public static void GetMappings()
        {
            Mapper.CreateMap<PaginableResult<CategorySummaryDTO>, BrowseViewModel<CategorySummaryDTO>>();
            Mapper.CreateMap<PaginableResult<DocumentDTO>, BrowseViewModel<DocumentDTO>>();

            Mapper.CreateMap<DocumentDTO, VideoPodViewModel>();

            Mapper.CreateMap<AuthorDTO, Web.ViewModels.Authors.DetailViewModel>();
            Mapper.CreateMap<PaginableResult<AuthorDTO>, BrowseViewModel>();

            Mapper.CreateMap<PaginableResult<ArticleSummaryDTO>, Mises.Web.ViewModels.Articles.BrowseViewModel>();

            Mapper.CreateMap<VideoDTO, VideoPlayerViewModel>();
            Mapper.CreateMap<AudioDTO, AudioPlayerViewModel>();
            Mapper.CreateMap<LiteratureDTO, LiteratureViewerViewModel>();
            Mapper.CreateMap<DocumentDTO, Mises.Web.ViewModels.Media.DetailViewModel>();
            Mapper.CreateMap<PaginableResult<MisesDTO>, SearchViewModel>();
            Mapper.CreateMap<PaginableResult<DocumentDTO>, Mises.Web.ViewModels.Media.BrowseViewModel>();
            Mapper.CreateMap<PaginableResult<MisesDTO>, Mises.Web.ViewModels.Media.BrowseViewModel>();

            Mapper.CreateMap<Mises.Data.DocumentDTO, VideoPodViewModel>();
        }
    }

    [TestClass]
    public class MediaTests : MediaTestsBase
    {
        [TestMethod]
        public void MediaIndex_DefaultPage_ReturnsMediaFiles()
        {
            GetMappings();

            var services =
              new ServiceLayer(new InProcessMediaService(new MediaRepository(), new AuthorRepository(), null));    

            var document = new Mises.Web.Controllers.MediaController(services);
            document.SetFakeControllerContext();

            var view = document.Index3("", 0, "", 1) as ViewResult;
            Assert.IsNotNull(view.Model, "The model is null");
            var model = ((IndexViewModel)view.Model);

            Assert.IsTrue(model.Documents.Items.Count > 0);

        }

        [TestMethod]
        public void MediaIndex_DefaultPage_ReturnsAuthorsAndSubjects()
        {
            GetMappings();

            var services =
                new ServiceLayer(new InProcessMediaService(new MediaRepository(), new AuthorRepository(), null));
                

            var document = new Mises.Web.Controllers.MediaController(services);

            document.SetFakeControllerContext();

            var view = document.Index3("", 0, "", 1) as ViewResult;
            Assert.IsNotNull(view.Model, "The model is null");
            var model = ((IndexViewModel)view.Model);

            Assert.IsTrue(model.Authors.Count() > 300);

            Assert.IsTrue(model.Subjects.Count() > 30);

        }

        [TestMethod]
        public void MediaIndex_DefaultPage_ReturnsCategories()
        {
            GetMappings();
            var services =
              new ServiceLayer(new InProcessMediaService(new MediaRepository(), new AuthorRepository(), null));

            var document = new Mises.Web.Controllers.MediaController(services);
            document.SetFakeControllerContext();

            var view = document.Index3("", 0, "", 1) as ViewResult;
            Assert.IsNotNull(view.Model, "The model is null");
            var model = ((IndexViewModel)view.Model);

            Assert.IsTrue(model.Categories.Count() > 5);

        }

        [TestMethod]
        public void MediaIndex_DefaultPage_ReturnsFiveFiles()
        {
            GetMappings();

            var services =
              new ServiceLayer(new InProcessMediaService(new MediaRepository(), new AuthorRepository(), null));

            var document = new Mises.Web.Controllers.MediaController(services);
            document.SetFakeControllerContext();

            var view = document.Index3("", 0, "", 1) as ViewResult;
            Assert.IsNotNull(view.Model, "The model is null");
            var model = ((IndexViewModel)view.Model);

            Assert.AreEqual(model.Documents.Items.Count, 5);


        }

        //[TestMethod]
        //public void Some_Clever_Test()
        //{
        //    var querystring = new NameValueCollection { { "something", "got_value" } };
        //    var formvalues = new NameValueCollection { { "pageid", "1" } };
        //    ControllerContext context = MvcMockHelpers.GetControllerContext("GET", querystring, formvalues);
        //    var controller = GetController(context);
        //    ActionResult actionResult = controller.Index();
        //    var model = actionResult.Model<productpageviewmodel>();  // A nice helper I found somewhere
        //    Assert.That(model, Is.Not.Null); // Perhaps a more clever assert here...
        //}

        [TestMethod]
        public void GetMediaDetailReturnsFiles()
        {
            GetMappings();

            const int mediaId = 5702;
            var querystring = new NameValueCollection { { "d", "" } }; // document id

            var services =
              new ServiceLayer(new InProcessMediaService(new MediaRepository(), new AuthorRepository(), null));

            var document = new Mises.Web.Controllers.MediaController(services);
            document.ControllerContext = MvcMockHelpers.GetControllerContext("GET", querystring, new NameValueCollection { });


            var view = document.Detail(mediaId, 1) as ViewResult;

            Assert.IsNotNull(view.Model, "The model is null");

            var model = ((DetailViewModel)view.Model);

            const int documentId = 6024;
            Assert.IsTrue(model.Id == documentId);
            Assert.IsTrue(model.Title == "How 'Mainstream' Economics Miseducates About Money and the Fed");

            Assert.IsTrue(model.Media.Count > 0);



        }
    }
}
