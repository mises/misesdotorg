USE [AbleCommerceV2]
GO
/****** Object:  Table [dbo].[ac_Stores]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Stores](
	[StoreId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ApplicationName] [nvarchar](255) NOT NULL,
	[LoweredApplicationName] [nvarchar](255) NOT NULL,
	[LicenseKey] [varchar](max) NULL,
	[DefaultWarehouseId] [int] NULL,
	[NextOrderId] [int] NOT NULL,
	[OrderIdIncrement] [smallint] NOT NULL,
	[WeightUnitId] [smallint] NOT NULL,
	[MeasurementUnitId] [smallint] NOT NULL,
 CONSTRAINT [ac_Stores_PK] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Groups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Groups](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[IsReadOnly] [bit] NOT NULL,
 CONSTRAINT [ac_Groups_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Affiliates]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Affiliates](
	[AffiliateId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[PayeeName] [nvarchar](100) NULL,
	[FirstName] [nvarchar](30) NULL,
	[LastName] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[Province] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](15) NULL,
	[CountryCode] [char](2) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[MobileNumber] [nvarchar](50) NULL,
	[WebsiteUrl] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[CommissionRate] [decimal](9, 4) NOT NULL,
	[CommissionIsPercent] [bit] NOT NULL,
	[CommissionOnTotal] [bit] NOT NULL,
	[ReferralDays] [smallint] NOT NULL,
	[ReferralPeriodId] [tinyint] NOT NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [ac_Affiliates_PK] PRIMARY KEY CLUSTERED 
(
	[AffiliateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Users]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[LoweredUserName] [nvarchar](255) NOT NULL,
	[Email] [nvarchar](255) NULL,
	[LoweredEmail] [nvarchar](255) NULL,
	[AffiliateId] [int] NULL,
	[AffiliateReferralDate] [datetime] NULL,
	[PrimaryAddressId] [int] NULL,
	[PrimaryWishlistId] [int] NULL,
	[PayPalId] [nvarchar](50) NULL,
	[PasswordQuestion] [nvarchar](255) NULL,
	[PasswordAnswer] [nvarchar](255) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsAnonymous] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastActivityDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL,
	[LastPasswordChangedDate] [datetime] NULL,
	[LastLockoutDate] [datetime] NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NULL,
	[Comment] [nvarchar](max) NULL,
	[TaxExemptionType] [int] NOT NULL,
	[TaxExemptionReference] [nvarchar](200) NULL,
 CONSTRAINT [ac_Users_PK] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Wishlists]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Wishlists](
	[WishlistId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ViewPassword] [nvarchar](50) NULL,
	[IsPublic] [bit] NOT NULL,
	[ViewCode] [uniqueidentifier] NOT NULL,
 CONSTRAINT [ac_Wishlists_PK] PRIMARY KEY CLUSTERED 
(
	[WishlistId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Countries]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Countries](
	[CountryCode] [char](2) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[AddressFormat] [nvarchar](255) NULL,
 CONSTRAINT [ac_Countries_PK] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Warehouses]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Warehouses](
	[WarehouseId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[Province] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](15) NULL,
	[CountryCode] [char](2) NULL,
	[Phone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](255) NULL,
 CONSTRAINT [ac_Warehouses_PK] PRIMARY KEY CLUSTERED 
(
	[WarehouseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_TaxCodes]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_TaxCodes](
	[TaxCodeId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [ac_TaxCodes_PK] PRIMARY KEY CLUSTERED 
(
	[TaxCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_WrapGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_WrapGroups](
	[WrapGroupId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [ac_WrapGroups_PK] PRIMARY KEY CLUSTERED 
(
	[WrapGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Vendors]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Vendors](
	[VendorId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](255) NULL,
 CONSTRAINT [ac_Vendors_PK] PRIMARY KEY CLUSTERED 
(
	[VendorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Manufacturers]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Manufacturers](
	[ManufacturerId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [ac_Manufacturers_PK] PRIMARY KEY CLUSTERED 
(
	[ManufacturerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Products]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Price] [decimal](12, 4) NOT NULL,
	[CostOfGoods] [decimal](12, 4) NOT NULL,
	[MSRP] [decimal](12, 4) NOT NULL,
	[Weight] [decimal](12, 4) NOT NULL,
	[Length] [decimal](12, 4) NOT NULL,
	[Width] [decimal](12, 4) NOT NULL,
	[Height] [decimal](12, 4) NOT NULL,
	[ManufacturerId] [int] NULL,
	[Sku] [nvarchar](40) NULL,
	[ModelNumber] [nvarchar](40) NULL,
	[DisplayPage] [varchar](100) NULL,
	[TaxCodeId] [int] NULL,
	[ShippableId] [tinyint] NOT NULL,
	[WarehouseId] [int] NULL,
	[InventoryModeId] [tinyint] NOT NULL,
	[InStock] [int] NOT NULL,
	[InStockWarningLevel] [int] NOT NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[ThumbnailAltText] [nvarchar](255) NULL,
	[ImageUrl] [varchar](255) NULL,
	[ImageAltText] [nvarchar](255) NULL,
	[Summary] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ExtendedDescription] [nvarchar](max) NULL,
	[VendorId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[IsFeatured] [bit] NOT NULL,
	[IsProhibited] [bit] NOT NULL,
	[AllowReviews] [bit] NOT NULL,
	[AllowBackorder] [bit] NOT NULL,
	[WrapGroupId] [int] NULL,
	[ExcludeFromFeed] [bit] NOT NULL,
	[HtmlHead] [nvarchar](max) NULL,
	[DisablePurchase] [bit] NOT NULL,
	[MinQuantity] [smallint] NOT NULL,
	[MaxQuantity] [smallint] NOT NULL,
	[VisibilityId] [tinyint] NOT NULL,
	[Theme] [varchar](100) NULL,
	[IconUrl] [varchar](255) NULL,
	[IconAltText] [nvarchar](255) NULL,
	[IsGiftCertificate] [bit] NOT NULL,
	[UseVariablePrice] [bit] NOT NULL,
	[MinimumPrice] [decimal](10, 2) NULL,
	[MaximumPrice] [decimal](10, 2) NULL,
	[SearchKeywords] [nvarchar](max) NULL,
	[HidePrice] [bit] NOT NULL,
	[WebpageId] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[MetaDescription] [nvarchar](1000) NULL,
	[MetaKeywords] [nvarchar](1000) NULL,
	[GTIN] [nvarchar](30) NULL,
	[GoogleCategory] [nvarchar](300) NULL,
	[Condition] [nvarchar](20) NULL,
	[Gender] [nvarchar](10) NULL,
	[AgeGroup] [nvarchar](10) NULL,
	[Color] [nvarchar](20) NULL,
	[Size] [nvarchar](20) NULL,
	[AdwordsGrouping] [nvarchar](50) NULL,
	[AdwordsLabels] [nvarchar](256) NULL,
	[AdwordsRedirect] [nvarchar](256) NULL,
	[PublishFeedAsVariants] [bit] NOT NULL,
	[RowVersion] [int] NOT NULL,
	[OrderBy] [decimal](10, 2) NOT NULL,
	[AvailabilityDate] [datetime] NULL,
	[ExcludedDestination] [nvarchar](20) NULL,
	[EnableGroups] [bit] NOT NULL,
 CONSTRAINT [ac_Products_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_WishlistItems]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_WishlistItems](
	[WishlistItemId] [int] IDENTITY(1,1) NOT NULL,
	[WishlistId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[OptionList] [varchar](500) NULL,
	[Price] [decimal](12, 4) NULL,
	[LineMessage] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[Desired] [smallint] NOT NULL,
	[Received] [smallint] NOT NULL,
	[Priority] [tinyint] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[KitList] [varchar](500) NULL,
 CONSTRAINT [ac_WishlistItems_PK] PRIMARY KEY CLUSTERED 
(
	[WishlistItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_OrderStatuses]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderStatuses](
	[OrderStatusId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[DisplayName] [nvarchar](100) NULL,
	[InventoryActionId] [smallint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_OrderStatuses_PK] PRIMARY KEY CLUSTERED 
(
	[OrderStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Orders]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[OrderNumber] [int] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[StoreId] [int] NOT NULL,
	[UserId] [int] NULL,
	[AffiliateId] [int] NULL,
	[BillToFirstName] [nvarchar](30) NULL,
	[BillToLastName] [nvarchar](50) NULL,
	[BillToCompany] [nvarchar](50) NULL,
	[BillToAddress1] [nvarchar](255) NULL,
	[BillToAddress2] [nvarchar](255) NULL,
	[BillToCity] [nvarchar](50) NULL,
	[BillToProvince] [nvarchar](50) NULL,
	[BillToPostalCode] [nvarchar](15) NULL,
	[BillToCountryCode] [char](2) NULL,
	[BillToPhone] [nvarchar](50) NULL,
	[BillToFax] [nvarchar](50) NULL,
	[BillToEmail] [nvarchar](255) NULL,
	[ProductSubtotal] [decimal](12, 4) NOT NULL,
	[TotalCharges] [decimal](12, 4) NOT NULL,
	[TotalPayments] [decimal](12, 4) NOT NULL,
	[OrderStatusId] [int] NOT NULL,
	[Exported] [bit] NOT NULL,
	[RemoteIP] [varchar](39) NULL,
	[Referrer] [nvarchar](255) NULL,
	[GoogleOrderNumber] [nvarchar](50) NULL,
	[PaymentStatusId] [tinyint] NOT NULL,
	[ShipmentStatusId] [tinyint] NOT NULL,
	[TaxExemptionType] [int] NOT NULL,
	[TaxExemptionReference] [nvarchar](200) NULL,
 CONSTRAINT [ac_Orders_PK] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_ShipGateways]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_ShipGateways](
	[ShipGatewayId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ClassId] [varchar](255) NOT NULL,
	[ConfigData] [nvarchar](max) NULL,
	[ReCrypt] [bit] NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [ac_ShipGateways_PK] PRIMARY KEY CLUSTERED 
(
	[ShipGatewayId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_ShipMethods]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipMethods](
	[ShipMethodId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[ShipMethodTypeId] [smallint] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Surcharge] [decimal](12, 4) NOT NULL,
	[ShipGatewayId] [int] NULL,
	[ServiceCode] [nvarchar](255) NULL,
	[MinPurchase] [decimal](12, 4) NOT NULL,
	[SurchargeIsVisible] [bit] NOT NULL,
	[SurchargeIsPercent] [bit] NULL,
	[TaxCodeId] [int] NULL,
	[OrderBy] [smallint] NOT NULL,
	[SurchargeTaxCodeId] [int] NULL,
 CONSTRAINT [ac_ShipMethods_PK] PRIMARY KEY CLUSTERED 
(
	[ShipMethodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_OrderShipments]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderShipments](
	[OrderShipmentId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[WarehouseId] [int] NULL,
	[ShipToFirstName] [nvarchar](30) NULL,
	[ShipToLastName] [nvarchar](50) NULL,
	[ShipToCompany] [nvarchar](50) NULL,
	[ShipToAddress1] [nvarchar](255) NULL,
	[ShipToAddress2] [nvarchar](255) NULL,
	[ShipToCity] [nvarchar](50) NULL,
	[ShipToProvince] [nvarchar](50) NULL,
	[ShipToPostalCode] [nvarchar](15) NULL,
	[ShipToCountryCode] [nchar](2) NULL,
	[ShipToPhone] [nvarchar](50) NULL,
	[ShipToFax] [nvarchar](50) NULL,
	[ShipToEmail] [nvarchar](255) NULL,
	[ShipToResidence] [bit] NULL,
	[ShipMethodId] [int] NULL,
	[ShipMethodName] [nvarchar](255) NULL,
	[ShipMessage] [nvarchar](255) NULL,
	[ShipDate] [datetime] NULL,
 CONSTRAINT [ac_OrderShipments_PK] PRIMARY KEY CLUSTERED 
(
	[OrderShipmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_WrapStyles]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_WrapStyles](
	[WrapStyleId] [int] IDENTITY(1,1) NOT NULL,
	[WrapGroupId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[TaxCodeId] [int] NULL,
	[Price] [decimal](12, 4) NOT NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[ImageUrl] [varchar](255) NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_WrapStyles_PK] PRIMARY KEY CLUSTERED 
(
	[WrapStyleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_OrderItems]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_OrderItems](
	[OrderItemId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ParentItemId] [int] NULL,
	[OrderItemTypeId] [smallint] NOT NULL,
	[ShippableId] [tinyint] NOT NULL,
	[OrderShipmentId] [int] NULL,
	[ProductId] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[OptionList] [varchar](500) NULL,
	[VariantName] [nvarchar](255) NULL,
	[Sku] [nvarchar](100) NULL,
	[Price] [decimal](12, 4) NOT NULL,
	[Weight] [decimal](12, 4) NOT NULL,
	[CostOfGoods] [decimal](12, 4) NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[LineMessage] [nvarchar](500) NULL,
	[OrderBy] [smallint] NOT NULL,
	[GiftMessage] [nvarchar](500) NULL,
	[TaxCodeId] [int] NULL,
	[WrapStyleId] [int] NULL,
	[WishlistItemId] [int] NULL,
	[InventoryStatusId] [smallint] NOT NULL,
	[TaxRate] [decimal](12, 4) NOT NULL,
	[TaxAmount] [decimal](12, 4) NOT NULL,
	[KitList] [varchar](500) NULL,
	[CustomFields] [nvarchar](max) NULL,
 CONSTRAINT [ac_OrderItems_PK] PRIMARY KEY CLUSTERED 
(
	[OrderItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_SubscriptionPlans]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_SubscriptionPlans](
	[ProductId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[NumberOfPayments] [smallint] NOT NULL,
	[PaymentFrequency] [smallint] NOT NULL,
	[PaymentFrequencyUnitId] [tinyint] NOT NULL,
	[RecurringCharge] [decimal](9, 2) NOT NULL,
	[RecurringChargeSpecified] [bit] NOT NULL,
	[GroupId] [int] NULL,
	[TaxCodeId] [int] NULL,
 CONSTRAINT [ac_SubscriptionPlans_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Subscriptions]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Subscriptions](
	[SubscriptionId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[OrderItemId] [int] NULL,
	[TransactionId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[ExpirationDate] [datetime] NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [ac_Subscriptions_PK] PRIMARY KEY CLUSTERED 
(
	[SubscriptionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_UserGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_UserGroups](
	[UserId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[SubscriptionId] [int] NULL,
 CONSTRAINT [ac_UserGroups_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[acsp_MergeUserGroupMembers]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[acsp_MergeUserGroupMembers]
	@sourceGroupId int,
	@targetGroupId int
as
begin
	
	SELECT UG.UserId, UG.GroupId, S.SubscriptionId, S.ExpirationDate
	INTO #sourceGroup
	FROM ac_UserGroups AS UG LEFT JOIN ac_Subscriptions AS S
	ON UG.SubscriptionId = S.SubscriptionId
	WHERE UG.GroupId = @sourceGroupId;

	
	SELECT UG.UserId, UG.GroupId, S.SubscriptionId, S.ExpirationDate
	INTO #targetGroup
	FROM ac_UserGroups AS UG LEFT JOIN ac_Subscriptions AS S
	ON UG.SubscriptionId = S.SubscriptionId
	WHERE UG.GroupId = @targetGroupId;

	
	DELETE #sourceGroup
	FROM #sourceGroup INNER JOIN #targetGroup ON #sourceGroup.UserId = #targetGroup.UserId
	WHERE #sourceGroup.ExpirationDate IS NULL AND #targetGroup.ExpirationDate IS NULL;

	
	DELETE #sourceGroup
	FROM #sourceGroup INNER JOIN #targetGroup ON #sourceGroup.UserId = #targetGroup.UserId
	WHERE #sourceGroup.ExpirationDate IS NOT NULL 
	AND (#targetGroup.ExpirationDate IS NULL OR #sourceGroup.ExpirationDate < #targetGroup.ExpirationDate);

	
	DELETE #targetGroup
	FROM #targetGroup INNER JOIN #sourceGroup ON #targetGroup.UserId = #sourceGroup.UserId
	WHERE #targetGroup.ExpirationDate IS NOT NULL 
	AND (#sourceGroup.ExpirationDate IS NULL OR #targetGroup.ExpirationDate < #sourceGroup.ExpirationDate);

	
	DELETE FROM ac_UserGroups
	WHERE UserId NOT IN (SELECT UserId FROM #sourceGroup)
	AND GroupID = @sourceGroupId;

	
	DELETE FROM ac_UserGroups
	WHERE UserId NOT IN (SELECT UserId FROM #targetGroup)
	AND GroupID = @targetGroupId;

	
	UPDATE ac_UserGroups
	SET GroupId = @targetGroupId
	WHERE GroupId = @sourceGroupId;

	
	DROP TABLE #sourceGroup;
	DROP TABLE #targetGroup;
end
GO
/****** Object:  Table [dbo].[W2M_IntegraExportedOrders]    Script Date: 03/03/2015 22:05:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[W2M_IntegraExportedOrders](
	[W2M_IntegraExportedOrder] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[Exported] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[products]    Script Date: 03/03/2015 22:05:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[products](
	[sku] [nvarchar](50) NULL,
	[instock] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_space]    Script Date: 03/03/2015 22:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_space]
                @sortbyrows BIT  = 0
AS
  SET NOCOUNT  ON
  
  --http://www.sqlteam.com/forums/topic.asp?TOPIC_ID=53843
  --EXEC sp_space --show stats sorted by reserved space size
  --EXEC sp_space 1 --show stats sorted by row count
  SELECT CAST(OBJECT_NAME(id) AS VARCHAR(50)) AS name,
         SUM(CASE 
               WHEN indid < 2
               THEN ROWS
             END) AS ROWS,
         SUM(reserved) * 8                    AS reserved,
         SUM(dpages) * 8                      AS data,
         SUM(used - dpages) * 8               AS index_size,
         SUM(reserved - used) * 8             AS unused
  FROM     sysindexes WITH (NOLOCK)
  WHERE    indid IN (0,1,255)
           AND id > 100
  GROUP BY id WITH ROLLUP
  ORDER BY CASE 
             WHEN @sortbyrows = 1
             THEN SUM(CASE 
                        WHEN indid < 2
                        THEN ROWS
                      END)
             ELSE SUM(reserved) * 8
           END DESC
GO
/****** Object:  Table [dbo].[ac_Roles]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LoweredName] [nvarchar](100) NOT NULL,
 CONSTRAINT [ac_Roles_PK] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_PrintedCatalog]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_PrintedCatalog](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[Zip] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ac_PrintedCatelog] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_LanguageStrings]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_LanguageStrings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[ResourceName] [nvarchar](255) NULL,
	[Translation] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Languages]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Culture] [nvarchar](255) NULL,
	[IsActive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ReviewerProfiles]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ReviewerProfiles](
	[ReviewerProfileId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[Location] [nvarchar](50) NULL,
	[EmailVerified] [bit] NOT NULL,
	[EmailVerificationCode] [uniqueidentifier] NULL,
 CONSTRAINT [ac_ReviewerProfiles_PK] PRIMARY KEY CLUSTERED 
(
	[ReviewerProfileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[_Table_Sizes2]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_Table_Sizes2] @sortbyrows bit = 0
AS
SET NOCOUNT ON

-- http://www.databasejournal.com/features/mssql/article.php/3414111/Gathering-Space-Usage-Statistics.htm

BEGIN try  
DECLARE @table_name VARCHAR(500) ;  
DECLARE @schema_name VARCHAR(500) ;  
DECLARE @tab1 TABLE( 
        tablename VARCHAR (500) collate database_default 
,       schemaname VARCHAR(500) collate database_default 
);  
DECLARE  @temp_table TABLE (     
        tablename sysname 
,       row_count INT 
,       reserved VARCHAR(50) collate database_default 
,       data VARCHAR(50) collate database_default 
,       index_size VARCHAR(50) collate database_default 
,       unused VARCHAR(50) collate database_default  
);  

INSERT INTO @tab1  
SELECT t1.name 
,       t2.name  
FROM sys.tables t1  
INNER JOIN sys.schemas t2 ON ( t1.schema_id = t2.schema_id );    

DECLARE c1 CURSOR FOR  
SELECT t2.name + '.' + t1.name   
FROM sys.tables t1  
INNER JOIN sys.schemas t2 ON ( t1.schema_id = t2.schema_id );    

OPEN c1;  
FETCH NEXT FROM c1 INTO @table_name; 
WHILE @@FETCH_STATUS = 0  
BEGIN   
        SET @table_name = REPLACE(@table_name, '[','');  
        SET @table_name = REPLACE(@table_name, ']','');  

        -- make sure the object exists before calling sp_spacedused 
        IF EXISTS(SELECT OBJECT_ID FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(@table_name)) 
        BEGIN 
                INSERT INTO @temp_table EXEC sp_spaceused @table_name, false ; 
        END 
         
        FETCH NEXT FROM c1 INTO @table_name;  
END;  
CLOSE c1;  
DEALLOCATE c1;  
SELECT t1.* 
,       t2.schemaname  
FROM @temp_table t1  
INNER JOIN @tab1 t2 ON (t1.tablename = t2.tablename ) 
ORDER BY  row_count desc, schemaname,tablename; 
END try  
BEGIN catch  
SELECT -100 AS l1 
,       ERROR_NUMBER() AS tablename 
,       ERROR_SEVERITY() AS row_count 
,       ERROR_STATE() AS reserved 
,       ERROR_MESSAGE() AS data 
,       1 AS index_size, 1 AS unused, 1 AS schemaname  
END catch
GO
/****** Object:  StoredProcedure [dbo].[_Table_Sizes]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_Table_Sizes]
                @sortbyrows BIT  = 0
AS
  SET NOCOUNT  ON
  
  --http://www.sqlteam.com/forums/topic.asp?TOPIC_ID=53843
  --EXEC sp_space --show stats sorted by reserved space size
  --EXEC sp_space 1 --show stats sorted by row count
  SELECT CAST(OBJECT_NAME(id) AS VARCHAR(50)) AS name,
         SUM(CASE 
               WHEN indid < 2
               THEN ROWS
             END) AS ROWS,
         SUM(reserved) * 8                    AS reserved,
         SUM(dpages) * 8                      AS data,
         SUM(used - dpages) * 8               AS index_size,
         SUM(reserved - used) * 8             AS unused
  FROM     sysindexes WITH (NOLOCK)
  WHERE    indid IN (0,1,255)
           AND id > 100
  GROUP BY id WITH ROLLUP
  ORDER BY CASE 
             WHEN @sortbyrows = 1
             THEN SUM(CASE 
                        WHEN indid < 2
                        THEN ROWS
                      END)
             ELSE SUM(reserved) * 8
           END DESC
GO
/****** Object:  StoredProcedure [dbo].[_SearchAndReplace]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_SearchAndReplace](
                @SearchStr  NVARCHAR(100),
                @ReplaceStr NVARCHAR(100))
AS
  BEGIN
    -- Copyright © 2002 Narayana Vyas Kondreddi. All rights reserved.
    -- Purpose: To search all columns of all tables for a given search string and replace it with another string
    -- Written by: Narayana Vyas Kondreddi
    -- Site: http://vyaskn.tripod.com
    -- Tested on: SQL Server 7.0 and SQL Server 2000
    -- Date modified: 2nd November 2002 13:50 GMT
    SET NOCOUNT  ON
    
    DECLARE  @TableName  NVARCHAR(256),
             @ColumnName NVARCHAR(128),
             @SearchStr2 NVARCHAR(110),
             @SQL        NVARCHAR(4000),
             @RCTR       INT
    
    SET @TableName = ''
    
    SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%','''')
    
    SET @RCTR = 0
    
    WHILE @TableName IS NOT NULL
      BEGIN
        SET @ColumnName = ''
        
        SET @TableName = (SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
                          FROM   INFORMATION_SCHEMA.TABLES
                          WHERE  TABLE_TYPE = 'BASE TABLE'
                                 AND QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName
                                 AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)),
                                                    'IsMSShipped') = 0)
        
        WHILE (@TableName IS NOT NULL)
              AND (@ColumnName IS NOT NULL)
          BEGIN
            SET @ColumnName = (SELECT MIN(QUOTENAME(COLUMN_NAME))
                               FROM   INFORMATION_SCHEMA.COLUMNS
                               WHERE  TABLE_SCHEMA = PARSENAME(@TableName,2)
                                      AND TABLE_NAME = PARSENAME(@TableName,1)
                                      AND DATA_TYPE IN ('char','varchar','nchar','nvarchar')
                                      AND QUOTENAME(COLUMN_NAME) > @ColumnName)
            
            IF @ColumnName IS NOT NULL
              BEGIN
                SET @SQL = 'UPDATE ' + @TableName + ' SET ' + @ColumnName + ' =  REPLACE(' + @ColumnName + ', ' + QUOTENAME(@SearchStr,'''') + ', ' + QUOTENAME(@ReplaceStr,'''') + ') WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2
                
                EXEC( @SQL)
                
                SET @RCTR = @RCTR + @@ROWCOUNT
              END
          END
      END
    
    SELECT 'Replaced ' + CAST(@RCTR AS VARCHAR) + ' occurence(s)' AS 'Outcome'
  END
GO
/****** Object:  Table [dbo].[ac_Options]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Options](
	[OptionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ShowThumbnails] [bit] NOT NULL,
	[ThumbnailColumns] [tinyint] NOT NULL,
	[ThumbnailWidth] [smallint] NOT NULL,
	[ThumbnailHeight] [smallint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[HeaderText] [nvarchar](100) NULL,
 CONSTRAINT [ac_ProductAttributes_PK] PRIMARY KEY CLUSTERED 
(
	[OptionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_OptionChoices]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_OptionChoices](
	[OptionChoiceId] [int] IDENTITY(1,1) NOT NULL,
	[OptionId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[ImageUrl] [varchar](255) NULL,
	[PriceModifier] [decimal](10, 2) NULL,
	[CogsModifier] [decimal](10, 2) NULL,
	[WeightModifier] [decimal](10, 2) NULL,
	[SkuModifier] [nvarchar](255) NULL,
	[OrderBy] [smallint] NOT NULL,
	[Selected] [bit] NOT NULL,
 CONSTRAINT [ac_AttributeOptions_PK] PRIMARY KEY CLUSTERED 
(
	[OptionChoiceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Links]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Links](
	[LinkId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[TargetUrl] [varchar](255) NOT NULL,
	[TargetWindow] [varchar](40) NULL,
	[Summary] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[ThumbnailAltText] [nvarchar](255) NULL,
	[DisplayPage] [varchar](100) NULL,
	[Theme] [varchar](100) NULL,
	[HtmlHead] [nvarchar](max) NULL,
	[VisibilityId] [tinyint] NOT NULL,
	[MetaDescription] [nvarchar](1000) NULL,
	[MetaKeywords] [nvarchar](1000) NULL,
 CONSTRAINT [ac_Links_PK] PRIMARY KEY CLUSTERED 
(
	[LinkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_LicenseAgreements]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_LicenseAgreements](
	[LicenseAgreementId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[AgreementText] [nvarchar](max) NOT NULL,
	[IsHTML] [bit] NOT NULL,
 CONSTRAINT [ac_LicenseAgreements_PK] PRIMARY KEY CLUSTERED 
(
	[LicenseAgreementId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_KitComponents]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_KitComponents](
	[KitComponentId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[UserPrompt] [nvarchar](255) NULL,
	[InputTypeId] [smallint] NOT NULL,
	[Columns] [tinyint] NOT NULL,
	[HeaderOption] [nvarchar](100) NULL,
 CONSTRAINT [ac_KitComponents_PK] PRIMARY KEY CLUSTERED 
(
	[KitComponentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ErrorMessages]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ErrorMessages](
	[ErrorMessageId] [int] IDENTITY(1,1) NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[StoreId] [int] NOT NULL,
	[MessageSeverityId] [tinyint] NOT NULL,
	[Text] [nvarchar](500) NOT NULL,
	[DebugData] [nvarchar](max) NULL,
 CONSTRAINT [ac_ErrorMessages_PK] PRIMARY KEY CLUSTERED 
(
	[ErrorMessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_EmailTemplates]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_EmailTemplates](
	[EmailTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ToAddress] [nvarchar](255) NOT NULL,
	[FromAddress] [nvarchar](255) NOT NULL,
	[ReplyToAddress] [nvarchar](255) NULL,
	[CCList] [nvarchar](255) NULL,
	[BCCList] [nvarchar](255) NULL,
	[Subject] [nvarchar](255) NULL,
	[IsHTML] [bit] NOT NULL,
	[ContentFileName] [nvarchar](100) NULL,
 CONSTRAINT [ac_EmailTemplates_PK] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_EmailLists]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_EmailLists](
	[EmailListId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[IsPublic] [bit] NOT NULL,
	[SignupRuleId] [smallint] NOT NULL,
	[SignupEmailTemplateId] [int] NULL,
	[LastSendDate] [datetime] NULL,
 CONSTRAINT [ac_EmailLists_PK] PRIMARY KEY CLUSTERED 
(
	[EmailListId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Coupons]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Coupons](
	[CouponId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[CouponTypeId] [tinyint] NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CouponCode] [nvarchar](50) NOT NULL,
	[DiscountAmount] [decimal](12, 4) NOT NULL,
	[IsPercent] [bit] NOT NULL,
	[MaxValue] [decimal](12, 4) NOT NULL,
	[MinPurchase] [decimal](12, 4) NOT NULL,
	[MinQuantity] [smallint] NOT NULL,
	[MaxQuantity] [smallint] NOT NULL,
	[QuantityInterval] [smallint] NOT NULL,
	[MaxUses] [smallint] NOT NULL,
	[MaxUsesPerCustomer] [smallint] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[ProductRuleId] [tinyint] NOT NULL,
	[AllowCombine] [bit] NOT NULL,
 CONSTRAINT [ac_Coupons_PK] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CustomUrls]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CustomUrls](
	[CustomUrlId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[CatalogNodeId] [int] NOT NULL,
	[CatalogNodeTypeId] [tinyint] NOT NULL,
	[Url] [nvarchar](300) NOT NULL,
	[LoweredUrl] [nvarchar](300) NOT NULL,
 CONSTRAINT [ac_CustomUrls_PK] PRIMARY KEY NONCLUSTERED 
(
	[CustomUrlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CustomFields]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CustomFields](
	[CustomFieldId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[TableName] [nvarchar](50) NOT NULL,
	[ForeignKeyId] [int] NOT NULL,
	[FieldName] [nvarchar](255) NOT NULL,
	[FieldValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_CustomFields_PK] PRIMARY KEY CLUSTERED 
(
	[CustomFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Currencies]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Currencies](
	[CurrencyId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CurrencySymbol] [nvarchar](40) NULL,
	[DecimalDigits] [int] NOT NULL,
	[DecimalSeparator] [nvarchar](4) NULL,
	[GroupSeparator] [nvarchar](4) NULL,
	[GroupSizes] [varchar](8) NULL,
	[NegativePattern] [tinyint] NOT NULL,
	[NegativeSign] [varchar](4) NULL,
	[PositivePattern] [tinyint] NOT NULL,
	[ISOCode] [varchar](3) NOT NULL,
	[ISOCodePattern] [tinyint] NULL,
	[ExchangeRate] [decimal](12, 4) NOT NULL,
	[AutoUpdate] [bit] NOT NULL,
	[LastUpdate] [datetime] NULL,
 CONSTRAINT [ac_Currencies_PK] PRIMARY KEY CLUSTERED 
(
	[CurrencyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_BannedIPs]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_BannedIPs](
	[BannedIPId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[IPRangeStart] [bigint] NOT NULL,
	[IPRangeEnd] [bigint] NOT NULL,
	[Comment] [nvarchar](100) NULL,
 CONSTRAINT [ac_BannedIPs_PK] PRIMARY KEY CLUSTERED 
(
	[BannedIPId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Categories]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Categories](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Summary] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[ThumbnailAltText] [nvarchar](255) NULL,
	[HtmlHead] [nvarchar](max) NULL,
	[VisibilityId] [tinyint] NOT NULL,
	[WebpageId] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[MetaDescription] [nvarchar](1000) NULL,
	[MetaKeywords] [nvarchar](1000) NULL,
 CONSTRAINT [ac_Categories_PK] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Redirects]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Redirects](
	[RedirectId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[SourceUrl] [nvarchar](255) NOT NULL,
	[LoweredSourceUrl] [nvarchar](255) NOT NULL,
	[TargetUrl] [nvarchar](255) NOT NULL,
	[UseRegEx] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastVisitedDate] [datetime] NULL,
	[VisitCount] [int] NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_Redirects_PK] PRIMARY KEY NONCLUSTERED 
(
	[RedirectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Readmes]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Readmes](
	[ReadmeId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[ReadmeText] [nvarchar](max) NOT NULL,
	[IsHTML] [bit] NOT NULL,
 CONSTRAINT [ac_Readmes_PK] PRIMARY KEY CLUSTERED 
(
	[ReadmeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductTemplates]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductTemplates](
	[ProductTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [ac_ProductTemplates_PK] PRIMARY KEY CLUSTERED 
(
	[ProductTemplateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_PaymentGateways]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_PaymentGateways](
	[PaymentGatewayId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ClassId] [varchar](255) NOT NULL,
	[ConfigData] [nvarchar](max) NULL,
	[ReCrypt] [bit] NOT NULL,
 CONSTRAINT [ac_PaymentGatways_PK] PRIMARY KEY CLUSTERED 
(
	[PaymentGatewayId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_PaymentMethods]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_PaymentMethods](
	[PaymentMethodId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[PaymentInstrumentId] [smallint] NOT NULL,
	[PaymentGatewayId] [int] NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_PaymentMethods_PK] PRIMARY KEY CLUSTERED 
(
	[PaymentMethodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Payments]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Payments](
	[PaymentId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[SubscriptionId] [int] NULL,
	[PaymentMethodId] [int] NULL,
	[PaymentMethodName] [nvarchar](100) NULL,
	[ReferenceNumber] [nvarchar](50) NULL,
	[Amount] [decimal](12, 4) NOT NULL,
	[CurrencyCode] [nvarchar](3) NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[PaymentStatusId] [smallint] NOT NULL,
	[PaymentStatusReason] [nvarchar](255) NULL,
	[CompletedDate] [datetime] NULL,
	[EncryptedAccountData] [nvarchar](max) NULL,
	[ReCrypt] [bit] NOT NULL,
 CONSTRAINT [ac_Payments_PK] PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Transactions]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Transactions](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionTypeId] [smallint] NOT NULL,
	[PaymentId] [int] NOT NULL,
	[PaymentGatewayId] [int] NULL,
	[ProviderTransactionId] [nvarchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Amount] [decimal](12, 4) NOT NULL,
	[TransactionStatusId] [smallint] NOT NULL,
	[ResponseCode] [nvarchar](50) NULL,
	[ResponseMessage] [nvarchar](255) NULL,
	[AuthorizationCode] [nvarchar](255) NULL,
	[AVSResultCode] [char](1) NULL,
	[CVVResultCode] [char](1) NULL,
	[CAVResultCode] [char](1) NULL,
	[RemoteIP] [varchar](39) NULL,
	[Referrer] [nvarchar](255) NULL,
	[AdditionalData] [nvarchar](255) NULL,
 CONSTRAINT [ac_Transactions_PK] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_VolumeDiscounts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_VolumeDiscounts](
	[VolumeDiscountId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[IsValueBased] [bit] NOT NULL,
	[IsGlobal] [bit] NOT NULL,
 CONSTRAINT [ac_VolumeDiscounts_PK] PRIMARY KEY CLUSTERED 
(
	[VolumeDiscountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Webpages]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Webpages](
	[WebpageId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Summary] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[ThumbnailAltText] [nvarchar](255) NULL,
	[Theme] [varchar](100) NULL,
	[HtmlHead] [nvarchar](max) NULL,
	[VisibilityId] [tinyint] NOT NULL,
	[Layout] [varchar](100) NULL,
	[WebpageTypeId] [tinyint] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[MetaDescription] [nvarchar](1000) NULL,
	[MetaKeywords] [nvarchar](1000) NULL,
 CONSTRAINT [ac_Webpages_PK] PRIMARY KEY CLUSTERED 
(
	[WebpageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_StoreSettings]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_StoreSettings](
	[StoreSettingId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[FieldName] [nvarchar](255) NOT NULL,
	[FieldValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_StoreSettings_PK] PRIMARY KEY CLUSTERED 
(
	[StoreSettingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipZones]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipZones](
	[ShipZoneId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[PostalCodeFilter] [nvarchar](450) NULL,
	[CountryRuleId] [tinyint] NOT NULL,
	[ProvinceRuleId] [tinyint] NOT NULL,
	[ExcludePostalCodeFilter] [nvarchar](500) NULL,
	[UsePCPM] [bit] NOT NULL,
 CONSTRAINT [ac_ShipZones_PK] PRIMARY KEY CLUSTERED 
(
	[ShipZoneId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_TaxGateways]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_TaxGateways](
	[TaxGatewayId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ClassId] [varchar](255) NOT NULL,
	[ConfigData] [nvarchar](max) NULL,
	[ReCrypt] [bit] NOT NULL,
 CONSTRAINT [ac_TaxGateways_PK] PRIMARY KEY CLUSTERED 
(
	[TaxGatewayId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_TaxRules]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_TaxRules](
	[TaxRuleId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[TaxRate] [decimal](9, 4) NOT NULL,
	[UseBillingAddress] [bit] NOT NULL,
	[GroupRuleId] [tinyint] NOT NULL,
	[TaxCodeId] [int] NULL,
	[Priority] [smallint] NOT NULL,
	[RoundingRuleId] [tinyint] NOT NULL,
	[UsePerItemTax] [bit] NOT NULL,
 CONSTRAINT [ac_TaxRules_PK] PRIMARY KEY CLUSTERED 
(
	[TaxRuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_TaxRuleGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_TaxRuleGroups](
	[TaxRuleId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_TaxRuleGroups_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[TaxRuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipZoneCountries]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_ShipZoneCountries](
	[ShipZoneId] [int] NOT NULL,
	[CountryCode] [char](2) NOT NULL,
 CONSTRAINT [ac_ShipZoneCountries_PK] PRIMARY KEY CLUSTERED 
(
	[ShipZoneId] ASC,
	[CountryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_VolumeDiscountLevels]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_VolumeDiscountLevels](
	[VolumeDiscountLevelId] [int] IDENTITY(1,1) NOT NULL,
	[VolumeDiscountId] [int] NOT NULL,
	[MinValue] [decimal](12, 4) NOT NULL,
	[MaxValue] [decimal](12, 4) NOT NULL,
	[DiscountAmount] [decimal](12, 4) NOT NULL,
	[IsPercent] [bit] NOT NULL,
 CONSTRAINT [ac_VolumeDiscountLevels_PK] PRIMARY KEY CLUSTERED 
(
	[VolumeDiscountLevelId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_VolumeDiscountGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_VolumeDiscountGroups](
	[VolumeDiscountId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_VolumeDiscountGroups_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[VolumeDiscountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_TaxRuleTaxCodes]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_TaxRuleTaxCodes](
	[TaxRuleId] [int] NOT NULL,
	[TaxCodeId] [int] NOT NULL,
 CONSTRAINT [ac_TaxRuleTaxCodes_PK] PRIMARY KEY CLUSTERED 
(
	[TaxRuleId] ASC,
	[TaxCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_TaxRuleShipZones]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_TaxRuleShipZones](
	[TaxRuleId] [int] NOT NULL,
	[ShipZoneId] [int] NOT NULL,
 CONSTRAINT [ac_TaxRuleShipZones_PK] PRIMARY KEY CLUSTERED 
(
	[TaxRuleId] ASC,
	[ShipZoneId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_VendorGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_VendorGroups](
	[VendorId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_VendorGroups_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[VendorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_KitProducts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_KitProducts](
	[KitProductId] [int] IDENTITY(1,1) NOT NULL,
	[KitComponentId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[OptionList] [varchar](255) NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Price] [decimal](12, 4) NOT NULL,
	[PriceModeId] [tinyint] NOT NULL,
	[Weight] [decimal](12, 4) NOT NULL,
	[WeightModeId] [tinyint] NOT NULL,
	[IsSelected] [bit] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_KitProducts_PK] PRIMARY KEY CLUSTERED 
(
	[KitProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_OrderStatusTriggers]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderStatusTriggers](
	[StoreEventId] [int] NOT NULL,
	[OrderStatusId] [int] NOT NULL,
 CONSTRAINT [ac_OrderStatusTriggers_PK] PRIMARY KEY CLUSTERED 
(
	[StoreEventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Provinces]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Provinces](
	[ProvinceId] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [char](2) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LoweredName] [nvarchar](50) NOT NULL,
	[ProvinceCode] [nvarchar](10) NULL,
 CONSTRAINT [ac_Provinces_PK] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_CatalogNodes]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CatalogNodes](
	[CategoryId] [int] NOT NULL,
	[CatalogNodeId] [int] NOT NULL,
	[CatalogNodeTypeId] [tinyint] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [ac_CatalogNodes_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[_CleanupOldData]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_CleanupOldData] 
	
AS
BEGIN
	
	
	truncate table ac_ErrorMessages
	
END
GO
/****** Object:  Table [dbo].[ac_CategoryVolumeDiscounts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CategoryVolumeDiscounts](
	[CategoryId] [int] NOT NULL,
	[VolumeDiscountId] [int] NOT NULL,
 CONSTRAINT [ac_CategoryVolumeDiscounts_PK] PRIMARY KEY NONCLUSTERED 
(
	[CategoryId] ASC,
	[VolumeDiscountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CategoryParents]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CategoryParents](
	[CategoryId] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[ParentLevel] [tinyint] NOT NULL,
	[ParentNumber] [tinyint] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [ac_CategoryParents_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CouponGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CouponGroups](
	[CouponId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_CouponGroups_PK] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CouponCombos]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CouponCombos](
	[CouponId] [int] NOT NULL,
	[ComboCouponId] [int] NOT NULL,
 CONSTRAINT [ac_CouponCombos_PK] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[ComboCouponId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_DigitalGoods]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_DigitalGoods](
	[DigitalGoodId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileSize] [bigint] NOT NULL,
	[MaxDownloads] [tinyint] NULL,
	[DownloadTimeout] [nvarchar](15) NULL,
	[ActivationTimeout] [nvarchar](15) NULL,
	[MediaKey] [nvarchar](255) NULL,
	[ServerFileName] [varchar](255) NULL,
	[SerialKeyProviderId] [varchar](255) NULL,
	[SerialKeyConfigData] [nvarchar](max) NULL,
	[ActivationModeId] [tinyint] NOT NULL,
	[ActivationEmailId] [int] NULL,
	[FulfillmentEmailId] [int] NULL,
	[ReadmeId] [int] NULL,
	[LicenseAgreementId] [int] NULL,
	[LicenseAgreementModeId] [tinyint] NOT NULL,
	[FulfillmentModeId] [tinyint] NOT NULL,
	[EnableSerialKeys] [bit] NOT NULL,
 CONSTRAINT [ac_DigitalGoods_PK] PRIMARY KEY CLUSTERED 
(
	[DigitalGoodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_EmailListUsers]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_EmailListUsers](
	[EmailListId] [int] NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[SignupDate] [datetime] NOT NULL,
	[SignupIP] [varchar](39) NULL,
	[LastSendDate] [datetime] NULL,
	[FailureCount] [smallint] NOT NULL,
 CONSTRAINT [ac_EmailListUsers_PK] PRIMARY KEY CLUSTERED 
(
	[EmailListId] ASC,
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_EmailListSignups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_EmailListSignups](
	[EmailListSignupId] [int] IDENTITY(1,1) NOT NULL,
	[EmailListId] [int] NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[SignupDate] [datetime] NOT NULL,
 CONSTRAINT [ac_EmailListSignups_PK] PRIMARY KEY CLUSTERED 
(
	[EmailListSignupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_EmailTemplateTriggers]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_EmailTemplateTriggers](
	[EmailTemplateId] [int] NOT NULL,
	[StoreEventId] [int] NOT NULL,
 CONSTRAINT [ac_EmailTemplateTriggers_PK] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateId] ASC,
	[StoreEventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_GroupRoles]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_GroupRoles](
	[GroupId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [ac_GroupRoles_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_InputFields]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_InputFields](
	[InputFieldId] [int] IDENTITY(1,1) NOT NULL,
	[ProductTemplateId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[UserPrompt] [nvarchar](255) NOT NULL,
	[InputTypeId] [smallint] NOT NULL,
	[Rows] [tinyint] NOT NULL,
	[Columns] [tinyint] NOT NULL,
	[MaxLength] [smallint] NOT NULL,
	[IsRequired] [bit] NOT NULL,
	[RequiredMessage] [nvarchar](max) NULL,
	[IsMerchantField] [bit] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
	[PersistWithOrder] [bit] NOT NULL,
	[PromptCssClass] [nvarchar](100) NULL,
	[ControlCssClass] [nvarchar](100) NULL,
	[AdditionalData] [nvarchar](255) NULL,
 CONSTRAINT [ac_InputFields_PK] PRIMARY KEY CLUSTERED 
(
	[InputFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_OrderStatusEmails]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderStatusEmails](
	[OrderStatusId] [int] NOT NULL,
	[EmailTemplateId] [int] NOT NULL,
 CONSTRAINT [ac_OrderStatusEmails_PK] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateId] ASC,
	[OrderStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_OrderStatusActions]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderStatusActions](
	[OrderStatusId] [int] NOT NULL,
	[OrderActionId] [smallint] NOT NULL,
 CONSTRAINT [ac_OrderStatusActions_PK] PRIMARY KEY CLUSTERED 
(
	[OrderActionId] ASC,
	[OrderStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_InputChoices]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_InputChoices](
	[InputChoiceId] [int] IDENTITY(1,1) NOT NULL,
	[InputFieldId] [int] NOT NULL,
	[ChoiceText] [nvarchar](100) NOT NULL,
	[ChoiceValue] [nvarchar](100) NULL,
	[IsSelected] [bit] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_InputChoices_PK] PRIMARY KEY CLUSTERED 
(
	[InputChoiceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CouponShipMethods]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CouponShipMethods](
	[CouponId] [int] NOT NULL,
	[ShipMethodId] [int] NOT NULL,
 CONSTRAINT [ac_CouponShipMethods_PK] PRIMARY KEY CLUSTERED 
(
	[ShipMethodId] ASC,
	[CouponId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipMethodGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipMethodGroups](
	[ShipMethodId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_ShipMethodGroups_PK] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[ShipMethodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_PaymentMethodGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_PaymentMethodGroups](
	[PaymentMethodId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_PaymentMethodGroups_PK] PRIMARY KEY CLUSTERED 
(
	[PaymentMethodId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipRateMatrix]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipRateMatrix](
	[ShipRateMatrixId] [int] IDENTITY(1,1) NOT NULL,
	[ShipMethodId] [int] NOT NULL,
	[RangeStart] [decimal](12, 4) NULL,
	[RangeEnd] [decimal](12, 4) NULL,
	[Rate] [decimal](12, 4) NOT NULL,
	[IsPercent] [bit] NOT NULL,
 CONSTRAINT [ac_ShipRateMatrix_PK] PRIMARY KEY CLUSTERED 
(
	[ShipRateMatrixId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipMethodWarehouses]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipMethodWarehouses](
	[ShipMethodId] [int] NOT NULL,
	[WarehouseId] [int] NOT NULL,
 CONSTRAINT [ac_ShipMethodWarehouses_PK] PRIMARY KEY CLUSTERED 
(
	[ShipMethodId] ASC,
	[WarehouseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipMethodShipZones]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipMethodShipZones](
	[ShipMethodId] [int] NOT NULL,
	[ShipZoneId] [int] NOT NULL,
 CONSTRAINT [ac_ShipMethodShipZones_PK] PRIMARY KEY CLUSTERED 
(
	[ShipMethodId] ASC,
	[ShipZoneId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_SerialKeys]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_SerialKeys](
	[SerialKeyId] [int] IDENTITY(1,1) NOT NULL,
	[DigitalGoodId] [int] NOT NULL,
	[SerialKeyData] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_LicenseKeys_PK] PRIMARY KEY CLUSTERED 
(
	[SerialKeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ShipZoneProvinces]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ShipZoneProvinces](
	[ProvinceId] [int] NOT NULL,
	[ShipZoneId] [int] NOT NULL,
 CONSTRAINT [ac_ShipZoneProvinces_PK] PRIMARY KEY CLUSTERED 
(
	[ProvinceId] ASC,
	[ShipZoneId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ReviewReminders]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ReviewReminders](
	[ReviewReminderId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReviewReminderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_SearchHistory]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_SearchHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SearchTerm] [nvarchar](200) NOT NULL,
	[UserId] [int] NULL,
	[SearchDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_UserPasswords]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_UserPasswords](
	[UserId] [int] NOT NULL,
	[PasswordNumber] [tinyint] NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[PasswordFormat] [nvarchar](10) NULL,
	[CreateDate] [datetime] NOT NULL,
	[ForceExpiration] [bit] NOT NULL,
 CONSTRAINT [ac_UserPasswords_PK] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[PasswordNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_UserSettings]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_UserSettings](
	[UserSettingId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FieldName] [nvarchar](255) NOT NULL,
	[FieldValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_UserSettings_PK] PRIMARY KEY CLUSTERED 
(
	[UserSettingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductTemplateFields]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductTemplateFields](
	[ProductTemplateFieldId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[InputFieldId] [int] NOT NULL,
	[InputValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_ProductTemplateFields_PK] PRIMARY KEY CLUSTERED 
(
	[ProductTemplateFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_PageViews]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_PageViews](
	[PageViewId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[ActivityDate] [datetime] NOT NULL,
	[RemoteIP] [varchar](39) NOT NULL,
	[RequestMethod] [nvarchar](4) NOT NULL,
	[UserId] [int] NOT NULL,
	[UriStem] [nvarchar](255) NOT NULL,
	[UriQuery] [nvarchar](255) NULL,
	[TimeTaken] [int] NOT NULL,
	[UserAgent] [nvarchar](255) NULL,
	[Referrer] [nvarchar](255) NULL,
	[CatalogNodeId] [int] NULL,
	[CatalogNodeTypeId] [smallint] NULL,
	[Browser] [nvarchar](150) NOT NULL,
	[BrowserName] [nvarchar](100) NOT NULL,
	[BrowserPlatform] [nvarchar](40) NOT NULL,
	[BrowserVersion] [nvarchar](40) NOT NULL,
	[AffiliateId] [int] NOT NULL,
 CONSTRAINT [ac_PageViews_PK] PRIMARY KEY CLUSTERED 
(
	[PageViewId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Kits]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Kits](
	[ProductId] [int] NOT NULL,
	[ItemizeDisplay] [bit] NOT NULL,
 CONSTRAINT [ac_Kits_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductVolumeDiscounts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductVolumeDiscounts](
	[ProductId] [int] NOT NULL,
	[VolumeDiscountId] [int] NOT NULL,
 CONSTRAINT [ac_ProductVolumeDiscounts_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[VolumeDiscountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductVariants]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_ProductVariants](
	[ProductVariantId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Option1] [int] NOT NULL,
	[Option2] [int] NOT NULL,
	[Option3] [int] NOT NULL,
	[Option4] [int] NOT NULL,
	[Option5] [int] NOT NULL,
	[Option6] [int] NOT NULL,
	[Option7] [int] NOT NULL,
	[Option8] [int] NOT NULL,
	[VariantName] [nvarchar](255) NULL,
	[Sku] [nvarchar](100) NULL,
	[Price] [decimal](12, 4) NULL,
	[PriceModeId] [tinyint] NOT NULL,
	[Weight] [decimal](12, 4) NULL,
	[WeightModeId] [tinyint] NOT NULL,
	[CostOfGoods] [decimal](12, 4) NULL,
	[InStock] [int] NOT NULL,
	[InStockWarningLevel] [int] NOT NULL,
	[Available] [bit] NOT NULL,
	[RowVersion] [int] NOT NULL,
	[GTIN] [nvarchar](30) NULL,
	[ImageUrl] [varchar](255) NULL,
	[ThumbnailUrl] [varchar](255) NULL,
	[IconUrl] [varchar](255) NULL,
	[AvailabilityDate] [datetime] NULL,
 CONSTRAINT [ac_ProductVariants_PK] PRIMARY KEY CLUSTERED 
(
	[ProductVariantId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_RelatedProducts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_RelatedProducts](
	[ProductId] [int] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
	[ChildProductId] [int] NOT NULL,
 CONSTRAINT [ac_RelatedProducts_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[ChildProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Specials]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Specials](
	[SpecialId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Price] [decimal](12, 4) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [ac_Specials_PK] PRIMARY KEY CLUSTERED 
(
	[SpecialId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_CouponProducts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_CouponProducts](
	[CouponId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [ac_CouponProducts_PK] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_AuditEvents]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_AuditEvents](
	[AuditEventId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[EventTypeId] [int] NOT NULL,
	[Successful] [bit] NOT NULL,
	[UserId] [int] NULL,
	[RelatedId] [int] NULL,
	[RemoteIP] [varchar](39) NULL,
	[Comment] [nvarchar](max) NULL,
 CONSTRAINT [ac_AuditLog_PK] PRIMARY KEY NONCLUSTERED 
(
	[AuditEventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Addresses]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Addresses](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Nickname] [nvarchar](100) NULL,
	[FirstName] [nvarchar](30) NULL,
	[LastName] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[Province] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](15) NULL,
	[CountryCode] [char](2) NULL,
	[Phone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](255) NULL,
	[Residence] [bit] NOT NULL,
	[Validated] [bit] NOT NULL,
	[IsBilling] [bit] NOT NULL,
 CONSTRAINT [ac_Addresses_PK] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_Baskets]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Baskets](
	[BasketId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ContentHash] [nvarchar](32) NULL,
 CONSTRAINT [ac_Baskets_PK] PRIMARY KEY CLUSTERED 
(
	[BasketId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductReviews]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductReviews](
	[ProductReviewId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[ReviewerProfileId] [int] NOT NULL,
	[ReviewDate] [datetime] NOT NULL,
	[Rating] [tinyint] NOT NULL,
	[ReviewTitle] [nvarchar](100) NOT NULL,
	[ReviewBody] [nvarchar](max) NOT NULL,
	[IsApproved] [bit] NOT NULL,
 CONSTRAINT [ac_ProductReviews_PK] PRIMARY KEY CLUSTERED 
(
	[ProductReviewId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductProductTemplates]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductProductTemplates](
	[ProductId] [int] NOT NULL,
	[ProductTemplateId] [int] NOT NULL,
 CONSTRAINT [ac_ProductProductTemplates_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[ProductTemplateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductOptions]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductOptions](
	[ProductId] [int] NOT NULL,
	[OptionId] [int] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_ProductOptions_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[OptionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductKitComponents]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductKitComponents](
	[ProductId] [int] NOT NULL,
	[KitComponentId] [int] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_ProductKitComponents_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[KitComponentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductImages]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_ProductImages](
	[ProductImageId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[ImageUrl] [varchar](255) NOT NULL,
	[ImageAltText] [nvarchar](100) NULL,
	[OrderBy] [smallint] NOT NULL,
	[Moniker] [varchar](255) NULL,
 CONSTRAINT [ac_ProductImages_PK] PRIMARY KEY CLUSTERED 
(
	[ProductImageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_ProductGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductGroups](
	[ProductGroupId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductDigitalGoods]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_ProductDigitalGoods](
	[ProductDigitalGoodId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[OptionList] [varchar](255) NULL,
	[DigitalGoodId] [int] NOT NULL,
 CONSTRAINT [ac_ProductDigitalGoods_PK] PRIMARY KEY CLUSTERED 
(
	[ProductDigitalGoodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_ProductCustomFields]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductCustomFields](
	[ProductFieldId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[IsUserDefined] [bit] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[FieldName] [nvarchar](100) NOT NULL,
	[FieldValue] [nvarchar](max) NULL,
 CONSTRAINT [ac_ProductCustomFields_PK] PRIMARY KEY CLUSTERED 
(
	[ProductFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_ProductAssets]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_ProductAssets](
	[ProductAssetId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[AssetUrl] [nvarchar](255) NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_ProductAssets_PK] PRIMARY KEY CLUSTERED 
(
	[ProductAssetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spGetRandomProduct]    Script Date: 03/03/2015 22:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetRandomProduct]
                @Id INT  = 0
AS -- Random Product

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF (@Id >0)
BEGIN
	
	SELECT ProductId,
               [ac_Products].Name,
               [ac_Manufacturers].[Name]           AS Author,
               [ac_Manufacturers].[ManufacturerId] AS AuthorId,
               ImageUrl AS [ThumbnailUrl],
               [ThumbnailAltText],
               [Summary],
               Price,
               MSRP
  FROM   ac_Products WITH (NOLOCK)
         INNER JOIN [ac_Manufacturers] WITH (NOLOCK)
           ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
  WHERE  ProductId = @Id
END 

 -- Select the columns from the table joined for the key 
  -- selected from the cursor
  --select top 1 ProductId FROM ac_Products WHERE Active = 1 AND ProductId = @Product
  SELECT TOP 1 ProductId,
               [ac_Products].Name,
               [ac_Manufacturers].[Name]           AS Author,
               [ac_Manufacturers].[ManufacturerId] AS AuthorId,
               ImageUrl AS [ThumbnailUrl],
               [ThumbnailAltText],
               [Summary],
               Price,
               MSRP
  FROM   ac_Products WITH (NOLOCK)
         INNER JOIN [ac_Manufacturers] WITH (NOLOCK)
           ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
  ORDER BY NEWID()

--select * from ac_Products where name like '%ladies%'
GO
/****** Object:  StoredProcedure [dbo].[spGetNewProducts]    Script Date: 03/03/2015 22:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spGetNewProducts]
                @MaxItems INT  = 5
AS
  BEGIN
    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET CONCAT_NULL_YIELDS_NULL  OFF
    
    SET ROWCOUNT  @MaxItems
    
    SELECT   ac_PRODUCTS.ProductId,
             ac_PRODUCTS.Name,
             --CAST(ac_PRODUCTS.Description1 AS VARCHAR(MAX)) + ' ' +
             CAST(ac_PRODUCTS.[Description] AS VARCHAR(500)) + '...' AS DESCRIPTION,
             [ac_Manufacturers].[Name]                                               AS AuthorName,
             [ac_Manufacturers].[ManufacturerId]                                     AS ManufacturerId,
             ac_PRODUCTS.[Sku]                                                       AS GUID,
             'product.aspx?ProductId=' + CAST(ac_PRODUCTS.ProductId AS VARCHAR(MAX)) AS DisplayPage,
             [ac_Products].[CreatedDate],
             [ac_Products].[Price],
             ac_Products.[ThumbnailUrl],
             Summary
    FROM     ac_PRODUCTS WITH(NOLOCK)
             INNER JOIN [ac_Manufacturers]
               ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
    WHERE    ac_PRODUCTS.[DisablePurchase] = 0 AND SKU NOT LIKE '%EPUB%'
    ORDER BY [CreatedDate] DESC
  END
GO
/****** Object:  StoredProcedure [dbo].[spGetNewBooks]    Script Date: 03/03/2015 22:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spGetNewBooks]
AS
  BEGIN
    SET CONCAT_NULL_YIELDS_NULL  OFF
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
    SELECT   TOP 15 ac_PRODUCTS.ProductId,
                    ac_PRODUCTS.Name,
                    --CAST(ac_PRODUCTS.Description1 AS VARCHAR(MAX)) + ' ' +
                    CAST(ac_PRODUCTS.DESCRIPTION AS VARCHAR(1500)) + '...' AS DESCRIPTION,
                    [ac_Manufacturers].[Name]                                               AS AuthorName,
                    ac_PRODUCTS.[Sku]                                                       AS GUID,
                    ac_PRODUCTS.[Sku],
                    'product.aspx?ProductId=' + CAST(ac_PRODUCTS.ProductId AS VARCHAR(MAX)) AS DisplayPage,
                    [ac_Products].[CreatedDate]
    FROM     ac_PRODUCTS
             INNER JOIN [ac_Manufacturers]
               ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
    WHERE    ac_PRODUCTS.[DisablePurchase] = 0
             AND SKU IS NOT NULL AND SKU NOT LIKE '%EPUB%'
    ORDER BY [CreatedDate] DESC
  END
GO
/****** Object:  Table [dbo].[bh_ProductExtended]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bh_ProductExtended](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ISBN] [nvarchar](50) NULL,
	[Binding] [nvarchar](50) NULL,
	[Pages] [nvarchar](50) NULL,
	[Edition] [nvarchar](50) NULL,
	[PubDate] [nvarchar](50) NULL,
 CONSTRAINT [PK_ac_ProductExtended] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_UpsellProducts]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_UpsellProducts](
	[ProductId] [int] NOT NULL,
	[ChildProductId] [int] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
 CONSTRAINT [ac_UpsellProducts_PK] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[ChildProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_SnowCatImagesTShirts]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SnowCatImagesTShirts]
AS

SELECT  '<photo image="http://mises.org/store/assets/productimages/'+ CAST(SKU AS VARCHAR(MAX))+ '.jpg" bigimage="http://mises.org/store/assets/productimages/'+ CAST(SKU AS VARCHAR(MAX))+ '.jpg" url="http://mises.org/store/Product.aspx?ProductId='+ CAST(ProductId AS VARCHAR(MAX)) + '" target="_self" lightboxinfo = "' + NAME + ' &lt;br&gt; $' + dbo.format_currency(Price) +  ' &lt;br&gt; &lt;a href=&quot;http://mises.org/store/-P' + CAST(ProductId AS VARCHAR(MAX)) +  '.aspx&quot;&gt;View Product Page&lt;/a&gt; ">
 <![CDATA[ <tooltip> <br><b>' + Name + '</b><br> $' + dbo.format_currency(Price) +  '</tooltip>
<description>' + CAST(Summary AS VARCHAR(100)) + '
<a href="http://mises.org/store/Product.aspx?ProductId=' + CAST(ProductId AS VARCHAR(MAX)) +'">Read More</a></description>]]></photo>'

FROM ac_Products WHERE DisablePurchase = 0 AND VisibilityId = 0 AND (NAME LIKE '%T-Shirt%')

    ORDER BY Name
GO
/****** Object:  StoredProcedure [dbo].[SnowCatImagesGetTshirtInfo]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SnowCatImagesGetTshirtInfo]
AS

SELECT SKU, NAME, Price, Summary  FROM ac_Products WHERE  NAME LIKE '%T-Shirt%'
ORDER BY Name
GO
/****** Object:  StoredProcedure [dbo].[ProductsGetList]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ProductsGetList] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT ProductId, [Name], REPLACE(ISNULL(ThumbnailUrl,''),'~','/store') AS ThumbnailUrl, ThumbnailAltText, VisibilityId
    FROM ac_Products WHERE VisibilityId = 0 AND ThumbnailUrl IS NOT NULL
    ORDER BY Name

END
GO
/****** Object:  StoredProcedure [dbo].[ProductIdFromSKU]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ProductIdFromSKU] 
	-- Add the parameters for the stored procedure here
	@SKU VARCHAR(150)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
		SELECT ProductId FROM ac_Products WHERE sku = @sku
	
END
GO
/****** Object:  StoredProcedure [dbo].[ProductGetRandom]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductGetRandom]                
AS 
 
  SELECT TOP 1 ProductId,
               [ac_Products].Name,
               [ac_Manufacturers].[Name]           AS Author,
               [ac_Manufacturers].[ManufacturerId] AS AuthorId,
               [ThumbnailUrl],
               [ThumbnailAltText],
               [Summary],
               Price, 
               MSRP
  FROM   ac_Products WITH (NOLOCK)
         INNER JOIN [ac_Manufacturers] WITH (NOLOCK)
           ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
  ORDER BY NEWID()
GO
/****** Object:  StoredProcedure [dbo].[MoveOrderToUser]    Script Date: 03/03/2015 22:05:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[MoveOrderToUser] 
	-- Add the parameters for the stored procedure here
	@NewUser int, 
	@OldUser int
AS
BEGIN
	UPDATE ac_orders SET dbo.ac_Orders.UserId = @NewUser WHERE dbo.ac_Orders.UserId = @OldUser
END
GO
/****** Object:  Table [dbo].[ac_OrderNotes]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderNotes](
	[OrderNoteId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[UserId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[NoteTypeId] [tinyint] NOT NULL,
	[IsRead] [bit] NOT NULL,
 CONSTRAINT [ac_OrderNotes_PK] PRIMARY KEY CLUSTERED 
(
	[OrderNoteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_BasketShipments]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_BasketShipments](
	[BasketShipmentId] [int] IDENTITY(1,1) NOT NULL,
	[BasketId] [int] NOT NULL,
	[WarehouseId] [int] NULL,
	[ShipMethodId] [int] NULL,
	[AddressId] [int] NULL,
	[ShipMessage] [nvarchar](255) NULL,
 CONSTRAINT [ac_BasketShipments_PK] PRIMARY KEY CLUSTERED 
(
	[BasketShipmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_SpecialGroups]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_SpecialGroups](
	[SpecialId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [ac_SpecialGroups_PK] PRIMARY KEY CLUSTERED 
(
	[SpecialId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_OrderCoupons]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderCoupons](
	[OrderId] [int] NOT NULL,
	[CouponCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [ac_OrderCoupons_PK] PRIMARY KEY CLUSTERED 
(
	[CouponCode] ASC,
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_BasketCoupons]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_BasketCoupons](
	[BasketId] [int] NOT NULL,
	[CouponId] [int] NOT NULL,
	[AppliedDate] [datetime] NOT NULL,
 CONSTRAINT [ac_BasketCoupons_PK] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[BasketId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_WishlistItemInputs]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_WishlistItemInputs](
	[WishlistItemInputId] [int] IDENTITY(1,1) NOT NULL,
	[WishlistItemId] [int] NOT NULL,
	[InputFieldId] [int] NOT NULL,
	[InputValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_WishlistItemInputs_PK] PRIMARY KEY CLUSTERED 
(
	[WishlistItemInputId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_TrackingNumbers]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_TrackingNumbers](
	[TrackingNumberId] [int] IDENTITY(1,1) NOT NULL,
	[OrderShipmentId] [int] NOT NULL,
	[ShipGatewayId] [int] NULL,
	[TrackingNumberData] [nvarchar](100) NOT NULL,
 CONSTRAINT [ac_TrackingNumbers_PK] PRIMARY KEY CLUSTERED 
(
	[TrackingNumberId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_BasketItems]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_BasketItems](
	[BasketItemId] [int] IDENTITY(1,1) NOT NULL,
	[ParentItemId] [int] NOT NULL,
	[BasketId] [int] NOT NULL,
	[BasketShipmentId] [int] NULL,
	[ProductId] [int] NULL,
	[OptionList] [varchar](500) NULL,
	[TaxCodeId] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Sku] [nvarchar](100) NULL,
	[Price] [decimal](12, 4) NOT NULL,
	[Weight] [decimal](12, 4) NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[LineMessage] [nvarchar](500) NULL,
	[OrderItemTypeId] [smallint] NOT NULL,
	[OrderBy] [smallint] NOT NULL,
	[WrapStyleId] [int] NULL,
	[GiftMessage] [nvarchar](500) NULL,
	[WishlistItemId] [int] NULL,
	[ShippableId] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[TaxRate] [decimal](12, 4) NOT NULL,
	[TaxAmount] [decimal](12, 4) NOT NULL,
	[KitList] [varchar](500) NULL,
	[CustomFields] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [ac_BasketItems_PK] PRIMARY KEY CLUSTERED 
(
	[BasketItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[spGetTopSellers]    Script Date: 03/03/2015 22:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spGetTopSellers]
                @MaxItems INT  = 5
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  BEGIN
    SET ROWCOUNT  @MaxItems
    
    SELECT   ac_PRODUCTS.ProductId,
             ac_PRODUCTS.Name,
             --CAST(ac_PRODUCTS.Description1 AS VARCHAR(MAX)) + ' ' +
             CAST(ac_PRODUCTS.[Description] AS VARCHAR(1500)) + '...' AS DESCRIPTION,
             [ac_Manufacturers].[Name] AS Author,
             [ac_Manufacturers].[ManufacturerId] AS ManufacturerId,
             ac_PRODUCTS.[Sku]                                                       AS GUID,
             'product.aspx?ProductId=' + CAST(ac_PRODUCTS.ProductId AS VARCHAR(MAX)) AS DisplayPage,
             [ac_Products].[CreatedDate],
             [ac_Products].[Price],
             ac_Products.[ThumbnailUrl],
             SUM(ac_ORDERITEMS.Quantity)                                             AS TotalQuantity,
             SUM(ac_ORDERITEMS.[Price] * ac_ORDERITEMS.Quantity)                     AS TotalSales
    FROM     (ac_ORDERS
              INNER JOIN [ac_ORDERITEMS]
                ON [ac_ORDERITEMS].[OrderId] = ac_ORDERS.OrderId)
             INNER JOIN ac_PRODUCTS
               ON ac_ORDERITEMS.ProductId = ac_PRODUCTS.ProductId
             INNER JOIN [ac_Manufacturers]
               ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
    WHERE    ac_PRODUCTS.[DisablePurchase] = 0
             AND ac_PRODUCTS.[VisibilityId] = 0
             --AND ac_ORDERS.StoreId = 3
             AND [OrderDate] > GETDATE() - 15
             AND ac_ORDERS.[OrderDate] IS NOT NULL
             AND ac_ORDERITEMS.ProductId > 0
             AND dbo.ac_Products.Price > 1
             AND dbo.ac_Products.ProductId <> 157
    GROUP BY ac_PRODUCTS.ProductId,
             ac_PRODUCTS.Name,
             ac_PRODUCTS.DisplayPage,
             ac_PRODUCTS.[Description],
             ac_PRODUCTS.[Sku],
             ac_PRODUCTS.[ThumbnailUrl],
             ac_PRODUCTS.[Price],
             [ac_Manufacturers].[Name],
             [ac_Products].[CreatedDate],
             [ac_Manufacturers].[ManufacturerId]
    ORDER BY SUM(ac_ORDERITEMS.Quantity) DESC
--             SUM(ac_ORDERITEMS.[Price] * ac_ORDERITEMS.Quantity) DESC
  END
GO
/****** Object:  StoredProcedure [dbo].[spGetBestSellers]    Script Date: 03/03/2015 22:05:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		DavidV
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spGetBestSellers]
AS
  BEGIN
    SELECT   TOP 6 ac_PRODUCTS.ProductId,
                    ac_PRODUCTS.Name,
                    [ac_Manufacturers].[Name]                                               AS Author,
                    'product.aspx?ProductId=' + CAST(ac_PRODUCTS.ProductId AS VARCHAR(MAX)) AS DisplayPage,
                    SUM(ac_ORDERITEMS.Quantity)                                             AS TotalQuantity,
                    SUM(ac_ORDERITEMS.[Price] * ac_ORDERITEMS.Quantity)                     AS TotalSales
    FROM     (ac_ORDERS
              INNER JOIN [ac_ORDERITEMS]
                ON [ac_ORDERITEMS].[OrderId] = ac_ORDERS.OrderId)
             INNER JOIN ac_PRODUCTS
               ON ac_ORDERITEMS.ProductId = ac_PRODUCTS.ProductId
             INNER JOIN [ac_Manufacturers]
               ON [ac_Manufacturers].[ManufacturerId] = [ac_Products].[ManufacturerId]
    WHERE    ac_PRODUCTS.[DisablePurchase] = 0   
             AND ac_Products.[VisibilityId] = 0        
             AND ac_ORDERS.[OrderDate] IS NOT NULL
             AND [OrderDate] > GETDATE() - 15             
             AND ac_ORDERITEMS.ProductId > 0
             AND dbo.ac_Products.ProductId <> 157
    GROUP BY ac_PRODUCTS.ProductId,
             ac_PRODUCTS.Name,
             ac_PRODUCTS.DisplayPage,
             [ac_Manufacturers].[Name]
    ORDER BY SUM(ac_ORDERITEMS.Quantity) DESC
--            SUM(ac_ORDERITEMS.[Price] * ac_ORDERITEMS.Quantity) DESC
  END
GO
/****** Object:  Table [dbo].[ac_OrderItemInputs]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderItemInputs](
	[OrderItemInputId] [int] IDENTITY(1,1) NOT NULL,
	[OrderItemId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[InputValue] [nvarchar](max) NOT NULL,
	[IsMerchantField] [bit] NOT NULL,
 CONSTRAINT [ac_OrderItemInputs_PK] PRIMARY KEY CLUSTERED 
(
	[OrderItemInputId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_OrderItemDigitalGoods]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_OrderItemDigitalGoods](
	[OrderItemDigitalGoodId] [int] IDENTITY(1,1) NOT NULL,
	[OrderItemId] [int] NOT NULL,
	[DigitalGoodId] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ActivationDate] [datetime] NULL,
	[DownloadDate] [datetime] NULL,
	[SerialKeyData] [nvarchar](max) NULL,
 CONSTRAINT [ac_OrderItemDigitalGoods_PK] PRIMARY KEY CLUSTERED 
(
	[OrderItemDigitalGoodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_BasketItemInputs]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_BasketItemInputs](
	[BasketItemInputId] [int] IDENTITY(1,1) NOT NULL,
	[BasketItemId] [int] NOT NULL,
	[InputFieldId] [int] NOT NULL,
	[InputValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [ac_BasketItemInputs_PK] PRIMARY KEY CLUSTERED 
(
	[BasketItemInputId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_GiftCertificates]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_GiftCertificates](
	[GiftCertificateId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[SerialNumber] [nvarchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[OrderItemId] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[Balance] [decimal](12, 4) NOT NULL,
 CONSTRAINT [ac_GiftCertificates_PK] PRIMARY KEY CLUSTERED 
(
	[GiftCertificateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_GiftCertificateTransactions]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_GiftCertificateTransactions](
	[GiftCertificateTransactionId] [int] IDENTITY(1,1) NOT NULL,
	[GiftCertificateId] [int] NOT NULL,
	[OrderId] [int] NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Amount] [decimal](12, 4) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [ac_GiftCertificateTransactions_PK] PRIMARY KEY CLUSTERED 
(
	[GiftCertificateTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ac_Downloads]    Script Date: 03/03/2015 22:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Downloads](
	[DownloadId] [int] IDENTITY(1,1) NOT NULL,
	[OrderItemDigitalGoodId] [int] NOT NULL,
	[DownloadDate] [datetime] NOT NULL,
	[RemoteAddr] [nvarchar](39) NOT NULL,
	[UserAgent] [nvarchar](255) NOT NULL,
	[Referrer] [nvarchar](255) NOT NULL,
 CONSTRAINT [ac_Downloads_PK] PRIMARY KEY NONCLUSTERED 
(
	[DownloadId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__ac_Addres__IsBil__2630A1B7]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Addresses] ADD  DEFAULT ((0)) FOR [IsBilling]
GO
/****** Object:  Default [DF__ac_Basket__Order__662B2B3B]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItems] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Catalo__Order__5F7E2DAC]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CatalogNodes] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Curren__Excha__4B7734FF]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Currencies] ADD  DEFAULT ((1)) FOR [ExchangeRate]
GO
/****** Object:  Default [DF__ac_InputC__Order__339FAB6E]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_InputChoices] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_InputF__Order__30C33EC3]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_InputFields] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_KitPro__Order__2BFE89A6]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_KitProducts] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Langua__IsAct__31A25463]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Languages] ADD  DEFAULT ((0)) FOR [IsActive]
GO
/****** Object:  Default [DF__ac_Option__Order__72910220]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OptionChoices] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Option__Selec__3B2BBE9D]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OptionChoices] ADD  DEFAULT ((0)) FOR [Selected]
GO
/****** Object:  Default [DF__ac_OrderI__Order__1BC821DD]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_OrderN__IsRea__09946309]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderNotes] ADD  DEFAULT ((1)) FOR [IsRead]
GO
/****** Object:  Default [DF__ac_OrderS__Order__0F624AF8]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderStatuses] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Paymen__Order__04E4BC85]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PaymentMethods] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF_ac_PrintedCatelog_CreatedDate]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PrintedCatalog] ADD  CONSTRAINT [DF_ac_PrintedCatelog_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF__ac_Produc__Order__7E37BEF6]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductAssets] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Produc__Order__778AC167]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductImages] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_Produc__Order__74AE54BC]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductKitComponents] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF_ac_Products_GUID]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products] ADD  CONSTRAINT [DF_ac_Products_GUID]  DEFAULT (newid()) FOR [GUID]
GO
/****** Object:  Default [DF__ac_Produc__Shipp__6FE99F9F]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products] ADD  CONSTRAINT [DF__ac_Produc__Shipp__6FE99F9F]  DEFAULT ((0)) FOR [ShippableId]
GO
/****** Object:  Default [DF__ac_Produc__Publi__290D0E62]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products] ADD  DEFAULT ((0)) FOR [PublishFeedAsVariants]
GO
/****** Object:  Default [DF__ac_Produc__RowVe__2A01329B]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products] ADD  DEFAULT ((1)) FOR [RowVersion]
GO
/****** Object:  Default [DF__ac_Produc__Enabl__41D8BC2C]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products] ADD  DEFAULT ((0)) FOR [EnableGroups]
GO
/****** Object:  Default [DF__ac_Produc__RowVe__2AF556D4]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductVariants] ADD  DEFAULT ((1)) FOR [RowVersion]
GO
/****** Object:  Default [DF__ac_Redire__Order__53385258]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Redirects] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_ShipMe__Order__5629CD9C]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethods] ADD  CONSTRAINT [DF__ac_ShipMe__Order__5629CD9C]  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  Default [DF__ac_ShipRa__IsPer__4F7CD00D]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipRateMatrix] ADD  DEFAULT ((0)) FOR [IsPercent]
GO
/****** Object:  Default [DF__ac_Stores__NextO__4222D4EF]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Stores] ADD  DEFAULT ((1)) FOR [NextOrderId]
GO
/****** Object:  Default [DF__ac_Stores__Order__4316F928]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Stores] ADD  DEFAULT ((1)) FOR [OrderIdIncrement]
GO
/****** Object:  Default [DF__ac_Wishli__IsPub__2BE97B0D]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Wishlists] ADD  DEFAULT ((0)) FOR [IsPublic]
GO
/****** Object:  Default [DF__ac_Wishli__ViewC__2CDD9F46]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Wishlists] ADD  DEFAULT (newsequentialid()) FOR [ViewCode]
GO
/****** Object:  Default [DF__ac_WrapSt__Order__117F9D94]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WrapStyles] ADD  DEFAULT ((0)) FOR [OrderBy]
GO
/****** Object:  ForeignKey [ac_Countries_ac_Addresses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Addresses]  WITH CHECK ADD  CONSTRAINT [ac_Countries_ac_Addresses_FK1] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[ac_Countries] ([CountryCode])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Addresses] CHECK CONSTRAINT [ac_Countries_ac_Addresses_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_Addresses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Addresses]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_Addresses_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Addresses] CHECK CONSTRAINT [ac_Users_ac_Addresses_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_Affiliates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Affiliates]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_Affiliates_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Affiliates] CHECK CONSTRAINT [ac_Groups_ac_Affiliates_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Affiliates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Affiliates]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Affiliates_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Affiliates] CHECK CONSTRAINT [ac_Stores_ac_Affiliates_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_AuditEvents_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_AuditEvents]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_AuditEvents_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_AuditEvents] CHECK CONSTRAINT [ac_Stores_ac_AuditEvents_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_Auditlog_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_AuditEvents]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_Auditlog_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_AuditEvents] CHECK CONSTRAINT [ac_Users_ac_Auditlog_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_BannedIPs_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BannedIPs]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_BannedIPs_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_BannedIPs] CHECK CONSTRAINT [ac_Stores_ac_BannedIPs_FK1]
GO
/****** Object:  ForeignKey [ac_Baskets_ac_BasketCoupons_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketCoupons]  WITH CHECK ADD  CONSTRAINT [ac_Baskets_ac_BasketCoupons_FK1] FOREIGN KEY([BasketId])
REFERENCES [dbo].[ac_Baskets] ([BasketId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketCoupons] CHECK CONSTRAINT [ac_Baskets_ac_BasketCoupons_FK1]
GO
/****** Object:  ForeignKey [ac_Coupons_ac_OrderCouponAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketCoupons]  WITH CHECK ADD  CONSTRAINT [ac_Coupons_ac_OrderCouponAssn_FK1] FOREIGN KEY([CouponId])
REFERENCES [dbo].[ac_Coupons] ([CouponId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketCoupons] CHECK CONSTRAINT [ac_Coupons_ac_OrderCouponAssn_FK1]
GO
/****** Object:  ForeignKey [ac_BasketItems_ac_BasketItemInputs_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItemInputs]  WITH CHECK ADD  CONSTRAINT [ac_BasketItems_ac_BasketItemInputs_FK1] FOREIGN KEY([BasketItemId])
REFERENCES [dbo].[ac_BasketItems] ([BasketItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketItemInputs] CHECK CONSTRAINT [ac_BasketItems_ac_BasketItemInputs_FK1]
GO
/****** Object:  ForeignKey [ac_InputFields_ac_BasketItemInputs_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItemInputs]  WITH CHECK ADD  CONSTRAINT [ac_InputFields_ac_BasketItemInputs_FK1] FOREIGN KEY([InputFieldId])
REFERENCES [dbo].[ac_InputFields] ([InputFieldId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketItemInputs] CHECK CONSTRAINT [ac_InputFields_ac_BasketItemInputs_FK1]
GO
/****** Object:  ForeignKey [ac_Baskets_ac_BasketItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItems]  WITH CHECK ADD  CONSTRAINT [ac_Baskets_ac_BasketItems_FK1] FOREIGN KEY([BasketId])
REFERENCES [dbo].[ac_Baskets] ([BasketId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketItems] CHECK CONSTRAINT [ac_Baskets_ac_BasketItems_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_BasketItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItems]  WITH NOCHECK ADD  CONSTRAINT [ac_Products_ac_BasketItems_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_BasketItems] NOCHECK CONSTRAINT [ac_Products_ac_BasketItems_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_BasketItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItems]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_BasketItems_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_BasketItems] CHECK CONSTRAINT [ac_TaxCodes_ac_BasketItems_FK1]
GO
/****** Object:  ForeignKey [ac_WishlistItems_ac_BasketItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItems]  WITH CHECK ADD  CONSTRAINT [ac_WishlistItems_ac_BasketItems_FK1] FOREIGN KEY([WishlistItemId])
REFERENCES [dbo].[ac_WishlistItems] ([WishlistItemId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_BasketItems] CHECK CONSTRAINT [ac_WishlistItems_ac_BasketItems_FK1]
GO
/****** Object:  ForeignKey [ac_WrapStyles_ac_BasketItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketItems]  WITH CHECK ADD  CONSTRAINT [ac_WrapStyles_ac_BasketItems_FK1] FOREIGN KEY([WrapStyleId])
REFERENCES [dbo].[ac_WrapStyles] ([WrapStyleId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_BasketItems] CHECK CONSTRAINT [ac_WrapStyles_ac_BasketItems_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_Baskets_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Baskets]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_Baskets_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
GO
ALTER TABLE [dbo].[ac_Baskets] CHECK CONSTRAINT [ac_Users_ac_Baskets_FK1]
GO
/****** Object:  ForeignKey [ac_Baskets_ac_BasketShipments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketShipments]  WITH CHECK ADD  CONSTRAINT [ac_Baskets_ac_BasketShipments_FK1] FOREIGN KEY([BasketId])
REFERENCES [dbo].[ac_Baskets] ([BasketId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketShipments] CHECK CONSTRAINT [ac_Baskets_ac_BasketShipments_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_BasketShipments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketShipments]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_BasketShipments_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_BasketShipments] CHECK CONSTRAINT [ac_ShipMethods_ac_BasketShipments_FK1]
GO
/****** Object:  ForeignKey [ac_Warehouses_ac_BasketShipments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_BasketShipments]  WITH CHECK ADD  CONSTRAINT [ac_Warehouses_ac_BasketShipments_FK1] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[ac_Warehouses] ([WarehouseId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_BasketShipments] CHECK CONSTRAINT [ac_Warehouses_ac_BasketShipments_FK1]
GO
/****** Object:  ForeignKey [ac_Categories_ac_CatalogNodes_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CatalogNodes]  WITH NOCHECK ADD  CONSTRAINT [ac_Categories_ac_CatalogNodes_FK1] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ac_Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[ac_CatalogNodes] NOCHECK CONSTRAINT [ac_Categories_ac_CatalogNodes_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Categories_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Categories]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Categories_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Categories] CHECK CONSTRAINT [ac_Stores_ac_Categories_FK1]
GO
/****** Object:  ForeignKey [ac_Categories_ac_CategoryAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CategoryParents]  WITH NOCHECK ADD  CONSTRAINT [ac_Categories_ac_CategoryAssn_FK1] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ac_Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[ac_CategoryParents] NOCHECK CONSTRAINT [ac_Categories_ac_CategoryAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Categories_ac_CategoryDiscountAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CategoryVolumeDiscounts]  WITH CHECK ADD  CONSTRAINT [ac_Categories_ac_CategoryDiscountAssn_FK1] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ac_Categories] ([CategoryId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CategoryVolumeDiscounts] CHECK CONSTRAINT [ac_Categories_ac_CategoryDiscountAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Discounts_ac_CategoryDiscountAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CategoryVolumeDiscounts]  WITH CHECK ADD  CONSTRAINT [ac_Discounts_ac_CategoryDiscountAssn_FK1] FOREIGN KEY([VolumeDiscountId])
REFERENCES [dbo].[ac_VolumeDiscounts] ([VolumeDiscountId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CategoryVolumeDiscounts] CHECK CONSTRAINT [ac_Discounts_ac_CategoryDiscountAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Countries_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Countries]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Countries_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Countries] CHECK CONSTRAINT [ac_Stores_ac_Countries_FK1]
GO
/****** Object:  ForeignKey [ac_Coupons_ac_CouponCombos_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponCombos]  WITH CHECK ADD  CONSTRAINT [ac_Coupons_ac_CouponCombos_FK1] FOREIGN KEY([CouponId])
REFERENCES [dbo].[ac_Coupons] ([CouponId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponCombos] CHECK CONSTRAINT [ac_Coupons_ac_CouponCombos_FK1]
GO
/****** Object:  ForeignKey [ac_Coupons_ac_CouponCombos_FK2]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponCombos]  WITH CHECK ADD  CONSTRAINT [ac_Coupons_ac_CouponCombos_FK2] FOREIGN KEY([ComboCouponId])
REFERENCES [dbo].[ac_Coupons] ([CouponId])
GO
ALTER TABLE [dbo].[ac_CouponCombos] CHECK CONSTRAINT [ac_Coupons_ac_CouponCombos_FK2]
GO
/****** Object:  ForeignKey [ac_Coupons_ac_CouponGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponGroups]  WITH CHECK ADD  CONSTRAINT [ac_Coupons_ac_CouponGroups_FK1] FOREIGN KEY([CouponId])
REFERENCES [dbo].[ac_Coupons] ([CouponId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponGroups] CHECK CONSTRAINT [ac_Coupons_ac_CouponGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_CouponGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_CouponGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponGroups] CHECK CONSTRAINT [ac_Groups_ac_CouponGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Coupons_ac_CouponProductAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponProducts]  WITH CHECK ADD  CONSTRAINT [ac_Coupons_ac_CouponProductAssn_FK1] FOREIGN KEY([CouponId])
REFERENCES [dbo].[ac_Coupons] ([CouponId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponProducts] CHECK CONSTRAINT [ac_Coupons_ac_CouponProductAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_CouponProductAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponProducts]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_CouponProductAssn_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponProducts] CHECK CONSTRAINT [ac_Products_ac_CouponProductAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Coupons_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Coupons]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Coupons_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Coupons] CHECK CONSTRAINT [ac_Stores_ac_Coupons_FK1]
GO
/****** Object:  ForeignKey [ac_Coupons_ac_CouponShipMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponShipMethods]  WITH CHECK ADD  CONSTRAINT [ac_Coupons_ac_CouponShipMethods_FK1] FOREIGN KEY([CouponId])
REFERENCES [dbo].[ac_Coupons] ([CouponId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponShipMethods] CHECK CONSTRAINT [ac_Coupons_ac_CouponShipMethods_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_CouponShipMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CouponShipMethods]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_CouponShipMethods_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CouponShipMethods] CHECK CONSTRAINT [ac_ShipMethods_ac_CouponShipMethods_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Currencies_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Currencies]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Currencies_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Currencies] CHECK CONSTRAINT [ac_Stores_ac_Currencies_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_CustomFields_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CustomFields]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_CustomFields_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_CustomFields] CHECK CONSTRAINT [ac_Stores_ac_CustomFields_FK1]
GO
/****** Object:  ForeignKey [ac_CustomUrls_ac_Stores_FK]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_CustomUrls]  WITH CHECK ADD  CONSTRAINT [ac_CustomUrls_ac_Stores_FK] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_CustomUrls] CHECK CONSTRAINT [ac_CustomUrls_ac_Stores_FK]
GO
/****** Object:  ForeignKey [ac_LicenseAgreements_ac_DigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_DigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_LicenseAgreements_ac_DigitalGoods_FK1] FOREIGN KEY([LicenseAgreementId])
REFERENCES [dbo].[ac_LicenseAgreements] ([LicenseAgreementId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_DigitalGoods] CHECK CONSTRAINT [ac_LicenseAgreements_ac_DigitalGoods_FK1]
GO
/****** Object:  ForeignKey [ac_Readmes_ac_DigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_DigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_Readmes_ac_DigitalGoods_FK1] FOREIGN KEY([ReadmeId])
REFERENCES [dbo].[ac_Readmes] ([ReadmeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_DigitalGoods] CHECK CONSTRAINT [ac_Readmes_ac_DigitalGoods_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_DigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_DigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_DigitalGoods_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_DigitalGoods] CHECK CONSTRAINT [ac_Stores_ac_DigitalGoods_FK1]
GO
/****** Object:  ForeignKey [ac_OrderFiles_ac_Downloads_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Downloads]  WITH CHECK ADD  CONSTRAINT [ac_OrderFiles_ac_Downloads_FK1] FOREIGN KEY([OrderItemDigitalGoodId])
REFERENCES [dbo].[ac_OrderItemDigitalGoods] ([OrderItemDigitalGoodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Downloads] CHECK CONSTRAINT [ac_OrderFiles_ac_Downloads_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_EmailLists_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_EmailLists]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_EmailLists_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_EmailLists] CHECK CONSTRAINT [ac_Stores_ac_EmailLists_FK1]
GO
/****** Object:  ForeignKey [ac_EmailLists_ac_EmailListSignups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_EmailListSignups]  WITH CHECK ADD  CONSTRAINT [ac_EmailLists_ac_EmailListSignups_FK1] FOREIGN KEY([EmailListId])
REFERENCES [dbo].[ac_EmailLists] ([EmailListId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_EmailListSignups] CHECK CONSTRAINT [ac_EmailLists_ac_EmailListSignups_FK1]
GO
/****** Object:  ForeignKey [ac_EmailLists_ac_EmailListUsers_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_EmailListUsers]  WITH CHECK ADD  CONSTRAINT [ac_EmailLists_ac_EmailListUsers_FK1] FOREIGN KEY([EmailListId])
REFERENCES [dbo].[ac_EmailLists] ([EmailListId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_EmailListUsers] CHECK CONSTRAINT [ac_EmailLists_ac_EmailListUsers_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Emails_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_EmailTemplates]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Emails_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_EmailTemplates] CHECK CONSTRAINT [ac_Stores_ac_Emails_FK1]
GO
/****** Object:  ForeignKey [ac_Emails_ac_EmailEventAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_EmailTemplateTriggers]  WITH CHECK ADD  CONSTRAINT [ac_Emails_ac_EmailEventAssn_FK1] FOREIGN KEY([EmailTemplateId])
REFERENCES [dbo].[ac_EmailTemplates] ([EmailTemplateId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_EmailTemplateTriggers] CHECK CONSTRAINT [ac_Emails_ac_EmailEventAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_ErrorMessages_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ErrorMessages]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_ErrorMessages_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_ErrorMessages] CHECK CONSTRAINT [ac_Stores_ac_ErrorMessages_FK1]
GO
/****** Object:  ForeignKey [ac_OrderItems_ac_GiftCertificates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_GiftCertificates]  WITH CHECK ADD  CONSTRAINT [ac_OrderItems_ac_GiftCertificates_FK1] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[ac_OrderItems] ([OrderItemId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_GiftCertificates] CHECK CONSTRAINT [ac_OrderItems_ac_GiftCertificates_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_GiftCertificates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_GiftCertificates]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_GiftCertificates_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_GiftCertificates] CHECK CONSTRAINT [ac_Stores_ac_GiftCertificates_FK1]
GO
/****** Object:  ForeignKey [ac_GiftCertificates_ac_GiftCertificateTransactions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_GiftCertificateTransactions]  WITH CHECK ADD  CONSTRAINT [ac_GiftCertificates_ac_GiftCertificateTransactions_FK1] FOREIGN KEY([GiftCertificateId])
REFERENCES [dbo].[ac_GiftCertificates] ([GiftCertificateId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_GiftCertificateTransactions] CHECK CONSTRAINT [ac_GiftCertificates_ac_GiftCertificateTransactions_FK1]
GO
/****** Object:  ForeignKey [ac_Orders_ac_GiftCertificateTransactions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_GiftCertificateTransactions]  WITH CHECK ADD  CONSTRAINT [ac_Orders_ac_GiftCertificateTransactions_FK1] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ac_Orders] ([OrderId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_GiftCertificateTransactions] CHECK CONSTRAINT [ac_Orders_ac_GiftCertificateTransactions_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_GroupRoles_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_GroupRoles]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_GroupRoles_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_GroupRoles] CHECK CONSTRAINT [ac_Groups_ac_GroupRoles_FK1]
GO
/****** Object:  ForeignKey [ac_Roles_ac_GroupRoles_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_GroupRoles]  WITH CHECK ADD  CONSTRAINT [ac_Roles_ac_GroupRoles_FK1] FOREIGN KEY([RoleId])
REFERENCES [dbo].[ac_Roles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_GroupRoles] CHECK CONSTRAINT [ac_Roles_ac_GroupRoles_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Groups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Groups]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Groups_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Groups] CHECK CONSTRAINT [ac_Stores_ac_Groups_FK1]
GO
/****** Object:  ForeignKey [ac_InputFields_ac_InputChoices_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_InputChoices]  WITH CHECK ADD  CONSTRAINT [ac_InputFields_ac_InputChoices_FK1] FOREIGN KEY([InputFieldId])
REFERENCES [dbo].[ac_InputFields] ([InputFieldId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_InputChoices] CHECK CONSTRAINT [ac_InputFields_ac_InputChoices_FK1]
GO
/****** Object:  ForeignKey [ac_ProductTemplates_ac_InputFields_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_InputFields]  WITH CHECK ADD  CONSTRAINT [ac_ProductTemplates_ac_InputFields_FK1] FOREIGN KEY([ProductTemplateId])
REFERENCES [dbo].[ac_ProductTemplates] ([ProductTemplateId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_InputFields] CHECK CONSTRAINT [ac_ProductTemplates_ac_InputFields_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_KitComponents_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_KitComponents]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_KitComponents_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_KitComponents] CHECK CONSTRAINT [ac_Stores_ac_KitComponents_FK1]
GO
/****** Object:  ForeignKey [ac_KitComponents_ac_KitProduct_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_KitProducts]  WITH CHECK ADD  CONSTRAINT [ac_KitComponents_ac_KitProduct_FK1] FOREIGN KEY([KitComponentId])
REFERENCES [dbo].[ac_KitComponents] ([KitComponentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_KitProducts] CHECK CONSTRAINT [ac_KitComponents_ac_KitProduct_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_Kits_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Kits]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_Kits_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Kits] CHECK CONSTRAINT [ac_Products_ac_Kits_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_LicenseAgreements_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_LicenseAgreements]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_LicenseAgreements_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_LicenseAgreements] CHECK CONSTRAINT [ac_Stores_ac_LicenseAgreements_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Links_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Links]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Links_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Links] CHECK CONSTRAINT [ac_Stores_ac_Links_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Manufacturers_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Manufacturers]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Manufacturers_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Manufacturers] CHECK CONSTRAINT [ac_Stores_ac_Manufacturers_FK1]
GO
/****** Object:  ForeignKey [ac_ProductAttributes_ac_AttributeOptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OptionChoices]  WITH CHECK ADD  CONSTRAINT [ac_ProductAttributes_ac_AttributeOptions_FK1] FOREIGN KEY([OptionId])
REFERENCES [dbo].[ac_Options] ([OptionId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OptionChoices] CHECK CONSTRAINT [ac_ProductAttributes_ac_AttributeOptions_FK1]
GO
/****** Object:  ForeignKey [ac_Orders_ac_OrderCoupons_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderCoupons]  WITH CHECK ADD  CONSTRAINT [ac_Orders_ac_OrderCoupons_FK1] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ac_Orders] ([OrderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderCoupons] CHECK CONSTRAINT [ac_Orders_ac_OrderCoupons_FK1]
GO
/****** Object:  ForeignKey [ac_OrderItemDigitalGoods_ac_DigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItemDigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_OrderItemDigitalGoods_ac_DigitalGoods_FK1] FOREIGN KEY([DigitalGoodId])
REFERENCES [dbo].[ac_DigitalGoods] ([DigitalGoodId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderItemDigitalGoods] CHECK CONSTRAINT [ac_OrderItemDigitalGoods_ac_DigitalGoods_FK1]
GO
/****** Object:  ForeignKey [ac_OrderItems_ac_OrderItemDigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItemDigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_OrderItems_ac_OrderItemDigitalGoods_FK1] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[ac_OrderItems] ([OrderItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderItemDigitalGoods] CHECK CONSTRAINT [ac_OrderItems_ac_OrderItemDigitalGoods_FK1]
GO
/****** Object:  ForeignKey [ac_OrderItems_ac_OrderItemInputs_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItemInputs]  WITH CHECK ADD  CONSTRAINT [ac_OrderItems_ac_OrderItemInputs_FK1] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[ac_OrderItems] ([OrderItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderItemInputs] CHECK CONSTRAINT [ac_OrderItems_ac_OrderItemInputs_FK1]
GO
/****** Object:  ForeignKey [ac_Orders_ac_OrderItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems]  WITH CHECK ADD  CONSTRAINT [ac_Orders_ac_OrderItems_FK1] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ac_Orders] ([OrderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderItems] CHECK CONSTRAINT [ac_Orders_ac_OrderItems_FK1]
GO
/****** Object:  ForeignKey [ac_OrderShipments_ac_OrderItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems]  WITH CHECK ADD  CONSTRAINT [ac_OrderShipments_ac_OrderItems_FK1] FOREIGN KEY([OrderShipmentId])
REFERENCES [dbo].[ac_OrderShipments] ([OrderShipmentId])
GO
ALTER TABLE [dbo].[ac_OrderItems] CHECK CONSTRAINT [ac_OrderShipments_ac_OrderItems_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_OrderItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_OrderItems_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderItems] CHECK CONSTRAINT [ac_Products_ac_OrderItems_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_OrderItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_OrderItems_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderItems] CHECK CONSTRAINT [ac_TaxCodes_ac_OrderItems_FK1]
GO
/****** Object:  ForeignKey [ac_WishlistItems_ac_OrderItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems]  WITH CHECK ADD  CONSTRAINT [ac_WishlistItems_ac_OrderItems_FK1] FOREIGN KEY([WishlistItemId])
REFERENCES [dbo].[ac_WishlistItems] ([WishlistItemId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderItems] CHECK CONSTRAINT [ac_WishlistItems_ac_OrderItems_FK1]
GO
/****** Object:  ForeignKey [ac_WrapStyles_ac_OrderItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderItems]  WITH CHECK ADD  CONSTRAINT [ac_WrapStyles_ac_OrderItems_FK1] FOREIGN KEY([WrapStyleId])
REFERENCES [dbo].[ac_WrapStyles] ([WrapStyleId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderItems] CHECK CONSTRAINT [ac_WrapStyles_ac_OrderItems_FK1]
GO
/****** Object:  ForeignKey [ac_Orders_ac_OrderNotes_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderNotes]  WITH CHECK ADD  CONSTRAINT [ac_Orders_ac_OrderNotes_FK1] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ac_Orders] ([OrderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderNotes] CHECK CONSTRAINT [ac_Orders_ac_OrderNotes_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_OrderNotes_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderNotes]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_OrderNotes_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderNotes] CHECK CONSTRAINT [ac_Users_ac_OrderNotes_FK1]
GO
/****** Object:  ForeignKey [ac_Affiliates_ac_Orders_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Orders]  WITH CHECK ADD  CONSTRAINT [ac_Affiliates_ac_Orders_FK1] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[ac_Affiliates] ([AffiliateId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Orders] CHECK CONSTRAINT [ac_Affiliates_ac_Orders_FK1]
GO
/****** Object:  ForeignKey [ac_OrderStatuses_ac_Orders_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Orders]  WITH CHECK ADD  CONSTRAINT [ac_OrderStatuses_ac_Orders_FK1] FOREIGN KEY([OrderStatusId])
REFERENCES [dbo].[ac_OrderStatuses] ([OrderStatusId])
GO
ALTER TABLE [dbo].[ac_Orders] CHECK CONSTRAINT [ac_OrderStatuses_ac_Orders_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Orders_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Orders]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Orders_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Orders] CHECK CONSTRAINT [ac_Stores_ac_Orders_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_Orders_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Orders]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_Orders_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Orders] CHECK CONSTRAINT [ac_Users_ac_Orders_FK1]
GO
/****** Object:  ForeignKey [ac_Orders_ac_Shipments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderShipments]  WITH CHECK ADD  CONSTRAINT [ac_Orders_ac_Shipments_FK1] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ac_Orders] ([OrderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderShipments] CHECK CONSTRAINT [ac_Orders_ac_Shipments_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_OrderShipments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderShipments]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_OrderShipments_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderShipments] CHECK CONSTRAINT [ac_ShipMethods_ac_OrderShipments_FK1]
GO
/****** Object:  ForeignKey [ac_Warehouses_ac_Shipments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderShipments]  WITH CHECK ADD  CONSTRAINT [ac_Warehouses_ac_Shipments_FK1] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[ac_Warehouses] ([WarehouseId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_OrderShipments] CHECK CONSTRAINT [ac_Warehouses_ac_Shipments_FK1]
GO
/****** Object:  ForeignKey [ac_OrderStatuses_ac_OrderStatusActions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderStatusActions]  WITH CHECK ADD  CONSTRAINT [ac_OrderStatuses_ac_OrderStatusActions_FK1] FOREIGN KEY([OrderStatusId])
REFERENCES [dbo].[ac_OrderStatuses] ([OrderStatusId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderStatusActions] CHECK CONSTRAINT [ac_OrderStatuses_ac_OrderStatusActions_FK1]
GO
/****** Object:  ForeignKey [ac_Emails_ac_OrderStatusEmails_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderStatusEmails]  WITH CHECK ADD  CONSTRAINT [ac_Emails_ac_OrderStatusEmails_FK1] FOREIGN KEY([EmailTemplateId])
REFERENCES [dbo].[ac_EmailTemplates] ([EmailTemplateId])
GO
ALTER TABLE [dbo].[ac_OrderStatusEmails] CHECK CONSTRAINT [ac_Emails_ac_OrderStatusEmails_FK1]
GO
/****** Object:  ForeignKey [ac_OrderStatuses_ac_OrderStatusEmails_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderStatusEmails]  WITH CHECK ADD  CONSTRAINT [ac_OrderStatuses_ac_OrderStatusEmails_FK1] FOREIGN KEY([OrderStatusId])
REFERENCES [dbo].[ac_OrderStatuses] ([OrderStatusId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderStatusEmails] CHECK CONSTRAINT [ac_OrderStatuses_ac_OrderStatusEmails_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_OrderStatuses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderStatuses]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_OrderStatuses_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_OrderStatuses] CHECK CONSTRAINT [ac_Stores_ac_OrderStatuses_FK1]
GO
/****** Object:  ForeignKey [ac_OrderStatuses_ac_OrderStatusTriggers_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_OrderStatusTriggers]  WITH CHECK ADD  CONSTRAINT [ac_OrderStatuses_ac_OrderStatusTriggers_FK1] FOREIGN KEY([OrderStatusId])
REFERENCES [dbo].[ac_OrderStatuses] ([OrderStatusId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_OrderStatusTriggers] CHECK CONSTRAINT [ac_OrderStatuses_ac_OrderStatusTriggers_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_PageViews_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PageViews]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_PageViews_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_PageViews] CHECK CONSTRAINT [ac_Stores_ac_PageViews_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_PageViews_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PageViews]  WITH NOCHECK ADD  CONSTRAINT [ac_Users_ac_PageViews_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
GO
ALTER TABLE [dbo].[ac_PageViews] NOCHECK CONSTRAINT [ac_Users_ac_PageViews_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_PaymentGateways_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PaymentGateways]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_PaymentGateways_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_PaymentGateways] CHECK CONSTRAINT [ac_Stores_ac_PaymentGateways_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_PaymentMethodGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PaymentMethodGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_PaymentMethodGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_PaymentMethodGroups] CHECK CONSTRAINT [ac_Groups_ac_PaymentMethodGroups_FK1]
GO
/****** Object:  ForeignKey [ac_PaymentMethods_ac_PaymentMethodsGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PaymentMethodGroups]  WITH CHECK ADD  CONSTRAINT [ac_PaymentMethods_ac_PaymentMethodsGroups_FK1] FOREIGN KEY([PaymentMethodId])
REFERENCES [dbo].[ac_PaymentMethods] ([PaymentMethodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_PaymentMethodGroups] CHECK CONSTRAINT [ac_PaymentMethods_ac_PaymentMethodsGroups_FK1]
GO
/****** Object:  ForeignKey [ac_PaymentGateways_ac_PaymentMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PaymentMethods]  WITH CHECK ADD  CONSTRAINT [ac_PaymentGateways_ac_PaymentMethods_FK1] FOREIGN KEY([PaymentGatewayId])
REFERENCES [dbo].[ac_PaymentGateways] ([PaymentGatewayId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_PaymentMethods] CHECK CONSTRAINT [ac_PaymentGateways_ac_PaymentMethods_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_PaymentMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_PaymentMethods]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_PaymentMethods_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_PaymentMethods] CHECK CONSTRAINT [ac_Stores_ac_PaymentMethods_FK1]
GO
/****** Object:  ForeignKey [ac_Orders_ac_Payments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Payments]  WITH CHECK ADD  CONSTRAINT [ac_Orders_ac_Payments_FK1] FOREIGN KEY([OrderId])
REFERENCES [dbo].[ac_Orders] ([OrderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Payments] CHECK CONSTRAINT [ac_Orders_ac_Payments_FK1]
GO
/****** Object:  ForeignKey [ac_PaymentMethods_ac_Payments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Payments]  WITH CHECK ADD  CONSTRAINT [ac_PaymentMethods_ac_Payments_FK1] FOREIGN KEY([PaymentMethodId])
REFERENCES [dbo].[ac_PaymentMethods] ([PaymentMethodId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Payments] CHECK CONSTRAINT [ac_PaymentMethods_ac_Payments_FK1]
GO
/****** Object:  ForeignKey [ac_Subscriptions_ac_Payments_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Payments]  WITH CHECK ADD  CONSTRAINT [ac_Subscriptions_ac_Payments_FK1] FOREIGN KEY([SubscriptionId])
REFERENCES [dbo].[ac_Subscriptions] ([SubscriptionId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Payments] CHECK CONSTRAINT [ac_Subscriptions_ac_Payments_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductAssets_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductAssets]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductAssets_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductAssets] CHECK CONSTRAINT [ac_Products_ac_ProductAssets_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductCustomFields_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductCustomFields]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductCustomFields_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductCustomFields] CHECK CONSTRAINT [ac_Products_ac_ProductCustomFields_FK1]
GO
/****** Object:  ForeignKey [ac_DigitalGoods_ac_ProductDigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductDigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_DigitalGoods_ac_ProductDigitalGoods_FK1] FOREIGN KEY([DigitalGoodId])
REFERENCES [dbo].[ac_DigitalGoods] ([DigitalGoodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductDigitalGoods] CHECK CONSTRAINT [ac_DigitalGoods_ac_ProductDigitalGoods_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_DigitalGoods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductDigitalGoods]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_DigitalGoods_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductDigitalGoods] CHECK CONSTRAINT [ac_Products_ac_DigitalGoods_FK1]
GO
/****** Object:  ForeignKey [FK_ac_ProductGroups_ac_Groups]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductGroups]  WITH CHECK ADD  CONSTRAINT [FK_ac_ProductGroups_ac_Groups] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductGroups] CHECK CONSTRAINT [FK_ac_ProductGroups_ac_Groups]
GO
/****** Object:  ForeignKey [FK_ac_ProductGroups_ac_Products]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductGroups]  WITH CHECK ADD  CONSTRAINT [FK_ac_ProductGroups_ac_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductGroups] CHECK CONSTRAINT [FK_ac_ProductGroups_ac_Products]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductImages_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductImages]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductImages_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductImages] CHECK CONSTRAINT [ac_Products_ac_ProductImages_FK1]
GO
/****** Object:  ForeignKey [ac_KitComponents_ac_ProductKitComponents_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductKitComponents]  WITH CHECK ADD  CONSTRAINT [ac_KitComponents_ac_ProductKitComponents_FK1] FOREIGN KEY([KitComponentId])
REFERENCES [dbo].[ac_KitComponents] ([KitComponentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductKitComponents] CHECK CONSTRAINT [ac_KitComponents_ac_ProductKitComponents_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductKitComponents_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductKitComponents]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductKitComponents_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductKitComponents] CHECK CONSTRAINT [ac_Products_ac_ProductKitComponents_FK1]
GO
/****** Object:  ForeignKey [ac_Options_ac_ProductOptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductOptions]  WITH CHECK ADD  CONSTRAINT [ac_Options_ac_ProductOptions_FK1] FOREIGN KEY([OptionId])
REFERENCES [dbo].[ac_Options] ([OptionId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductOptions] CHECK CONSTRAINT [ac_Options_ac_ProductOptions_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductOptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductOptions]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductOptions_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductOptions] CHECK CONSTRAINT [ac_Products_ac_ProductOptions_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductProductTemplates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductProductTemplates]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductProductTemplates_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductProductTemplates] CHECK CONSTRAINT [ac_Products_ac_ProductProductTemplates_FK1]
GO
/****** Object:  ForeignKey [ac_ProductTemplates_ac_ProductProductTemplates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductProductTemplates]  WITH CHECK ADD  CONSTRAINT [ac_ProductTemplates_ac_ProductProductTemplates_FK1] FOREIGN KEY([ProductTemplateId])
REFERENCES [dbo].[ac_ProductTemplates] ([ProductTemplateId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductProductTemplates] CHECK CONSTRAINT [ac_ProductTemplates_ac_ProductProductTemplates_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductReviews_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductReviews]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductReviews_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_ProductReviews] CHECK CONSTRAINT [ac_Products_ac_ProductReviews_FK1]
GO
/****** Object:  ForeignKey [ac_ReviewerProfiles_ac_ProductReviews_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductReviews]  WITH CHECK ADD  CONSTRAINT [ac_ReviewerProfiles_ac_ProductReviews_FK1] FOREIGN KEY([ReviewerProfileId])
REFERENCES [dbo].[ac_ReviewerProfiles] ([ReviewerProfileId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductReviews] CHECK CONSTRAINT [ac_ReviewerProfiles_ac_ProductReviews_FK1]
GO
/****** Object:  ForeignKey [ac_Manufacturers_ac_Products_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products]  WITH CHECK ADD  CONSTRAINT [ac_Manufacturers_ac_Products_FK1] FOREIGN KEY([ManufacturerId])
REFERENCES [dbo].[ac_Manufacturers] ([ManufacturerId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Products] CHECK CONSTRAINT [ac_Manufacturers_ac_Products_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Products_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Products_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Products] CHECK CONSTRAINT [ac_Stores_ac_Products_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_Products_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_Products_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Products] CHECK CONSTRAINT [ac_TaxCodes_ac_Products_FK1]
GO
/****** Object:  ForeignKey [ac_Vendors_ac_Products_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products]  WITH CHECK ADD  CONSTRAINT [ac_Vendors_ac_Products_FK1] FOREIGN KEY([VendorId])
REFERENCES [dbo].[ac_Vendors] ([VendorId])
GO
ALTER TABLE [dbo].[ac_Products] CHECK CONSTRAINT [ac_Vendors_ac_Products_FK1]
GO
/****** Object:  ForeignKey [ac_Warehouses_ac_Products_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products]  WITH CHECK ADD  CONSTRAINT [ac_Warehouses_ac_Products_FK1] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[ac_Warehouses] ([WarehouseId])
GO
ALTER TABLE [dbo].[ac_Products] CHECK CONSTRAINT [ac_Warehouses_ac_Products_FK1]
GO
/****** Object:  ForeignKey [ac_WrapGroups_ac_Products_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Products]  WITH CHECK ADD  CONSTRAINT [ac_WrapGroups_ac_Products_FK1] FOREIGN KEY([WrapGroupId])
REFERENCES [dbo].[ac_WrapGroups] ([WrapGroupId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Products] CHECK CONSTRAINT [ac_WrapGroups_ac_Products_FK1]
GO
/****** Object:  ForeignKey [ac_InputFields_ac_ProductTemplateFields_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductTemplateFields]  WITH CHECK ADD  CONSTRAINT [ac_InputFields_ac_ProductTemplateFields_FK1] FOREIGN KEY([InputFieldId])
REFERENCES [dbo].[ac_InputFields] ([InputFieldId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductTemplateFields] CHECK CONSTRAINT [ac_InputFields_ac_ProductTemplateFields_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductTemplateFields_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductTemplateFields]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductTemplateFields_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductTemplateFields] CHECK CONSTRAINT [ac_Products_ac_ProductTemplateFields_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_ProductTemplates_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductTemplates]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_ProductTemplates_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_ProductTemplates] CHECK CONSTRAINT [ac_Stores_ac_ProductTemplates_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductVariations_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductVariants]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductVariations_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_ProductVariants] CHECK CONSTRAINT [ac_Products_ac_ProductVariations_FK1]
GO
/****** Object:  ForeignKey [ac_Discounts_ac_ProductDiscountAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductVolumeDiscounts]  WITH CHECK ADD  CONSTRAINT [ac_Discounts_ac_ProductDiscountAssn_FK1] FOREIGN KEY([VolumeDiscountId])
REFERENCES [dbo].[ac_VolumeDiscounts] ([VolumeDiscountId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductVolumeDiscounts] CHECK CONSTRAINT [ac_Discounts_ac_ProductDiscountAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductDiscountAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ProductVolumeDiscounts]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductDiscountAssn_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ProductVolumeDiscounts] CHECK CONSTRAINT [ac_Products_ac_ProductDiscountAssn_FK1]
GO
/****** Object:  ForeignKey [ac_Countries_ac_Provinces_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Provinces]  WITH CHECK ADD  CONSTRAINT [ac_Countries_ac_Provinces_FK1] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[ac_Countries] ([CountryCode])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Provinces] CHECK CONSTRAINT [ac_Countries_ac_Provinces_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Readmes_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Readmes]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Readmes_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Readmes] CHECK CONSTRAINT [ac_Stores_ac_Readmes_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Redirects_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Redirects]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Redirects_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Redirects] CHECK CONSTRAINT [ac_Stores_ac_Redirects_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_RelatedProducts_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_RelatedProducts]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_RelatedProducts_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_RelatedProducts] CHECK CONSTRAINT [ac_Products_ac_RelatedProducts_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_RelatedProducts_FK2]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_RelatedProducts]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_RelatedProducts_FK2] FOREIGN KEY([ChildProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_RelatedProducts] CHECK CONSTRAINT [ac_Products_ac_RelatedProducts_FK2]
GO
/****** Object:  ForeignKey [FK_ac_ReviewReminders_ac_Products]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ReviewReminders]  WITH CHECK ADD  CONSTRAINT [FK_ac_ReviewReminders_ac_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ReviewReminders] CHECK CONSTRAINT [FK_ac_ReviewReminders_ac_Products]
GO
/****** Object:  ForeignKey [FK_ac_ReviewReminders_ac_Users]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ReviewReminders]  WITH CHECK ADD  CONSTRAINT [FK_ac_ReviewReminders_ac_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ReviewReminders] CHECK CONSTRAINT [FK_ac_ReviewReminders_ac_Users]
GO
/****** Object:  ForeignKey [ac_Users_ac_SearchHistory_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SearchHistory]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_SearchHistory_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_SearchHistory] CHECK CONSTRAINT [ac_Users_ac_SearchHistory_FK1]
GO
/****** Object:  ForeignKey [ac_DigitalGoods_ac_SerialKeys_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SerialKeys]  WITH CHECK ADD  CONSTRAINT [ac_DigitalGoods_ac_SerialKeys_FK1] FOREIGN KEY([DigitalGoodId])
REFERENCES [dbo].[ac_DigitalGoods] ([DigitalGoodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_SerialKeys] CHECK CONSTRAINT [ac_DigitalGoods_ac_SerialKeys_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_ShipGateways_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipGateways]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_ShipGateways_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_ShipGateways] CHECK CONSTRAINT [ac_Stores_ac_ShipGateways_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_ShipMethodGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethodGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_ShipMethodGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
GO
ALTER TABLE [dbo].[ac_ShipMethodGroups] CHECK CONSTRAINT [ac_Groups_ac_ShipMethodGroups_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_ShipMethodGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethodGroups]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_ShipMethodGroups_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
GO
ALTER TABLE [dbo].[ac_ShipMethodGroups] CHECK CONSTRAINT [ac_ShipMethods_ac_ShipMethodGroups_FK1]
GO
/****** Object:  ForeignKey [ac_ShipGateways_ac_ShipMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethods]  WITH CHECK ADD  CONSTRAINT [ac_ShipGateways_ac_ShipMethods_FK1] FOREIGN KEY([ShipGatewayId])
REFERENCES [dbo].[ac_ShipGateways] ([ShipGatewayId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipMethods] CHECK CONSTRAINT [ac_ShipGateways_ac_ShipMethods_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_ShipMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethods]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_ShipMethods_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_ShipMethods] CHECK CONSTRAINT [ac_Stores_ac_ShipMethods_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_ShipMethods_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethods]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_ShipMethods_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_ShipMethods] CHECK CONSTRAINT [ac_TaxCodes_ac_ShipMethods_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_ShipMethodShipZones_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethodShipZones]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_ShipMethodShipZones_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipMethodShipZones] CHECK CONSTRAINT [ac_ShipMethods_ac_ShipMethodShipZones_FK1]
GO
/****** Object:  ForeignKey [ac_ShipZones_ac_ShipMethodShipZones_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethodShipZones]  WITH CHECK ADD  CONSTRAINT [ac_ShipZones_ac_ShipMethodShipZones_FK1] FOREIGN KEY([ShipZoneId])
REFERENCES [dbo].[ac_ShipZones] ([ShipZoneId])
GO
ALTER TABLE [dbo].[ac_ShipMethodShipZones] CHECK CONSTRAINT [ac_ShipZones_ac_ShipMethodShipZones_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_ShipMethodWarehouses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethodWarehouses]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_ShipMethodWarehouses_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipMethodWarehouses] CHECK CONSTRAINT [ac_ShipMethods_ac_ShipMethodWarehouses_FK1]
GO
/****** Object:  ForeignKey [ac_Warehouses_ac_ShipMethodWarehouses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipMethodWarehouses]  WITH CHECK ADD  CONSTRAINT [ac_Warehouses_ac_ShipMethodWarehouses_FK1] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[ac_Warehouses] ([WarehouseId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipMethodWarehouses] CHECK CONSTRAINT [ac_Warehouses_ac_ShipMethodWarehouses_FK1]
GO
/****** Object:  ForeignKey [ac_ShipMethods_ac_ShipMatrix_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipRateMatrix]  WITH CHECK ADD  CONSTRAINT [ac_ShipMethods_ac_ShipMatrix_FK1] FOREIGN KEY([ShipMethodId])
REFERENCES [dbo].[ac_ShipMethods] ([ShipMethodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipRateMatrix] CHECK CONSTRAINT [ac_ShipMethods_ac_ShipMatrix_FK1]
GO
/****** Object:  ForeignKey [ac_Countries_ac_ShipZoneCountries_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipZoneCountries]  WITH CHECK ADD  CONSTRAINT [ac_Countries_ac_ShipZoneCountries_FK1] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[ac_Countries] ([CountryCode])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipZoneCountries] CHECK CONSTRAINT [ac_Countries_ac_ShipZoneCountries_FK1]
GO
/****** Object:  ForeignKey [ac_ShipZones_ac_ShipZoneCountries_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipZoneCountries]  WITH CHECK ADD  CONSTRAINT [ac_ShipZones_ac_ShipZoneCountries_FK1] FOREIGN KEY([ShipZoneId])
REFERENCES [dbo].[ac_ShipZones] ([ShipZoneId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipZoneCountries] CHECK CONSTRAINT [ac_ShipZones_ac_ShipZoneCountries_FK1]
GO
/****** Object:  ForeignKey [ac_Provinces_ac_ShipZoneProvinces_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipZoneProvinces]  WITH CHECK ADD  CONSTRAINT [ac_Provinces_ac_ShipZoneProvinces_FK1] FOREIGN KEY([ProvinceId])
REFERENCES [dbo].[ac_Provinces] ([ProvinceId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipZoneProvinces] CHECK CONSTRAINT [ac_Provinces_ac_ShipZoneProvinces_FK1]
GO
/****** Object:  ForeignKey [ac_ShipZones_ac_ShipZoneProvinces_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipZoneProvinces]  WITH CHECK ADD  CONSTRAINT [ac_ShipZones_ac_ShipZoneProvinces_FK1] FOREIGN KEY([ShipZoneId])
REFERENCES [dbo].[ac_ShipZones] ([ShipZoneId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_ShipZoneProvinces] CHECK CONSTRAINT [ac_ShipZones_ac_ShipZoneProvinces_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_ShipZones_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_ShipZones]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_ShipZones_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_ShipZones] CHECK CONSTRAINT [ac_Stores_ac_ShipZones_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_SpecialGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SpecialGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_SpecialGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_SpecialGroups] CHECK CONSTRAINT [ac_Groups_ac_SpecialGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Specials_ac_SpecialGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SpecialGroups]  WITH CHECK ADD  CONSTRAINT [ac_Specials_ac_SpecialGroups_FK1] FOREIGN KEY([SpecialId])
REFERENCES [dbo].[ac_Specials] ([SpecialId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_SpecialGroups] CHECK CONSTRAINT [ac_Specials_ac_SpecialGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_Specials_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Specials]  WITH NOCHECK ADD  CONSTRAINT [ac_Products_ac_Specials_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_Specials] NOCHECK CONSTRAINT [ac_Products_ac_Specials_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_StoreSettings_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_StoreSettings]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_StoreSettings_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_StoreSettings] CHECK CONSTRAINT [ac_Stores_ac_StoreSettings_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_SubscriptionPlans_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SubscriptionPlans]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_SubscriptionPlans_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_SubscriptionPlans] CHECK CONSTRAINT [ac_Groups_ac_SubscriptionPlans_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_SubscriptionPlans_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SubscriptionPlans]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_SubscriptionPlans_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_SubscriptionPlans] CHECK CONSTRAINT [ac_Products_ac_SubscriptionPlans_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_SubscriptionPlans_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_SubscriptionPlans]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_SubscriptionPlans_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_SubscriptionPlans] CHECK CONSTRAINT [ac_TaxCodes_ac_SubscriptionPlans_FK1]
GO
/****** Object:  ForeignKey [ac_OrderItems_ac_Subscriptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Subscriptions]  WITH CHECK ADD  CONSTRAINT [ac_OrderItems_ac_Subscriptions_FK1] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[ac_OrderItems] ([OrderItemId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Subscriptions] CHECK CONSTRAINT [ac_OrderItems_ac_Subscriptions_FK1]
GO
/****** Object:  ForeignKey [ac_SubscriptionPlans_ac_Subscriptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Subscriptions]  WITH CHECK ADD  CONSTRAINT [ac_SubscriptionPlans_ac_Subscriptions_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_SubscriptionPlans] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Subscriptions] CHECK CONSTRAINT [ac_SubscriptionPlans_ac_Subscriptions_FK1]
GO
/****** Object:  ForeignKey [ac_Transactions_ac_Subscriptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Subscriptions]  WITH CHECK ADD  CONSTRAINT [ac_Transactions_ac_Subscriptions_FK1] FOREIGN KEY([TransactionId])
REFERENCES [dbo].[ac_Transactions] ([TransactionId])
GO
ALTER TABLE [dbo].[ac_Subscriptions] CHECK CONSTRAINT [ac_Transactions_ac_Subscriptions_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_Subscriptions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Subscriptions]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_Subscriptions_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Subscriptions] CHECK CONSTRAINT [ac_Users_ac_Subscriptions_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_TaxCodes_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxCodes]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_TaxCodes_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_TaxCodes] CHECK CONSTRAINT [ac_Stores_ac_TaxCodes_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_TaxGateways_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxGateways]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_TaxGateways_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_TaxGateways] CHECK CONSTRAINT [ac_Stores_ac_TaxGateways_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_TaxRuleGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRuleGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_TaxRuleGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TaxRuleGroups] CHECK CONSTRAINT [ac_Groups_ac_TaxRuleGroups_FK1]
GO
/****** Object:  ForeignKey [ac_TaxRules_ac_TaxRuleGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRuleGroups]  WITH CHECK ADD  CONSTRAINT [ac_TaxRules_ac_TaxRuleGroups_FK1] FOREIGN KEY([TaxRuleId])
REFERENCES [dbo].[ac_TaxRules] ([TaxRuleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TaxRuleGroups] CHECK CONSTRAINT [ac_TaxRules_ac_TaxRuleGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_TaxRules_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRules]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_TaxRules_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_TaxRules] CHECK CONSTRAINT [ac_Stores_ac_TaxRules_FK1]
GO
/****** Object:  ForeignKey [ac_ShipZones_ac_TaxRuleShipZones_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRuleShipZones]  WITH CHECK ADD  CONSTRAINT [ac_ShipZones_ac_TaxRuleShipZones_FK1] FOREIGN KEY([ShipZoneId])
REFERENCES [dbo].[ac_ShipZones] ([ShipZoneId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TaxRuleShipZones] CHECK CONSTRAINT [ac_ShipZones_ac_TaxRuleShipZones_FK1]
GO
/****** Object:  ForeignKey [ac_TaxRules_ac_TaxRuleShipZones_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRuleShipZones]  WITH CHECK ADD  CONSTRAINT [ac_TaxRules_ac_TaxRuleShipZones_FK1] FOREIGN KEY([TaxRuleId])
REFERENCES [dbo].[ac_TaxRules] ([TaxRuleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TaxRuleShipZones] CHECK CONSTRAINT [ac_TaxRules_ac_TaxRuleShipZones_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_TaxRuleTaxCodeAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRuleTaxCodes]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_TaxRuleTaxCodeAssn_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TaxRuleTaxCodes] CHECK CONSTRAINT [ac_TaxCodes_ac_TaxRuleTaxCodeAssn_FK1]
GO
/****** Object:  ForeignKey [ac_TaxRules_ac_TaxRuleTaxCodeAssn_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TaxRuleTaxCodes]  WITH CHECK ADD  CONSTRAINT [ac_TaxRules_ac_TaxRuleTaxCodeAssn_FK1] FOREIGN KEY([TaxRuleId])
REFERENCES [dbo].[ac_TaxRules] ([TaxRuleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TaxRuleTaxCodes] CHECK CONSTRAINT [ac_TaxRules_ac_TaxRuleTaxCodeAssn_FK1]
GO
/****** Object:  ForeignKey [ac_ShipGateways_ac_Tracking_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TrackingNumbers]  WITH CHECK ADD  CONSTRAINT [ac_ShipGateways_ac_Tracking_FK1] FOREIGN KEY([ShipGatewayId])
REFERENCES [dbo].[ac_ShipGateways] ([ShipGatewayId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_TrackingNumbers] CHECK CONSTRAINT [ac_ShipGateways_ac_Tracking_FK1]
GO
/****** Object:  ForeignKey [ac_Shipments_ac_Tracking_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_TrackingNumbers]  WITH CHECK ADD  CONSTRAINT [ac_Shipments_ac_Tracking_FK1] FOREIGN KEY([OrderShipmentId])
REFERENCES [dbo].[ac_OrderShipments] ([OrderShipmentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_TrackingNumbers] CHECK CONSTRAINT [ac_Shipments_ac_Tracking_FK1]
GO
/****** Object:  ForeignKey [ac_PaymentGateways_ac_Transactions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Transactions]  WITH CHECK ADD  CONSTRAINT [ac_PaymentGateways_ac_Transactions_FK1] FOREIGN KEY([PaymentGatewayId])
REFERENCES [dbo].[ac_PaymentGateways] ([PaymentGatewayId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Transactions] CHECK CONSTRAINT [ac_PaymentGateways_ac_Transactions_FK1]
GO
/****** Object:  ForeignKey [ac_Payments_ac_Transactions_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Transactions]  WITH CHECK ADD  CONSTRAINT [ac_Payments_ac_Transactions_FK1] FOREIGN KEY([PaymentId])
REFERENCES [dbo].[ac_Payments] ([PaymentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_Transactions] CHECK CONSTRAINT [ac_Payments_ac_Transactions_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_UpsellProducts_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_UpsellProducts]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_UpsellProducts_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_UpsellProducts] CHECK CONSTRAINT [ac_Products_ac_UpsellProducts_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_UserGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_UserGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_UserGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_UserGroups] CHECK CONSTRAINT [ac_Groups_ac_UserGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Subscriptions_ac_UserGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_UserGroups]  WITH CHECK ADD  CONSTRAINT [ac_Subscriptions_ac_UserGroups_FK1] FOREIGN KEY([SubscriptionId])
REFERENCES [dbo].[ac_Subscriptions] ([SubscriptionId])
GO
ALTER TABLE [dbo].[ac_UserGroups] CHECK CONSTRAINT [ac_Subscriptions_ac_UserGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_UserGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_UserGroups]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_UserGroups_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_UserGroups] CHECK CONSTRAINT [ac_Users_ac_UserGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_UserPasswords_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_UserPasswords]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_UserPasswords_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_UserPasswords] CHECK CONSTRAINT [ac_Users_ac_UserPasswords_FK1]
GO
/****** Object:  ForeignKey [ac_Affiliates_ac_Users_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Users]  WITH CHECK ADD  CONSTRAINT [ac_Affiliates_ac_Users_FK1] FOREIGN KEY([AffiliateId])
REFERENCES [dbo].[ac_Affiliates] ([AffiliateId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Users] CHECK CONSTRAINT [ac_Affiliates_ac_Users_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Users_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Users]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Users_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Users] CHECK CONSTRAINT [ac_Stores_ac_Users_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_UserSettings_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_UserSettings]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_UserSettings_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_UserSettings] CHECK CONSTRAINT [ac_Users_ac_UserSettings_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_VendorGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_VendorGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_VendorGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
GO
ALTER TABLE [dbo].[ac_VendorGroups] CHECK CONSTRAINT [ac_Groups_ac_VendorGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Vendors_ac_VendorGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_VendorGroups]  WITH CHECK ADD  CONSTRAINT [ac_Vendors_ac_VendorGroups_FK1] FOREIGN KEY([VendorId])
REFERENCES [dbo].[ac_Vendors] ([VendorId])
GO
ALTER TABLE [dbo].[ac_VendorGroups] CHECK CONSTRAINT [ac_Vendors_ac_VendorGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Vendors_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Vendors]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Vendors_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Vendors] CHECK CONSTRAINT [ac_Stores_ac_Vendors_FK1]
GO
/****** Object:  ForeignKey [ac_Groups_ac_VolumeDiscountGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_VolumeDiscountGroups]  WITH CHECK ADD  CONSTRAINT [ac_Groups_ac_VolumeDiscountGroups_FK1] FOREIGN KEY([GroupId])
REFERENCES [dbo].[ac_Groups] ([GroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_VolumeDiscountGroups] CHECK CONSTRAINT [ac_Groups_ac_VolumeDiscountGroups_FK1]
GO
/****** Object:  ForeignKey [ac_VolumeDiscounts_ac_VolumeDiscountGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_VolumeDiscountGroups]  WITH CHECK ADD  CONSTRAINT [ac_VolumeDiscounts_ac_VolumeDiscountGroups_FK1] FOREIGN KEY([VolumeDiscountId])
REFERENCES [dbo].[ac_VolumeDiscounts] ([VolumeDiscountId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_VolumeDiscountGroups] CHECK CONSTRAINT [ac_VolumeDiscounts_ac_VolumeDiscountGroups_FK1]
GO
/****** Object:  ForeignKey [ac_Discounts_ac_DiscountMatrix_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_VolumeDiscountLevels]  WITH CHECK ADD  CONSTRAINT [ac_Discounts_ac_DiscountMatrix_FK1] FOREIGN KEY([VolumeDiscountId])
REFERENCES [dbo].[ac_VolumeDiscounts] ([VolumeDiscountId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_VolumeDiscountLevels] CHECK CONSTRAINT [ac_Discounts_ac_DiscountMatrix_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Discounts_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_VolumeDiscounts]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Discounts_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_VolumeDiscounts] CHECK CONSTRAINT [ac_Stores_ac_Discounts_FK1]
GO
/****** Object:  ForeignKey [ac_Countries_ac_Warehouses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Warehouses]  WITH CHECK ADD  CONSTRAINT [ac_Countries_ac_Warehouses_FK1] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[ac_Countries] ([CountryCode])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_Warehouses] CHECK CONSTRAINT [ac_Countries_ac_Warehouses_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Warehouses_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Warehouses]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Warehouses_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Warehouses] CHECK CONSTRAINT [ac_Stores_ac_Warehouses_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_Webpages_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Webpages]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_Webpages_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_Webpages] CHECK CONSTRAINT [ac_Stores_ac_Webpages_FK1]
GO
/****** Object:  ForeignKey [ac_InputFields_ac_WishlistItemInputs_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WishlistItemInputs]  WITH CHECK ADD  CONSTRAINT [ac_InputFields_ac_WishlistItemInputs_FK1] FOREIGN KEY([InputFieldId])
REFERENCES [dbo].[ac_InputFields] ([InputFieldId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_WishlistItemInputs] CHECK CONSTRAINT [ac_InputFields_ac_WishlistItemInputs_FK1]
GO
/****** Object:  ForeignKey [ac_WishlistItems_ac_WishlistItemInputs_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WishlistItemInputs]  WITH CHECK ADD  CONSTRAINT [ac_WishlistItems_ac_WishlistItemInputs_FK1] FOREIGN KEY([WishlistItemId])
REFERENCES [dbo].[ac_WishlistItems] ([WishlistItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_WishlistItemInputs] CHECK CONSTRAINT [ac_WishlistItems_ac_WishlistItemInputs_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_WishlistItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WishlistItems]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_WishlistItems_FK1] FOREIGN KEY([ProductId])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[ac_WishlistItems] CHECK CONSTRAINT [ac_Products_ac_WishlistItems_FK1]
GO
/****** Object:  ForeignKey [ac_Wishlists_ac_WishlistItems_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WishlistItems]  WITH CHECK ADD  CONSTRAINT [ac_Wishlists_ac_WishlistItems_FK1] FOREIGN KEY([WishlistId])
REFERENCES [dbo].[ac_Wishlists] ([WishlistId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_WishlistItems] CHECK CONSTRAINT [ac_Wishlists_ac_WishlistItems_FK1]
GO
/****** Object:  ForeignKey [ac_Users_ac_Wishlists_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_Wishlists]  WITH CHECK ADD  CONSTRAINT [ac_Users_ac_Wishlists_FK1] FOREIGN KEY([UserId])
REFERENCES [dbo].[ac_Users] ([UserId])
GO
ALTER TABLE [dbo].[ac_Wishlists] CHECK CONSTRAINT [ac_Users_ac_Wishlists_FK1]
GO
/****** Object:  ForeignKey [ac_Stores_ac_WrapGroups_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WrapGroups]  WITH CHECK ADD  CONSTRAINT [ac_Stores_ac_WrapGroups_FK1] FOREIGN KEY([StoreId])
REFERENCES [dbo].[ac_Stores] ([StoreId])
GO
ALTER TABLE [dbo].[ac_WrapGroups] CHECK CONSTRAINT [ac_Stores_ac_WrapGroups_FK1]
GO
/****** Object:  ForeignKey [ac_TaxCodes_ac_WrapStyles_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WrapStyles]  WITH CHECK ADD  CONSTRAINT [ac_TaxCodes_ac_WrapStyles_FK1] FOREIGN KEY([TaxCodeId])
REFERENCES [dbo].[ac_TaxCodes] ([TaxCodeId])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ac_WrapStyles] CHECK CONSTRAINT [ac_TaxCodes_ac_WrapStyles_FK1]
GO
/****** Object:  ForeignKey [ac_WrapGroups_ac_WrapStyles_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[ac_WrapStyles]  WITH CHECK ADD  CONSTRAINT [ac_WrapGroups_ac_WrapStyles_FK1] FOREIGN KEY([WrapGroupId])
REFERENCES [dbo].[ac_WrapGroups] ([WrapGroupId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ac_WrapStyles] CHECK CONSTRAINT [ac_WrapGroups_ac_WrapStyles_FK1]
GO
/****** Object:  ForeignKey [ac_Products_ac_ProductExtended_FK1]    Script Date: 03/03/2015 22:05:10 ******/
ALTER TABLE [dbo].[bh_ProductExtended]  WITH CHECK ADD  CONSTRAINT [ac_Products_ac_ProductExtended_FK1] FOREIGN KEY([ProductID])
REFERENCES [dbo].[ac_Products] ([ProductId])
GO
ALTER TABLE [dbo].[bh_ProductExtended] CHECK CONSTRAINT [ac_Products_ac_ProductExtended_FK1]
GO
